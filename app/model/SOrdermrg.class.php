<?php
 class SOrdermrg {

 	public function additem(){
		$resultreturn = $this->_additem();
		return $resultreturn;
	}
	public function updateQTY(){
		$resultreturn = $this->_updateQTY();
		return $resultreturn;
	}
	public function updateComment(){
		$resultreturn = $this->_updateComment();
		return $resultreturn;
	}
	public function removeItem($key){
		$resultreturn = $this->_removeItem($key);
		return $resultreturn;
	}
	public function takeOrder(){
		$resultreturn = $this->_takeOrder();
		return $resultreturn;
	}
	public function orderList(){
		$resultreturn = $this->_orderList();
		return $resultreturn;
	}
	public function orderInfo(){
		$resultreturn = $this->_orderInfo();
		return $resultreturn;
	}
	public function itemList(){
		$resultreturn = $this->_itemList();
		return $resultreturn;
	}
	public function delItem(){
		$resultreturn = $this->_delItem();
		return $resultreturn;
	}
	public function reOrder($orderid){
		$resultreturn = $this->_reOrder($orderid);
		return $resultreturn;
	}
	public function cancelOrder(){
		$resultreturn = $this->_cancelOrder();
		return $resultreturn;
	}
	public function getRate(){
		$resultreturn = $this->_getRate();
		return $resultreturn;
	}
	public function exportExcel(){
		$resultreturn = $this->_exportExcel();
		return $resultreturn;
	}

    /*Weerasak 16-10-2564*/
    public function updateAmountSaller(){
        $resultreturn = $this->_updateAmountSaller();
        return $resultreturn;
    }
    public function updateDiscountSaller(){
        $resultreturn = $this->_updateDiscountSaller();
        return $resultreturn;
    }
    public function updateShippingSaller(){
        $resultreturn = $this->_updateCNShipping();
        return $resultreturn;
    }
    /*End Edit*/

	public function orderStatus(){
		$resultreturn = $this->_orderStatus();
		return $resultreturn;
	}
   public function orderStatus2(){
		$resultreturn = $this->_orderStatus2();
		return $resultreturn;
	}
	public function orderStatus3(){
		$resultreturn = $this->_orderStatus3();
		return $resultreturn;
	}
	public function itemStatus(){
		$resultreturn = $this->_itemStatus();
		return $resultreturn;
	}
   public function itemType(){
		$resultreturn = $this->_itemType();
		return $resultreturn;
	}
	public function itemComment(){
		$resultreturn = $this->_itemComment();
		return $resultreturn;
	}
	public function priceRmb(){
		$resultreturn = $this->_priceRmb();
		return $resultreturn;
	}
   public function priceRmb2(){
		$resultreturn = $this->_priceRmb2();
		return $resultreturn;
	}
   public function priceCnShip(){
		$resultreturn = $this->_priceCnShip();
		return $resultreturn;
	}
	public function priceThb(){
		$resultreturn = $this->_priceThb();
		return $resultreturn;
	}
	public function sallerNumber(){
		$resultreturn = $this->_sallerNumber();
		return $resultreturn;
	}
	public function itemQty(){
		$resultreturn = $this->_itemQty();
		return $resultreturn;
	}
   public function itemQty2(){
		$resultreturn = $this->_itemQty2();
		return $resultreturn;
	}
	public function addOtherItem(){
		$resultreturn = $this->_addOtherItem();
		return $resultreturn;
	}
	public function paymentList(){
		$resultreturn = $this->_paymentList();
		return $resultreturn;
	}
	public function paymentStatus(){
		$resultreturn = $this->_paymentStatus();
		return $resultreturn;
	}
	public function orderPay(){
		$resultreturn = $this->_orderPay();
		return $resultreturn;
	}

	public function moneyWallet(){
		$resultreturn = $this->_getMoneyFromWallet();
		return $resultreturn;
	}
	public function importOrderExcel(){
		$resultreturn = $this->_importOrderExcel();
		return $resultreturn;
	}
	public function notiBuyStatus(){
		$resultreturn = $this->_notiBuyStatus();
		return $resultreturn;
	}
	public function refundToWallet(){
		$resultreturn = $this->_refundToWallet();
		return $resultreturn;
	}


   public function refundlist(){
 		$resultreturn = $this->_refundList();
 		return $resultreturn;
 	}
 	 public function refundListExcel(){
 		$resultreturn = $this->_refundListExcel();
 		return $resultreturn;
 	}
   public function cancelRefund(){
  		$resultreturn = $this->_cancelRefund();
  		return $resultreturn;
  	}
   public function refundinfo(){
     $resultreturn = $this->_refundInfo();
     return $resultreturn;
   }
   public function updateRefund(){
     $resultreturn = $this->_updateRefund();
     return $resultreturn;
   }
   public function refundToWalletByREF(){
     $resultreturn = $this->_refundToWalletByREF();
     return $resultreturn;
   }
   public function refundToWalletByOrder(){
     $resultreturn = $this->_refundToWalletByOrder();
     return $resultreturn;
   }
   public function exportRefund(){
     $resultreturn = $this->_exportRefund();
     return $resultreturn;
   }


   public function orderListBuy(){
		$resultreturn = $this->_orderListBuy();
		return $resultreturn;
    }
    public function orderListBuyUser(){
		$resultreturn = $this->_orderListBuyUser();
		return $resultreturn;
	}
   public function orderListBuyView(){
		$resultreturn = $this->_orderListBuyView();
		return $resultreturn;
	}
    public function getSallerOrder(){  //Weerasak 29-10-2564 คำนวณยอดการซื้อเดิมคนจีน
        $resultreturn = $this->_orderBySaller();
        return $resultreturn;
    }
   public function getshipItem(){
		$resultreturn = $this->_getshipItem();
		return $resultreturn;
	}
   public function updateshipItem(){
		$resultreturn = $this->_updateshipItem();
		return $resultreturn;
	}
   public function addshipItem(){
		$resultreturn = $this->_addshipItem();
		return $resultreturn;
	}
   public function sendOrderToCN(){
		$resultreturn = $this->_sendOrderToCN();
		return $resultreturn;
    }
    public function sendOrderToCNUser(){
		$resultreturn = $this->_sendOrderToCNUser();
		return $resultreturn;
	}

   public function uploadTempOrder(){
		$resultreturn = $this->_uploadTempOrder();
		return $resultreturn;
	}
   public function updateItemImg(){
 		$resultreturn = $this->_updateItemImg();
 		return $resultreturn;
 	}

   public function updateTrueRate(){
 		$resultreturn = $this->_updateTrueRate();
 		return $resultreturn;
 	}

     public function ordernoTrackList(){
 		$resultreturn = $this->_ordernoTrackList();
 		return $resultreturn;
     }
     public function ordernoTrackListSend(){
 		$resultreturn = $this->_ordernoTrackListSend();
 		return $resultreturn;
     }
     public function uploadSallerImage(){
        $resultreturn = $this->_uploadSallerImage();
        return $resultreturn;
    }
    public function mailOrderNoTracking(){
        $resultreturn = $this->_mailOrderNoTracking();
        return $resultreturn;
    }
    public function setNotrackToDash(){
        $resultreturn = $this->_setNotrackToDash();
        return $resultreturn;
    }
    public function setNotrackComment(){
        $resultreturn = $this->_setNotrackComment();
        return $resultreturn;
    }
    public function outsourceList(){
        $resultreturn = $this->_outsourceList();
        return $resultreturn;
    }
  public function outsourceListNoTrack(){
        $resultreturn = $this->_outsourceListNoTrack();
        return $resultreturn;
    }

    public function updateServiceShipping(){
      $resultreturn = $this->_updateServiceShipping();
      return $resultreturn;
    }

 	private function _additem(){

		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$params = $base->get('POST');
 		//GF::print_r($_SESSION);

		if(empty($_SESSION['items'])){
			$_SESSION['items'] = 0;
		}
		$pdname = 'N';
		$key_opt = '0';
		foreach($_SESSION['item'] as $keyx=>$valc){
			if($valc['product_name']==$params['product_name']){
				$pdname = 'C';
				$key_opt = $keyx;
			}
		}

		if($pdname=='C'){
			$optionall = count($params['op_name']);
			$kx = 0;
			foreach($params['op_name'] as $key=>$vals){
				//$_SESSION['item'][$key_opt]['product_option'][$key]['op_name'] = $vals;
				//$_SESSION['item'][$key_opt]['product_option'][$key]['op_value'] = $params['op_value'][$key];
				if($_SESSION['item'][$key_opt]['product_option'][$key]['op_name']==$vals && $_SESSION['item'][$key_opt]['product_option'][$key]['op_value']==$params['op_value'][$key]){
					$kx++;
				}
			}

			if($kx==$optionall){
				$_SESSION['item'][$key_opt]['product_qty'] = $params['product_qty']+$_SESSION['item'][$key_opt]['product_qty'];
			}else{
				$_SESSION['items'] = $_SESSION['items']+1;
				$_SESSION['item'][$_SESSION['items']]['product_link'] = $params['product_link'];
				$_SESSION['item'][$_SESSION['items']]['product_name'] = $params['product_name'];
				$_SESSION['item'][$_SESSION['items']]['product_image'] = $params['pd_img'];
				$_SESSION['item'][$_SESSION['items']]['saller_name'] = $params['shop_name'];
				$_SESSION['item'][$_SESSION['items']]['saller_link'] = $params['shop_link'];
				$_SESSION['item'][$_SESSION['items']]['product_qty'] = $params['product_qty'];
				$_SESSION['item'][$_SESSION['items']]['product_price_rmb'] = $params['product_price_rmb'];
				$_SESSION['item'][$_SESSION['items']]['product_price_thb'] = $params['product_price_thb'];
				$_SESSION['item'][$_SESSION['items']]['comment'] = '';
				foreach($params['op_name'] as $key=>$vals){
					$_SESSION['item'][$_SESSION['items']]['product_option'][$key]['op_name'] = $vals;
					$_SESSION['item'][$_SESSION['items']]['product_option'][$key]['op_value'] = $params['op_value'][$key];
					$_SESSION['item'][$_SESSION['items']]['product_option'][$key]['op_name_th'] = $params['op_name_th'][$key];
					$_SESSION['item'][$_SESSION['items']]['product_option'][$key]['op_value_th'] = $params['op_value_th'][$key];
				}
			}


		}else{
			$_SESSION['items'] = $_SESSION['items']+1;
			$_SESSION['item'][$_SESSION['items']]['product_link'] = $params['product_link'];
			$_SESSION['item'][$_SESSION['items']]['product_name'] = $params['product_name'];
			$_SESSION['item'][$_SESSION['items']]['product_image'] = $params['pd_img'];
			$_SESSION['item'][$_SESSION['items']]['saller_name'] = $params['shop_name'];
			$_SESSION['item'][$_SESSION['items']]['saller_link'] = $params['shop_link'];
			$_SESSION['item'][$_SESSION['items']]['product_qty'] = $params['product_qty'];
			$_SESSION['item'][$_SESSION['items']]['product_price_rmb'] = $params['product_price_rmb'];
			$_SESSION['item'][$_SESSION['items']]['product_price_thb'] = $params['product_price_thb'];
			$_SESSION['item'][$_SESSION['items']]['comment'] = '';
			foreach($params['op_name'] as $key=>$vals){
				$_SESSION['item'][$_SESSION['items']]['product_option'][$key]['op_name'] = $vals;
				$_SESSION['item'][$_SESSION['items']]['product_option'][$key]['op_value'] = $params['op_value'][$key];
				$_SESSION['item'][$_SESSION['items']]['product_option'][$key]['op_name_th'] = $params['op_name_th'][$key];
				$_SESSION['item'][$_SESSION['items']]['product_option'][$key]['op_value_th'] = $params['op_value_th'][$key];
			}
		}
		return true;

		//aksort($_SESSION);
	}
	private function _updateQTY(){
		$base = Base::getInstance();

 		$params = $base->get('GET');
		$_SESSION['item'][$params['key']]['product_qty'] = $params['value'];
	}
	private function _updateComment(){
		$base = Base::getInstance();

 		$params = $base->get('GET');
		$_SESSION['item'][$params['key']]['comment'] = $params['value'];
	}
	private function _removeItem(){
		$base = Base::getInstance();
		unset($_SESSION['item'][$base->get('GET.key')]);
	}

	private function _takeOrder(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$array_param = $base->get('POST');

		$lasrorder = $this->_getlastorder();

		$year = substr($lasrorder,0,4);
		$month = substr($lasrorder,4,2);
		$times = substr($lasrorder,6,4);

		$c_year = date('Y');
		$c_month = date('m');

		$new_time = '';
		$order_id = '';
		if($year==$c_year && $month==$c_month){
			$new_time = intval($times)+1;
			$new_time = str_pad($new_time, 2, "0", STR_PAD_LEFT);
			$order_id = $year.$month.$new_time;
		}else{
			$order_id = $c_year.$c_month."01";
		}

		//$order_id = ;//(1000+$_SESSION['user_id_front']).date('dmY');
		$my_id = '';

		$sql = "INSERT INTO web_order (
							order_id,
							member_id,
							order_payment,
							order_ship_cn,
							order_box,
							order_ship_th,
							order_comment,
							rate,
							status,
							active_status,
							create_dtm,
							update_dtm
						) VALUES (
							".GF::quote($order_id).",
							".GF::quote($user_id).",
							".GF::quote($array_param['order_payment']).",
							".GF::quote($array_param['order_ship_cn']).",
							".GF::quote($array_param['order_box']).",
							".GF::quote($array_param['order_ship_th']).",
							".GF::quote($array_param['order_comment']).",
							".GF::quote($array_param['web_rate']).",
							'W',
							'O',
							NOW(),
							NOW()
						)";
		 $res = $db->Execute($sql);
		 $last_id = $db->Insert_ID();
		 $my_id = $last_id;
		 if($res){
		 	foreach($_SESSION['item'] as $key=>$vals){

		 		$img_name = GF::randomNum(25);
				$file = $vals['product_image'];
				$data = file_get_contents($file);
				$new = $base->get('BASEDIR').'/uploads/product/'.$img_name.".jpg";
				file_put_contents($new, $data);

				$vals['saller_name'] = trim($vals['saller_name']);
				if($vals['saller_name']==''){
					$vals['saller_name'] = 'N/A';
				}

				$sql = "INSERT INTO web_order_item (
									order_id,
									product_link,
									product_title,
									product_image,
									product_saller_name,
									product_saller_link,
									product_qty,
									product_price_rmb,
									product_price_thb,
									note,
									status,
									create_dtm
								) VALUES (
									".GF::quote($last_id).",
									".GF::quote($vals['product_link']).",
									".GF::quote($vals['product_name']).",
									".GF::quote($img_name.'.jpg').",
									".GF::quote($vals['saller_name']).",
									".GF::quote($vals['saller_link']).",
									".GF::quote($vals['product_qty']).",
									".GF::quote($vals['product_price_rmb']).",
									".GF::quote($vals['product_price_thb']).",
									".GF::quote($vals['comment']).",
									'W',
									NOW()
								)";
				 $res = $db->Execute($sql);
				 $item_id = $db->Insert_ID();
				 foreach($vals['product_option'] as $k_y=>$val_c){
					 	$sql = "INSERT INTO web_order_item_option (
											item_id,
											option_name,
											option_value,
											option_name_th,
											option_value_th
										) VALUES (
											".GF::quote($item_id).",
											".GF::quote($val_c['op_name']).",
											".GF::quote($val_c['op_value']).",
											".GF::quote($val_c['op_name_th']).",
											".GF::quote($val_c['op_value_th'])."
										)";
						 $res = $db->Execute($sql);
				 }
             //unset($_SESSION['item'][$key]);
			}
			unset($_SESSION['item']);
			unset($_SESSION['items']);

			$this->_gen_saller_number($my_id);

			/*$member = new Member();

			$userinfo = $member->userInfo();

			$mail_mssg = file_get_contents(BASEURL."/member/order_1/view_mail/".$my_id);
			//echo $mail_mssg.'fffff';
			$array_param['order'] = $order_id;
			$array_param['web_name'] = $userinfo['name'];
			$array_param['web_email'] = $userinfo['email'];
		 	$array_param['mail_body'] = $mail_mssg;
		 	$this->_sendMail($array_param);

		 	$admin = $this->_admininfo();


		 	$array_param['web_name'] = $admin['admin_name'];
			$array_param['web_email'] = $admin['email'];
		 	$array_param['mail_body'] = $mail_mssg;
		 	$this->_sendMail($array_param);*/

		 	return true;
		 }else{
		 	return false;
		 }
	}
	private function _gen_saller_number($order_id){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$base->set('_ids',$order_id);
		$item_list = $this->_itemList();

		$old_shop = '';
		$item_number = 1;
		foreach($item_list as $vals){
			if($vals['product_saller_name']!=$old_shop && $vals['product_saller_name']!=''){
				$old_shop=$vals['product_saller_name'];
				$sql = 'UPDATE web_order_item SET saller_number='.$item_number." WHERE product_saller_name='".$vals['product_saller_name']."'";
				$res = $db->Execute($sql);
				$item_number++;
			}

		}
		$sql = 'UPDATE web_order_item SET saller_number='.$item_number." WHERE order_id='".$order_id."' AND product_saller_name='' ";
		$res = $db->Execute($sql);

	}
   
	private function _orderList(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];


		$dtatReturn=array();

		/*$type = $base->get('GET.type');
		$type2 = $base->get('type');
		if($type==''){
			$type = 'ALL';
		}*/

		/*if(!empty($_POST)){
			if(!empty($_POST['s_ordernumber'])){
				$_SESSION['s_ordernumber'] = $_POST['s_ordernumber'];
			}
			if(!empty($_POST['s_status'])){
				$_SESSION['s_status'] = $_POST['s_status'];
			}
			if(!empty($_POST['s_start'])){
				$_SESSION['s_start'] = $_POST['s_start'];
			}
			if(!empty($_POST['s_end'])){
				$_SESSION['s_end'] = $_POST['s_end'];
			}
		}*/



		$cond = '';


		if($_SESSION['odr_search']['status']!=''){

			if($_SESSION['odr_search']['status']=='ALL'){
				//$cond = " AND status=".GF::quote($type);
			}else{
                
                if($_SESSION['odr_search']['status']=='W'){
                    $cond .= " AND o.status='3' AND o.export='N' ";
                }
                else if($_SESSION['odr_search']['status']=='S'){
                    $cond .= " AND o.status='3' AND o.export='Y' ";
                }else if($_SESSION['odr_search']['status']=='5'){ //edit by hatairat 04-12-64
                  $cond .= " AND o.status='3' AND o.export='S' ";
                }else{
                    $cond .= " AND o.status=".GF::quote($_SESSION['odr_search']['status']);
                }
			}
		}
		if($_SESSION['odr_search']['order_number']!=''){

			$cond .= " AND o.order_number LIKE '%".$_SESSION['odr_search']['order_number']."%' ";
        }
        
        
        if($_SESSION['odr_search']['buy_id']!=''){
            $user_id_ = GF::user_id($_SESSION['odr_search']['buy_id']);
            if(intval($user_id_)==0){
                $cond .= " AND o.buy_id='99999999999999999999' ";
            }else{
                $cond .= " AND o.buy_id=".intval($user_id_)." ";
            } 
            
        }

      if($_SESSION['odr_search']['user_code']!=''){
         $user_id = GF::user_id($_SESSION['odr_search']['user_code']);
			$cond .= " AND o.member_id =".GF::quote($user_id)." ";
		}

		if($_SESSION['odr_search']['create_dtm']!=''){
			$date_check = date('Y-m-d',strtotime($_SESSION['odr_search']['create_dtm']));
			$cond .= " AND o.create_dtm >=".GF::quote($date_check);
		}

      //$role_id = $memberInfomation['user_role_id'];
      $agency =  $memberInfomation['agency'];
      if($agency=='A'){
         $list_ref = GF::refgroup();
         if(count($list_ref)>0){
            //GF::print_r($list_ref);
            $ref_str = implode(',', $list_ref);
            $cond .= " AND o.member_id IN(".$ref_str.") ";
         }else{
            $cond .= " AND o.member_id ='X' ";
         }
      }
		
		//$sql = "SELECT * FROM web_order WHERE  active_status='O' ".$cond." ORDER BY update_dtm DESC,create_dtm DESC";
		
      /* รอตรวจสอบหากถูกต้องลบได้เลย
      $sql = "SELECT o.*,p.payment_amount,
		if((o.order_price - p.payment_amount) < 0 ,0
      ,(o.order_price - p.payment_amount)) as balance 
		FROM web_order o
		LEFT JOIN (SELECT SUM(payment_amount) payment_amount,order_id,member_id
								FROM web_payment WHERE status = 'A'
								GROUP BY order_id) p ON p.order_id = o.id
		WHERE  o.active_status='O' ".$cond." 
		ORDER BY o.update_dtm DESC,o.create_dtm DESC";
      */
      
      /*
      $sql = "SELECT DISTINCT *,(
            sum_price - payment_amount 
            ) AS over_payamount
            ,(sum_price - payment_amount_web) as overdue
            ,CASE WHEN payment_amount_web > 0 THEN
                     CASE WHEN (sum_price - payment_amount) > 0 THEN (sum_price - order_price)
                     ELSE (sum_price - order_price) END
                  WHEN status = '7' THEN sum_price
                  ELSE 0.00
            END balance
            ,if((SELECT COUNT(*) FROM project_withdraw WHERE status = 'A' AND user_id = P.member_id AND order_id = P.id) > 0,'Y','N') AS refund_status
            , case when abs((sum_price - payment_amount_web )) > 0 THEN
abs((sum_price - payment_amount_web )) - (SELECT payment_amount FROM web_payment WHERE member_id = P.member_id and order_id = P.id  and `status` = 'A' ORDER BY id desc LIMIT 1) ELSE 0 END as overdue_paid
         from (
         SELECT *,case WHEN order_price < sum_price && order_price > 0 then 
         order_price - payment_amount_web + sum_price 
         else payment_amount_web end payment_amount
         FROM (
                              SELECT
                                 o.*,
                                 IFNULL(ROUND(((i.order_price_rmb * o.old_rate) + o.order_pay_ship) - o.order_discount,2),o.order_price) as sum_price,
                                 IFNULL(p.payment_amount,0) AS payment_amount_web
                              FROM
                                 web_order o
                                 LEFT JOIN ( SELECT SUM( payment_amount ) payment_amount, order_id, member_id 
                                                   FROM web_payment WHERE STATUS = 'A' and order_type='1' GROUP BY order_id, member_id ) p ON p.order_id = o.id 
                                 LEFT JOIN (SELECT order_id,ROUND(SUM(product_price_rmb * product_qty),2) as order_price_rmb 
                                                   FROM web_order_item WHERE status = 'A' AND item_typs_s = 'NR' GROUP BY order_id) i ON i.order_id = o.id
                              WHERE
                                 o.active_status = 'O' ".$cond."
         ) K	) P
         ORDER BY
         update_dtm DESC,
         create_dtm DESC";
         */
      $sql = "WITH temp_order AS (
         SELECT	*
            ,CASE				
                  WHEN order_price < sum_price && order_price > 0 THEN	order_price - payment_amount_web + sum_price 
                  ELSE payment_amount_web 
               END payment_amount 
         FROM	(
         SELECT
            o.*,
            IFNULL( ROUND((( i.order_price_rmb * o.old_rate ) + o.order_pay_ship ) - o.order_discount, 2 ), o.order_price ) AS sum_price,
            IFNULL( p.payment_amount, 0 ) AS payment_amount_web 
         FROM web_order o
         LEFT JOIN ( 
                                    SELECT SUM( payment_amount ) payment_amount, order_id, member_id 
                                    FROM web_payment 
                                    WHERE STATUS = 'A' AND order_type = '1' GROUP BY order_id,member_id 
                                 ) p ON p.order_id = o.id
         LEFT JOIN (
                                 SELECT	order_id,ROUND( SUM( product_price_rmb * product_qty ), 2 ) AS order_price_rmb 
                                 FROM	web_order_item 
                                 WHERE	STATUS = 'A' AND item_typs_s = 'NR' 
                                 GROUP BY	order_id 
                                 ) i ON i.order_id = o.id 
         WHERE	o.create_dtm > DATE_ADD(NOW(),INTERVAL -1 YEAR ) AND o.active_status = 'O' 
         ) K 
)
         
SELECT DISTINCT
*,(	sum_price - payment_amount ) AS over_payamount,(sum_price - payment_amount_web ) AS overdue
,CASE WHEN payment_amount_web > 0 THEN
CASE WHEN ( sum_price - payment_amount ) > 0 THEN	( sum_price - order_price ) ELSE ( sum_price - order_price ) END 
WHEN STATUS = '7' THEN sum_price ELSE 0.00 
END balance
,IF((SELECT	COUNT(*) FROM project_withdraw WHERE STATUS = 'A' AND user_id = P.member_id AND order_id = P.id ) > 0,'Y','N' ) AS refund_status
,CASE	
WHEN abs((sum_price - payment_amount_web)) > 0 THEN 
abs((sum_price - payment_amount_web )) - COALESCE(( SELECT payment_amount FROM web_payment WHERE member_id = P.member_id 	AND order_id  = P.id  AND status = 'A' ORDER BY id DESC LIMIT 1 ),0) ELSE 0 
END AS overdue_paid 
FROM	(						
         SELECT * FROM temp_order
) P 
ORDER BY
update_dtm DESC,
create_dtm DESC";
//echo $sql;    
      $sqlCountOrder = "SELECT * FROM web_order WHERE	create_dtm > DATE_ADD(NOW(),INTERVAL -1 YEAR ) AND active_status = 'O' ";
		$res = $db->Execute($sqlCountOrder);
		$count = $res->NumRows();
	   $base->set('allPage',ceil($count/20));

		$page = $base->get('GET.page');

		$numr = 20;
		$start = "";
		$end = "";
		if($page==1||empty($page)){
			$start = 0;
			$page = 1;
			//$end = $numr;
		}else{
			//$end = ($page*$numr);
			$start = ($page*$numr)-$numr;
		}
		 $condNum = " LIMIT ".$start.",".$numr;

		 $base->set('currpage',$page);
      
		$res = $db->Execute($sql.$condNum);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['order_id'] = $res->fields['order_id'];
			$ArrList[$i]['order_number'] = $res->fields['order_number'];
			$ArrList[$i]['status'] = $res->fields['status'];
           // $ArrList[$i]['order_buy_rmb'] = $res->fields['order_buy_rmb']; //hatairat 23-10-2564
			$ArrList[$i]['order_price'] = $res->fields['order_price'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			$ArrList[$i]['ship_by'] = $res->fields['ship_by'];
			$ArrList[$i]['rate'] = $res->fields['rate'];
         $ArrList[$i]['old_rate'] = $res->fields['old_rate'];
			$ArrList[$i]['order_pay_ship'] = $res->fields['order_pay_ship'];
         $ArrList[$i]['order_pay_ship_yuan'] = $res->fields['order_pay_ship_yuan'];
         $ArrList[$i]['order_pay_cn'] = $res->fields['order_pay_cn'];
			$ArrList[$i]['order_discount'] = $res->fields['order_discount'];
         
         if($ArrList[$i]['status'] == '7'){
            $ArrList[$i]['payment'] = $res->fields['payment_amount_web'] + $res->fields['over_payamount'];
            $ArrList[$i]['payment_balance'] = abs($res->fields['balance']); //Weerasak 17-11-2564			   
         }
         
         $ArrList[$i]['sum_price'] = $res->fields['sum_price'];
         if($res->fields['export']=='S')
            $ArrList[$i]['status_text'] = $this->_convStatus('order','5');
         else
			   $ArrList[$i]['status_text'] = $this->_convStatus('order',$res->fields['status']);
			
         $base->set('user_id',$res->fields['member_id']);
			$ArrList[$i]['member'] = $member->memberInfomationByID();
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
         $ArrList[$i]['export'] = $res->fields['export'];
         $ArrList[$i]['buy_id'] = $res->fields['buy_id'];
         $ArrList[$i]['refund_wallet'] = $res->fields['refund_wallet'];
			$ArrList[$i]['items'] = $this->_countItem($res->fields['id']);
			$ArrList[$i]['countprice'] = $this->_countItemPrice($res->fields['id']);
         $ArrList[$i]['refund_status'] = $res->fields['refund_status'];
         $ArrList[$i]['overdue_paid'] = $res->fields['overdue_paid'];

			$i++;
			$res->MoveNext();
		}

		//print_r();
		$dtatReturn['data'] = $ArrList;

		return $dtatReturn;
	}

    private function _ordernoTrackList(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

        $user_id = $memberInfomation['id'];
        $cond = '';

        $agency =  $memberInfomation['agency'];
        if($agency=='A'){
        $list_ref = GF::refgroup();
        if(count($list_ref)>0){
            //GF::print_r($list_ref);
            foreach ($list_ref as $key => $value) {
                $list_ref[$key] = GF::quote(GF::user_code($value));
            }
            $ref_str = implode(',', $list_ref);
            $cond .= " AND web_order.member_id IN(".$ref_str.") ";
        }else{
            $cond .= " AND web_order.member_id ='X' ";
        }
        }else{
        //$cond = '1=1';
        }

		if($_SESSION['odrno_search']['order_number']!=''){

			$cond .= " AND web_order.order_number LIKE '%".$_SESSION['odrno_search']['order_number']."%' ";
        }
        
        
        if($_SESSION['odrno_search']['buy_id']!=''){
           $cond .= " AND project_users.user_code=".GF::quote($_SESSION['odrno_search']['buy_id']);
        }
 if($_SESSION['odrno_search']['user_code']!=''){
         $user_id = GF::user_id($_SESSION['odrno_search']['user_code']);
			$cond .= " AND web_order.member_id =".GF::quote($user_id)." ";
		}

		if($_SESSION['odrno_search']['create_dtm']!=''){
			$date_check = date('Y-m-d',strtotime($_SESSION['odrno_search']['create_dtm']));
			$cond .= " AND web_order.create_dtm >=".GF::quote($date_check);
		}
		$dtatReturn=array();
        $scdate = date('Y-m-d H:i:s',strtotime("-5 days"));;

      // Edit By Weerasak 20-01-2566
      // เอายอดสั่งซื้อที่เป็น 0 ออก
      // เพิ่มเงื่อนไข AND product_buy_qty > 0
        $SUPPERSQL = "SELECT web_order_item.order_id,web_order_item.saller_number,
                                web_order_item.notrack_send_dtm,web_order_item.notrack_num,
                                web_order_item.notrack_comment,web_order.* ,
                                project_user.user_code AS user_code,
                                project_user.user_name AS user_name,
                                project_users.user_code AS user_codes,
                                project_users.user_name AS user_names
                                FROM web_order_item 
                                LEFT JOIN web_order ON web_order_item.order_id=web_order.id 
                                LEFT JOIN project_user ON project_user.id=web_order.member_id 
                                LEFT JOIN project_user AS project_users ON project_users.id=web_order.buy_id 
                                WHERE web_order_item.product_sku_1='' AND web_order_item.item_typs_s='NR' AND web_order.status='5' AND web_order.export='S' AND web_order.send_order_status='Y' AND web_order.update_dtm<='".$scdate."' ".$cond." 
                                AND product_buy_qty > 0
                                GROUP BY web_order_item.saller_number,web_order_item.order_id 
                                ORDER BY web_order_item.order_id DESC";

		//echo $type;
		$sql = "SELECT * FROM web_order WHERE  active_status='O' AND status='3' AND id IN($cond_id) ORDER BY update_dtm DESC,create_dtm DESC";
		//echo $sql;

		$res = $db->Execute($SUPPERSQL);
		$count = $res->NumRows();

			$base->set('allPage',ceil($count/20));

		$page = $base->get('GET.page');

		$numr = 20;
		$start = "";
		$end = "";
		if($page==1||empty($page)){
			$start = 0;
			$page = 1;
			//$end = $numr;
		}else{
			//$end = ($page*$numr);
			$start = ($page*$numr)-$numr;
		}
		 $condNum = " LIMIT ".$start.",".$numr;

		 $base->set('currpage',$page);

		$res = $db->Execute($SUPPERSQL);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['order_id'] = $res->fields['order_id'];
			$ArrList[$i]['user_code'] = $res->fields['user_code'];
			$ArrList[$i]['user_codes'] = $res->fields['user_codes'];
			$ArrList[$i]['user_name'] = $res->fields['user_name'];
			$ArrList[$i]['user_names'] = $res->fields['user_names'];
			$ArrList[$i]['order_number'] = $res->fields['order_number'];
			$ArrList[$i]['status'] = $res->fields['status'];
            $ArrList[$i]['saller_number'] = $res->fields['saller_number'];
            $ArrList[$i]['notrack_send_dtm'] = $res->fields['notrack_send_dtm'];
            $ArrList[$i]['notrack_num'] = $res->fields['notrack_num'];
            $ArrList[$i]['notrack_comment'] = $res->fields['notrack_comment'];
			$ArrList[$i]['status_text'] = $this->_convStatus('order',$res->fields['status']);
			$base->set('user_id',$res->fields['member_id']);
			$ArrList[$i]['member'] = $member->memberInfomationByID();
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
			$i++;
			$res->MoveNext();
		}

		//print_r();
		$dtatReturn['data'] = $ArrList;

		return $dtatReturn;
    }
    private function _ordernoTrackListSend(){
        
                $base = Base::getInstance();
                $db = DB::getInstance();
        
                $member = new Member();
        
        
                $memberInfomation = $member->memberInfomation();
    
                $user_id = $memberInfomation['id'];

                $cond = '';
                
                $agency =  $memberInfomation['agency'];
                if($agency=='A'){
                $list_ref = GF::refgroup();
                if(count($list_ref)>0){
                    //GF::print_r($list_ref);
                    foreach ($list_ref as $key => $value) {
                        $list_ref[$key] = GF::quote(GF::user_code($value));
                    }
                    $ref_str = implode(',', $list_ref);
                    $cond .= " AND web_order.member_id IN(".$ref_str.") ";
                }else{
                    $cond .= " AND web_order.member_id ='X' ";
                }
                }else{
                //$cond = '1=1';
                }
        
		        if($_SESSION['odrnose_search']['order_number']!=''){

					$cond .= " AND web_order.order_number LIKE '%".$_SESSION['odrnose_search']['order_number']."%' ";
		        }
		        
		        
		        if($_SESSION['odrnose_search']['buy_id']!=''){
		           $cond .= " AND project_users.user_code=".GF::quote($_SESSION['odrnose_search']['buy_id']);
		        }
		 if($_SESSION['odrnose_search']['user_code']!=''){
		         $user_id = GF::user_id($_SESSION['odrnose_search']['user_code']);
					$cond .= " AND web_order.member_id =".GF::quote($user_id)." ";
				}

				if($_SESSION['odrnose_search']['create_dtm']!=''){
					$date_check = date('Y-m-d',strtotime($_SESSION['odrnose_search']['create_dtm']));
					$cond .= " AND web_order.create_dtm >=".GF::quote($date_check);
				}
                $dtatReturn=array();
        
                $scdate = date('Y-m-d H:i:s',strtotime("-5 days"));;
        
                $SUPPERSQL = "SELECT web_order_item.order_id,web_order_item.saller_number,
                                    web_order_item.notrack_send_dtm,web_order_item.notrack_num,
                                    web_order_item.notrack_comment,
                                    web_order.* ,
	                                project_user.user_code AS user_code,
	                                project_user.user_name AS user_name,
	                                project_users.user_code AS user_codes,
	                                project_users.user_name AS user_names
                                    FROM web_order_item 
                                    LEFT JOIN web_order ON web_order_item.order_id=web_order.id 
                                    
                                    LEFT JOIN project_user ON project_user.id=web_order.member_id 
                                	LEFT JOIN project_user AS project_users ON project_users.id=web_order.buy_id
                                    WHERE web_order_item.product_sku_1='-' AND web_order_item.item_typs_s='NR' AND web_order.status='3' AND web_order.export='S' AND web_order.send_order_status='Y' AND web_order.update_dtm<='".$scdate."' ".$cond." 
                                    GROUP BY web_order_item.saller_number,web_order_item.order_id 
                                    ORDER BY web_order_item.order_id DESC";
                //$resSUPPERSQL = $db->Execute($SUPPERSQL);
                // $iss = array();
                // while(!$resSUPPERSQL->EOF){
                // 		$iss[] = $resSUPPERSQL->fields['order_id'];
                // 		$resSUPPERSQL->MoveNext();
                // }
                // $cond_id = '';
                // if(count($iss)>0){
                //     $cond_id = implode(",",$iss);
                // }else{
                //     return array();
                //     exit();
                // }
                //echo $type;
                $sql = "SELECT * FROM web_order WHERE  active_status='O' AND status='3' AND id IN($cond_id) ORDER BY update_dtm DESC,create_dtm DESC";
                //echo $sql;
        
                $res = $db->Execute($SUPPERSQL);
                $count = $res->NumRows();
        
                    $base->set('allPage',ceil($count/20));
        
                $page = $base->get('GET.page');
        
                $numr = 20;
                $start = "";
                $end = "";
                if($page==1||empty($page)){
                    $start = 0;
                    $page = 1;
                    //$end = $numr;
                }else{
                    //$end = ($page*$numr);
                    $start = ($page*$numr)-$numr;
                }
                 $condNum = " LIMIT ".$start.",".$numr;
        
                 $base->set('currpage',$page);
        
                $res = $db->Execute($SUPPERSQL);
                $i=0;
                $ArrList = array();
                while(!$res->EOF){
                    $ArrList[$i]['id'] = $res->fields['id'];
                    $ArrList[$i]['order_id'] = $res->fields['order_id'];
                    $ArrList[$i]['order_number'] = $res->fields['order_number'];
                    $ArrList[$i]['status'] = $res->fields['status'];
                    $ArrList[$i]['user_code'] = $res->fields['user_code'];
					$ArrList[$i]['user_codes'] = $res->fields['user_codes'];
					$ArrList[$i]['user_name'] = $res->fields['user_name'];
					$ArrList[$i]['user_names'] = $res->fields['user_names'];
                    $ArrList[$i]['saller_number'] = $res->fields['saller_number'];
                    $ArrList[$i]['notrack_send_dtm'] = $res->fields['notrack_send_dtm'];
                    $ArrList[$i]['notrack_num'] = $res->fields['notrack_num'];
                    $ArrList[$i]['notrack_comment'] = $res->fields['notrack_comment'];
                    $ArrList[$i]['status_text'] = $this->_convStatus('order',$res->fields['status']);
                    
                    $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
                    $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
                    $i++;
                    $res->MoveNext();
                }
        
                //print_r();
                $dtatReturn['data'] = $ArrList;
        
                return $dtatReturn;
            }
            private function _orderListBuyUser(){
                
                        $base = Base::getInstance();
                        $db = DB::getInstance();
                
                        $member = new Member();
                
                
                         $memberInfomation = $member->memberInfomation();
                
                         $user_id = $memberInfomation['id'];

                         $cond = '';
                         
                         
                        if($_SESSION['odrbuy_user_search']['status']!=''){
                
                            if($_SESSION['odrbuy_user_search']['status']=='ALL'){
                                //$cond = " AND status=".GF::quote($type);
                            }else{
                                $cond .= " AND buy_status=".GF::quote($_SESSION['odrbuy_user_search']['status']);
                            }
                        }
                        if($_SESSION['odrbuy_user_search']['order_number']!=''){
                
                            $cond .= " AND order_number LIKE '%".$_SESSION['odrbuy_user_search']['order_number']."%' ";
                        }
                
                        if($_SESSION['odrbuy_user_search']['user_code']!=''){
                        $user_id = GF::user_id($_SESSION['odrbuy_user_search']['user_code']);
                            $cond .= " AND member_id =".GF::quote($user_id)." ";
                        }
                
                        
                
                        //echo $type;
                        $sql = "SELECT * FROM web_order WHERE  send_order_status='Y' AND active_status='O'  AND (buy_status='N' OR buy_status='Y') $cond ORDER BY update_dtm DESC,create_dtm DESC";
                        /*$sql = "SELECT * FROM web_order WHERE  send_order_status='Y' AND active_status='O' AND export='Y' AND (buy_status='N' OR buy_status='Y') $cond ORDER BY update_dtm DESC,create_dtm DESC";*/
                        //echo $sql;
                        $res = $db->Execute($sql);
                        //Weerasak 16-11-2564 แบ่งหน้า
                        $count = $res->NumRows();
                        $base->set('allPage',ceil($count/20));
                        $page = $base->get('GET.page');
                        $numr = 20;
                        $start = "";
                        $end = "";
                        
                        if($page==1||empty($page)){
                           $start = 0;
                           $page = 1;                          
                        }else{
                           $start = ($page*$numr)-$numr;
                        }
                         $condNum = " LIMIT ".$start.",".$numr;
                  
                         $base->set('currpage',$page);
                  
                        $res = $db->Execute($sql.$condNum);
                        //END
                        $i=0;
                        $ArrList = array();
                        while(!$res->EOF){
                            $ArrList[$i]['id'] = $res->fields['id'];
                            $ArrList[$i]['order_id'] = $res->fields['order_id'];
                            $ArrList[$i]['order_number'] = $res->fields['order_number'];
                            $ArrList[$i]['status'] = $res->fields['status'];
                            $ArrList[$i]['order_price'] = $res->fields['order_price'];
                            $ArrList[$i]['order_comment'] = $res->fields['order_comment'];
                            $ArrList[$i]['ship_by'] = $res->fields['ship_by'];
                            $ArrList[$i]['rate'] = $res->fields['rate'];
                            $ArrList[$i]['order_pay_ship'] = $res->fields['order_pay_ship'];
                            $ArrList[$i]['order_pay_cn'] = $res->fields['order_pay_cn'];
                            $ArrList[$i]['order_discount'] = $res->fields['order_discount'];
                            $ArrList[$i]['status_text'] = $this->_convStatus('order',$res->fields['status']);
                            $base->set('user_id',$res->fields['member_id']);
                            $ArrList[$i]['member'] = $member->memberInfomationByID();
                            $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
                            $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
                            $ArrList[$i]['export'] = $res->fields['export'];
                            $ArrList[$i]['send_order_status'] = $res->fields['send_order_status'];
                            $ArrList[$i]['send_order_dtm'] = $res->fields['send_order_dtm'];
                            $ArrList[$i]['send_order_round'] = $res->fields['send_order_round'];
                            $ArrList[$i]['items'] = $this->_countItem($res->fields['id']);
                            $ArrList[$i]['countprice'] = $this->_countItemPrice($res->fields['id']);
                            $ArrList[$i]['buy_id'] = $res->fields['buy_id'];
                            $ArrList[$i]['buy_status'] = $res->fields['buy_status'];
                            $i++;
                            $res->MoveNext();
                        }
                
                        //print_r();
                        $dtatReturn['data'] = $ArrList;
                
                        return $dtatReturn;
                    }
   private function _orderListBuy(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$dtatReturn=array();

		/*$type = $base->get('GET.type');
		$type2 = $base->get('type');
		if($type==''){
			$type = 'ALL';
		}*/

		/*if(!empty($_POST)){
			if(!empty($_POST['s_ordernumber'])){
				$_SESSION['s_ordernumber'] = $_POST['s_ordernumber'];
			}
			if(!empty($_POST['s_status'])){
				$_SESSION['s_status'] = $_POST['s_status'];
			}
			if(!empty($_POST['s_start'])){
				$_SESSION['s_start'] = $_POST['s_start'];
			}
			if(!empty($_POST['s_end'])){
				$_SESSION['s_end'] = $_POST['s_end'];
			}
		}*/



		$cond = '';


		if($_SESSION['odrbuy_search']['status']!=''){

			if($_SESSION['odrbuy_search']['status']=='ALL'){
				//$cond = " AND status=".GF::quote($type);
			}else{
				$cond .= " AND export=".GF::quote($_SESSION['odrbuy_search']['status']);
			}
		}
      if($_SESSION['odrbuy_search']['status_send']!=''){

			if($_SESSION['odrbuy_search']['status_send']=='ALL'){
				//$cond = " AND status=".GF::quote($type);
			}else{
				$cond .= " AND send_order_status=".GF::quote($_SESSION['odrbuy_search']['status_send']);
			}
		}
		if($_SESSION['odrbuy_search']['order_number']!=''){

			$cond .= " AND order_number LIKE '%".$_SESSION['odrbuy_search']['order_number']."%' ";
		}

      if($_SESSION['odrbuy_search']['user_code']!=''){
         $user_id = GF::user_id($_SESSION['odrbuy_search']['user_code']);
			$cond .= " AND member_id =".GF::quote($user_id)." ";
		}

		if($_SESSION['odrbuy_search']['create_dtm']!=''){
			$date_check = date('Y-m-d',strtotime($_SESSION['odrbuy_search']['create_dtm']));
			$cond .= " AND export_date >=".GF::quote($date_check);
		}

      $agency =  $memberInfomation['agency'];
      if($agency=='A'){
         $list_ref = GF::refgroup();
         if(count($list_ref)>0){
            //GF::print_r($list_ref);
            $ref_str = implode(',', $list_ref);
            $cond .= " AND member_id IN(".$ref_str.") ";
         }else{
            $cond .= " AND member_id ='X' ";
         }
      }

      $cds = '';
      if($base->get('type_b')=='buy'){
        $cds = "AND buy_status='Y' AND buy_id=".intval($memberInfomation['id']);
      }
		//echo $type;
		$sql = "SELECT * FROM web_order WHERE status<>'C' AND active_status='O' ".$cond." AND export!='N' ".$cds." ORDER BY update_dtm DESC,create_dtm DESC";
		//echo $sql;

		$res = $db->Execute($sql);
		$count = $res->NumRows();

			$base->set('allPage',ceil($count/20));

		$page = $base->get('GET.page');

		$numr = 20;
		$start = "";
		$end = "";
		if($page==1||empty($page)){
			$start = 0;
			$page = 1;
			//$end = $numr;
		}else{
			//$end = ($page*$numr);
			$start = ($page*$numr)-$numr;
		}
		 $condNum = " LIMIT ".$start.",".$numr;

		 $base->set('currpage',$page);

		$res = $db->Execute($sql.$condNum);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['order_id'] = $res->fields['order_id'];
			$ArrList[$i]['order_number'] = $res->fields['order_number'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['order_price'] = $res->fields['order_price'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			$ArrList[$i]['ship_by'] = $res->fields['ship_by'];
			$ArrList[$i]['rate'] = $res->fields['rate'];
			$ArrList[$i]['order_pay_ship'] = $res->fields['order_pay_ship'];
            $ArrList[$i]['order_pay_cn'] = $res->fields['order_pay_cn'];
			$ArrList[$i]['order_discount'] = $res->fields['order_discount'];
			$ArrList[$i]['status_text'] = $this->_convStatus('order',$res->fields['status']);
			$base->set('user_id',$res->fields['member_id']);
			$ArrList[$i]['member'] = $member->memberInfomationByID();
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
            $ArrList[$i]['export'] = $res->fields['export'];
            $ArrList[$i]['send_order_status'] = $res->fields['send_order_status'];
            $ArrList[$i]['send_order_dtm'] = $res->fields['send_order_dtm'];
            $ArrList[$i]['send_order_round'] = $res->fields['send_order_round'];
			$ArrList[$i]['items'] = $this->_countItem($res->fields['id']);
			$ArrList[$i]['countprice'] = $this->_countItemPrice($res->fields['id']);
			$i++;
			$res->MoveNext();
		}

		//print_r();
		$dtatReturn['data'] = $ArrList;

		return $dtatReturn;
	}
   private function _orderListBuyView(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$dtatReturn=array();

      $idin = implode(',', $_SESSION['order_chinarorder']);//$base->get('POST.order_id_id')

		$sql = "SELECT * FROM web_order WHERE  id IN($idin) ORDER BY update_dtm DESC,create_dtm DESC";
		//echo $sql;

		$res = $db->Execute($sql);

		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['order_id'] = $res->fields['order_id'];
			$ArrList[$i]['order_number'] = $res->fields['order_number'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['order_price'] = $res->fields['order_price'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			$ArrList[$i]['ship_by'] = $res->fields['ship_by'];
			$ArrList[$i]['rate'] = $res->fields['rate'];
         $ArrList[$i]['old_rate'] = $res->fields['old_rate'];
			$ArrList[$i]['order_pay_ship'] = $res->fields['order_pay_ship'];
         $ArrList[$i]['order_pay_cn'] = $res->fields['order_pay_cn'];
			$ArrList[$i]['order_discount'] = $res->fields['order_discount'];
			$ArrList[$i]['status_text'] = $this->_convStatus('order',$res->fields['status']);
			$base->set('user_id',$res->fields['member_id']);
			$ArrList[$i]['member'] = $member->memberInfomationByID();
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
         $ArrList[$i]['export'] = $res->fields['export'];
         $ArrList[$i]['send_order_status'] = $res->fields['send_order_status'];
         $ArrList[$i]['send_order_dtm'] = $res->fields['send_order_dtm'];
         $ArrList[$i]['send_order_round'] = $res->fields['send_order_round'];
			$ArrList[$i]['items'] = $this->_countItem($res->fields['id']);
			$ArrList[$i]['countprice'] = $this->_countItemPrice($res->fields['id']);
			$i++;
			$res->MoveNext();
		}

		//print_r();
		$dtatReturn['data'] = $ArrList;

		return $dtatReturn;
    }
    
	public function _countItem($order_id){
			$base = Base::getInstance();
			$db = DB::getInstance();
			$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D'";
			//echo $sql;
			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$i++;
				$res->MoveNext();
			}
			return $i;
		}
		public function _countItemPrice($order_id){
			$base = Base::getInstance();
			$db = DB::getInstance();
         $db2 =  DB2::getInstance();

			$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D'";
			//echo $sql;
			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			$product_price_rmb = 0;
			$product_price_thb = 0;
			$product_buy_rmb = 0;
			$ship_cn_cn = 0;
			$ship_cn_th = 0;
			$ship_all = 0;
			$refund = 0;
			$unfund = 0;
         $buy_qty = 0;;
         $product_qty = 0;
			while(!$res->EOF){



				if($res->fields['item_typs_s']=='NR'){

               if($res->fields['product_qty']=='0'){
                  $res->fields['product_price_rmb'] = 0;
               }
               if($res->fields['product_buy_qty']=='0'){
                  $res->fields['product_buy_rmb'] = 0;
               }
					$product_price_rmb = $product_price_rmb+($res->fields['product_price_rmb']*$res->fields['product_qty']);
					$product_buy_rmb = $product_buy_rmb+($res->fields['product_buy_rmb']*$res->fields['product_buy_qty']);
               //echo $res->fields['order_id']."-".$product_buy_rmb."<br />";
               $product_qty += $res->fields['product_qty'];
               $buy_qty += $res->fields['product_buy_qty'];
				}

				if($res->fields['item_typs_s']=='DE'){
					$product_price_thb = $product_price_thb-($res->fields['product_price_thb']);
				}
				if($res->fields['item_typs_s']=='IN'){
					$product_price_thb = $product_price_thb+($res->fields['product_price_thb']);
				}



				$ship_cn_cn = $product_price_rmb+$res->fields['ship_cn_cn'];
				$ship_cn_th = $product_price_rmb+$res->fields['ship_cn_th'];
				$ship_all = $product_price_rmb+$res->fields['ship_all'];
				$refund = $product_price_rmb+$res->fields['refund'];
				$unfund = $product_price_rmb+$res->fields['unfund']; //ปิดไว้ก่อน
				$i++;
				$res->MoveNext();
			}

         $arrin = array();

         $sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D' GROUP BY saller_number";
			$res = $db->Execute($sql);
         $xc=0;
         while(!$res->EOF){
            $arrin[$xc] = $res->fields['saller_number'];
            $xc++;
            $res->MoveNext();
			}

         $ship_cn = 0;
         //Edit By Weerasak 17-10-2564
         $saller_amount = 0;
         $saller_discount = 0;
         $saller_shipping = 0;
         $total_order_buy_amount = 0; //ยอดซื้อจริง (หยวน)
         
        //End edit
         if(count($arrin)>0){
            $addIn = implode(',', $arrin);
            $sql = "SELECT * FROM web_order_item_ship WHERE order_id=".GF::quote($order_id)." AND saller_id IN(".$addIn.")";//Edit by Weerasak เปลี่ยน saller_id > saller_number
   			$res = $db->Execute($sql);
            
            while(!$res->EOF){//พี่เอส เอา ! ออก เพราะว่า กรณีไม่มีค่า ก็ปล่อยให้มันรันไป
                
               $ship_cn += $res->fields['price_rmb'];
               //Edit By Weerasak 17-10-2564               
               $saller_amount += $res->fields['saller_amount_rmb'];
               $saller_discount += $res->fields['saller_discount_rmb'];
               $saller_shipping += $res->fields['saller_shipping_rmb'];
               //End edit
               $res->MoveNext();
   			}
               //Edit By Weerasak 19-10-2564       
                if($saller_shipping > 0)
                     $saller_shipping = $ship_cn;
         }

         if($saller_amount == 0)
         {
             $total_order_buy_amount = $product_buy_rmb + $ship_cn;
         }else{
             $total_order_buy_amount = ($saller_amount + $saller_shipping) - $saller_discount;
         }//End edit

			$ArrList['product_price_rmb'] = $product_price_rmb;
			$ArrList['product_price_thb'] = $product_price_thb;
			$ArrList['product_buy_rmb'] = $product_buy_rmb;
            $ArrList['product_buy_qty'] = $buy_qty;
            $ArrList['product_qty'] = $product_qty;
            $ArrList['ship_cn'] = $ship_cn;
			$ArrList['ship_cn_cn'] = $ship_cn_cn;
			$ArrList['ship_cn_th'] = $ship_cn_th;
			$ArrList['ship_all'] = $ship_all;
			$ArrList['refund'] = $refund;
			$ArrList['unfund'] = $unfund;
			$ArrList['all'] = ($product_price_thb+$ship_cn_cn+$ship_cn_th+$ship_all+$refund)-$unfund;
			$ArrList['all'] = number_format($ArrList['all'],2);
            $ArrList['total_order_buy_amount'] = $total_order_buy_amount; //Edit By Weerasak 17-10-2564
			//GF::print_r($ArrList);
			return $ArrList;
		}
	private function _itemList(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$item_id = $base->get('_ids');

		$cond = '';
		if($_SESSION['t_ordernumber']!='' && $sc!=NULL){
			$cond .= " AND product_sku_1 LIKE '%".$_SESSION['t_ordernumber']."%' ";
		}

		$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($item_id)." AND status!='D' ".$cond." ORDER BY product_saller_name ASC";
		//echo $sql;
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['product_link'] = $res->fields['product_link'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['product_title'] = $res->fields['product_title'];
			$ArrList[$i]['product_sku_1'] = $res->fields['product_sku_1'];
			$ArrList[$i]['product_image'] = $res->fields['product_image'];
			$ArrList[$i]['product_qty'] = $res->fields['product_qty'];
			$ArrList[$i]['product_saller_name'] = $res->fields['product_saller_name'];
            $ArrList[$i]['saller_number'] = $res->fields['saller_number'];
            $ArrList[$i]['saller_image'] = $res->fields['saller_image'];
			$ArrList[$i]['product_saller_link'] = $res->fields['product_saller_link'];
			$ArrList[$i]['product_price_rmb'] = $res->fields['product_price_rmb'];
			$ArrList[$i]['product_price_thb'] = $res->fields['product_price_thb'];
			$ArrList[$i]['product_buy_rmb'] = $res->fields['product_buy_rmb'];
			$ArrList[$i]['product_buy_qty'] = $res->fields['product_buy_qty'];
			$ArrList[$i]['ship_cn_cn'] = $res->fields['ship_cn_cn'];
			$ArrList[$i]['ship_cn_th'] = $res->fields['ship_cn_th'];
			$ArrList[$i]['ship_all'] = $res->fields['ship_all'];
			$ArrList[$i]['refund'] = $res->fields['refund'];
			$ArrList[$i]['upfund'] = $res->fields['upfund'];
			$ArrList[$i]['note'] = $res->fields['note'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['order_ship_cn'] = $res->fields['order_ship_cn'];
			$ArrList[$i]['order_ship_th'] = $res->fields['order_ship_th'];
			$ArrList[$i]['order_box'] = $res->fields['order_box'];
			$ArrList[$i]['order_type'] = $res->fields['order_type'];
			$ArrList[$i]['ship_by'] = $res->fields['ship_by'];
			$ArrList[$i]['num_box'] = $res->fields['num_box'];
			$ArrList[$i]['item_typs_s'] = $res->fields['item_typs_s'];
			$ArrList[$i]['option'] = $this->_itemOption($res->fields['id']);
			$ArrList[$i]['status_text'] = $this->_convStatus('item',$res->fields['status']);
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$newsdate = date("d-m-Y H:i:s", strtotime($res->fields['create_dtm']));
			if($res->fields['update_dtm']!='0000-00-00 00:00:00'){
				$newsdate = date("d-m-Y H:i:s", strtotime($res->fields['update_dtm']));
			}
			$ArrList[$i]['update_dtm'] = $newsdate;
			$i++;
			$res->MoveNext();
		}
		//print_r($ArrList);
		return $ArrList;
	}
	private function _itemOption($item_id){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "SELECT * FROM web_order_item_option WHERE item_id=".GF::quote($item_id);
		//echo $sql;
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['option_name'] = $res->fields['option_name'];
			$ArrList[$i]['option_value'] = $res->fields['option_value'];
			$ArrList[$i]['option_name_th'] = $res->fields['option_name_th'];
			$ArrList[$i]['option_value_th'] = $res->fields['option_value_th'];
			$i++;
			$res->MoveNext();
		}
		//print_r();
		return $ArrList;
	}
	public function _convStatus($type,$status){
		$text_return = '';
		if($type=='order'){
			/*if($status=='1'){
				$text_return = 'รอตรวจสอบ';
			}
			else if($status=='2'){
				$text_return = 'รอชำระเงิน';
			}
			else if($status=='C'){
				$text_return = 'ยกเลิก';
			}
			else if($status=='3'){
				$text_return = 'สั่งซื้อ';
			}

			else if($status=='4'){
				$text_return = 'แจ้งจัดส่ง';
			}
			else if($status=='5'){
				$text_return = 'เสร็จสิ้น';
			}
         else if($status=='6'){
				$text_return = 'รอตรวจสอบยอด';
			}
         else if($status=='7'){
				$text_return = 'ค้างชำระเงิน';
			}*/
         $text_return = GF::order_status($status);
		}
		else if($type=='item'){
			if($status=='W'){
				$text_return = 'รอตรวจสอบ';
			}
			else if($status=='V'){
				$text_return = 'กำลังตรวจสอบ';
			}
			else if($status=='C'){
				$text_return = 'ยกเลิก';
			}
			else if($status=='A'){
				$text_return = 'ตรวจสอบแล้ว';
			}
		}
		return $text_return;
	}

	private function _getlastorder(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "SELECT * FROM web_order WHERE 1=1 ORDER BY id DESC LIMIT 1";
		$res = $db->Execute($sql);

		return $res->fields['order_id'];
	}
	public function _orderInfo(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();

		$orderid = $base->get('_ids');

		$sql = "SELECT * FROM web_order WHERE id=".GF::quote($orderid);
		$res = $db->Execute($sql);
		if($res->fields['order_ship_th']=='A100'){
			$res->fields['order_ship_th']='มารับด้วยตนเอง';
		}else{
			$res->fields['order_ship_th'] = $this->_getShipTH($res->fields['order_ship_th']);
		}
		$res->fields['countprice'] = $this->_countItemPrice($orderid);
		$res->fields['status_text'] = $this->_convStatus('order',$res->fields['status']);
		$base->set('user_id',$res->fields['member_id']);
		$res->fields['member'] = $member->memberInfomationByID();
       // $res->fields['order_buy_saller'] = $this->_orderBySaller($orderid);
		return  $res->fields;
	}
    public function _orderBySaller() //Weerasak 12-03-2566
    {
        $base = Base::getInstance();
        $db = DB::getInstance();
        $orer_id = $base->get('_ids');
        $saller_number = 0;
        $product_buy_rmb = 0;
        $product_buy_qty = 0;
        $i=0;
        //$sql = "SELECT saller_number,SUM(product_buy_rmb * product_buy_qty) as amount FROM web_order_item WHERE order_id=".GF::quote($orer_id)." AND status!='D' GROUP BY saller_number ORDER BY saller_number";
        $sql = "SELECT oi.saller_number,oi.product_saller_name,oi.product_saller_link,oi.product_sku_1
        ,(SELECT product_image FROM web_order_item WHERE order_id = oi.order_id AND saller_number = oi.saller_number ORDER BY id DESC LIMIT 1) saller_image
        ,SUM(product_buy_rmb * product_buy_qty) as amount 
        FROM web_order_item oi
        WHERE order_id=".GF::quote($orer_id)."
        AND oi.status!='D' 
        AND oi.item_typs_s = 'NR'
        GROUP BY oi.saller_number,oi.product_saller_name,oi.product_saller_link,oi.product_sku_1
        ORDER BY oi.saller_number";
        $res = $db->Execute($sql);
        //echo $sql;
        while(!$res->EOF){
            $ArrList[$i]['saller_number'] = $res->fields['saller_number'];
            $ArrList[$i]['saller_name'] = $res->fields['product_saller_name'];
            $ArrList[$i]['saller_link'] = $res->fields['product_saller_link'];
            $ArrList[$i]['saller_image'] = $res->fields['saller_image'];
            $ArrList[$i]['amount'] = $res->fields['amount'];
            $i++;
            $res->MoveNext();
        }
        return $ArrList;
    }
	public function _getShipTH($scode){
		$db = DB::getInstance();
		$sql = "SELECT * FROM web_ship_th WHERE code=".GF::quote($scode);
		$res = $db->Execute($sql);

		$arrReturn = array();
		$arrReturnp['code'] = $res->fields['code'];
		$arrReturn['name']=$res->fields['name'];
		return  $arrReturn;

	}

	public function _getRate(){
		$db = DB::getInstance();
		$sql = "SELECT * FROM web_rate WHERE status='O' LIMIT 1";
		$res = $db->Execute($sql);

		/*$arrReturn = array();
		$arrReturnp['code'] = $res->fields['code'];*/
		return  $res->fields['rate'];

	}
	private function _delItem(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order_item SET status='D' WHERE id=".GF::quote($base->get('GET.item'));
		$res = $db->Execute($sql);
	}
	private function _reOrder(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$order_id = $base->get('GET.order_id');

		$sql = "SELECT * FROM web_order WHERE id=".GF::quote($order_id);
		$res = $db->Execute($sql);

		$base->set('_ids',$order_id);
		$item_list = $this->_itemList();

		$my_id = $order_id;
		//$order_id = (1000+$_SESSION['user_id_front']).date('dmY');
		$lasrorder = $this->_getlastorder();

		$year = substr($lasrorder,0,4);
		$month = substr($lasrorder,4,2);
		$times = substr($lasrorder,6,4);

		$c_year = date('Y');
		$c_month = date('m');

		$new_time = '';
		$order_id = '';
		if($year==$c_year && $month==$c_month){
			$new_time = intval($times)+1;
			$new_time = str_pad($new_time, 2, "0", STR_PAD_LEFT);
			$order_id = $year.$month.$new_time;
		}else{
			$order_id = $c_year.$c_month."01";
		}

		$sql = "INSERT INTO web_order (
							order_id,
							member_id,
							order_payment,
							order_ship_cn,
							order_box	,
							order_ship_th,
							order_comment,
							rate,
							status,
							active_status,
							create_dtm,
							update_dtm
						) VALUES (
							".GF::quote($order_id).",
							".GF::quote($user_id).",
							".GF::quote($res->fields['order_payment']).",
							".GF::quote($res->fields['order_ship_cn']).",
							".GF::quote($res->fields['order_box']).",
							".GF::quote($res->fields['order_ship_th']).",
							".GF::quote($res->fields['order_comment']).",
							".GF::quote($res->fields['rate']).",
							'W',
							'O',
							NOW(),
							NOW()
						)";
		 $res = $db->Execute($sql);
		 $last_id = $db->Insert_ID();
		 $my_id = $last_id;

		  if($res){
		 	foreach($item_list as $key=>$vals){

		 		$img_name = GF::randomNum(25);
				$file = $base->get('BASEDIR')."/uploads/product/".$vals['product_image'];
				$data = file_get_contents($file);
				$new = $base->get('BASEDIR').'/uploads/product/'.$img_name.".jpg";
				file_put_contents($new, $data);


				$sql = "INSERT INTO web_order_item (
									order_id,
									product_link,
									product_title,
									product_image,
									product_saller_name,
									product_saller_link,
									product_qty,
									product_price_rmb,
									product_price_thb,
									note,
									status,
									create_dtm
								) VALUES (
									".GF::quote($last_id).",
									".GF::quote($vals['product_link']).",
									".GF::quote($vals['product_title']).",
									".GF::quote($img_name.'.jpg').",
									".GF::quote($vals['product_saller_name']).",
									".GF::quote($vals['product_saller_link']).",
									".GF::quote($vals['product_qty']).",
									".GF::quote($vals['product_price_rmb']).",
									".GF::quote($vals['product_price_thb']).",
									".GF::quote($vals['note']).",
									'W',
									NOW()
								)";
				 $res = $db->Execute($sql);
				 $item_id = $db->Insert_ID();
				 foreach($vals['option'] as $k_y=>$val_c){
					 	$sql = "INSERT INTO web_order_item_option (
											item_id,
											option_name,
											option_value,
											option_name_th,
											option_value_th
										) VALUES (
											".GF::quote($item_id).",
											".GF::quote($val_c['option_name']).",
											".GF::quote($val_c['option_value']).",
											".GF::quote($val_c['option_name_th']).",
											".GF::quote($val_c['option_value_th'])."
										)";
						 $res = $db->Execute($sql);
				 }
			}

			/*$member = new Member();

			$userinfo = $member->userInfo();

			$mail_mssg = file_get_contents(BASEURL."/member/order_1/view_mail/".$my_id);
			//echo $mail_mssg.'fffff';
			$array_param['order'] = $order_id;
			$array_param['web_name'] = $userinfo['name'];
			$array_param['web_email'] = $userinfo['email'];
		 	$array_param['mail_body'] = $mail_mssg;//$this->_setMailBody($array_param); //print_r($array_param);
		 	$this->_sendMail($array_param);

		 	$admin = $this->_admininfo();


		 	$array_param['web_name'] = $admin['admin_name'];
			$array_param['web_email'] = $admin['email'];
		 	$array_param['mail_body'] = $mail_mssg;//$this->_setMailBodyAdmin($array_param); //print_r($array_param);
		 	$this->_sendMail($array_param);
			return 'A';*/

			return 'A';
		}
	}
	private function _cancelOrder(){

		$base = Base::getInstance();
		$db = DB::getInstance();
        

		$sql = "UPDATE web_order SET status='C' WHERE id=".GF::quote($base->get('GET.order_id'));
		$res = $db->Execute($sql);

        $mail = Mailer::getInstance();
        $db2 = DB2::getInstance();
        $odrid = $base->get('GET.order_id');
        $orderinfo = $db2->get("web_order","*",array("id"=>$odrid));
        $mail->userid = $orderinfo['member_id'];
        $mail->Subject = "ยกเลิกการสั่งซื้อ";
        $members = GF::memberinfo($orderinfo['member_id']);
        $dataHtml = GF::mailtemplate('9');
        $dataHtml = str_replace("{CUST_NAME}",$members['user_name'],$dataHtml);
        $dataHtml = str_replace("{ORDER_NO}",$orderinfo['order_number'],$dataHtml);
        $dataHtml = str_replace("{ORDER_STATUS}",'ยกเลิกการสั่งซื้อ',$dataHtml);
        $mail->msg = $dataHtml;
        $mail->SendingToNoti();
	}
	private function _orderStatus(){

		$base = Base::getInstance();
		$db = DB::getInstance();
        $db2 = DB2::getInstance();
		$member = new Member();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];
		$sql = "UPDATE web_order SET status=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.order_id'));
        $res = $db->Execute($sql);
        $odrid = $base->get('GET.order_id');

        $orderinfo_v = $db2->get("web_order","*",array("id"=>$odrid));

		if($base->get('GET.value')=='3' && $orderinfo_v['order_number']==''){
			$base->set('orderid',$base->get('GET.order_id'));
			$this->_genOrderNumber();
		}
		if($base->get('GET.value')=='C'){
			$sql2 = "SELECT member_id,order_price FROM web_order WHERE id=".GF::quote($base->get('GET.order_id'));
			$res2 = $db->Execute($sql2);
			$member_id = $res2->fields['member_id'];
			$order_price = $res2->fields['order_price'];
			
			$sql4 = "SELECT wallet FROM project_user WHERE id=".GF::quote($member_id);
			$res4 = $db->Execute($sql4);
			$owallet = $res4->fields['wallet'];
         //Weerasak 25-04-2566
			//$sqlUp = "UPDATE project_user SET wallet=wallet+".GF::quote($order_price)." WHERE id=".GF::quote($member_id);
			//$db->Execute($sqlUp);
			
			$base->set('_ids',$odrid);
	        $orderInfo = $this->_orderInfo();
	        $orernumber = $orderInfo['order_number'];
	        $test_odr = 'ยกเลิก ORDER ID :'.$orernumber;
	        
	        $sql3 = "SELECT wallet FROM project_user WHERE id=".GF::quote($member_id);
			$res3 = $db->Execute($sql3);
			
	        $new_balance = $res3->fields['wallet']+$owallet;
	        $old_balance= $owallet;
	        $mywallet=$res3->fields['wallet'];
			$sql ="INSERT INTO project_wallet_log(
	 												user_id,
	 												wallet,
	                                                before_balance,
	                                                after_balance,
	 												wallet_type,
	 												wallet_desc,
	                                                wallet_ref,
	                                                wallet_ref_type,
	 												create_dtm,
	 												create_by
	 												)
	 												VALUES(
	 												".GF::quote($member_id).",
	 												".GF::quote($mywallet).",
	                                                ".GF::quote($old_balance).",
	                                                ".GF::quote($new_balance).",
	 												'IN',
	                                                ".GF::quote($test_odr).",
	                                                ".GF::quote($odrid).",
	                                                '7',
	 												NOW(),
	 												".GF::quote($user_id)."
	 												)";
	 		$res = $db->Execute($sql);
		}

        //$mail = Mailer::getInstance();
        
        
        // $orderinfo = $db2->get("web_order","*",array("id"=>$odrid));
        // $mail->userid = $orderinfo['member_id'];
        // $mail->Subject = "เปลี่ยนแปลงสถานะการสั่งซื้อ";
        // $members = GF::memberinfo($orderinfo['member_id']);
        // $dataHtml = GF::mailtemplate('9');
        // $dataHtml = str_replace("{CUST_NAME}",$members['user_name'],$dataHtml);
        // $dataHtml = str_replace("{ORDER_NO}",$orderinfo['order_number'],$dataHtml);
        // $dataHtml = str_replace("{ORDER_STATUS}",GF::cvstatus($base->get('GET.value')),$dataHtml);
        // $mail->msg = $dataHtml;
        // $mail->SendingToNoti();
        $orderinfo_b = $db2->get("web_order","*",array("id"=>$odrid));
        $members = GF::memberinfo($orderinfo_b['member_id']);
        $link = $base->get('BASEURL').'/#/buyship/view/'.$base->get('GET.order_id');
        $dataReplace = array(
            "{CUST_NAME}"=>$members['user_name'],
            "{LINK}"=>GF::shorturl($link,'1'),
            "{ORDER_NO}"=>$orderinfo_b['order_number'],
            "{ORDER_STATUS}"=>GF::cvstatus($base->get('GET.value'))
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('3',$members['ref_id']);
        $mail->msgHTML = GF::mailtemplate('3',$dataReplace,$members['ref_id']);
        $mail->addAddressEmail = $members['user_email'];
        $mail->addAddressName = $members['user_name'];
        $mail->sendMail();
	}
   private function _orderStatus2(){

		$base = Base::getInstance();
		$db = DB::getInstance();
      	$db2 = DB2::getInstance();

      	$member = new Member();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

      	$rate = GF::getrate();


		$sql = "UPDATE web_order SET export=".GF::quote($base->get('GET.value')).",export_date=NOW(),export_by=".GF::quote($user_id)." WHERE id=".GF::quote($base->get('GET.order_id'));
		$res = $db->Execute($sql);

      /*$db2->insert("web_order_rate_history",array(
         "order_id"=>$base->get('GET.order_id'),
         "rate"=>$rate,
         "status"=>$base->get('GET.value'),
         "create_dtm"=>date('Y-m-d H:i:s'),
         "create_by"=>$user_id

      ));*/

    //   $mail = Mailer::getInstance();
    //   $db2 = DB2::getInstance();
    //   $odrid = $base->get('GET.order_id');
    //   $orderinfo = $db2->get("web_order","*",array("id"=>$odrid));
    //   $mail->userid = $orderinfo['member_id'];
    //   $mail->Subject = "เปลี่ยนแปลงสถานะการสั่งซื้อ";
    //   $members = GF::memberinfo($orderinfo['member_id']);
    //   $dataHtml = GF::mailtemplate('9');
    //   $dataHtml = str_replace("{CUST_NAME}",$members['user_name'],$dataHtml);
    //   $dataHtml = str_replace("{ORDER_NO}",$orderinfo['order_number'],$dataHtml);
    //   $dataHtml = str_replace("{ORDER_STATUS}",'ส่งรายการสั่งซื้อ',$dataHtml);
    //   $mail->msg = $dataHtml;
    //   $mail->SendingToNoti();
        $orderinfo_b = $db2->get("web_order","*",array("id"=>$odrid));
        $members = GF::memberinfo($orderinfo_b['member_id']);
        $link = $base->get('BASEURL').'/#/buyship/view/'.$base->get('GET.order_id');
        $dataReplace = array(
            "{CUST_NAME}"=>$members['user_name'],
            "{LINK}"=>GF::shorturl($link,'1'),
            "{ORDER_NO}"=>$orderinfo_b['order_number'],
            "{ORDER_STATUS}"=>'ส่งรายการสั่งซื้อ'
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('3',$members['ref_id']);
        $mail->msgHTML = GF::mailtemplate('3',$dataReplace,$members['ref_id']);
        $mail->addAddressEmail = $members['user_email'];
        $mail->addAddressName = $members['user_name'];
        $mail->sendMail();

	}
    private function _orderStatus3(){

		$base = Base::getInstance();
		$db = DB::getInstance();
      	$db2 = DB2::getInstance();

      	$member = new Member();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

      $rate = GF::getrate();
     //weerasak 17/11/2564
     //add > status = '5'
		$sql = "UPDATE web_order SET status = '5',export=".GF::quote($base->get('GET.value')).",export_date=NOW(),export_by=".GF::quote($user_id)." WHERE id=".GF::quote($base->get('GET.order_id'));
		$res = $db->Execute($sql);

		$SQL_order = "SELECT * FROM web_order WHERE id=".GF::quote($base->get('GET.order_id'));
		$res_order = $db->Execute($SQL_order);

		$SQL_user = "SELECT * FROM project_user WHERE id=".GF::quote($res_order->fields['member_id']);
		$res_user = $db->Execute($SQL_user);

		$test_odr = '';
		$walltype = '';
		$priceOrder = abs($base->get('GET.diff'));
        $wallet_ref = '';
        $wallet_ref_type = '';

		if($base->get('GET.diff')<0){
			$sql = "UPDATE web_order SET order_price=order_price+".abs($base->get('GET.diff'))." WHERE id=".GF::quote($base->get('GET.order_id'));
			$res = $db->Execute($sql);

			$test_odr = 'ชำระค่าสั่งซื้อ ORDER ID :'.$res_order->fields['order_number'];
            $walltype = 'OU';

            $wallet_ref = $res_order->fields['order_id'];
            $wallet_ref_type = '1';
		}else{
			$sql = "UPDATE web_order SET refund_wallet='".$base->get('GET.diff')."' WHERE id=".GF::quote($base->get('GET.order_id'));
			$res = $db->Execute($sql);

			$test_odr = 'คืนเงินค่าสินค้า ORDER ID :'.$res_order->fields['order_number'];
            $walltype = 'IN';
            
            $wallet_ref = $res_order->fields['order_id'];
            $wallet_ref_type = '3';
		}
      // Weerasak 25-04-2566
		// $sql = "UPDATE project_user SET
		// 								wallet=wallet+".$base->get('GET.diff').",
		// 								update_dtm=NOW()
		// 								WHERE id=".GF::quote($res_order->fields['member_id']);
		// $res = $db->Execute($sql);

		$old_balance = $res_user->fields['wallet'];
		$new_balance = $res_user->fields['wallet']+$base->get('GET.diff');

		
		$sql ="INSERT INTO project_wallet_log(
												user_id,
												wallet,
												before_balance,
												after_balance,
												wallet_type,
												wallet_desc,
                                                wallet_ref,
                                                wallet_ref_type,
												create_dtm,
												create_by
												)
												VALUES(
												".GF::quote($res_order->fields['member_id']).",
												".GF::quote($priceOrder).",
												".GF::quote($old_balance).",
												".GF::quote($new_balance).",
												".GF::quote($walltype).",
                                                ".GF::quote($test_odr).",
                                                ".GF::quote($wallet_ref).",
                                                ".GF::quote($wallet_ref_type).",
												NOW(),
												".GF::quote($user_id)."
												)";
		$res = $db->Execute($sql);

      /*$db2->insert("web_order_rate_history",array(
         "order_id"=>$base->get('GET.order_id'),
         "rate"=>$rate,
         "status"=>$base->get('GET.value'),
         "create_dtm"=>date('Y-m-d H:i:s'),
         "create_by"=>$user_id

      ));*/

    //   $mail = Mailer::getInstance();
    //   $db2 = DB2::getInstance();
    //   $odrid = $base->get('GET.order_id');
    //   $orderinfo = $db2->get("web_order","*",array("id"=>$odrid));
    //   $mail->userid = $orderinfo['member_id'];
    //   $mail->Subject = "เปลี่ยนแปลงสถานะการสั่งซื้อ";
    //   $members = GF::memberinfo($orderinfo['member_id']);
    //   $dataHtml = GF::mailtemplate('9');
    //   $dataHtml = str_replace("{CUST_NAME}",$members['user_name'],$dataHtml);
    //   $dataHtml = str_replace("{ORDER_NO}",$orderinfo['order_number'],$dataHtml);
    //   $dataHtml = str_replace("{ORDER_STATUS}",GF::cvstatus('4'),$dataHtml);
    //   $mail->msg = $dataHtml;
    //   $mail->SendingToNoti();
        $orderinfo_b = $db2->get("web_order","*",array("id"=>$odrid));
        $members = GF::memberinfo($orderinfo_b['member_id']);
        $link = $base->get('BASEURL').'/#/buyship/view/'.$base->get('GET.order_id');
        $dataReplace = array(
            "{CUST_NAME}"=>$members['user_name'],
            "{LINK}"=>GF::shorturl($link,'1'),
            "{ORDER_NO}"=>$orderinfo_b['order_number']
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('14',$members['ref_id']);
        $mail->msgHTML = GF::mailtemplate('14',$dataReplace,$members['ref_id']);
        $mail->addAddressEmail = $members['user_email'];
        $mail->addAddressName = $members['user_name'];
        $mail->sendMail();


	}
	private function _orderPay(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order SET order_price=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.order_id'));
		$res = $db->Execute($sql);
	}
	private function _itemStatus(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order_item SET status=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);
	}
   private function _itemType(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order_item SET order_type=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.item_id'));
      echo $sql; 
		$res = $db->Execute($sql);
	}
	private function _itemComment(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order_item SET note=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);
	}
	private function _priceRmb(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$priceThb = ($base->get('GET.value')*$base->get('GET.rate'))*$base->get('GET.qty');

		$sql = "UPDATE web_order_item SET product_price_rmb=".GF::quote($base->get('GET.value')).",product_price_thb=".GF::quote($priceThb)." WHERE id=".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);

      $sql = "SELECT * FROM web_order_item WHERE id=".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);

      $order_id = $res->fields['order_id'];

      $count_price = $this->_countItemPrice($order_id);

      $price_rmb = $count_price['product_price_rmb'];
      $total_ship = ($price_rmb*10)/100;
      if($total_ship<10){$total_ship=10;}
      if($total_ship>100){$total_ship=100;}

      $sql2 = "SELECT rate FROM web_order WHERE id=".GF::quote($order_id);
		$res2 = $db->Execute($sql2);
        $total_ship_yuan = $total_ship;
      $total_ship = $total_ship*$res2->fields['rate'];

      $sql = "UPDATE web_order SET order_pay_ship=".GF::quote($total_ship).",order_pay_ship_yuan=".GF::quote($total_ship_yuan)." WHERE id=".GF::quote($order_id);
		$res = $db->Execute($sql);

	}
   private function _priceRmb2(){

		$base = Base::getInstance();
		$db = DB::getInstance();



		$sql = "UPDATE web_order_item SET product_buy_rmb=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);

      // $sql = "SELECT * FROM web_order_item WHERE id=".GF::quote($base->get('GET.item_id'));
		// $res = $db->Execute($sql);
      //
      // $order_id = $res->fields['order_id'];
      //
      // $count_price = $this->_countItemPrice($order_id);
      //
      // $price_rmb = $count_price['product_price_rmb'];
      // $total_ship = ($price_rmb*10)/100;
      // if($total_ship<10){$total_ship=10;}
      //
      // $sql2 = "SELECT rate FROM web_order WHERE id=".GF::quote($order_id);
		// $res2 = $db->Execute($sql2);
      //
      // $total_ship = $total_ship*$res2->fields['rate'];
      //
      // $sql = "UPDATE web_order SET order_pay_ship=".GF::quote($total_ship)." WHERE id=".GF::quote($order_id);
		// $res = $db->Execute($sql);

	}
   private function _priceCnShip(){

		$base = Base::getInstance();
		$db = DB::getInstance();



		$sql = "UPDATE web_order SET order_pay_cn=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.id'));
		$res = $db->Execute($sql);

      // $sql = "SELECT * FROM web_order_item WHERE id=".GF::quote($base->get('GET.item_id'));
		// $res = $db->Execute($sql);
      //
      // $order_id = $res->fields['order_id'];
      //
      // $count_price = $this->_countItemPrice($order_id);
      //
      // $price_rmb = $count_price['product_price_rmb'];
      // $total_ship = ($price_rmb*10)/100;
      // if($total_ship<10){$total_ship=10;}
      //
      // $sql2 = "SELECT rate FROM web_order WHERE id=".GF::quote($order_id);
		// $res2 = $db->Execute($sql2);
      //
      // $total_ship = $total_ship*$res2->fields['rate'];
      //
      // $sql = "UPDATE web_order SET order_pay_ship=".GF::quote($total_ship)." WHERE id=".GF::quote($order_id);
		// $res = $db->Execute($sql);

	}
	private function _priceThb(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order_item SET product_price_thb=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);
	}
	private function _sallerNumber(){

		$base = Base::getInstance();
		$db = DB::getInstance();

      $sqlGetNoSaller = "SELECT order_id,product_saller_name FROM web_order_item WHERE id = ".GF::quote($base->get('GET.item_id'))." AND (product_saller_name = 'ไม่พบชื่อร้าน' OR product_saller_name = '')";
      $res = $db->Execute($sqlGetNoSaller);
      $count = $res->NumRows();
      if($count > 0){
         $sqlGetSaller = "SELECT product_saller_name FROM web_order_item WHERE order_id = ".GF::quote($res->fields['order_id'])." AND saller_number = ".GF::quote($base->get('GET.value'))." ORDER BY id LIMIT 1";
         $res1 = $db->Execute($sqlGetSaller);
         $sql = "UPDATE web_order_item SET saller_number=".GF::quote($base->get('GET.value')).",product_saller_name = '".$res1->fields['product_saller_name']."' WHERE id=".GF::quote($base->get('GET.item_id'));
      }else{
         $sql = "UPDATE web_order_item SET saller_number=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.item_id'));
      }


		//$sql = "UPDATE web_order_item SET saller_number=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);
	}
	private function _itemQty(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$priceThb = ($base->get('GET.price_rmb')*$base->get('GET.rate'))*$base->get('GET.value');

		$sql = "UPDATE web_order_item SET product_qty=".GF::quote($base->get('GET.value')).",product_price_thb=".GF::quote($priceThb)." WHERE id=".GF::quote($base->get('GET.item_id'));
		//echo $sql;
		$res = $db->Execute($sql);

        $sql = "SELECT * FROM web_order_item WHERE id=".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);

      $order_id = $res->fields['order_id'];

      $count_price = $this->_countItemPrice($order_id);

      $price_rmb = $count_price['product_price_rmb'];
      $total_ship = ($price_rmb*10)/100;
      if($total_ship<10){$total_ship=10;}
      if($total_ship>100){$total_ship=100;}

      $sql2 = "SELECT rate FROM web_order WHERE id=".GF::quote($order_id);
		$res2 = $db->Execute($sql2);

        $total_ship_yuan = $total_ship;
      $total_ship = $total_ship*$res2->fields['rate'];

      $sql = "UPDATE web_order SET order_pay_ship=".GF::quote($total_ship).",order_pay_ship_yuan=".GF::quote($total_ship_yuan)." WHERE id=".GF::quote($order_id);
		$res = $db->Execute($sql);

	}
   private function _itemQty2(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		//$priceThb = ($base->get('GET.price_rmb')*$base->get('GET.rate'))*$base->get('GET.value');

		$sql = "UPDATE web_order_item SET product_buy_qty=".GF::quote($base->get('GET.value'))." WHERE id=".GF::quote($base->get('GET.item_id'));
		//echo $sql;
		$res = $db->Execute($sql);

      // $sql = "SELECT * FROM web_order_item WHERE id=".GF::quote($base->get('GET.item_id'));
		// $res = $db->Execute($sql);
      //
      // $order_id = $res->fields['order_id'];
      //
      // $count_price = $this->_countItemPrice($order_id);
      //
      // $price_rmb = $count_price['product_price_rmb'];
      // $total_ship = ($price_rmb*10)/100;
      // if($total_ship<10){$total_ship=10;}
      //
      // $sql2 = "SELECT rate FROM web_order WHERE id=".GF::quote($order_id);
		// $res2 = $db->Execute($sql2);
      //
      // $total_ship = $total_ship*$res2->fields['rate'];
      //
      // $sql = "UPDATE web_order SET order_pay_ship=".GF::quote($total_ship)." WHERE id=".GF::quote($order_id);
		// $res = $db->Execute($sql);

	}
	private function _addOtherItem(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		if($base->get('POST.order_id')!=''){
			$sql = "INSERT INTO web_order_item (
									order_id,
									product_title,
									product_qty,
									product_price_rmb,
									product_price_thb,
									item_typs_s,
									status,
									create_dtm
								) VALUES (
									".GF::quote($base->get('POST.order_id')).",
									".GF::quote($base->get('POST.item_name')).",
									".GF::quote($base->get('POST.item_qty')).",
									".GF::quote($base->get('POST.item_rmb')).",
									".GF::quote($base->get('POST.item_thb')).",
									".GF::quote($base->get('POST.item_type')).",
									'W',
									NOW()
								)";
								echo $sql;
			$res = $db->Execute($sql);
			if($res){
				//return true;
			}
			return true;
		}
		return true;
	}

	private function _paymentList(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();
		$tracking = new STracking();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

			$dtatReturn = array();

         $cond_search = '';
         if($_SESSION['pay_search']['order_number']!=''){
            $order_ids = GF::order_id($_SESSION['pay_search']['order_number']);
            $cond_search .= " AND order_id=".GF::quote($order_ids)." ";
         }
         if($_SESSION['pay_search']['user_code']!=''){
            $user_id = GF::user_id($_SESSION['pay_search']['user_code']);
            $cond_search .= " AND member_id =".GF::quote($user_id)." ";
        }

         if($_SESSION['pay_search']['status']!=''){
            $cond_search .= " AND status=".GF::quote($_SESSION['pay_search']['status'])." ";
         }

         if($_SESSION['pay_search']['create_dtm']!=''){
            $cond_search .= " AND payment_date=".GF::quote($_SESSION['pay_search']['create_dtm'])." ";
         }
         // Edit By Weerasak 25-01-2566
         if($_SESSION['pay_search']['start_dtm'] != ''){
            $cond_search .= " AND pay_date >=".GF::quote(date('Y-m-d',strtotime($_SESSION['pay_search']['start_dtm'])))." ";
         }
         if($_SESSION['pay_search']['end_dtm'] != ''){
            $cond_search .= " AND pay_date <=".GF::quote(date('Y-m-d',strtotime($_SESSION['pay_search']['end_dtm'])))." ";
         }
         // END

         $agency =  $memberInfomation['agency'];
         if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
               //GF::print_r($list_ref);
               $ref_str = implode(',', $list_ref);
               $cond_search .= " AND member_id IN(".$ref_str.") ";
            }else{
               $cond_search .= " AND member_id ='X' ";
            }
         }

         // Edit By Weerasak 25-01-2566
			//$sql = "SELECT * FROM web_payment WHERE status!='C' ".$cond_search." ORDER BY update_dtm DESC,create_dtm DESC";
         $sql = "SELECT * FROM (
            SELECT
               *,date(STR_TO_DATE(payment_date, '%m/%d/%Y')) pay_date
            FROM	web_payment 
            WHERE
               STATUS != 'C' 
            ) K WHERE STATUS != 'C' ".$cond_search."
            ORDER BY
               update_dtm DESC,
               create_dtm DESC";
         // END
         //echo $sql;
         $res = $db->Execute($sql);
   		$count = $res->NumRows();

   			$base->set('allPage',ceil($count/20));

   		$page = $base->get('GET.page');

   		$numr = 20;
   		$start = "";
   		$end = "";
   		if($page==1||empty($page)){
   			$start = 0;
   			$page = 1;
   			//$end = $numr;
   		}else{
   			//$end = ($page*$numr);
   			$start = ($page*$numr)-$numr;
   		}
   		 $condNum = " LIMIT ".$start.",".$numr;

   		 $base->set('currpage',$page);

   		 $res = $db->Execute($sql.$condNum);

        

			//$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['id'] = $res->fields['id'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
				$base->set('_ids',$res->fields['order_id']);
				$ArrList[$i]['order_type'] = $res->fields['order_type'];
				$base->set('_ids',$res->fields['order_id']);
				if($res->fields['order_type']=='1'){
					$orderInfo = $this->_orderInfo();
				}else{

					$orderInfo = $tracking->shippingInfo();
				}
                $ArrList[$i]['order_info'] = $orderInfo;
                $base->set('bank_id',$res->fields['payment_bank']);
                $paybank = $this->_newbankInfo();
                $ArrList[$i]['paybank'] = $paybank;
				$bankdata = GF::masterdata($paybank['master_id']);
                $ArrList[$i]['payment_bank_id'] = $res->fields['payment_bank'];
				$bankdata = GF::masterdata($res->fields['payment_bank']);
				$ArrList[$i]['payment_bank'] = $bankdata['master_name'];//$this->_bankInfo($res->fields['payment_bank']);
				$ArrList[$i]['status'] = $res->fields['status'];
				$ArrList[$i]['payment_amount'] = $res->fields['payment_amount'];
				$ArrList[$i]['payment_date'] = $res->fields['payment_date'];
				$ArrList[$i]['payment_time'] = $res->fields['payment_time'];
				$ArrList[$i]['payment_comment'] = $res->fields['payment_comment'];
				$ArrList[$i]['payment_slip'] = $res->fields['payment_slip'];
            $ArrList[$i]['member_id'] = $res->fields['member_id'];
            $ArrList[$i]['remarks'] = $res->fields['remarks'];
				$i++;
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
		}


		private function _bankInfo($bankid){
			$base = Base::getInstance();
			$db = DB::getInstance();

			$sql = "SELECT * FROM web_payment_bank WHERE id=".$bankid;
			$res = $db->Execute($sql);

			return  $res->fields['description'];
		}
		private function _paymentStatus(){

			$base = Base::getInstance();
			$db = DB::getInstance();
            $db2 = DB2::getInstance();

            $member = new Member();

    		$memberInfomation = $member->memberInfomation();

    		$user_id_admin = $memberInfomation['id'];
    		$sql = "SELECT * FROM web_payment WHERE id=".GF::quote($base->get('GET.id'));
			$resWebPay = $db->Execute($sql);
			$user_id = $resWebPay->fields['member_id'];

			//GF::print_r($base->get('GET'));

			//exit();
         //Edit By Weerasak 26-01-2566
         //เพิ่มอัพเดทฟิว remarks สำหรับ admin
			$sql = "UPDATE web_payment SET status=".GF::quote($base->get('GET.value')).",remarks=".GF::quote($base->get('GET.remarks')).",update_dtm=NOW() WHERE id=".GF::quote($base->get('GET.id'));
         //END
         $res = $db->Execute($sql);

         if($base->get('GET.value') =='F'){
            $sql = "UPDATE web_order SET status = '7' WHERE id=".GF::quote($resWebPay->fields['order_id']);            
            $db->Execute($sql);
         }else if($base->get('GET.value')=='A'){

				$sql = "SELECT * FROM web_payment WHERE id=".GF::quote($base->get('GET.id'));
				$res = $db->Execute($sql);

                $pay_amount = $res->fields['payment_amount'];
                $member_id = $res->fields['member_id'];

				if($res->fields['order_type']=='1'){

                    $member_old_info = GF::memberinfo($user_id);
                    $oldWallet = $member_old_info['wallet'];

                    $old_balance = $oldWallet;
                    $new_balance = $pay_amount+$oldWallet;
                  
                  // Weerasak 25-04-2566
                  //   $sql = "UPDATE project_user SET
                  //                           wallet=wallet+'".$pay_amount."',
                  //                           update_dtm=NOW()
                  //                           WHERE id=".GF::quote($user_id);
                  //                           //echo $sql;
                  //   $res2 = $db->Execute($sql);


                    $sql_ss = "UPDATE web_shipping SET status=".GF::quote(5)." WHERE id=".GF::quote($base->get('GET.orderid'));
            		$res_ss = $db->Execute($sql_ss);

            		$sql_tt = "SELECT * FROM web_shipping_item WHERE shipping_id=".GF::quote($base->get('GET.orderid'));
					$res_tt = $db->Execute($sql_tt);

					$sql_tr = "UPDATE web_tracking SET status=".GF::quote(7)." WHERE tracking_id=".GF::quote($res_tt->fields['tracking_id']);
            		$res_tr = $db->Execute($sql_tr);
            		



                    $sql ="INSERT INTO project_wallet_log(
                                                    user_id,
                                                    wallet,
                                                    before_balance,
                                                    after_balance,
                                                    wallet_type,
                                                    wallet_desc,
                                                    wallet_ref_type,
                                                    create_dtm,
                                                    create_by
                                                    )
                                                    VALUES(
                                                    ".GF::quote($member_id).",
                                                    ".GF::quote(abs($pay_amount)).",
                                                    ".GF::quote($old_balance).",
                                                    ".GF::quote($new_balance).",
                                                    'IN',
                                                    'เติมเงิน',
                                                    '5',
                                                    NOW(),
                                                    ".GF::quote($user_id_admin)."
                                                    )";
                    $res3 = $db->Execute($sql);






                    $member_new_info = GF::memberinfo($user_id);
                    $currentWallet = $member_new_info['wallet'];

                    $base->set('_ids',$res->fields['order_id']);

                    $orderInfo = $this->_orderInfo();
                    $odr_idd = $res->fields['order_id'];

                    $totals = (($orderInfo['countprice']['product_price_rmb']*$orderInfo['rate'])+$orderInfo['order_pay_ship'])-$orderInfo['order_discount'];
                    $totals = $totals+$orderInfo['countprice']['product_price_thb'];
                    $totals = $totals-doubleval($orderInfo['order_price']);
                    $totals = number_format($totals, 2, '.', '');
                    $order_price = $totals; //totals ก่อน เทียบ wallet
                    //if($currentWallet>=$totals){
                    
                        //edit by kae 05-12-64
                        if($totals>=$currentWallet){  //เหลือ เออเร่อ กรณีจ่ายไม่ครบ ยังคำนวนไม่ถูก toyo.ta@hotmail.com
                           if($totals=$currentWallet)
                           {
                              $new_wallet = 0;
                              $totals = $currentWallet;
                           }                           
                        }
                        else
                        {
                           $new_wallet = $currentWallet-doubleval($totals);
                        }
                        //end edit by kae 05-12-64

                        //echo $totals;
                        // $order_price = $totals
                        // if($res->fields['payment_amount']<$totals){
                        //    $totals = $res->fields['payment_amount'];
                        // }
                        //Weerasak 19-11-2564
                        $sql = "SELECT SUM(payment_amount) as amount FROM web_payment WHERE order_id=".GF::quote($odr_idd)." AND status = 'A' and order_type='1' AND member_id = ".GF::quote($member_id);                        
                        $rs_payment = $db->Execute($sql);
                        // if($oldWallet < 0) //edit by kae 05-12-64,Weerasak Comment 20/12/2564
                        // {
                        //    $payment_amount = (round($order_price,2) - round($rs_payment->fields['amount'],2));
                        // }
                        // else{$payment_amount = round($order_price,2) - round($rs_payment->fields['amount'],2);}
                        
                        //Weerasak 20/12/2564 คำนวณยอดค้างจากการชำระเงินใหม่
                        $payment_amount = abs(($oldWallet - $order_price)) - round($rs_payment->fields['amount'],2);                        
                        if(($currentWallet-doubleval($order_price)) >= 0)
                           $payment_amount = 0;
                        
                        //  $x =  $currentWallet-doubleval($totals);
                        //  $s = "UPDATE test SET result ='".$x."' where id = 1";
                        //  $db->Execute($s);

                        //End edit
                        $sql = "UPDATE web_order SET order_price=order_price+".GF::quote($totals)." WHERE id=".GF::quote($res->fields['order_id']);
                        $res4 = $db->Execute($sql);

                        // Weerasak 25-04-2566
                        // $sql = "UPDATE project_user SET
                        //                         wallet='".$new_wallet."',
                        //                         update_dtm=NOW()
                        //                         WHERE id=".GF::quote($user_id);
                        //                         //echo $sql;
                        // $res = $db->Execute($sql);

                        if($payment_amount > 0) //Weerasak กำหนดสถานะเป็นค้างชำระ
                        {
                           $sql = "UPDATE web_order SET status='7' WHERE id=".GF::quote($odr_idd);
                           $res = $db->Execute($sql);
                        }else{
                           $base->set('orderid',$odr_idd);
                           $this->_genOrderNumber();

                           $base->set('_ids',$odr_idd);
                           $orderInfo = $this->_orderInfo();
                           $orernumber = $orderInfo['order_number'];

                           $sql = "UPDATE web_order SET status='3' WHERE id=".GF::quote($odr_idd);
                           $res = $db->Execute($sql);
                        }

                        // if($orderInfo['status']>=3){
                            
                        // }else{
                           //  $base->set('orderid',$odr_idd);
                           //  $this->_genOrderNumber();

                           //  $base->set('_ids',$odr_idd);
                           //  $orderInfo = $this->_orderInfo();
                           //  $orernumber = $orderInfo['order_number'];

                        //     $sql = "UPDATE web_order SET status='3' WHERE id=".GF::quote($odr_idd);
                        //     $res = $db->Execute($sql);
                        // }

                        $old_balance = $currentWallet;
                        $new_balance = $currentWallet-$totals;

                        $sql ="INSERT INTO project_wallet_log(
                                                        user_id,
                                                        wallet,
                                                        before_balance,
                                                        after_balance,
                                                        wallet_type,
                                                        wallet_desc,
                                                        wallet_ref,
                                                        wallet_ref_type,
                                                        create_dtm,
                                                        create_by
                                                        )
                                                        VALUES(
                                                        ".GF::quote($member_id).",
                                                        ".GF::quote(abs($totals)).",
                                                        ".GF::quote($old_balance).",
                                                        ".GF::quote($new_balance).",
                                                        'OU',
                                                        'ชำระค่าสั่งซื้อ ORDER : ".GF::order_no($resWebPay->fields['order_id'])."',
                                                        ".GF::quote($resWebPay->fields['order_id']).",
                                                        '1',
                                                        NOW(),
                                                        ".GF::quote($user_id_admin)."
                                                        )";
                        $res = $db->Execute($sql);
                        

                    //}

                    


				}
                else if($res->fields['order_type']=='3'){

                    $member_old_info = GF::memberinfo($user_id);
                    $oldWallet = $member_old_info['wallet'];

                    $old_balance = $oldWallet;
                    $new_balance = $pay_amount+$oldWallet;

                  // Weerasak 25-04-2566
                  //   $sql = "UPDATE project_user SET
                  //                           wallet=wallet+'".$pay_amount."',
                  //                           update_dtm=NOW()
                  //                           WHERE id=".GF::quote($user_id);
                  //                           echo $sql;
                  //   $res = $db->Execute($sql);



                    $sql ="INSERT INTO project_wallet_log(
                                                    user_id,
                                                    wallet,
                                                    before_balance,
                                                    after_balance,
                                                    wallet_type,
                                                    wallet_desc,
                                                    wallet_ref_type,
                                                    create_dtm,
                                                    create_by
                                                    )
                                                    VALUES(
                                                    ".GF::quote($member_id).",
                                                    ".GF::quote(abs($pay_amount)).",
                                                    ".GF::quote($old_balance).",
                                                    ".GF::quote($new_balance).",
                                                    'IN',
                                                    'เติมเงิน',
                                                    '5',
                                                    NOW(),
                                                    ".GF::quote($user_id_admin)."
                                                    )";
                    $res = $db->Execute($sql);


                }else{

                    $member_old_info = GF::memberinfo($user_id);
                    $oldWallet = $member_old_info['wallet'];

                    $sqlShipping = "SELECT * FROM web_shipping WHERE id=".GF::quote($resWebPay->fields['order_id']);
                    $resShipping  = $db->Execute($sqlShipping);

                    $tracking = new STracking();

                    $calculate_tracking_price = $tracking->_calculateShippingprice($resWebPay->fields['order_id']);
                    $calculate_tracking_price_ol = $calculate_tracking_price;

                    $other_price = $resShipping->fields['shipping_th_price']+$resShipping->fields['shipping_th_service_price']+$resShipping->fields['shipping_other_price'];
                    $calculate_tracking_price = $calculate_tracking_price+$other_price;

                    $total_track_price = $calculate_tracking_price-$resShipping->fields['ship_pay'];

                    $old_balance = $oldWallet;
                    $new_balance = $pay_amount+$oldWallet;
                    

                    //Weerasak 25-04-2566
                  //   $sql = "UPDATE project_user SET
                  //                           wallet=wallet+'".$pay_amount."',
                  //                           update_dtm=NOW()
                  //                           WHERE id=".GF::quote($user_id);
                  //                           //echo $sql;
                  //   $res2 = $db->Execute($sql);



                    $sql ="INSERT INTO project_wallet_log(
                                                    user_id,
                                                    wallet,
                                                    before_balance,
                                                    after_balance,
                                                    wallet_type,
                                                    wallet_desc,
                                                    wallet_ref_type,
                                                    create_dtm,
                                                    create_by
                                                    )
                                                    VALUES(
                                                    ".GF::quote($member_id).",
                                                    ".GF::quote(abs($pay_amount)).",
                                                    ".GF::quote($old_balance).",
                                                    ".GF::quote($new_balance).",
                                                    'IN',
                                                    'เติมเงิน',
                                                    '5',
                                                    NOW(),
                                                    ".GF::quote($user_id_admin)."
                                                    )";
                    $res4 = $db->Execute($sql);


                    $member_old_info = GF::memberinfo($user_id);
                    $oldWallet = $member_old_info['wallet'];

                    $old_balance = $oldWallet;
                    
                    
                    if($old_balance>0){

                            if($old_balance>=$total_track_price){

                                $new_balance = $old_balance-$total_track_price;

                                $sql = "UPDATE web_shipping SET ship_pay=ship_pay+".GF::quote($total_track_price).",status='5',ship_pay_dtm=NOW(),billing_total=".GF::quote($calculate_tracking_price_ol)." WHERE id=".GF::quote($resWebPay->fields['order_id']);
                                $res = $db->Execute($sql);

                                $memberinf_do = GF::memberinfo($user_id);                                
                                $dataSet = $db2->get("web_shipping",array(
                                                                            "shipping_code(billing_no)",
                                                                            "create_dtm(billing_date)",
                                                                            "shipping_name(billing_recipients)",
                                                                            "shipping_address(billing_address)",
                                                                            "create_dtm(billing_submit_date)",
                                                                            "shipping_phone(billing_tel)",
                                                                            "status(billing_status)",
                                                                            "billing_total(billing_total)"
                                                                            ),array(
                                                                            "id"=>$resWebPay->fields['order_id']
                                                                            ));
                                $dataAdd = $db2->get("web_shipping","*",array(
                                    "id"=>$resWebPay->fields['order_id']
                                ));

                                $dataSet['billing_p3'] = $dataAdd['billing_total']+$dataAdd['shipping_th_price']+$dataAdd['shipping_th_service_price']+$dataAdd['shipping_other_price'];
                                $dataSet['billing_sum_total'] = $dataSet['billing_p3'];
                                                                            
                                $dataSet['user_code'] = $memberinf_do['user_code'];
                                $dataItem = $db2->select("web_shipping_item",array(
                                                           "tracking(track_no)",
                                                           "ship_price(track_price)",
                                                           "track_rate(track_rate)"
	   													),array("shipping_id"=>$resWebPay->fields['order_id']));
                                $dataSetArr = array();
                                $dataSetArr['order'] =  $dataSet;
                                $dataSetArr['item'] = $dataItem;
                                $db2->insert("project_webservice",array(
                                                                            "service_token"=>GF::randomStr(20),
                                                                            "service_method"=>"UPDATE_BILL",
                                                                            "service_data"=>json_encode($dataSetArr),
                                                                            "status"=>'N',
                                                                            "create_dtm"=>date('Y-m-d H:i:s'),
                                                                            "update_dtm"=>date('Y-m-d H:i:s')
                                                                            ));

                                $order_number = $resShipping->fields['shipping_code'];

                                $sql ="INSERT INTO project_wallet_log(
                                                                user_id,
                                                                wallet,
                                                                before_balance,
                                                                after_balance,
                                                                wallet_type,
                                                                wallet_desc,
                                                                wallet_ref,
                                                                wallet_ref_type,
                                                                create_dtm,
                                                                create_by
                                                                )
                                                                VALUES(
                                                                ".GF::quote($member_id).",
                                                                ".GF::quote(abs($total_track_price)).",
                                                                ".GF::quote($old_balance).",
                                                                ".GF::quote($new_balance).",
                                                                'OU',
                                                                'ชำระค่านำเข้า ORDER ID : ".$order_number."',
                                                                ".GF::quote($resWebPay->fields['order_id']).",
                                                                '2',
                                                                NOW(),
                                                                ".GF::quote($user_id_admin)."
                                                                )";
                                $res = $db->Execute($sql);

                                $sqlselectItem = "SELECT tracking_id FROM web_shipping_item WHERE shipping_id=".intval($resWebPay->fields['order_id']);
                                $resItem = $db->Execute($sqlselectItem);
                                $itemArr = array();
                                $x=0;
                                while(!$resItem->EOF){
                                    $itemArr[$x] = $resItem->fields['tracking_id'];
                                    $x++;
                                    $resItem->MoveNext();
		                        }
                                foreach($itemArr as $itmval){
                                    $SQLupdate = "UPDATE web_tracking SET status='7' WHERE tracking_id=".intval($itmval);
                                    $db->Execute($SQLupdate);
                                }

                              // Weerasak 25-04-2566
                              //   $sql = "UPDATE project_user SET
                              //                           wallet=wallet-'".$total_track_price."',
                              //                           update_dtm=NOW()
                              //                           WHERE id=".GF::quote($user_id);
                              //                           //echo $sql;
                              //   $res2 = $db->Execute($sql);
                                
                                $trackArr = $itemArr;
                                $dataSet = $db2->select("web_tracking",array(
                                                                            "tracking(track_no)",
                                                                            "member_code(track_user_code)",
                                                                            "status(track_status)",
                                                                            "update_dtm(track_upddate)",
                                                                            "create_dtm(track_createdate)"
                                                                        ),array(
                                                                        "tracking_id"=>$trackArr
                                                                        ));
                                if(count($dataSet)>0){
                                    
                                    $db2->insert("project_webservice",array(
                                                                            "service_token"=>GF::randomStr(20),
                                                                            "service_method"=>"UPDATE_TRACK",
                                                                            "service_data"=>json_encode($dataSet),
                                                                            "status"=>'N',
                                                                            "create_dtm"=>date('Y-m-d H:i:s'),
                                                                            "update_dtm"=>date('Y-m-d H:i:s')
                                                                            ));
                                }
                            }else{
                                $new_balance = 0;

                                $sql = "UPDATE web_shipping SET ship_pay=ship_pay+".GF::quote($old_balance)." WHERE id=".GF::quote($resWebPay->fields['order_id']);
                                $res = $db->Execute($sql);

                                $order_number = $resShipping->fields['shipping_code'];

                                $sql ="INSERT INTO project_wallet_log(
                                                                user_id,
                                                                wallet,
                                                                before_balance,
                                                                after_balance,
                                                                wallet_type,
                                                                wallet_desc,
                                                                wallet_ref,
                                                                wallet_ref_type,
                                                                create_dtm,
                                                                create_by
                                                                )
                                                                VALUES(
                                                                ".GF::quote($member_id).",
                                                                ".GF::quote(abs($old_balance)).",
                                                                ".GF::quote($old_balance).",
                                                                ".GF::quote($new_balance).",
                                                                'OU',
                                                                'ชำระค่านำเข้า ORDER ID : ".$order_number."',
                                                                ".GF::quote($resWebPay->fields['order_id']).",
                                                                '2',
                                                                NOW(),
                                                                ".GF::quote($user_id_admin)."
                                                                )";
                                $res = $db->Execute($sql);

                              // Weerasak 25-04-2566
                              //   $sql = "UPDATE project_user SET
                              //                           wallet=0,
                              //                           update_dtm=NOW()
                              //                           WHERE id=".GF::quote($user_id);
                              //                           //echo $sql;
                              //   $res2 = $db->Execute($sql);
                            }

                    }

                }

				//echo $sql;

			}

            $text_status = 'ยืนยันยอดแล้ว';
            if($base->get('GET.value')!='A'){
                $text_status = 'ไม่พบยอด';
            }

            // $mail = Mailer::getInstance();
            // $mail->userid = $member_id;
            // $mail->Subject = "แจ้งการชำระเงิน";
            // $members = GF::memberinfo($resWebPay->fields['member_id']);
            // $dataHtml = GF::mailtemplate('10');
            // $dataHtml = str_replace("{CUST_NAME}",$members['user_name'],$dataHtml);
            // $dataHtml = str_replace("{PAYMENT_STATUS}",$text_status,$dataHtml);
            // $mail->msg = $dataHtml;
            // $mail->SendingToNoti();
            $link = $base->get('BASEURL').'/#/ewallet/paymentlist/';;
            $members = GF::memberinfo($member_id);
            $dataReplace = array(
                "{CUST_NAME}"=>$members['user_name'],
                "{PAYMENT_STATUS}"=>$text_status,
                "{LINK}"=>$link
            );
            $mail = Mailer::getInstance();
            $mail->Subject = GF::mailsubject('4',$members['ref_id']);
            $mail->msgHTML = GF::mailtemplate('4',$dataReplace,$members['ref_id']);
            $mail->addAddressEmail = $members['user_email'];
            $mail->addAddressName = $members['user_name'];
            $mail->sendMail();
		}
        
		private function _itemListByExport(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		/*$item_id = $base->get('_ids');

		$cond = '';

		$sql = "SELECT * FROM web_order WHERE status='E' AND export='N' ".$cond." ORDER BY id ASC";
		echo $sql;
		$res = $db->Execute($sql);
		$i=0;
		$ArrListOrp = array();
		while(!$res->EOF){
			$ArrListOrp[$i] = $res->fields['id'];
			$i++;
			$res->MoveNext();
		}
		$allID = implode(',',$ArrListOrp);
		if($allID==''){
			return false;
			exit();
		}*/
		$allID = $base->get('newid');//implode(',',$base->get('POST.ids'));

		$sql = "SELECT web_order_item.*,web_order.member_id FROM web_order_item
LEFT JOIN web_order
ON web_order_item.order_id=web_order.id

WHERE web_order_item.order_id IN(".$allID.") AND web_order_item.status!='D' AND web_order_item.item_typs_s='NR' ORDER BY web_order_item.product_saller_name ASC";

		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['product_link'] = $res->fields['product_link'];
			$ArrList[$i]['member_id'] = $res->fields['member_id'];
			$memberinfo = GF::memberinfo($res->fields['member_id']);
			$ArrList[$i]['member_code'] = $memberinfo['user_code'];
			$base->set('order_id',$res->fields['order_id']);
			$ordinfo = $this->_orderSet();
			$ArrList[$i]['order_id'] = $ordinfo['order_id'];
			$ArrList[$i]['order_number'] = $ordinfo['order_number'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['product_title'] = $res->fields['product_title'];
			$ArrList[$i]['product_sku_1'] = $res->fields['product_sku_1'];
			$ArrList[$i]['product_image'] = $res->fields['product_image'];
			$ArrList[$i]['product_qty'] = $res->fields['product_qty'];
			$ArrList[$i]['product_saller_name'] = $res->fields['product_saller_name'];
			$ArrList[$i]['saller_number'] = $res->fields['saller_number'];
			$ArrList[$i]['product_saller_link'] = $res->fields['product_saller_link'];
			$ArrList[$i]['product_price_rmb'] = $res->fields['product_price_rmb'];
			$ArrList[$i]['product_price_thb'] = $res->fields['product_price_thb'];
			$ArrList[$i]['ship_cn_cn'] = $res->fields['ship_cn_cn'];
			$ArrList[$i]['ship_cn_th'] = $res->fields['ship_cn_th'];
			$ArrList[$i]['ship_all'] = $res->fields['ship_all'];
			$ArrList[$i]['refund'] = $res->fields['refund'];
			$ArrList[$i]['upfund'] = $res->fields['upfund'];
			$ArrList[$i]['note'] = $res->fields['note'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['order_ship_cn'] = $res->fields['order_ship_cn'];
			$ArrList[$i]['order_ship_th'] = $res->fields['order_ship_th'];
			$ArrList[$i]['order_box'] = $res->fields['order_box'];
			$ArrList[$i]['order_type'] = $res->fields['order_type'];
			$ArrList[$i]['ship_by'] = $res->fields['ship_by'];
			$ArrList[$i]['num_box'] = $res->fields['num_box'];
			$ArrList[$i]['item_typs_s'] = $res->fields['item_typs_s'];
			$ArrList[$i]['option'] = $this->_itemOption($res->fields['id']);
			$ArrList[$i]['status_text'] = $this->_convStatus('item',$res->fields['status']);
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$newsdate = date("d-m-Y H:i:s", strtotime($res->fields['create_dtm']));
			if($res->fields['update_dtm']!='0000-00-00 00:00:00'){
				$newsdate = date("d-m-Y H:i:s", strtotime($res->fields['update_dtm']));
			}
			$ArrList[$i]['update_dtm'] = $newsdate;
			$i++;
			$res->MoveNext();
		}
		//print_r($ArrList);
		return $ArrList;
	}
	private function _templateDataList(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$sql = "SELECT * FROM project_export_template_item WHERE template_id=".GF::quote($base->get('POST.template_id'));
 		$res = $db->Execute($sql);
 		$arrReturn = array();
 		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['master_id'] = $res->fields['master_id'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
   private function _exportExcel(){
		$base = Base::getInstance();
		$db = DB2::getInstance();

      $ids = $_SESSION['order_import'];//$base->get('POST.ids');
      $now_couuter = date('Y-m-d H:i:s');
      $member = new Member();

      $memberInfomation = $member->memberInfomation();

      $user_id_admin = $memberInfomation['id'];
		if(count($ids)>0){
			foreach($ids as $valccc){
                if($valccc!=''){
                    $result = $db->update("web_order",
                        array("export"=>'Y',"export_date"=>$now_couuter,"export_by"=>$user_id_admin),
                        array("id"=>$valccc)
                    );
                }  
         }
		}
        unset($_SESSION['order_import']);
      return true;

   }
	private function _exportExcel_backup(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

		$ids = $base->get('POST.ids');
		if(count($ids)==0){
			$ids = array();
		}
		foreach($ids as $valccc){

			$base->set('newid',$valccc);
			$itemList = $this->_itemListByExport();


			$dataList = $this->_templateDataList();

			if($itemList){
				require_once $base->get('BASEDIR').'/assets/phpexcel/PHPExcel.php';
				$objPHPExcel = new PHPExcel();

				$objPHPExcel->getProperties()->setCreator("TAOBAO CHINA CARGO")
								 ->setLastModifiedBy("TAOBAO CHINA CARGO")
								 ->setTitle("Office 2007 XLSX TAOBAO CHINA CARGO")
								 ->setSubject("Office 2007 XLSX TAOBAO CHINA CARGO")
								 ->setDescription("TAOBAO CHINA CARGO Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("TAOBAO CHINA CARGO");

				$objPHPExcel->getActiveSheet()->setCellValue("A1", "จำนวน สั่งซื้อจริง");
				$objPHPExcel->getActiveSheet()->setCellValue("B1", "ราคา สั่งซื้อจริง");
				$i = 1;
				$alldt = count($dataList);
				foreach($dataList as $key=>$dtlist){
					$mass = GF::masterdata($dtlist['master_id']);
					$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$key+2] . $i, $mass['master_name']);
				}

				$i = 2;
				$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
				foreach($itemList as $vals){

					foreach($dataList as $key=>$dtlist){
						$mass = GF::masterdata($dtlist['master_id']);

						$objPHPExcel->getActiveSheet()->setCellValue("A" . $i,"");
						$objPHPExcel->getActiveSheet()->setCellValue("B" . $i,"");
						$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$key+2] . $i, $vals[$mass['master_value']]);

						if($mass['master_value']=='product_image' && is_file($base->get('BASEDIR')."/uploads/product/".$vals['product_image'])){
							$filesize = filesize($base->get('BASEDIR')."/uploads/product/".$vals['product_image']);
							if($filesize>0){
								$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$key+2] . $i, '');
								$gdImage = imagecreatefromjpeg( $base->get('BASEDIR')."/uploads/product/".$vals['product_image']);
								$objDrawing->setName('Sample image');$objDrawing->setDescription('Sample image');
								$objDrawing->setImageResource($gdImage);
								$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
								$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
								$objDrawing->setHeight(100);
								$objDrawing->setWidth(100);
								$objDrawing->setCoordinates($alphabet[$key+2].$i);
								$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

								//$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$key+2] . $i, $base->get('BASEURL')."/uploads/product/".$vals['product_image']);
								$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(80);
							}else{
								$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$key+2] . $i, "No Image.");
							}

						}else{
							$objPHPExcel->getActiveSheet()->setCellValue($alphabet[$key+2] . $i, $vals[$mass['master_value']]);
						}

					}


					$i++;
				}
				//echo $i;


				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
				$objPHPExcel->getActiveSheet()->setTitle('My Customer');


				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				$sql = "SELECT order_number FROM web_order WHERE id=".GF::quote($valccc);
				$res = $db->Execute($sql);
				// Save Excel 2007 file
				//echo date('H:i:s') . " Write to Excel2007 format\n";
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$filenames = $res->fields['order_number'];//GF::randomNum(10);
				$strFileName = $base->get('BASEDIR')."/uploads/excel/".$filenames.".xlsx";
				$objWriter->save($strFileName);
				//echo $base->get('BASEURL')."/uploads/excel/".$filenames.".xlsx";
				$base->set('file_name',$filenames.".xlsx");
				$this->_uploadFtp();

				//return $base->get('BASEURL')."/uploads/excel/".$filenames.".xlsx";
			}

		}
		return $base->get('BASEURL')."/uploads/excel/2059031716.xlsx";

	}

	private function _uploadFtp(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$template_id = $base->get('POST.template_id');
		$file_name = $base->get('file_name');

		$sql = "SELECT * FROM project_export_template WHERE template_id=".GF::quote($template_id);
 		$res = $db->Execute($sql);
 		$arrReturn = array();

		$arrReturn['ftp_host'] = $res->fields['ftp_host'];
		$arrReturn['ftp_username'] = $res->fields['ftp_username'];
		$arrReturn['ftp_password'] = $res->fields['ftp_password'];
		$arrReturn['ftp_port'] = $res->fields['ftp_port'];
		$arrReturn['ftp_path'] = $res->fields['ftp_path'];

		$ftp_server = $arrReturn['ftp_host'];
		$ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
		$login = ftp_login($ftp_conn, $arrReturn['ftp_username'], $arrReturn['ftp_password']);

		$file = $base->get('BASEDIR')."/uploads/excel/".$file_name;

		// upload file
		if (ftp_put($ftp_conn, $arrReturn['ftp_path'].$file_name, $file, FTP_ASCII)){

		}
		else{

		}
		ftp_close($ftp_conn);
		return true;

	}
	private function _orderSet(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();

		$orderid = $base->get('order_id');

		$sql = "SELECT * FROM web_order WHERE id=".GF::quote($orderid);
		$res = $db->Execute($sql);


		return  $res->fields;

	}
	private function _genOrderNumber(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "SELECT * FROM web_order WHERE id=".GF::quote($base->get('orderid'));
		$res = $db->Execute($sql);

		if($res->fields['order_number']!='-'){
			exit();
		}

		$datenow = date('Ym');
		$sql = "SELECT * FROM web_order_number WHERE 1=1 ORDER BY id DESC LIMIT 1";
		$res = $db->Execute($sql);

		$i = 0;
		if($res->fields['id']==''){
			$i = 0;
		}else{
			$i = $res->fields['id'];
		}
		$new_id = intval($i)+1;

		$tt_id = str_pad($new_id, 5, "0", STR_PAD_LEFT);
		//$tt_id = $datenow.$tt_id;

		$sql = "INSERT INTO web_order_number (
												order_id,
												order_number,
												create_dtm
												)VALUES(
												".GF::quote($base->get('orderid')).",
												".GF::quote($tt_id).",
												NOW()
												)";
		$res = $db->Execute($sql);
		$sql = "UPDATE web_order SET order_number=".GF::quote($tt_id)." WHERE id=".GF::quote($base->get('orderid'));
		$res = $db->Execute($sql);

	}

    private function _getMoneyFromWallet(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$orderid = $base->get('GET.orderid');
 		$mywallet = $memberInfomation['wallet'];
 		$priceOrder = $base->get('GET.order_price');

 		if($priceOrder>=$mywallet){

         $member_old_info = GF::memberinfo($user_id);
         $oldWallet = $member_old_info['wallet'];

         $old_balance = $oldWallet;
         $new_balance = $oldWallet-$mywallet;

         // Weerasak 25-04-2566
			// $sql = "UPDATE project_user SET
			// 							  wallet='0',
			// 							  update_dtm=NOW()
			// 								WHERE id=".GF::quote($user_id);
			// $res = $db->Execute($sql);

         $base->set('_ids',$orderid);
         $orderInfo = $this->_orderInfo();

         $orernumber = $orderInfo['order_number'];
         $test_odr = 'ชำระค่าสั่งซื้อ ORDER ID :'.$orernumber;

			$sql ="INSERT INTO project_wallet_log(
	 												user_id,
	 												wallet,
                                                    before_balance,
                                                    after_balance,
	 												wallet_type,
	 												wallet_desc,
                                                    wallet_ref,
                                                    wallet_ref_type,
	 												create_dtm,
	 												create_by
	 												)
	 												VALUES(
	 												".GF::quote($user_id).",
	 												".GF::quote($mywallet).",
                                                    ".GF::quote($old_balance).",
                                                    ".GF::quote($new_balance).",
	 												'OU',
                                                    ".GF::quote($test_odr).",
                                                    ".GF::quote($orderid).",
                                                    '1',
	 												NOW(),
	 												".GF::quote($user_id)."
	 												)";
	 		$res = $db->Execute($sql);

			$order_balance = $priceOrder-$mywallet;
			$sql = "UPDATE web_order SET order_price=order_price+".GF::quote($mywallet)." WHERE id=".GF::quote($orderid);
			$res = $db->Execute($sql);
		}else{

         $member_old_info = GF::memberinfo($user_id);
         $oldWallet = $member_old_info['wallet'];

         $old_balance = $oldWallet;
         $new_balance = $oldWallet-$priceOrder;

			$wallet_balance = $mywallet-$priceOrder;

         // Weerasak 25-04-2566
			// $sql = "UPDATE project_user SET
			// 							  wallet='".$wallet_balance."',
			// 							  update_dtm=NOW()
			// 								WHERE id=".GF::quote($user_id);
			// $res = $db->Execute($sql);

         $base->set('_ids',$orderid);
         $orderInfo = $this->_orderInfo();

         $orernumber = $orderInfo['order_number'];

         if($orderInfo['status']>=3){
            $sql = "UPDATE web_order SET order_price=order_price+".GF::quote($priceOrder)." WHERE id=".GF::quote($orderid);
   			$res = $db->Execute($sql);
         }else{
            $base->set('orderid',$orderid);
            $this->_genOrderNumber();

            $base->set('_ids',$orderid);
            $orderInfo = $this->_orderInfo();
            $orernumber = $orderInfo['order_number'];

            $sql = "UPDATE web_order SET order_price=order_price+".GF::quote($priceOrder).",status='3' WHERE id=".GF::quote($orderid);
   			$res = $db->Execute($sql);
         }

         $test_odr = 'ชำระค่าสั่งซื้อ ORDER ID :'.$orernumber;
			$sql ="INSERT INTO project_wallet_log(
	 												user_id,
	 												wallet,
                                                    before_balance,
                                                    after_balance,
	 												wallet_type,
	 												wallet_desc,
                                                    wallet_ref,
                                                    wallet_ref_type,
	 												create_dtm,
	 												create_by
	 												)
	 												VALUES(
	 												".GF::quote($user_id).",
	 												".GF::quote($priceOrder).",
                                                    ".GF::quote($old_balance).",
                                                    ".GF::quote($new_balance).",
	 												'OU',
                                                    ".GF::quote($test_odr).",
                                                    ".GF::quote($orderid).",
                                                    '1',
	 												NOW(),
	 												".GF::quote($user_id)."
	 												)";
	 		$res = $db->Execute($sql);





		}


	}

	private function _getMoneyFromWallet_backup_21_05_2017(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$orderid = $base->get('GET.orderid');
 		$mywallet = $memberInfomation['wallet'];
 		$priceOrder = $base->get('GET.order_price');

 		if($priceOrder=='X'){ //if($priceOrder>=$mywallet){

         $member_old_info = GF::memberinfo($user_id);
         $oldWallet = $member_old_info['wallet'];

         $old_balance = $oldWallet;
         $new_balance = $oldWallet-$mywallet;

         // Weerasak 25-04-2566
			// $sql = "UPDATE project_user SET
			// 							  wallet='0',
			// 							  update_dtm=NOW()
			// 								WHERE id=".GF::quote($user_id);
			// $res = $db->Execute($sql);

         $base->set('_ids',$orderid);
         $orderInfo = $this->_orderInfo();

         $orernumber = $orderInfo['order_number'];
         $test_odr = 'ชำระค่าสั่งซื้อ ORDER ID :'.$orernumber;

			$sql ="INSERT INTO project_wallet_log(
	 												user_id,
	 												wallet,
                                                    before_balance,
                                                    after_balance,
	 												wallet_type,
	 												wallet_desc,
                                                    wallet_ref,
                                                    wallet_ref_type,
	 												create_dtm,
	 												create_by
	 												)
	 												VALUES(
	 												".GF::quote($user_id).",
	 												".GF::quote($mywallet).",
                                                    ".GF::quote($old_balance).",
                                                    ".GF::quote($new_balance).",
	 												'OU',
                                                    ".GF::quote($test_odr).",
                                                    ".GF::quote($orderid).",
                                                    '1',
	 												NOW(),
	 												".GF::quote($user_id)."
	 												)";
	 		$res = $db->Execute($sql);

			$order_balance = $priceOrder-$mywallet;
			$sql = "UPDATE web_order SET order_price=order_price+".GF::quote($mywallet)." WHERE id=".GF::quote($orderid);
			$res = $db->Execute($sql);
		}else{

         $member_old_info = GF::memberinfo($user_id);
         $oldWallet = $member_old_info['wallet'];

         $old_balance = $oldWallet;
         $new_balance = $oldWallet-$priceOrder;

			$wallet_balance = $mywallet-$priceOrder;

         // Weerasak 25-04-2566
			// $sql = "UPDATE project_user SET
			// 							  wallet='".$wallet_balance."',
			// 							  update_dtm=NOW()
			// 								WHERE id=".GF::quote($user_id);
			// $res = $db->Execute($sql);

         $base->set('_ids',$orderid);
         $orderInfo = $this->_orderInfo();

         $orernumber = $orderInfo['order_number'];

         if($orderInfo['status']>=3){
            $sql = "UPDATE web_order SET order_price=order_price+".GF::quote($priceOrder)." WHERE id=".GF::quote($orderid);
   			$res = $db->Execute($sql);
         }else{
            $base->set('orderid',$orderid);
            $this->_genOrderNumber();

            $base->set('_ids',$orderid);
            $orderInfo = $this->_orderInfo();
            $orernumber = $orderInfo['order_number'];

            $sql = "UPDATE web_order SET order_price=order_price+".GF::quote($priceOrder).",status='3' WHERE id=".GF::quote($orderid);
   			$res = $db->Execute($sql);
         }

         $test_odr = 'ชำระค่าสั่งซื้อ ORDER ID :'.$orernumber;
			$sql ="INSERT INTO project_wallet_log(
	 												user_id,
	 												wallet,
                                                    before_balance,
                                                    after_balance,
	 												wallet_type,
	 												wallet_desc,
                                                    wallet_ref,
                                                    wallet_ref_type,
	 												create_dtm,
	 												create_by
	 												)
	 												VALUES(
	 												".GF::quote($user_id).",
	 												".GF::quote($priceOrder).",
                                                    ".GF::quote($old_balance).",
                                                    ".GF::quote($new_balance).",
	 												'OU',
                                                    ".GF::quote($test_odr).",
                                                    ".GF::quote($orderid).",
                                                    '1',
	 												NOW(),
	 												".GF::quote($user_id)."
	 												)";
	 		$res = $db->Execute($sql);





		}


	}
   private function _uploadTempOrder(){
      $base = Base::getInstance();

      if(!empty($base->get('POST.filetemp'))){

         $tmp_file = $base->get('POST.filetemp');

         $base->set('BASE64_STR',$tmp_file);

         return $base->get('BASEURL')."/uploads/temp/".$this->base64_to_jpeg();
      }
   }
   private function base64_to_jpeg() {

      $base = Base::getInstance();
      $fileReturn = GF::randomNum(25).".jpg";

       $ifp = fopen($base->get('BASEDIR').'/uploads/temp/'.$fileReturn, "wb");

       $data = explode(',', $base->get('BASE64_STR'));

       fwrite($ifp, base64_decode($data[1]));
       fclose($ifp);

       return $fileReturn;
   }
	private function _uploadTemp(){
		$base = Base::getInstance();
		if(!empty($_FILES['ordername']['name'])){
			$picname = $_FILES['ordername']['name'];

			$random = GF::randomNum(25);

			$type = explode('.',$picname);
			$dest_picname_o = $base->get('BASEDIR')."/uploads/temp/".$random.".".end($type);

			$tmp_file = $_FILES['ordername']['tmp_name'];
			@copy($tmp_file, $dest_picname_o);
			$payment_slip = $random.".".end($type);

			return $payment_slip;

		}
	}
	private function _importOrderExcel(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$filename = $this->_uploadTemp();

		require_once $base->get('BASEDIR').'/assets/phpexcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$inputFileName = $base->get('BASEDIR')."/uploads/temp/".$filename;
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);



		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
	    $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
		    if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
		        ++$r;
		        foreach($headingsArray as $columnKey => $columnHeading) {
		            $namedDataArray[$r][$columnKey] = $dataRow[$row][$columnKey];
		        }
		    }
	    }
	    unlink($inputFileName);
	    $dataCheck = true;
	    if(count($namedDataArray)>0){
			foreach($namedDataArray as $key=>$list){
				$base->set('tracking_id',$list['A']);
				$base->set('status',$list['F']);

				$qty = $list['A'];
				$price = $list['B'];
				$product_id = $list['C'];

				$base->set('p_id',$product_id);
				$result = $this->_checkRefund();
				if(!$result){
					$dataCheck = false;
				}
			}
		}else{
			$dataCheck = false;
		}
		if($dataCheck){

			foreach($namedDataArray as $key=>$list){
				$base->set('tracking_id',$list['A']);
				$base->set('status',$list['F']);

				$qty = $list['A'];
				$price = $list['B'];
				$product_id = $list['C'];

				$sql = "UPDATE web_order_item SET product_buy_rmb=".GF::quote($price).",product_buy_qty=".GF::quote($qty)." WHERE id=".GF::quote($product_id);
				echo $sql;
				$res = $db->Execute($sql);
			}

			return true;
		}else{
			return false;
		}
	}
	private function _checkRefund(){
		$base = Base::getInstance();
		$db = DB::getInstance();
		$product_id = $base->get('p_id');

		$sql = "SELECT order_id FROM `web_order_item` WHERE id=".GF::quote($product_id);
		$res = $db->Execute($sql);
		//echo $sql;
		if($res->fields['order_id']!=''){
			$sql2 = "SELECT refund FROM `web_order` WHERE id=".GF::quote($res->fields['order_id']);
			$res2 = $db->Execute($sql2);
			if($res2->fields['refund']=='N'){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}
	private function _notiBuyStatus(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order SET status='2' WHERE id=".GF::quote($base->get('_ids'));
		$res = $db->Execute($sql);
		return true;

	}
	private function _refundToWallet(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$orderid = $base->get('_ids');
 		$mywallet = $memberInfomation['wallet'];

 		$orderInfo = $this->_orderInfo();

 		$totals = (($orderInfo['countprice']['product_price_rmb']*$orderInfo['rate'])+$orderInfo['order_pay_ship'])-$orderInfo['order_discount'];
 		$totals = $totals+$orderInfo['countprice']['product_price_thb'];

 		$refund = ($orderInfo['countprice']['product_buy_rmb']*$orderInfo['rate'])-$totals;
		$ttpay = ($totals-$orderInfo['order_price']);
		$total_uppay = $ttpay+$refund;

		$wallet_balance = $mywallet+abs($total_uppay);

        $member_old_info = GF::memberinfo($user_id);
        $oldWallet = $member_old_info['wallet'];

        $old_balance = $oldWallet;
        $new_balance = $oldWallet+$total_uppay;

      //Weerasak 12-03-2566
      //ปิดใช้งานเงินคืนเข้า wallet
		// $sql = "UPDATE project_user SET
		// 							  wallet='".$wallet_balance."',
		// 							  update_dtm=NOW()
		// 								WHERE id=".GF::quote($user_id);
		// $res = $db->Execute($sql);



		$sql ="INSERT INTO project_wallet_log(
 												user_id,
 												wallet,
                                                before_balance,
                                                after_balance,
 												wallet_type,
 												wallet_desc,
                                                wallet_ref,
                                                wallet_ref_type,
 												create_dtm,
 												create_by
 												)
 												VALUES(
 												".GF::quote($user_id).",
 												".GF::quote(abs($total_uppay)).",
                                                ".GF::quote($old_balance).",
                                                ".GF::quote($new_balance).",
 												'IN',
                                                'เงินคืนค่าสินค้า',
                                                ".GF::quote($orderid).",
                                                '3',
 												NOW(),
 												".GF::quote($user_id)."
 												)";
 		$res = $db->Execute($sql);

		$sql = "UPDATE web_order SET refund='Y' WHERE id=".GF::quote($orderid);
      //echo $sql;
        $res = $db->Execute($sql);
        
        // $mail = Mailer::getInstance();
        
        // $mail->userid = $user_id;
        // $mail->Subject = "แจ้งคืนเงิน";
        // $members = GF::memberinfo($user_id);
        // $dataHtml = GF::mailtemplate('15');
        // $dataHtml = str_replace("{CUST_NAME}",$members['user_name'],$dataHtml);
        // $dataHtml = str_replace("{ORDER_NO}",$orderInfo['order_number'],$dataHtml);
        // $dataHtml = str_replace("{PAYMENT_TOTAL}",abs($total_uppay),$dataHtml);
        // $mail->msg = $dataHtml;
        // $mail->SendingToNoti();

        $members = GF::memberinfo($user_id);
        $link = $base->get('BASEURL').'/#/buyship/refundlist/';
        $dataReplace = array(
            "{CUST_NAME}"=>$members['user_name'],
            "{PAYMENT_TOTAL}"=>abs($total_uppay),
            "{ORDER_NO}"=>$orderInfo['order_number'],
            "{LINK}"=>$link
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('9',$members['ref_id']);
        $mail->msgHTML = GF::mailtemplate('9',$dataReplace,$members['ref_id']);
        $mail->addAddressEmail = $members['user_email'];
        $mail->addAddressName = $members['user_name'];
        $mail->sendMail();

	}
   public function _itemInfo(){
        $base = Base::getInstance();
        $db = DB::getInstance();

        $orderid = $base->get('_ids');

        $sql = "SELECT * FROM web_order_item WHERE id=".GF::quote($orderid);
        $res = $db->Execute($sql);

        $res->fields['img_path'] = $base->get('BASEURL')."/uploads/product/".$res->fields['product_image'];

        return  $res->fields;

   }
   private function _refundInfo(){
        $base = Base::getInstance();
        $db = DB::getInstance();

        $orderid = $base->get('_ids');

        $sql = "SELECT * FROM project_item_refund WHERE ref_id=".GF::quote($orderid);
        $res = $db->Execute($sql);

        $base->set('_ids',$res->fields['item_id']);
        $res->fields['item_info'] = $this->_itemInfo();
        $base->set('_ids',$res->fields['item_info']['order_id']);
        $res->fields['order_info'] = $this->_orderInfo();

        return  $res->fields;

   }
   private function _refundList(){
     $base = Base::getInstance();
     $db = DB::getInstance();
     $db2 = DB2::getInstance();

     $member = new Member();

        $cond_hold = '';
        if($base->get('_hold_')=='A'){
            $olddate = date('Y-m-d H:i:s',strtotime("-15 days"));
            $cond_hold = " AND (status='Wait' OR status='Progress' OR status='Ship') AND (update_dtm<='".$olddate."')";
        }

        $memberInfomation = $member->memberInfomation();

        $user_id = $memberInfomation['id'];
        $cond_search= '';
        $agency =  $memberInfomation['agency'];
        if($agency=='A'){
           $list_ref = GF::refgroup();
           if(count($list_ref)>0){
             //GF::print_r($list_ref);
             $ref_str = implode(',', $list_ref);
             $cond_search .= " AND create_by IN(".$ref_str.") ";
           }else{
             $cond_search .= " AND create_by ='X' ";
           }
        }

        if($_SESSION['odr_ref']['order_number']!=''){
          $resOrderNumber = $db2->get('web_order',"*",array("order_number"=>$_SESSION['odr_ref']['order_number']));
          if($resOrderNumber['id']!=''){
             $resOrderNumber2 = $db2->select('web_order_item',"*",array("order_id"=>$resOrderNumber['id']));

             if(count($resOrderNumber2)>0){
                $xArr = array();
                $c=0;
                foreach ($resOrderNumber2 as $key => $value) {
                   $xArr[$c] = $value['id'];
                   $c++;
                }
                $whereIn = implode(',', $xArr);
                $cond_search .= " AND item_id IN(".$whereIn.") ";

             }else{
                $cond_search .= " AND item_id='X' ";
             }
          }else{
             $cond_search .= " AND item_id='X' ";
          }
        }

        if($_SESSION['odr_ref']['user_code']!=''){
           $user_my_id = GF::user_id($_SESSION['odr_ref']['user_code']);
           if($user_my_id!=''){
             $cond_search .= " AND create_by=".GF::quote($user_my_id)." ";
          }else{
             $cond_search .= " AND create_by IN(0) ";
          }
        }

        if($_SESSION['odr_ref']['status']!=''){
           $cond_search .= " AND status=".GF::quote($_SESSION['odr_ref']['status'])." ";
        }

        $export_data = $base->get('_exp');
        if($export_data!=''){
           $cond_search .= " AND export='N' ";
        }

        $export_id = $base->get('_exp_ref');
        if($export_id!=''){
           $cond_search .= " AND ref_id IN(".$export_id.") ";
        }


    $sql = "SELECT * FROM project_item_refund WHERE status!='Delete' ".$cond_search.$cond_hold."  ORDER BY ref_id DESC";
     //$sql = "SELECT * FROM web_order WHERE  active_status='O' ".$cond." ORDER BY update_dtm DESC,create_dtm DESC";
     //echo $sql;

     $res = $db->Execute($sql);
     $count = $res->NumRows();

        $base->set('allPage',ceil($count/20));

     $page = $base->get('GET.page');

     $numr = 20;
     if($export_data!='' || $export_id!=''){
        $numr = 999999;
     }
     $start = "";
     $end = "";
     if($page==1||empty($page)){
        $start = 0;
        $page = 1;
        //$end = $numr;
     }else{
        //$end = ($page*$numr);
        $start = ($page*$numr)-$numr;
     }
      $condNum = " LIMIT ".$start.",".$numr;

      $base->set('currpage',$page);

     $res = $db->Execute($sql.$condNum);
     //echo $sql;
    // $res = $db->Execute($sql);
     $i=0;
     $ArrList = array();
     while(!$res->EOF){
        $ArrList[$i]['ref_id'] = $res->fields['ref_id'];
        $ArrList[$i]['item_id'] = $res->fields['item_id'];
        $ArrList[$i]['ref_refund'] = $res->fields['ref_refund'];
        $ArrList[$i]['ref_item'] = $res->fields['ref_item'];
        $ArrList[$i]['ref_file'] = $res->fields['ref_file'];
        $ArrList[$i]['ref_file2'] = $res->fields['ref_file2'];
        $ArrList[$i]['ref_rmb'] = $res->fields['ref_rmb'];
        $ArrList[$i]['ref_thb'] = $res->fields['ref_thb'];
        $ArrList[$i]['ref_rate'] = $res->fields['ref_rate'];
        $ArrList[$i]['ref_comment'] = $res->fields['ref_comment'];
        $ArrList[$i]['ref_admin'] = $res->fields['ref_admin'];
        $ArrList[$i]['status'] = $res->fields['status'];
        //$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
        $ArrList[$i]['order_number'] = $this->_getOrderNumber($res->fields['item_id']);
        $ArrList[$i]['create_by'] = $res->fields['create_by'];
        $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
        $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
        $ArrList[$i]['export'] = $res->fields['export'];
        $ArrList[$i]['export_date'] = $res->fields['export_date'];
        $base->set('_ids',$res->fields['item_id']);
        $ArrList[$i]['item_info'] = $this->_itemInfo();
        $base->set('_ids',$ArrList[$i]['item_info']['order_id']);
        $ArrList[$i]['order_info'] = $this->_orderInfo();
        $i++;
        $res->MoveNext();
     }
     //print_r($ArrList);
     return $ArrList;
  }
	private function _refundListExcel(){
     $base = Base::getInstance();
     $db = DB::getInstance();
     $db2 = DB2::getInstance();

     $member = new Member();
//GF::print_r($base->get('id'));
        //$cond_hold = "AND (status='Progress')";
        if($base->get('_hold_')=='A'){
            $olddate = date('Y-m-d H:i:s',strtotime("-15 days"));
            $cond_hold = " AND (status='Progress') AND (update_dtm<='".$olddate."')";
        }

        $memberInfomation = $member->memberInfomation();

        $user_id = $memberInfomation['id'];
        $cond_search= '';
        $agency =  $memberInfomation['agency'];
        if($agency=='A'){
           $list_ref = GF::refgroup();
           if(count($list_ref)>0){
             //GF::print_r($list_ref);
             $ref_str = implode(',', $list_ref);
             $cond_search .= " AND create_by IN(".$ref_str.") ";
           }else{
             $cond_search .= " AND create_by ='X' ";
           }
        }

        if($_SESSION['odr_ref']['order_number']!=''){
          $resOrderNumber = $db2->get('web_order',"*",array("order_number"=>$_SESSION['odr_ref']['order_number']));
          if($resOrderNumber['id']!=''){
             $resOrderNumber2 = $db2->select('web_order_item',"*",array("order_id"=>$resOrderNumber['id']));

             if(count($resOrderNumber2)>0){
                $xArr = array();
                $c=0;
                foreach ($resOrderNumber2 as $key => $value) {
                   $xArr[$c] = $value['id'];
                   $c++;
                }
                $whereIn = implode(',', $xArr);
                $cond_search .= " AND item_id IN(".$whereIn.") ";

             }else{
                $cond_search .= " AND item_id='X' ";
             }
          }else{
             $cond_search .= " AND item_id='X' ";
          }
        }

        if($_SESSION['odr_ref']['user_code']!=''){
           $user_my_id = GF::user_id($_SESSION['odr_ref']['user_code']);
           if($user_my_id!=''){
             $cond_search .= " AND create_by=".GF::quote($user_my_id)." ";
          }else{
             $cond_search .= " AND create_by IN(0) ";
          }
        }

        if($_SESSION['odr_ref']['status']!='' &&  $_SESSION['odr_ref']['status']!="Wait" ){
			
			
		
           	 	$cond_search .= " AND status=".GF::quote($_SESSION['odr_ref']['status'])." ";
			
        }

        $export_data = $base->get('_exp');
        if($export_data!=''){
           $cond_search .= " AND export='N' ";
        }

        $export_id = $base->get('_exp_ref');
        if($export_id!=''){
           $cond_search .= " AND ref_id IN(".$export_id.") ";
        }


    $sql = "SELECT * FROM project_item_refund WHERE status!='Delete' ".$cond_search.$cond_hold."  ORDER BY ref_id DESC";
     //$sql = "SELECT * FROM web_order WHERE  active_status='O' ".$cond." ORDER BY update_dtm DESC,create_dtm DESC";
     //echo $sql;

     $res = $db->Execute($sql);
     $count = $res->NumRows();

        $base->set('allPage',ceil($count/20));

     $page = $base->get('GET.page');

     $numr = 20;
     if($export_data!='' || $export_id!=''){
        $numr = 999999;
     }
     $start = "";
     $end = "";
     if($page==1||empty($page)){
        $start = 0;
        $page = 1;
        //$end = $numr;
     }else{
        //$end = ($page*$numr);
        $start = ($page*$numr)-$numr;
     }
      $condNum = " LIMIT ".$start.",".$numr;

      $base->set('currpage',$page);

     $res = $db->Execute($sql.$condNum);
     //echo $sql;
	 //exit;
    // $res = $db->Execute($sql);
     $i=0;
     $ArrList = array();
     while(!$res->EOF){
        $ArrList[$i]['ref_id'] = $res->fields['ref_id'];
        $ArrList[$i]['item_id'] = $res->fields['item_id'];
        $ArrList[$i]['ref_refund'] = $res->fields['ref_refund'];
        $ArrList[$i]['ref_item'] = $res->fields['ref_item'];
        $ArrList[$i]['ref_file'] = $res->fields['ref_file'];
        $ArrList[$i]['ref_file2'] = $res->fields['ref_file2'];
        $ArrList[$i]['ref_rmb'] = $res->fields['ref_rmb'];
        $ArrList[$i]['ref_thb'] = $res->fields['ref_thb'];
        $ArrList[$i]['ref_rate'] = $res->fields['ref_rate'];
        $ArrList[$i]['ref_comment'] = $res->fields['ref_comment'];
        $ArrList[$i]['ref_admin'] = $res->fields['ref_admin'];
        $ArrList[$i]['status'] = $res->fields['status'];
        //$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
        $ArrList[$i]['order_number'] = $this->_getOrderNumber($res->fields['item_id']);
        $ArrList[$i]['create_by'] = $res->fields['create_by'];
        $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
        $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
        $ArrList[$i]['export'] = $res->fields['export'];
        $ArrList[$i]['export_date'] = $res->fields['export_date'];
        $base->set('_ids',$res->fields['item_id']);
        $ArrList[$i]['item_info'] = $this->_itemInfo();
        $base->set('_ids',$ArrList[$i]['item_info']['order_id']);
        $ArrList[$i]['order_info'] = $this->_orderInfo();
        $i++;
        $res->MoveNext();
     }
     //print_r($ArrList);
     return $ArrList;
  }
  private function _exportRefund(){

     $base = Base::getInstance();
     $db = DB::getInstance();

     //$base->set('_exp','Y');
     $dataList = $this->_refundList();
     if(count($dataList)>0){
        $dataid = '';
        $xc = array();
        $i=0;
        foreach($dataList as $vals){
           $xc[$i] = $vals['ref_id'];
           $i++;
        }
        $dataid = implode(',', $xc);
		
		if($_SESSION['odr_ref']['status']=="Wait"){
			$sql = "UPDATE project_item_refund SET export='Y',status='Progress' ,export_date=NOW() WHERE ref_id IN(".$dataid.")";
		}else{
        	$sql = "UPDATE project_item_refund SET export='Y',export_date=NOW() WHERE ref_id IN(".$dataid.")";
		}
        $res = $db->Execute($sql);

        return $dataid;
     }else{
        return '0';
     }

  }
  private function _cancelRefund(){

     $base = Base::getInstance();
     $db = DB::getInstance();

     $sql = "UPDATE project_item_refund SET status='Delete' WHERE ref_id=".GF::quote($base->get('GET.ref_id'));
     $res = $db->Execute($sql);

     $sql = "UPDATE web_order_item SET ref_id='0' WHERE id=".GF::quote($base->get('GET.item_id'));
     $db->Execute($sql);
  }
  public function _getOrderNumber($order_id){
     $base = Base::getInstance();
     $db = DB::getInstance();

     $sql = "SELECT order_id FROM web_order_item WHERE id=".GF::quote($order_id);
     $res = $db->Execute($sql);

     $sql2 = "SELECT order_number FROM web_order WHERE id=".GF::quote($res->fields['order_id']);
     $res2 = $db->Execute($sql2);

     return $res2->fields[order_number];
  }
  private function _updateRefund(){

     $base = Base::getInstance();
     $db = DB::getInstance();

     $member = new Member();


     $memberInfomation = $member->memberInfomation();

     $user_id = $memberInfomation['id'];

     $ref_baht = 0;
     if($base->get('POST.ref_rate')!='0' && $base->get('POST.ref_rmb')!='0'){
        $ref_baht = $base->get('POST.ref_rate')*$base->get('POST.ref_rmb');
     }

     $payment_slip = NULL;

     if(!empty($_FILES['ref_file2']['name'])){
        $picname = $_FILES['ref_file2']['name'];

        $random = GF::randomNum(25);

        $type = explode('.',$picname);
        $dest_picname_o = $base->get('BASEDIR')."/uploads/refund/".$random.".".end($type);

        $tmp_file = $_FILES['ref_file2']['tmp_name'];
        @copy($tmp_file, $dest_picname_o);
        $payment_slip = $random.".".end($type);


     }

     $tt_ref = (int)$base->get('POST.tt_all')-(int)$base->get('POST.ref_refund');


     $sql = "UPDATE project_item_refund SET
                                          status=".GF::quote($base->get('POST.status')).",
                                          ref_admin=".GF::quote($base->get('POST.ref_admin')).",
                                          ref_rate=".GF::quote($base->get('POST.ref_rate')).",
                                          ref_rmb=".GF::quote($base->get('POST.ref_rmb')).",
                                          ref_refund=".GF::quote($tt_ref).",
                                          ref_thb=".GF::quote($ref_baht).",
                                          update_dtm=NOW(),
                                          update_by=".GF::quote($user_id)."
                                          WHERE ref_id=".GF::quote($base->get('POST.ref_id'));
     $res = $db->Execute($sql);
     if($payment_slip!=NULL && $payment_slip!=''){
        $sql = "UPDATE project_item_refund SET

                                            ref_file2=".GF::quote($payment_slip)."
                                            WHERE ref_id=".GF::quote($base->get('POST.ref_id'));
       $res = $db->Execute($sql);
     }
     return true;

  }
  private function _refundToWalletByREF(){
     $base = Base::getInstance();
     $db = DB::getInstance();
     $db2 = DB2::getInstance();

     $member = new Member();


     $memberInfomation = $member->memberInfomation();

     $user_id = $memberInfomation['id'];

     $orderid = $base->get('_ids');
     //echo $orderid;
     $mywallet = $memberInfomation['wallet'];

     $refundInfoo = $this->_refundInfo();

     $orderInfo = $this->_orderInfo();

      // $totals = (($orderInfo['countprice']['product_price_rmb']*$orderInfo['rate'])+$orderInfo['order_pay_ship'])-$orderInfo['order_discount'];
	  
     //  $totals = $totals+$orderInfo['countprice']['product_price_thb'];
     //
   //   $refund = ($orderInfo['countprice']['product_buy_rmb']*$orderInfo['rate'])-$totals;
   //   $ttpay = ($totals-$orderInfo['order_price']);
   //$total_uppay = $ttpay+$refund;

	$total_uppay = $refundInfoo['ref_thb'];
	
	
    $member_old_info = GF::memberinfo($refundInfoo['create_by']);
    $oldWallet = $member_old_info['wallet'];

    $old_balance = $oldWallet;
    $new_balance = $oldWallet+$refundInfoo['ref_thb'];

    $base->set('user_id',$refundInfoo['create_by']);
    $userInfo = $member->memberInfomationByID();

    //$db2->insert("test",array("value"=>$userInfo['bank_id']));
    //Weerasak 12-03-2566
    //ปิดใช้งานเงินคืนเข้า wallet
   //    $sql = "UPDATE project_user SET
   //                         wallet=wallet+'".$refundInfoo['ref_thb']."',
   //                         update_dtm=NOW()
   //                           WHERE id=".GF::quote($refundInfoo['create_by']);
   //                           //echo $sql;
   //  $res = $db->Execute($sql);

   
   $last_id = $db2->insert("project_withdraw",array(
      "status"=>'R',
      "create_dtm"=>date('Y-m-d H:i:s'),
      "user_id"=> $refundInfoo['create_by'],
      "wd_total"=>$refundInfoo['ref_thb'],
      "wd_wallet"=>$refundInfoo['ref_thb'],
      "wd_bank"=> $userInfo['bank_id'],
      "wd_number"=> $userInfo['user_bookbank'],
      "wd_name"=> $userInfo['user_name'],
      "wd_message"=>"ถอนเงิน",
      "create_by"=>$user_id,
      "update_dtm"=>date('Y-m-d H:i:s'),
      "update_by"=>$user_id,			 
      "order_id"=>$orderInfo['id']
     ));
     //END EDIT
      $sql ="INSERT INTO project_wallet_log(
                                   user_id,
                                   wallet,
                                   before_balance,
                                   after_balance,
                                   wallet_type,
                                   wallet_desc,
                                   wallet_ref,
                                   wallet_ref_type,
                                   create_dtm,
                                   create_by
                                   )
                                   VALUES(
                                   ".GF::quote($refundInfoo['create_by']).",
                                   ".GF::quote(abs($refundInfoo['ref_thb'])).",
                                   ".GF::quote($old_balance).",
                                   ".GF::quote($new_balance).",
                                   'IN',
                                   'เงินคืนค่าสินค้า Refund',
                                   ".GF::quote($refundInfoo['ref_id']).",
                                   '7',
                                   NOW(),
                                   ".GF::quote($user_id)."
                                   )";
    $res = $db->Execute($sql);

    $sql = "UPDATE project_item_refund SET status='Refunded' WHERE ref_id=".$refundInfoo['ref_id'];
    $res = $db->Execute($sql);

        $members = GF::memberinfo($refundInfoo['create_by']);
        $link = $base->get('BASEURL').'/#/buyship/refundlist/';
        $dataReplace = array(
            "{CUST_NAME}"=>$members['user_name'],
            "{PAYMENT_TOTAL}"=>abs($total_uppay),
            "{ORDER_NO}"=>$orderInfo['order_number'],
            "{LINK}"=>$link
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('9',$members['ref_id']);
        $mail->msgHTML = GF::mailtemplate('9',$dataReplace,$members['ref_id']);
        $mail->addAddressEmail = $members['user_email'];
        $mail->addAddressName = $members['user_name'];
        $mail->sendMail();

  }
  private function _getshipItem(){

     $base = Base::getInstance();
     $db = DB2::getInstance();

     $orer_id = $base->get('_ids');
     $result = $db->select("web_order_item_ship","*",array("order_id"=>$orer_id));

     $resultReturn = array();

     $resultReturn['keyname'] = array();
     $resultReturn['data'] = array();

     if(count($result)>0){
        $i = 0;
        foreach($result as $key=>$vals){
           $resultReturn['data'][$i] = $vals;
           $resultReturn['keyname'][$i] = $vals['saller_id'];
           $i++;
        }
     }
   //   GF::print_r($resultReturn['keyname']);
   // $keysearch = array_search('1',$resultReturn['keyname']);
   // echo $keysearch."dd";
   // if($keysearch!==false){
   //    GF::print_r($resultReturn['data'][$keysearch]);
   // }
     return $resultReturn;
  }
  private function _addshipItem(){

    //Edit by Weerasak 29-10-2564
     $base = Base::getInstance();
     $db = DB2::getInstance();
     $amount=0;  
     $discount_rmb=0;
     $shipping_rmb=0;

    $amount = $base->get('GET.value');
    $discount_rmb = $base->get('GET.discount');
    $shipping_rmb = $base->get('GET.ship');
    $ship_thb = $shipping_rmb * $base->get('GET.rate');
     $db->insert("web_order_item_ship",array(
        "order_id"=>$base->get('GET.order_id'),
        "saller_id"=>$base->get('GET.saller_id'),
        "price_rmb"=>$shipping_rmb,
        "price_thb"=>$ship_thb,
        "saller_amount_rmb"=>$amount,
        "saller_discount_rmb"=>$discount_rmb,
        "saller_shipping_rmb"=>$shipping_rmb
     ));

      echo $db->last_query();
  }
  private function _updateshipItem(){

     $base = Base::getInstance();
     $db = DB2::getInstance();

     $price_rmb = $base->get('GET.value');
     $price_thb = $base->get('GET.value')*$base->get('GET.rate');

     $db->update("web_order_item_ship",array(
        "price_rmb"=>$price_rmb,
        "price_thb"=>$price_thb
     ),array(
        "item_id"=>$base->get('GET.id')
     ));

  }
  private function _sendOrderToCN(){

     $base = Base::getInstance();
     $db = DB2::getInstance();
		$db2 = DB::getInstance();
	//	$order_ids[0]=31;
     $order_ids = $_SESSION['order_chinarorder'];//$base->get('POST.order_id_id');
		//GF::print_r($order_ids);
     if(count($order_ids)>0){  
        $timenow = time();
        $strDate1 = date('Y-m-d')." 12:00:00";
        $strDate2 = date('Y-m-d')." 16:30:00";

        $time_1 = strtotime($strDate1);
        $time_2 = strtotime($strDate2);
        $round = '1';

        $DateToSend = date('Y-m-d')." ".date(" H:i:s");

        if($timenow<=$time_1){
           $round = '1';
        }
        else if($timenow>$time_1 && $timenow<=$time_2){
           $round = '1';
        }else{
           $DateToSend = date('Y-m-d',strtotime($date1 . "+1 days")).date(" H:i:s");
        }
        $db->update("web_order",array(
          "send_order_status"=>'Y',
          "send_order_dtm"=>$DateToSend,
          "send_order_round"=>$round,
          "rate"=>$base->get('POST.rate')
        ),array(
          "id"=>$order_ids
        ));
     }
     $sql = "SELECT master_name,master_value FROM project_master WHERE id='47'";
     $res = $db2->Execute($sql);
     
     
     $html = '<table style="width:600px;" border="1">
     <tr>
	     <td style="text-align:center;font-weight: bold;">รหัสสมาชิก</td>
	     <td style="text-align:center;font-weight: bold;">เลขที่สั่งซื้อ</td>
	     <td style="text-align:center;font-weight: bold;">ยอดรวมหยวนก่อนสั่ง</td>
     </tr>';
    
     $total =0;
     foreach($order_ids AS $val){
     	 $sum1=0;
     	$sqlOrder = "SELECT web_order.order_number,web_order.order_price,web_order.old_rate,web_order.order_pay_ship_yuan,project_user.user_code
     				
     				FROM  web_order
     				LEFT JOIN project_user ON (project_user.id=web_order.member_id)
     				WHERE web_order.id=".GF::quote($val);
     	$resOrder = $db2->Execute($sqlOrder);
     	$sum1 = ($resOrder->fields['order_price']/$resOrder->fields['old_rate'])-$resOrder->fields['order_pay_ship_yuan'];
     	$total = $total+($resOrder->fields['order_price']/$resOrder->fields['old_rate'])-$resOrder->fields['order_pay_ship_yuan'];
	 	$html .= '<tr>
	 				<td>'.$resOrder->fields['user_code'].'</td>
	 				<td>'.$resOrder->fields['order_number'].'</td>
	 				<td>'.number_format($sum1,2).'</td>
	 			</tr>
	 	';
	 }
     $html .= '
      <tr>
	     <td style="text-align:center;font-weight: bold;">รวม</td>
	     <td style="text-align:right;font-weight: bold;" colspan="2">'.number_format($total,2).' RMB</td>
	 </tr>
     ';
    
       $dataReplace = array(
            "{CREATE_DATE}"=>date("d F Y"),
            "{DATA_CNSEND}"=>$html,
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('16','1');
        $mail->msgHTML = GF::mailtemplate('16',$dataReplace,'1');
        $mail->addAddressEmail = $res->fields['master_value'];
        $mail->addAddressName = $res->fields['master_name'];
        $mail->sendMail();
	 unset($_SESSION['order_chinarorder']);

     return true;

  }

    private function _sendOrderToCNUser(){
    
        $base = Base::getInstance();
        $db = DB2::getInstance();

        $order_ids = $base->get('POST.order_id');
        $buy_ids = $base->get('POST.buy_id');
        if(count($order_ids)>0){
        foreach($order_ids as $key=>$vals){
            if($buy_ids[$key]!=''){
                $db->update("web_order",array(
                    "buy_status"=>'Y',
                    "buy_id"=>$buy_ids[$key]
                    ),array(
                    "id"=>$vals
                    ));
            }
        }   
    }

    
         return true;
    
      }

  private function _updateItemImg(){

     $base = Base::getInstance();
     $db = DB::getInstance();

     $item_id = $base->get('POST.item_id');
     $path = $base->get('pathimg');

     $img_name = GF::randomNum(25);
     $file = $path;
     $data = file_get_contents($file);
     $new = $base->get('BASEDIR').'/uploads/product/'.$img_name.".jpg";
     file_put_contents($new, $data);

     $sql = "UPDATE web_order_item SET product_image=".GF::quote($img_name.".jpg")." WHERE id=".GF::quote($item_id);
     $db->Execute($sql);
  }

  private function _refundToWalletByOrder(){
     $base = Base::getInstance();
     $db = DB::getInstance();

     $member = new Member();


     $memberInfomation = $member->memberInfomation();

     $user_id = $memberInfomation['id'];

     $orderid = $base->get('_ids');
     $total_ref = $base->get('GET.value');




     $orderInfo = $this->_orderInfo();




     $member_old_info = GF::memberinfo($orderInfo['member_id']);
     $oldWallet = $member_old_info['wallet'];

     $old_balance = $oldWallet;
     $new_balance = $oldWallet+$total_ref;

    //Weerasak 12-03-2566
    //ปิดใช้งานเงินคืนเข้า wallet
   //   $sql = "UPDATE project_user SET
   //                         wallet=wallet+'".$total_ref."',
   //                         update_dtm=NOW()
   //                           WHERE id=".GF::quote($orderInfo['member_id']);
   //   $res = $db->Execute($sql);



     $sql ="INSERT INTO project_wallet_log(
                                   user_id,
                                   wallet,
                                   before_balance,
                                   after_balance,
                                   wallet_type,
                                   wallet_desc,
                                   wallet_ref,
                                   wallet_ref_type,
                                   create_dtm,
                                   create_by
                                   )
                                   VALUES(
                                   ".GF::quote($orderInfo['member_id']).",
                                   ".GF::quote(abs($total_ref)).",
                                   ".GF::quote($old_balance).",
                                   ".GF::quote($new_balance).",
                                   'IN',
                                   'เงินคืนค่าสินค้า ORDER : ".$orderInfo['order_number']."',
                                   ".GF::quote($orderInfo['order_id']).",
                                   '3',
                                   NOW(),
                                   ".GF::quote($user_id)."
                                   )";
     $res = $db->Execute($sql);

     $sql = "UPDATE web_order SET status='5', refund_wallet=refund_wallet+".$total_ref." WHERE id=".GF::quote($orderid);
     $res = $db->Execute($sql);

  }
  private function _updateTrueRate(){

     $base = Base::getInstance();
     $db = DB2::getInstance();

     $rate = $base->get('GET.rate');
     $order_id = $base->get('GET.order');

     $db->update("web_order",array(
        "rate"=>$rate
     ),array(
        "id"=>$order_id
     ));

  }
  private function _uploadSallerImage(){
    $base = Base::getInstance();
    $db = DB2::getInstance();
    if(!empty($_FILES['file_seller']['name'])){
        $picname = $_FILES['file_seller']['name'];

        $random = GF::randomNum(25);

        $type = explode('.',$picname);
        $dest_picname_o = $base->get('BASEDIR')."/uploads/temp/".$random.".".end($type);

        $tmp_file = $_FILES['file_seller']['tmp_name'];
        @copy($tmp_file, $dest_picname_o);

        $payment_slip = $random.".".end($type);

        $db->update("web_order_item",array(
                                         "saller_image"=>$payment_slip
                                        ),array(
                                            "AND"=>array(
                                                "saller_number"=>$base->get('POST.saller_number'),
                                                "order_id"=>$base->get('POST.order_id')
                                            )
                                        ));
        return true;

    }
    }

    private function _mailOrderNoTracking(){
        $base = Base::getInstance();
        $db2 = DB2::getInstance();
        $db = DB::getInstance();
        $member = new Member();
        $memberInfomation = $member->memberInfomation();

        $trackingList = $this->_ordernoTrackList();
        foreach($trackingList['data'] as $list){
            if($list['notrack_sen_dtm']!=''){
                $sql = "UPDATE web_order_item SET notrack_num=notrack_num+1 WHERE order_id=".$db->Quote($list['id'])." AND saller_number=".$db->Quote($list['saller_number']);
                $res = $db->Execute($sql);
            }else{
                $sql = "UPDATE web_order_item SET notrack_num=notrack_num+1,notrack_send_dtm=NOW() WHERE order_id=".$db->Quote($list['id'])." AND saller_number=".$db->Quote($list['saller_number']);
                $res = $db->Execute($sql);
            }
            
        }
        $dataHtml = '<table id="example1" border="1">
                     <thead>
  
                       <tr>
  						  <th style="text-align: center;" >เลขที่สั่งซื้อ</th>
                          <th style="text-align: center;">Agency</th>
                          <th style="text-align: center;">รหัสสมาชิก</th>
                          <th style="text-align: center;">รหัสคนสั่ง</th>
                          <th style="text-align: center;">ร้านค้า</th>
                          <th style="text-align: center;">วันที่สั่งซื้อ</th>
                          <th style="text-align: center;">ระยะวันที่ค้าง</th>
                          <th style="text-align: center;">จำนวนครั้งที่แจ้ง</th>
                          <th style="text-align: center;">วันที่แจ้งครั้งแรก</th>
                       </tr>
                     </thead>
                     <tbody>
                     ';
                      foreach($trackingList['data'] as $list){
                        $dataHtml .= '<tr>
  
                           <td>'.$list['order_number'].'</td>';
                       if($memberInfomation['user_role_id']=='1'){
						 $dataHtml .= '<td style="text-align: center;">
                                         <span class="text-blue">'.GF::agencyname($list['member']['id']).'</span>
                                    </td>';
                       }
                       $dataHtml .= ' 
                       		 <td style="text-align: center;">

                                <span class="black">'.$list['user_code'].'</span>
                                 <br />
                                    <span class="text-green">'.$list['user_name'].'</span>
                            </td>
                            <td style="text-align: center;">
                                <span class="black">'.$list['user_codes'].'</span>
                                 <br />
                                    <span class="black">'.$list['user_names'].'</span>
                            </td>
                             <td style="text-align: center;">
                                <span class="black">'.$list['saller_number'].'</span>
                            </td>
                             <td style="text-align: center;">
	                            <span class="black"> วันที่ '.$list['create_dtm'].' </span>
	                        </td>
                       		 <td style="text-align: center;">
                               ';
                               		(string)$dateNow = date('Y-m-d');
                                    (string)$dateCreate = date('Y-m-d',strtotime($list['create_dtm']));
                                    $date1=date_create($dateNow);
                                    $date2=date_create($dateCreate);
                                    $diff=date_diff($date1,$date2);
                           $dataHtml .= $diff->format("%a").' </td>
                       		<td style="text-align: center;">'.$list['notrack_num'].'</td>
                       		<td style="text-align: center;">';
                                if($list['notrack_send_dtm']!=''){
                                 $dataHtml .= ' <span class="black"> วันที่ '.$list['notrack_send_dtm'].'</span>';
                               }else{ 
                                $dataHtml .= '-';
                                } 
                           $dataHtml .= ' </td>
                        </tr>
                     ';
                  }
                    $dataHtml .= '</tbody></table>';
  
                    $mail = Mailer::getInstance();
                    $masterList =  array();
                    $masterList = $db2->select("project_master","*",array("group_id"=>'10'));
                    //echo "<br />-----------<br />".$dataHtml."-----------<br />";
  
                    foreach($masterList as $vald){
                        $mail->addAddressEmail = $vald['master_value'];
                        $mail->addAddressName = $vald['master_name'];
     
                        $mail->Subject = "แจ้งรายการสั่งซื้อไม่มี Tracking";
                        $mail->msgHTML = $dataHtml;
                        $mail->sendMail();
                    }
                    
  
     }
     private function _setNotrackToDash(){
        $base = Base::getInstance();
        $db = DB2::getInstance();
        $db1 = DB::getInstance();

        $order_id = $base->get('GET.order');
        $saller_number = $base->get('GET.saller');
        $member = new Member();


        $memberInfomation = $member->memberInfomation();

        $user_id = $memberInfomation['id'];

        $db->update("web_order_item",array("product_sku_1"=>'-'),array(
            "AND"=>array(
                "order_id"=>$order_id,
                "saller_number"=>$saller_number
            )
        ));

        $orderData = $db->get("web_order",array("member_id"),array("id"=>$order_id));

      //   $itemList = $db->select("web_order_item","*",array(
      //                                                  "AND"=>array(
      //                                                      "order_id="=>$order_id,
      //                                                      "saller_number"=>$saller_number
      //                                                  )     
      //                                                   ));
      $sql = "SELECT @rownum := @rownum + 1 AS row_number, t1.* FROM web_order_item t1, (SELECT @rownum := 0) r WHERE t1.order_id =".$order_id." and t1.saller_number =".$saller_number ;
		$itemList = $db1->Execute($sql);

        foreach($itemList as $itm){
            $lstid = $db->insert("project_item_refund",array(
                                                    "item_id"=>$itm['id'],
                                                    "ref_refund"=>'0',
                                                    "ref_item"=>$itm['row_number'], 
                                                    "ref_comment"=>'ร้านค้าไม่ส่งของ',
                                                    "status"=>'Wait',
                                                    "create_dtm"=>date('Y-m-d H:i:s'),
                                                    "create_by"=>$orderData['member_id'],
                                                    "update_dtm"=>date('Y-m-d H:i:s'),  
                                                    "update_by"=>$user_id, 
                                                    ));
            //$lstid = $db->lastInsertId();
            $db->update("web_order_item",array("ref_id"=>$lstid),array(
                "AND"=>array(
                    "id"=>$itm['id']
                )
            ));
        }                                                
        return true;
    }
    private function _setNotrackComment(){
        $base = Base::getInstance();
        $db = DB2::getInstance();

        $order_id = $base->get('GET.order');
        $saller_number = $base->get('GET.saller');
        $comment = $base->get('GET.comment');

        $db->update("web_order_item",array("notrack_comment"=>$comment),array(
            "AND"=>array(
                "order_id"=>$order_id,
                "saller_number"=>$saller_number
            )
        ));
        return true;
    }
    private function _newbankInfo(){
        $base = Base::getInstance();
        $db = DB2::getInstance();


        $result = $db->get("project_bank","*",array("bank_id"=>$base->get('bank_id')));

        return $result;

    }
    private function _outsourceList(){
        $base = Base::getInstance();
        $db = DB2::getInstance();


        $result = $db->select("project_user","*",array("user_role_id"=>'4'));

        return $result;

    }
  private function _outsourceListNoTrack(){
        $base = Base::getInstance();
        $db = DB::getInstance();
		$sql = "SELECT user_code,user_name FROM project_user WHERE user_code<>'' AND user_name<>'' AND user_role_id='4'";    
        $res = $db->Execute($sql);
	    $i=0;
	     $ArrList = array();
	     while(!$res->EOF){
	        $ArrList[$i]['user_code'] = $res->fields['user_code'];
	        $ArrList[$i]['user_name'] = $res->fields['user_name'];
	        $i++;
	        $res->MoveNext();
	     }                      
	     //print_r($ArrList);
	     return $ArrList;

    }

    /*Weerasak 16-10-2564*/
    //ยอดสั่งซื้อ
    private function _updateAmountSaller(){
        //Table web_order_item_ship
        $base = Base::getInstance();
		$db = DB2::getInstance();        
		//$sql = "UPDATE web_order_item_ship SET saller_amount_rmb =".GF::quote($base->get('GET.value'))." WHERE item_id =".GF::quote($base->get('GET.item_id'));

        $price_rmb = $base->get('GET.value');
        $price_thb = $base->get('GET.value')*$base->get('GET.rate');
   
        $db->update("web_order_item_ship",array(
           "saller_amount_rmb"=>$price_rmb
        ),array(
           "item_id"=>$base->get('GET.item_id')
        ));

		//$res = $db->Execute($sql);
    }
    //ส่วนลด
    private function _updateDiscountSaller(){
        //Table web_order_item_ship
      $base = Base::getInstance();
		$db = DB::getInstance();        
		$sql = "UPDATE web_order_item_ship SET saller_discount_rmb =".GF::quote($base->get('GET.value'))." WHERE item_id =".GF::quote($base->get('GET.item_id'));
		$res = $db->Execute($sql);
    }
    //ค่าส่งในจีน
    private function _updateCNShipping(){
        // $base = Base::getInstance();
		// $db = DB::getInstance();        
		// $sql = "UPDATE web_order_item_ship SET saller_shipping_rmb =".GF::quote($base->get('GET.value'))." WHERE item_id =".GF::quote($base->get('GET.item_id'));
		// $res = $db->Execute($sql);
      $base = Base::getInstance();
		$db = DB2::getInstance();        	

        $amount = $base->get('GET.order_price');
        $ship_cn = $base->get('GET.ship');
        $ship_thb = $base->get('GET.ship')*$base->get('GET.rate');
        $discount = $base->get('GET.discount');
   
       // $sql = "UPDATE web_order_item_ship SET price_rmb =".$ship_cn.",price_thb = ".$ship_thb.",saller_amount_rmb = ".$amount.",saller_discount_rmb = ".$discount.",saller_shipping_rmb =".$ship_thb." WHERE item_id =".GF::quote($base->get('GET.item_id'));
		//$res = $db->Execute($sql);

        $db->update("web_order_item_ship",array(
           "price_rmb"=>$ship_cn,
           "price_thb"=>$ship_thb,
           "saller_amount_rmb"=>$amount,
           "saller_discount_rmb"=>$discount,
           "saller_shipping_rmb"=>$ship_cn
        ),array(
           "item_id"=> $base->get('GET.item_id')
        ));
    }
    /* End Edit */
   // Edit By Weerasak 24-01-2566
   //ปรับปรุงค่าขนส่งหน้าบิล
    private function _updateServiceShipping()
    {
       $base = Base::getInstance();
       $db = DB::getInstance();
       $member = new Member();
       
       $memberInfomation = $member->memberInfomation();
       $user_id = $memberInfomation['id'];

       $sql = "UPDATE web_shipping SET
       shipping_th_price=".$base->get('GET.ship_price').",
       shipping_th_service_price=".$base->get('GET.service_price').",
       shipping_other_price=".$base->get('GET.other_price').",
       update_dtm=NOW(),
       update_by=".GF::quote($user_id)."
       WHERE id=".$base->get('GET.shipping_id');
       $res = $db->Execute($sql);
    }
   //  END
}
?>
