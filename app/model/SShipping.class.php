<?php
 class SShipping {

 		public function additem_import(){
			$resultreturn = $this->_additem_import();
			return $resultreturn;
		}
		public function removeItemImp(){
			$resultreturn = $this->_removeItemImp();
			return $resultreturn;
		}
		public function takeOrderImp(){
			$resultreturn = $this->_takeOrderImp();
			return $resultreturn;
		}
		public function orderListImp(){
			$resultreturn = $this->_orderListImp();
			return $resultreturn;
		}
		public function invoiceInfo($id){
			$resultreturn = $this->_invoiceInfo($id);
			return $resultreturn;
		}
		public function update_num($key,$num){
			$_SESSION['temp_item'][$key]['product_qty'] = $num;
		}
		public function del_num($key){
			unset($_SESSION['temp_item'][$key]);
		}
		public function addRegister(){
			$resultreturn = $this->_addRegister();
			return $resultreturn;
		}
		public function checkRegister(){
			$resultreturn = $this->_checkRegister();
			return $resultreturn;
		}
		public function registerList(){
			$resultreturn = $this->_registerList();
			return $resultreturn;
		}
		public function updateRegister(){
			$resultreturn = $this->_updateRegister();
			return $resultreturn;
		}


		private function _additem_import(){

			$base = Base::getInstance();
 			$db = DB::getInstance();

 			$params = $base->get('POST');

			if(empty($_SESSION['items_imp'])){
				$_SESSION['items_imp'] = 0;
			}

			$_SESSION['items_imp'] = $_SESSION['items_imp']+1;
			$item_type = $params['item_typs_s'];
			if($item_type=='C'){
				$item_type = $params['item_typs_o'];
			}
			$_SESSION['item_imp'][$_SESSION['items_imp']]['tracking_no'] = $params['tracking_no'];
			$_SESSION['item_imp'][$_SESSION['items_imp']]['item_typs_s'] = $item_type;
			$_SESSION['item_imp'][$_SESSION['items_imp']]['num_box'] = $params['num_box'];
			$_SESSION['item_imp'][$_SESSION['items_imp']]['order_ship_cn'] = $params['order_ship_cn'];
			$_SESSION['item_imp'][$_SESSION['items_imp']]['order_ship_th'] = $params['order_ship_th'];
			$_SESSION['item_imp'][$_SESSION['items_imp']]['order_box'] = $params['order_box'];
			$_SESSION['item_imp'][$_SESSION['items_imp']]['order_comment'] = $params['order_comment'];

			if(!empty($_FILES['img_file']['name'])){
				$picname = $_FILES['img_file']['name'];

				$random = GF::randomNum(25);

				$type = substr(strrchr($picname, '.'), 1);
				$dest_picname_o = $base->get('BASEDIR')."/uploads/temp/".$random.".jpg";

				$tmp_file = $_FILES['img_file']['tmp_name'];
				@copy($tmp_file, $dest_picname_o);
				$_SESSION['item_imp'][$_SESSION['items_imp']]['img_file'] = $random.".jpg";
			}else{
				$_SESSION['item_imp'][$_SESSION['items_imp']]['img_file'] = NULL;
			}

			return true;

			//aksort($_SESSION);
		}

		private function _removeItemImp(){
			$base = Base::getInstance();
			if($_SESSION['item_imp'][$base->get('GET.key')]['img_file']!=NULL){
				unlink($base->get('BASEDIR')."/uploads/temp/".$_SESSION['item_imp'][$base->get('GET.key')]['img_file']);
			}
			unset($_SESSION['item_imp'][$base->get('GET.key')]);

		}

		private function _takeOrderImp(){

			$base = Base::getInstance();
 			$db = DB::getInstance();

 			$member = new Member();


 			$memberInfomation = $member->memberInfomation();

 			$user_id = $memberInfomation['id'];

			$lasrorder = $this->_getlastorder();

			$year = substr($lasrorder,0,4);
			$month = substr($lasrorder,4,2);
			$times = substr($lasrorder,6,4);

			$c_year = date('Y');
			$c_month = date('m');

			$new_time = '';
			$order_id = '';

			$my_id = '';

			if($year==$c_year && $month==$c_month){
				$new_time = intval($times)+1;
				$new_time = str_pad($new_time, 2, "0", STR_PAD_LEFT);
				$order_id = $year.$month.$new_time;
			}else{
				$order_id = $c_year.$c_month."01";
			}

			//$order_id = (1000+$_SESSION['user_id_front']).date('dmY');

			$sql = "INSERT INTO web_order (
								order_id,
								member_id,
								order_type,
								status,
								active_status,
								create_dtm,
								update_dtm
							) VALUES (
								".GF::quote($order_id).",
								".GF::quote($user_id).",
								'2',
								'W',
								'O',
								NOW(),
								NOW()
							)";
			 $res = $db->Execute($sql);
			 $last_id = $db->Insert_ID();
			 $my_id = $last_id;
			 if($res){
			 	$k = 0;
			 	$tt = count($_SESSION['item_imp']);
			 	foreach($_SESSION['item_imp'] as $key=>$vals){
			 		if($vals['img_file']!=NULL){
						$img_name = GF::randomSTR(25);
						$file = $base->get('BASEDIR')."/uploads/temp/".$vals['img_file'];
						//$data = file_get_contents($file);
						$new = $base->get('BASEDIR').'/uploads/product/'.$vals['img_file'];

						//file_put_contents($new, $data);
						if(copy($base->get('BASEDIR')."/uploads/temp/".$vals['img_file'],$new)){
							unlink($base->get('BASEDIR')."/uploads/temp/".$vals['img_file']);
						}

					}
					/*$item_type = $vals[''];
					if(){

					}*/




					$sql = "INSERT INTO web_order_item (
										order_id,
										product_sku_1,
										order_payment,
										order_ship_cn,
										order_box,
										order_ship_th,
										note,
										product_image,
										item_typs_c,
										num_box,
										status,
										create_dtm
									) VALUES (
										".GF::quote($last_id).",
										".GF::quote($vals['tracking_no']).",
										'0.00',
										".GF::quote($vals['order_ship_cn']).",
										".GF::quote($vals['order_box']).",
										".GF::quote($vals['order_ship_th']).",
										".GF::quote($vals['order_comment']).",
										".GF::quote($vals['img_file']).",
										".GF::quote($vals['item_typs_s']).",
										".GF::quote($vals['num_box']).",
										'W',
										NOW()
									)";
					 $res = $db->Execute($sql);
					 $k++;

				}
				unset($_SESSION['item_imp']);
				unset($_SESSION['items_imp']);
				//$this->sendMIMP($my_id);
			 	return true;
			 }else{
			 	return false;
			 }
		}
		private function _getlastorder(){

			$base = Base::getInstance();
			$db = DB::getInstance();

			$sql = "SELECT * FROM web_order WHERE 1=1 ORDER BY id DESC LIMIT 1";
			$res = $db->Execute($sql);

			return $res->fields['order_id'];
		}
		private function _orderListImp(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$dtatReturn=array();

		$type = $base->get('GET.type');
		if($type==''){
			$type = 'ALL';
		}

		/*if(!empty($_POST)){
			if(!empty($_POST['s_ordernumber'])){
				$_SESSION['s_ordernumber'] = $_POST['s_ordernumber'];
			}
			if(!empty($_POST['s_status'])){
				$_SESSION['s_status'] = $_POST['s_status'];
			}
			if(!empty($_POST['s_start'])){
				$_SESSION['s_start'] = $_POST['s_start'];
			}
			if(!empty($_POST['s_end'])){
				$_SESSION['s_end'] = $_POST['s_end'];
			}
		}*/



		$cond = '';
		if($type!='ALL'){
			$cond = " AND status=".GF::quote($type);
		}

		if($_SESSION['s_status']!='' && $_SESSION['s_status']!='ALL'){

			if($type!='ALL'){
				$cond = " AND status=".GF::quote($type);
			}else{
				$cond .= " AND status=".GF::quote($_SESSION['s_status']);
			}
		}
		if($_SESSION['s_ordernumber']!=''){
			$cond .= " AND order_id LIKE '%".$_SESSION['s_ordernumber']."%' ";
		}
		if($_SESSION['s_start']!=''){
			$date_check = date('Y-m-d',strtotime($_SESSION['s_start']));
			$cond .= " AND create_dtm >=".GF::quote($date_check);
		}
		if($_SESSION['s_end']!=''){
			$date_check = date('Y-m-d',strtotime($_SESSION['s_end']));
			$cond .= " AND create_dtm <=".GF::quote($date_check);
		}
		//echo $type;
		$sql = "SELECT * FROM web_order WHERE  active_status='O' AND order_type='2' AND member_id=".GF::quote($user_id)." ".$cond." ORDER BY update_dtm DESC,create_dtm DESC";
		//echo $sql;

		$res = $db->Execute($sql);
		$count = $res->NumRows();

			$base->set('allPage',ceil($count/20));

		$page = $base->get('GET.page');

		$numr = 20;
		$start = "";
		$end = "";
		if($page==1||empty($page)){
			$start = 0;
			$page = 1;
			//$end = $numr;
		}else{
			//$end = ($page*$numr);
			$start = ($page*$numr)-$numr;
		}
		 $condNum = " LIMIT ".$start.",".$numr;

		 $base->set('currpage',$page);

		$res = $db->Execute($sql.$condNum);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['order_id'] = $res->fields['order_id'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['order_price'] = $res->fields['order_price'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			$ArrList[$i]['status_text'] = $this->_convStatus('order',$res->fields['status']);
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
			$ArrList[$i]['items'] = $this->_countItem($res->fields['id']);
			$ArrList[$i]['countprice'] = $this->_countItemPrice($res->fields['id']);
			$i++;
			$res->MoveNext();
		}

		//print_r();
		$dtatReturn['data'] = $ArrList;

		return $dtatReturn;
	}
		private function _convStatus($type,$status){
			$text_return = '';
			if($type=='order'){
				if($status=='W'){
					$text_return = 'รอตรวจสอบ';
				}
				else if($status=='PM'){
					$text_return = 'ชำระเงินแล้ว';
				}
				else if($status=='C'){
					$text_return = 'ยกเลิก';
				}
				else if($status=='OR'){
					$text_return = 'สั่งซื้อแล้ว';
				}

				else if($status=='S'){
					$text_return = 'จัดส่งแล้ว';
				}
				else if($status=='E'){
					$text_return = 'ตรวจสอบแล้ว';
				}
			}
			else if($type=='item'){
				if($status=='W'){
					$text_return = 'รอตรวจสอบ';
				}
				else if($status=='V'){
					$text_return = 'กำลังตรวจสอบ';
				}
				else if($status=='C'){
					$text_return = 'ยกเลิก';
				}
				else if($status=='A'){
					$text_return = 'ตรวจสอบแล้ว';
				}
			}
			return $text_return;
		}
		private function _countItem($order_id){
			$base = Base::getInstance();
			$db = DB::getInstance();
			$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D'";
			//echo $sql;
			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$i++;
				$res->MoveNext();
			}
			return $i;
		}

		private function _itemList(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$item_id = $base->get('_ids');

		$cond = '';
		if($_SESSION['t_ordernumber']!='' && $sc!=NULL){
			$cond .= " AND product_sku_1 LIKE '%".$_SESSION['t_ordernumber']."%' ";
		}

		$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($item_id)." AND status!='D' ".$cond." ORDER BY product_saller_name ASC";
		//echo $sql;
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['product_link'] = $res->fields['product_link'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['product_title'] = $res->fields['product_title'];
			$ArrList[$i]['product_sku_1'] = $res->fields['product_sku_1'];
			$ArrList[$i]['product_image'] = $res->fields['product_image'];
			$ArrList[$i]['product_qty'] = $res->fields['product_qty'];
			$ArrList[$i]['product_saller_name'] = $res->fields['product_saller_name'];
			$ArrList[$i]['saller_number'] = $res->fields['saller_number'];
			$ArrList[$i]['product_saller_link'] = $res->fields['product_saller_link'];
			$ArrList[$i]['product_price_rmb'] = $res->fields['product_price_rmb'];
			$ArrList[$i]['product_price_thb'] = $res->fields['product_price_thb'];
			$ArrList[$i]['ship_cn_cn'] = $res->fields['ship_cn_cn'];
			$ArrList[$i]['ship_cn_th'] = $res->fields['ship_cn_th'];
			$ArrList[$i]['ship_all'] = $res->fields['ship_all'];
			$ArrList[$i]['refund'] = $res->fields['refund'];
			$ArrList[$i]['upfund'] = $res->fields['upfund'];
			$ArrList[$i]['note'] = $res->fields['note'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['order_ship_cn'] = $res->fields['order_ship_cn'];
			$ArrList[$i]['order_ship_th'] = $res->fields['order_ship_th'];
			$ArrList[$i]['order_box'] = $res->fields['order_box'];
			$ArrList[$i]['num_box'] = $res->fields['num_box'];
			$ArrList[$i]['item_typs_s'] = $res->fields['item_typs_s'];
			$ArrList[$i]['option'] = $this->_itemOption($res->fields['id']);
			$ArrList[$i]['status_text'] = $this->_convStatus('item',$res->fields['status']);
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$newsdate = date("d-m-Y H:i:s", strtotime($res->fields['create_dtm']));
			if($res->fields['update_dtm']!='0000-00-00 00:00:00'){
				$newsdate = date("d-m-Y H:i:s", strtotime($res->fields['update_dtm']));
			}
			$ArrList[$i]['update_dtm'] = $newsdate;
			$i++;
			$res->MoveNext();
		}
		//print_r($ArrList);
		return $ArrList;
	}
	private function _itemOption($item_id){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "SELECT * FROM web_order_item_option WHERE item_id=".GF::quote($item_id);
		//echo $sql;
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['option_name'] = $res->fields['option_name'];
			$ArrList[$i]['option_value'] = $res->fields['option_value'];
			$ArrList[$i]['option_name_th'] = $res->fields['option_name_th'];
			$ArrList[$i]['option_value_th'] = $res->fields['option_value_th'];
			$i++;
			$res->MoveNext();
		}
		//print_r();
		return $ArrList;
	}
	private function _countItemPrice($order_id){
			$base = Base::getInstance();
			$db = DB::getInstance();

			$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D'";
			//echo $sql;
			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			$product_price_rmb = 0;
			$product_price_thb = 0;
			$ship_cn_cn = 0;
			$ship_cn_th = 0;
			$ship_all = 0;
			$refund = 0;
			$unfund = 0;
			while(!$res->EOF){
				$product_price_rmb = $product_price_rmb+($res->fields['product_price_rmb']*$res->fields['product_qty']);
				if($res->fields['item_typs_s']=='DE'){
					$product_price_thb = $product_price_thb-($res->fields['product_price_thb']);
				}else{
					$product_price_thb = $product_price_thb+($res->fields['product_price_thb']);
				}

				$ship_cn_cn = $product_price_rmb+$res->fields['ship_cn_cn'];
				$ship_cn_th = $product_price_rmb+$res->fields['ship_cn_th'];
				$ship_all = $product_price_rmb+$res->fields['ship_all'];
				$refund = $product_price_rmb+$res->fields['refund'];
				$unfund = $product_price_rmb+$res->fields['unfund'];
				$i++;
				$res->MoveNext();
			}
			$ArrList['product_price_rmb'] = $product_price_rmb;
			$ArrList['product_price_thb'] = $product_price_thb;
			$ArrList['ship_cn_cn'] = $ship_cn_cn;
			$ArrList['ship_cn_th'] = $ship_cn_th;
			$ArrList['ship_all'] = $ship_all;
			$ArrList['refund'] = $refund;
			$ArrList['unfund'] = $unfund;
			$ArrList['all'] = ($product_price_thb+$ship_cn_cn+$ship_cn_th+$ship_all+$refund)-$unfund;
			$ArrList['all'] = number_format($ArrList['all'],2);
			//GF::print_r($ArrList);
			return $ArrList;
		}
		private function _addRegister(){
			$base = Base::getInstance();
			$db = DB2::getInstance();

			$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$user_id = $memberInfomation['id'];
 			//GF::print_r($_FILES);
 			if(!empty($_FILES['file_img']['name'])){
				$picname = $_FILES['file_img']['name'];

				$random = GF::randomNum(25);

				$type = explode('.',$picname);
				$typname = end($type);
            $typname_lover = strtolower($typname);
            if($typname_lover!='jpg' && $typname_lover!='png' && $typname_lover!='pdf'){
               return false;
               exit();
            }
				$dest_picname_o = $base->get('BASEDIR')."/uploads/shipping/".$random.".".$typname;

				$tmp_file = $_FILES['file_img']['tmp_name'];
				if(@copy($tmp_file, $dest_picname_o)){
					$arrData = array();
					$arrData['user_id'] = $user_id;
					$arrData['file_path'] = $random.".".$typname;
					$arrData['status'] = 'R';
					$arrData['update_dtm'] = date("Y-m-d H:i:s");
					$arrData['update_by'] = $user_id;
					$last_user_id = $db->insert("project_shipping_request",$arrData);

				}
				return true;
			}else{
				return false;
			}

		}
		private function _checkRegister(){
			$base = Base::getInstance();
			$db = DB2::getInstance();

			$member = new Member();
			$memberInfomation = $member->memberInfomation();
 			$user_id = $memberInfomation['id'];

			$result = $db->select("project_shipping_request","*",
									array(
										"user_id"=>$user_id,
										"ORDER"=>'req_id DESC',
										"LIMIT"=>1
									)
									);
			if(count($result)>0){
				return $result[0];
			}else{
				return array();
			}

		}
		private function _registerList(){
			$base = Base::getInstance();
			$db = DB2::getInstance();

			

            $member = new Member();

            $memberInfomation = $member->memberInfomation();

            $user_id = $memberInfomation['id'];
            $cond_search= '';
            $agency =  $memberInfomation['agency'];
            $arrSearch = array();
            if($agency=='A'){
                $list_ref = GF::refgroup();
                if(count($list_ref)>0){
                $arrSearch['user_id'] = $list_ref;
                //$cond_search .= " AND create_by IN(".$ref_str.") ";
                }else{
                $arrSearch['user_id'] = 'X';
                }
            }
            $arrSearch['status[!]'] = 'DDD';
            //echo 'ddd';exit();
            $count = $db->count("project_shipping_request",array("AND"=>$arrSearch));
            //echo $db->last_query();exit();

			$base->set('allPage',ceil($count/20));

			$page = $base->get('GET.page');

			$numr = 20;
			$start = "";
			$end = "";
			if($page==1||empty($page)){
				$start = 0;
				$page = 1;
				//$end = $numr;
			}else{
				//$end = ($page*$numr);
				$start = ($page*$numr)-$numr;
			}


			$base->set('currpage',$page);

			$result = $db->select("project_shipping_request","*",
									array(
                                        "AND"=>$arrSearch,
										"ORDER"=>'req_id DESC',
										"LIMIT"=>[$start,$numr]
									)
									);
			return $result;
		}
		private function _updateRegister(){
			$base = Base::getInstance();
			$db = DB2::getInstance();

			$req_id = $base->get('GET.id');
			$status = $base->get('GET.status');

			if($status=='Active'){
				$data = $db->get("project_shipping_request","*",
									array(
										"req_id"=>$req_id
									)
                                );
                                
								
                $datauser = $db->get("project_user","*",array("id"=>$data['user_id']));
                
                $role_id = $this->_getAgencyRole($datauser['ref_id']);

                //GF:print_r($role_id);

				$membercode = GF::genmembercode("MEMBER_SHIP",$datauser['ref_id']);

				//exit();
				
				$last_user_id = $db->update("project_shipping_request",array("status"=>'O'),array('req_id'=>$req_id));
				$last_user_id = $db->update("project_user",array("user_role_id"=>3,"user_code"=>$membercode),array('id'=>$data['user_id']));
				
				$dataSet = $db->get("project_user",array(
			  										    "user_code(user_code)",
			  										    "user_email(user_email)",
			  										    "user_name(user_name)",
			  										    "user_nickname(user_surname)",
			  										    "user_address(user_address)",
			  										    "user_phone_number(user_tel)",
			  										    "create_dtm(user_datecreate)"
			  											),array(
			  											"id"=>$data['user_id']
			  											));
                $addressdata = $db->get("project_user","*",array("id"=>$data['user_id']));
                
                $dst = $db->get("project_thai_district","*",array("DISTRICT_ID"=>$addressdata['district']));
                $amp = $db->get("project_thai_amphur","*",array("AMPHUR_ID"=>$addressdata['amphur']));
                $prv = $db->get("project_thai_province","*",array("PROVINCE_ID"=>$addressdata['provicne']));
                
                $dataSet['user_address'] = $dataSet['user_address']." ".$dst['DISTRICT_NAME']." ".$amp['AMPHUR_NAME']." ".$prv['PROVINCE_NAME']." ".$addressdata['user_post'];
                
                $dataSet['user_group'] = 'TCC';
                $dataSet['user_role'] = 'member';
                $dataSet['user_type'] = 'ship';
                $dataSet['user_status'] = '1';

                $resultRate = $db->select("project_user_rate","*",array("user_id"=>$addressdata['user_role_id']));
                $dataArrSet = array();
                $x=0;
                foreach($resultRate as $dataArr){
                    //$dataMaster = $db->get("project_master","*",array("id"=>$dataArr['unit']));
                    $dataArrSet[$x]['rate_user_code'] = $addressdata['user_code'];
                    $dataArrSet[$x]['rate_goods_code'] = $dataArr['unit_code'];
                    $dataArrSet[$x]['rate_price'] = $dataArr['rate'];
                    $dataArrSet[$x]['rate_status'] = '1';
                    $x++;
                }
                $dataSet['user_data_rate'] = $dataArrSet;	
                                                    
                                                            
                $db->insert("project_webservice",array(
                                                            "service_token"=>GF::randomStr(20),
                                                            "service_method"=>"ADD_MEMBER",
                                                            "service_data"=>json_encode($dataSet),
                                                            "status"=>'N',
                                                            "create_dtm"=>date('Y-m-d H:i:s'),
                                                            "update_dtm"=>date('Y-m-d H:i:s')
                                                            ));
                $mail = Mailer::getInstance();
                
                
                //$orderinfo = $db2->get("web_order","*",array("id"=>$odrid));
                // $mail->userid = $addressdata['member_id'];
                // $mail->Subject = "แจ้งอนุมัติ คำขอเปิดรหัส Shipping";
                // $members = GF::memberinfo($orderinfo['member_id']);
                // $dataHtml = GF::mailtemplate('13');
                // $dataHtml = str_replace("{CUST_NAME}",$members['user_name'],$dataHtml);
                // $dataHtml = str_replace("{CUST_CODE}",$members['user_code'],$dataHtml);
                // $mail->msg = $dataHtml;
                // $mail->SendingToNoti();

               
                $members = GF::memberinfo($addressdata['member_id']);
                $link = $base->get('BASEURL').'/#/shipping/register/';
                $dataReplace = array(
                    "{CUST_NAME}"=>$members['user_name'],
                    "{LINK}"=>GF::shorturl($link,'1'),
                    "{CUST_CODE}"=>$members['user_code']
                );
                $mail = Mailer::getInstance();
                $mail->Subject = GF::mailsubject('7',$members['ref_id']);
                $mail->msgHTML = GF::mailtemplate('7',$dataReplace,$members['ref_id']);
                $mail->addAddressEmail = $members['user_email'];
                $mail->addAddressName = $members['user_name'];
                $mail->sendMail();
				
			}else{
				$last_user_id = $db->update("project_shipping_request",array("status"=>'N'),array('req_id'=>$req_id));
				//var_dump($db->last_query());
            }
            
            



        }
        
        private function _getAgencyRole($ref_id){
            $base = Base::getInstance();
            $db = DB2::getInstance();
    
            $dataReturn = '2';
            $member_data = $db->get("project_user",array("user_role_id"),array("id"=>$ref_id));
            if(intval($member_data['user_role_id'])>0){
                $data = $db->get("project_role",array("id"),array(
                    "AND"=>array(
                        "agency_type"=>'S',
                        "agency_default"=>'Y'
                    )
                ));
                $dataReturn = $data['id'];
            }
    
            return $dataReturn;
    
        }
 }
?>
