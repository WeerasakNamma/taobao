<?php
 class SProject {





 	public function createProject(){
		$resultreturn = $this->_createProject();
		return $resultreturn;
	}
	public function updateProject(){
		$resultreturn = $this->_updateProject();
		return $resultreturn;
	}
	public function projectList(){
		$resultreturn = $this->_projectList();
		return $resultreturn;
	}
	public function projectInfomation(){
		$resultreturn = $this->_projectInfomation();
		return $resultreturn;
	}
	public function deleteFile(){
		$resultreturn = $this->_deleteFile();
		return $resultreturn;
	}

	public function createAddon(){
		$resultreturn = $this->_createAddon();
		return $resultreturn;
	}
	public function updateAddon(){
		$resultreturn = $this->_updateAddon();
		return $resultreturn;
	}
	public function addonList(){
		$resultreturn = $this->_addOnList();
		return $resultreturn;
	}
	public function deleteAddon(){
		$resultreturn = $this->_deleteAddon();
		return $resultreturn;
	}
	public function addonInfomation(){
		$resultreturn = $this->_addonInfomation();
		return $resultreturn;
	}

	public function createPayment(){
		$resultreturn = $this->_createPayment();
		return $resultreturn;
	}
	public function updatePaymentValue(){
		$resultreturn = $this->_updatePaymentValue();
		return $resultreturn;
	}
	public function paymentList(){
		$resultreturn = $this->_paymentList();
		return $resultreturn;
	}
	public function checkRoundForupdate($params){
		$resultreturn = $this->_checkRoundForUpdate($params);
		return $resultreturn;
	}
	public function createPaymentRound(){
		$resultreturn = $this->_createPaymentRound();
		return $resultreturn;
	}
	public function updatePaymentRound(){
		$resultreturn = $this->_updatePaymentRound();
		return $resultreturn;
	}
	public function deleteRound(){
		$resultreturn = $this->_deletePaymentRound();
		return $resultreturn;
	}
	public function deletePayment(){
		$resultreturn = $this->_deletePayment();
		return $resultreturn;
	}
	public function commentList($project_id){
		$resultreturn = $this->_projectCommentList($project_id);
		return $resultreturn;
	}
	public function createComment(){
		$resultreturn = $this->_createComment();
		return $resultreturn;
	}
	public function uploadFile(){
		$resultreturn = $this->_uploadFile();
		return $resultreturn;
	}
	public function updateFileName(){
		$resultreturn = $this->_updateFilename();
		return $resultreturn;
	}
	public function deleteProject(){
		$resultreturn = $this->_deleteProject();
		return $resultreturn;
	}
	public function deleteComment(){
		$resultreturn = $this->_deleteComment();
		return $resultreturn;
	}


	private function _projectList(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		/*$member = new Member();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];*/
 		
 		
 		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$start1 = date("Y-m-d", strtotime($_SESSION['project_project']['start_date1']));
 		$start2 = date("Y-m-d", strtotime($_SESSION['project_project']['start_date2']));
 		$end1 = date("Y-m-d", strtotime($_SESSION['project_project']['end_date1']));
 		$end2 = date("Y-m-d", strtotime($_SESSION['project_project']['end_date2']));

 		//$case = $base->get('_case');
 		$cond_select = '';
 		if($_SESSION['project_project']['start_date1']!=''){
			$cond_select .= ' AND project_project.project_start>='.GF::quote($start1);
		}
		if($_SESSION['project_project']['start_date2']!=''){
			$cond_select .= ' AND project_project.project_start<='.GF::quote($start2);
		}
		
		if($_SESSION['project_project']['end_date1']!=''){
			$cond_select .= ' AND project_project.project_end<='.GF::quote($end1);
		}
		if($_SESSION['project_project']['end_date2']!=''){
			$cond_select .= ' AND project_project.project_end>='.GF::quote($end2);
		}

		if($_SESSION['project_project']['project_status']!=''){
			$cond_select .= " AND project_project.project_status LIKE '%".$_SESSION['project_project']['project_status']."%'";
		}
		if($_SESSION['project_project']['project_type']!=''){
			$cond_select .= " AND project_project.project_type =".GF::quote($_SESSION['project_project']['project_type'])."";
		}
		//$_SESSION['project_project']['project_name'] = '';
		if($_SESSION['project_project']['project_name']!=''){
			$cond_select .= " AND project_project.project_name LIKE '%".$_SESSION['project_project']['project_name']."%' ";
		}

		$cond_in = '';
		if($_SESSION['project_project']['user_type']!=''){
			if($_SESSION['project_project']['user_type']=='O'){
				$cond_in = " AND project_project.project_id IN(SELECT project_project_owner.project_id FROM project_project_owner WHERE user_id=".GF::quote($_SESSION['project_project']['user_id']).") ";
			}
			if($_SESSION['project_project']['user_type']=='T'){
				$cond_in = " AND (project_project.project_id IN(SELECT project_project_team.project_id FROM project_project_team WHERE user_id=".GF::quote($_SESSION['project_project']['user_id']).")) ";
			}

		}

 		//$sql = '';

		//$sql = "SELECT * FROM project_project WHERE (active_status='O' ".$cond_select.") ".$cond_in." ORDER BY project_id DESC";
 		
 		
		$case = $base->get('_case');
 		/*$case = $_SESSION['project_project']['project_case'];//
 		if($case==''){
			$_SESSION['project_project']['project_case']='OW';
		}
		$case = $_SESSION['project_project']['project_case'];
		$base->set('_case',$_SESSION['project_project']['project_case']);*/
 		$sql = '';
 		if($case=='OW'){
			$sql = "SELECT * FROM project_project WHERE (project_project.active_status='O' ".$cond_select.") AND
									(project_project.project_id IN
									(SELECT project_project_owner.project_id FROM project_project_owner WHERE project_project_owner.user_id=".GF::quote($user_id)." )
									OR project_project.project_id IN
									(SELECT project_project_team.project_id FROM project_project_team WHERE project_project_team.user_id=".GF::quote($user_id)." )) ".$cond_in."
									 ORDER BY project_project.project_id ASC";
		}else{
			$sql = "SELECT * FROM project_project WHERE (project_project.active_status='O' ".$cond_select.") AND
									(project_project.project_id NOT IN
									(SELECT project_project_owner.project_id FROM project_project_owner WHERE project_project_owner.user_id=".GF::quote($user_id)." )
									AND project_project.project_id NOT IN (SELECT project_project_team.project_id FROM project_project_team WHERE project_project_team.user_id=".GF::quote($user_id)." ) ) ".$cond_in."
									  ORDER BY project_project.project_id ASC";
		}
		//echo $sql;
		$res = $db->Execute($sql);
		$count = 0;
			while(!$res->EOF){
				$count++;
				$res->MoveNext();
			}
			$res->Close();

			$base->set('allPage_'.$base->get('_case'),ceil($count/10));

		$page = $base->get('GET.page');

		$numr = 10;
		$start = "";
		$end = "";
		if($page==1||empty($page)){
			$start = 0;
			$page = 1;
			//$end = $numr;
		}else{
			//$end = ($page*$numr);
			$start = ($page*$numr)-$numr;
		}
		 $cond = " LIMIT ".$start.",".$numr;

		 $base->set('currpage',$page);

		$res = $db->Execute($sql.$cond);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['project_id'] = $res->fields['project_id'];
			$arrReturn[$i]['project_name'] = $res->fields['project_name'];
			$arrReturn[$i]['project_desc'] = $res->fields['project_desc'];
			$arrReturn[$i]['project_start'] = $res->fields['project_start'];
			$arrReturn[$i]['project_end'] = $res->fields['project_end'];
			$arrReturn[$i]['project_thumb'] = $res->fields['project_thumb'];
			$arrReturn[$i]['project_status'] = $res->fields['project_status'];
			$arrReturn[$i]['project_type'] = $res->fields['project_type'];
			
			$arrReturn[$i]['comment'] = $this->_projectComment($res->fields['project_id']);
			
			$arrReturn[$i]['status'] = $res->fields['status'];
			$arrReturn[$i]['owner'] = $this->_ownerList($res->fields['project_id']);
			$arrReturn[$i]['team'] =  $this->_ownerTeam($res->fields['project_id']);
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;


	}

	protected function _ownerList($project_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$sql = "SELECT project_project_owner.user_id,project_user.* FROM project_project_owner LEFT JOIN project_user ON project_project_owner.user_id=project_user.id WHERE project_project_owner.project_id=".GF::quote($project_id);
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['user_id'] = $res->fields['user_id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
			$arrReturn[$i]['user_email'] = $res->fields['user_email'];
			$arrReturn[$i]['user_nickname'] = $res->fields['user_nickname'];
			$arrReturn[$i]['user_thumb'] = $res->fields['user_thumb'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	protected function _ownerTeam($project_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$sql = "SELECT project_project_team.user_id,project_user.* FROM project_project_team LEFT JOIN project_user ON project_project_team.user_id=project_user.id WHERE project_project_team.project_id=".GF::quote($project_id);
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['user_id'] = $res->fields['user_id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
			$arrReturn[$i]['user_email'] = $res->fields['user_email'];
			$arrReturn[$i]['user_nickname'] = $res->fields['user_nickname'];
			$arrReturn[$i]['user_thumb'] = $res->fields['user_thumb'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	protected function _projectFile($project_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$sql = "SELECT project_project_file.*,project_user.user_name FROM project_project_file LEFT JOIN project_user ON project_project_file.create_by=project_user.id WHERE project_project_file.project_id=".GF::quote($project_id);
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['file_id'] = $res->fields['file_id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
			$arrReturn[$i]['file_name'] = $res->fields['file_name'];
			$arrReturn[$i]['file_title'] = $res->fields['file_title'];
			$arrReturn[$i]['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn[$i]['create_by'] = $res->fields['create_by'];

			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	protected function _projectComment($project_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();

 		$sql = "SELECT project_project_comment.*,project_user.user_name FROM project_project_comment LEFT JOIN project_user ON project_project_comment.create_by=project_user.id WHERE project_project_comment.project_id=".GF::quote($project_id)." ORDER BY project_project_comment.create_dtm DESC";
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['comment_id'] = $res->fields['comment_id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
			$arrReturn[$i]['comment_desc'] = $res->fields['comment_desc'];
			$arrReturn[$i]['comment_type'] = $res->fields['comment_type'];
			$arrReturn[$i]['create_dtm'] = $res->fields['create_dtm'];
			$base->set('user_id',$res->fields['create_by']);
			$arrReturn[$i]['member'] = $member->memberInfomationByID();

			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	private function _projectCommentList($project_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();

 		$sql = "SELECT project_project_comment.*,project_user.user_name FROM project_project_comment LEFT JOIN project_user ON project_project_comment.create_by=project_user.id WHERE project_project_comment.project_id=".GF::quote($project_id)." ORDER BY project_project_comment.create_dtm DESC";
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['comment_id'] = $res->fields['comment_id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
			$arrReturn[$i]['comment_desc'] = $res->fields['comment_desc'];
			$arrReturn[$i]['create_dtm'] = $res->fields['create_dtm'];
			$base->set('user_id',$res->fields['create_by']);
			$arrReturn[$i]['member'] = $member->memberInfomationByID();

			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}

	private function _projectInfomation(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$project_id = $base->get('_ids');

 		if(!empty($project_id)){
			$sql = "SELECT * FROM project_project WHERE project_id=".GF::quote($project_id);
	 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);
			$arrReturn = array();

			$arrReturn['project_id'] = $res->fields['project_id'];
			$arrReturn['project_name'] = $res->fields['project_name'];
			$arrReturn['project_desc'] = $res->fields['project_desc'];
			$arrReturn['project_start'] = $res->fields['project_start'];
			$arrReturn['project_end'] = $res->fields['project_end'];
			$arrReturn['project_thumb'] = $res->fields['project_thumb'];
			$arrReturn['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn['project_status'] = $res->fields['project_status'];
			$arrReturn['project_type'] = $res->fields['project_type'];
			$arrReturn['status'] = $res->fields['status'];
			$arrReturn['owner'] = $this->_ownerList($res->fields['project_id']);
			$arrReturn['team'] =  $this->_ownerTeam($res->fields['project_id']);
			$arrReturn['file'] = $this->_projectFile($res->fields['project_id']);
			$arrReturn['comment'] = $this->_projectComment($res->fields['project_id']);

			return $arrReturn;

		}else{
			return false;
		}
	}

 	private function _createProject(){ 
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$picsave = '';

 		if($_FILES['project_thumb']['name']){
			$picname = $_FILES['project_thumb']['name'];

			$filetype = explode('.',$picname);
			$picname = GF::randomStr(25);

			$dest_picname_o = $base->get('BASEDIR').'/uploads/project/'.$picname."_original.".end($filetype);

			$tmp_file = $_FILES['project_thumb']['tmp_name'];
			if(copy($tmp_file, $dest_picname_o)){
				$picsave = $picname."_original.".end($filetype);
			}
		}
		$start_date = NULL;
 		if($base->get('POST.project_start')!=''){
			$start_date = str_replace('/','-',$base->get('POST.project_start'));
 			$start_date = date("Y-m-d", strtotime($start_date));
		}
		
		$start_end = NULL;
		if($base->get('POST.project_end')!=''){
			$start_end = str_replace('/','-',$base->get('POST.project_end'));
 			$start_end = date("Y-m-d", strtotime($start_end));
		}

 		$sql = "INSERT INTO project_project (
											  project_name,
											  project_desc,
											  project_start,
											  project_end,
											  project_thumb,
											  project_status,
											  project_type,
											  status,
											  create_dtm,
											  create_by
											)VALUES(
												".GF::quote($base->get('POST.project_name')).",
												".GF::quote($base->get('POST.project_desc')).",
												".GF::quote($start_date).",
												".GF::quote($start_end).",
												".GF::quote($picsave).",
												".GF::quote($base->get('POST.project_status')).",
												".GF::quote($base->get('POST.project_type')).",
												'O',
												NOW(),
												".GF::quote($user_id)."
											)";

		$res = $db->Execute($sql);

		if($res){
			$project_id = $db->Insert_ID();

			$project_owner = $base->get('POST.user_owner');
			$project_team = $base->get('POST.user_team');

			if(count($project_owner)>=1){
				foreach($project_owner as $owner){
					$sql = "INSERT INTO project_project_owner (
														  project_id,
														  user_id,
														  create_dtm,
														  create_by
														)VALUES(
															".GF::quote($project_id).",
															".GF::quote($owner).",
															NOW(),
															".GF::quote($user_id)."
														)";

					$res = $db->Execute($sql);
				}
			}

			if(count($project_team)>=1){
				foreach($project_team as $team){
					$sql = "INSERT INTO project_project_team (
														  project_id,
														  user_id,
														  create_dtm,
														  create_by
														)VALUES(
															".GF::quote($project_id).",
															".GF::quote($team).",
															NOW(),
															".GF::quote($user_id)."
														)";

					$res = $db->Execute($sql);
				}
			}

			$project_file = $_FILES['item_file'];
			$item_name = $base->get('POST.item_name');

			if($project_file['name'][0]!=''){
				foreach($project_file['name'] as $key=>$d_file){
					//print_r($d_file);
					$filename = $d_file;
					$filetype = explode('.',$filename);
					$filename = GF::randomStr(25);

					$dest_filename_o = $base->get('BASEDIR').'/uploads/projectfiles/'.$filename.".".end($filetype);

					$tmp_file = $project_file['tmp_name'][$key];
					if(copy($tmp_file, $dest_filename_o)){
						$filesave = $filename.".".end($filetype);
								$sql = "INSERT INTO project_project_file (
																  project_id,
																  file_name,
																  file_title,
																  create_dtm,
																  create_by
																)VALUES(
																	".GF::quote($project_id).",
																	".GF::quote($filesave).",
																	".GF::quote($item_name[$key]).",
																	NOW(),
																	".GF::quote($user_id)."
																)";
								$res = $db->Execute($sql);
					}
				}
			}
			$base->set('_project_id_',$project_id);
			$this->_sendNotification('created');
			return true;
		}else{
			return false;
		}

	}
	private function _sendNotification($type){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$mail = Mailer::getInstance();

 		$member = new Member();
 		$base->set('_ids',$base->get('_project_id_'));
 		$projectInfomation = $this->projectInfomation();
 		$memberInfomation = $member->memberInfomation();


 		$mailHTML = "<b>".$memberInfomation['user_name']."</b> has ".$type." project : <b>".$projectInfomation['project_name']."</b><br />";
 		foreach($projectInfomation['owner'] as $items){
			$mailHTML .= "<br />Owner : <b>".$items['user_name']."</b>";
		}
		foreach($projectInfomation['team'] as $items){
			$mailHTML .= "<br />Team : <b>".$items['user_name']."</b>";
		}

 		$mailHTML .= "<br />Description : <b>".$projectInfomation['project_desc']."</b>";
 		$mailHTML .= "<br />Start : <b>".date("d-m-Y",strtotime($projectInfomation['project_start']))."</b>";
 		$mailHTML .= "<br />Create : <b>".date("d-m-Y H:i:s",strtotime($projectInfomation['create_dtm']))."</b>";

 		foreach($projectInfomation['owner'] as $mailMember){
			$mail->Subject = '[Project Mail]::'.$memberInfomation['user_name']." has ".$type." project : ".$projectInfomation['project_name'];
			$mail->msgBody = $mailHTML;

			$mail->addAddressEmail = $mailMember['user_email'];
			$mail->addAddressName = $mailMember['user_name'];
			$mail->sendMail();
		}
		foreach($projectInfomation['team'] as $mailMember){
			$mail->Subject = '[Project Mail]::'.$memberInfomation['user_name']." has ".$type." project : ".$projectInfomation['project_name'];
			$mail->msgBody = $mailHTML;

			$mail->addAddressEmail = $mailMember['user_email'];
			$mail->addAddressName = $mailMember['user_name'];
			$mail->sendMail();
		}
		$mail->Subject = '[Project Mail]::'.$memberInfomation['user_name']." has ".$type." project : ".$projectInfomation['project_name'];
 		$mail->SendingToNoti();
	}
	private function _uploadFile(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$filename_o = $_FILES['project_file']['name'];
		$filetype = explode('.',$filename_o);
		$filename = GF::randomStr(25);

		$member = new Member();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$dest_filename_o = $base->get('BASEDIR').'/uploads/projectfiles/'.$filename.".".end($filetype);

		$tmp_file = $_FILES['project_file']['tmp_name'];
		if(copy($tmp_file, $dest_filename_o)){
			$filesave = $filename.".".end($filetype);
			$sql = "INSERT INTO project_project_file (
											  project_id,
											  file_name,
											  file_title,
											  create_dtm,
											  create_by
											)VALUES(
												".GF::quote($base->get('POST.project_id')).",
												".GF::quote($filesave).",
												".GF::quote($filename_o).",
												NOW(),
												".GF::quote($user_id)."
											)";
											
											
											//".GF::quote($base->get('POST.file_name')).", ===change==> $filename_o
			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	private function _updateProject(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$picsave = '';

 		$project_id = $base->get('POST.project_id');

 		if($_FILES['project_thumb']['name']!=''){
			$picname = $_FILES['project_thumb']['name'];

			$filetype = explode('.',$picname);
			$picname = GF::randomStr(25);

			$dest_picname_o = $base->get('BASEDIR').'/uploads/project/'.$picname."_original.".end($filetype);

			$tmp_file = $_FILES['project_thumb']['tmp_name'];
			if(copy($tmp_file, $dest_picname_o)){
				$picsave = $picname."_original.".end($filetype);
				$sql = "UPDATE project_project SET
												project_thumb=".GF::quote($picsave)."
												WHERE project_id=".GF::quote($project_id);
				$res = $db->Execute($sql);
				if(is_file($base->get('BASEDIR')."/uploads/project/".$base->get('POST.old_thumb'))){
					@unlink($base->get('BASEDIR').'/uploads/project/'.$base->get('POST.old_thumb'));
				}
  
			}
		}

		$start_date = NULL;
 		if($base->get('POST.project_start')!=''){
			$start_date = str_replace('/','-',$base->get('POST.project_start'));
 			$start_date = date("Y-m-d", strtotime($start_date));
		}
		
		$start_end = NULL;
		if($base->get('POST.project_end')!=''){
			$start_end = str_replace('/','-',$base->get('POST.project_end'));
 			$start_end = date("Y-m-d", strtotime($start_end));
		}
		//$start_date = str_replace('/','-',$base->get('POST.project_start'));
 		//$start_date = date("Y-m-d", strtotime($start_date));
 		$sql = "UPDATE project_project SET   project_name=".GF::quote($base->get('POST.project_name')).",
											  project_desc=".GF::quote($base->get('POST.project_desc')).",
											  project_start=".GF::quote($start_date).",
											  project_end=".GF::quote($start_end).",
											  project_status=".GF::quote($base->get('POST.project_status')).",
											  project_type=".GF::quote($base->get('POST.project_type')).",
											  update_dtm=NOW(),
											  update_by=".GF::quote($user_id)." WHERE project_id=".GF::quote($project_id);

		$res = $db->Execute($sql);
		if($res){


			$project_owner = $base->get('POST.user_owner');
			$project_team = $base->get('POST.user_team');

			if(count($project_owner)>=1){
				$sql = "DELETE FROM project_project_owner WHERE project_id=".GF::quote($project_id);
				$res = $db->Execute($sql);
				foreach($project_owner as $owner){
					$sql = "INSERT INTO project_project_owner (
														  project_id,
														  user_id,
														  create_dtm,
														  create_by
														)VALUES(
															".GF::quote($project_id).",
															".GF::quote($owner).",
															NOW(),
															".GF::quote($user_id)."
														)";

					$res = $db->Execute($sql);
				}
			}

			if(count($project_team)>=1){
				$sql = "DELETE FROM project_project_team WHERE project_id=".GF::quote($project_id);
				$res = $db->Execute($sql);
				foreach($project_team as $team){
					$sql = "INSERT INTO project_project_team (
														  project_id,
														  user_id,
														  create_dtm,
														  create_by
														)VALUES(
															".GF::quote($project_id).",
															".GF::quote($team).",
															NOW(),
															".GF::quote($user_id)."
														)";

					$res = $db->Execute($sql);
				}
			}

			$project_file = $_FILES['item_file'];
			$item_name = $base->get('POST.item_name');

			if($project_file['name'][0]!=''){
				foreach($project_file['name'] as $key=>$d_file){
					//print_r($d_file);
					$filename_o = $d_file;
					$filetype = explode('.',$filename_o);
					$filename = GF::randomStr(25);

					$dest_filename_o = $base->get('BASEDIR').'/uploads/projectfiles/'.$filename.".".end($filetype);

					$tmp_file = $project_file['tmp_name'][$key];
					if(copy($tmp_file, $dest_filename_o)){
						$filesave = $filename.".".end($filetype);
								$sql = "INSERT INTO project_project_file (
																  project_id,
																  file_name,
																  file_title,
																  create_dtm,
																  create_by
																)VALUES(
																	".GF::quote($project_id).",
																	".GF::quote($filesave).",
																	".GF::quote($filename_o).",
																	NOW(),
																	".GF::quote($user_id)."
																)";
																
																
																//".GF::quote($item_name[$key]).", ==change=>$filename_o
								$res = $db->Execute($sql);
					}
				}
			}

			return true;
		}else{
			return false;
		}


	}
	private function _deleteFile(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$file_id = $base->get('_ids');
 		if($file_id!=''){
			$sql = "SELECT * FROM project_project_file WHERE file_id=".GF::quote($file_id);
	 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);

			$file_name = $res->fields['file_name'];

			$unlink = @unlink($base->get('BASEDIR')."/uploads/projectfiles/".$file_name);

			if($unlink){
				$sql = "DELETE FROM project_project_file WHERE file_id=".GF::quote($file_id);
				$res = $db->Execute($sql);

				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}


	private function _createAddon(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$sql = "INSERT INTO project_project_addon (
											  project_id,
											  addon_type,
											  addon_info,
											  addon_payment,
											  create_dtm,
											  create_by
											)VALUES(
												".GF::quote($base->get('POST.project_id')).",
												".GF::quote($base->get('POST.addon_type')).",
												".GF::quote($base->get('POST.addon_info')).",
												".GF::quote($base->get('POST.addon_payment')).",
												NOW(),
												".GF::quote($user_id)."
											)";
		$res = $db->Execute($sql);
		if($res){
			return true;
		}else{
			return false;
		}
	}
	private function _updateAddon(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$sql = "UPDATE project_project_addon SET
												  addon_type=".GF::quote($base->get('POST.addon_type')).",
												  addon_info=".GF::quote($base->get('POST.addon_info')).",
												  addon_payment=".GF::quote($base->get('POST.addon_payment')).",
												  update_dtm=NOW(),
												  update_by=".GF::quote($user_id)."
													WHERE addon_id=".GF::quote($base->get('POST.addon_id'));
		$res = $db->Execute($sql);
		if($res){
			return true;
		}else{
			return false;
		}
	}
	private function _addOnList(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$project_id = $base->get('_ids');

 		$sql = "SELECT * FROM project_project_addon WHERE project_id=".GF::quote($project_id);
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['addon_id'] = $res->fields['addon_id'];
			$arrReturn[$i]['addon_type'] = $res->fields['addon_type'];
			$arrReturn[$i]['addon_info'] = $res->fields['addon_info'];
			$arrReturn[$i]['addon_payment'] = $res->fields['addon_payment'];
			$arrReturn[$i]['create_dtm'] = $res->fields['create_dtm'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}

	private function _deleteAddon(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$addon_id = $base->get('_ids');

 		$sql = "DELETE FROM project_project_addon WHERE addon_id=".GF::quote($addon_id);
		$res = $db->Execute($sql);

 		if($res){
			return true;
		}else{
			return false;
		}
	}
	private function _addonInfomation(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$addon_id = $base->get('GET.id');

 		if(!empty($addon_id)){
			$sql = "SELECT * FROM project_project_addon WHERE addon_id=".GF::quote($addon_id);
	 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);
			$arrReturn = array();

			$arrReturn['addon_id'] = $res->fields['addon_id'];
			$arrReturn['addon_type'] = $res->fields['addon_type'];
			$arrReturn['addon_info'] = $res->fields['addon_info'];
			$arrReturn['addon_payment'] = $res->fields['addon_payment'];
			$arrReturn['create_dtm'] = $res->fields['create_dtm'];

			return $arrReturn;

		}else{
			return false;
		}



	}

	private function _createPayment(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$sql = "INSERT INTO project_project_payment (
											  project_id,
											  payment_type,
											  payment_value,
											  payment_status,
											  create_dtm,
											  create_by
											)VALUES(
												".GF::quote($base->get('POST.project_id')).",
												".GF::quote($base->get('POST.payment_type')).",
												".GF::quote($base->get('POST.payment_value')).",
												'1',
												NOW(),
												".GF::quote($user_id)."
											)";
		$res = $db->Execute($sql);
		if($res){
			if($base->get('POST.payment_type')!='1'){
				$payment_id = $db->Insert_ID();
				
				$now_date = date('Y-m-d');
				$now_time = strtotime($now_date);
				
				$effectiveDate = date('Y-m-d', strtotime("+1 months", strtotime($now_date)));
				$effectiveTime = strtotime($effectiveDate);

				//$start_date = str_replace('/','-',$base->get('POST.payment_strat'));
 				$start_date = date("Y-m-d", strtotime($base->get('POST.payment_start')));

 				//$end_date = str_replace('/','-',$base->get('POST.payment_end'));
 				$end_date = date("Y-m-d", strtotime($base->get('POST.payment_end')));

 				$base->set('payment_strat',$start_date);
 				$base->set('payment_end',$end_date);
 				//GF::print_r($end_date);exit();
				$sql = "UPDATE project_project_payment SET
													  payment_start=".GF::quote($start_date).",
													  payment_end=".GF::quote($end_date).",
													  payment_term=".GF::quote($base->get('POST.payment_term'))."
													  WHERE payment_id=".GF::quote($payment_id);
													 echo $sql;
				$res = $db->Execute($sql);

				if($base->get('POST.payment_term')=='1'){
					$splite_date = $this->_calculateDay();
					foreach($splite_date as $key=>$spDate){
						$params = array();
						$params['payment_id'] = $payment_id;
						$params['round_name'] = $key+1;
						$params['start'] = $spDate['start'];
						$params['end'] = $spDate['end'];
						$params['bath'] = $base->get('POST.payment_value');

						$this->_createPaymentRoundByPID($params);
					}
				}
				else if($base->get('POST.payment_term')=='2'){
					$splite_date = $this->_calculateMonth();
					$cal_i = 1;
					foreach($splite_date as $key=>$spDate){
						$params = array();
						$params['payment_id'] = $payment_id;
						$params['round_name'] = $key+1;
						$params['start'] = $spDate['start'];
						$params['end'] = $spDate['end'];
						$params['bath'] = $base->get('POST.payment_value');
						
						$start_time = strtotime($spDate['start']);
						$end_time = strtotime($spDate['end']);
						
						$params['round_status']='1';
						if($cal_i==1){
							$params['round_status']='2';
						}else{
							if(($now_time>=$start_time && $now_time<=$end_time) || ($effectiveTime>=$start_time)){
								$params['round_status']='2';
							}
						}
						
						$this->_createPaymentRoundByPID($params);
						$cal_i++;
					}
				}else{
					$params = array();
					$params['payment_id'] = $payment_id;
					$params['round_name'] = '1';
					$params['start'] = $start_date;
					$params['end'] = $end_date;
					$params['bath'] = $base->get('POST.payment_value');
					//GF::print_r($base->get('POST.payment_start'));
					$this->_createPaymentRoundByPID($params);
				}


			}
			return true;
		}else{
			return false;
		}
	}
	private function _calculateMonth(){
		$base = Base::getInstance();
		$stringStart = (string)$base->get('payment_strat');
		$stringEnd = (string)$base->get('payment_end');
		$begin = new DateTime($stringStart);
		$endc = new DateTime($stringEnd);
		$end = $endc->modify( '+2 day' );

		$interval = new DateInterval('P1M');
		$daterange = new DatePeriod($begin, $interval ,$end);

		$last = '';
		$endc = new DateTime($stringEnd);
		$ed = $endc->format("Y-m-d");

		$ndate = array();
		$i=0;
		foreach($daterange as $date){
		    $ndate[$i] = $date->format("Y-m-d");
		    $last = $date->format("Y-m-d");
		    $i++;
		}
		$arrReturn = array();
		$num = 0;
		foreach($ndate as $key=>$dts){
			$k = (string)$dts;
			$j = (string)$ndate[$key+1];
			if($ndate[$key+1]==''){
				$endc = $endc->modify( '+1 day' );
				$j = (string)$endc->format("Y-m-d");
			}
			$x = new DateTime($k);
			$y = new DateTime($j);

			$xy = $y->modify( '-1 day' );

			//echo $x->format("Y-m-d")."+++".$xy->format("Y-m-d")."<br />";
			if($ndate[$key+1]<$ed){
				$arrReturn[$num]['start'] = $x->format("Y-m-d");
				$arrReturn[$num]['end'] = $xy->format("Y-m-d");
				$num++;
			}

			//echo (string)$dts;
		}
		//GF::print_r($arrReturn);
		return $arrReturn;
	}
	private function _calculateDay(){
		$base = Base::getInstance();
		$stringStart = (string)$base->get('payment_strat');
		$stringEnd = (string)$base->get('payment_end');
		$begin = new DateTime($stringStart);
		$endc = new DateTime($stringEnd);
		$end = $endc->modify( '+1 day' );

		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);


		$ndate = array();
		$arrReturn = array();
		$num = 0;
		foreach($daterange as $date){
		    $arrReturn[$num]['start'] = $date->format("Y-m-d");
			$arrReturn[$num]['end'] = '-';
			$num++;
		}
		return $arrReturn;
	}
	private function _checkRoundForUpdate($params){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$date_now = date('Y-m-d');
 		$now_time = strtotime($date_now);
 		
 		$effectiveDate = date('Y-m-d', strtotime("+1 months", strtotime($date_now)));
		$effectiveTime = strtotime($effectiveDate);
 		
 		if(count($params)>0){
			foreach($params as $payment){
				if($payment['payment_term']=='2'){
					if(count($payment['round']>0)){
						foreach($payment['round'] as $round){
							
							if($round['round_start']!='' && $round['round_start']!=NULL){
								
								$start_time = strtotime(date("Y-m-d",strtotime($round['round_start'])));
								$end_time = strtotime(date("Y-m-d",strtotime($round['round_end'])));
								
								//echo ($effectiveTime-$start_time)."<br />";
								
								if(($now_time>=$start_time && $now_time<=$end_time) || ($effectiveTime>=$start_time)){
									if($round['round_status']=='1' && $round['round_flag']=='N'){
										$sql = "UPDATE project_project_payment_round SET round_status='2' WHERE round_id=".GF::quote($round['round_id']);
										//echo $sql;
										$res = $db->Execute($sql);
									}
								}
								
							}
							
						}
					}
				}/// ENd Payment Term = 2
				else if($payment['payment_term']=='3'){
					$effectiveDate = date('Y-m-d', strtotime("+1 months", strtotime($date_now)));
					$effectiveTime = strtotime($effectiveDate);
					
					if(count($payment['round']>0)){
						foreach($payment['round'] as $round){
							
							if($round['round_start']!='' && $round['round_start']!=NULL){
								
								$start_time = strtotime(date("Y-m-d",strtotime($round['round_start'])));
								$end_time = strtotime(date("Y-m-d",strtotime($round['round_end'])));
								
								echo ($effectiveTime-$start_time)."<br />";
								
								if(($now_time>=$start_time && $now_time<=$end_time) || ($effectiveTime>=$start_time)){
									if($round['round_status']=='1' && $round['round_flag']=='N'){
										$sql = "UPDATE project_project_payment_round SET round_status='2' WHERE round_id=".GF::quote($round['round_id']);
										
										$res = $db->Execute($sql);
									}
								}
								
							}
							
						}
					}
					
					
				}/// ENd Payment Term = 1
			}
		}
	}
	private function _paymentList(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$project_id = $base->get('_ids');

 		$sql = "SELECT * FROM project_project_payment WHERE project_id=".GF::quote($project_id);
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['payment_id'] = $res->fields['payment_id'];
			$arrReturn[$i]['payment_type'] = $res->fields['payment_type'];
			$arrReturn[$i]['payment_value'] = $res->fields['payment_value'];
			$arrReturn[$i]['payment_term'] = $res->fields['payment_term'];
			$arrReturn[$i]['payment_status'] = $res->fields['payment_status'];
			$arrReturn[$i]['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn[$i]['type_name'] = $this->_payType($res->fields['payment_type']);
			$arrReturn[$i]['round'] = $this->_paymenbtRoundList($res->fields['payment_id']);
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	public function _paymenbtRoundList($payment_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();



 		$sql = "SELECT * FROM project_project_payment_round WHERE payment_id=".GF::quote($payment_id);
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['round_id'] = $res->fields['round_id'];
			$arrReturn[$i]['round_name'] = $res->fields['round_name'];
			$arrReturn[$i]['round_desc'] = $res->fields['round_desc'];
			$arrReturn[$i]['round_percent'] = $res->fields['round_percent'];
			$arrReturn[$i]['round_bath'] = $res->fields['round_bath'];
			$arrReturn[$i]['round_start'] = $res->fields['round_start'];
			$arrReturn[$i]['round_end'] = $res->fields['round_end'];
			$arrReturn[$i]['round_status'] = $res->fields['round_status'];
			$arrReturn[$i]['round_flag'] = $res->fields['round_flag'];
			$arrReturn[$i]['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn[$i]['history'] = $this->_paymenbtRoundHistoryList($res->fields['round_id']);

			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	public function _paymenbtRoundHistoryList($round_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();



 		$sql = "SELECT * FROM project_project_payment_round_history WHERE round_id=".GF::quote($round_id);
 		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['history_note'] = $res->fields['history_note'];
			$arrReturn[$i]['round_status_text'] = '';
			if($res->fields['round_status']=='1'){
				$arrReturn[$i]['round_status_text'] = 'รอ Confirm';
			}
			else if($res->fields['round_status']=='2'){
				$arrReturn[$i]['round_status_text'] = 'วางบิลได้';
			}
			else if($res->fields['round_status']=='3'){
				$arrReturn[$i]['round_status_text'] = 'ได้รับเงินแล้ว';
			}
			else if($res->fields['round_status']=='4'){
				$arrReturn[$i]['round_status_text'] = 'ได้รับเงินแล้ว';
			}
			else if($res->fields['round_status']=='5'){
				$arrReturn[$i]['round_status_text'] = 'ยกเลิก';
			}
			$arrReturn[$i]['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn[$i]['create_by'] = $res->fields['create_by'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}

	public function _payType($type){
		if($type=="1"){
			return "Project";
		}
		else if($type=='2'){
			return "Hosting";
		}
		else if($type=='3'){
			return "Co-Location";
		}
		else if($type=='4'){
			return "Domainname";
		}
		else if($type=='5'){
			return "Online Marketting";
		}
		else if($type=='6'){
			return "M/A";
		}
		else if($type=='7'){
			return "Service";
		}
		else if($type=='8'){
			return "SSL";
		}
	}
	private function _createPaymentRoundByPID($params){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];
 		
 		$round_status = '1';
 		if(isset($params['round_status'])){
			if($params['round_status']!=''){
				$round_status = $params['round_status'];
			}
		}

 		$sql = "INSERT INTO project_project_payment_round (
											  payment_id,
											  round_name,
											  round_start,
											  round_end,
											  round_bath,
											  round_status,
											  create_dtm,
											  create_by
											)VALUES(
												".GF::quote($params['payment_id']).",
												".GF::quote($params['round_name']).",
												".GF::quote($params['start']).",
												".GF::quote($params['end']).",
												".GF::quote($params['bath']).",
												".GF::quote($round_status).",
												NOW(),
												".GF::quote($user_id)."
											)";
											//echo $sql;
		$res = $db->Execute($sql);
		if($res){
			$round_id = $db->Insert_ID();
			$sql = "INSERT INTO project_project_payment_round_history (
												  round_id,
												  round_status,
												  history_note,
												  create_dtm,
												  create_by
												)VALUES(
													".GF::quote($round_id).",
													".GF::quote($round_status).",
													'create',
													NOW(),
													".GF::quote($user_id)."
												)";
			$res = $db->Execute($sql);
			$this->_sendNotification2('Create',$round_id);
			return true;
		}else{
			return false;
		}
	}
	private function _createPaymentRound(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$start_date = str_replace('/','-',$base->get('POST.round_start'));
		$start_date = date("Y-m-d", strtotime($start_date));

		$end_date = str_replace('/','-',$base->get('POST.round_end'));
		$end_date = date("Y-m-d", strtotime($end_date));


 		$sql = "INSERT INTO project_project_payment_round (
											  payment_id,
											  round_name,
											  round_desc,
											  round_percent,
											  round_bath,
											  round_start,
											  round_end,
											  round_status,
											  round_flag,
											  create_dtm,
											  create_by
											)VALUES(
												".GF::quote($base->get('POST.payment_id')).",
												".GF::quote($base->get('POST.round_name')).",
												".GF::quote($base->get('POST.round_desc')).",
												".GF::quote($base->get('POST.round_percent')).",
												".GF::quote($base->get('POST.round_bath')).",
												".GF::quote($start_date).",
												".GF::quote($end_date).",
												".GF::quote($base->get('POST.round_status')).",
												'Y',
												NOW(),
												".GF::quote($user_id)."
											)";
		$res = $db->Execute($sql);
		if($res){
			$round_id = $db->Insert_ID();
			$sql = "INSERT INTO project_project_payment_round_history (
												  round_id,
												  round_status,
												  history_note,
												  create_dtm,
												  create_by
												)VALUES(
													".GF::quote($round_id).",
													".GF::quote($base->get('POST.round_status')).",
													".GF::quote($base->get('POST.note')).",
													NOW(),
													".GF::quote($user_id)."
												)";
			$res = $db->Execute($sql);
			$this->_sendNotification2('Create',$round_id);
			return true;
		}else{
			return false;
		}
	}
	private function _updatePaymentRound(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$sql = "UPDATE project_project_payment_round SET
											  round_name=".GF::quote($base->get('POST.round_name')).",
											  round_desc=".GF::quote($base->get('POST.round_desc')).",
											  round_percent=".GF::quote($base->get('POST.round_percent')).",
											  round_bath=".GF::quote($base->get('POST.round_bath')).",
											  round_status=".GF::quote($base->get('POST.round_status')).",
											  round_flag='Y',
											  update_dtm=NOW(),
											  update_by=".GF::quote($user_id)."
											WHERE round_id=".GF::quote($base->get('POST.round_id'));
		$res = $db->Execute($sql);
		if($res){
			$round_id = $db->Insert_ID();
			$sql = "INSERT INTO project_project_payment_round_history (
												  round_id,
												  round_status,
												  history_note,
												  create_dtm,
												  create_by
												)VALUES(
													".GF::quote($base->get('POST.round_id')).",
													".GF::quote($base->get('POST.round_status')).",
													".GF::quote($base->get('POST.note')).",
													NOW(),
													".GF::quote($user_id)."
												)";
			$res = $db->Execute($sql);
			$this->_sendNotification2('changed',$base->get('POST.round_id'));
			return true;
		}else{
			return false;
		}
	}
	private function _sendNotification2($type,$round_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$mail = Mailer::getInstance();

 		$member = new Member();

 		$base->set('_round_id_',$round_id);
 		$roundInfomation = $this->_roundInfomation();
 		$base->set('_payment_id_',$roundInfomation['payment_id']);
 		$paymentInfomation = $this->_paymentInfomation();

 		$base->set('_ids',$paymentInfomation['project_id']);
 		$projectInfomation = $this->projectInfomation();
 		$memberInfomation = $member->memberInfomation();



 		$mailHTML = "<b>".$memberInfomation['user_name']."</b> has ".$type." payment status : <b>".$projectInfomation['project_name']."</b><br />";

 		$mailHTML .= "<br />Project Title : <b>".$projectInfomation['project_name']."</b>";
 		$mailHTML .= "<br />Payment Type : <b>".$paymentInfomation['type_name']."</b>";
 		$mailHTML .= "<br />Round : <b>".$roundInfomation['round_name']."</b>";
 		$mailHTML .= "<br />Round Description : <b>".$roundInfomation['round_desc']."</b>";
 		$mailHTML .= "<br />Percent : <b>".$roundInfomation['round_percent']."</b>";
 		$mailHTML .= "<br />Bath : <b>".$roundInfomation['round_bath']."</b>";

 		$mailHTML .= "<br />Status : <b>".$roundInfomation['status_text']."</b>";
 		$kk = end($roundInfomation['history']);
 		$mailHTML .= "<br />Note : <b>".$kk['history_note']."</b>";

 		foreach($projectInfomation['owner'] as $mailMember){
			$mail->Subject = '[Payment Mail][Owner]::'.$memberInfomation['user_name']." has ".$type." payment status : ".$projectInfomation['project_name'];
			$mail->msgBody = $mailHTML;

			$mail->addAddressEmail = $mailMember['user_email'];
			$mail->addAddressName = $mailMember['user_name'];
			$mail->sendMail();
		}
		$mail->Subject = '[Payment Mail]::'.$memberInfomation['user_name']." has ".$type." payment status : ".$projectInfomation['project_name'];
 		$mail->SendingToNoti();
 		$mail->Subject = '[Payment Mail]::'.$memberInfomation['user_name']." has ".$type." payment status : ".$projectInfomation['project_name'];
 		$mail->SendingToNotiAccount();
	}
	private function _deletePaymentRound(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$round_id = $base->get('_ids');

 		$sql = "DELETE FROM project_project_payment_round WHERE round_id=".GF::quote($round_id);
		$res = $db->Execute($sql);
		if($res){

			$sql = "DELETE FROM project_project_payment_round_history WHERE round_id=".GF::quote($round_id);
			$res = $db->Execute($sql);
			return true;
		}else{
			return false;
		}
	}
	private function _deletePayment(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$payment_id = $base->get('_ids');

 		$sql = "DELETE FROM project_project_payment WHERE payment_id=".GF::quote($payment_id);
		$res = $db->Execute($sql);

 		$sql = "DELETE FROM project_project_payment_round WHERE payment_id=".GF::quote($payment_id);
		$res = $db->Execute($sql);
		if($res){

			$sql = "SELECT * FROM project_project_payment_round WHERE payment_id=".GF::quote($payment_id);
			$res = $db->Execute($sql);

			while(!$res->EOF){
				$sql = "DELETE FROM project_project_payment_round_history WHERE round_id=".GF::quote($res->fields['round_id']);
				$res_d = $db->Execute($sql);
				$res->MoveNext();
			}
			$res->Close();


			return true;
		}else{
			return false;
		}
	}
	private function _updatePaymentValue(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$payment_id = $base->get('_ids');
 		$payment_value = $base->get('GET.value');

 		$sql = "UPDATE project_project_payment SET payment_value=".GF::quote($payment_value)." WHERE payment_id=".GF::quote($payment_id);
		$res = $db->Execute($sql);
		if($res){

			return true;
		}else{
			return false;
		}
	}

	private function _createComment(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

 		$sql = "INSERT INTO project_project_comment (
											  project_id,
											  comment_desc,
											  comment_type,
											  create_dtm,
											  create_by
											)VALUES(
												".GF::quote($base->get('POST.project_id')).",
												".GF::quote($base->get('POST.comment_text')).",
												".GF::quote($base->get('POST.comment_type')).",
												NOW(),
												".GF::quote($user_id)."
											)";
		$res = $db->Execute($sql);
		if($res){
			return true;
		}else{
			return false;
		}
	}
	private function _updateFilename(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$sql = "UPDATE project_project_file SET file_title=".GF::quote(base64_decode($base->get('_ids')))." WHERE file_id=".GF::quote($base->get('GET.myid'));
 		$res = $db->Execute($sql);
 	}

	public function _roundInfomation(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$round_id = $base->get('_round_id_');

 		if(!empty($round_id)){
			$sql = "SELECT * FROM project_project_payment_round WHERE round_id=".GF::quote($round_id);
	 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);
			$arrReturn = array();

			$arrReturn['round_id'] = $res->fields['round_id'];
			$arrReturn['round_name'] = $res->fields['round_name'];
			$arrReturn['payment_id'] = $res->fields['payment_id'];
			$arrReturn['round_desc'] = $res->fields['round_desc'];
			$arrReturn['round_percent'] = $res->fields['round_percent'];
			$arrReturn['round_bath'] = $res->fields['round_bath'];
			$arrReturn['round_start'] = $res->fields['round_start'];
			$arrReturn['round_end'] = $res->fields['round_end'];
			$arrReturn['round_status'] = $res->fields['round_status'];

		    $stt_text = 'รอ Confirm';
		    if($res->fields['round_status']=='2'){
				$stt_text = 'วางบิลได้';
			}
			else if($res->fields['round_status']=='3'){
				$stt_text = 'วางบิลแล้ว';
			}
			else if($res->fields['round_status']=='4'){
				$stt_text = 'ได้รับเงินแล้ว';
			}
			else if($res->fields['round_status']=='5'){
				$stt_text = 'ยกเลิก';
			}
			$arrReturn['status_text'] = $stt_text;
			$arrReturn['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn['history'] = $this->_paymenbtRoundHistoryList($res->fields['round_id']);
			//GF::print_r($arrReturn['history']);

			return $arrReturn;

		}else{
			return false;
		}
	}
	private function _paymentInfomation(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$pay_id = $base->get('_payment_id_');

 		if(!empty($pay_id)){
			$sql = "SELECT * FROM project_project_payment WHERE payment_id=".GF::quote($pay_id);
	 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);
			$arrReturn = array();

			$arrReturn['payment_id'] = $res->fields['payment_id'];
			$arrReturn['payment_type'] = $res->fields['payment_type'];
			$arrReturn['payment_value'] = $res->fields['payment_value'];
			$arrReturn['payment_status'] = $res->fields['payment_status'];
			$arrReturn['project_id'] = $res->fields['project_id'];
			$arrReturn['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn['type_name'] = $this->_payType($res->fields['payment_type']);

			return $arrReturn;

		}else{
			return false;
		}
	}
	private function _deleteProject(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$project_id = $base->get('_ids');

 		$sql = "UPDATE project_project SET active_status='C' WHERE project_id=".GF::quote($project_id);
		$res = $db->Execute($sql);

 		if($res){
			return true;
		}else{
			return false;
		}
	}
	private function _deleteComment(){
		$base = Base::getInstance();
 		$db = DB::getInstance();


 		$comment_id = $base->get('_ids');

 		$sql = "DELETE FROM project_project_comment WHERE comment_id=".GF::quote($comment_id);
		$res = $db->Execute($sql);

	}



 }
?>