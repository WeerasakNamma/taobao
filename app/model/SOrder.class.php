<?php
 class SOrder {

 	public function additem(){
		$resultreturn = $this->_additem();
		return $resultreturn;
	}
	public function updateQTY(){
		$resultreturn = $this->_updateQTY();
		return $resultreturn;
	}
	public function updateComment(){
		$resultreturn = $this->_updateComment();
		return $resultreturn;
	}




	public function updateTempQTY(){
		$resultreturn = $this->_updateTempQTY();
		return $resultreturn;
	}
	public function updateTempComment(){
		$resultreturn = $this->_updateTempComment();
		return $resultreturn;
	}





	public function updateOption(){
		$resultreturn = $this->_updateOption();
		return $resultreturn;
	}
	public function removeItem($key){
		$resultreturn = $this->_removeItem($key);
		return $resultreturn;
	}
	public function setKeyForOrder(){
		$resultreturn = $this->_setKeyForOrder();
		return $resultreturn;
	}
	public function takeOrder(){
		$resultreturn = $this->_takeOrder();
		return $resultreturn;
	}
	public function orderList(){
		$resultreturn = $this->_orderList();
		return $resultreturn;
	}
	public function orderInfo(){
		$resultreturn = $this->_orderInfo();
		return $resultreturn;
	}
	public function itemList(){
		$resultreturn = $this->_itemList();
		return $resultreturn;
	}
	public function delItem(){
		$resultreturn = $this->_delItem();
		return $resultreturn;
	}
	public function reOrder($orderid){
		$resultreturn = $this->_reOrder($orderid);
		return $resultreturn;
	}
	public function cancelOrder(){
		$resultreturn = $this->_cancelOrder();
		return $resultreturn;
	}
	public function getRate(){
		$resultreturn = $this->_getRate();
		return $resultreturn;
	}
	public function getBank(){
		$resultreturn = $this->_getBank();
		return $resultreturn;
	}
	public function updateSlip(){
		$resultreturn = $this->_updateSlip();
		return $resultreturn;
	}
	public function paymentList(){
		$resultreturn = $this->_paymentList();
		return $resultreturn;
	}
	public function uploadTemp(){
		$resultreturn = $this->_uploadTemp();
		return $resultreturn;
	}
  public function itemInfo(){
		$resultreturn = $this->_itemInfo();
		return $resultreturn;
	}
  public function addRefund(){
		$resultreturn = $this->_addRefund();
		return $resultreturn;
	}
  public function refundlist(){
		$resultreturn = $this->_refundList();
		return $resultreturn;
	}
   public function cancelRefund(){
 		$resultreturn = $this->_cancelRefund();
 		return $resultreturn;
 	}

   public function changeoption(){
 		$resultreturn = $this->_changeoption();
 		return $resultreturn;
 	}

   public function updateItemImg(){
 		$resultreturn = $this->_updateItemImg();
 		return $resultreturn;
 	}
   public function getshipItem(){
		$resultreturn = $this->_getshipItem();
		return $resultreturn;
    }
    public function bankList(){
		$resultreturn = $this->_bankList();
		return $resultreturn;
    }
    public function newbankInfo(){
		$resultreturn = $this->_newbankInfo();
		return $resultreturn;
	}	
	public function get_order_detail(){
		$resultreturn = $this->_getlist_orderdetail();
		return $resultreturn;
	}

 	private function _additem(){

		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$params = $base->get('POST');
 		$_SESSION['thrate'] = GF::getrate();
 		//GF::print_r($_SESSION);

		if(empty($_SESSION['items_tmp'])){
			$_SESSION['items_tmp'] = 0;
		}
		$pdname = 'N';
		$key_opt = '0';
		foreach($_SESSION['item_tmp'] as $keyx=>$valc){
			if($valc['product_name']==$params['product_name']){
				$pdname = 'C';
				$key_opt = $keyx;
			}
		}

		if($pdname=='F'){
			$optionall = count($params['op_name']);
			$kx = 0;
			foreach($params['op_name'] as $key=>$vals){
				//$_SESSION['item'][$key_opt]['product_option'][$key]['op_name'] = $vals;
				//$_SESSION['item'][$key_opt]['product_option'][$key]['op_value'] = $params['op_value'][$key];
				if($_SESSION['item_tmp'][$key_opt]['product_option'][$key]['op_name']==$vals && $_SESSION['item_tmp'][$key_opt]['product_option'][$key]['op_value']==$params['op_value'][$key]){
					$kx++;
				}
			}

			if($kx==$optionall){
				$_SESSION['item_tmp'][$key_opt]['product_qty'] = $params['product_qty']+$_SESSION['item_tmp'][$key_opt]['product_qty'];
			}else{
				$_SESSION['items_tmp'] = $_SESSION['items_tmp']+1;
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_link'] = $params['product_link'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_name'] = $params['product_name'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_image'] = $params['pd_img'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['saller_name'] = $params['shop_name'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['saller_link'] = $params['shop_link'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_qty'] = $params['product_qty'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_price_rmb'] = $params['product_price_rmb'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_price_thb'] = $params['product_price_thb'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['comment'] = $params['note'];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['order_type'] = $params['order_type'];
				foreach($params['op_name'] as $key=>$vals){
					$_SESSION['item_tmp'][$_SESSION['items']]['product_option'][$key]['op_name'] = $vals;
					$_SESSION['item_tmp'][$_SESSION['items']]['product_option'][$key]['op_value'] = $params['op_value'][$key];
					$_SESSION['item_tmp'][$_SESSION['items']]['product_option'][$key]['op_name_th'] = $params['op_name_th'][$key];
					$_SESSION['item_tmp'][$_SESSION['items']]['product_option'][$key]['op_value_th'] = $params['op_value_th'][$key];
				}
			}


		}else{
			$_SESSION['items_tmp'] = $_SESSION['items_tmp']+1;
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_link'] = $params['product_link'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_name'] = $params['product_name'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_image'] = $params['pd_img'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['saller_name'] = $params['shop_name'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['saller_link'] = $params['shop_link'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_qty'] = $params['product_qty'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_price_rmb'] = $params['product_price_rmb'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_price_thb'] = $params['product_price_thb'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['comment'] = $params['note'];
			$_SESSION['item_tmp'][$_SESSION['items_tmp']]['order_type'] = $params['order_type'];
			foreach($params['op_name'] as $key=>$vals){
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_option'][$key]['op_name'] = $vals;
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_option'][$key]['op_value'] = $params['op_value'][$key];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_option'][$key]['op_name_th'] = $params['op_name_th'][$key];
				$_SESSION['item_tmp'][$_SESSION['items_tmp']]['product_option'][$key]['op_value_th'] = $params['op_value_th'][$key];
			}
		}
		setcookie('carts', $_SESSION['item_tmp'], time() + (86400 * 2), "/");
		return $_SESSION['item_tmp'];

		//aksort($_SESSION);
	}
	public function addTmpToCart(){
		if(count($_SESSION['item_tmp'])>0){
			foreach ($_SESSION['item_tmp'] as $key => $value) {
				$_SESSION['item'][] = $value;
			}
		}

		$_SESSION['item_tmp'] = [];
	}
	public function countSessions(){
		$qty = 0;
		$yuans = 0;
		if(count($_SESSION['item'])>0){
			foreach($_SESSION['item'] as $key=>$vals){
				if(count($_SESSION['orderkey'])>0){
					$addr = array_search($key,$_SESSION['orderkey']);
					//echo $addr;
					if($addr!==false){
						$qty = intval($vals['product_qty'])+$qty;
						$yuans = intval($vals['product_price_rmb'])+$yuans;
					}
				}else{
					$qty = intval($vals['product_qty'])+$qty;
					$yuans = (intval($vals['product_price_rmb'])*intval($vals['product_qty']))+$yuans;
				}

			}
		}
		$_SESSION['allqty'] = $qty;
		$_SESSION['allyuan'] = $yuans;
	}
	private function _updateQTY(){
		$base = Base::getInstance();

 		$params = $base->get('GET');
		$_SESSION['item'][$params['key']]['product_qty'] = $params['value'];
		setcookie('carts', $_SESSION['item'], time() + (86400 * 2), "/");
	}
	private function _updateComment(){
		$base = Base::getInstance();

 		$params = $base->get('GET');
		$_SESSION['item'][$params['key']]['comment'] = $params['value'];
		setcookie('carts', $_SESSION['item'], time() + (86400 * 2), "/");
	}
	private function _updateOption(){
		$base = Base::getInstance();

 		$params = $base->get('GET');
		$_SESSION['item'][$params['key']]['product_option'][$params['keyn']]['op_value'] = $params['value'];
		$_SESSION['item'][$params['key']]['product_option'][$params['keyn']]['op_value_th'] = $params['value'];
		setcookie('carts', $_SESSION['item'], time() + (86400 * 2), "/");
	}

	private function _updateTempQTY(){
		$base = Base::getInstance();

 		$params = $base->get('GET');
		$_SESSION['item_tmp'][$params['key']]['product_qty'] = $params['value'];
		setcookie('carts', $_SESSION['item_tmp'], time() + (86400 * 2), "/");
	}
	private function _updateTempComment(){
		$base = Base::getInstance();

 		$params = $base->get('GET');
		$_SESSION['item_tmp'][$params['key']]['comment'] = $params['value'];
		setcookie('carts', $_SESSION['item_tmp'], time() + (86400 * 2), "/");
	}
	private function _updateTempOption(){
		$base = Base::getInstance();

 		$params = $base->get('GET');
		$_SESSION['item_tmp'][$params['key']]['product_option'][$params['keyn']]['op_value'] = $params['value'];
		$_SESSION['item_tmp'][$params['key']]['product_option'][$params['keyn']]['op_value_th'] = $params['value'];
		setcookie('carts', $_SESSION['item_tmp'], time() + (86400 * 2), "/");
	}


	private function _removeItem(){
		$base = Base::getInstance();


		//GF::print_r([$base->get('GET.key')]);
		if(isset($_SESSION['item'][$base->get('GET.key')])){
			unset($_SESSION['item'][$base->get('GET.key')]);
			setcookie('carts', $_SESSION['item'], time() + (86400 * 2), "/");
		}
	}
	private function _setKeyForOrder(){
		$base = Base::getInstance();
		$_SESSION['orderkey'] = array();
		$keys = $base->get('POST.select_product');
		$i = 0;
		if(count($keys)>0){
			foreach($keys as $vals){
				$_SESSION['orderkey'][$i] = $vals;
				$i++;
			}
		}
		setcookie('carts', $_SESSION['item'], time() + (86400 * 2), "/");
		return true;
	}


	private function _takeOrder(){

		$base = Base::getInstance();
		$db = DB::getInstance();
      $db2 = DB2::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$array_param = $base->get('POST');

		$lasrorder = $this->_getlastorder();

		$year = substr($lasrorder,0,4);
		$month = substr($lasrorder,4,2);
		$times = substr($lasrorder,6,4);

		$c_year = date('Y');
		$c_month = date('m');

		$new_time = '';
		$order_id = '';
		if($year==$c_year && $month==$c_month){
			$new_time = intval($times)+1;
			$new_time = str_pad($new_time, 2, "0", STR_PAD_LEFT);
			$order_id = $year.$month.$new_time;
		}else{
			$order_id = $c_year.$c_month."01";
		}



		//$order_id = ;//(1000+$_SESSION['user_id_front']).date('dmY');
		$my_id = '';

        $pay_ship_yuan = $_SESSION['order_des']['pay_ship']/$array_param['web_rate'];

		$sql = "INSERT INTO web_order (
							order_id,
							member_id,
							order_payment,
							order_ship_cn,
							order_box,
							order_ship_th,
							order_comment,
							ship_by,
                            old_rate,
							rate,
							order_pay_ship,
                            order_pay_ship_yuan,
							order_discount,
							status,
							active_status,
							create_dtm,
							update_dtm
						) VALUES (
							".GF::quote($order_id).",
							".GF::quote($user_id).",
							".GF::quote($array_param['order_payment']).",
							".GF::quote($array_param['order_ship_cn']).",
							".GF::quote($array_param['order_box']).",
							".GF::quote($array_param['order_ship_th']).",
							".GF::quote($array_param['order_comment']).",
							".GF::quote($array_param['ship_by']).",
                            ".GF::quote($array_param['web_rate']).",
							".GF::quote($array_param['web_rate']).",
							".GF::quote($_SESSION['order_des']['pay_ship']).",
                            ".GF::quote($pay_ship_yuan).",
							".GF::quote($_SESSION['order_des']['discount']).",
							'1',
							'O',
							NOW(),
							NOW()
						)";
		 $res = $db->Execute($sql);
		 $last_id = $db->Insert_ID();
		 $my_id = $last_id;
		 if($res){

		 	if($_SESSION['coupon_code']!=''){
		 		$setting = new SSetting();
		 		$base->set('e_no',$_SESSION['coupon_code']);
		 		$ecoupon = $setting->getEcouponByCode();
				$sql = "INSERT INTO project_ecoupon_log (
														e_id,
														order_id,
                                                        user_id,
														create_dtm
														)VALUES(
														".GF::quote($ecoupon['e_id']).",
														".GF::quote($my_id).",
                                                        ".GF::quote($user_id).",
														NOW()
														)";
				$db->Execute($sql);
			}

		 	foreach($_SESSION['item'] as $key=>$vals){
		 		$icheck = true;
		 		if(count($_SESSION['orderkey'])>0){
		 			$ckks = array_search($key,$_SESSION['orderkey']);
					if($ckks!==false){
						$icheck = true;
					}else{
						$icheck = false;
					}
				}
				if($icheck){

					$img_name = GF::randomNum(25);
					$file = $vals['product_image'];
					//$data = file_get_contents($file);
					$new = $base->get('BASEDIR').'/uploads/product/'.$img_name.".jpg";
                    //file_put_contents($new, $data);
                    $checkhttp = substr($file,0,2);
                    if($checkhttp=='//'){
                        $file = 'http:'.$file;
                    }
                    $ch = curl_init ($file);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
                    $raw=curl_exec($ch);
                    curl_close ($ch);
                    // if(file_exists($saveto)){
                    //     unlink($saveto);
                    // }
                    $fp = fopen($new,'x');
                    fwrite($fp, $raw);
                    fclose($fp);

					$vals['saller_name'] = trim($vals['saller_name']);
					if($vals['saller_name']==''){
						$vals['saller_name'] = 'N/A';
					}

					$sql = "INSERT INTO web_order_item (
										order_id,
										product_link,
										product_title,
										product_image,
										product_saller_name,
										product_saller_link,
										product_qty,
										product_price_rmb,
										product_price_thb,
										note,
										order_type,
										ship_by,
										status,
										create_dtm
									) VALUES (
										".GF::quote($last_id).",
										".GF::quote($vals['product_link']).",
										".GF::quote($vals['product_name']).",
										".GF::quote($img_name.'.jpg').",
										".GF::quote($vals['saller_name']).",
										".GF::quote($vals['saller_link']).",
										".GF::quote($vals['product_qty']).",
										".GF::quote($vals['product_price_rmb']).",
										".GF::quote($vals['product_price_thb']).",
										".GF::quote($vals['comment']).",
										".GF::quote($vals['order_type']).",
										".GF::quote($array_param['ship_by']).",
										'A',
										NOW()
									)";
					 $res = $db->Execute($sql);
					 $item_id = $db->Insert_ID();
					 foreach($vals['product_option'] as $k_y=>$val_c){
						 	$sql = "INSERT INTO web_order_item_option (
												item_id,
												option_name,
												option_value,
												option_name_th,
												option_value_th
											) VALUES (
												".GF::quote($item_id).",
												".GF::quote($val_c['op_name']).",
												".GF::quote($val_c['op_value']).",
												".GF::quote($val_c['op_name_th']).",
												".GF::quote($val_c['op_value_th'])."
											)";
							 $res = $db->Execute($sql);
					 }

                unset($_SESSION['item'][$key]);

				}

			}


         $this->_gen_saller_number($my_id);





         // $sql = "INSERT INTO web_order_item (
			// 						order_id,
			// 						product_title,
			// 						product_qty,
			// 						product_price_rmb,
			// 						product_price_thb,
			// 						item_typs_s,
			// 						status,
			// 						create_dtm
			// 					) VALUES (
			// 						".GF::quote($my_id).",
			// 						'ค่าบริการสั่งซื้อ',
			// 						'1',
			// 						".GF::quote($_SESSION['order_buy_service']).",
			// 						".GF::quote($_SESSION['order_buy_service']*GF::getrate()).",
			// 						'IN',
			// 						'W',
			// 						NOW()
			// 					)";
         //
			// $res = $db->Execute($sql);

         //unset($_SESSION['item']);
			unset($_SESSION['items']);
			unset($_SESSION['orderkey']);
			unset($_SESSION['coupon_code']);
			unset($_SESSION['e_amount']);
			unset($_SESSION['e_type']);
			unset($_SESSION['order_des']);
         unset($_SESSION['order_buy_service']);





			/*$member = new Member();

			$userinfo = $member->userInfo();

			$mail_mssg = file_get_contents(BASEURL."/member/order_1/view_mail/".$my_id);
			//echo $mail_mssg.'fffff';
			$array_param['order'] = $order_id;
			$array_param['web_name'] = $userinfo['name'];
			$array_param['web_email'] = $userinfo['email'];
		 	$array_param['mail_body'] = $mail_mssg;
		 	$this->_sendMail($array_param);

		 	$admin = $this->_admininfo();


		 	$array_param['web_name'] = $admin['admin_name'];
			$array_param['web_email'] = $admin['email'];
		 	$array_param['mail_body'] = $mail_mssg;
		 	$this->_sendMail($array_param);*/

			unset($_COOKIE['carts']);
    		setcookie('carts', '', time() - 3600, '/');

         $db2->insert("web_order_rate_history",array(
            "order_id"=>$my_id,
            "rate"=>$array_param['web_rate'],
            "status"=>'1',
            "create_dtm"=>date('Y-m-d H:i:s'),
            "create_by"=>$user_id

         ));
        /*$link = $base->get('BASEURL').'/#/buyship/view/'.$last_id;
        $dataReplace = array(
            "{CUST_NAME}"=>$memberInfomation['user_name'],
            "{LINK}"=>GF::shorturl($link,'1')
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('2',$memberInfomation['ref_id']);
        $mail->msgHTML = GF::mailtemplate('2',$dataReplace,$memberInfomation['ref_id']);
        $mail->addAddressEmail = $memberInfomation['user_email'];
        $mail->addAddressName = $memberInfomation['user_name'];
        $mail->sendMail();*/

		 	return $my_id;
		 }else{
		 	return false;
		 }
	}
	private function _gen_saller_number($order_id){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$base->set('_ids',$order_id);
		$item_list = $this->_itemList();

		$old_shop = '';
		$item_number = 1;
		foreach($item_list as $vals){
			if($vals['product_saller_name']!=$old_shop && $vals['product_saller_name']!=''){
				$old_shop=$vals['product_saller_name'];
				$sql = 'UPDATE web_order_item SET saller_number='.$item_number." WHERE product_saller_name='".$vals['product_saller_name']."'";
				$res = $db->Execute($sql);
				$item_number++;
			}

		}
		$sql = 'UPDATE web_order_item SET saller_number='.$item_number." WHERE order_id='".$order_id."' AND product_saller_name='' ";
		$res = $db->Execute($sql);

	}
	private function _orderList(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$dtatReturn=array();



		$cond = '';


		if($_SESSION['odr_search']['status']!=''){

			if($_SESSION['odr_search']['status']=='ALL'){
				//$cond = " AND status=".GF::quote($type);
			}else if($_SESSION['odr_search']['status']=='5'){ //edit by hatairat 04-12-64
				$cond .= " AND o.status='3' AND o.export='S' ";
			  }else{
				$cond .= " AND o.status=".GF::quote($_SESSION['odr_search']['status']);
			}
		}
		if($_SESSION['odr_search']['order_number']!=''){
			$cond .= " AND o.order_number LIKE '%".$_SESSION['odr_search']['order_number']."%' ";
		}
		if($_SESSION['odr_search']['create_dtm']!=''){
			$date_check = date('Y-m-d',strtotime($_SESSION['odr_search']['create_dtm']));
			$cond .= " AND o.create_dtm >=".GF::quote($date_check);
		}
		//Weerasak 17-11-2564
		//$sql = "SELECT * FROM web_order WHERE  active_status='O' AND order_type='1' AND member_id=".GF::quote($user_id)." ".$cond." ORDER BY update_dtm DESC,create_dtm DESC";
		
		/* รอตรวจสอบ ถ้าถูกลบทิ้งได้เลย
		$sql = "SELECT o.*,p.payment_amount,
		If((o.order_price - p.payment_amount) < 0 ,0		
		,(o.order_price - p.payment_amount)) as balance 
		FROM web_order o
		LEFT JOIN (SELECT SUM(payment_amount) payment_amount,order_id,member_id
								FROM web_payment WHERE status = 'A'
								GROUP BY order_id,member_id) p ON (p.order_id = o.id AND p.member_id = ".GF::quote($user_id)." )
		WHERE  o.active_status='O' AND o.order_type='1' 
		AND o.member_id= ".GF::quote($user_id)." ".$cond."
		ORDER BY o.update_dtm DESC,o.create_dtm DESC";
		*/
		$sql = "SELECT DISTINCT 
			*,(sum_price - payment_amount 
				) AS over_payamount
				,(sum_price - payment_amount_web) as overdue
				,CASE WHEN payment_amount_web > 0 THEN
						CASE WHEN (sum_price - payment_amount) > 0 THEN (sum_price - order_price)
						ELSE (sum_price - order_price) END
					WHEN status = '7' THEN sum_price
					ELSE 0.00
				END balance
				,case when abs((sum_price - payment_amount_web )) > 0 THEN
abs((sum_price - payment_amount_web )) - COALESCE((SELECT payment_amount FROM web_payment WHERE member_id = P.member_id and order_id = P.id  and `status` = 'A' ORDER BY id desc LIMIT 1),0) ELSE 0 END as overdue_paid
		  	from (
			SELECT K.*,case WHEN order_price < sum_price && order_price > 0 then 
			order_price - payment_amount_web + sum_price 
			else payment_amount_web end payment_amount,
			(SELECT GROUP_CONCAT(remarks) FROM web_payment wp WHERE wp.order_id = K.id AND wp.member_id = K.member_id) as remarks
			FROM (
								 SELECT
									o.*,
									IFNULL(ROUND(((i.order_price_rmb * o.old_rate) + o.order_pay_ship) - o.order_discount,2),o.order_price) as sum_price,
									IFNULL(p.payment_amount,0) AS payment_amount_web
									,CASE WHEN (SELECT COUNT(*) FROM project_withdraw WHERE status = 'A' AND user_id = o.member_id AND order_id = o.id) = 0 THEN 'N' ELSE 'Y' END AS is_refunded
						FROM
							web_order o
							LEFT JOIN ( SELECT SUM( payment_amount ) payment_amount, order_id, member_id 
													FROM web_payment WHERE STATUS = 'A' and order_type='1' GROUP BY order_id, member_id ) p ON ( p.order_id = o.id AND p.member_id = ".GF::quote($user_id)." ) 
							LEFT JOIN (SELECT order_id,ROUND(SUM(product_price_rmb * product_qty),2) as order_price_rmb 
													FROM web_order_item WHERE status = 'A' AND item_typs_s = 'NR' GROUP BY order_id) i ON i.order_id = o.id
						WHERE
							o.active_status = 'O' 
							AND o.order_type = '1' 
							AND o.member_id = ".GF::quote($user_id)." ".$cond." 							
				) K	
				) P
				ORDER BY
				update_dtm DESC,
				create_dtm DESC";
		// echo $sql;
		$res = $db->Execute($sql);
		$count = $res->NumRows();

			$base->set('allPage',ceil($count/20));

		$page = $base->get('GET.page');

		$numr = 20;
		$start = "";
		$end = "";
		if($page==1||empty($page)){
			$start = 0;
			$page = 1;
			//$end = $numr;
		}else{
			//$end = ($page*$numr);
			$start = ($page*$numr)-$numr;
		}
		 $condNum = " LIMIT ".$start.",".$numr;

		 $base->set('currpage',$page);

		$res = $db->Execute($sql.$condNum);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['order_id'] = $res->fields['order_id'];
			$ArrList[$i]['order_number'] = $res->fields['order_number'];
			$ArrList[$i]['status'] = $res->fields['status'];
            $ArrList[$i]['order_buy_rmb'] = $res->fields['order_buy_rmb']; //Weerasak 21-10-2564
			$ArrList[$i]['order_price'] = $res->fields['order_price'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			if($res->fields['export'] == 'S')
				$ArrList[$i]['status_text'] = $this->_convStatus('order','5');
			else
				$ArrList[$i]['status_text'] = $this->_convStatus('order',$res->fields['status']);
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
			$ArrList[$i]['rate'] = $res->fields['rate'];
            $ArrList[$i]['old_rate'] = $res->fields['old_rate'];
			$ArrList[$i]['order_pay_ship'] = $res->fields['order_pay_ship'];
            $ArrList[$i]['order_pay_ship_yuan'] = $res->fields['order_pay_ship_yuan'];
            $ArrList[$i]['order_pay_cn'] = $res->fields['order_pay_cn'];
            $ArrList[$i]['export'] = $res->fields['export'];
            $ArrList[$i]['refund_wallet'] = $res->fields['refund_wallet'];
			$ArrList[$i]['order_discount'] = $res->fields['order_discount'];
			$ArrList[$i]['remarks'] = $res->fields['remarks'];
			$ArrList[$i]['overdue_paid'] = $res->fields['overdue_paid'];
			$ArrList[$i]['is_refunded'] = $res->fields['is_refunded'];
			//Weerasak 01-12-2564
			if($ArrList[$i]['status']== '7'){
				$ArrList[$i]['payment'] = $res->fields['payment_amount_web'] + $res->fields['over_payamount'];
				$ArrList[$i]['payment_balance'] = abs($res->fields['balance']); //Weerasak 17-11-2564
			}
			$ArrList[$i]['sum_price'] = $res->fields['sum_price'];
			//End Edit
			$ArrList[$i]['items'] = $this->_countItem($res->fields['id']);
			$ArrList[$i]['countprice'] = $this->_countItemPrice($res->fields['id']);			

            $totals = (($ArrList[$i]['countprice']['product_price_rmb']*$ArrList[$i]['old_rate'])+$ArrList[$i]['order_pay_ship'])-$ArrList[$i]['order_discount'];
            $totals = $totals+$ArrList[$i]['countprice']['product_price_thb'];

            if($ArrList[$i]['status']=='C'){$totals=0;}
            $ArrList[$i]['total'] = $totals;

            $true_price = ($ArrList[$i]['countprice']['product_buy_rmb'])*$ArrList[$i]['rate'];
            $true_price += ($ArrList[$i]['countprice']['ship_cn']*$ArrList[$i]['rate']);
            $true_price -= $ArrList[$i]['order_discount'];
            if($ArrList[$i]['export']=='N'){$true_price=0;}

            $ArrList[$i]['true_price'] = 0;
			$ArrList[$i]['order_refund'] =0;

            if($ArrList[$i]['export']=='S'){
                $ArrList[$i]['true_price'] = $true_price;
				
				//คำนวณ refund Weerasak 27-10-2564
				$order_pay_bath = (($ArrList[$i]['countprice']['product_price_rmb']*$ArrList[$i]['old_rate']) + $ArrList[$i]['order_pay_ship'])-$ArrList[$i]['order_discount'];
				$netOrder_pay_bath = ($ArrList[$i]['countprice']['total_order_buy_amount'] * $ArrList[$i]['rate']) - $order_pay_bath;
				$ArrList[$i]['order_refund'] = $netOrder_pay_bath;
				//END
            }

			$i++;
			$res->MoveNext();
		}

		//print_r();
		$dtatReturn['data'] = $ArrList;

		return $dtatReturn;
	}
	public function _countItem($order_id){
			$base = Base::getInstance();
			$db = DB::getInstance();
			$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D'";
			//echo $sql;
			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$i += $res->fields['product_qty'];
				$res->MoveNext();
			}
			return $i;
		}
		// private function _countItemPrice($order_id){
		// 	$base = Base::getInstance();
		// 	$db = DB::getInstance();
      //
      //    $sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D'";
		// 	//echo $sql;
		// 	$res = $db->Execute($sql);
		// 	$i=0;
		// 	$ArrList = array();
		// 	$product_price_rmb = 0;
		// 	$product_price_thb = 0;
		// 	$product_buy_rmb = 0;
		// 	$ship_cn_cn = 0;
		// 	$ship_cn_th = 0;
		// 	$ship_all = 0;
		// 	$refund = 0;
		// 	$unfund = 0;
      //    $buy_qty = 0;;
      //    $product_qty = 0;
		// 	while(!$res->EOF){
		// 		if($res->fields['item_typs_s']=='NR'){
		// 			$product_price_rmb = $product_price_rmb+($res->fields['product_price_rmb']*$res->fields['product_qty']);
		// 			$product_buy_rmb = $product_buy_rmb+($res->fields['product_buy_rmb']);
      //          $product_qty += $res->fields['product_qty'];
      //          $buy_qty += $res->fields['product_buy_qty'];
		// 		}
      //
		// 		if($res->fields['item_typs_s']=='DE'){
		// 			$product_price_thb = $product_price_thb-($res->fields['product_price_thb']);
		// 		}
		// 		if($res->fields['item_typs_s']=='IN'){
		// 			$product_price_thb = $product_price_thb+($res->fields['product_price_thb']);
		// 		}
      //
		// 		$ship_cn_cn = $product_price_rmb+$res->fields['ship_cn_cn'];
		// 		$ship_cn_th = $product_price_rmb+$res->fields['ship_cn_th'];
		// 		$ship_all = $product_price_rmb+$res->fields['ship_all'];
		// 		$refund = $product_price_rmb+$res->fields['refund'];
		// 		$unfund = $product_price_rmb+$res->fields['unfund'];
		// 		$i++;
		// 		$res->MoveNext();
		// 	}
		// 	$ArrList['product_price_rmb'] = $product_price_rmb;
		// 	$ArrList['product_price_thb'] = $product_price_thb;
		// 	$ArrList['product_buy_rmb'] = $product_buy_rmb;
      //    $ArrList['product_buy_qty'] = $buy_qty;
      //    $ArrList['product_qty'] = $product_qty;
		// 	$ArrList['ship_cn_cn'] = $ship_cn_cn;
		// 	$ArrList['ship_cn_th'] = $ship_cn_th;
		// 	$ArrList['ship_all'] = $ship_all;
		// 	$ArrList['refund'] = $refund;
		// 	$ArrList['unfund'] = $unfund;
		// 	$ArrList['all'] = ($product_price_thb+$ship_cn_cn+$ship_cn_th+$ship_all+$refund)-$unfund;
		// 	$ArrList['all'] = number_format($ArrList['all'],2);
		// 	//GF::print_r($ArrList);
		// 	return $ArrList;
		// }
   public function _countItemPrice($order_id){
         $base = Base::getInstance();
         $db = DB::getInstance();
         $db2 =  DB2::getInstance();

         $sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D'";
         //echo $sql;
         $res = $db->Execute($sql);
         $i=0;
         $ArrList = array();
         $product_price_rmb = 0;
         $product_price_thb = 0;
         $product_buy_rmb = 0;
         $ship_cn_cn = 0;
         $ship_cn_th = 0;
         $ship_all = 0;
         $refund = 0;
         $unfund = 0;
         $buy_qty = 0;;
         $product_qty = 0;
         while(!$res->EOF){



            if($res->fields['item_typs_s']=='NR'){

               if($res->fields['product_qty']=='0'){
                  $res->fields['product_price_rmb'] = 0;
               }
               if($res->fields['product_buy_qty']=='0'){
                  $res->fields['product_buy_rmb'] = 0;
               }
               $product_price_rmb = $product_price_rmb+($res->fields['product_price_rmb']*$res->fields['product_qty']);
               $product_buy_rmb = $product_buy_rmb+($res->fields['product_buy_rmb']*$res->fields['product_buy_qty']);
               //echo $res->fields['order_id']."-".$product_buy_rmb."<br />";
               $product_qty += $res->fields['product_qty'];
               $buy_qty += $res->fields['product_buy_qty'];
            }

            if($res->fields['item_typs_s']=='DE'){
               $product_price_thb = $product_price_thb-($res->fields['product_price_thb']);
            }
            if($res->fields['item_typs_s']=='IN'){
               $product_price_thb = $product_price_thb+($res->fields['product_price_thb']);
            }



            $ship_cn_cn = $product_price_rmb+$res->fields['ship_cn_cn'];
            $ship_cn_th = $product_price_rmb+$res->fields['ship_cn_th'];
            $ship_all = $product_price_rmb+$res->fields['ship_all'];
            $refund = $product_price_rmb+$res->fields['refund'];
            $unfund = $product_price_rmb+$res->fields['unfund'];
            $i++;
            $res->MoveNext();
         }

         $arrin = array();

         $sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($order_id)." AND status!='D' GROUP BY saller_number";
         $res = $db->Execute($sql);
         $xc=0;
         while(!$res->EOF){
            $arrin[$xc] = $res->fields['saller_number'];
            $xc++;
            $res->MoveNext();
         }

         $ship_cn = 0;

         //Edit By Weerasak 17-10-2564
         $saller_amount = 0;
         $saller_discount = 0;
         $saller_shipping = 0;
         $total_order_buy_amount  = 0;
        //End edit

         if(count($arrin)>0){
            $addIn = implode(',', $arrin);
            $sql = "SELECT * FROM web_order_item_ship WHERE order_id=".GF::quote($order_id)." AND saller_id IN(".$addIn.")";
            $res = $db->Execute($sql);
                while(!$res->EOF){ //พี่เอส เอา ! ออก เพราะว่า กรณีไม่มีค่า ก็ปล่อยให้มันรันไป
                $ship_cn += $res->fields['price_rmb'];
                    //Edit By Weerasak 17-10-2564
                    $saller_amount += $res->fields['saller_amount_rmb'];
                    $saller_discount += $res->fields['saller_discount_rmb'];
                    $saller_shipping += $res->fields['saller_shipping_rmb'];
                    //End edit
                $res->MoveNext();
                }			 	
			//Weerasak ค่าส่ง
			if($saller_shipping > 0)
				$saller_shipping = $ship_cn;
            
            // if($saller_amount == 0)
            //     $saller_amount = $ship_cn;
			//End
         }//Weerasak
         if($saller_amount == 0)
         {
            $total_order_buy_amount = $product_buy_rmb + $ship_cn;
         }else
         {
            $total_order_buy_amount = ($saller_amount + $saller_shipping) - $saller_discount;
         }
         //End edit
         $ArrList['product_price_rmb'] = $product_price_rmb;
         $ArrList['product_price_thb'] = $product_price_thb;
         $ArrList['product_buy_rmb'] = $product_buy_rmb;
         $ArrList['product_buy_qty'] = $buy_qty;
         $ArrList['product_qty'] = $product_qty;
         $ArrList['ship_cn'] = $ship_cn;
         $ArrList['ship_cn_cn'] = $ship_cn_cn;
         $ArrList['ship_cn_th'] = $ship_cn_th;
         $ArrList['ship_all'] = $ship_all;
         $ArrList['refund'] = $refund;
         $ArrList['unfund'] = $unfund;
         $ArrList['all'] = ($product_price_thb+$ship_cn_cn+$ship_cn_th+$ship_all+$refund)-$unfund;
         $ArrList['all'] = number_format($ArrList['all'],2);
         $ArrList['total_order_buy_amount'] = $total_order_buy_amount; //Edit By Weerasak 17-10-2564
         //GF::print_r($ArrList);
         return $ArrList;
      }
   private function _itemList(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$item_id = $base->get('_ids');

		$cond = '';
		if($_SESSION['t_ordernumber']!='' && $sc!=NULL){
			$cond .= " AND product_sku_1 LIKE '%".$_SESSION['t_ordernumber']."%' ";
		}

		$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($item_id)." AND status!='D' ".$cond." ORDER BY product_saller_name ASC";
		//echo $sql;
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['product_link'] = $res->fields['product_link'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['product_title'] = $res->fields['product_title'];
			$ArrList[$i]['product_sku_1'] = $res->fields['product_sku_1'];
			$ArrList[$i]['product_image'] = $res->fields['product_image'];
			$ArrList[$i]['product_qty'] = $res->fields['product_qty'];
			$ArrList[$i]['product_saller_name'] = $res->fields['product_saller_name'];
            $ArrList[$i]['saller_number'] = $res->fields['saller_number'];
            $ArrList[$i]['saller_image'] = $res->fields['saller_image'];
            $ArrList[$i]['notrack_comment'] = $res->fields['notrack_comment'];
            $ArrList[$i]['saller_image'] = $res->fields['saller_image'];
            $ArrList[$i]['saller_image'] = $res->fields['saller_image'];
			$ArrList[$i]['product_saller_link'] = $res->fields['product_saller_link'];
			$ArrList[$i]['product_price_rmb'] = $res->fields['product_price_rmb'];
			$ArrList[$i]['product_price_thb'] = $res->fields['product_price_thb'];
			$ArrList[$i]['product_buy_rmb'] = $res->fields['product_buy_rmb'];
			$ArrList[$i]['product_buy_qty'] = $res->fields['product_buy_qty'];
			$ArrList[$i]['ship_cn_cn'] = $res->fields['ship_cn_cn'];
			$ArrList[$i]['ship_cn_th'] = $res->fields['ship_cn_th'];
			$ArrList[$i]['ship_all'] = $res->fields['ship_all'];
			$ArrList[$i]['refund'] = $res->fields['refund'];
			$ArrList[$i]['upfund'] = $res->fields['upfund'];
			$ArrList[$i]['note'] = $res->fields['note'];
			$ArrList[$i]['order_comment'] = $res->fields['order_comment'];
			$ArrList[$i]['status'] = $res->fields['status'];
			$ArrList[$i]['order_ship_cn'] = $res->fields['order_ship_cn'];
			$ArrList[$i]['order_ship_th'] = $res->fields['order_ship_th'];
			$ArrList[$i]['order_box'] = $res->fields['order_box'];
         $ArrList[$i]['ref_id'] = $res->fields['ref_id'];
			$ArrList[$i]['num_box'] = $res->fields['num_box'];
			$ArrList[$i]['order_type'] = $res->fields['order_type'];
			$ArrList[$i]['ship_by'] = $res->fields['ship_by'];
			$ArrList[$i]['item_typs_s'] = $res->fields['item_typs_s'];
			$ArrList[$i]['option'] = $this->_itemOption($res->fields['id']);
			$ArrList[$i]['status_text'] = $this->_convStatus('item',$res->fields['status']);
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$newsdate = date("d-m-Y H:i:s", strtotime($res->fields['create_dtm']));
			if($res->fields['update_dtm']!='0000-00-00 00:00:00'){
				$newsdate = date("d-m-Y H:i:s", strtotime($res->fields['update_dtm']));
			}
			$ArrList[$i]['update_dtm'] = $newsdate;
			$i++;
			$res->MoveNext();
		}
		//print_r($ArrList);
		return $ArrList;
	}
	private function _itemOption($item_id){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "SELECT * FROM web_order_item_option WHERE item_id=".GF::quote($item_id);
		//echo $sql;
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['option_name'] = $res->fields['option_name'];
			$ArrList[$i]['option_value'] = $res->fields['option_value'];
			$ArrList[$i]['option_name_th'] = $res->fields['option_name_th'];
			$ArrList[$i]['option_value_th'] = $res->fields['option_value_th'];
			$i++;
			$res->MoveNext();
		}
		//print_r();
		return $ArrList;
	}
	public function _convStatus($type,$status){
		$text_return = '';
		if($type=='order'){
			/*
			if($status=='1'){
				$text_return = 'รอตรวจสอบ';
			}
			else if($status=='2'){
				$text_return = 'รอชำระเงิน';
			}
			else if($status=='C'){
				$text_return = 'ยกเลิก';
			}
			else if($status=='3'){
				$text_return = 'สั่งซื้อ';
			}

			else if($status=='4'){
				$text_return = 'แจ้งจัดส่ง';
			}
			else if($status=='5'){
				$text_return = 'เสร็จสิ้น';
			}
			else if($status=='6'){
				$text_return = 'รอตรวจสอบยอด';
			}
         else if($status=='7'){
				$text_return = 'ค้างชำระเงิน';
			}*/
			$text_return = GF::order_status($status);
		}
		else if($type=='item'){
			if($status=='W'){
				$text_return = 'รอตรวจสอบ';
			}
			else if($status=='V'){
				$text_return = 'กำลังตรวจสอบ';
			}
			else if($status=='C'){
				$text_return = 'ยกเลิก';
			}
			else if($status=='A'){
				$text_return = 'ตรวจสอบแล้ว';
			}
		}
		return $text_return;
	}


	private function _getlastorder(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "SELECT * FROM web_order WHERE 1=1 ORDER BY id DESC LIMIT 1";
		$res = $db->Execute($sql);

		return $res->fields['order_id'];
	}
	private function _orderInfo(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$orderid = $base->get('_ids');

		$sql = "SELECT * FROM web_order WHERE id=".GF::quote($orderid);
		$res = $db->Execute($sql);
		if($res->fields['order_ship_th']=='A100'){
			$res->fields['order_ship_th']='มารับด้วยตนเอง';
		}else{
			$res->fields['order_ship_th'] = $this->_getShipTH($res->fields['order_ship_th']);
		}
		$res->fields['countprice'] = $this->_countItemPrice($orderid);
		//Weerasak 17/11/2564
		$res->fields['export'] = $res->fields['export'];
		if($res->fields['export']=='S')
			$res->fields['status_text'] = $this->_convStatus('order','5');
		else
			$res->fields['status_text'] = $this->_convStatus('order',$res->fields['status']);

		return  $res->fields;

	}
	public function _getShipTH($scode){
		$db = DB::getInstance();
		$sql = "SELECT * FROM web_ship_th WHERE code=".GF::quote($scode);
		$res = $db->Execute($sql);

		$arrReturn = array();
		$arrReturnp['code'] = $res->fields['code'];
		$arrReturn['name']=$res->fields['name'];
		return  $arrReturn;

	}

	public function _getRate(){
		$db = DB::getInstance();
		$sql = "SELECT * FROM web_rate WHERE status='O' LIMIT 1";
		$res = $db->Execute($sql);

		/*$arrReturn = array();
		$arrReturnp['code'] = $res->fields['code'];*/
		return  $res->fields['rate'];

	}
	private function _delItem(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order_item SET status='D' WHERE id=".GF::quote($base->get('GET.item'));
		$res = $db->Execute($sql);
	}
	private function _reOrder(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$order_id = $base->get('GET.order_id');

		$sql = "SELECT * FROM web_order WHERE id=".GF::quote($order_id);
		$res = $db->Execute($sql);

		$base->set('_ids',$order_id);
		$item_list = $this->_itemList();

		$my_id = $order_id;
		//$order_id = (1000+$_SESSION['user_id_front']).date('dmY');
		$lasrorder = $this->_getlastorder();

		$year = substr($lasrorder,0,4);
		$month = substr($lasrorder,4,2);
		$times = substr($lasrorder,6,4);

		$c_year = date('Y');
		$c_month = date('m');

		$new_time = '';
		$order_id = '';
		if($year==$c_year && $month==$c_month){
			$new_time = intval($times)+1;
			$new_time = str_pad($new_time, 2, "0", STR_PAD_LEFT);
			$order_id = $year.$month.$new_time;
		}else{
			$order_id = $c_year.$c_month."01";
		}

		$sql = "INSERT INTO web_order (
							order_id,
							member_id,
							order_payment,
							order_ship_cn,
							order_box	,
							order_ship_th,
							order_comment,
							ship_by,
							rate,
							status,
							active_status,
							create_dtm,
							update_dtm
						) VALUES (
							".GF::quote($order_id).",
							".GF::quote($user_id).",
							".GF::quote($res->fields['order_payment']).",
							".GF::quote($res->fields['order_ship_cn']).",
							".GF::quote($res->fields['order_box']).",
							".GF::quote($res->fields['order_ship_th']).",
							".GF::quote($res->fields['order_comment']).",
							".GF::quote($res->fields['ship_by']).",
							".GF::quote($res->fields['rate']).",
							'W',
							'O',
							NOW(),
							NOW()
						)";
		 $res = $db->Execute($sql);
		 $last_id = $db->Insert_ID();
		 $my_id = $last_id;

		  if($res){
		 	foreach($item_list as $key=>$vals){

		 		$img_name = GF::randomNum(25);
				$file = $base->get('BASEDIR')."/uploads/product/".$vals['product_image'];
				$data = file_get_contents($file);
				$new = $base->get('BASEDIR').'/uploads/product/'.$img_name.".jpg";
				file_put_contents($new, $data);


				$sql = "INSERT INTO web_order_item (
									order_id,
									product_link,
									product_title,
									product_image,
									product_saller_name,
									product_saller_link,
									product_qty,
									product_price_rmb,
									product_price_thb,
									note,
									order_type,
									ship_by,
									status,
									create_dtm
								) VALUES (
									".GF::quote($last_id).",
									".GF::quote($vals['product_link']).",
									".GF::quote($vals['product_title']).",
									".GF::quote($img_name.'.jpg').",
									".GF::quote($vals['product_saller_name']).",
									".GF::quote($vals['product_saller_link']).",
									".GF::quote($vals['product_qty']).",
									".GF::quote($vals['product_price_rmb']).",
									".GF::quote($vals['product_price_thb']).",
									".GF::quote($vals['note']).",
									".GF::quote($vals['order_type']).",
									".GF::quote($vals['ship_by']).",
									'15',
									NOW()
								)";
				 $res = $db->Execute($sql);
				 $item_id = $db->Insert_ID();
				 foreach($vals['option'] as $k_y=>$val_c){
					 	$sql = "INSERT INTO web_order_item_option (
											item_id,
											option_name,
											option_value,
											option_name_th,
											option_value_th
										) VALUES (
											".GF::quote($item_id).",
											".GF::quote($val_c['option_name']).",
											".GF::quote($val_c['option_value']).",
											".GF::quote($val_c['option_name_th']).",
											".GF::quote($val_c['option_value_th'])."
										)";
						 $res = $db->Execute($sql);
				 }
			}

			/*$member = new Member();

			$userinfo = $member->userInfo();

			$mail_mssg = file_get_contents(BASEURL."/member/order_1/view_mail/".$my_id);
			//echo $mail_mssg.'fffff';
			$array_param['order'] = $order_id;
			$array_param['web_name'] = $userinfo['name'];
			$array_param['web_email'] = $userinfo['email'];
		 	$array_param['mail_body'] = $mail_mssg;//$this->_setMailBody($array_param); //print_r($array_param);
		 	$this->_sendMail($array_param);

		 	$admin = $this->_admininfo();


		 	$array_param['web_name'] = $admin['admin_name'];
			$array_param['web_email'] = $admin['email'];
		 	$array_param['mail_body'] = $mail_mssg;//$this->_setMailBodyAdmin($array_param); //print_r($array_param);
		 	$this->_sendMail($array_param);
			return 'A';*/

			return 'A';
		}
	}
	private function _cancelOrder(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE web_order SET status='C' WHERE id=".GF::quote($base->get('GET.order_id'));
		$res = $db->Execute($sql);
	}
	private function _getBank(){

		$base = Base::getInstance();
        $db = DB::getInstance();
        


		$sql = "SELECT * FROM web_payment_bank WHERE status='O'";
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['description'] = $res->fields['description'];
			$ArrList[$i]['image'] = $res->fields['image'];
			$i++;
			$res->MoveNext();
		}
		return $ArrList;
	}
	private function _updateSlip(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$params = $base->get('POST');

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$payment_slip = '';
		if(!empty($_FILES['payment_slip']['name'])){
			$picname = $_FILES['payment_slip']['name'];

			$random = GF::randomNum(25);

			$type = substr(strrchr($picname, '.'), 1);
			$dest_picname_o = $base->get('BASEDIR')."/uploads/slip/".$random.".".$type;

			$tmp_file = $_FILES['payment_slip']['tmp_name'];
			@copy($tmp_file, $dest_picname_o);
			
			$payment_slip = $random.".".$type;
		}

		//Weerasak 20-03-2566
		//$sqlChekPay= "SELECT * FROM web_payment WHERE status IN ('A','W') AND member_id = ".GF::quote($user_id)." AND order_id =".GF::quote($params['order_id']);
		$sqlChekPay = "SELECT 
COALESCE(ROUND((SUM((saller_amount_rmb + saller_shipping_rmb)-saller_discount_rmb) * o.rate),2) -
COALESCE((SELECT SUM(payment_amount) FROM web_payment WHERE status IN ('A','W') AND member_id = o.member_id AND order_id = o.id),0)
,ROUND(((SELECT SUM(oi.product_price_rmb * oi.product_qty) AS order_price_rmb FROM web_order_item oi WHERE oi.order_id = o.id) + o.order_pay_ship_yuan) * o.rate,2))
AS actual_purchase
		FROM web_order_item_ship sp
		INNER JOIN web_order o ON o.id = sp.order_id
		WHERE o.member_id = ".GF::quote($user_id)." AND o.id = ".GF::quote($params['order_id']);
		$res = $db->Execute($sqlChekPay);	

		// ปิดไว้ชั่วคราวเนื่องจาก ไม่สามารถชำระเงินบิลขนส่งได้ 12/09/2566
		// if($res->fields['actual_purchase'] > 0)
		// {
			$sql = "INSERT INTO web_payment (
								order_type,
								member_id,
								order_id,
								payment_bank,
								payment_type,
								payment_date,
								payment_time,
								payment_amount,
								payment_slip,
								payment_comment,
								status,
								create_dtm,
								update_dtm
							) VALUES (
								".GF::quote($params['order_type']).",
								".GF::quote($user_id).",
								".GF::quote($params['order_id']).",
								".GF::quote($params['payment_bank']).",
								".GF::quote($params['payment_type']).",
								".GF::quote($params['payment_date']).",
								".GF::quote($params['payment_time']).",
								".GF::quote($params['pay_amount']).",
								".GF::quote($payment_slip).",
								".GF::quote($params['payment_comment']).",
								'W',
								NOW(),
								NOW()
							)";
			$res = $db->Execute($sql);

			if($res){
				$last_id = $db->Insert_ID();
				//$this->_setMailSlip($last_id);
				$sql = "UPDATE web_order SET status ='6' WHERE id = ".GF::quote($params['order_id']);
				$db->Execute($sql);				
			}
		// }
		return TRUE;
	}

	private function _paymentList(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();
		$tracking = new STracking();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

			$dtatReturn = array();

			$sql = "SELECT * FROM web_payment WHERE member_id=".GF::quote($user_id)." AND status!='C' ORDER BY update_dtm DESC,create_dtm DESC";



			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['id'] = $res->fields['id'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
				$ArrList[$i]['order_type'] = $res->fields['order_type'];
				$base->set('_ids',$res->fields['order_id']);
				if($res->fields['order_type']=='1'){
					$orderInfo = $this->_orderInfo();
				}else{

					$orderInfo = $tracking->shippingInfo();
				}

				$ArrList[$i]['order_info'] = $orderInfo;
                //$ArrList[$i]['payment_bank'] = $this->_bankInfo($res->fields['payment_bank']);
                $base->set('bank_id',$res->fields['payment_bank']);
                $paybank = $this->_newbankInfo();
                $ArrList[$i]['paybank'] = $paybank;
				$bankdata = GF::masterdata($paybank['master_id']);
				$ArrList[$i]['payment_bank'] = $bankdata['master_name'];
				$ArrList[$i]['payment_bank_id'] = $res->fields['payment_bank'];
				$ArrList[$i]['status'] = $res->fields['status'];
				$ArrList[$i]['payment_amount'] = $res->fields['payment_amount'];
				$ArrList[$i]['payment_date'] = $res->fields['payment_date'];
				$ArrList[$i]['payment_time'] = $res->fields['payment_time'];
				$ArrList[$i]['payment_comment'] = $res->fields['payment_comment'];
                $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
				$i++;
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
		}

		private function _bankInfo($bankid){
			$base = Base::getInstance();
			$db = DB::getInstance();

			$sql = "SELECT * FROM web_payment_bank WHERE id=".$bankid;
			$res = $db->Execute($sql);

			return  $res->fields['description'];
		}

		private function _uploadTemp(){
			$base = Base::getInstance();

			if(!empty($base->get('POST.filetemp'))){

				$tmp_file = $base->get('POST.filetemp');

				$base->set('BASE64_STR',$tmp_file);

				return $base->get('BASEURL')."/uploads/temp/".$this->base64_to_jpeg();
			}
		}
		private function base64_to_jpeg() {

			$base = Base::getInstance();
			$fileReturn = GF::randomNum(25).".jpg";

		    $ifp = fopen($base->get('BASEDIR').'/uploads/temp/'.$fileReturn, "wb");

		    $data = explode(',', $base->get('BASE64_STR'));

		    fwrite($ifp, base64_decode($data[1]));
		    fclose($ifp);

		    return $fileReturn;
		}

    private function _itemInfo(){
  		$base = Base::getInstance();
  		$db = DB::getInstance();

  		$orderid = $base->get('_ids');

  		$sql = "SELECT * FROM web_order_item WHERE id=".GF::quote($orderid);
  		$res = $db->Execute($sql);

  		$res->fields['img_path'] = $base->get('BASEURL')."/uploads/product/".$res->fields['product_image'];

  		return  $res->fields;

  	}
    private function _addRefund(){

  		$base = Base::getInstance();
  		$db = DB::getInstance();

  		$params = $base->get('POST');

  		$member = new Member();


   	$memberInfomation = $member->memberInfomation();

   	$user_id = $memberInfomation['id'];

      $ref_file = '';
		if(!empty($_FILES['ref_file']['name'])){
			$picname = $_FILES['ref_file']['name'];

			$random = GF::randomNum(25);

			$type = substr(strrchr($picname, '.'), 1);
			$dest_picname_o = $base->get('BASEDIR')."/uploads/refund/".$random.".".$type;

			$tmp_file = $_FILES['ref_file']['tmp_name'];
			@copy($tmp_file, $dest_picname_o);
			$ref_file = $random.".".$type;
		}

        $ref_refund = $params['item_qty']-$params['ref_refund'];

  		$sql = "INSERT INTO project_item_refund (
								item_id,
								ref_refund,
                                ref_item,
                                ref_file,
  							    ref_comment,
  							    status,
  							    create_dtm,
                                create_by,
                                update_dtm,
                                update_by
  						) VALUES (
  							".GF::quote($params['item_id']).",
  							".GF::quote($ref_refund).",
                            ".GF::quote($params['ref_item']).",
                            ".GF::quote($ref_file).",
  							".GF::quote($params['comment']).",
  							'Wait',
  							NOW(),
                            ".GF::quote($user_id).",
                            NOW(),
                            ".GF::quote($user_id)."
  						)";
  						//echo $sql;
  		 $res = $db->Execute($sql);

  		 if($res){
  		 	$last_id = $db->Insert_ID();
        $sql = "UPDATE web_order_item SET ref_id=".GF::quote($last_id)." WHERE id=".GF::quote($params['item_id']);
        $db->Execute($sql);

  		 	return TRUE;
  		 }
  	}

    private function _refundList(){
  		$base = Base::getInstance();
  		$db = DB::getInstance();

      $member = new Member();


   		$memberInfomation = $member->memberInfomation();

   		$user_id = $memberInfomation['id'];

         $status_search = $base->get('GET.status');

         $cond = '';
         if($status_search!=''){
            $cond = " AND status=".GF::quote($status_search)." ";
         }else{
            $cond = " AND status!='Delete' ";
         }

         $sql = "SELECT * FROM project_item_refund WHERE create_by=".GF::quote($user_id).$cond." ORDER BY ref_id DESC";
        //$sql = "SELECT * FROM web_order WHERE  active_status='O' ".$cond." ORDER BY update_dtm DESC,create_dtm DESC";
        //echo $sql;

        $res = $db->Execute($sql);
        $count = $res->NumRows();

           $base->set('allPage',ceil($count/20));

        $page = $base->get('GET.page');

        $numr = 20;
        $start = "";
        $end = "";
        if($page==1||empty($page)){
           $start = 0;
           $page = 1;
           //$end = $numr;
        }else{
           //$end = ($page*$numr);
           $start = ($page*$numr)-$numr;
        }
         $condNum = " LIMIT ".$start.",".$numr;

         $base->set('currpage',$page);

        $res = $db->Execute($sql.$condNum);
  		$i=0;
  		$ArrList = array();
  		while(!$res->EOF){
  			$ArrList[$i]['ref_id'] = $res->fields['ref_id'];
         $ArrList[$i]['item_id'] = $res->fields['item_id'];
  			$ArrList[$i]['ref_refund'] = $res->fields['ref_refund'];
         $ArrList[$i]['ref_item'] = $res->fields['ref_item'];
         $ArrList[$i]['ref_file'] = $res->fields['ref_file'];
         $ArrList[$i]['ref_file2'] = $res->fields['ref_file2'];
         $ArrList[$i]['ref_rmb'] = $res->fields['ref_rmb'];
         $ArrList[$i]['ref_thb'] = $res->fields['ref_thb'];
         $ArrList[$i]['ref_comment'] = $res->fields['ref_comment'];
         $ArrList[$i]['ref_admin'] = $res->fields['ref_admin'];
         $ArrList[$i]['status'] = $res->fields['status'];
         $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
         $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
         $base->set('_ids',$res->fields['item_id']);
  			$ArrList[$i]['item_info'] = $this->_itemInfo();
         $base->set('_ids',$ArrList[$i]['item_info']['order_id']);
         $ArrList[$i]['order_info'] = $this->_orderInfo();
  			$i++;
  			$res->MoveNext();
  		}
  		//print_r($ArrList);
  		return $ArrList;
  	}
   private function _cancelRefund(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "UPDATE project_item_refund SET status='Delete' WHERE ref_id=".GF::quote($base->get('GET.ref_id'));
		$res = $db->Execute($sql);

      $sql = "UPDATE web_order_item SET ref_id='0' WHERE id=".GF::quote($base->get('GET.item_id'));
      $db->Execute($sql);
	}

    private function _changeoption(){

		$base = Base::getInstance();
		$db = DB::getInstance();

      $option_id = $base->get('GET.opid');
      $order_id = $base->get('GET.order_id');
      $opt_val = $base->get('GET.value');

		$sql = "UPDATE web_order SET status='1' WHERE id=".GF::quote($order_id);
		$res = $db->Execute($sql);

      $sql = "UPDATE web_order_item_option SET option_value=".GF::quote($opt_val).",option_value_th=".GF::quote($opt_val)." WHERE id=".GF::quote($option_id);
      $db->Execute($sql);
	}

    private function _updateItemImg(){

		$base = Base::getInstance();
		$db = DB::getInstance();

      $item_id = $base->get('POST.item_id');
      $path = $base->get('pathimg');

      $img_name = GF::randomNum(25);
      $file = $path;
      $data = file_get_contents($file);
      $new = $base->get('BASEDIR').'/uploads/product/'.$img_name.".jpg";
      file_put_contents($new, $data);

      $sql = "UPDATE web_order_item SET product_image=".GF::quote($img_name.".jpg")." WHERE id=".GF::quote($item_id);
      $db->Execute($sql);
	}

    private function _getshipItem(){

        $base = Base::getInstance();
        $db = DB2::getInstance();

        $orer_id = $base->get('_ids');
        $result = $db->select("web_order_item_ship","*",array("order_id"=>$orer_id));

        $resultReturn = array();

        $resultReturn['keyname'] = array();
        $resultReturn['data'] = array();

        if(count($result)>0){
            $i = 0;
            foreach($result as $key=>$vals){
                $resultReturn['data'][$i] = $vals;
                $resultReturn['keyname'][$i] = $vals['saller_id'];
                $i++;
            }
        }

        return $resultReturn;
    }
    private function _bankList(){
        $base = Base::getInstance();
       // $db = DB2::getInstance();
		$db = DB::getInstance();

        $member = new Member();
        $memberInfomation = $member->memberInfomation();

        $ref_id = $memberInfomation['ref_id'];
        if($ref_id=='0'){
            $ref_id = '1';
        }
        /*$cond = array("status[!]"=>'D',"agency_id"=>$ref_id);
        
        

        $result = $db->select("project_bank","*",array(
                                                        "AND"=>$cond    
                                                        ));
        return $result;*/
		
        $cond = " AND b.agency_id=".GF::quote($ref_id);
        
        $tp = $base->get('GET.tp');
        if($tp!=''){
            $cond .= " AND b.bank_type=".GF::quote($tp)." ";
        }
		
		$sql = "SELECT b.*, m.master_name, m.master_value, m.master_char FROM project_bank b 
			LEFT JOIN project_master m ON m.id=b.master_id
			WHERE b.status!='D' AND m.master_char='Y' ".$cond." ORDER BY b.bank_id DESC";				
											
			$res = $db->Execute($sql);	
			
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['bank_id'] = $res->fields['bank_id'];
				$arrReturn[$i]['master_id'] = $res->fields['master_id'];
				$arrReturn[$i]['bank_desc'] = $res->fields['bank_desc'];
				$arrReturn[$i]['agency_id'] = $res->fields['agency_id'];
				$arrReturn[$i]['master_value'] = $res->fields['master_value'];
				$arrReturn[$i]['master_char'] = $res->fields['master_char'];
				$arrReturn[$i]['bank_type'] = $res->fields['bank_type'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();										
											
			//return $result;
			return $arrReturn;

    }
    private function _newbankInfo(){
        $base = Base::getInstance();
        $db = DB2::getInstance();


        $result = $db->get("project_bank","*",array("bank_id"=>$base->get('bank_id')));

        return $result;

    }

	public function get_order_overdue_payment(){
		$base = Base::getInstance();
        $db = DB::getInstance();
		$member = new Member();
		$memberInfomation = $member->memberInfomation();
		$user_id = $memberInfomation['id'];

		$sql = "SELECT 
		COUNT(o.id) as count_ordernumber
		,SUM(o.order_price) as pay_price
		,SUM(IFNULL(ROUND(((i.order_price_rmb * o.old_rate) + o.order_pay_ship) - o.order_discount,2),o.order_price)) as order_price
		,u.wallet
		FROM project_user u
		LEFT JOIN web_order o on o.member_id = u.id AND (o.status IN (2,7)) and o.active_status = 'O' 
			AND o.order_type = '1' 
		LEFT JOIN (SELECT order_id,ROUND(SUM(product_price_rmb * product_qty),2) as order_price_rmb 
								FROM web_order_item WHERE status = 'A' AND item_typs_s = 'NR' GROUP BY order_id) i ON i.order_id = o.id
		WHERE
			 u.id = ".GF::quote($user_id)."					
		GROUP BY u.wallet";
		$res = $db->Execute($sql);
		
		$arrReturn = array();		
		while(!$res->EOF){
			$arrReturn['count'] = $res->fields['count_ordernumber'];
			$arrReturn['total'] = $res->fields['order_price'] - $res->fields['pay_price'];;
			$arrReturn['wallet'] = $res->fields['wallet'];
			$res->MoveNext();
		}
		
		return  $arrReturn;
	}

	// Edit by Weerasak 14-01-2566
	private function _getlist_orderdetail(){
		$base = Base::getInstance();
        $db = DB::getInstance();
		$member = new Member();
		$memberInfomation = $member->memberInfomation();
		$user_id = $memberInfomation['id'];
/*
		$sql = "SELECT * FROM (
			SELECT *
			,CASE WHEN (receive_amount - actual_purchase_thb) > 0 AND export = 'S' THEN (receive_amount - actual_purchase_thb) ELSE 0 END AS refund_amount
			,CASE WHEN ( receive_amount - actual_purchase_thb ) < 0 AND export = 'S' THEN ( actual_purchase_thb - receive_amount ) 
				WHEN export = 'N' AND status IN ('2','6','7') THEN ROUND(((order_price_rmb + order_pay_ship_yuan)  * old_rate),2)
				WHEN export = 'N' THEN actual_purchase_thb ELSE 0 
				END AS extra_pay_amount
			,CASE WHEN pay_status != 'SUCCESS' THEN 
						CASE WHEN pay_status = 'CHECK' THEN pay_status 
							WHEN status IN ('2','7') THEN 'WAIT'								
						END
			 ELSE pay_status
			 END AS payment_status
			FROM (
					SELECT
					o.id,o.order_id,o.order_number,o.old_rate,o.rate,o.order_pay_ship,o.order_pay_ship_yuan
					,o.status,o.export
					,qty
					,order_price_rmb
					,CASE WHEN o.status IN ('2','6','7') THEN ((order_price_rmb * qty) + order_pay_ship_yuan) ELSE IFNULL( actual_purchase_rmb, 2 ) END AS actual_purchase_rmb
					,IFNULL(ROUND((actual_purchase_rmb * o.rate),2),0) as actual_purchase_thb 
					,IFNULL(p.receive_amount,0) AS receive_amount 
					,CASE wd.status WHEN  'R' THEN 'WAIT'
										WHEN 'A' THEN 'SUCCESS' 
										WHEN 'N' THEN 'CANCEL'
										END as refund_status
					,COALESCE((SELECT	IF( STATUS = 'W', 'CHECK', 'WAIT' ) FROM web_payment WHERE	member_id = ".$user_id." AND order_id = o.id 	AND STATUS != 'A' ORDER BY id desc LIMIT 1 ) ,'SUCCESS') AS pay_status 
					FROM
					web_order o
					LEFT JOIN ( SELECT SUM( payment_amount ) AS receive_amount,order_id, member_id
						FROM web_payment 
						WHERE status = 'A' AND member_id = ".$user_id."
						GROUP BY order_id, member_id ) p ON p.order_id = o.id
					LEFT JOIN (
											SELECT o.order_id
											,SUM(IFNULL(o.product_qty,o.product_buy_qty)) AS qty
											,SUM(o.product_price_rmb * o.product_qty) AS order_price_rmb
											,(SELECT ROUND(SUM((saller_amount_rmb + saller_shipping_rmb)-saller_discount_rmb),2) FROM web_order_item_ship WHERE order_id = o.order_id) AS actual_purchase_rmb
											FROM web_order_item o
											WHERE o.status != 'C' AND o.item_typs_s = 'NR'
											GROUP BY o.order_id
					) i ON i.order_id = o.id
					LEFT JOIN project_withdraw wd ON wd.user_id = o.member_id AND COALESCE(wd.order_id,-1) = o.id
					WHERE date(o.update_dtm) > DATE(Concat(YEAR(NOW()),'-01','-01'))
					AND o.active_status = 'O' 
					AND o.order_type = '1' 
					AND o.member_id = ".$user_id." 	
			) A ) B -- WHERE refund_amount > 0 OR extra_pay_amount > 0
			ORDER BY id desc";
*/
/*แสดงหน้า ยอดคืน-ค้างชำระ*/
		$sql = "
		WITH ORDER_ITEMS AS (
		SELECT oi.order_id
			,SUM(IFNULL(oi.product_qty,oi.product_buy_qty)) AS qty
			,SUM(oi.product_price_rmb * oi.product_qty) AS order_price_rmb
			,SUM(round((oi.product_price_rmb * oi.product_qty) * o.rate,2)) as order_price_thb
			,oi.saller_number
			,CASE WHEN oi.ref_id = 0 THEN ((osh.saller_amount_rmb + osh.saller_shipping_rmb) - osh.saller_discount_rmb) 
			ELSE ((osh.saller_amount_rmb + osh.saller_shipping_rmb) - osh.saller_discount_rmb) -- (oi.product_price_rmb * oi.product_qty) 
			END AS actual_purchase_rmb
		FROM web_order o
		INNER JOIN web_order_item oi ON oi.order_id = o.id
		LEFT JOIN web_order_item_ship osh ON osh.order_id = o.id AND osh.saller_id = oi.saller_number
		
		WHERE oi.status != 'C' AND oi.item_typs_s = 'NR'
		AND o.member_id = ".$user_id."
		GROUP BY oi.order_id,oi.saller_number
		)
		
		SELECT B.id
		,order_number
		,qty
		,order_price_rmb
		,old_rate
		,round(((order_price_rmb * old_rate) + order_pay_ship),2) as order_price_thb
		,rate
		,actual_purchase_thb
		,receive_amount
		,round(refund_amount,2) as refund_amount
		,extra_pay_amount
		-- ,CASE WHEN extra_pay_amount > 0 AND payment_status = 'SUCCESS' THEN 'WAIT' ELSE payment_status END status
		,CASE WHEN refund_amount > 0 AND (SELECT COUNT(*) FROM project_withdraw wd WHERE wd.create_by = ".$user_id."  AND order_id = B.id ) = 0 THEN ''
			WHEN refund_amount > 0 AND (SELECT COUNT(*) FROM project_withdraw wd WHERE status = 'R' AND wd.create_by = ".$user_id."  AND order_id = B.id ) > 0 THEN 'WAIT'
			WHEN refund_amount > 0 AND (SELECT COUNT(*) FROM project_withdraw wd WHERE status = 'A' AND wd.create_by = ".$user_id."  AND order_id = B.id ) > 0 THEN 'SUCCESS'
			WHEN extra_pay_amount > 0 AND payment_status = 'SUCCESS' THEN 'WAIT' 
	ELSE payment_status 
	END status
		,'N' AS user_refund
		,(SELECT remarks FROM web_payment WHERE status = 'F' AND order_id = B.id AND member_id = ".$user_id." ORDER BY id DESC LIMIT 1 ) AS remark
		FROM (
				SELECT *
				,CASE WHEN (receive_amount - actual_purchase_thb) > 0 AND export = 'S' THEN (receive_amount - actual_purchase_thb) ELSE 0 END AS refund_amount
				,CASE WHEN ( receive_amount - actual_purchase_thb ) < 0 AND export = 'S' THEN ( actual_purchase_thb - receive_amount ) 
				WHEN export = 'N' AND `status` IN ('2','6','7') THEN ROUND(((order_price_rmb + order_pay_ship_yuan)  * old_rate),2)
				WHEN export = 'N' THEN actual_purchase_thb ELSE 0 
				END AS extra_pay_amount
				,CASE WHEN pay_status != 'SUCCESS' THEN 
				CASE WHEN pay_status = 'CHECK' THEN pay_status 
				WHEN status IN ('2','7') THEN 'WAIT'		
				ELSE 'SUCCESS'					 
				END
				ELSE pay_status
				END AS payment_status
				FROM (
						SELECT
							o.id,o.order_id,o.order_number,o.old_rate,o.rate,o.order_pay_ship,o.order_pay_ship_yuan
							,o.`status`,o.export
							,oi.qty
							,oi.order_price_rmb
							,CASE 
							WHEN o.status = '2' THEN oi.order_price_rmb
							WHEN o.status IN ('6','7') THEN ((oi.order_price_rmb * oi.qty) + o.order_pay_ship_yuan) ELSE IFNULL( oi.actual_purchase_rmb, 2 ) END AS actual_purchase_rmb
							,IFNULL(ROUND((oi.actual_purchase_rmb * o.rate),2),0) as actual_purchase_thb 
							,IFNULL(p.receive_amount,0) AS receive_amount
							-- ,(SELECT status FROM ORDER_REFUND WHERE id = o.id LIMIT 1) as refund_status
							,COALESCE((SELECT	IF( STATUS = 'W', 'CHECK', 'WAIT' ) 
										FROM web_payment 
										WHERE	member_id = ".$user_id." AND order_id = o.id 	AND STATUS != 'A' ORDER BY id desc LIMIT 1 ) ,'SUCCESS') AS pay_status 
						FROM	web_order o
						LEFT JOIN ( SELECT SUM( payment_amount ) AS receive_amount	, order_id, member_id
									FROM web_payment 
									WHERE status = 'A' AND member_id =".$user_id."
									GROUP BY order_id, member_id
						) p ON (p.order_id = o.id )
						LEFT JOIN (
									SELECT order_id,qty,SUM(order_price_rmb) as order_price_rmb,COALESCE(SUM(actual_purchase_rmb),0) as actual_purchase_rmb
									FROM ORDER_ITEMS oi 
									GROUP BY order_id
									ORDER BY order_id
						) oi ON oi.order_id = o.id
						WHERE
						o.active_status = 'O' 
						AND o.order_type = '1' 
						AND o.member_id = ".$user_id."
						AND date(o.update_dtm) > DATE(Concat(YEAR(NOW())-1,'-01','-01'))
			) A 
		) B  
		
		UNION
		
		/*คืนเงินการขอ refund*/
		SELECT	
		o.id
		,o.order_number
		,oi.product_qty as qty
		,ref.ref_rmb as order_price_rmb
		,ref.ref_rate as old_rate
		,ref.ref_thb as order_price_thb
		,o.rate
		,0 as actual_purchase_thb
		,0 as receive_amount
		,round(ref.ref_thb,2) as refund_amount
		,0 as extra_pay_amount
		,CASE 
		WHEN ref.status = 'Refunded' OR COALESCE(wd.status,'') != '' THEN
							CASE wd.status WHEN 'A' THEN 'SUCCESS'
							WHEN 'N' THEN 'CANCEL'
							WHEN 'R' THEN 'WAIT'
							END
		WHEN ref.status = 'Refunded' THEN 'SUCCESS'
		END status
		,'Y' AS user_refund
		,ref.ref_admin as remark
		FROM project_item_refund ref
		INNER JOIN web_order_item oi ON oi.id = ref.item_id
		INNER JOIN web_order o ON o.id = oi.order_id
		LEFT JOIN project_withdraw wd ON wd.create_by = 1 AND COALESCE(wd.order_id,0) = o.id
		WHERE ref.create_by = ".$user_id." 
		ORDER BY id DESC";
		//echo $sql;
		$res = $db->Execute($sql);
		//print_r($res);
		$arrReturn = array();
		$i = 0;		
		while(!$res->EOF){
			$arrReturn[$i]['order_id'] = $res->fields["id"];
			$arrReturn[$i]['order_number'] = $res->fields['order_number'];
			$arrReturn[$i]['count'] = $res->fields['qty'];
			$arrReturn[$i]['order_price_rmb'] = $res->fields['order_price_rmb'];
			$arrReturn[$i]['order_price_thb'] = $res->fields['order_price_thb'];
			$arrReturn[$i]['rate'] = $res->fields['old_rate'];
			$arrReturn[$i]['actual_purchase_thb'] = $res->fields['actual_purchase_thb'];;
			$arrReturn[$i]['actural_rate'] = $res->fields['rate'];
			$arrReturn[$i]['receive_amount'] = $res->fields['receive_amount'];
			$arrReturn[$i]['refund_amount'] = $res->fields['refund_amount'];
			$arrReturn[$i]['extra_pay_amount'] = $res->fields['extra_pay_amount'];
			$arrReturn[$i]['user_refund'] = $res->fields['user_refund'];
			$arrReturn[$i]['status'] = $res->fields['status'];
			$arrReturn[$i]['remark'] = $res->fields['remark'];
			$i++;
			$res->MoveNext();
		}

		$sql = "SELECT 
		(SELECT id FROM web_order WHERE order_number = t.order_id) as order_id,
		s.id,s.shipping_code,
		COUNT(t.shipping_code) as qty,
		SUM(t.price_true) as shipping_price,
		COALESCE((SELECT IF( STATUS = 'W', 'CHECK', 'WAIT' )  FROM web_payment WHERE member_id = s.create_by AND order_id = s.id AND STATUS != 'A' ORDER BY id desc LIMIT 1 ),'SUCESS') AS status
		FROM web_shipping s
		INNER JOIN web_tracking t ON t.shipping_code = s.shipping_code
		WHERE s.create_by = ".$user_id." 
		AND s.status = '3'
		GROUP BY s.id,s.shipping_code
		ORDER BY s.create_dtm DESC";
		$resShip = $db->Execute($sql);
		while(!$resShip->EOF)
		{
			$arrReturn[$i]['order_id'] = $res->fields["id"];
			$arrReturn[$i]['shipping_id'] = $resShip->fields["id"];
			$arrReturn[$i]['shipping_number'] = $resShip->fields['shipping_code'];
			$arrReturn[$i]['count'] = $resShip->fields['qty'];
			$arrReturn[$i]['extra_pay_amount'] = $resShip->fields['shipping_price'];
			$arrReturn[$i]['status'] = $resShip->fields['status'];
			$i++;
			$resShip->MoveNext();
		}
		return  $arrReturn;
	}


 }
?>
