<?php
class GF{




	public static function formatUrl($string,$encode=true)
	{
		//$string=strtolower(trim($string));
		//$string = strtolower(str_replace(" ","_",$string));
		$encode=false; //true = no encode; false= encode
		$string=trim($string);
		$string = str_replace(" ","-",$string);
		$string = preg_replace('~[^a-z0-9ก-๙\.\-\_]~iu','',$string);
		$string=preg_replace("/[\_]{2,}/",'-',$string);

		return ($encode==true)?rawurlencode(trim($string, '-')):trim($string, '-');
	}

	public static function randomStr($length){
		$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		return $randomString;
	}
	public static function randomNum($length){
		$randomString = substr(str_shuffle("123456789012345678901234567890123456789012345678901234567890"), 0, $length);
		return $randomString;
	}

	public static function viewipaddress(){
		$ipaddress = '';
	    if ($_SERVER['HTTP_CLIENT_IP'])
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if($_SERVER['HTTP_X_FORWARDED'])
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if($_SERVER['HTTP_FORWARDED_FOR'])
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if($_SERVER['HTTP_FORWARDED'])
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if($_SERVER['REMOTE_ADDR'])
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';

	    return $ipaddress;
	}

	public static function print_r($data){
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	}


	public static function quote($value){
		//$db = DB::getInstance();
		//return $db->Quote(trim($value));
		return "'".str_replace('\\"', '"', addslashes($value))."'";
	}

	public static function htmlchar($value){
		return htmlspecialchars(trim($value));
	}

	public static function trimArray($str){
		$ret = array();
		$arr = explode("<br />",nl2br($str));
		$arr = array_map('_ctrim', array_filter($arr));
		return $arr;
	}


	 public static function  array_sort($array, $on, $order=SORT_ASC){
		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
				break;
				case SORT_DESC:
					arsort($sortable_array);
				break;
			}
			$i = 0;
			foreach ($sortable_array as $k => $v) {
				$new_array[$i] = $array[$k];
				//print $i."<BR>";
				$i++;
			}
		}

		return $new_array;
	}

	public function  ShowDate($ymd,$tm=NULL){
	global $db;

	if($ymd != "0000-00-00 00:00:00" && $ymd != "0000-00-00"  && !empty($ymd ) ){

		$d = substr($ymd,8,2);
		$m = substr($ymd,5,2);
		$y = substr($ymd,0,4);
		$t = ($tm) ? ' '.substr($ymd,11,8) : "";


		if(!empty($_SESSION['L_LANG'])){
			$flag = $_SESSION['L_LANG'];

			$sql = "SELECT date_format FROM localizationlanguage WHERE flag='$flag'";
			$res = $db->Execute($sql);
			$date_format = $res->fields["date_format"];
			$sql = "SELECT format_id, format_one, format_two, format_three, format_character
						FROM localizationdateformat WHERE format_id=$date_format";
			$res = $db->Execute($sql);
			$format_one = $res->fields['format_one'];
			$format_two = $res->fields['format_two'];
			$format_three = $res->fields['format_three'];
			$format_character = !empty($res->fields['format_character']) ? $res->fields['format_character'] : " ";

			$sql = "SELECT month_code, monthshort_$flag, monthfull_$flag
						FROM localizationmonth WHERE month_code='$m'";
			$res = $db->Execute($sql);
			$month_code = $res->fields['month_code'];
			$monthshort = $res->fields['monthshort_'.$flag];
			$monthfull = $res->fields['monthfull_'.$flag];

			//case $format_one
			if($format_one=="dd"){
				$format_one = $d;
			}else if($format_one=="mm"){
				$format_one = $m;
			}else if($format_one=="mmm"){
				$format_one = $monthshort;
			}else if($format_one=="mmmm"){
				$format_one = $monthfull;
			}else if($format_one=="yy"){
				if($flag=="th"){
					$format_one = substr(($y+543),-2);
				}else{
					$format_one = substr($y,-2);
				}
			}else if($format_one=="yyyy"){
				if($flag=="th"){
					$format_one = ($y+543);
				}else{
					$format_one = $y;
				}
			}

			//case $format_two
			if($format_two=="dd"){
				$format_two = $d;
			}else if($format_two=="mm"){
				$format_two = $m;
			}else if($format_two=="mmm"){
				$format_two = $monthshort;
			}else if($format_two=="mmmm"){
				$format_two = $monthfull;
			}else if($format_two=="yy"){
				if($flag=="th"){
					$format_two = substr(($y+543),-2);
				}else{
					$format_two = substr($y,-2);
				}
			}else if($format_two=="yyyy"){
				if($flag=="th"){
					$format_two = ($y+543);
				}else{
					$format_two = $y;
				}
			}

			//case $format_three
			if($format_three=="dd"){
				$format_three = $d;
			}else if($format_three=="mm"){
				$format_three = $m;
			}else if($format_three=="mmm"){
				$format_three = $monthshort;
			}else if($format_three=="mmmm"){
				$format_three = $monthfull;
			}else if($format_three=="yy"){
				if($flag=="th"){
					$format_three = substr(($y+543),-2);
				}else{
					$format_three = substr($y,-2);
				}
			}else if($format_three=="yyyy"){
				if($flag=="th"){
					$format_three = ($y+543);
				}else{
					$format_three = $y;
				}
			}

			$show_date = $format_one.$format_character.$format_two.$format_character.$format_three.$t;
		}else{
			$show_date = $ymd;
		}

			return $show_date;
		}else{
			return "-";
		}
	}

	public static function masterdata($group_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();


 		//$group_id = $base->get('_ids');

 		if($group_id!=''){
			$sql = "SELECT * FROM project_master WHERE id=".GF::quote($group_id);


			//$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);
			$arrReturn = array();
			$arrReturn['id'] = $res->fields['id'];
			$arrReturn['master_name'] = $res->fields['master_name'];
			$arrReturn['master_value'] = $res->fields['master_value'];
			$arrReturn['master_char'] = $res->fields['master_char'];
			$arrReturn['group_id'] = $res->fields['group_id'];


			return $arrReturn;
		}else{
			return NULL;
		}
	}
    public static function masterdataset($value,$from,$to){
		$base = Base::getInstance();
 		$db = DB::getInstance();

			$sql = "SELECT * FROM project_master WHERE ".$from."=".GF::quote($value)." LIMIT 1";
			//$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);

			return $res->fields[$to];
	}
	public static function masterdataval($group_id,$val){
		$base = Base::getInstance();
 		$db = DB::getInstance();


 		//$group_id = $base->get('_ids');

 		if($group_id!=''){
			$sql = "SELECT * FROM project_master WHERE group_id=".GF::quote($group_id)." AND master_value=".GF::quote($val);


			//$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);
			$arrReturn = array();
			$arrReturn['id'] = $res->fields['id'];
			$arrReturn['master_name'] = $res->fields['master_name'];
			$arrReturn['master_value'] = $res->fields['master_value'];
			$arrReturn['master_char'] = $res->fields['master_char'];
			$arrReturn['group_id'] = $res->fields['group_id'];


			return $arrReturn;
		}else{
			return NULL;
		}
	}
	public static function masterdataList($group_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();

		$sql = "SELECT * FROM project_master WHERE status='O' AND active_status='O' AND group_id=".GF::quote($group_id)." ORDER BY id ASC";
		$res = $db->Execute($sql);

		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['id'];
			$arrReturn[$i]['master_name'] = $res->fields['master_name'];
			$arrReturn[$i]['master_value'] = $res->fields['master_value'];
			$arrReturn[$i]['master_char'] = $res->fields['master_char'];
			$arrReturn[$i]['group_id'] = $res->fields['group_id'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	public static function memberinfo($id){
		$base = Base::getInstance();
		$member  = new Member();
		$base->set('user_id',$id);
		return $member->memberInfomationByID();
	}
	public static function genmembercode($type,$ref_id){
		$base = Base::getInstance();
		$db = DB2::getInstance();

		$ref_id = $ref_id;
		
		if($type=='MEMBER_ORDER'){
			
			$agency_code = 'A';
			if($ref_id!=''){
				$ref_user = $db->get("project_user","*",array("id"=>$ref_id));
				$agency_code = $ref_user['agency_code'];
				if($agency_code==''){
					$agency_code = 'A';
				}
			}
			
			$dataCode = $db->get("project_run_number","*",array("AND"=>array("run_module"=>$type,"run_extra"=>$agency_code)));
			$db->update("project_run_number",array("run_number"=>($dataCode['run_number']+1)),array("run_id"=>$dataCode['run_id']));
			
			$lastRun = "T".$agency_code."O".str_pad($dataCode['run_number'],4,"0",STR_PAD_LEFT);
			return $lastRun;
			
		}
		
		if($type=='MEMBER_SHIP'){
			
			$agency_code = 'A';

			if($ref_id!=''){
				$ref_user = $db->get("project_user","*",array("id"=>$ref_id));
				//GF::print_r($ref_user);
				$agency_code = $ref_user['agency_code'];
				$extra_code = $ref_user['agency_code'];
				if($agency_code==''){
					$agency_code = 'A';
				}
			}

			//echo $ref_id;
			
			$dataCode = $db->get("project_run_number","*",array("AND"=>array("run_module"=>$type,"run_extra"=>$agency_code)));
//GF::print_r($dataCode);

//exit();
			$db->update("project_run_number",array("run_number"=>($dataCode['run_number']+1)),array("run_id"=>$dataCode['run_id']));

			//$dataCode2 = $db->get("project_run_number","*",array("AND"=>array("run_module"=>$type,"run_extra"=>$extra_code)));
			
			$lastRun = "T".$agency_code."S".str_pad($dataCode['run_number'],4,"0",STR_PAD_LEFT);
			return $lastRun;
		}
		
		
		
	}
	public static function genbilling(){
		$base = Base::getInstance();
		$db = DB2::getInstance();
		
		$dataCode = $db->get("project_run_number","*",array("run_id"=>'53'));
		$now1 = date('m');
		$now2 = date('Y');
		$now = $now1.str_replace("20","",$now2);
		$codeReturn = '';
		if($now!=$dataCode['run_extra']){
			$db->update("project_run_number",array("run_number"=>'1',"run_extra"=>$now),array("run_id"=>'53'));
			$codeReturn = $now."".str_pad('1',4,"0",STR_PAD_LEFT);
		}else{
            $db->update("project_run_number",array("run_number"=>($dataCode['run_number']+1)),array("run_id"=>'53'));
            $newRun = $dataCode['run_number']+1;
			$codeReturn = $now."".str_pad($newRun,4,"0",STR_PAD_LEFT);
		}	
		return $codeReturn;	
	}
	public static function getrate(){
		$order = new SOrder();
 		$rate = $order->getRate();
 		return $rate;
	}
	public static function langdefault(){
		return 'th';
	}
   public static function orderlimit(){
      $base = Base::getInstance();
      $db = DB::getInstance();

      $sql = "SELECT * FROM project_order_limit WHERE id='1'";
		$res = $db->Execute($sql);
      //GF::print_r($res->fields);
      return $res->fields;

	}
   public static function user_code($user_code){
		$base = Base::getInstance();
 		$db = DB::getInstance();

		$sql = "SELECT user_code FROM project_user WHERE id=".GF::quote($user_code);
		$res = $db->Execute($sql);


		return 	$res->fields['user_code'];
	}

   public static function user_id($user_code){
		$base = Base::getInstance();
 		$db = DB::getInstance();

		$sql = "SELECT id FROM project_user WHERE user_code=".GF::quote($user_code);
		$res = $db->Execute($sql);


		return 	$res->fields['id'];
	}

   public static function order_no($orderid){
		$base = Base::getInstance();
 		$db = DB::getInstance();

		$sql = "SELECT order_number FROM web_order WHERE id=".GF::quote($orderid);
		$res = $db->Execute($sql);


		return 	$res->fields['order_number'];
	}
   public static function order_id($orderid){
		$base = Base::getInstance();
 		$db = DB::getInstance();

		$sql = "SELECT id FROM web_order WHERE order_number=".GF::quote($orderid);
		$res = $db->Execute($sql);


		return 	$res->fields['id'];
	}
   public static function user_order($orderid){
		$base = Base::getInstance();
 		$db = DB::getInstance();

		$sql = "SELECT member_id FROM web_order WHERE id=".GF::quote($orderid);
		$res = $db->Execute($sql);

      $memberinfo = GF::memberinfo($res->fields['member_id']);


		return 	$memberinfo;
	}
   public static function refgroup(){
		$base = Base::getInstance();
 		$db = DB2::getInstance();

      $member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		$user_id = $memberInfomation['id'];

		$result = $db->select("project_user",array("id"),array("ref_id"=>$user_id));


      $return = array();
      if(count($result)>0){
         $i = 0;
         foreach ($result as $key => $value) {
            $return[$i] = $value['id'];
            $i++;
         }
      }

		return 	$return;
	}
   public static function agencyname($user_id){
		$base = Base::getInstance();
 		$db = DB2::getInstance();

      $memberinfo = GF::memberinfo($user_id);
      $ref_name = '';

      if($memberinfo['ref_id']!='0'){
         $memberinfo2 = GF::memberinfo($memberinfo['ref_id']);
         $ref_name = $memberinfo2['user_code']."<br />".$memberinfo2['user_name'];

      }else{

      }
		return 	$ref_name;
	}
	public static function convgoodscode($code){
		$myArr = array("1","2","3");
		$convArr = array("G","M","B");
		
		$key = array_search($code,$myArr);
		
		return $convArr[$key];
	}
    public static function cvprovince($code){
		$base = Base::getInstance();
 		$db = DB::getInstance();
         

		$sql = "SELECT PROVINCE_NAME FROM project_thai_province WHERE PROVINCE_ID=".GF::quote($code);
		$res = $db->Execute($sql);
		
		return $res->fields['PROVINCE_NAME'];
	}
    public static function cvamphur($code){
		$base = Base::getInstance();
 		$db = DB::getInstance();
         

		$sql = "SELECT AMPHUR_NAME FROM project_thai_amphur WHERE AMPHUR_ID=".GF::quote($code);
		$res = $db->Execute($sql);
		
		return $res->fields['AMPHUR_NAME'];
	}
    public static function cvdistrict($code){
		$base = Base::getInstance();
 		$db = DB::getInstance();
         

		$sql = "SELECT DISTRICT_NAME FROM project_thai_district WHERE DISTRICT_ID=".GF::quote($code);
		$res = $db->Execute($sql);
		
		return $res->fields['DISTRICT_NAME'];
    }
    public static function shorturl($url,$login='0'){
		$base = Base::getInstance();
        $db = DB2::getInstance();
         
        $result = $db->get("project_shorturl","*",array("url_link"=>$url));
        if($result['url_id']==''){
            $code = GF::randomStr(10);
            $db->insert("project_shorturl",array("url_link"=>$url,"url_short"=>$code,"login"=>$login));
            $durl = $base->get('BASEURL')."/u/".$code;
            return $durl;
        }else{
            $durl = $base->get('BASEURL')."/u/".$result['url_short'];
            return $durl;
        }
    }
    public static function mailtemplate($gen_id,$arrReplace=array(),$agency_id='1'){
		$base = Base::getInstance();
        $db = DB2::getInstance();
        if(intval($agency_id)==0){
            $agency_id='1';
        } 
        $result = $db->get("project_email_template","*",array(
                                                                "AND"=>array(
                                                                    "gen_id"=>$gen_id,
                                                                    "agency_id"=>$agency_id
                                                                )
                                                            ));
        $dataHtml = file_get_contents($base->get('BASEURL')."/app/view/adminlte/utility_ui/mail.htm");
        $dataHtml = str_replace("{MAIL_BODY}",$result['gen_field_1'],$dataHtml);
        if(count($arrReplace)>0){
            foreach($arrReplace as $key=>$data){
                $dataHtml = str_replace($key,$data,$dataHtml);
            }
        }
        return $dataHtml;
    }
    public static function mailsubject($gen_id,$agency_id='1'){
		$base = Base::getInstance();
        $db = DB2::getInstance();
        if(intval($agency_id)==0){
            $agency_id='1';
        } 
        $result = $db->get("project_email_template","*",array(
                                                                "AND"=>array(
                                                                    "gen_id"=>$gen_id,
                                                                    "agency_id"=>$agency_id
                                                                )
                                                            ));
        return $result['gen_field_2'];
    }
    public static function cvstatus($gen_id){
        $text = '';
        if($gen_id=='1'){
            $text = 'รอตรวจสอบ';
        }
        else if($gen_id=='2'){
            $text = 'รอชำระเงิน';
        }
        else if($gen_id=='3'){
            $text = 'สังซื้อ';
        }
        else if($gen_id=='4'){
            $text = 'เสร็จสิ้น';
        }else{
            $text = 'ยกเลิก';
        }
        return $text;
    }
    public static function shippingstatus($gen_id){
        $text = '';
        if($gen_id=='1'){
            $text = 'รอตรวจสอบ';
        }
        else if($gen_id=='9'){
            $text = 'รอการชำระเงิน';
        }
        else if($gen_id=='5'){
            $text = 'ชำระเงินเรียบร้อย เตรียมจัดส่ง';
        }
        else if($gen_id=='6'){
            $text = 'จัดส่งสินค้าแล้ว';
        }else{
            $text = 'N/A';
        }
        return $text;
    }
	public static function order_status($statusId)
	{
		$text = '';
        if($statusId=='1'){
            $text = 'รอตรวจสอบ';
        }else if($statusId=='2'){
            $text = 'รอชำระเงิน';
        }else if($statusId=='3'){
            $text = 'สั่งซื้อ';
        }else if($statusId=='5'){
            $text = 'เสร็จสิ้น';
		}else if($statusId=='6'){
            $text = 'รอตรวจสอบยอด';
		}else if($statusId=='7'){
            $text = 'ค้างชำระเงิน';
		}else{
            $text = 'ยกเลิก';
        }
        return $text;
	}
	public static function order_color_status($statusId)
	{
		$color = '';
        if($statusId=='1' || $statusId =='5' || $statusId == '6'){//รอตรวจสอบ,เสร็จสิ้น
            $color = '#0073b7'; 
        }else if($statusId=='2'){ //รอชำระเงิน
            $color = '#DF2B24';
        }else if($statusId=='3'){ //สั่งซื้อ
            $color = '#0F6F11';
		}else if($statusId=='7'){//ค้างชำระเงิน 
            $color = '#F07120';
		}else{ //แจ้งจัดส่ง , ยกเลิก
            $color = 'black';
        }
        return $color;
	}

}



/*-----------------------------------------------------*/
#Global callback functions
/*-----------------------------------------------------*/
function _ctrim($n){
	return trim($n, "\x00..\x1F");
}
