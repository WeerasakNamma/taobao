<?php
require $base->get('BASEDIR').'/assets/libs/PHPMailer/PHPMailerAutoload.php';
 class Mailer {
 	private static $instance = false;
 	public static function getInstance() {
	    if (!self::$instance) {
	      self::$instance = new Mailer();
	    }

	    return self::$instance;
	}

	public $setFromEmail;
	public $setFromName;
	public $addAddressEmail;
	public $addAddressName;
	public $Subject;
	public $msgHTML;
	public $msgBody;
	public $mutiAddress = array();
	public $mailtemp = array();

   public $userid;
   public $msg;

	private function _sendmail(){

		//date_default_timezone_set('Etc/UTC');
		//return true;exit();
		$base = Base::getInstance();

		if($this->setFromEmail==''){
			$this->setFromEmail = $base->get('MAIL_USERNAME');
		}
		if($this->setFromName==''){
			$this->setFromName = 'TAOBAO CHINA CARGO';
		}




		$mail = new PHPMailer;

		$mail->isSMTP();

		$mail->SMTPDebug = 0;

		$mail->Debugoutput = 'html';

		$mail->CharSet = 'utf-8';

		$mail->Host = $base->get('MAIL_HOST');

		$mail->Port = $base->get('MAIL_PORT');

		$mail->SMTPAuth = true;

		$mail->SMTPSecure = "tls";

		$mail->Username = $base->get('MAIL_USERNAME');

		$mail->Password = $base->get('MAIL_PASSWORD');

		$mail->setFrom($this->setFromEmail, $this->setFromName);
		//$mail->setFrom('testmail@demo.siamgolive.com', $this->setFromName);

		//$mail->addReplyTo('replyto@example.com', 'First Last');

		$mail->addAddress($this->addAddressEmail, $this->addAddressName);

		$mail->Subject = $this->Subject;

		$mail->msgHTML($this->msgHTML);

		//$mail->AltBody = 'This is a plain-text message body';

		//$mail->addAttachment('images/phpmailer_mini.png');

		//send the message, check for errors
		if($this->_checkTempAddress()){
			if (!$mail->send()) {
			    echo "Mailer Error: " . $mail->ErrorInfo;
			} else {
			    return true;
			}
		}else{
			return true;
		}

	}
	public function clearMailTemp(){
		$this->mailtemp = array();
	}
	private function _checkTempAddress(){

		$resultReturn = true;

		if(count($this->mailtemp)>0){
			foreach($this->mailtemp as $templist){
				if($templist==$this->addAddressEmail){
					$resultReturn = false;
				}
			}
		}
		if($resultReturn){
			$this->mailtemp[] = $this->addAddressEmail;
		}

		return $resultReturn;
	}
	public function sendMail(){
		$base = Base::getInstance();
		//$this->_renderTemplate();
		// $resultreturn = $this->_sendmail(); ปิดไว้ไม่ให้ส่งเมลล์สำหรับ Test
		return $resultreturn;
	}
	public function SendingToNoti(){

		$resultreturn = $this->_SendingToNoti();
		return $resultreturn;
	}



	private function _multiSending(){
		$base = Base::getInstance();
		$this->_renderTemplate();

		if(count($this->mutiAddress)>=1){
			foreach($this->mutiAddress as $email){
				$this->addAddressEmail = $email['email'];
				$this->addAddressName = $email['name'];
				$resultreturn = $this->_sendmail();
			}
			return true;
		}else{
			return true;
		}
	}
	private function _SendingToNoti(){
		$base = Base::getInstance();

      $member = new Member();

      $base->set('user_id',$this->userid);
      $memberinfo = $member->memberInfomationByID();

      if($memberinfo['id']!=''){

         
         $mssg .= $this->msg;
         $this->Subject = $this->Subject." :: Taobao Chiana Cargo";
         $this->msgHTML = $mssg;
         $this->addAddressEmail = $memberinfo['user_email'];
         $this->addAddressName = $memberinfo['user_name'];
         $resultreturn = $this->_sendmail();
      }



	}



 }
?>
