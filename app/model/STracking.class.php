<?php
 class STracking {

 	public function importTracking(){
		$resultreturn = $this->_reeadExcel();
		return $resultreturn;
	}
	public function trackingList(){
		$resultreturn = $this->_trackingList();
		return $resultreturn;
	}
   public function trackingListHold(){
		$resultreturn = $this->_trackingListHold();
		return $resultreturn;
	}
	public function trackingListByStatus(){
		$resultreturn = $this->_trackingListByStatus();
		return $resultreturn;
	}
	public function trackingListByUser(){
		$resultreturn = $this->_trackingListByUser();
		return $resultreturn;
	}
	public function trackingListByUserStatus(){
		$resultreturn = $this->_trackingListByUserStatus();
		return $resultreturn;
	}
   public function trackingListByUserStatusAdmin(){
		$resultreturn = $this->_trackingListByUserStatusAdmin();
		return $resultreturn;
	}
	public function trackingInfo(){
		$resultreturn = $this->_trackingInfo();
		return $resultreturn;
	}
	
	public function trackingInfoUpweight(){
		$resultreturn = $this->_trackingInfoUpweight();
		return $resultreturn;
	}
	
	public function updateTracking(){
		$resultreturn = $this->_updateTracking();
		return $resultreturn;
	}
	public function deleteTracking(){
		$resultreturn = $this->User();
		return $resultreturn;
	}
	public function updateTrackingGroup(){
		$resultreturn = $this->_updateTrackingGroup();
		return $resultreturn;
	}
	public function saveGetShipping(){
		$resultreturn = $this->_saveGetShipping();
		return $resultreturn;
	}
	public function shippingListByuser(){
		$resultreturn = $this->_shippingListByuser();
		return $resultreturn;
	}
   public function saveGetShippingByAdmin(){
		$resultreturn = $this->_saveGetShippingByAdmin();
		return $resultreturn;
	}
	public function shippingListAll(){
		$resultreturn = $this->_shippingListAll();
		return $resultreturn;
	}
    public function shippingListForTh(){
		$resultreturn = $this->_shippingListForTh();
		return $resultreturn;
	}
	public function deleteShipping(){
		$resultreturn = $this->_deleteShipping();
		return $resultreturn;
	}
	public function shippingInfo(){
		$resultreturn = $this->_shippingInfo();
		return $resultreturn;
	}
	public function updateGetShipping(){
		$resultreturn = $this->_updateGetShipping();
		return $resultreturn;
	}
   public function updateGetShipping2(){
		$resultreturn = $this->_updateGetShipping2();
		return $resultreturn;
	}
	public function getRateByOrder($order_info){
		$resultreturn = $this->_getRateByOrder($order_info);
		return $resultreturn;
	}
	public function listTrackingByLink(){
		$resultreturn = $this->_listTrackingByLink();
		return $resultreturn;
	}
	public function getMoneyFromWallet(){
		$resultreturn = $this->_getMoneyFromWallet();
		return $resultreturn;
	}
   public function updateOrderTracking(){
		$resultreturn = $this->_updateOrderTracking();
		return $resultreturn;
	}
   public function checkTrackingNo(){
		$resultreturn = $this->_checkTrackingNo();
		return $resultreturn;
	}
   public function updateTrackingHold(){
		$resultreturn = $this->_updateTrackingHold();
		return $resultreturn;
	}
   public function updateTrackingHoldStatus(){
		$resultreturn = $this->_updateTrackingHoldStatus();
		return $resultreturn;
	}
    public function updateTHShipping(){
		$resultreturn = $this->_updateTHShipping();
		return $resultreturn;
	}
    public function deleteTrackingUser(){
		$resultreturn = $this->_deleteTrackingUser();
		return $resultreturn;
    }
    public function checkBillingData(){
		$resultreturn = $this->_checkBillingData();
		return $resultreturn;
	}
	//KAE
	public function getTrackingNumber($tracking_number){
		$resultreturn = $this->get_trackingID($tracking_number);
		return $resultreturn;
	}

	private function _reeadExcel(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$filename = $this->_uploadTemp();

		require_once $base->get('BASEDIR').'/assets/phpexcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$inputFileName = $base->get('BASEDIR')."/uploads/temp/".$filename;
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($inputFileName);



		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
	    $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
		    if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
		        ++$r;
		        foreach($headingsArray as $columnKey => $columnHeading) {
		            $namedDataArray[$r][$columnKey] = $dataRow[$row][$columnKey];
		        }
		    }
	    }
	    unlink($inputFileName);

	    if(count($namedDataArray)>0){
			foreach($namedDataArray as $key=>$list){
				$base->set('tracking_id',$list['A']);
				$base->set('status',$list['G']);
				$rss = $this->_checkTracking();
				if($rss['tracking_id']==''){

					$cbm = (intval($list['I'])*intval($list['J'])*intval($list['K']))/1000000;

					$sql = "INSERT INTO web_tracking(
													tracking,
													member_code,
													saller_name,
													order_id,
													order_type,
													ship_by,
													status,
													weight,
													dime_x,
													dime_y,
													dime_w,
													other_desc,
													other_price,
													create_dtm,
													create_by,
                                       update_dtm,
													update_by
													)
													VALUES(
													".GF::quote($list['A']).",
													".GF::quote($list['B']).",
													".GF::quote($list['C']).",
													".GF::quote($list['D']).",
													".GF::quote($list['E']).",
													".GF::quote($list['F']).",
													".GF::quote($list['G']).",
													".GF::quote($list['H']).",
													".GF::quote($list['I']).",
													".GF::quote($list['J']).",
													".GF::quote($list['K']).",
													".GF::quote($list['L']).",
													".GF::quote($list['M']).",
													NOW(),
													".GF::quote($user_id).",
                                       NOW(),
													".GF::quote($user_id)."
													)";
					//echo $sql;
					$res = $db->Execute($sql);
					$last_id = $db->Insert_ID();


					$sql = "INSERT INTO web_tracking_history(
													tracking_id,
													status,
													create_dtm,
													create_by
													)
													VALUES(
													".GF::quote($last_id).",
													".GF::quote($list['F']).",
													NOW(),
													".GF::quote($user_id)."
													)";
					$res = $db->Execute($sql);
					$base->set('order_id',$list['D']);
					$orderinfo = $this->_orderInfo();
					//GF::print_r($orderinfo);
					$base->set('odr_type',$list['E']);
					$base->set('odr_by',$list['F']);
					$rateAll = $this->_getRateByOrder($orderinfo);

					$totalQue = ($list['I']*$list['J']*$list['K'])/1000000;
					$priceQue = $totalQue*$rateAll['que'];
					$priceKilo = $list['H']*$rateAll['kilo'];

					$pricTrue = $priceKilo;
					if($priceQue>=$priceKilo){
						$pricTrue = $priceQue;
					}

					$sql = "UPDATE web_tracking SET price_weight=".GF::quote($priceKilo).",price_dimen=".GF::quote($priceQue).",price_true=".GF::quote($pricTrue).",cbm=".GF::quote($totalQue)." WHERE tracking_id=".GF::quote($last_id);
					$db->Execute($sql);

					//GF::print_r($list['D']);
					if($orderinfo['order_number']!=''){
						$sql = "UPDATE web_order_item SET
														product_sku_1=".GF::quote($list['A']).",
														status=".GF::quote($list['G']).",

														update_dtm=NOW()
														WHERE order_id=".GF::quote($orderinfo['id'])." AND saller_number=".GF::quote($list['C'])." AND status!='D' AND status!='14' ";
														//echo $sql;
						$res = $db->Execute($sql);
					}else{
						$sql = "UPDATE web_order_item SET
													status=".GF::quote($list['F']).",
													update_dtm=NOW()
													WHERE product_sku_1=".GF::quote($list['A'])." AND status!='D' AND status!='14' ";
						$res = $db->Execute($sql);
					}




				}else{

					if($rss['status']!=$list['F']){

						$sql = "UPDATE web_tracking SET
													member_code=".GF::quote($list['B']).",
													saller_name=".GF::quote($list['C']).",
													order_id=".GF::quote($list['D']).",
													order_type=".GF::quote($list['E']).",
													ship_by=".GF::quote($list['F']).",
													weight=".GF::quote($list['H']).",
													dime_x=".GF::quote($list['I']).",
													dime_y=".GF::quote($list['J']).",
													dime_w=".GF::quote($list['K']).",
													other_desc=".GF::quote($list['L']).",
													other_price=".GF::quote($list['M']).",
													status=".GF::quote($list['G']).",
                                       update_dtm=NOW(),
                                       update_by=".GF::quote($user_id)."
													WHERE tracking=".GF::quote($list['A']);
						$res = $db->Execute($sql);

						//echo $sql;

						$sql = "INSERT INTO web_tracking_history(
													tracking_id,
													status,
													create_dtm,
													create_by
													)
													VALUES(
													".GF::quote($rss['tracking_id']).",
													".GF::quote($list['G']).",
													NOW(),
													".GF::quote($user_id)."
													)";
						$res = $db->Execute($sql);
						$base->set('order_id',$list['D']);
						$orderinfo = $this->_orderInfo();

						$base->set('odr_type',$list['E']);
						$base->set('odr_by',$list['F']);
						$rateAll = $this->_getRateByOrder($orderinfo);

						$totalQue = ($list['I']*$list['J']*$list['K'])/1000000;
						$priceQue = $totalQue*$rateAll['que'];
						$priceKilo = $list['H']*$rateAll['kilo'];

						$pricTrue = $priceKilo;
						if($priceQue>=$priceKilo){
							$pricTrue = $priceQue;
						}

						$sql = "UPDATE web_tracking SET price_weight=".GF::quote($priceKilo).",price_dimen=".GF::quote($priceQue).",price_true=".GF::quote($pricTrue).",cbm=".GF::quote($totalQue)." WHERE tracking=".GF::quote($list['A']);
						$db->Execute($sql);

						if($orderinfo['product_link']!=''){
						$sql = "UPDATE web_order_item SET
															product_sku_1=".GF::quote($list['A']).",
															status=".GF::quote($list['G']).",

															update_dtm=NOW()
															WHERE order_id=".GF::quote($orderinfo['id'])." AND saller_number=".GF::quote($list['C'])." AND status!='D' AND status!='14' ";
															//echo $sql;
							$res = $db->Execute($sql);
						}else{
							$sql = "UPDATE web_order_item SET
														status=".GF::quote($list['G']).",
														update_dtm=NOW()
														WHERE product_sku_1=".GF::quote($list['A'])." AND status!='D' AND status!='14' ";
							$res = $db->Execute($sql);
						}

					}else{
						$sql = "UPDATE web_tracking SET
													member_code=".GF::quote($list['B']).",
													saller_name=".GF::quote($list['C']).",
													order_id=".GF::quote($list['D']).",
													order_type=".GF::quote($list['E']).",
													ship_by=".GF::quote($list['F']).",
													weight=".GF::quote($list['H']).",
													dime_x=".GF::quote($list['I']).",
													dime_y=".GF::quote($list['J']).",
													dime_w=".GF::quote($list['K']).",
													other_desc=".GF::quote($list['L']).",
													other_price=".GF::quote($list['M']).",
													status=".GF::quote($list['G']).",
                                       update_dtm=NOW(),
                                       update_by=".GF::quote($user_id)."
													WHERE tracking=".GF::quote($list['A']);
						$res = $db->Execute($sql);

						$base->set('order_id',$list['D']);
						$orderinfo = $this->_orderInfo();

						$base->set('odr_type',$list['E']);
						$base->set('odr_by',$list['F']);
						$rateAll = $this->_getRateByOrder($orderinfo);

						//GF::print_r($rateAll);

						$totalQue = ($list['I']*$list['J']*$list['K'])/1000000;
						$priceQue = $totalQue*$rateAll['que'];
						$priceKilo = $list['H']*$rateAll['kilo'];

						$pricTrue = $priceKilo;
						if($priceQue>=$priceKilo){
							$pricTrue = $priceQue;
						}

						$sql = "UPDATE web_tracking SET price_weight=".GF::quote($priceKilo).",price_dimen=".GF::quote($priceQue).",price_true=".GF::quote($pricTrue).",cbm=".GF::quote($totalQue)." WHERE tracking=".GF::quote($list['A']);
						$db->Execute($sql);

					}

				}
			}
		}
		//GF::print_r($namedDataArray);

	    return true;


	}

	private function _getRateByOrder($orderinfo){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();

		//$base->set('order_id',$base->get('order_number'));
		//$orderinfo = $this->_orderInfo();

		$base->set('user_id',$orderinfo['member_id']);
		$memberInfo = $member->memberInfomationByID();

		$arrReturn = array();

		$sql = "SELECT rate FROM project_user_rate WHERE user_id=".GF::quote($memberInfo['user_role_id'])." AND unit='16' AND order_type=".GF::quote($base->get('odr_type'))." AND ship_by=".GF::quote($base->get('odr_by'));
		//echo $sql;
		$res = $db->Execute($sql);

		$arrReturn['que'] = intval($res->fields['rate']);

		$sqlw = "SELECT rate FROM project_user_rate WHERE user_id=".GF::quote($memberInfo['user_role_id'])." AND unit='17' AND order_type=".GF::quote($base->get('odr_type'))." AND ship_by=".GF::quote($base->get('odr_by'));
		$resw = $db->Execute($sqlw);

		$arrReturn['kilo'] = intval($resw->fields['rate']);

		return $arrReturn;
	}

	private function _getRateByMemberCode(){

	}

	private function _uploadTemp(){
		$base = Base::getInstance();
		if(!empty($_FILES['tracking']['name'])){
			$picname = $_FILES['tracking']['name'];

			$random = GF::randomNum(25);

			$type = explode('.',$picname);
			$dest_picname_o = $base->get('BASEDIR')."/uploads/temp/".$random.".".end($type);

			$tmp_file = $_FILES['tracking']['tmp_name'];
			@copy($tmp_file, $dest_picname_o);
			$payment_slip = $random.".".end($type);

			return $payment_slip;

		}
	}
	private function _checkTracking(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$tracking_id = $base->get('tracking_id');
		$status = $base->get('status');

		$sql = "SELECT * FROM web_tracking WHERE tracking=".GF::quote($tracking_id);
 		$res = $db->Execute($sql);

 		$arrReturn = array();
 		$arrReturn['tracking_id'] = $res->fields['tracking_id'];
 		$arrReturn['status'] = $res->fields['status'];
		return $arrReturn;
	}
	private function _trackingList(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();



			$dtatReturn = array();
            $cond = '';

            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $list_ref = GF::refgroup();
                if(count($list_ref)>0){
                //GF::print_r($list_ref);
                foreach ($list_ref as $key => $value) {
                    $list_ref[$key] = GF::quote(GF::user_code($value));
                }
				//edit by hatairat 27-11-64
                $ref_str = implode(',', $list_ref);
                $cond .= " member_code IN('b999',".$ref_str.") ";
                }else{
                $cond .= " member_code IN ('b999','X') ";
				//end edit
                }
            }else{
                $cond = '1=1';
            }

            if($_SESSION['tra_search']['tracking']!=''){

                $cond .= " AND tracking LIKE '%".$_SESSION['tra_search']['tracking']."%' ";
            }

            if($_SESSION['tra_search']['order_number']!=''){

                $cond .= " AND order_id LIKE '%".$_SESSION['tra_search']['order_number']."%' ";
            }

            if($_SESSION['tra_search']['user_code']!=''){
                $cond .= " AND member_code =".GF::quote($_SESSION['tra_search']['user_code'])." ";
            }

            if($_SESSION['tra_search']['status']!=''){
                $cond .= " AND status =".GF::quote($_SESSION['tra_search']['status'])." ";
            }

			$sql = "SELECT * FROM web_tracking WHERE  ".$cond." ORDER BY tracking_id DESC";

         //echo $sql;

			$res = $db->Execute($sql);
            $count = $res->NumRows();

                $base->set('allPage',ceil($count/20));

            $page = $base->get('GET.page');

            $numr = 20;
            $start = "";
            $end = "";
            if($page==1||empty($page)){
                $start = 0;
                $page = 1;
                //$end = $numr;
            }else{
                //$end = ($page*$numr);
                $start = ($page*$numr)-$numr;
            }
            $condNum = " LIMIT ".$start.",".$numr;

            $base->set('currpage',$page);

            $res = $db->Execute($sql.$condNum);
            
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
				$member = GF::memberinfo($res->fields['member_code']);
				$ArrList[$i]['member_code'] = $member['user_code'];
                $ArrList[$i]['user_id'] = $member['id'];
                $ArrList[$i]['weight_cn'] = $res->fields['weight_cn'];
				$ArrList[$i]['saller_name'] = $res->fields['saller_name'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
				$ord_type = GF::masterdata($res->fields['order_type']);
				$ArrList[$i]['order_type'] = $ord_type['master_name'];
				$stt = GF::masterdataval('3',$res->fields['status']);
				$ArrList[$i]['status'] = $stt['master_name'];
				$ArrList[$i]['status_value'] = $stt['master_value'];
				$ArrList[$i]['weight'] = $res->fields['weight'];
				$ArrList[$i]['cbm'] = $res->fields['cbm'];
				$ArrList[$i]['price_true'] = $res->fields['price_true'];
				
				$ArrList[$i]['price_weight'] = $res->fields['price_weight'];
				
				
				$ArrList[$i]['price_que'] = $res->fields['price_que'];
				$ArrList[$i]['other_price'] = $res->fields['other_price'];
                $ArrList[$i]['other_desc'] = $res->fields['other_desc'];
				$ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];

				$sql5 = "SELECT * FROM web_shipping_item WHERE tracking=".GF::quote($res->fields['tracking']);
				$res5 = $db->Execute($sql5);



				$i++;
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
	}
   private function _trackingListHold(){

		$base = Base::getInstance();
		$db = DB::getInstance();
      $db2 = DB2::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		//GF::print_r($memberInfomation);


		$dtatReturn = array();
         $cond = '';

         $agency =  $memberInfomation['agency'];
         if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
               //GF::print_r($list_ref);
               foreach ($list_ref as $key => $value) {
                  $list_ref[$key] = GF::quote(GF::user_code($value));
               }
               $ref_str = implode(',', $list_ref);
               $cond .= " member_code IN(".$ref_str.") ";
            }else{
               $cond .= " member_code ='X' ";
            }
         }else{
            $cond = '1=1';
         }

         if($_SESSION['tracking_hold_search']['status']!=''){

   			if($_SESSION['tracking_hold_search']['status']=='1'){
   				//$cond = " AND status=".GF::quote($type);
               $cond .= " AND hold_status='N' ";
   			}else{
   				$cond .= " AND hold_status='Y' ";
   			}
   		}
   		if($_SESSION['tracking_hold_search']['order_number']!=''){

   			$cond .= " AND order_id LIKE '%".$_SESSION['tracking_hold_search']['order_number']."%' ";
   		}

         if($_SESSION['tracking_hold_search']['user_code']!=''){
   			$cond .= " AND member_code =".GF::quote($_SESSION['tracking_hold_search']['user_code'])." ";
   		}

   		if($_SESSION['tracking_hold_search']['create_dtm']!=''){
   			$date_check = date('Y-m-d',strtotime($_SESSION['tracking_hold_search']['create_dtm']));
   			$cond .= " AND hold_date >=".GF::quote($date_check." 00:00:00");
   		}

         //echo $cond;exit();

         $master1 = $db2->get("project_master","*",array("id"=>'45'));
         $master3 = $db2->get("project_master","*",array("id"=>'46'));

         $now = date("Y-m-d H:i:s");
         $Old1 = date('Y-m-d H:i:s',strtotime($now . "-".$master1['master_char']." days"));
         $Old3 = date('Y-m-d H:i:s',strtotime($now . "-".$master3['master_char']." days"));

         $sql = "SELECT tracking_id FROM web_tracking WHERE  ".$cond." AND status='1' AND create_dtm<=".GF::quote($Old1)." ORDER BY create_dtm ASC";
         //echo $sql;exit();
         $res = $db->Execute($sql);
         $x=0;
			$idArr = array();
			while(!$res->EOF){
            $idArr[$x] = $res->fields['tracking_id'];
            $x++;
				$res->MoveNext();
			}

         $sql = "SELECT tracking_id FROM web_tracking WHERE  ".$cond." AND status='3' AND create_dtm<=".GF::quote($Old3)." ORDER BY create_dtm ASC";
         //echo $sql;exit();
         $res = $db->Execute($sql);

			while(!$res->EOF){
            $idArr[$x] = $res->fields['tracking_id'];
            $x++;
				$res->MoveNext();
			}
         $idexplode = 'D';
         if(count($idArr)>0){
            $idexplode = implode(',', $idArr);
         }

         if($idexplode=='D'){
            return array();
            exit();
         }
			//edit by hatairat 22-311-64 
			$sql = "SELECT * FROM web_tracking WHERE  tracking_id IN(".$idexplode.") and member_code <> 'b999' ORDER BY status ASC";

         //echo $sql;

			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){

				if($res->fields['tracking']!='b999'){
					
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
            $ArrList[$i]['export_date'] = $this->_getExportDate($res->fields['order_id']);
				$member = GF::memberinfo($res->fields['member_code']);
				$ArrList[$i]['member_code'] = $member['user_code'];
            $ArrList[$i]['user_id'] = $member['id'];
            $ArrList[$i]['weight_cn'] = $res->fields['weight_cn'];
				$ArrList[$i]['saller_name'] = $res->fields['saller_name'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
				$ord_type = GF::masterdata($res->fields['order_type']);
				$ArrList[$i]['order_type'] = $ord_type['master_name'];
				$stt = GF::masterdataval('3',$res->fields['status']);
				$ArrList[$i]['status'] = $stt['master_name'];
				$ArrList[$i]['status_value'] = $stt['master_value'];
				$ArrList[$i]['weight'] = $res->fields['weight'];
				$ArrList[$i]['cbm'] = $res->fields['cbm'];
				$ArrList[$i]['price_true'] = $res->fields['price_true'];
				$ArrList[$i]['price_weight'] = $res->fields['price_weight'];
				$ArrList[$i]['price_que'] = $res->fields['price_que'];
				$ArrList[$i]['other_price'] = $res->fields['other_price'];
            $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
            $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];

            $ArrList[$i]['hold_status'] = $res->fields['hold_status'];
            $ArrList[$i]['hold_text'] = $res->fields['hold_text'];
            $ArrList[$i]['hold_date'] = $res->fields['hold_date'];
            $ArrList[$i]['hold_comment'] = $res->fields['hold_comment'];
            $ArrList[$i]['hold_count'] = $res->fields['hold_count'];
            $ArrList[$i]['other_desc'] = $res->fields['other_desc'];
			$ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
				$i++;

				}
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
	}

   private function _trackingListHoldEMail(){

		$base = Base::getInstance();
		$db = DB::getInstance();
      $db2 = DB2::getInstance();

		//$member = new Member();


 		//$memberInfomation = $member->memberInfomation();



			$dtatReturn = array();
         $cond = '';

         // $agency =  $memberInfomation['agency'];
         // if($agency=='A'){
         //    $list_ref = GF::refgroup();
         //    if(count($list_ref)>0){
         //
         //       foreach ($list_ref as $key => $value) {
         //          $list_ref[$key] = GF::quote(GF::user_code($value));
         //       }
         //       $ref_str = implode(',', $list_ref);
         //       $cond .= " member_code IN(".$ref_str.") ";
         //    }else{
         //       $cond .= " member_code ='X' ";
         //    }
         // }else{
         //    $cond = '1=1';
         // }



         // $now = date("Y-m-d H:i:s");
         // $Old1 = date('Y-m-d H:i:s',strtotime($now . "-5 days"));
         // $Old3 = date('Y-m-d H:i:s',strtotime($now . "-10 days"));

         // $sql = "SELECT tracking_id FROM web_tracking WHERE  ".$cond." AND status='1' AND update_dtm<=".GF::quote($Old1)." ORDER BY update_dtm ASC";
         // $res = $db->Execute($sql);
         // $x=0;
			// $idArr = array();
			// while(!$res->EOF){
         //    $idArr[$x] = $res->fields['tracking_id'];
         //    $x++;
			// 	$res->MoveNext();
			// }
         //
         // $sql = "SELECT tracking_id FROM web_tracking WHERE  ".$cond." AND status='3' AND update_dtm<=".GF::quote($Old3)." ORDER BY update_dtm ASC";
         // $res = $db->Execute($sql);
         //
			// while(!$res->EOF){
         //    $idArr[$x] = $res->fields['tracking_id'];
         //    $x++;
			// 	$res->MoveNext();
			// }
         // $idexplode = 'D';
         // if(count($idArr)>0){
         //    $idexplode = implode(',', $idArr);
         // }

         $status = $base->get('stt');
         $idin = $base->get('idds');
         $idexplode = implode(',', $idin);


			$sql = "SELECT * FROM web_tracking WHERE  tracking_id IN(".$idexplode.") AND status='".$status."' ORDER BY status ASC";

         //echo $sql;

			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
            $ArrList[$i]['export_date'] = $this->_getExportDate($res->fields['order_id']);
				$member = GF::memberinfo($res->fields['member_code']);
				$ArrList[$i]['member_code'] = $member['user_code'];
            $ArrList[$i]['user_id'] = $member['id'];
            $ArrList[$i]['weight_cn'] = $res->fields['weight_cn'];
				$ArrList[$i]['saller_name'] = $res->fields['saller_name'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
				$ord_type = GF::masterdata($res->fields['order_type']);
				$ArrList[$i]['order_type'] = $ord_type['master_name'];
				$stt = GF::masterdataval('3',$res->fields['status']);
				$ArrList[$i]['status'] = $stt['master_name'];
				$ArrList[$i]['status_value'] = $stt['master_value'];
				$ArrList[$i]['weight'] = $res->fields['weight'];
				$ArrList[$i]['cbm'] = $res->fields['cbm'];
				$ArrList[$i]['price_true'] = $res->fields['price_true'];
				$ArrList[$i]['price_weight'] = $res->fields['price_weight'];
				$ArrList[$i]['price_que'] = $res->fields['price_que'];
				$ArrList[$i]['other_price'] = $res->fields['other_price'];
            $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];

            $ArrList[$i]['hold_status'] = $res->fields['hold_status'];
            $ArrList[$i]['hold_text'] = $res->fields['hold_text'];
            $ArrList[$i]['hold_date'] = $res->fields['hold_date'];
            $ArrList[$i]['hold_comment'] = $res->fields['hold_comment'];
            $ArrList[$i]['hold_count'] = $res->fields['hold_count'];
            $ArrList[$i]['other_desc'] = $res->fields['other_desc'];
			$ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
				$i++;
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
	}

   private function _getExportDate($order_id){

		$base = Base::getInstance();
		$db = DB2::getInstance();

      $result = $db->get("web_order","*",array("order_id"=>$order_id));

      return $result['export_date'];

   }
	private function _trackingListByStatus(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();



			$dtatReturn = array();
         $cond = '';
         $agency =  $memberInfomation['agency'];
         if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
               //GF::print_r($list_ref);
               foreach ($list_ref as $key => $value) {
                  $list_ref[$key] = GF::quote(GF::user_code($value));
               }
               $ref_str = implode(',', $list_ref);
               $cond .= " AND member_code IN(".$ref_str.") ";
            }else{
               $cond .= " AND member_code ='X' ";
            }
         }

			$sql = "SELECT * FROM web_tracking WHERE status=".GF::quote($base->get('status')).$cond." ORDER BY create_dtm DESC";



			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
				$member = GF::memberinfo($res->fields['member_code']);
				$ArrList[$i]['member_code'] = $member['user_code'];
            $ArrList[$i]['weight_cn'] = $res->fields['weight_cn'];
				$ArrList[$i]['saller_name'] = $res->fields['saller_name'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
				$ord_type = GF::masterdata($res->fields['order_type']);
				$ArrList[$i]['order_type'] = $ord_type['master_name'];
				$stt = GF::masterdataval('3',$res->fields['status']);
				$ArrList[$i]['status'] = $stt['master_name'];
				$ArrList[$i]['status_value'] = $stt['master_value'];
				$ArrList[$i]['weight'] = $res->fields['weight'];
				$ArrList[$i]['cbm'] = $res->fields['cbm'];
				$ArrList[$i]['price_true'] = $res->fields['price_true'];
				$ArrList[$i]['price_weight'] = $res->fields['price_weight'];
				$ArrList[$i]['price_que'] = $res->fields['price_que'];
				$ArrList[$i]['other_price'] = $res->fields['other_price'];
            $ArrList[$i]['other_desc'] = $res->fields['other_desc'];
			$ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
				$i++;
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
	}

	private function _trackingListByUserStatus(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();
		$user_id = $memberInfomation['user_code'];


			$dtatReturn = array();

			$sql = "SELECT * FROM web_tracking WHERE status=".GF::quote($base->get('status'))." AND member_code=".GF::quote($user_id)." AND status!='D' ORDER BY create_dtm DESC";



			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
				$member = GF::memberinfo($res->fields['member_code']);
				$ArrList[$i]['member_code'] = $member['user_code'];
				$ArrList[$i]['saller_name'] = $res->fields['saller_name'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
                $ArrList[$i]['weight_cn'] = $res->fields['weight_cn'];
				$ord_type = GF::masterdata($res->fields['order_type']);
				$ArrList[$i]['order_type'] = $ord_type['master_name'];
				$stt = GF::masterdataval('3',$res->fields['status']);
				$ArrList[$i]['status'] = $stt['master_name'];
				$ArrList[$i]['flag'] = $res->fields['flag'];
				$ArrList[$i]['status_value'] = $stt['master_value'];
				$ArrList[$i]['weight'] = $res->fields['weight'];
				$ArrList[$i]['cbm'] = $res->fields['cbm'];
				$ArrList[$i]['price_true'] = $res->fields['price_true'];
				$ArrList[$i]['price_weight'] = $res->fields['price_weight'];
				$ArrList[$i]['price_que'] = $res->fields['price_que'];
				$ArrList[$i]['other_price'] = $res->fields['other_price'];
            $ArrList[$i]['other_desc'] = $res->fields['other_desc'];
			$ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
				$i++;
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
	}

   private function _trackingListByUserStatusAdmin(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		//$memberInfomation = $member->memberInfomation();
		//$user_id = $memberInfomation['user_code'];

      $cond = '';

      if($_SESSION['track_search']['user_code']!=''){
         $cond .= " AND member_code=".GF::quote($_SESSION['track_search']['user_code'])." ";
      }

      // if($_SESSION['tracking_search']['tracking']!=''){
      //    $cond .= " AND tracking=".GF::quote($_SESSION['tracking_search']['tracking'])." ";
      // }


			$dtatReturn = array();
			//edit by hatairat 22-11-64
			$sql = "SELECT * FROM web_tracking WHERE status=".GF::quote($base->get('status')).$cond." AND status!='D' and member_code <> 'b999' ORDER BY create_dtm DESC";

         //echo $sql;

			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
				$member = GF::memberinfo($res->fields['member_code']);
                $ArrList[$i]['user_name'] = $member['user_name'];
				$ArrList[$i]['member_code'] = $member['user_code'];
				$ArrList[$i]['saller_name'] = $res->fields['saller_name'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
                $ArrList[$i]['weight_cn'] = $res->fields['weight_cn'];
				$ord_type = GF::masterdata($res->fields['order_type']);
				$ArrList[$i]['order_type'] = $ord_type['master_name'];
				$stt = GF::masterdataval('3',$res->fields['status']);
				$ArrList[$i]['status'] = $stt['master_name'];
				$ArrList[$i]['flag'] = $res->fields['flag'];
				$ArrList[$i]['status_value'] = $stt['master_value'];
				$ArrList[$i]['weight'] = $res->fields['weight'];
				$ArrList[$i]['cbm'] = $res->fields['cbm'];
				$ArrList[$i]['price_true'] = $res->fields['price_true'];
				$ArrList[$i]['price_weight'] = $res->fields['price_weight'];
				$ArrList[$i]['price_que'] = $res->fields['price_que'];
				$ArrList[$i]['other_price'] = $res->fields['other_price'];
                $ArrList[$i]['other_desc'] = $res->fields['other_desc'];
			    $ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
				$i++;
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
	}

	private function _trackingListByUser(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['user_code'];

      $cond = '';
      $status_search = $base->get('GET.status');
      if($status_search !=''){
         $cond = " AND status=".GF::quote($status_search)." ";
      }


			$dtatReturn = array();

			$sql = "SELECT * FROM web_tracking WHERE member_code=".GF::quote($user_id).$cond." AND status!='D' ORDER BY create_dtm DESC";


			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
				$member = GF::memberinfo($res->fields['member_code']);
				$ArrList[$i]['member_code'] = $member['user_code'];
				$ArrList[$i]['saller_name'] = $res->fields['saller_name'];
				$ArrList[$i]['order_id'] = $res->fields['order_id'];
            $ArrList[$i]['weight_cn'] = $res->fields['weight_cn'];
				$ord_type = GF::masterdata($res->fields['order_type']);
				$ArrList[$i]['order_type'] = $ord_type['master_name'];
				$stt = GF::masterdataval('3',$res->fields['status']);
				$ArrList[$i]['status'] = $stt['master_name'];
				$ArrList[$i]['status_value'] = $stt['master_value'];
				$ArrList[$i]['weight'] = $res->fields['weight'];
				$ArrList[$i]['cbm'] = $res->fields['cbm'];
				
				$SQL3 = "SELECT * FROM project_user WHERE user_code = ".GF::quote($res->fields['member_code'])." ";
		$res3 = $db->Execute($SQL3);
        
if($res->fields['weight']<=0.5 && $res->fields['weight']>=0.1){

			//Comment By Weerasak 05/04/2565
			//$dataRate = "SELECT * FROM project_user_rate WHERE unit_code=".GF::quote($res->fields['price_rate'])." AND order_type=".GF::quote($res->fields['order_type'])." AND user_id =".GF::quote($res3->fields['user_role_id'])." ";

			//Edit By Weerasak 05/04/2565
			$dataRate = "SELECT * FROM project_user_rate WHERE user_id =".GF::quote($res3->fields['user_role_id']) ." AND order_type=".GF::quote($res->fields['order_type'])." AND ship_by =".GF::quote($res->fields['ship_by'])." ";
			
				
				$resDataRate = $db->Execute($dataRate);
				$price_true = 0;
            $price_dimen = 0;
            $cbm = 0;
            $price_weight = 0;
            $price_rate = '';
				foreach($resDataRate as $valsRate){
					
                    $SQLmasterCK = "SELECT * FROM project_master WHERE status='O' AND active_status='O' AND id = ".GF::quote($valsRate['unit']);
					$masterCK = $db->Execute($SQLmasterCK);
					


                   if($masterCK->fields['master_char']=='CALK'){
                        $price_weight = $valsRate['rate']*0.5;
                        if($price_true<$price_weight){
                            $price_true = $price_weight;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }
                    if($masterCK->fields['master_char']=='CALQ'){
                        $cbm =  ($res->fields['dime_x']*0.5)/1000000;
                        $price_dimen = $cbm*$valsRate['rate'];
                        if($price_true<$price_dimen){
                            $price_true = $price_dimen;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }
					
					
                }
			
			
			$ArrList[$i]['price_weight'] = $price_weight;
			$ArrList[$i]['price_true'] = $price_true;
			
}else{
				
				$ArrList[$i]['price_true'] = $res->fields['price_true'];
				$ArrList[$i]['price_weight'] = $res->fields['price_weight'];
}

				
				$ArrList[$i]['price_que'] = $res->fields['price_que'];
				$ArrList[$i]['other_price'] = $res->fields['other_price'];
            $ArrList[$i]['other_desc'] = $res->fields['other_desc'];
			$ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
				$i++;
				$res->MoveNext();
			}
			//print_r();
			$dtatReturn = $ArrList;

			return $ArrList;
	}

	private function _trackingInfo(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$tracking_id = $base->get('_ids');

		$sql = "SELECT * FROM web_tracking WHERE tracking_id=".GF::quote($tracking_id);
 		$res = $db->Execute($sql);
		
		$SQL3 = "SELECT * FROM project_user WHERE user_code = ".GF::quote($res->fields['member_code'])." ";
		$res3 = $db->Execute($SQL3);
		
		

 		$ArrList = array();
 		$ArrList['tracking_id'] = $res->fields['tracking_id'];
		$ArrList['tracking'] = $res->fields['tracking'];
		$member = GF::memberinfo($res->fields['member_code']);
		$ArrList['member_code'] = $member['user_code'];
		$ArrList['saller_name'] = $res->fields['saller_name'];
		$ArrList['order_id'] = $res->fields['order_id'];
		
       // $ArrList['weight_cn'] = $res->fields['weight_cn'];
		
		$ord_type = GF::masterdata($res->fields['order_type']);
		$ArrList['order_type'] = $ord_type['master_name'];
		$ArrList['order_type_t'] = $res->fields['order_type'];
		$stt = GF::masterdataval('3',$res->fields['status']);
		$ArrList['status_t'] = $res->fields['status'];
		$ArrList['status'] = $stt['master_name'];
		$ArrList['status_value'] = $stt['master_value']; 
		$ArrList['history'] = $this->_trackingHistory();
		
		$ArrList['weight'] = $res->fields['weight'];
		
		

		$ArrList['weight_cn'] = $res->fields['weight_cn'];

		
		
		$ArrList['cbm'] = $res->fields['cbm'];
		$ArrList['dime_x'] = $res->fields['dime_x'];
		$ArrList['dime_y'] = $res->fields['dime_y'];
		$ArrList['dime_w'] = $res->fields['dime_w'];
		$ArrList['dime_w'] = $res->fields['dime_w'];
		$ArrList['dime_w'] = $res->fields['dime_w'];
		if($res->fields['weight']<=0.5 && $res->fields['weight']>=0.1){
			
			//Comment By Weerasak 05/04/2565
			//$dataRate = "SELECT * FROM project_user_rate WHERE unit_code=".GF::quote($res->fields['price_rate'])." AND order_type=".GF::quote($res->fields['order_type'])." AND user_id =".GF::quote($res3->fields['user_role_id'])." ";
			
			//Edit By Weerasak 05/04/2565
			$dataRate = "SELECT * FROM project_user_rate WHERE user_id =".GF::quote($res3->fields['user_role_id']) ." AND order_type=".GF::quote($res->fields['order_type'])." AND ship_by =".GF::quote($res->fields['ship_by'])." ";
			
				
			$resDataRate = $db->Execute($dataRate);
			$price_true = 0;
            $price_dimen = 0;
            $cbm = 0;
            $price_weight = 0;
            $price_rate = '';
				
				foreach($resDataRate as $valsRate){
					
                    $SQLmasterCK = "SELECT * FROM project_master WHERE status='O' AND active_status='O' AND id = ".GF::quote($valsRate['unit']);
					$masterCK = $db->Execute($SQLmasterCK);
					


                   if($masterCK->fields['master_char']=='CALK'){
                        $price_weight = $valsRate['rate']*0.5;
                        if($price_true<$price_weight){
                            $price_true = $price_weight;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }
                    if($masterCK->fields['master_char']=='CALQ'){
                        $cbm =  ($ArrList[$i]['tracking_info']['dime_x']*0.5)/1000000;
                        $price_dimen = $cbm*$valsRate['rate'];
                        if($price_true<$price_dimen){
                            $price_true = $price_dimen;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }
					
					
                }
			
			
			$ArrList['price_weight'] = $price_weight;
			$ArrList['price_true'] = $price_true;
			
		}else{
		$ArrList['price_weight'] = $res->fields['price_weight'];
		$ArrList['price_true'] = $res->fields['price_true'];
		
		}
		
		$ArrList['price_dimen'] = $res->fields['price_dimen'];
		
		$ArrList['other_desc'] = $res->fields['other_desc'];
		$ArrList['other_price'] = $res->fields['other_price'];

        $ArrList['tracking_img'] = $res->fields['tracking_img'];
        $ArrList['num_box'] = $res->fields['num_box'];
        $ArrList['order_box'] = $res->fields['order_box'];
        $ArrList['tracking_img'] = $res->fields['tracking_img'];
        $ArrList['order_comment'] = $res->fields['order_comment'];
	    $ArrList['shipping_code'] = $res->fields['shipping_code'];
		return $ArrList;
	}
	
	
		private function _trackingInfoUpweight(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$tracking_id = $base->get('_ids');

		$sql = "SELECT * FROM web_tracking WHERE tracking_id=".GF::quote($tracking_id);
 		$res = $db->Execute($sql);

 		$ArrList = array();
 		$ArrList['tracking_id'] = $res->fields['tracking_id'];
		$ArrList['tracking'] = $res->fields['tracking'];
		$member = GF::memberinfo($res->fields['member_code']);
		$ArrList['member_code'] = $member['user_code'];
		$ArrList['saller_name'] = $res->fields['saller_name'];
		$ArrList['order_id'] = $res->fields['order_id'];
		
       // $ArrList['weight_cn'] = $res->fields['weight_cn'];
		
		$ord_type = GF::masterdata($res->fields['order_type']);
		$ArrList['order_type'] = $ord_type['master_name'];
		$ArrList['order_type_t'] = $res->fields['order_type'];
		$stt = GF::masterdataval('3',$res->fields['status']);
		$ArrList['status_t'] = $res->fields['status'];
		$ArrList['status'] = $stt['master_name'];
		$ArrList['status_value'] = $stt['master_value']; 
		$ArrList['history'] = $this->_trackingHistory();
		if($res->fields['weight']<=0.5 && $res->fields['weight']>=0.1){
			$ArrList['weight'] = 0.5;
		}else{
			$ArrList['weight'] = $res->fields['weight'];
		}
		
		if($res->fields['weight_cn']<=0.5 && $res->fields['weight_cn']>=0.1){
			$ArrList['weight_cn'] = 0.5;
		}else{
			$ArrList['weight_cn'] = $res->fields['weight_cn'];
		}
		
		
		$ArrList['cbm'] = $res->fields['cbm'];
		$ArrList['dime_x'] = $res->fields['dime_x'];
		$ArrList['dime_y'] = $res->fields['dime_y'];
		$ArrList['dime_w'] = $res->fields['dime_w'];
		$ArrList['dime_w'] = $res->fields['dime_w'];
		$ArrList['dime_w'] = $res->fields['dime_w'];
		$ArrList['price_weight'] = $res->fields['price_weight'];
		$ArrList['price_dimen'] = $res->fields['price_dimen'];
		$ArrList['price_true'] = $res->fields['price_true'];
		$ArrList['price_rate'] = $res->fields['price_rate']; //เพิ่ม Price_rate
		$ArrList['other_desc'] = $res->fields['other_desc'];
		$ArrList['other_price'] = $res->fields['other_price'];

        $ArrList['tracking_img'] = $res->fields['tracking_img'];
        $ArrList['num_box'] = $res->fields['num_box'];
        $ArrList['order_box'] = $res->fields['order_box'];
        $ArrList['tracking_img'] = $res->fields['tracking_img'];
        $ArrList['order_comment'] = $res->fields['order_comment'];
	    $ArrList['shipping_code'] = $res->fields['shipping_code'];
		$ArrList['ship_by'] = $res->fields['ship_by'];
		return $ArrList;
	}
	
	private function _trackingHistory(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
		$dtatReturn = array();

		$sql = "SELECT * FROM web_tracking_history WHERE tracking_id=".GF::quote($base->get('_ids'))." ORDER BY create_dtm ASC";
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$ArrList[$i]['create_by'] = $res->fields['create_by'];
			$stt = GF::masterdataval('3',$res->fields['status']);
			$ArrList[$i]['status'] = $stt['master_name'];
			$ArrList[$i]['status_value'] = $stt['master_value'];
			$i++;
			$res->MoveNext();
		}

		return $ArrList;
	}
	private function _orderInfo(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();

		$orderid = $base->get('order_id');

		$sql = "SELECT * FROM web_order WHERE order_number=".GF::quote($orderid);
		$res = $db->Execute($sql);


		return  $res->fields;

	}
	private function _checkTracking2(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$tracking_id = $base->get('tracking_id');
		$status = $base->get('status');

		$sql = "SELECT * FROM web_tracking WHERE tracking_id=".GF::quote($tracking_id);
 		$res = $db->Execute($sql);

 		$arrReturn = array();
 		$arrReturn['tracking_id'] = $res->fields['tracking_id'];
 		$arrReturn['status'] = $res->fields['status'];
		return $arrReturn;
	}
	private function _updateTracking(){
		$base = Base::getInstance();
		$db = DB::getInstance();
		$db2 = DB2::getInstance();
		$list = $base->get('POST');

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		if($list['tracking_id']!=''){
			//echo $list['tracking_id'];
			$base->set('tracking_id',$list['tracking_id']);
			$base->set('status',$list['status']);
			$rss = $this->_checkTracking2();
			$price_rtue = $list['price_weight'];

			if($list['price_weight']<=$list['price_dimen']){
				$price_rtue=$list['price_dimen'];
			}


			$sql = "UPDATE web_tracking SET
										tracking=".GF::quote($list['tracking']).",
										member_code=".GF::quote($list['member_code']).",
										saller_name=".GF::quote($list['saller_name']).",
										order_id=".GF::quote($list['order_id']).",
										order_type=".GF::quote($list['order_type']).",
										weight=".GF::quote($list['weight']).",
                              			weight_cn=".GF::quote($list['weight_cn']).",
										dime_x=".GF::quote($list['dime_x']).",
										dime_y=".GF::quote($list['dime_y']).",
										dime_w=".GF::quote($list['dime_w']).",
										cbm=".GF::quote($list['cbm']).",
										other_desc=".GF::quote($list['other_desc']).",
										other_price=".GF::quote($list['other_price']).",
										price_weight=".GF::quote($list['price_weight']).",
										price_dimen=".GF::quote($list['price_dimen']).",
										price_true=".GF::quote($price_rtue).",
										status=".GF::quote($list['status']).",
                              			update_dtm=NOW(),
                              			update_by=".GF::quote($user_id)."
										WHERE tracking_id=".GF::quote($list['tracking_id']);

			$res = $db->Execute($sql);
			//if($rss['status']!=$list['status']){
				$sql = "INSERT INTO web_tracking_history(
											tracking_id,
											status,
											create_dtm,
											create_by
											)
											VALUES(
											".GF::quote($rss['tracking_id']).",
											".GF::quote($list['status']).",
											NOW(),
											".GF::quote($user_id)."
											)";

				$res = $db->Execute($sql);
            //$orderid = $this->_OrderNumberToorderID($list['order_id']);
				$base->set('order_id',$list['order_id']);
				$orderinfo = $this->_orderInfo();
            //GF::print_r($orderinfo);

                $sql = "UPDATE web_order_item SET
                                 status=".GF::quote($list['status']).",
                                 product_sku_1=".GF::quote($list['tracking']).",
                                 update_dtm=NOW()
                                 WHERE order_id=".GF::quote($orderinfo['id'])." AND saller_number=".GF::quote($list['saller_name'])." AND status!='D' AND status!='14' ";

                $res = $db->Execute($sql);
				// if($orderinfo['product_link']!=''){
				//
				// }else{
				// 	$sql = "UPDATE web_order_item SET
				// 								status=".GF::quote($list['status']).",
				// 								update_dtm=NOW()
				// 								WHERE product_sku_1=".GF::quote($list['tracking'])." AND status!='D' AND status!='14' ";
            //
				// 	$res = $db->Execute($sql);
				// }


				//return true;
			    //}
			    $dataSet = $db2->select("web_tracking",array(
			  										    "tracking(track_no)",
														"member_code(track_user_code)",
														"order_type(track_goods_code)",
														"status(track_status)",
														"order_comment(track_remark)",
														"order_id(track_order)",
														"saller_name(track_shop)",
														"weight(track_weight)",
														"dime_w(track_width)",
														"dime_x(track_long)",
														"dime_y(track_hight)",
														"cbm(track_cbm)",
														"update_dtm(track_upddate)",
														"create_dtm(track_createdate)"
			  											),array(
			  											"tracking"=>$list['tracking']
			  											));
			  if(count($dataSet)>0){
			  	foreach($dataSet as $key=>$valdata){
					$dataSet[$key]['track_goods_code'] = GF::convgoodscode($valdata['track_goods_code']);
					$dataSet[$key]['track_timp_code'] = 'Car';
				}
				$db2->insert("project_webservice",array(
														"service_token"=>GF::randomStr(20),
														"service_method"=>"UPDATE_TRACK",
														"service_data"=>json_encode($dataSet),
														"status"=>'N',
														"create_dtm"=>date('Y-m-d H:i:s'),
														"update_dtm"=>date('Y-m-d H:i:s')
														));
			  }
			return true;

		}else{
         $price_rtue = $list['price_weight'];
			if($list['price_weight']<=$list['price_dimen']){
				$price_rtue=$list['price_dimen'];
			}
			$sql = "INSERT INTO web_tracking(
													tracking,
													member_code,
													saller_name,
													order_id,
													order_type,
													status,
													weight,
                                                    weight_cn,
													dime_x,
													dime_y,
													dime_w,
													cbm,
													other_desc,
													other_price,
													price_weight,
													price_dimen,
													price_true,
													create_dtm,
													create_by,
                                                    update_dtm,
													update_by
													)
													VALUES(
													".GF::quote($list['tracking']).",
													".GF::quote($list['member_code']).",
													".GF::quote($list['saller_name']).",
													".GF::quote($list['order_id']).",
													".GF::quote($list['order_type']).",
													".GF::quote($list['status']).",
													".GF::quote($list['weight']).",
                                                    ".GF::quote($list['weight_cn']).",
													".GF::quote($list['dime_x']).",
													".GF::quote($list['dime_y']).",
													".GF::quote($list['dime_w']).",
													".GF::quote($list['cbm']).",
													".GF::quote($list['other_desc']).",
													".GF::quote($list['other_price']).",
													".GF::quote($list['price_weight']).",
													".GF::quote($list['price_dimen']).",
													".GF::quote($price_rtue).",
													NOW(),
													".GF::quote($user_id).",
                                                     NOW(),
													".GF::quote($user_id)."
													)";
					//echo $sql;
					$res = $db->Execute($sql);
					$last_id = $db->Insert_ID();
					$sql = "INSERT INTO web_tracking_history(
													tracking_id,
													status,
													create_dtm,
													create_by
													)
													VALUES(
													".GF::quote($last_id).",
													".GF::quote($list['status']).",
													NOW(),
													".GF::quote($user_id)."
													)";
					$res = $db->Execute($sql);
               //$orderid = $this->_OrderNumberToorderID($list['order_id']);
   				    $base->set('order_id',$list['order_id']);
					$orderinfo = $this->_orderInfo();

                    $sql = "UPDATE web_order_item SET
													status=".GF::quote($list['status']).",
                                       product_sku_1=".GF::quote($list['tracking']).",
													update_dtm=NOW()
													WHERE order_id=".GF::quote($orderinfo['id'])." AND saller_number=".GF::quote($list['saller_name'])." AND status!='D' AND status!='14' ";
						$res = $db->Execute($sql);

					// if($orderinfo['product_link']!=''){
					//
					// }else{
					// 	$sql = "UPDATE web_order_item SET
					// 								status=".GF::quote($list['status']).",
					// 								update_dtm=NOW()
					// 								WHERE product_sku_1=".GF::quote($list['tracking'])." AND status!='D' AND status!='14' ";
					// 	$res = $db->Execute($sql);
					// }
					
					  $dataSet = $db2->select("web_tracking",array(
					  										    "tracking(track_no)",
																"member_code(track_user_code)",
																"order_type(track_goods_code)",
																"status(track_status)",
																"order_comment(track_remark)",
																"order_id(track_order)",
																"saller_name(track_shop)",
																"weight(track_weight)",
																"dime_w(track_width)",
																"dime_x(track_long)",
																"dime_y(track_hight)",
																"cbm(track_cbm)",
																"update_dtm(track_upddate)",
																"create_dtm(track_createdate)"
					  											),array(
					  											"tracking"=>$list['tracking']
					  											));
					  if(count($dataSet)>0){
					  	foreach($dataSet as $key=>$valdata){
							$dataSet[$key]['track_goods_code'] = GF::convgoodscode($valdata['track_goods_code']);
							$dataSet[$key]['track_timp_code'] = 'Car';
						}
						$db2->insert("project_webservice",array(
																"service_token"=>GF::randomStr(20),
																"service_method"=>"ADD_TRACK",
																"service_data"=>json_encode($dataSet),
																"status"=>'N',
																"create_dtm"=>date('Y-m-d H:i:s'),
																"update_dtm"=>date('Y-m-d H:i:s')
																));
					  }


					return true;
		}
	}
	private function User(){
		$base = Base::getInstance();
		$db = DB::getInstance();
		$db2 = DB2::getInstance();

		$tracking_id = $base->get('_ids');
		
		$dataSet = $db2->select("web_tracking",array(
		  										    "tracking(track_no)",
													"member_code(track_user_code)",
													"order_type(track_goods_code)",
													"status(track_status)",
													"order_comment(track_remark)",
													"create_dtm(track_createdate)"
		  											),array(
		  											"tracking_id"=>$tracking_id
		  											));

		$sql = "DELETE FROM web_tracking WHERE tracking_id=".GF::quote($tracking_id);
 		$res = $db->Execute($sql);

 		$sql = "DELETE FROM web_tracking_history WHERE tracking_id=".GF::quote($tracking_id);
 		$res = $db->Execute($sql);
 		
 		
		  if(count($dataSet)>0){
		  	foreach($dataSet as $key=>$valdata){
				$dataSet[$key]['track_goods_code'] = GF::convgoodscode($valdata['track_goods_code']);
				$dataSet[$key]['track_timp_code'] = 'Car';
			}
			$db2->insert("project_webservice",array(
													"service_token"=>GF::randomStr(20),
													"service_method"=>"DEL_TRACK",
													"service_data"=>json_encode($dataSet),
													"status"=>'N',
													"create_dtm"=>date('Y-m-d H:i:s'),
													"update_dtm"=>date('Y-m-d H:i:s')
													));
		  }

		return true;
	}

	private function _updateTrackingGroup(){
		$base = Base::getInstance();
		$db = DB::getInstance();
		$db2 = DB2::getInstance();
		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$tracking_id = $base->get('POST.trackid');

		if(count($tracking_id)>0){

			$track_IN = implode(',',$tracking_id);

			$sql = "UPDATE web_tracking SET
										status='5',
                              update_dtm=NOW(),
                              update_by=".GF::quote($user_id)."
										WHERE tracking_id IN(".$track_IN.") ";

			$res = $db->Execute($sql);

			$sql = "SELECT * FROM web_tracking WHERE tracking_id IN(".$track_IN.") ";
			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i] = $res->fields['tracking'];
				$i++;
				$res->MoveNext();
			}
			$track_sku = implode(',',$ArrList);

			$sql = "UPDATE web_order_item SET
										status='5',
										update_dtm=NOW()
										WHERE product_sku_1 IN(".$track_sku.") AND status!='D' AND status!='14' ";
			$res = $db->Execute($sql);

			foreach($tracking_id as $vals){
					$sql = "INSERT INTO web_tracking_history(
													tracking_id,
													status,
													create_dtm,
													create_by
													)
													VALUES(
													".GF::quote($vals).",
													'5',
													NOW(),
													".GF::quote($user_id)."
													)";

					$res = $db->Execute($sql);
			}
			$dataSet = $db2->select("web_tracking",array(
		  										    	"tracking(track_no)",
														"member_code(track_user_code)",
														"order_type(track_goods_code)",
														"status(track_status)",
														"order_comment(track_remark)",
														"order_id(track_order)",
														"saller_name(track_shop)",
														"weight(track_weight)",
														"dime_w(track_width)",
														"dime_x(track_long)",
														"dime_y(track_hight)",
														"cbm(track_cbm)",
														"update_dtm(track_upddate)",
														"create_dtm(track_createdate)"
		  											),array(
		  											"tracking_id"=>$tracking_id
		  											));
			if(count($dataSet)>0){
			  	foreach($dataSet as $key=>$valdata){
					$dataSet[$key]['track_goods_code'] = GF::convgoodscode($valdata['track_goods_code']);
					$dataSet[$key]['track_timp_code'] = 'Car';
				}
				$db2->insert("project_webservice",array(
														"service_token"=>GF::randomStr(20),
														"service_method"=>"UPDATE_TRACK",
														"service_data"=>json_encode($dataSet),
														"status"=>'N',
														"create_dtm"=>date('Y-m-d H:i:s'),
														"update_dtm"=>date('Y-m-d H:i:s')
														));
		   }
		}

		return true;

	}
	private function _saveGetShipping(){
		$base = Base::getInstance();
		$db = DB::getInstance();
		$db2 = DB2::getInstance();
		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$tracking_id = $base->get('POST.trackid');
 		//GF::print_r($tracking_id);
 		if(count($tracking_id)>0){
 			//$track_IN = implode(',',$tracking_id);
 			$track_IN = '';
 			foreach($tracking_id as $dds){
				$track_IN .= "'".$dds."',";
			}
 			$track_IN = rtrim($track_IN,",");

 			$sql = "SELECT * FROM web_tracking WHERE tracking IN(".$track_IN.")";
 			//echo $sql;
 			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
				$i++;
				$res->MoveNext();
			}
			//$track_sku = implode(',',$ArrList);
			$sql = "INSERT INTO web_shipping(
											ship_type,
											pay_office,
											shipping_name,
											shipping_address,
											shipping_phone,
											status,
                                            status_last,
											create_dtm,
											create_by,
											update_dtm,
											update_by,
                                 inst_by,
                                 inst_dtm
											)
											VALUES(
											".GF::quote($base->get('POST.ship_by')).",
											".GF::quote($base->get('POST.payoffice')).",
											".GF::quote($base->get('POST.user_name')).",
											".GF::quote($base->get('POST.user_address')).",
											".GF::quote($base->get('POST.user_phone_number')).",
											'1',
                                            '1',
											NOW(),
											".GF::quote($user_id).",
											NOW(),
											".GF::quote($user_id).",
                                            ".GF::quote($user_id).",
                                            NOW()
											)";
			//echo $sql;
			$res = $db->Execute($sql);
			if($res){
				$last_id = $db->Insert_ID();
				$numbercode = GF::genbilling();//date('dmY').str_pad($last_id,4,"0",STR_PAD_LEFT);
				$sql = "UPDATE web_shipping SET shipping_code=".GF::quote($numbercode)." WHERE id=".GF::quote($last_id);
				$res = $db->Execute($sql);

				foreach($ArrList as $list){

					$sql = "SELECT * FROM web_tracking WHERE tracking=".GF::quote($list['tracking']);
		 			$res = $db->Execute($sql);

		 			$rateNow = GF::getrate();
		 			/*$otr_price = $res->fields['other_price'];
		 			$otr_price_baht = $otr_price*$rateNow;
		 			$price_true = $res->fields['price_true'];*/
		 			$otr_price = 0;
		 			$otr_price_baht = 0;
		 			$price_true = 0;


		 			foreach($res as $list2){
					$sql = "INSERT INTO web_shipping_item(
													shipping_id,
													tracking_id,
													tracking,
													ship_price,
													otr_price_yuan,
													otr_price_rate,
													otr_price_baht
													)
													VALUES(
													".GF::quote($last_id).",
													".GF::quote($list2['tracking_id']).",
													".GF::quote($list2['tracking']).",
													".GF::quote($price_true).",
													".GF::quote($otr_price).",
													".GF::quote($rateNow).",
													".GF::quote($otr_price_baht)."
													)";

					$res = $db->Execute($sql);
					}



					$sql = "UPDATE web_tracking SET flag='2',status='5',shipping_code=".$db->Quote($numbercode)." WHERE tracking_id=".GF::quote($list['tracking_id']);
					$res = $db->Execute($sql);
					
					$dataSet2 = $db2->select("web_tracking",array(
		  										    	"tracking(track_no)",
														"member_code(track_user_code)",
														"status(track_status)",
														"update_dtm(track_upddate)",
														"create_dtm(track_createdate)"
		  											),array(
		  											"tracking"=>$list['tracking']
		  											));
													
													
					if(count($dataSet2)>0){
						
						$db2->insert("project_webservice",array(
																"service_token"=>GF::randomStr(20),
																"service_method"=>"UPDATE_TRACK",
																"service_data"=>json_encode($dataSet2),
																"status"=>'N',
																"create_dtm"=>date('Y-m-d H:i:s'),
																"update_dtm"=>date('Y-m-d H:i:s')
																));
				   }
					
					
				}
				// $sql = "INSERT INTO web_shipping_order(
				// 									shipping_id,
				// 									order_type,
				// 									order_desc,
				// 									order_price,
				// 									create_dtm,
				// 									create_by
				// 									)
				// 									VALUES(
				// 									".GF::quote($last_id).",
				// 									'IN',
				// 									'ค่าจัดส่ง',
				// 									'0.00',
				// 									NOW(),
				// 									".GF::quote($user_id)."
				// 									)";

				// 	$res = $db->Execute($sql);
					// $sql = "INSERT INTO web_shipping_order(
					// 								shipping_id,
					// 								order_type,
					// 								order_desc,
					// 								order_price,
					// 								create_dtm,
					// 								create_by
					// 								)
					// 								VALUES(
					// 								".GF::quote($last_id).",
					// 								'IN',
					// 								'ค่าบรรจุภัณฑ์',
					// 								'0.00',
					// 								NOW(),
					// 								".GF::quote($user_id)."
					// 								)";

					// $res = $db->Execute($sql);
					// $sql = "INSERT INTO web_shipping_order(
					// 								shipping_id,
					// 								order_type,
					// 								order_desc,
					// 								order_price,
					// 								create_dtm,
					// 								create_by
					// 								)
					// 								VALUES(
					// 								".GF::quote($last_id).",
					// 								'IN',
					// 								'อื่น ๆ',
					// 								'0.00',
					// 								NOW(),
					// 								".GF::quote($user_id)."
					// 								)";

					// $res = $db->Execute($sql);
					// $sql = "INSERT INTO web_shipping_order(
					// 								shipping_id,
					// 								order_type,
					// 								order_desc,
					// 								order_price,
					// 								create_dtm,
					// 								create_by
					// 								)
					// 								VALUES(
					// 								".GF::quote($last_id).",
					// 								'DE',
					// 								'ส่วนลด',
					// 								'0.00',
					// 								NOW(),
					// 								".GF::quote($user_id)."
					// 								)";

					// $res = $db->Execute($sql);
					
					$dataSet = $db2->get("web_shipping",array(
					  										    "shipping_code(billing_no)",
																"create_dtm(billing_date)",
																"shipping_name(billing_recipients)",
																"shipping_address(billing_address)",
																"create_dtm(billing_submit_date)",
																"shipping_phone(billing_tel)",
                                                                "status(billing_status)",
                                                                "ship_type(billing_send_code)"
					  											),array(
					  											"id"=>$last_id
					  											));
				   $dataSet['user_code'] = $memberInfomation['user_code'];
				   
				   $dataItem = $db2->select("web_shipping_item",array(
                                                                       "tracking(track_no)",
                                                                       "ship_price(track_price)",
                                                                       "track_rate(track_rate)"
				   													),array("shipping_id"=>$last_id));

				   $dataSetArr = array();
                //    if($dataSet['billing_status']=='1'){
                //     $dataSet['billing_status']='3';
                //    }
                //    else if($dataSet['billing_status']=='3'){
                //     $dataSet['billing_status']='5';
                //    }
                //    else if($dataSet['billing_status']=='2'){
                //     $dataSet['billing_status']='6';
                //    }
				   $dataSetArr['order'] =  $dataSet;
				   $dataSetArr['item'] = $dataItem;
				   $db2->insert("project_webservice",array(
															"service_token"=>GF::randomStr(20),
															"service_method"=>"ADD_BILL",
															"service_data"=>json_encode($dataSetArr),
															"status"=>'N',
															"create_dtm"=>date('Y-m-d H:i:s'),
															"update_dtm"=>date('Y-m-d H:i:s')
                                                        ));

                    
                    $members = GF::memberinfo($user_id);
                    $link = $base->get('BASEURL').'/#/tracking/viewshipping/'.$last_id;
                    $dataReplace = array(
                        "{CUST_NAME}"=>$memberInfomation['user_name'],
                        "{LINK}"=>GF::shorturl($link,'1'),
                        "{ORDER_NO}"=>$numbercode,
                        "{ORDER_STATUS}"=>GF::shippingstatus($dataSet['billing_status'])
                    );
                    $mail = Mailer::getInstance();
                    $mail->Subject = GF::mailsubject('6',$members['ref_id']);
                    $mail->msgHTML = GF::mailtemplate('6',$dataReplace,$members['ref_id']);
                    $mail->addAddressEmail = $members['user_email'];
                    $mail->addAddressName = $members['user_name'];
                    $mail->sendMail();
			}



 		}
 		return TRUE;

	}

   private function _saveGetShippingByAdmin(){
		$base = Base::getInstance();
		$db = DB::getInstance();
		$db2 = DB2::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id_admin = $memberInfomation['id'];

      $base->set('user_id',$_SESSION['track_search']['user_code']);
 		$memberInfomation = $member->memberInfomationByID();

      $user_id = $memberInfomation['id'];

 		$tracking_id = $base->get('POST.trackid');
 		//GF::print_r($tracking_id);
 		if(count($tracking_id)>0){
 			//$track_IN = implode(',',$tracking_id);
 			$track_IN = '';
 			foreach($tracking_id as $dds){
				$track_IN .= "'".$dds."',";
			}
 			$track_IN = rtrim($track_IN,",");

 			$sql = "SELECT * FROM web_tracking WHERE tracking IN(".$track_IN.")";
 			//echo $sql;
 			$res = $db->Execute($sql);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i]['tracking_id'] = $res->fields['tracking_id'];
				$ArrList[$i]['tracking'] = $res->fields['tracking'];
				$i++;
				$res->MoveNext();
			}
			//$track_sku = implode(',',$ArrList);
			$sql = "INSERT INTO web_shipping(
											ship_type,
											pay_office,
											shipping_name,
											shipping_address,
											shipping_phone,
											status,
                                            status_last,
											create_dtm,
											create_by,
											update_dtm,
											update_by,
                                 inst_by,
                                 inst_dtm
											)
											VALUES(
											".GF::quote($base->get('POST.ship_by')).",
											".GF::quote($base->get('POST.payoffice')).",
											".GF::quote($base->get('POST.user_name')).",
											".GF::quote($base->get('POST.user_address')).",
											".GF::quote($base->get('POST.user_phone_number')).",
											'1',
                                            '1',
											NOW(),
											".GF::quote($user_id).",
											NOW(),
											".GF::quote($user_id_admin).",
                                            ".GF::quote($user_id).",
                                            NOW()
											)";
			//echo $sql;
			$res = $db->Execute($sql);
			if($res){
				$last_id = $db->Insert_ID();
				$numbercode = GF::genbilling();//date('dmY').str_pad($last_id,4,"0",STR_PAD_LEFT);
				$sql = "UPDATE web_shipping SET shipping_code=".GF::quote($numbercode)." WHERE id=".GF::quote($last_id);
				$res = $db->Execute($sql);

				foreach($ArrList as $list){

					$sql = "SELECT * FROM web_tracking WHERE tracking=".GF::quote($list['tracking']);
		 			$res = $db->Execute($sql);

		 			$rateNow = GF::getrate();
		 			/*$otr_price = $res->fields['other_price'];
		 			$otr_price_baht = $otr_price*$rateNow;
		 			$price_true = $res->fields['price_true'];*/
		 			$otr_price = 0;
		 			$otr_price_baht = 0;
		 			$price_true = 0;

		 			foreach($res as $list2){
					$sql = "INSERT INTO web_shipping_item(
													shipping_id,
													tracking_id,
													tracking,
													ship_price,
													otr_price_yuan,
													otr_price_rate,
													otr_price_baht
													)
													VALUES(
													".GF::quote($last_id).",
													".GF::quote($list2['tracking_id']).",
													".GF::quote($list2['tracking']).",
													".GF::quote($price_true).",
													".GF::quote($otr_price).",
													".GF::quote($rateNow).",
													".GF::quote($otr_price_baht)."
													)";

					$res = $db->Execute($sql);
				    }



					$sql = "UPDATE web_tracking SET flag='2',status='5',shipping_code=".$db->Quote($numbercode)." WHERE tracking_id=".GF::quote($list['tracking_id']);
					$res = $db->Execute($sql);
				}
				// $sql = "INSERT INTO web_shipping_order(
				// 									shipping_id,
				// 									order_type,
				// 									order_desc,
				// 									order_price,
				// 									create_dtm,
				// 									create_by
				// 									)
				// 									VALUES(
				// 									".GF::quote($last_id).",
				// 									'IN',
				// 									'ค่าจัดส่ง',
				// 									'0.00',
				// 									NOW(),
				// 									".GF::quote($user_id)."
				// 									)";

				// 	$res = $db->Execute($sql);
					// $sql = "INSERT INTO web_shipping_order(
					// 								shipping_id,
					// 								order_type,
					// 								order_desc,
					// 								order_price,
					// 								create_dtm,
					// 								create_by
					// 								)
					// 								VALUES(
					// 								".GF::quote($last_id).",
					// 								'IN',
					// 								'ค่าบรรจุภัณฑ์',
					// 								'0.00',
					// 								NOW(),
					// 								".GF::quote($user_id)."
					// 								)";

					// $res = $db->Execute($sql);
					// $sql = "INSERT INTO web_shipping_order(
					// 								shipping_id,
					// 								order_type,
					// 								order_desc,
					// 								order_price,
					// 								create_dtm,
					// 								create_by
					// 								)
					// 								VALUES(
					// 								".GF::quote($last_id).",
					// 								'IN',
					// 								'อื่น ๆ',
					// 								'0.00',
					// 								NOW(),
					// 								".GF::quote($user_id)."
					// 								)";

					// $res = $db->Execute($sql);
					// $sql = "INSERT INTO web_shipping_order(
					// 								shipping_id,
					// 								order_type,
					// 								order_desc,
					// 								order_price,
					// 								create_dtm,
					// 								create_by
					// 								)
					// 								VALUES(
					// 								".GF::quote($last_id).",
					// 								'DE',
					// 								'ส่วนลด',
					// 								'0.00',
					// 								NOW(),
					// 								".GF::quote($user_id)."
					// 								)";

					// $res = $db->Execute($sql);
					
					$dataSet = $db2->get("web_shipping",array(
					  										    "shipping_code(billing_no)",
																"create_dtm(billing_date)",
																"shipping_name(billing_recipients)",
																"shipping_address(billing_address)",
																"create_dtm(billing_submit_date)",
																"shipping_phone(billing_tel)",
                                                                "status(billing_status)",
                                                                "ship_type(billing_send_code)"
					  											),array(
					  											"id"=>$last_id
					  											));
				   $dataSet['user_code'] = $memberInfomation['user_code'];
				   
				   $dataItem = $db2->select("web_shipping_item",array(
                                                                       "tracking(track_no)",
                                                                       "ship_price(track_price)",
                                                                       "track_rate(track_rate)"
				   													),array("shipping_id"=>$last_id));

                //    if($dataSet['billing_status']=='1'){
                //     $dataSet['billing_status']='3';
                //    }
                //    else if($dataSet['billing_status']=='3'){
                //     $dataSet['billing_status']='5';
                //    }
                //    else if($dataSet['billing_status']=='2'){
                //     $dataSet['billing_status']='6';
                //    }                                                    
				   $dataSetArr['order'] =  $dataSet;
				   $dataSetArr['item'] = $dataItem;
				   $db2->insert("project_webservice",array(
															"service_token"=>GF::randomStr(20),
															"service_method"=>"ADD_BILL",
															"service_data"=>json_encode($dataSetArr),
															"status"=>'N',
															"create_dtm"=>date('Y-m-d H:i:s'),
															"update_dtm"=>date('Y-m-d H:i:s')
                                                            ));
                    $members = GF::memberinfo($user_id);
                    $link = $base->get('BASEURL').'/#/tracking/viewshipping/'.$last_id;
                    $dataReplace = array(
                        "{CUST_NAME}"=>$members['user_name'],
                        "{LINK}"=>GF::shorturl($link,'1'),
                        "{ORDER_NO}"=>$numbercode,
                        "{ORDER_STATUS}"=>GF::shippingstatus($dataSet['billing_status'])
                    );
                    $mail = Mailer::getInstance();
                    $mail->Subject = GF::mailsubject('6',$members['ref_id']);
                    $mail->msgHTML = GF::mailtemplate('6',$dataReplace,$members['ref_id']);
                    $mail->addAddressEmail = $members['user_email'];
                    $mail->addAddressName = $members['user_name'];
                    $mail->sendMail();
			}



 		}
 		return TRUE;

	}
	private function _shippingInfo(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$shipping_id = $base->get('_ids');

		$sql = "SELECT * FROM web_shipping WHERE id=".GF::quote($shipping_id);
 		$res = $db->Execute($sql);

 		$arrReturn = array();
 		$arrReturn['id'] = $res->fields['id'];

 		$arrReturn['order_id'] = $res->fields['shipping_code'];
		

 	    /*echo $sql2 = "SELECT project_user.ref_id FROM web_order 
 				LEFT JOIN  project_user ON(project_user.id=web_order.member_id)
 				WHERE order_id=".GF::quote($res->fields['shipping_code']);*/
				
		$sql2= "SELECT * FROM project_user WHERE id=".GF::quote($res->fields['create_by']);		
				
 		$res2 = $db->Execute($sql2);

 		//echo $res2->fields['ref_id'];
 		if($res2->fields['ref_id']>0){
			$sql3 = "
					SELECT project_user.*,
						project_thai_province.PROVINCE_NAME ,
						project_thai_amphur.AMPHUR_NAME ,
						project_thai_district.DISTRICT_NAME 
						
					FROM  project_user 
					LEFT JOIN  project_thai_province ON(project_thai_province.PROVINCE_ID=project_user.province)
					LEFT JOIN  project_thai_amphur ON(project_thai_amphur.AMPHUR_ID=project_user.amphur)
					LEFT JOIN  project_thai_district ON(project_thai_district.DISTRICT_ID=project_user.district)
					WHERE project_user.id=".GF::quote($res2->fields['ref_id']);
 			$res3 = $db->Execute($sql3);
 			$arrReturn['user_name'] = $res3->fields['user_name'];
 			$arrReturn['user_address'] = $res3->fields['user_address'];
 			
 			$arrReturn['DISTRICT_NAME'] = $res3->fields['DISTRICT_NAME'];
 			$arrReturn['AMPHUR_NAME'] = $res3->fields['AMPHUR_NAME'];
 			$arrReturn['PROVINCE_NAME'] = $res3->fields['PROVINCE_NAME'];
 			$arrReturn['user_post'] = $res3->fields['user_post'];
 			$arrReturn['user_thumb'] = $res3->fields['user_thumb'];
 		}else{
			$sql3 = "
					SELECT project_user.*,
						project_thai_province.PROVINCE_NAME ,
						project_thai_amphur.AMPHUR_NAME ,
						project_thai_district.DISTRICT_NAME 
						
					FROM  project_user 
					LEFT JOIN  project_thai_province ON(project_thai_province.PROVINCE_ID=project_user.province)
					LEFT JOIN  project_thai_amphur ON(project_thai_amphur.AMPHUR_ID=project_user.amphur)
					LEFT JOIN  project_thai_district ON(project_thai_district.DISTRICT_ID=project_user.district)
					WHERE project_user.id=".GF::quote(1);
 			$res3 = $db->Execute($sql3);
 			$arrReturn['user_name'] = 'Taobao China Cargo';
 			$arrReturn['user_address'] = $res3->fields['user_address'];
 			
 			$arrReturn['DISTRICT_NAME'] = $res3->fields['DISTRICT_NAME'];
 			$arrReturn['AMPHUR_NAME'] = $res3->fields['AMPHUR_NAME'];
 			$arrReturn['PROVINCE_NAME'] = $res3->fields['PROVINCE_NAME'];
 			$arrReturn['user_post'] = $res3->fields['user_post'];
 			$arrReturn['user_thumb'] = $res3->fields['user_thumb'];
		}
 		
 		$arrReturn['ship_type'] = $res->fields['ship_type'];
 		$arrReturn['pay_office'] = $res->fields['pay_office'];
 		$arrReturn['shipping_name'] = $res->fields['shipping_name'];
 		$arrReturn['shipping_address'] = $res->fields['shipping_address'];
 		$arrReturn['shipping_phone'] = $res->fields['shipping_phone'];
 		$arrReturn['status'] = $res->fields['status'];
 		$arrReturn['track_code'] = $res->fields['track_code'];
 		$arrReturn['update_dtm'] = $res->fields['update_dtm'];
 		$arrReturn['update_by'] = $res->fields['update_by'];
      	$arrReturn['create_by'] = $res->fields['create_by'];
      	$arrReturn['create_dtm'] = $res->fields['create_dtm'];
		$arrReturn['shipping_code'] = $res->fields['shipping_code'];
		$arrReturn['remark'] = $res->fields['remark'];
		
		$sqlS = "SELECT track_rate FROM web_shipping_item WHERE shipping_id=".GF::quote($shipping_id);
		$resS = $db->Execute($sqlS);
       $arrReturn['track_rate'] = $resS->fields['track_rate'];
        $arrReturn['shipping_th_price'] = $res->fields['shipping_th_price'];
        $arrReturn['shipping_th_service_price'] = $res->fields['shipping_th_service_price'];
        $arrReturn['shipping_other_price'] = $res->fields['shipping_other_price'];
 		$arrReturn['tracking'] = $this->_shippingTrackList($res->fields['id']);
 		$arrReturn['ordering'] = $this->_shippingTrackOrder($res->fields['id']);
		$arrReturn['tracking_status'] = $this->_getTrackingStatus($res->fields['shipping_code']);
 		//GF::print_r($arrReturn);
		return $arrReturn;
	}
	private function _shippingTrackList($ship_id){

		$base = Base::getInstance();
		$db = DB::getInstance();
		
		$SQL = "SELECT * FROM web_shipping WHERE id = ".GF::quote($ship_id)." ";
		$res2 = $db->Execute($SQL);

		$SQL3 = "SELECT * FROM project_user WHERE id = ".GF::quote($res2->fields['create_by'])." ";
		$res3 = $db->Execute($SQL3);
		
		$sql = "SELECT * FROM web_shipping_item WHERE shipping_id=".GF::quote($ship_id)." ";
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['shipping_id'] = $res->fields['shipping_id'];
			
			$ArrList[$i]['dimen_x'] = $res->fields['dimen_x'];
            $ArrList[$i]['dimen_y'] = $res->fields['dimen_y'];
            $ArrList[$i]['dimen_w'] = $res->fields['dimen_w'];
			
            $base->set('_ids',$res->fields['tracking_id']);
            $ArrList[$i]['tracking_info'] = $this->trackingInfoUpweight();
            $ArrList[$i]['tracking'] = $res->fields['tracking'];
            $ArrList[$i]['track_rate'] = $res->fields['track_rate'];
			//$ArrList[$i]['tracking'] = $res->fields['tracking'];
			
			// คำนวณใหม่
			if($ArrList[$i]['tracking_info']['weight']<=0.5 && $ArrList[$i]['tracking_info']['weight']>=0.1){
				//Comment By Weerasak 04/04/2565
				//$dataRate = "SELECT * FROM project_user_rate WHERE unit_code=".GF::quote($ArrList[$i]['tracking_info']['price_rate'])." AND order_type=".GF::quote($ArrList[$i]['tracking_info']['order_type_t'])." AND user_id =".GF::quote($res3->fields['user_role_id'])." ";
				
				//Edit By Weerasak 04/04/2565
				$dataRate = "SELECT * FROM project_user_rate WHERE user_id = ".GF::quote($res3->fields['user_role_id'])." AND order_type =".GF::quote($ArrList[$i]['tracking_info']['order_type_t'])." AND ship_by = ".GF::quote($ArrList[$i]['tracking_info']['ship_by'])." ";
				
				$resDataRate = $db->Execute($dataRate);				
				//GF::print_r($resDataRate);
				foreach($resDataRate as $valsRate){
					
                    $SQLmasterCK = "SELECT * FROM project_master WHERE status='O' AND active_status='O' AND id = ".GF::quote($valsRate['unit']);

					$masterCK = $db->Execute($SQLmasterCK);

					//GF::print_r($masterCK->fields);

					$cbm =  ($ArrList[$i]['tracking_info']['dime_x']*$ArrList[$i]['tracking_info']['dime_y']*$ArrList[$i]['tracking_info']['dime_w'])/1000000;

					//Edit By Weerasak 05/04/2565
					$track_ship_type_code = substr($res->fields['track_rate'],0,1);

					if($track_ship_type_code == 'K'){
						if($masterCK->fields['master_char']=='CALK'){
							$price_weight = $valsRate['rate']*$ArrList[$i]['tracking_info']['weight'];
							if($price_true<$price_weight){
								$price_true = $price_weight;
								$price_rate = $valsRate['unit_code'];
							}
							$ArrList[$i]['ship_price'] = $price_weight;
						}
					} else if($track_ship_type_code == 'Q'){
						if($masterCK->fields['master_char']=='CALQ'){
							$cbm =  ($ArrList[$i]['tracking_info']['dime_x']*$ArrList[$i]['tracking_info']['dime_y']*$ArrList[$i]['tracking_info']['dime_w'])/1000000;
							$price_dimen = $cbm*$valsRate['rate'];
							if($price_true<$price_dimen){
								$price_true = $price_dimen;
								$price_rate = $valsRate['unit_code'];
							}
							$ArrList[$i]['ship_price'] = $res->fields['ship_price'];
						}
					}
					//END EDIT
					
				//echo "<br>".$ch_CAL = 150*$cbm;
				/*	Comment By Weerasak 05/04/2565
				if($res->fields['track_rate']=="K"){	
				
                   if($masterCK->fields['master_char']=='CALK'){
                        $price_weight = $valsRate['rate']*$ArrList[$i]['tracking_info']['weight'];
                        if($price_true<$price_weight){
                            $price_true = $price_weight;
                            $price_rate = $valsRate['unit_code'];
                        }


                    }

                    $ArrList[$i]['ship_price'] = $price_weight;
                    
                 }else if($res->fields['track_rate']=="KB"){
                 	
                 	 if($masterCK->fields['master_char']=='CALK'){
                 	$price_weight = $valsRate['rate']*$ArrList[$i]['tracking_info']['weight'];
                        if($price_true<$price_weight){
                            $price_true = $price_weight;
                            $price_rate = $valsRate['unit_code'];
                        }

                 	
                 	}

                 	$ArrList[$i]['ship_price'] = $price_weight;

                  }else if($res->fields['track_rate']=="KM"){	

                  	if($masterCK->fields['master_char']=='CALK'){
                 	$price_weight = $valsRate['rate']*$ArrList[$i]['tracking_info']['weight'];
                        if($price_true<$price_weight){
                            $price_true = $price_weight;
                            $price_rate = $valsRate['unit_code'];
                        }

                 	
                 	}

                 	$ArrList[$i]['ship_price'] = $price_weight;


                 }else if($res->fields['track_rate']=="QM"){

                 	if($masterCK->fields['master_char']=='CALQ'){
                        $cbm =  ($ArrList[$i]['tracking_info']['dime_x']*$ArrList[$i]['tracking_info']['dime_y']*$ArrList[$i]['tracking_info']['dime_w'])/1000000;
                        $price_dimen = $cbm*$valsRate['rate'];
                        if($price_true<$price_dimen){
                            $price_true = $price_dimen;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }

                    $ArrList[$i]['ship_price'] = $res->fields['ship_price'];	

                 }else if($res->fields['track_rate']=="QB"){

                 	if($masterCK->fields['master_char']=='CALQ'){
                        $cbm =  ($ArrList[$i]['tracking_info']['dime_x']*$ArrList[$i]['tracking_info']['dime_y']*$ArrList[$i]['tracking_info']['dime_w'])/1000000;
                        $price_dimen = $cbm*$valsRate['rate'];
                        if($price_true<$price_dimen){
                            $price_true = $price_dimen;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }

                    $ArrList[$i]['ship_price'] = $res->fields['ship_price'];


                 }else if($res->fields['track_rate']=="Q"){

                    if($masterCK->fields['master_char']=='CALQ'){
                        $cbm =  ($ArrList[$i]['tracking_info']['dime_x']*$ArrList[$i]['tracking_info']['dime_y']*$ArrList[$i]['tracking_info']['dime_w'])/1000000;
                        $price_dimen = $cbm*$valsRate['rate'];
                        if($price_true<$price_dimen){
                            $price_true = $price_dimen;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }

                    $ArrList[$i]['ship_price'] = $res->fields['ship_price'];
                 }
				 
				*/ //END Comment
					
                }
                        
                        
				
				//$ArrList[$i]['ship_price'] = $price_weight;

			}else{
			
				$ArrList[$i]['ship_price'] = $res->fields['ship_price'];
			}
			
			//$ArrList[$i]['ship_price'] = $res->fields['ship_price'];
			$ArrList[$i]['otr_price_baht'] = $res->fields['otr_price_baht'];

			$i++;
			$res->MoveNext();
		}

		return $ArrList;
	}
	
	
	private function _shippingTrackOrder($ship_id){

		$base = Base::getInstance();
		$db = DB::getInstance();


		$sql = "SELECT * FROM web_shipping_order WHERE shipping_id=".GF::quote($ship_id)." ";
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['order_id'] = $res->fields['order_id'];
			$ArrList[$i]['order_type'] = $res->fields['order_type'];
			$ArrList[$i]['order_desc'] = $res->fields['order_desc'];
			$ArrList[$i]['order_price'] = $res->fields['order_price'];
			$i++;
			$res->MoveNext();
		}

		return $ArrList;
	}
	private function _shippingListByuser(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
		$user_id = $memberInfomation['id'];
		$sql = "SELECT * FROM web_shipping WHERE create_by=".GF::quote($user_id)." ORDER BY create_dtm DESC";
		//echo $sql;
		$res = $db->Execute($sql);
		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
            $ArrList[$i]['ship_type'] = $res->fields['ship_type'];
			$ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
			$stt = GF::masterdataset($res->fields['ship_type'],'master_value','master_name');
			$ArrList[$i]['ship_by'] = $stt;
			$ArrList[$i]['pay_office'] = $res->fields['pay_office'];
			$ArrList[$i]['track_code'] = $res->fields['track_code'];
			$ArrList[$i]['status'] = $res->fields['status'];
            $ArrList[$i]['shipping_th_price'] = $res->fields['shipping_th_price'];
            $ArrList[$i]['shipping_th_service_price'] = $res->fields['shipping_th_service_price'];
            $ArrList[$i]['shipping_other_price'] = $res->fields['shipping_other_price'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
			$ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
			$ArrList[$i]['update_by'] = $res->fields['update_by'];
			$ArrList[$i]['ship_pay'] = $res->fields['ship_pay'];
            $ArrList[$i]['ship_pay_dtm'] = $res->fields['ship_pay_dtm'];
			$ArrList[$i]['total'] = $this->_calculateShippingprice($res->fields['id']);

            $other_price = $res->fields['shipping_th_price']+$res->fields['shipping_th_service_price']+$res->fields['shipping_other_price'];

            $ArrList[$i]['total'] = number_format($ArrList[$i]['total']+$other_price,2);

            $ArrList[$i]['tt_status'] = $this->_calShippingStatus($res->fields['id']);
            $this->ship_ids = $res->fields['id'];
            $ArrList[$i]['pay_date'] = $this->_getPaymentDateShip();

			$ArrList[$i]['tracking_status'] = $this->_getTrackingStatus($res->fields['shipping_code']);

			$i++;
			$res->MoveNext();
		}

		return $ArrList;
	}
	private function _getTrackingStatus($shipping_code){
		$base = Base::getInstance();
		$db = DB2::getInstance();	
		//$result = $db->min("web_tracking","status",array("shipping_code"=>$shipping_code));
		$result = $db->count("web_tracking",array(
			"AND"=>array(
				"shipping_code"=>$shipping_code,
				"cbm[>]" => 0,
				"weight[>]" => 0
			)));
		if($result > 0)
			return "6";
		else
			return "5";
	}
    private function _getPaymentDateShip(){
		$base = Base::getInstance();
		$db = DB2::getInstance();

        $shipping_id = $this->ship_ids;
        $result = $db->get("web_payment",array(
                                            "payment_date",
                                            "payment_time"
                                        ),array(
                                            "AND"=>array(
                                                "status"=>'A',
                                                "order_type"=>'2',
                                                "order_id"=>$shipping_id
                                            )
                                        ));
        return $result;

    }
	private function _shippingListAll(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
		$user_id = $memberInfomation['id'];
        $cond = '';
        $agency =  $memberInfomation['agency'];
        if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
                //GF::print_r($list_ref);
                $ref_str = implode(',', $list_ref);
                $cond .= " create_by IN(".$ref_str.") ";
            }else{
                $cond .= " create_by ='X' ";
            }
        }else{
            $cond = '1=1';
        }
        
        if($_SESSION['ship_search']['shipping_code']!=''){

   			$cond .= " AND shipping_code LIKE '%".$_SESSION['ship_search']['shipping_code']."%' ";
   		}

        if($_SESSION['ship_search']['user_code']!=''){
            $data_idd = GF::user_id($_SESSION['ship_search']['user_code']);
   			$cond .= " AND create_by =".GF::quote($data_idd)." ";
   		}

        if($_SESSION['ship_search']['ship_by']!=''){
   			$cond .= " AND ship_type =".GF::quote($_SESSION['ship_search']['ship_by'])." ";
   		}
        if($_SESSION['ship_search']['track_code']!=''){
   			$cond .= " AND track_code =".GF::quote($_SESSION['ship_search']['track_code'])." ";
   		}
        if($_SESSION['ship_search']['status']!=''){
   			$cond .= " AND status =".GF::quote($_SESSION['ship_search']['status'])." ";
   		}


		$sql = "SELECT * FROM web_shipping WHERE ".$cond." ORDER BY create_dtm DESC";
        
        $res = $db->Execute($sql);
   		$count = $res->NumRows();

   			$base->set('allPage',ceil($count/20));

   		$page = $base->get('GET.page');

   		$numr = 20;
   		$start = "";
   		$end = "";
   		if($page==1||empty($page)){
   			$start = 0;
   			$page = 1;
   			//$end = $numr;
   		}else{
   			//$end = ($page*$numr);
   			$start = ($page*$numr)-$numr;
   		}
   		 $condNum = " LIMIT ".$start.",".$numr;

   		 $base->set('currpage',$page);

   		 $res = $db->Execute($sql.$condNum);




		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
            $ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
            $ArrList[$i]['ship_type'] = $res->fields['ship_type'];
			$stt = GF::masterdataset($res->fields['ship_type'],'master_value','master_name');
			$ArrList[$i]['ship_by'] = $stt;
			$ArrList[$i]['pay_office'] = $res->fields['pay_office'];
			$ArrList[$i]['track_code'] = $res->fields['track_code'];
			$ArrList[$i]['status'] = $res->fields['status'];
            $ArrList[$i]['shipping_th_price'] = $res->fields['shipping_th_price'];
            $ArrList[$i]['shipping_th_service_price'] = $res->fields['shipping_th_service_price'];
            $ArrList[$i]['shipping_other_price'] = $res->fields['shipping_other_price'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
			$ArrList[$i]['update_by'] = $res->fields['update_by'];
            $ArrList[$i]['create_by'] = $res->fields['create_by'];
			$ArrList[$i]['ship_pay'] = $res->fields['ship_pay'];
            $ArrList[$i]['ship_pay_dtm'] = $res->fields['ship_pay_dtm'];
            
			$ArrList[$i]['total'] = $this->_calculateShippingprice($res->fields['id']);
            $other_price = $res->fields['shipping_th_price']+$res->fields['shipping_th_service_price']+$res->fields['shipping_other_price'];
            $ArrList[$i]['total'] = $ArrList[$i]['total']+$other_price;
            $ArrList[$i]['tt_status'] = $this->_calShippingStatus($res->fields['id']);
			$i++;
			$res->MoveNext();
		}

		return $ArrList;
	}
    private function _shippingListForTh(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
		$user_id = $memberInfomation['id'];
        $cond = '';
        $agency =  $memberInfomation['agency'];
        if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
                //GF::print_r($list_ref);
                $ref_str = implode(',', $list_ref);
                $cond .= " create_by IN(".$ref_str.") ";
            }else{
                $cond .= " create_by ='X' ";
            }
        }else{
            $cond = '1=1';
        }
        
        if($_SESSION['ship_th_search']['shipping_code']!=''){

   			$cond .= " AND shipping_code LIKE '%".$_SESSION['ship_th_search']['shipping_code']."%' ";
   		}

        if($_SESSION['ship_th_search']['user_code']!=''){
            $data_idd = GF::user_id($_SESSION['ship_th_search']['user_code']);
   			$cond .= " AND create_by =".GF::quote($data_idd)." ";
   		}

        if($_SESSION['ship_th_search']['ship_by']!=''){
   			$cond .= " AND ship_type =".GF::quote($_SESSION['ship_th_search']['ship_by'])." ";
   		}
        if($_SESSION['ship_th_search']['track_code']!=''){
   			$cond .= " AND track_code =".GF::quote($_SESSION['ship_th_search']['track_code'])." ";
   		}


		$sql = "SELECT * FROM web_shipping WHERE ".$cond." AND shipping_th_flag='N' AND status='6' AND ship_type<>'06' ORDER BY create_dtm DESC";
        
        $res = $db->Execute($sql);
   		$count = $res->NumRows();

   			$base->set('allPage',ceil($count/20));

   		$page = $base->get('GET.page');

   		$numr = 20;
   		$start = "";
   		$end = "";
   		if($page==1||empty($page)){
   			$start = 0;
   			$page = 1;
   			//$end = $numr;
   		}else{
   			//$end = ($page*$numr);
   			$start = ($page*$numr)-$numr;
   		}
   		 $condNum = " LIMIT ".$start.",".$numr;

   		 $base->set('currpage',$page);

   		 $res = $db->Execute($sql.$condNum);




		$i=0;
		$ArrList = array();
		while(!$res->EOF){
			$ArrList[$i]['id'] = $res->fields['id'];
			$ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
			$stt = GF::masterdataset($res->fields['ship_type'],'master_value','master_name');
			$ArrList[$i]['ship_by'] = $stt;
			$ArrList[$i]['pay_office'] = $res->fields['pay_office'];
			$ArrList[$i]['track_code'] = $res->fields['track_code'];
			$ArrList[$i]['status'] = $res->fields['status'];
            $ArrList[$i]['shipping_th_price'] = $res->fields['shipping_th_price'];
            $ArrList[$i]['shipping_th_price_true'] = $res->fields['shipping_th_price_true'];
            $ArrList[$i]['shipping_th_service_price'] = $res->fields['shipping_th_service_price'];
            $ArrList[$i]['shipping_other_price'] = $res->fields['shipping_other_price'];
			$ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
			$ArrList[$i]['update_by'] = $res->fields['update_by'];
            $ArrList[$i]['create_by'] = $res->fields['create_by'];
			$ArrList[$i]['ship_pay'] = $res->fields['ship_pay'];
            $ArrList[$i]['ship_pay_dtm'] = $res->fields['ship_pay_dtm'];
            
			$ArrList[$i]['total'] = $this->_calculateShippingprice($res->fields['id']);
            $other_price = $res->fields['shipping_th_price']+$res->fields['shipping_th_service_price']+$res->fields['shipping_other_price'];
            $ArrList[$i]['total'] = $ArrList[$i]['total']+$other_price;
            $ArrList[$i]['tt_status'] = $this->_calShippingStatus($res->fields['id']);
			$i++;
			$res->MoveNext();
		}

		return $ArrList;
	}
	public function _calculateShippingprice($shipping_id){
		$base = Base::getInstance();
		$tracking = $this->_shippingTrackList($shipping_id);
        //GF::print_r($tracking);
		//Edit By Weerasak 20-01-2566
		//ปิดบรรทัดข้างล่างไว้เนื่องจากไม่ได้ใช้งาน
 		//$ordering = $this->_shippingTrackOrder($shipping_id);

 		$total = 0;
 		if(count($tracking)>0){
 			foreach($tracking as $vals){
				$total = $total+$vals['ship_price'];
				//$total = $total+$vals['otr_price_baht'];
			}
			//echo $total;
		}
		// if(count($ordering)>0){
 		// 	foreach($ordering as $vals){
 		// 		if($vals['order_type']=='IN'){
		// 			$total = $total+$vals['order_price'];
		// 		}else{
		// 			$total = $total-$vals['order_price'];
		// 		}

		// 	}
		// }

		//GF::print_r($total);

		return $total;

	}
    public function _calShippingStatus($shipping_id){
		$base = Base::getInstance();
		$tracking = $this->_shippingTrackList($shipping_id);
        //GF::print_r($tracking);
        $valdata = '6';
 		if(count($tracking)>0){
             $i = 0;
 			foreach($tracking as $vals){ 
                if($i==0){
                    $valdata = $vals['tracking_info']['status_t'];
                }else{
                    if($vals['status_t']<$valdata){
                        $valdata = $vals['tracking_info']['status_t'];
                    }
                }
                $i++;
				
			}
		}
        //echo $valdata;
		
		return $valdata;

	}
	private function _deleteShipping(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$shipping_id = $base->get('_ids');
		$shippininfo = $this->_shippingInfo();

		$sql = "DELETE FROM web_shipping WHERE id=".GF::quote($shipping_id);
		$res = $db->Execute($sql);

		$sql = "DELETE FROM web_shipping_item WHERE shipping_id=".GF::quote($shipping_id);
		$res = $db->Execute($sql);

		foreach($shippininfo['tracking'] as $tracking){
			$sql = "UPDATE web_tracking SET flag='1',status='5',shipping_code='' WHERE tracking_id=".GF::quote($tracking['tracking_id']);
			$res = $db->Execute($sql);
		}
		return true;

	}
	private function _updateGetShipping(){
		$base = Base::getInstance();
		$db = DB::getInstance();
		$db2 = DB2::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];
		
		$sql = "UPDATE web_shipping SET
										ship_type=".GF::quote($base->get('POST.ship_by')).",
										pay_office=".GF::quote($base->get('POST.payoffice')).",
										shipping_name=".GF::quote($base->get('POST.user_name')).",
										shipping_address=".GF::quote($base->get('POST.user_address')).",
										shipping_phone=".GF::quote($base->get('POST.user_phone_number')).",
										track_code=".GF::quote($base->get('POST.track_code')).",
										status=".GF::quote($base->get('POST.status')).",
										update_dtm=NOW(),
										update_by=".GF::quote($user_id)."
										WHERE id=".GF::quote($base->get('POST.shipping_id'));

		$res = $db->Execute($sql);
		$base->set('_ids',$base->get('POST.shipping_id'));
		
		$shippininfo = $this->_shippingInfo();

		if($base->get('POST.status')=='6'){
			foreach($shippininfo['tracking'] as $tracking){
				$sql = "UPDATE web_tracking SET status='8' WHERE tracking=".GF::quote($tracking['tracking']);
				$res = $db->Execute($sql);
				
				$dataSet = $db2->select("web_tracking",array(
		  										    	"tracking(track_no)",
														"member_code(track_user_code)",
														"status(track_status)",
														"update_dtm(track_upddate)",
														"create_dtm(track_createdate)"
		  											),array(
		  											"tracking"=>$tracking['tracking']
		  											));
													
													
					if(count($dataSet)>0){
						
						$db2->insert("project_webservice",array(
																"service_token"=>GF::randomStr(20),
																"service_method"=>"UPDATE_TRACK",
																"service_data"=>json_encode($dataSet),
																"status"=>'N',
																"create_dtm"=>date('Y-m-d H:i:s'),
																"update_dtm"=>date('Y-m-d H:i:s')
																));
				   }
			}
		}
		
	
       
		$odr_itm = $base->get('POST.odr_itm');
		if(count($odr_itm)>0){
			foreach($odr_itm as $key=>$vals){
				$sql = "UPDATE web_shipping_order SET order_price=".GF::quote($vals)." WHERE order_id=".GF::quote($key);
				$res = $db->Execute($sql);
			}
		}
        $memberIds = $db2->get("web_shipping",array(
		  										    "create_by"
		  											),array(
		  											"id"=>$base->get('POST.shipping_id')
		  											));
        $memberinf_do = GF::memberinfo($memberIds['create_by']);                                
		$dataSet = $db2->get("web_shipping",array(
		  										    "shipping_code(billing_no)",
													"create_dtm(billing_date)",
													"shipping_name(billing_recipients)",
													"shipping_address(billing_address)",
													"create_dtm(billing_submit_date)",
													"shipping_phone(billing_tel)",
                                                    "status(billing_status)",
                                                    "track_code(billing_track)"
		  											),array(
		  											"id"=>$base->get('POST.shipping_id')
		  											));
		  											
	   $dataSet['user_code'] = $memberinf_do['user_code'];
	   
	   $dataItem = $db2->select("web_shipping_item",array(
                                                           "tracking(track_no)",
                                                           "ship_price(track_price)",
                                                           "track_rate(track_rate)"
	   													),array("shipping_id"=>$base->get('POST.shipping_id')));

	   $dataSetArr = array();
    //    if($dataSet['billing_status']=='1'){
    //                 $dataSet['billing_status']='3';
    //                }
    //                else if($dataSet['billing_status']=='3'){
    //                 $dataSet['billing_status']='5';
    //                }
    //                else if($dataSet['billing_status']=='2'){
    //                 $dataSet['billing_status']='6';
    //                } 
	   $dataSetArr['order'] =  $dataSet;
	   $dataSetArr['item'] = $dataItem;
	   $db2->insert("project_webservice",array(
												"service_token"=>GF::randomStr(20),
												"service_method"=>"UPDATE_BILL",
												"service_data"=>json_encode($dataSetArr),
												"status"=>'N',
												"create_dtm"=>date('Y-m-d H:i:s'),
												"update_dtm"=>date('Y-m-d H:i:s')
                                                ));
        $members = GF::memberinfo($memberIds['create_by']);
        $link = $base->get('BASEURL').'/#/tracking/viewshipping/'.$base->get('POST.shipping_id');
        $dataReplace = array(
            "{CUST_NAME}"=>$members['user_name'],
            "{LINK}"=>GF::shorturl($link,'1'),
            "{ORDER_NO}"=>$dataSet['billing_no'],
            "{ORDER_STATUS}"=>GF::shippingstatus($dataSet['billing_status'])
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('6',$members['ref_id']);
        $mail->msgHTML = GF::mailtemplate('6',$dataReplace,$members['ref_id']);
        $mail->addAddressEmail = $members['user_email'];
        $mail->addAddressName = $members['user_name'];
        $mail->sendMail();
		return true;

	}
   private function _updateGetShipping2(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];


		$sql = "UPDATE web_shipping SET

										track_code=".GF::quote($base->get('GET.track_code')).",
										status='2',
										update_dtm=NOW(),
										update_by=".GF::quote($user_id)."
										WHERE id=".GF::quote($base->get('GET.shipping_id'));

		$res = $db->Execute($sql);
		$base->set('_ids',$base->get('GET.shipping_id'));
		$shippininfo = $this->_shippingInfo();


		foreach($shippininfo['tracking'] as $tracking){
			$sql = "UPDATE web_tracking SET status='8' WHERE tracking=".GF::quote($tracking['tracking']);
			$res = $db->Execute($sql);
		}


		return true;

	}


	private function _listTrackingByLink(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$trackids = $base->get('GET.id');
		$trackids = urldecode($trackids);
		$trackids = base64_decode($trackids);
		$trackidsArr = explode(',',$trackids);
		$arrReturn = array();
		$i=0;
		if(count($trackidsArr)>0){
			foreach($trackidsArr as $list){
				$sql = "SELECT * FROM web_tracking WHERE tracking=".GF::quote($list);
				$res = $db->Execute($sql);
				$arrReturn[$i] = $res->fields;
				$i++;
			}
		}
		return $arrReturn;

	}

	private function _getMoneyFromWallet(){
		$base = Base::getInstance();
		$db = DB::getInstance();
        $db2 = DB2::getInstance();

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$orderid = $base->get('GET.orderid');
 		$mywallet = $memberInfomation['wallet'];
 		$priceOrder = $base->get('GET.order_price');

      	$member_new_info = GF::memberinfo($user_id);
      	$currentWallet = $mywallet;

		$sqlShipping = "SELECT * FROM web_shipping WHERE id=".GF::quote($orderid);
        $resShipping  = $db->Execute($sqlShipping);

      	$order_number = $resShipping->fields['shipping_code'];

 		if($priceOrder>=$mywallet){

			// Weerasak 25-04-2566
			// $sql = "UPDATE project_user SET
			// 							  wallet='0',
			// 							  update_dtm=NOW()
			// 								WHERE id=".GF::quote($user_id);
			// $res = $db->Execute($sql);

         $old_balance = $currentWallet;
         $new_balance = '0';

			$sql ="INSERT INTO project_wallet_log(
	 												user_id,
	 												wallet,
                                                    before_balance,
                                                    after_balance,
	 												wallet_type,
	 												wallet_desc,
                                                    wallet_ref,
                                                    wallet_ref_type,
	 												create_dtm,
	 												create_by
	 												)
	 												VALUES(
	 												".GF::quote($user_id).",
	 												".GF::quote($mywallet).",
                                       				".GF::quote($old_balance).",
                                       				".GF::quote($new_balance).",
	 												'OU',
                                                    'ชำระค่านำเข้า ORDER ID : ".$order_number."',
                                                    ".GF::quote($orderid).",
                                                    '2',
	 												NOW(),
	 												".GF::quote($user_id)."
	 												)";
	 		$res = $db->Execute($sql);

			$order_balance = $priceOrder-$mywallet;
			$sql = "UPDATE web_shipping SET ship_pay=ship_pay+".GF::quote($mywallet)." WHERE id=".GF::quote($orderid);
			$res = $db->Execute($sql);
		}else{
			$wallet_balance = $mywallet-$priceOrder;

         	$old_balance = $mywallet;
         	$new_balance = $wallet_balance;

			// Weerasak 25-04-2566
			// $sql = "UPDATE project_user SET
			// 							  wallet='".$wallet_balance."',
			// 							  update_dtm=NOW()
			// 								WHERE id=".GF::quote($user_id);
			// $res = $db->Execute($sql);



			$sql ="INSERT INTO project_wallet_log(
	 												user_id,
	 												wallet,
                                                    before_balance,
                                                    after_balance,
	 												wallet_type,
	 												wallet_desc,
                                                    wallet_ref,
                                                    wallet_ref_type,
	 												create_dtm,
	 												create_by
	 												)
	 												VALUES(
	 												".GF::quote($user_id).",
	 												".GF::quote($priceOrder).",
                                                    ".GF::quote($old_balance).",
                                                    ".GF::quote($new_balance).",
	 												'OU',
                                                    'ชำระค่านำเข้า ORDER ID : ".$order_number."',
                                                    ".GF::quote($orderid).",
                                                    '2',
	 												NOW(),
	 												".GF::quote($user_id)."
	 												)";
             $res = $db->Execute($sql);
             
             $calculate_tracking_price_ol = $this->_calculateShippingprice($orderid);

			$sql = "UPDATE web_shipping SET ship_pay=ship_pay+".GF::quote($priceOrder).",status='5',ship_pay_dtm=NOW(),billing_total=".GF::quote($calculate_tracking_price_ol)." WHERE id=".GF::quote($orderid);
			$res = $db->Execute($sql);

			$sqlselectItem = "SELECT tracking_id FROM web_shipping_item WHERE shipping_id=".intval($orderid);
			$resItem = $db->Execute($sqlselectItem);
			$itemArr = array();
			$x=0;
			while(!$resItem->EOF){
				$itemArr[$x] = $resItem->fields['tracking_id'];
				$x++;
				$resItem->MoveNext();
			}
			foreach($itemArr as $itmval){
				$SQLupdate = "UPDATE web_tracking SET status='7' WHERE tracking_id=".intval($itmval);
				$db->Execute($SQLupdate);
			}

            $memberinf_do = GF::memberinfo($user_id);                                
            $dataSet = $db2->get("web_shipping",array(
                                                        "shipping_code(billing_no)",
                                                        "create_dtm(billing_date)",
                                                        "shipping_name(billing_recipients)",
                                                        "shipping_address(billing_address)",
                                                        "create_dtm(billing_submit_date)",
                                                        "shipping_phone(billing_tel)",
                                                        "status(billing_status)",
                                                        "billing_total(billing_total)"
                                                        ),array(
                                                        "id"=>$orderid
                                                        ));
            
            $dataAdd = $db2->get("web_shipping","*",array(
                "id"=>$orderid
            ));
            $dataSet['billing_p3'] = $dataAdd['billing_total']+$dataAdd['shipping_th_price']+$dataAdd['shipping_th_service_price']+$dataAdd['shipping_other_price'];
            $dataSet['billing_sum_total'] = $dataSet['billing_p3'];

            $dataSet['user_code'] = $memberinf_do['user_code'];
            $dataItem = $db2->select("web_shipping_item",array(
                                                           "tracking(track_no)",
                                                           "ship_price(track_price)",
                                                           "track_rate(track_rate)"
	   													),array("shipping_id"=>$orderid));
        
            $dataSetArr = array();
            $dataSetArr['order'] =  $dataSet;
            $dataSetArr['item'] = $dataItem;
            $db2->insert("project_webservice",array(
                                                        "service_token"=>GF::randomStr(20),
                                                        "service_method"=>"UPDATE_BILL",
                                                        "service_data"=>json_encode($dataSetArr),
                                                        "status"=>'N',
                                                        "create_dtm"=>date('Y-m-d H:i:s'),
                                                        "update_dtm"=>date('Y-m-d H:i:s')
                                                        ));
 

            $trackArr = $itemArr;
            $dataSet = $db2->select("web_tracking",array(
		  										    	"tracking(track_no)",
														"member_code(track_user_code)",
														"status(track_status)",
														"update_dtm(track_upddate)",
														"create_dtm(track_createdate)"
		  											),array(
		  											"tracking_id"=>$trackArr
		  											));
			if(count($dataSet)>0){
			  	
				$db2->insert("project_webservice",array(
														"service_token"=>GF::randomStr(20),
														"service_method"=>"UPDATE_TRACK",
														"service_data"=>json_encode($dataSet),
														"status"=>'N',
														"create_dtm"=>date('Y-m-d H:i:s'),
														"update_dtm"=>date('Y-m-d H:i:s')
														));
		   }
            return true;

		}


	}

   private function _updateOrderTracking(){
		$base = Base::getInstance();
		$db = DB::getInstance();
		$db2 = DB2::getInstance();
		$list = $base->get('POST');

		//GF::print_r($_SESSION);

		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];
      $member_code = $memberInfomation['user_code'];

      if(count($_SESSION['item_imp'])>0){
		 $trackArr = array();
         foreach($_SESSION['item_imp'] as $key=>$vals){
            $trackArr[] = $vals['tracking_no'];
            $checkTrack = $db2->get("web_tracking","*",array("tracking"=>$vals['tracking_no']));
            if($checkTrack['tracking']!=''){


                $db2->update("web_tracking",array("member_code"=>$member_code),array("tracking"=>$vals['tracking_no']));

                if($vals['order_comment']!=""){

                	$db2->update("web_tracking",array("order_comment"=>$vals['order_comment']),array("tracking"=>$vals['tracking_no']));
                }
                if($vals['num_box']!=""){
                	
                	$db2->update("web_tracking",array("num_box"=>$vals['num_box']),array("tracking"=>$vals['tracking_no']));
                }
                if($vals['img_file']!=""){
                	
                	$db2->update("web_tracking",array("tracking_img"=>$vals['img_file']),array("tracking"=>$vals['tracking_no']));
                }

                if($vals['item_typs_s']!=""){
                	
                	$db2->update("web_tracking",array("order_type"=>$vals['item_typs_s']),array("tracking"=>$vals['tracking_no']));
                }

                $db2->update("web_tracking",array("ordership_status"=>'1'),array("tracking"=>$vals['tracking_no']));

                


            }else{
                $sql = "INSERT INTO web_tracking(
                                    tracking,
                                    member_code,
                                    order_type,
                                    status,
                                    order_comment,
                                    num_box,
                                    order_box,
                                    tracking_img,
                                    create_dtm,
                                    create_by,
                                    ordership_status
                                    )
                                    VALUES(
                                    ".GF::quote($vals['tracking_no']).",
                                    ".GF::quote($member_code).",
                                    ".GF::quote($vals['item_typs_s']).",
                                    '1',
                                    ".GF::quote($vals['order_comment']).",
                                ".GF::quote($vals['num_box']).",
                                ".GF::quote($vals['order_box']).",
                                ".GF::quote($vals['img_file']).",
                                    NOW(),
                                    ".GF::quote($user_id).",
                                    '1'
                                    )";
                //echo $sql;
                $res = $db->Execute($sql);
                $last_id = $db->Insert_ID();
                $sql = "INSERT INTO web_tracking_history(
                                    tracking_id,
                                    status,
                                    create_dtm,
                                    create_by
                                    )
                                    VALUES(
                                    ".GF::quote($last_id).",
                                    '1',
                                    NOW(),
                                    ".GF::quote($user_id)."
                                    )";
                $res = $db->Execute($sql);
            }
                    
         }

      }
	 // $trackArr;
	  $dataSet = $db2->select("web_tracking",array(
	  										    "tracking(track_no)",
												"member_code(track_user_code)",
												"order_type(track_goods_code)",
												"status(track_status)",
												"order_comment(track_remark)",
												"create_dtm(track_createdate)"
	  											),array(
	  											"tracking"=>$trackArr
	  											));
	  if(count($dataSet)>0){
	  	foreach($dataSet as $key=>$valdata){
			$dataSet[$key]['track_goods_code'] = GF::convgoodscode($valdata['track_goods_code']);
			$dataSet[$key]['track_timp_code'] = 'Car';
		}
		$db2->insert("project_webservice",array(
												"service_token"=>GF::randomStr(20),
												"service_method"=>"ADD_TRACK",
												"service_data"=>json_encode($dataSet),
												"status"=>'N',
												"create_dtm"=>date('Y-m-d H:i:s'),
												"update_dtm"=>date('Y-m-d H:i:s')
												));
	  }
      unset($_SESSION['item_imp']);
      unset($_SESSION['items_imp']);

		return true;

	}
   private function _checkTrackingNo(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$tracking = $base->get('GET.tracking');

		$sql = "SELECT * FROM web_tracking WHERE tracking=".GF::quote($tracking)." AND status='4' ";
 		$res = $db->Execute($sql);


 		$sql2 = "SELECT * FROM web_tracking WHERE tracking=".GF::quote($tracking)." AND ordership_status='1' AND status<'4' ";
 		$res2 = $db->Execute($sql2);

 		if($res->fields['tracking']!=''){

 			$ss = 1;
 		}elseif ($res->fields['tracking']=='' && $res2->fields['tracking']!='') {
 			$ss = 2;
 		}else{ $ss = ''; }

 		


		return $ss;
	}
   private function _OrderNumberToorderID($order_number){
		$base = Base::getInstance();
		$db = DB::getInstance();

		//$tracking = $base->get('GET.tracking');

		$sql = "SELECT order_id FROM web_order WHERE order_number=".GF::quote($order_number);
 		$res = $db->Execute($sql);

		return $res->fields['order_id'];
	}

   private function _updateTrackingHold(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		//$tracking = $base->get('GET.tracking');

      $tracing_ids = $base->get('POST.tracking_id');
      if(count($tracing_ids)>0){
         foreach($tracing_ids as $vals){
            $sql = "SELECT hold_status FROM web_tracking WHERE tracking_id=".GF::quote($vals);
            $res = $db->Execute($sql);
            if($res->fields['hold_status']=='N'){
               $sql = "UPDATE web_tracking SET hold_status='Y',hold_text='แจ้งโกดังเรียบร้อย',hold_date=NOW(),hold_count=1 WHERE tracking_id=".GF::quote($vals);
               $res = $db->Execute($sql);
            }else{
               $sql = "UPDATE web_tracking SET hold_count=hold_count+1 WHERE tracking_id=".GF::quote($vals);
               $res = $db->Execute($sql);
            }
         }

         $base->set('idds',$tracing_ids);

         $base->set('stt','1');
         $data1 = $this->_trackingListHoldEMail();

         $sendmail1 = false;

         if(count($data1)>0){
            $sendmail1 = $this->_mailTrackingHold($data1,'1');
         }else{
            $sendmail1 = true;
         }

         if($sendmail1){
            $base->set('stt','3');
            $data3 = $this->_trackingListHoldEMail();
            if(count($data3)>0){
               $this->_mailTrackingHold($data3,'3');
            }
         }





      }



		return true;
	}
   private function _mailTrackingHold($trackingList,$status){
      $base = Base::getInstance();
		$db = DB2::getInstance();

      $dataHtml = '<table id="example1" border="1">
                   <thead>

                     <tr>

                        <th class="text-center">
                           วันสั่ง
                        </th>
                        <th>Tracking No.</th>


                        <th>ร้านค้า</th>
                        <th>รอบออกจากจีน</th>
                        <th>สถานะค้า</th>
                        <th>ระยะวันที่ค้าง</th>
                        <th>รหัสสมาชิก</th>

                        <th style="text-align: center;">Agency</th>

                        <th>เลขออเดอร์</th>
                        <th>จำนวนครั้งที่แจ้ง</th>
                        <th>วันที่แจ้งครั้งแรก</th>
                     </tr>
                   </thead>
                   <tbody>
                   ';
                    foreach($trackingList as $list){
                      $dataHtml .= '<tr>

                         <td class="text-center">';

                        if($list["export_date"]=="" || $list["export_date"]=="0000-00-00 00:00:00"){
                              $dataHtml .= '-';
                         }else{
                              $dataHtml .=  date("d/m/Y",strtotime($list["export_date"]));
                         }
                         $dataHtml .= '</td>
                         <td>'.$list["tracking"].'</td>



                         <td>'.$list["saller_name"].'</td>
                         <td>';

                         if($list["status_value"]=="3"){
                            $dataHtml .= date("d/m/Y",strtotime($list["update_dtm"]));
                         }else{
                            $dataHtml .= "-";
                         }
                         $dataHtml .= '</td>
                         <td>ค้าง '.$list["status_value"].'</td>
                         <td>';

                            (string)$d1 = date("Y-m-d",strtotime($list["update_dtm"]));
                            (string)$d2 = date("Y-m-d");
                            $date1=date_create($d1);
                            $date2=date_create($d2);
                            $diffs = date_diff($date1,$date2);
                            echo $diffs->format("%a");
                        $dataHtml .= 'วัน
                         </td>
                         <td>'.$list["member_code"].'</td>


                            <td style="text-align: center;">
                           <span class="text-blue">'.GF::agencyname($list["user_id"]).'</span></td>

                         <td>'.$list["order_id"].'</td>
                         <td>'.$list["hold_count"].'</td>
                         <td>';
                        if($list["hold_date"]!="0000-00-00 00:00:00"){
                           $dataHtml .= date("d/m/Y",strtotime($list["hold_date"]));
                        }else{
                           $dataHtml .= "-";
                        }
                        $dataHtml .= '</td>
					</tr>
                   ';
                }
                  $dataHtml .= '</tbody></table>';

                  $mail = Mailer::getInstance();
                  $master =  array();
                  if($status=='1'){
                     $master = $db->get("project_master","*",array("id"=>'45'));
                  }

                  if($status=='3'){
                     $master = $db->get("project_master","*",array("id"=>'46'));
                  }
                  //echo "<br />-----------<br />".$dataHtml."-----------<br />";


                  $mail->addAddressEmail = $master['master_value'];
               	$mail->addAddressName = "Tracking ค้าง";

                  $mail->Subject = "แจ้ง Tracking ค้าง ".$status;
                  $mail->msgHTML = $dataHtml;
                  return $mail->sendMail();

   }
   private function _updateTrackingHoldStatus(){
		$base = Base::getInstance();
		$db = DB::getInstance();


      $sql = "UPDATE web_tracking SET hold_text=".GF::quote($base->get('POST.hold_text')).",hold_comment=".GF::quote($base->get('POST.hold_comment'))." WHERE tracking_id=".GF::quote($base->get('POST.tracking_id'));
      $res = $db->Execute($sql);
		return true;
	}

    private function _updateTHShipping(){
		$base = Base::getInstance();
		$db = DB::getInstance();
        $db2 = DB2::getInstance();

        $dataArr = $base->get('POST');

        if(count($dataArr['shipping_id'])>0){
            foreach($dataArr['shipping_id'] as $key=>$ship_id){
                if(intval($dataArr['shipping_th_price_true'][$key])>0){
                    $SQLorder = "SELECT * FROM web_shipping WHERE id=".intval($ship_id);
                    $res_order = $db->Execute($SQLorder);

                    $member_new_info = GF::memberinfo($res_order->fields['create_by']);

                    $user_id = $member_new_info['id'];

                    $diff = $dataArr['shipping_th_price'][$key]-$dataArr['shipping_th_price_true'][$key];

                    $type_wallet = 'OU';
                    $text_wallet = "ชำระค่าจัดส่ง เลขที่บิล : ".$res_order->fields['shipping_code'];
                    $wallet_ref = $ship_id;
                    $wallet_ref_type = '8';
                    if($diff>=0){
                        $type_wallet = 'IN';
                        $text_wallet = "คืนเงินค่าจัดส่ง เลขที่บิล : ".$res_order->fields['shipping_code'];
                        $wallet_ref = $ship_id;
                        $wallet_ref_type = '9';
                    }

                    $mywallet = $member_new_info['wallet'];

                    $priceOrder = 0;
                    $wallet_balance = 0;
                    if($diff>=0){
                        $priceOrder = $diff;
                        $wallet_balance = $mywallet+$priceOrder;
                    }else{
                        $priceOrder = $dataArr['shipping_th_price_true'][$key]-$dataArr['shipping_th_price'][$key];
                        $wallet_balance = $mywallet-$priceOrder;
                    }


                    $old_balance = $mywallet;
                    $new_balance = $wallet_balance;


					// Weerasak 25-04-2566
                    // $sql = "UPDATE project_user SET
                    //                             wallet='".$wallet_balance."',
                    //                             update_dtm=NOW()
                    //                                 WHERE id=".GF::quote($user_id);
                    // $res = $db->Execute($sql);



                    $sql ="INSERT INTO project_wallet_log(
                                                            user_id,
                                                            wallet,
                                                            before_balance,
                                                            after_balance,
                                                            wallet_type,
                                                            wallet_desc,
                                                            wallet_ref,
                                                            wallet_ref_type,
                                                            create_dtm,
                                                            create_by
                                                            )
                                                            VALUES(
                                                            ".GF::quote($user_id).",
                                                            ".GF::quote($priceOrder).",
                                                            ".GF::quote($old_balance).",
                                                            ".GF::quote($new_balance).",
                                                            ".GF::quote($type_wallet).",
                                                            ".GF::quote($text_wallet).",
                                                            ".GF::quote($wallet_ref).",
                                                            ".GF::quote($wallet_ref_type).",
                                                            NOW(),
                                                            ".GF::quote($user_id)."
                                                            )";
                    $res = $db->Execute($sql);

                    $sql = "UPDATE web_shipping SET shipping_th_flag='Y',shipping_th_price_true=".GF::quote($dataArr['shipping_th_price_true'][$key]).",track_code=".GF::quote($dataArr['track_code'][$key])."  WHERE id=".GF::quote($ship_id);
                    $res = $db->Execute($sql);
                }
            }
        }

        return true;

    }
    private function _deleteTrackingUser(){
		$base = Base::getInstance();
		$db = DB2::getInstance();

        $tracking_id = $base->get('GET.tracking');

        $db->update("web_tracking",array("status"=>'D'),array("tracking_id"=>$tracking_id));

        return true;

    }
    private function _checkBillingData(){
		$base = Base::getInstance();
		$db = DB2::getInstance();
		$db2 = DB::getInstance();

        $shipping_code = $base->get('GET.code');
        $user_id = $base->get('GET.id');

        $result = $db->get("web_shipping","*",array(
                                                    "AND"=>array(
                                                        "shipping_code"=>$shipping_code,
                                                        "create_by"=>$user_id
                                                    )
                                                ));
                                                
        $arrReturn = array();     
        if($result['id']==''){
        	$arrReturn['result'] = 'F';
        	$arrReturn['shipvalue'] = '0';
            $arrReturn['shipid'] = '0';
            return $arrReturn;
        }else{
			
			$sql = "SELECT * FROM web_shipping_item  WHERE shipping_id=".GF::quote($result['id']);
			$res = $db2->Execute($sql);
        	$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
												
				$arrReturn['ship_price'] += $res->fields['ship_price'];
				
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			//print_r($arrReturn['ship_price']);

		$arrReturn['que'] = intval($res->fields['rate']);									
												
										//print_r($result2);		
												
        	$arrReturn['result'] = 'T';
        	$arrReturn['shipvalue'] = $arrReturn['ship_price'];
        	 $arrReturn['shipid'] = $result['id'];  
            return $arrReturn;
        }

    }
	
	private function get_trackingID($vals)
	{
		$base = Base::getInstance();
		$db = DB::getInstance();

		$sql = "SELECT tracking_id FROM web_tracking WHERE tracking='".GF::quote($vals)."'";
		$res = $db->Execute($sql);
		return $res->fields['tracking_id'];
	}
	/* Add By Weerasak 28/02/2565 */
	public function SearchTracking(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$arrReturn = array();
		
		$track_number = $base->get('GET.tracking');		

		$sql = "SELECT tracking_id FROM web_tracking WHERE tracking ='".$track_number."'";
		$res = $db->Execute($sql);
		while(!$res->EOF){
			$arrReturn['tracking_id'] = $res->fields['tracking_id'];
			$res->MoveNext();
		}
		$res->Close();
		//print_r($res);
		return $arrReturn;
	}
	/* End */
}


?>
