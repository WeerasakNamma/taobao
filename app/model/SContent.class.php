<?php
	class SContent {




		public function createContent(){
			$resultReturn = $this->_createContent();
			return $resultReturn;
		}
		public function contentList(){
			$resultReturn = $this->_contentList();
			return $resultReturn;
		}
		public function updateSortContent(){
			$resultReturn = $this->_updateSortContent();
			return $resultReturn;
		}
		public function updateStatusContent(){
			$resultReturn = $this->_updateStatusContent();
			return $resultReturn;
		}
		public function contentInfomation(){
			$resultReturn = $this->_contentInfomation();
			//GF::print_r($resultReturn);
			return $resultReturn;
		}
		public function contentMapping(){
			$resultReturn = $this->_contentMapping();
			//GF::print_r($resultReturn);
			return $resultReturn;
		}
		public function contentUser(){
			$resultReturn = $this->_contentUser();
			//GF::print_r($resultReturn);
			return $resultReturn;
		}
		public function updateContent(){
			$resultReturn = $this->_updateContent();
			return $resultReturn;
		}
		public function contentListByRules(){
			$resultReturn = $this->_contentListByRules();
			return $resultReturn;
		}



	 	/**
		 * Content
		 *
		 * @return
		 */



	 	private function _createContent(){
			$base = Base::getInstance();
 			$db = DB::getInstance();
 			$member = new Member();

 			$memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];
            
            $agency_id = "1";
            if($memberInfomation['user_role_id']!='1'){
                $agency_id = intval($memberInfomation['id']);
            }

 			$sql = "INSERT INTO project_content (
                                                      agency_id,
													  status,
													  create_dtm,
													  create_by,
													  update_dtm,
													  update_by
													)VALUES(
														".GF::quote($agency_id).",
                                                        'O',
														NOW(),
														".GF::quote($user_id).",
														NOW(),
														".GF::quote($user_id)."
													)";

			$res = $db->Execute($sql);
 			$content_id = $db->Insert_ID();

 			//$langlist = GF::langlist();

 			//$random_number = GF::randomNumb(20);
 			$vals['lang_prefix'] = 'th';

 				$lang_prefix = $vals['lang_prefix'];





				$sql = "UPDATE project_content SET

													  content_name_".$lang_prefix."=".GF::quote($base->get('POST.content_name_'.$lang_prefix)).",
													  content_desc_".$lang_prefix."=".GF::quote($base->get('POST.content_desc_'.$lang_prefix))."
													  WHERE content_id=".GF::quote($content_id);
				////echo $sql;
				$res = $db->Execute($sql);

			$content_category = $base->get('POST.category_id');
			if(count($content_category)>0){
				foreach($content_category as $vals){
					$sql = "INSERT INTO project_content_mapping (
															  content_id,
															  category_id
															)VALUES(

																".GF::quote($content_id).",
																".GF::quote($vals)."
															)";

					$res = $db->Execute($sql);
				}
			}
			$content_user = $base->get('POST.user_ids');
			if(count($content_user)>0){
				foreach($content_user as $vals){
					$sql = "INSERT INTO project_content_user (
															  content_id,
															  category_id
															)VALUES(

																".GF::quote($content_id).",
																".GF::quote($vals)."
															)";

					$res = $db->Execute($sql);
				}
			}

 			return true;


		}

		private function _updateContent(){


			$base = Base::getInstance();
 			$db = DB::getInstance();
 			$member = new Member();
 			//GF::print_r($base->get('POST'));

 			$memberInfomation = $member->memberInfomation();
 			$user_id = $memberInfomation['id'];

 			$content_id = $base->get('POST.content_id');

 			//$langlist = GF::langlist();

 			//$random_number = GF::randomNumb(20);
 			$vals['lang_prefix'] = 'th';

 				$lang_prefix = $vals['lang_prefix'];





				$sql = "UPDATE project_content SET

													  content_name_".$lang_prefix."=".GF::quote($base->get('POST.content_name_'.$lang_prefix)).",
													  content_desc_".$lang_prefix."=".GF::quote($base->get('POST.content_desc_'.$lang_prefix)).",

													  update_dtm=NOW(),
													  update_by=".GF::quote($user_id)."
													  WHERE content_id=".GF::quote($content_id);
				////echo $sql;
				$res = $db->Execute($sql);


			$sql = "DELETE FROM project_content_mapping WHERE content_id=".GF::quote($content_id);
			////echo $sql;
			$res = $db->Execute($sql);

			$content_category = $base->get('POST.category_id');
			if(count($content_category)>0){
				foreach($content_category as $vals){
					$sql = "INSERT INTO project_content_mapping (
															  content_id,
															  category_id
															)VALUES(

																".GF::quote($content_id).",
																".GF::quote($vals)."
															)";

					$res = $db->Execute($sql);
				}
			}

 			$sql = "DELETE FROM project_content_user WHERE content_id=".GF::quote($content_id);
 			$res = $db->Execute($sql);
 			$content_user = $base->get('POST.user_ids');
			if(count($content_user)>0){
				foreach($content_user as $vals){
					$sql = "INSERT INTO project_content_user (
															  content_id,
															  category_id
															)VALUES(

																".GF::quote($content_id).",
																".GF::quote($vals)."
															)";

					$res = $db->Execute($sql);
				}
			}

 			return true;


		}

		private function _contentList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();


	 		$member = new Member();


	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];



	 		$cond_select = '';
	 		if($_SESSION['content_content']['content_name']!=''){
				$cond_select .= ' AND content_name_'.GF::langdefault()." LIKE '%".$_SESSION['content_content']['content_name']."%'";
			}
			if($_SESSION['content_content']['status']!=''){
				$cond_select .= ' AND status='.GF::quote($_SESSION['content_content']['status']);
			}

			$cond_in = '';
			if(count($_SESSION['content_content']['category_id'])>0){
				$sql_in = "SELECT content_id FROM project_content_mapping WHERE category_id IN(".implode(',',$_SESSION['content_content']['category_id']).") GROUP BY content_id";
				$res_in = $db->Execute($sql_in);
				$arr = array();
				$i = 0;
				while(!$res_in->EOF){
					$arr[$i] = $res_in->fields['content_id'];
					$i++;
					$res_in->MoveNext();
				}
				$res_in->Close();
				if(count($arr)>0){
					$cond_in .= ' AND content_id IN('.implode(',',$arr).") ";
				}else{
					$cond_in .= ' AND content_id IN(0) ';
				}

            }
            
            $cond = '';
            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $cond .= " AND agency_id = ".intval($memberInfomation['id']);
            }
            if($memberInfomation['user_role_id']=='1'){
                $cond = '';
            }

			$sql = "SELECT * FROM project_content WHERE status!='D' ".$cond_select.$cond_in.$cond." ORDER BY create_dtm DESC";

			//echo $sql;
			$res = $db->Execute($sql);
			$count = $res->NumRows();

				$base->set('allPage',ceil($count/10));

			$page = $base->get('GET.page');

			$numr = 10;
			$start = "";
			$end = "";
			if($page==1||empty($page)){
				$start = 0;
				$page = 1;
				//$end = $numr;
			}else{
				//$end = ($page*$numr);
				$start = ($page*$numr)-$numr;
			}
			 $cond = " LIMIT ".$start.",".$numr;

			 $base->set('currpage',$page);

			$res = $db->Execute($sql.$cond);
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['content_id'] = $res->fields['content_id'];
				$arrReturn[$i]['content_name'] = $res->fields['content_name_'.GF::langdefault()];
				$arrReturn[$i]['content_desc'] = $res->fields['content_desc_'.GF::langdefault()];
				$arrReturn[$i]['content_thumb'] = $res->fields['content_thumb_'.GF::langdefault()];
                $arrReturn[$i]['sort'] = $res->fields['sort'];
                $arrReturn[$i]['agency_id'] = $res->fields['agency_id'];
				$arrReturn[$i]['status'] = $res->fields['status'];
				$base->set('_cont_id',$res->fields['content_id']);
				$arrReturn[$i]['mapping'] = $this->_contentMappingName();
				$arrReturn[$i]['user'] = $this->_contentUserName();
				$arrReturn[$i]['update_dtm'] = $res->fields['update_dtm'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			//GF::print_r($arrReturn);
			return 	$arrReturn;


		}
		private function _updateSortContent(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$member = new Member();


	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];

	 		$sql = "UPDATE project_content SET
	 												sort=".GF::quote($base->get('GET.sort')).",
	 												update_dtm=NOW(),
	 												update_by=".GF::quote($user_id)."
	 												WHERE content_id=".GF::quote($base->get('GET.id'));
	 		////echo $sql;
	 		$res = $db->Execute($sql);
	 		if($res){
				echo('T');
			}
	 	}
		private function _updateStatusContent(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$member = new Member();


	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];

	 		$sql = "UPDATE project_content SET
	 												status=".GF::quote($base->get('GET.status')).",
	 												update_dtm=NOW(),
	 												update_by=".GF::quote($user_id)."
	 												WHERE content_id=".GF::quote($base->get('GET.id'));
	 		////echo $sql;
	 		$res = $db->Execute($sql);
	 		if($res){
				echo('T');
			}
	 	}

	 	private function _contentInfomation(){
	 		$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
	 		$content_id = $base->get('_ids');
	 		//echo $content_id;
	 		//echo $token;
	 		if($content_id!=''){
				$sql = "SELECT * FROM project_content WHERE content_id=".GF::quote($content_id);


				$db->SetFetchMode(ADODB_FETCH_ASSOC);
				$res = $db->Execute($sql);
				$base->set('user_id',$res->fields['create_by']);
				$res->fields['create'] = $member->memberInfomationByID();

				return $res->fields;
			}else{
				return NULL;
			}

		}
		private function _contentMappingName(){
	 		$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$content_id = $base->get('_cont_id');
	 		//echo $content_id;
	 		//echo $token;
	 		if($content_id!=''){
				$sql = "SELECT  project_content_mapping.category_id,project_role.role_name as cate_name FROM  project_content_mapping LEFT JOIN project_role ON project_content_mapping.category_id=project_role.id  WHERE project_content_mapping.content_id=".GF::quote($content_id);
				$res = $db->Execute($sql);

				$arrReturn = array();
				$i = 0;
				while(!$res->EOF){
					$arrReturn[$i] = $res->fields['cate_name'];
					$i++;
					$res->MoveNext();
				}
				$res->Close();
				//GF::print_r($arrReturn);
				return 	$arrReturn;

			}else{
				return NULL;
			}

		}
		private function _contentUserName(){
	 		$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$content_id = $base->get('_cont_id');
	 		//echo $content_id;
	 		//echo $token;
	 		if($content_id!=''){
				$sql = "SELECT  project_content_user.category_id,project_user.user_name as cate_name ,project_user.user_code as codes FROM  project_content_user LEFT JOIN project_user ON project_content_user.category_id=project_user.id  WHERE project_content_user.content_id=".GF::quote($content_id);
				$res = $db->Execute($sql);

				$arrReturn = array();
				$i = 0;
				while(!$res->EOF){
					$arrReturn[$i] = $res->fields['codes'].":".$res->fields['cate_name'];
					$i++;
					$res->MoveNext();
				}
				$res->Close();
				//GF::print_r($arrReturn);
				return 	$arrReturn;

			}else{
				return NULL;
			}

		}

		private function _contentMapping(){
	 		$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$content_id = $base->get('_ids');
	 		//echo $content_id;
	 		//echo $token;
	 		if($content_id!=''){
				$sql = "SELECT * FROM project_content_mapping WHERE content_id=".GF::quote($content_id);
				$res = $db->Execute($sql);

				$arrReturn = array();
				$i = 0;
				while(!$res->EOF){
					$arrReturn[$i] = $res->fields['category_id'];
					$i++;
					$res->MoveNext();
				}
				$res->Close();
				//GF::print_r($arrReturn);
				return 	$arrReturn;

			}else{
				return NULL;
			}

		}
		private function _contentUser(){
	 		$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$content_id = $base->get('_ids');
	 		//echo $content_id;
	 		//echo $token;
	 		if($content_id!=''){
				$sql = "SELECT * FROM project_content_user WHERE content_id=".GF::quote($content_id);
				$res = $db->Execute($sql);

				$arrReturn = array();
				$i = 0;
				while(!$res->EOF){
					$arrReturn[$i] = $res->fields['category_id'];
					$i++;
					$res->MoveNext();
				}
				$res->Close();
				//GF::print_r($arrReturn);
				return 	$arrReturn;

			}else{
				return NULL;
			}

		}
		private function _contentListByRules(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();


	 		$member = new Member();
	 		$memberInfomation = $member->memberInfomation();
	 		$user_id = $memberInfomation['id'];
            $user_role = $memberInfomation['user_role_id'];
            $agency_id = $memberInfomation['ref_id'];

	 		$rules = $base->get('rules');

	 		if($rules=='user'){
				$sql_in = "SELECT content_id FROM project_content_user WHERE category_id=".GF::quote($user_id)." GROUP BY content_id";
				$res_in = $db->Execute($sql_in);
				$arr = array();
				$i = 0;
				while(!$res_in->EOF){
					$arr[$i] = $res_in->fields['content_id'];
					$i++;
					$res_in->MoveNext();
				}
				$res_in->Close();
				if(count($arr)>0){
					$cond_in .= ' AND content_id IN('.implode(',',$arr).") ";
				}else{
					$cond_in .= ' AND content_id IN(0) ';
				}
			}

	 		if($rules=='group'){
				$sql_in = "SELECT content_id FROM project_content_mapping WHERE category_id=".GF::quote($user_role)."  GROUP BY content_id";
				$res_in = $db->Execute($sql_in);
				$arr = array();
				$i = 0;
				while(!$res_in->EOF){
					$arr[$i] = $res_in->fields['content_id'];
					$i++;
					$res_in->MoveNext();
				}
				$res_in->Close();
				if(count($arr)>0){
					$cond_in .= ' AND content_id IN('.implode(',',$arr).") AND agency_id=".intval($agency_id)." ";
				}else{
					$cond_in .= ' AND content_id IN(0) AND agency_id='.intval($agency_id).' ';
				}
			}


			$sql = "SELECT * FROM project_content WHERE status='O' ".$cond_select.$cond_in." ORDER BY create_dtm DESC";

			//echo $sql;
			$res = $db->Execute($sql);

         $count = $res->NumRows();
//echo $count;
   			$base->set('allPageHome',ceil($count/5));

   		$page = $base->get('GET.page');

   		$numr = 5;
   		$start = "";
   		$end = "";
   		if($page==1||empty($page)){
   			$start = 0;
   			$page = 1;
   			//$end = $numr;
   		}else{
   			//$end = ($page*$numr);
   			$start = ($page*$numr)-$numr;
   		}
   		 $condNum = " LIMIT ".$start.",".$numr;

   		 $base->set('currpage',$page);

   		$res = $db->Execute($sql.$condNum);

			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['content_id'] = $res->fields['content_id'];
				$arrReturn[$i]['content_name'] = $res->fields['content_name_th'];
				$arrReturn[$i]['content_desc'] = $res->fields['content_desc_th'];
				$arrReturn[$i]['content_thumb'] = $res->fields['content_thumb_th'];
				$arrReturn[$i]['sort'] = $res->fields['sort'];
				$arrReturn[$i]['status'] = $res->fields['status'];
				$base->set('_cont_id',$res->fields['content_id']);
				$arrReturn[$i]['mapping'] = $this->_contentMappingName();
				$arrReturn[$i]['user'] = $this->_contentUserName();
				$arrReturn[$i]['update_dtm'] = $res->fields['update_dtm'];
				$arrReturn[$i]['create_by'] = $res->fields['create_by'];
				$base->set('user_id',$res->fields['create_by']);
				$arrReturn[$i]['create_by_name'] = $member->memberInfomationByID();
				$arrReturn[$i]['flag'] = $this->_checkFlag($res->fields['content_id'],$user_id);
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			//GF::print_r($arrReturn);
			return 	$arrReturn;


		}
		private function _checkFlag($contetn_id,$user_id){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$sql = "SELECT flag FROM project_content_user WHERE category_id=".GF::quote($user_id)." AND content_id=".GF::quote($contetn_id);
	 		$res = $db->Execute($sql);
	 		return $res->fields['flag'];
		}


	}
?>
