<?php
 class SDashboard {

 		public function countInbox(){
			$resultreturn = $this->_countInbox();
			return $resultreturn;
		}
		public function countInboxFlag(){
			$resultreturn = $this->_countInboxFlag();
			return $resultreturn;
		}
		public function setFlag(){
			$resultreturn = $this->_setFlag();
			return $resultreturn;
		}
		public function getRate(){
			$resultreturn = $this->_getRate();
			return $resultreturn;
		}
      public function getRateActive(){
			$resultreturn = $this->_getRateActive();
			return $resultreturn;
		}
        public function countTracking(){
			$resultreturn = $this->_countTracking();
			return $resultreturn;
		}
        public function adminCountReport(){
			$resultreturn = $this->_adminCountReport();
			return $resultreturn;
		}
        public function agencyCountReport(){
			$resultreturn = $this->_agencyCountReport();
			return $resultreturn;
		}


		private function _countInbox(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$member = new Member();
	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];

	 		$sql = "SELECT count(content_id) as total FROM project_content_user WHERE category_id=".GF::quote($user_id);
	 		$res = $db->Execute($sql);


			return 	$res->fields['total'];


		}
		private function _countInboxFlag(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$member = new Member();
	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];

	 		$sql = "SELECT count(content_id) as total FROM project_content_user WHERE category_id=".GF::quote($user_id)." AND flag='N' ";
	 		$res = $db->Execute($sql);


			return 	$res->fields['total'];


		}
        private function _countTracking(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
            $stattus = $base->get('status_id');

	 		$member = new Member();
	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];

	 		$sql = "SELECT count(tracking_id) as total FROM web_tracking WHERE member_code=".GF::quote($memberInfomation['member_code'])." AND status=".$db->Quote($stattus);
	 		$res = $db->Execute($sql);


			return 	$res->fields['total'];


		}
		private function _setFlag(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$member = new Member();
	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];

	 		$contetn_id = $base->get('GET.inbox');
	 		$contetn_id = urldecode($contetn_id);
	 		$contetn_id = base64_decode($contetn_id);

	 		$sql = "UPDATE project_content_user SET flag='Y' WHERE content_id=".GF::quote($contetn_id)." AND category_id=".GF::quote($user_id);
	 		$res = $db->Execute($sql);

		}
		private function _getRate(){
			$base = Base::getInstance();
	 		$db = DB2::getInstance();

	 		$rate_id = $base->get('_ids');
	 		if($rate_id=='1'){
				$rate_id = '';

			}else{
				$rate_id = "_".$rate_id;
			}
	 		//echo $rate_id;
	 		$result = $db->select("web_rate".$rate_id,"*",array(
	 								"status[!]"=>'x',
	 								"ORDER"=>'id DESC',
	 								"LIMIT"=>20
	 								));


	 		return $result;

		}


      private function _getRateActive(){
			$base = Base::getInstance();
	 		$db = DB2::getInstance();

	 		$resultreturn = array();

	 		$result = $db->get("web_rate","*",array(
	 								"status"=>'O'
	 								));

	 		$resultreturn['rate1'] = $result;

            $result = $db->get("web_rate_2","*",array(
	 								"status"=>'O'
	 								));

	 		$resultreturn['rate2'] = $result;

            $result = $db->get("web_rate_3","*",array(
	 								"status"=>'O'
	 								));

	 		$resultreturn['rate3'] = $result;
	 		return $resultreturn;

		}
        private function _adminCountReport(){
            $base = Base::getInstance();
	 		$db = DB::getInstance();
            $arrReturn = array();

            //Edit By Weerasak 21-01-2566
            //ยอดสั่งซื้อย้อนหลัง 1 เดือน
            // $orderList = $this->_orderList();
            // $total_order = 0;
            // foreach($orderList as $vals){
            //     $total_order += $vals['true_price'];
            // }

            $sql = "SELECT ROUND(SUM(i.order_price) * o.rate,2) total
            FROM web_order o 
            INNER JOIN  (SELECT order_id,SUM(product_price_rmb * product_qty) order_price FROM web_order_item WHERE status != 'C' AND item_typs_s ='NR' GROUP BY order_id) i ON i.order_id = o.id
            WHERE active_status='O' AND order_type='1' AND status IN ('3','5')
            AND date(o.create_dtm) >= ADDDATE(NOW(),INTERVAL -1 MONTH)
            ORDER BY o.update_dtm DESC,o.create_dtm DESC";
            $res = $db->Execute($sql);
            $arrReturn['total_order'] = $res->fields['total'];
            //END
            
            $shippingList = $this->_shippingListAll();

            // Edit By Weerasak 21-01-2566
            $total_shipping = $shippingList;

            // foreach($shippingList as $vals){
            //     $total_shipping += $vals['total'];
            // }
            // END
            $arrReturn['total_shipping'] = $total_shipping;

            $memberList = $this->_memberList();
            $arrReturn['total_member'] = count($memberList);
            $arrReturn['total_weight'] = $this->_countWeight();
           //echo $total_shipping;

            return $arrReturn;

        }
        private function _agencyCountReport(){
            $base = Base::getInstance();
            $arrReturn = array();

            
            $arrReturn['total_waitorder'] = $this->_countWaitOrder();
            $arrReturn['total_waitpayment'] = $this->_paymentList();
            $arrReturn['total_refund'] = $this->_countRefund();//$this->_refundList(); Edit By Weerasak 23-01-2566
            $arrReturn['total_withdraw'] = $this->_withdrawListAll();
            $arrReturn['total_shipreq'] = $this->_registerList();
           //echo $total_shipping;

            return $arrReturn;

        }
        private function _countWeight(){

            $base = Base::getInstance();
            $db = DB::getInstance();

            // Edit By Weerasak 23-01-2566
            // กำหนดระยะเวลาในการดึงย้อนหลัง 1 เดือน
            //$SQL = "SELECT COUNT(weight) as total FROM web_tracking WHERE status>=6 AND status!='D'";
            $SQL = "SELECT round(SUM(weight),2) as total FROM web_tracking WHERE status>=6 AND status!='D' AND DATE(create_dtm) >= ADDDATE(NOW(),INTERVAL -1 MONTH)";
            $res = $db->Execute($SQL);

            //echo $res->fields['total'];
            return $res->fields['total'];

        }
        private function _orderList(){

            $base = Base::getInstance();
            $db = DB::getInstance();

            $order = new SOrder();
            $member = new Member();
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];


            $sql = "SELECT * FROM web_order WHERE  active_status='O' AND order_type='1' AND status='3' AND MONTH(update_dtm)='05' ORDER BY update_dtm DESC,create_dtm DESC";
            
            //echo $sql;exit();
            $res = $db->Execute($sql);
            $i=0;
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i]['id'] = $res->fields['id'];
                $ArrList[$i]['order_id'] = $res->fields['order_id'];
                $ArrList[$i]['order_number'] = $res->fields['order_number'];
                $ArrList[$i]['status'] = $res->fields['status'];
                $ArrList[$i]['order_price'] = $res->fields['order_price'];
                $ArrList[$i]['order_comment'] = $res->fields['order_comment'];
                $ArrList[$i]['status_text'] = $order->_convStatus('order',$res->fields['status']);
                $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
                $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
                $ArrList[$i]['rate'] = $res->fields['rate'];
                $ArrList[$i]['old_rate'] = $res->fields['old_rate'];
                $ArrList[$i]['order_pay_ship'] = $res->fields['order_pay_ship'];
            //echo $ArrList[$i]['order_pay_ship_yuan'] = $res->fields['order_pay_ship_yuan'];
                $ArrList[$i]['order_pay_cn'] = $res->fields['order_pay_cn'];
                $ArrList[$i]['export'] = $res->fields['export'];
                $ArrList[$i]['refund_wallet'] = $res->fields['refund_wallet'];
                $ArrList[$i]['order_discount'] = $res->fields['order_discount'];
                $ArrList[$i]['items'] = $order->_countItem($res->fields['id']);
                $ArrList[$i]['countprice'] = $order->_countItemPrice($res->fields['id']);

                $totals = (($ArrList[$i]['countprice']['product_price_rmb']*$ArrList[$i]['old_rate'])+$ArrList[$i]['order_pay_ship'])-$ArrList[$i]['order_discount'];
                $totals = $totals+$ArrList[$i]['countprice']['product_price_thb'];
                if($ArrList[$i]['status']=='C'){$totals=0;}
                $ArrList[$i]['total'] = $totals;

                $true_price = ($ArrList[$i]['countprice']['product_buy_rmb'])*$ArrList[$i]['rate'];
                $true_price += ($ArrList[$i]['countprice']['ship_cn']*$ArrList[$i]['rate']);
                $true_price -= $ArrList[$i]['order_discount'];
                if($ArrList[$i]['export']=='N'){$true_price=0;}

                $ArrList[$i]['true_price'] = 0;
                if($ArrList[$i]['export']=='S'){
                    $ArrList[$i]['true_price'] = $true_price;
                }

                $i++;
                $res->MoveNext();
            }

            return $ArrList;
	}
    private function _shippingListAll(){

		$base = Base::getInstance();
		$db = DB::getInstance();

        $tracking = new STracking();

		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
		$user_id = $memberInfomation['id'];
        $cond = '';
        $agency =  $memberInfomation['agency'];
        if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
                //GF::print_r($list_ref);
                $ref_str = implode(',', $list_ref);
                $cond .= " create_by IN(".$ref_str.") ";
            }else{
                $cond .= " create_by ='X' ";
            }
        }else{
            $cond = '1=1';
        }
        // Edit By Weerasak 21-01-2566
        //ยอดค่านำเข้าย้อนหลัง 1 เดือน
        $sql = "SELECT ROUND(sum(wsi.ship_price) 
        + (SELECT SUM(shipping_th_price) + SUM(shipping_th_service_price) + SUM(shipping_other_price)
        FROM web_shipping 
        WHERE ".$cond." AND status = 6
        AND date(create_dtm) >= ADDDATE(NOW(),INTERVAL -1 MONTH)),2) total
        FROM web_shipping ws
        LEFT JOIN web_shipping_item wsi ON wsi.shipping_id = ws.id
        WHERE ".$cond." AND ws.status = 6
        AND date(ws.create_dtm) >= ADDDATE(NOW(),INTERVAL -1 MONTH)";
        $res = $db->Execute($sql);
        
        // $sql = "SELECT * FROM web_shipping WHERE ".$cond." AND status='6' AND date(create_dtm) >= ADDDATE(NOW(),INTERVAL -3 MONTH)";
        // $res = $db->Execute($sql);

		// $i=0;
		// $ArrList = array();
        // $other = 0;
		// while(!$res->EOF){
		// 	$ArrList[$i]['id'] = $res->fields['id'];
            
		// 	$ArrList[$i]['total'] = $tracking->_calculateShippingprice($res->fields['id']);
        //     $other_price = $res->fields['shipping_th_price']+$res->fields['shipping_th_service_price']+$res->fields['shipping_other_price'];
        //     $ArrList[$i]['total'] = $ArrList[$i]['total']+$other_price;
		// 	$i++;
		// 	$res->MoveNext();
		// }
		//return $ArrList;

        return $res->fields['total'];
        //END
	}
    private function _memberList(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

        $member = new Member();
 		$memberInfomation = $member->memberInfomation();

      $user_id = $memberInfomation['id'];
      $cond_search= '';
      $agency =  $memberInfomation['agency'];
      if($agency=='A'){

         $cond_search .= " AND pu.ref_id =".GF::quote($user_id);
      }

		$sql = "SELECT pu.*,pr.role_name
						FROM project_user AS pu
						LEFT JOIN project_role AS pr ON(pr.id=pu.user_role_id)
						WHERE pu.active_status='O' ".$cond_search." ORDER BY pu.id ASC";
						//echo $sql;exit();
		$res = $db->Execute($sql);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
			$arrReturn[$i]['user_code'] = $res->fields['user_code'];
			$arrReturn[$i]['role_name'] = $res->fields['role_name'];
			$arrReturn[$i]['user_nickname'] = $res->fields['user_nickname'];
			$arrReturn[$i]['user_thumb'] = $res->fields['user_thumb'];
			$arrReturn[$i]['user_email'] = $res->fields['user_email'];
			$arrReturn[$i]['user_start_date'] = $res->fields['user_start_date'];
			$arrReturn[$i]['user_birthdate'] = $res->fields['user_birthdate'];
			$arrReturn[$i]['sick_leave'] = $res->fields['sick_leave'];
			$arrReturn[$i]['personal_leave'] = $res->fields['personal_leave'];
			$arrReturn[$i]['vacation_leave'] = $res->fields['vacation_leave'];
			$arrReturn[$i]['early_leave'] = $res->fields['early_leave'];
			$materdata = GF::masterdata($res->fields['position_id']);
			$arrReturn[$i]['position_name'] = $materdata['master_name'];
			$arrReturn[$i]['status'] = $res->fields['status'];
			$arrReturn[$i]['wallet'] = $res->fields['wallet'];
			$arrReturn[$i]['active_status'] = $res->fields['active_status'];
			$arrReturn[$i]['other_status'] = $res->fields['other_status'];
			//$base->set('_user_id',$res->fields['id']);
			//$arrReturn[$i]['online'] = $working->checkWokingByID();
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
    private function _countWaitOrder(){
		$base = Base::getInstance();
 		$db = DB2::getInstance();

        $member = new Member();
 		$memberInfomation = $member->memberInfomation();
		$user_id = $memberInfomation['id'];
        
        $resultReturn = 0;

        $agency =  $memberInfomation['agency'];
        if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
                $resultReturn = $db->count("web_order",array("AND"=>array("status"=>'1',"member_id"=>$list_ref)));
            }
        }else{
            $resultReturn = $db->count("web_order",array("AND"=>array("status"=>'1')));
        }
        return $resultReturn;
    }
    private function _paymentList(){

		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();
		$tracking = new STracking();

 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

        $agency =  $memberInfomation['agency'];
        $cond_search = '';
        if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
                //GF::print_r($list_ref);
                $ref_str = implode(',', $list_ref);
                $cond_search .= " AND member_id IN(".$ref_str.") ";
            }else{
                $cond_search .= " AND member_id ='X' ";
            }
        }

		$sql = "SELECT * FROM web_payment WHERE status='W' ".$cond_search." ORDER BY update_dtm DESC,create_dtm DESC";
         //echo $sql;
        $res = $db->Execute($sql);

        $i=0;
        while(!$res->EOF){
            $i++;
            $res->MoveNext();
        }

        return $i;
	}
    private function _refundList(){
        $base = Base::getInstance();
        $db = DB::getInstance();
        $db2 = DB2::getInstance();

        $member = new Member();

        $memberInfomation = $member->memberInfomation();

        $user_id = $memberInfomation['id'];
        $cond_search= '';
        $agency =  $memberInfomation['agency'];
        if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
                //GF::print_r($list_ref);
                $ref_str = implode(',', $list_ref);
                $cond_search .= " AND create_by IN(".$ref_str.") ";
            }else{
                $cond_search .= " AND create_by ='X' ";
            }
        }

        $sql = "SELECT * FROM project_item_refund WHERE status!='Delete' ".$cond_search."  ORDER BY ref_id DESC";
        //echo $sql;
        $res = $db->Execute($sql);
        $count = $res->NumRows();

            $base->set('allPage',ceil($count/20));

        $page = $base->get('GET.page');

        $numr = 20;
        if($export_data!='' || $export_id!=''){
            $numr = 999999;
        }
        $start = "";
        $end = "";
        if($page==1||empty($page)){
            $start = 0;
            $page = 1;
            //$end = $numr;
        }else{
            //$end = ($page*$numr);
            $start = ($page*$numr)-$numr;
        }
        $condNum = " LIMIT ".$start.",".$numr;

        $base->set('currpage',$page);

        $res = $db->Execute($sql.$condNum);
        //echo $sql;
        // $res = $db->Execute($sql);
        $i=0;
        while(!$res->EOF){
            $i++;
            $res->MoveNext();
        }
        //print_r($ArrList);
        return $i;
    }
    private function  _withdrawListAll(){
   		$base 	= Base::getInstance();
        $db 	= DB2::getInstance();

        $member = new Member();


        $memberInfomation = $member->memberInfomation();

         $user_id = $memberInfomation['id'];


         $memberInfomation = $member->memberInfomation();

         $user_id = $memberInfomation['id'];
         $cond_search= '';
         $agency =  $memberInfomation['agency'];
         if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
               $arrSearch['user_id'] = $list_ref;
              //$cond_search .= " AND create_by IN(".$ref_str.") ";
            }else{
               $arrSearch['user_id'] = 'X';
            }
         }
         $arrSearch['status'] = 'R';
//GF::print_r($arrSearch);
        $result = $db->count("project_withdraw","*",array(
            "AND"=>$arrSearch,
            "ORDER"=>"wd_id DESC"
        ));




		return $result;
	}
    private function _registerList(){
			$base = Base::getInstance();
			$db = DB2::getInstance();

			

            $member = new Member();

            $memberInfomation = $member->memberInfomation();

            $user_id = $memberInfomation['id'];
            $cond_search= '';
            $agency =  $memberInfomation['agency'];
            $arrSearch = array();
            if($agency=='A'){
                $list_ref = GF::refgroup();
                if(count($list_ref)>0){
                $arrSearch['user_id'] = $list_ref;
                //$cond_search .= " AND create_by IN(".$ref_str.") ";
                }else{
                $arrSearch['user_id'] = 'X';
                }
            }
            $arrSearch['status'] = 'R';
            //echo 'ddd';exit();
            $count = $db->count("project_shipping_request",array("AND"=>$arrSearch));
            //echo $db->last_query();exit();

			$base->set('allPage',ceil($count/20));

			$page = $base->get('GET.page');

			$numr = 20;
			$start = "";
			$end = "";
			if($page==1||empty($page)){
				$start = 0;
				$page = 1;
				//$end = $numr;
			}else{
				//$end = ($page*$numr);
				$start = ($page*$numr)-$numr;
			}


			$base->set('currpage',$page);

			$result = $db->count("project_shipping_request","*",
									array(
                                        "AND"=>$arrSearch,
										"ORDER"=>'req_id DESC',
										"LIMIT"=>[$start,$numr]
									)
									);
			return $result;
		}
        // Edit By Weerasak 23-01-2566
        private function _countRefund(){
            $base = Base::getInstance();
            $db = DB::getInstance();

            $sql = "SELECT COUNT(1) as total_refund
            FROM project_item_refund ref
            WHERE ref.status = 'Wait'";
            $res = $db->Execute($sql);
            return $res->fields['total_refund'];
        }
        //END
 }
?>
