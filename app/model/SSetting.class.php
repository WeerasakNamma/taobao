<?php
	class SSetting {

		public function moduleList(){
			$resultreturn =  $this->_modlueList();
			return $resultreturn;
		}
		public function createGroup(){
			$resultreturn =  $this->_createGroup();
			return $resultreturn;
		}
		public function updateGroup(){
			$resultreturn =  $this->_updateGroup();
			return $resultreturn;
		}
		public function groupInfomation(){
			$resultreturn =  $this->_groupInfomation();
			return $resultreturn;
		}
		public function groupList(){
			$resultreturn =  $this->_groupList();
			return $resultreturn;
		}
		public function deleteGroup(){
			$resultreturn =  $this->_deleteGroup();
			return $resultreturn;
		}
		public function updateUserPermission(){
			$resultreturn =  $this->_updateUserPermission();
			return $resultreturn;
		}
		public function positionInfomation(){
			$resultreturn =  $this->_positionInfomation();
			return $resultreturn;
		}
		public function createPosition(){
			$resultreturn =  $this->_createPosition();
			return $resultreturn;
		}
		public function updatePosition(){
			$resultreturn =  $this->_updatePosition();
			return $resultreturn;
		}
		public function deletePosition(){
			$resultreturn =  $this->_deletePosition();
			return $resultreturn;
		}
		public function masterGroupList(){
			$resultreturn =  $this->_masterGroupList();
			return $resultreturn;
		}
		public function masterGroupInfomation(){
			$resultreturn =  $this->_masterGroupInfomation();
			return $resultreturn;
		}
		public function createMasterdataGroup(){
			$resultreturn =  $this->_createMasterdataGroup();
			return $resultreturn;
		}
		public function updateMasterGroup(){
			$resultreturn =  $this->_updateMasterGroup();
			return $resultreturn;
		}
		public function deleteMasterGroup(){
			$resultreturn =  $this->_deleteMasterGroup();
			return $resultreturn;
		}

		public function createMasterData(){
			$resultreturn =  $this->_createMasterdata();
			return $resultreturn;
		}
		public function updateMasterData(){
			$resultreturn =  $this->_updateMasterdata();
			return $resultreturn;
		}
		public function masterdataList(){
			$resultreturn =  $this->_masterdataList();
			return $resultreturn;
		}
		public function masterdataInfomation(){
			$resultreturn =  $this->_masterdataInfomation();
			return $resultreturn;
		}
		public function deleteMasterdata(){
			$resultreturn =  $this->_deleteMasterdata();
			return $resultreturn;
		}
		public function saveSprintTemplate(){
			$resultreturn =  $this->_saveSprintTemplate();
			return $resultreturn;
		}
		public function getSprintTemplateList(){
			$resultreturn =  $this->_getSprintTemplateList();
			return $resultreturn;
		}
		public function editSprintTemplate(){

			$resultreturn =  $this->_editSprintTemplate();
			return $resultreturn;
		}
		public function editSprintTemplateItem(){
			$resultreturn =  $this->_editSprintTemplateItem();
			return $resultreturn;
		}
		public function deleteSprintTemplete(){
			$resultreturn =  $this->_deleteSprintTemplete();
			return $resultreturn;
		}
		public function saveHolidaySetting(){
			$resultreturn =  $this->_saveHolidaySetting();
			return $resultreturn;
		}
		public function getHolidayList(){
			$resultreturn = $this->_getHolidayList();
			return $resultreturn;
		}
		public function editHolidaySetting(){
			$resultreturn = $this->_editHolidaySetting();
			return $resultreturn;
		}
		public function deleteHolidaySetting(){
			$resultreturn = $this->_deleteHolidaySetting();
			return $resultreturn;
		}
		public function getRateExchange(){
			$resultreturn = $this->_getRateExchange();
			return $resultreturn;
		}
        public function getRateExchange2(){
			$resultreturn = $this->_getRateExchange2();
			return $resultreturn;
		}
        public function getRateExchange3(){
			$resultreturn = $this->_getRateExchange3();
			return $resultreturn;
		}
		public function saveRateExchange(){
			$resultreturn = $this->_saveRateExchange();
			return $resultreturn;
		}
        public function saveRateExchange2(){
			$resultreturn = $this->_saveRateExchange2();
			return $resultreturn;
		}
        public function saveRateExchange3(){
			$resultreturn = $this->_saveRateExchange3();
			return $resultreturn;
		}
		public function saveEcoupon(){
			$resultreturn = $this->_saveEcoupon();
			return $resultreturn;
		}
		public function getEcouponList(){
			$resultreturn = $this->_getEcouponList();
			return $resultreturn;
		}
		public function getEcoupon(){
			$resultreturn = $this->_getEcoupon();
			return $resultreturn;
		}
		public function delEcoupon(){
			$resultreturn = $this->_delEcoupon();
			return $resultreturn;
		}
		public function checkEcoupon(){
			$resultreturn = $this->_checkEcoupon();
			return $resultreturn;
		}
		public function uploadTemp(){
			$resultreturn = $this->_uploadTemp();
			return $resultreturn;
		}
		public function createFtpTemplate(){
			$resultreturn = $this->_createFtpTemplate();
			return $resultreturn;
		}
		public function templateList(){
			$resultreturn = $this->_templateList();
			return $resultreturn;
		}
		public function updateFtmTemplate(){
			$resultreturn = $this->_updateFtpTemplate();
			return $resultreturn;
		}
		public function templateInfo(){
			$resultreturn = $this->_templateInfo();
			return $resultreturn;
		}
		public function deleteTemplate(){
			$resultreturn = $this->_deleteFtpTemplate();
			return $resultreturn;
		}
		public function addRate(){
			$resultreturn = $this->_addrate();
			return $resultreturn;
		}
		public function rateUserList(){
			$resultreturn = $this->_rateUserList();
			return $resultreturn;
		}
		public function deleteRate(){
			$resultreturn = $this->_deleteRate();
			return $resultreturn;
		}
        public function getGeneralSetting(){
			$resultreturn = $this->_getGeneralSetting();
			return $resultreturn;
        }
        public function getGeneralSettingByuser(){
			$resultreturn = $this->_getGeneralSettingByuser();
			return $resultreturn;
		}
        public function updateGeneral(){
			$resultreturn = $this->_updateGeneral();
			return $resultreturn;
        }
        public function updateGeneralContent(){
			$resultreturn = $this->_updateGeneralContent();
			return $resultreturn;
		}
		
		public function syncList(){
			$resultreturn = $this->_syncList();
			return $resultreturn;
        }
        public function agencyList(){
			$resultreturn = $this->_agencyList();
			return $resultreturn;
        }
        public function insertBank(){
			$resultreturn = $this->_insertBank();
			return $resultreturn;
        }
        public function updateBank(){
			$resultreturn = $this->_updateBank();
			return $resultreturn;
        }
        public function bankList(){
			$resultreturn = $this->_bankList();
			return $resultreturn;
        }
        public function bankInfo(){
			$resultreturn = $this->_bankInfo();
			return $resultreturn;
        }
        public function deleteBank(){
			$resultreturn = $this->_deleteBank();
			return $resultreturn;
        }
        public function manualList(){
			$resultreturn = $this->_manualList();
			return $resultreturn;
		}
		public function createContent(){
			$resultReturn = $this->_createContent();
			return $resultReturn;
        }
        public function updateStatusContent(){
			$resultReturn = $this->_updateStatusContent();
			return $resultReturn;
		}
		public function contentInfomation(){
			$resultReturn = $this->_contentInfomation();
			//GF::print_r($resultReturn);
			return $resultReturn;
		}
		public function updateContent(){
			$resultReturn = $this->_updateContent();
			return $resultReturn;
        }
        public function getEmailTemplate(){
			$resultReturn = $this->_getEmailTemplate();
			return $resultReturn;
        }
        public function updateEmailTemplate(){
			$resultReturn = $this->_updateEmailTemplate();
			return $resultReturn;
		}
		
		 public function updateUserRate(){
			$resultReturn = $this->_updateUserRate();
			return $resultReturn;
		}

		

		private function _modlueList(){
			$base = Base::getInstance();
 			//$db = DB::getInstance();
 			//$member = new Member();
 			$module = new Module();

 			$moduleList = $module->moduleList();

 			return $moduleList;
		}
		private function _createGroup(){
			$base = Base::getInstance();
 			$db = DB::getInstance();
 			$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

 			$noti = $base->get('POST.email_notification');
            $noti_acc = $base->get('POST.email_account');
            $admin_group = $base->get('POST.admin_group');

 			if($noti==''){
				$noti = 'D';
			}
			if($noti_acc==''){
				$noti_acc = 'D';
            }
            if($admin_group==''){
				$admin_group = 'D';
			}

 			$sql = "INSERT INTO project_role (
												  role_name,
												  email_notification,
												  email_account,
                                                  admin_group,
                                                  agency_id,
                                                  agency_type,
                                                  agency_default,
												  status,
												  create_dtm,
												  create_by
												)VALUES(
													".GF::quote($base->get('POST.role_name')).",
													".GF::quote($noti).",
                                                    ".GF::quote($noti_acc).",
                                                    ".GF::quote($admin_group).",
                                                    ".GF::quote($base->get('POST.agency_id')).",
                                                    ".GF::quote($base->get('POST.agency_type')).",
                                                    ".GF::quote($base->get('POST.agency_default')).",
													'O',
													NOW(),
													".GF::quote($user_id)."
												)";
			//echo $sql;
			$res = $db->Execute($sql);
			if($res){
				$role_id = $db->Insert_ID();
				/*$allow_module = $base->get('POST.allow_module');
				$denied_module = $base->get('POST.denied_module');

				$view_module = $base->get('POST.view_module');
				$edit_module = $base->get('POST.edit_module');
				$delete_module = $base->get('POST.delete_module');

				$allow_item = $base->get('POST.allow_item');
				$denied_item = $base->get('POST.denied_item');

				$view_item = $base->get('POST.view_item');
				$edit_item = $base->get('POST.edit_item');
				$delete_item = $base->get('POST.delete_item');*/

				$allow_module = (is_array($base->get('POST.allow_module'))) ? $base->get('POST.allow_module') : array() ;
				$denied_module = (is_array($base->get('POST.denied_module'))) ? $base->get('POST.denied_module') : array() ;

				$view_module = (is_array($base->get('POST.view_module'))) ? $base->get('POST.view_module') : array() ;
				$edit_module = (is_array($base->get('POST.edit_module'))) ? $base->get('POST.edit_module') : array() ;
				$delete_module = (is_array($base->get('POST.delete_module'))) ? $base->get('POST.delete_module') : array() ;

				$allow_item = (is_array($base->get('POST.allow_item'))) ? $base->get('POST.allow_item') : array() ;
				$denied_item = (is_array($base->get('POST.denied_item'))) ? $base->get('POST.denied_item') : array() ;

				$view_item = (is_array($base->get('POST.view_item'))) ? $base->get('POST.view_item') : array() ;
				$edit_item = (is_array($base->get('POST.edit_item'))) ? $base->get('POST.edit_item') : array() ;
				$delete_item = (is_array($base->get('POST.delete_item'))) ? $base->get('POST.delete_item') : array() ;



				foreach($moduleList as $key=>$list){

						$permission = 'D';
						$per_view = 'D';
						$per_edit = 'D';
						$per_delete = 'D';


						$key_permission = array_search($list['id'],$allow_module);
						$key_view = array_search($list['id'],$view_module);
						$key_edit = array_search($list['id'],$edit_module);
						$key_delete = array_search($list['id'],$delete_module);

						if($key_permission !== false){
							$permission = 'A';
						}
						if($key_view !== false){
							$per_view = 'A';
						}
						if($key_edit !== false){
							$per_edit = 'A';
						}
						if($key_delete !== false){
							$per_delete = 'A';
						}

						$sql = "INSERT INTO project_role_module (
															  role_id,
															  module_id,
															  permission,
															  per_view,
															  per_edit,
															  per_delete
															)VALUES(
																".GF::quote($role_id).",
																".GF::quote($list['id']).",
																".GF::quote($permission).",
																".GF::quote($per_view).",
																".GF::quote($per_edit).",
																".GF::quote($per_delete)."
															)";

						$res = $db->Execute($sql);
						if($list['module_type']=='M'){
							foreach($list['item'] as $items){
								$permission = 'D';
								$per_view = 'D';
								$per_edit = 'D';
								$per_delete = 'D';

								$key_permission = array_search($items['id'],$allow_item);
								$key_view = array_search($items['id'],$view_item);
								$key_edit = array_search($items['id'],$edit_item);
								$key_delete = array_search($items['id'],$delete_item);

								if($key_permission !== false){
									$permission = 'A';
								}
								if($key_view !== false){
									$per_view = 'A';
								}
								if($key_edit !== false){
									$per_edit = 'A';
								}
								if($key_delete !== false){
									$per_delete = 'A';
								}
								$sql = "INSERT INTO project_role_item (
																	  role_id,
																	  item_id,
																	  permission,
																	  per_view,
																	  per_edit,
																	  per_delete
																	)VALUES(
																		".GF::quote($role_id).",
																		".GF::quote($items['id']).",
																		".GF::quote($permission).",
																		".GF::quote($per_view).",
																		".GF::quote($per_edit).",
																		".GF::quote($per_delete)."
																	)";

								$res = $db->Execute($sql);
							}
						}
				}
				return true;
			}else{
				return false;
			}
		}


		private function _updateGroup(){

			$base 	= Base::getInstance();
 			$db 	= DB::getInstance();
 			$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

 			$noti = $base->get('POST.email_notification');
             $noti_acc = $base->get('POST.email_account');
             $admin_group = $base->get('POST.admin_group');
 			if($noti==''){
				$noti = 'D';
			}
			if($noti_acc==''){
				$noti_acc = 'D';
            }
            if($admin_group==''){
				$admin_group = 'D';
			}
            
 			$sql = "UPDATE project_role SET
											role_name=".GF::quote($base->get('POST.role_name')).",
											email_notification=".GF::quote($noti).",
                                            email_account=".GF::quote($noti_acc).",
                                            admin_group=".GF::quote($admin_group).",
                                            agency_id=".GF::quote($base->get('POST.agency_id')).",
                                            agency_type=".GF::quote($base->get('POST.agency_type')).",
                                            agency_default=".GF::quote($base->get('POST.agency_default')).",
											update_dtm=NOW(),
											update_by=".GF::quote($user_id)."
										    WHERE id=".GF::quote($base->get('POST.role_id'));
			//echo $sql;
			$res = $db->Execute($sql);

			if($res){
				$role_id = $base->get('POST.role_id');

				$sql = "DELETE FROM project_role_module WHERE role_id=".GF::quote($role_id);
				$res = $db->Execute($sql);

				$sql = "DELETE FROM project_role_item WHERE role_id=".GF::quote($role_id);
				$res = $db->Execute($sql);

				$allow_module = $base->get('POST.allow_module');
				$denied_module = $base->get('POST.denied_module');

				/*$view_module = array();
				$edit_module = array();
				$delete_module = array();

				$allow_item = array();
				$denied_item = array();

				$view_item = array();
				$edit_item = array();
				$delete_item = array();*/

				$allow_module = (is_array($base->get('POST.allow_module'))) ? $base->get('POST.allow_module') : array() ;
				$denied_module = (is_array($base->get('POST.denied_module'))) ? $base->get('POST.denied_module') : array() ;

				$view_module = (is_array($base->get('POST.view_module'))) ? $base->get('POST.view_module') : array() ;
				$edit_module = (is_array($base->get('POST.edit_module'))) ? $base->get('POST.edit_module') : array() ;
				$delete_module = (is_array($base->get('POST.delete_module'))) ? $base->get('POST.delete_module') : array() ;

				$allow_item = (is_array($base->get('POST.allow_item'))) ? $base->get('POST.allow_item') : array() ;
				$denied_item = (is_array($base->get('POST.denied_item'))) ? $base->get('POST.denied_item') : array() ;

				$view_item = (is_array($base->get('POST.view_item'))) ? $base->get('POST.view_item') : array() ;
				$edit_item = (is_array($base->get('POST.edit_item'))) ? $base->get('POST.edit_item') : array() ;
				$delete_item = (is_array($base->get('POST.delete_item'))) ? $base->get('POST.delete_item') : array() ;

				//GF::print_r($base->get('POST'));

				foreach($moduleList as $key=>$list){

						$permission = 'D';
						$per_view = 'D';
						$per_edit = 'D';
						$per_delete = 'D';


						$key_permission = array_search($list['id'],$allow_module);
						$key_view = array_search($list['id'],$view_module);
						$key_edit = array_search($list['id'],$edit_module);
						$key_delete = array_search($list['id'],$delete_module);

						if($key_permission !== false){
							$permission = 'A';
						}
						if($key_view !== false){
							$per_view = 'A';
						}
						if($key_edit !== false){
							$per_edit = 'A';
						}
						if($key_delete !== false){
							$per_delete = 'A';
						}

						$sql = "INSERT INTO project_role_module (
															  role_id,
															  module_id,
															  permission,
															  per_view,
															  per_edit,
															  per_delete
															)VALUES(
																".GF::quote($role_id).",
																".GF::quote($list['id']).",
																".GF::quote($permission).",
																".GF::quote($per_view).",
																".GF::quote($per_edit).",
																".GF::quote($per_delete)."
															)";

						$res = $db->Execute($sql);
						if($list['module_type']=='M'){
							foreach($list['item'] as $items){
								$permission = 'D';
								$per_view = 'D';
								$per_edit = 'D';
								$per_delete = 'D';

								$key_permission = array_search($items['id'],$allow_item);
								$key_view = array_search($items['id'],$view_item);
								$key_edit = array_search($items['id'],$edit_item);
								$key_delete = array_search($items['id'],$delete_item);

								if($key_permission !== false){
									$permission = 'A';
								}
								if($key_view !== false){
									$per_view = 'A';
								}
								if($key_edit !== false){
									$per_edit = 'A';
								}
								if($key_delete !== false){
									$per_delete = 'A';
								}
								$sql = "INSERT INTO project_role_item (
																	  role_id,
																	  item_id,
																	  permission,
																	  per_view,
																	  per_edit,
																	  per_delete
																	)VALUES(
																		".GF::quote($role_id).",
																		".GF::quote($items['id']).",
																		".GF::quote($permission).",
																		".GF::quote($per_view).",
																		".GF::quote($per_edit).",
																		".GF::quote($per_delete)."
																	)";

								$res = $db->Execute($sql);
							}
						}
				}
				return true;
			}else{
				return false;
			}
		}
		private function _groupInfomation(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();


	 		$role_id = $base->get('_ids');
	 		if($base->get('_ids_')!=''){
				$role_id = $base->get('_ids_');
			}
	 		//echo $role_id;
	 		if($role_id!=''){
				$sql = "SELECT * FROM project_role WHERE id=".GF::quote($role_id);

				//echo $sql;

				$db->SetFetchMode(ADODB_FETCH_ASSOC);
				$res = $db->Execute($sql);
				$arrReturn = array();
				$arrReturn['role_id'] = $res->fields['id'];
				$arrReturn['role_name'] = $res->fields['role_name'];
				$arrReturn['email_notification'] = $res->fields['email_notification'];
                $arrReturn['email_account'] = $res->fields['email_account'];
                $arrReturn['admin_group'] = $res->fields['admin_group'];
                $arrReturn['agency_id'] = $res->fields['agency_id'];
                $arrReturn['agency_type'] = $res->fields['agency_type'];
                $arrReturn['agency_default'] = $res->fields['agency_default'];
				$base->set('_role_id_',$res->fields['id']);
				$arrReturn['module'] = $this->_roleModuleList();
				$arrReturn['item'] = $this->_roleItemList();

				//GF::print_r($arrReturn['module']);


				return $arrReturn;
			}else{
				return NULL;
			}
		}

		public function groupInfomation2(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();



				$role_id = $base->get('_ids_');


	 		if($role_id!=''){
				$sql = "SELECT * FROM project_role WHERE id=".GF::quote($role_id);

				//echo $sql;

				$db->SetFetchMode(ADODB_FETCH_ASSOC);
				$res = $db->Execute($sql);
				$arrReturn = array();
				$arrReturn['role_id'] = $res->fields['id'];
				$arrReturn['role_name'] = $res->fields['role_name'];
                $arrReturn['email_notification'] = $res->fields['email_notification'];
                $arrReturn['admin_group'] = $res->fields['admin_group'];
                $arrReturn['agency_id'] = $res->fields['agency_id'];
                $arrReturn['agency_type'] = $res->fields['agency_type'];
                $arrReturn['agency_default'] = $res->fields['agency_default'];
				$base->set('_role_id_',$res->fields['id']);
				$arrReturn['module'] = $this->_roleModuleList();
				$arrReturn['item'] = $this->_roleItemList();

				//GF::print_r($arrReturn['module']);


				return $arrReturn;
			}else{
				return NULL;
			}
		}
		private function _roleModuleList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$role_id = $base->get('_role_id_');

	 		$sql = "SELECT * FROM project_role_module WHERE role_id=".GF::quote($role_id);
	 		//echo $sql;
	 		$res = $db->Execute($sql);
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['module_id'] = $res->fields['module_id'];
				$arrReturn[$i]['permission'] = $res->fields['permission'];
				$arrReturn[$i]['per_view'] = $res->fields['per_view'];
				$arrReturn[$i]['per_edit'] = $res->fields['per_edit'];
				$arrReturn[$i]['per_delete'] = $res->fields['per_delete'];

				$i++;
				$res->MoveNext();
			}
			$res->Close();
			return 	$arrReturn;
		}
		private function _roleItemList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$role_id = $base->get('_role_id_');

	 		$sql = "SELECT * FROM project_role_item WHERE role_id=".GF::quote($role_id);
	 		$res = $db->Execute($sql);
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['item_id'] = $res->fields['item_id'];
				$arrReturn[$i]['permission'] = $res->fields['permission'];
				$arrReturn[$i]['per_view'] = $res->fields['per_view'];
				$arrReturn[$i]['per_edit'] = $res->fields['per_edit'];
				$arrReturn[$i]['per_delete'] = $res->fields['per_delete'];

				$i++;
				$res->MoveNext();
			}
			$res->Close();
			return 	$arrReturn;
		}
		private function _groupList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
            $member = new Member();

            $memberInfomation = $member->memberInfomation();
            
            $user_id = $memberInfomation['id'];
            
            $cond = '';
            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $cond .= " AND agency_id = ".intval($memberInfomation['id']);
            }
            if($memberInfomation['user_role_id']=='1'){
                $cond = '';
            }

	 		$sql = "SELECT * FROM project_role WHERE status='O'".$cond;
	 		$res = $db->Execute($sql);
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['role_id'] = $res->fields['id'];
                $arrReturn[$i]['role_name'] = $res->fields['role_name'];
                $arrReturn[$i]['agency_id'] = $res->fields['agency_id'];
				$base->set('_role_id',$res->fields['id']);
				$arrReturn[$i]['module'] = $this->_roleModuleList();
				$arrReturn[$i]['member'] = array();//$member->memberListByGroup();

				$i++;
				$res->MoveNext();
			}
			$res->Close();
			return 	$arrReturn;
		}
		private function _deleteGroup(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$sql = "UPDATE project_role SET status='C' WHERE id=".GF::quote($base->get('_ids'));

	 		$res = $db->Execute($sql);
	 	}

	 	private function _updateUserPermission(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$user_id = $base->get('POST.user_id');
	 		$role_id = $base->get('POST.role_id');

	 		if(is_array($user_id)){
				foreach($user_id as $vals){
					$sql = "UPDATE project_user SET user_role_id=".GF::quote($role_id)." WHERE id=".GF::quote($vals);
	 				$res = $db->Execute($sql);
				}
				return true;
			}else{
				return true;
			}


		}
		private function _positionInfomation(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();


	 		$position_id = $base->get('_ids');

	 		if($position_id!=''){
				$sql = "SELECT * FROM project_position WHERE id=".GF::quote($position_id);


				$db->SetFetchMode(ADODB_FETCH_ASSOC);
				$res = $db->Execute($sql);
				$arrReturn = array();
				$arrReturn['id'] = $res->fields['id'];
				$arrReturn['position_name'] = $res->fields['position_name'];



				return $arrReturn;
			}else{
				return NULL;
			}
		}
		private function _createPosition(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

			$sql = "INSERT INTO project_position (
												  position_name,
												  status,
												  active_status,
												  create_dtm,
												  create_by
												)VALUES(
													".GF::quote($base->get('POST.position_name')).",
													'O',
													'O',
													NOW(),
													".GF::quote($user_id)."
												)";

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}
		private function _updatePosition(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

			$sql = "UPDATE project_position SET
												  position_name=".GF::quote($base->get('POST.position_name')).",
												  update_dtm=NOW(),
												  update_by=".GF::quote($user_id)."
												WHERE id=".GF::quote($base->get('POST.position_id'));

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}
		private function _deletePosition(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

			$sql = "UPDATE project_position SET
												  status='C',
												  active_status='C',
												  update_dtm=NOW(),
												  update_by=".GF::quote($user_id)."
												WHERE id=".GF::quote($base->get('_ids'));

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}


		private function _masterGroupList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

			$sql = "SELECT * FROM project_master_group WHERE status='O' AND active_status='O' ORDER BY id ASC";
			$res = $db->Execute($sql);

			$res = $db->Execute($sql);
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['id'] = $res->fields['id'];
				$arrReturn[$i]['group_name'] = $res->fields['group_name'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			return 	$arrReturn;
		}
		private function _masterGroupInfomation(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();


	 		$group_id = $base->get('_ids');

	 		if($group_id!=''){
				$sql = "SELECT * FROM project_master_group WHERE id=".GF::quote($group_id);


				$db->SetFetchMode(ADODB_FETCH_ASSOC);
				$res = $db->Execute($sql);
				$arrReturn = array();
				$arrReturn['id'] = $res->fields['id'];
				$arrReturn['group_name'] = $res->fields['group_name'];



				return $arrReturn;
			}else{
				return NULL;
			}
		}

		private function _createMasterdataGroup(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();


 			$user_id = $memberInfomation['id'];

			$sql = "INSERT INTO project_master_group (
												  group_name,
												  status,
												  active_status,
												  create_dtm,
												  create_by
												)VALUES(
													".GF::quote($base->get('POST.group_name')).",
													'O',
													'O',
													NOW(),
													".GF::quote($user_id)."
												)";

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}
		private function _updateMasterGroup(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

			$sql = "UPDATE project_master_group SET
												  group_name=".GF::quote($base->get('POST.group_name')).",
												  update_dtm=NOW(),
												  update_by=".GF::quote($user_id)."
												WHERE id=".GF::quote($base->get('POST.group_id'));

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}

		private function _deleteMasterGroup(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

			$sql = "UPDATE project_master_group SET
												  status='C',
												  active_status='C',
												  update_dtm=NOW(),
												  update_by=".GF::quote($user_id)."
												WHERE id=".GF::quote($base->get('_ids'));

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}
		private function _createMasterdata(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();


 			$user_id = $memberInfomation['id'];

			$sql = "INSERT INTO project_master (
												  master_name,
												  master_value,
												  master_char,
												  group_id,
												  status,
												  active_status,
												  create_dtm,
												  create_by
												)VALUES(
													".GF::quote($base->get('POST.master_name')).",
													".GF::quote($base->get('POST.master_value')).",
													".GF::quote($base->get('POST.master_char')).",
													".GF::quote($base->get('POST.group_id')).",
													'O',
													'O',
													NOW(),
													".GF::quote($user_id)."
												)";

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}
		private function _masterdataList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

			$group_id = $base->get('_ids');
			$sql = "SELECT * FROM project_master WHERE status='O' AND active_status='O' AND group_id=".GF::quote($group_id)." ORDER BY id ASC";
			$res = $db->Execute($sql);

			$res = $db->Execute($sql);
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['id'] = $res->fields['id'];
				$arrReturn[$i]['master_name'] = $res->fields['master_name'];
				$arrReturn[$i]['master_value'] = $res->fields['master_value'];
				$arrReturn[$i]['master_char'] = $res->fields['master_char'];
				$arrReturn[$i]['group_id'] = $res->fields['group_id'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			return 	$arrReturn;
		}
		private function _masterdataInfomation(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();


	 		$group_id = $base->get('_ids');

	 		if($group_id!=''){
				$sql = "SELECT * FROM project_master WHERE id=".GF::quote($group_id);


				$db->SetFetchMode(ADODB_FETCH_ASSOC);
				$res = $db->Execute($sql);
				$arrReturn = array();
				$arrReturn['id'] = $res->fields['id'];
				$arrReturn['master_name'] = $res->fields['master_name'];
				$arrReturn['master_value'] = $res->fields['master_value'];
				$arrReturn['master_char'] = $res->fields['master_char'];
				$arrReturn['group_id'] = $res->fields['group_id'];


				return $arrReturn;
			}else{
				return NULL;
			}
		}
		private function _updateMasterdata(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

			$sql = "UPDATE project_master SET
												  master_name=".GF::quote($base->get('POST.master_name')).",
												  master_value=".GF::quote($base->get('POST.master_value')).",
												  master_char=".GF::quote($base->get('POST.master_char')).",
												  update_dtm=NOW(),
												  update_by=".GF::quote($user_id)."
												WHERE id=".GF::quote($base->get('POST.master_id'));

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}

		private function _deleteMasterdata(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();

 			$memberInfomation = $member->memberInfomation();
 			$moduleList = $this->_modlueList();


 			$user_id = $memberInfomation['id'];

			$sql = "UPDATE project_master SET
												  status='C',
												  active_status='C',
												  update_dtm=NOW(),
												  update_by=".GF::quote($user_id)."
												WHERE id=".GF::quote($base->get('_ids'));

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}

		}



		private function _getRateExchange(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$sql = "SELECT * FROM web_rate WHERE status='O' ORDER BY id DESC";
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();
			$arrReturn['id'] = $res->fields['id'];
			$arrReturn['rate'] = $res->fields['rate'];
			$arrReturn['status'] = $res->fields['status'];
			$arrReturn['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn['create_by'] = $res->fields['create_by'];
			return 	$arrReturn;
		}
        private function _getRateExchange2(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$sql = "SELECT * FROM web_rate_2 WHERE status='O' ORDER BY id DESC";
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();
			$arrReturn['id'] = $res->fields['id'];
			$arrReturn['rate'] = $res->fields['rate'];
			$arrReturn['status'] = $res->fields['status'];
			$arrReturn['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn['create_by'] = $res->fields['create_by'];
			return 	$arrReturn;
		}
        private function _getRateExchange3(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$sql = "SELECT * FROM web_rate_3 WHERE status='O' ORDER BY id DESC";
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();
			$arrReturn['id'] = $res->fields['id'];
			$arrReturn['rate'] = $res->fields['rate'];
			$arrReturn['status'] = $res->fields['status'];
			$arrReturn['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn['create_by'] = $res->fields['create_by'];
			return 	$arrReturn;
		}
		private function _saveRateExchange(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
			$user_id = $memberInfomation['id'];
			$rate = $base->get('POST.exchange');
			$exchange_id  = $base->get('POST.exchange_id');
			//echo $exchange_id;
			if($exchange_id>0){
				$sqlUp = "UPDATE web_rate SET status='C' WHERE 1=1";
				//echo $sqlUp;
				$resUp = $db->Execute($sqlUp);
				$sqlIn = "INSERT INTO web_rate(
									rate,
									status,
									create_dtm,
									create_by
									)VALUES(
									".GF::quote($rate).",
									'O',
									NOW(),
									".GF::quote($user_id)."
									)";
									//echo $resUp;
				$resIn = $db->Execute($sqlIn);
			}else{

				$sqlIn = "INSERT INTO web_rate(
									rate,
									status,
									create_dtm,
									create_by
									)VALUES(
									".GF::quote($rate).",
									'O',
									NOW(),
									".GF::quote($user_id)."
									)";
									//echo $sqlIn;
				$resIn = $db->Execute($sqlIn);
			}
			if($resIn){

				$sql = "SELECT * FROM web_order WHERE status='1'";
				$res = $db->Execute($sql);
				$arrReturn = array();
				$i = 0;
				while(!$res->EOF){
					//$arrReturn[$i]['id'] = $res->fields['id'];
					$this->_recalculateOrder($res->fields['id']);
					//$i++;
					$res->MoveNext();
				}
				$res->Close();


				return TRUE;
			}else{
				return FALSE;
			}
		}
        private function _saveRateExchange2(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
			$user_id = $memberInfomation['id'];
			$rate = $base->get('POST.exchange');
			$exchange_id  = $base->get('POST.exchange_id');
			//echo $exchange_id;
			if($exchange_id>0){
				$sqlUp = "UPDATE web_rate_2 SET status='C' WHERE 1=1";
				//echo $sqlUp;
				$resUp = $db->Execute($sqlUp);
				$sqlIn = "INSERT INTO web_rate_2(
									rate,
									status,
									create_dtm,
									create_by
									)VALUES(
									".GF::quote($rate).",
									'O',
									NOW(),
									".GF::quote($user_id)."
									)";
									//echo $resUp;
				$resIn = $db->Execute($sqlIn);
			}else{

				$sqlIn = "INSERT INTO web_rate_2(
									rate,
									status,
									create_dtm,
									create_by
									)VALUES(
									".GF::quote($rate).",
									'O',
									NOW(),
									".GF::quote($user_id)."
									)";
									//echo $sqlIn;
				$resIn = $db->Execute($sqlIn);
			}
			if($resIn){

				// $sql = "SELECT * FROM web_order WHERE status='1'";
				// $res = $db->Execute($sql);
				// $arrReturn = array();
				// $i = 0;
				// while(!$res->EOF){
				// 	//$arrReturn[$i]['id'] = $res->fields['id'];
				// 	$this->_recalculateOrder($res->fields['id']);
				// 	//$i++;
				// 	$res->MoveNext();
				// }
				// $res->Close();


				return TRUE;
			}else{
				return FALSE;
			}
		}
        private function _saveRateExchange3(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
			$user_id = $memberInfomation['id'];
			$rate = $base->get('POST.exchange');
			$exchange_id  = $base->get('POST.exchange_id');
			//echo $exchange_id;
			if($exchange_id>0){
				$sqlUp = "UPDATE web_rate_3 SET status='C' WHERE 1=1";
				//echo $sqlUp;
				$resUp = $db->Execute($sqlUp);
				$sqlIn = "INSERT INTO web_rate_3(
									rate,
									status,
									create_dtm,
									create_by
									)VALUES(
									".GF::quote($rate).",
									'O',
									NOW(),
									".GF::quote($user_id)."
									)";
									//echo $resUp;
				$resIn = $db->Execute($sqlIn);
			}else{

				$sqlIn = "INSERT INTO web_rate_3(
									rate,
									status,
									create_dtm,
									create_by
									)VALUES(
									".GF::quote($rate).",
									'O',
									NOW(),
									".GF::quote($user_id)."
									)";
									//echo $sqlIn;
				$resIn = $db->Execute($sqlIn);
			}
			if($resIn){

				// $sql = "SELECT * FROM web_order WHERE status='1'";
				// $res = $db->Execute($sql);
				// $arrReturn = array();
				// $i = 0;
				// while(!$res->EOF){
				// 	//$arrReturn[$i]['id'] = $res->fields['id'];
				// 	$this->_recalculateOrder($res->fields['id']);
				// 	//$i++;
				// 	$res->MoveNext();
				// }
				// $res->Close();


				return TRUE;
			}else{
				return FALSE;
			}
		}
		private function _recalculateOrder($odr_id){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$sql = "UPDATE web_order SET
										rate='".GF::getrate()."'
										WHERE id='".$odr_id."' ";
			$db->Execute($sql);

			$sql = "SELECT * FROM web_order_item WHERE order_id=".GF::quote($odr_id);
			$res = $db->Execute($sql);
	 		while(!$res->EOF){
				$sql2 = "UPDATE web_order_item SET
											product_price_thb=product_price_rmb*product_qty*".GF::getrate()."
											WHERE id='".$res->fields['id']."' ";
				$db->Execute($sql2);
				$res->MoveNext();
			}
			$res->Close();

		}
		private function _saveEcoupon(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];

            
            $agency_id = "1";
            if($memberInfomation['user_role_id']!='1'){
                $agency_id = intval($memberInfomation['id']);
            }

			$ecoupon_number = $base->get('POST.ecoupon_number');
			$ecoupon_amount = $base->get('POST.ecoupon_amount');
            $ecoupon_min = $base->get('POST.ecoupon_min');
            $ecoupon_limit = intval($base->get('POST.ecoupon_limit'));

			$start_date = '';
			if($base->get('POST.start_date')!=''){
				$start_date = date("Y-m-d",strtotime($base->get('POST.start_date')));
			}
			$end_date = '';
			if($base->get('POST.end_date')!=''){
				$end_date = date("Y-m-d",strtotime($base->get('POST.end_date')));
			}

			$mode = $base->get('POST.mode');
			if($mode=="createecoupon"){
				$sqlIn = "INSERT INTO project_ecoupon(
									e_no,
                                    agency_id,
									e_amount,
                                    e_min,
									e_type,
                                    e_limit,
									start_date,
									end_date,
                                    e_desc,
									status,
									create_dtm,
									create_by
									)VALUES(
                                    ".GF::quote($ecoupon_number).",
                                    ".GF::quote($agency_id).",
									".GF::quote($ecoupon_amount).",
                                    ".GF::quote($ecoupon_min).",
									".GF::quote($base->get('POST.e_type')).",
                                    ".GF::quote($ecoupon_limit).",
									".GF::quote($start_date).",
									".GF::quote($end_date).",
                                    ".GF::quote($base->get('POST.e_desc')).",
									'O',
									NOW(),
									".GF::quote($user_id)."
									)";
									//echo $sqlIn;
				$resIn = $db->Execute($sqlIn);
				if($resIn){
					return TRUE;
				}else{
					return FALSE;
				}
			}else if($mode=="editecoupon"){
				$e_id = $base->get('POST.e_id');
				$sql = "UPDATE project_ecoupon SET e_no=".GF::quote($ecoupon_number).",e_amount=".GF::quote($ecoupon_amount).",e_min=".GF::quote($ecoupon_min).",
						e_type=".GF::quote($base->get('POST.e_type')).",e_desc=".GF::quote($base->get('POST.e_desc')).",start_date=".GF::quote($start_date).",end_date=".GF::quote($end_date).",e_limit=".GF::quote($ecoupon_limit)."
								WHERE e_id=".GF::quote($e_id);
								//echo $sql;
				$res = $db->Execute($sql);
				if($res){
					return TRUE;
				}else{
					return FALSE;
				}
			}
		}
		private function _getEcouponList(){
			$base = Base::getInstance();
            $db = DB::getInstance();
             
            $member = new Member();
             
            $memberInfomation = $member->memberInfomation();
            
            $user_id = $memberInfomation['id'];
            
            $cond = '';
            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $cond .= " AND agency_id = ".intval($memberInfomation['id']);
            }
            if($memberInfomation['user_role_id']=='1'){
                $cond = '';
            }

	 		$sql = "SELECT * FROM project_ecoupon WHERE status='O' ".$cond." ORDER BY e_id DESC";
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();
	 		$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['e_id'] = $res->fields['e_id'];
				$arrReturn[$i]['e_no'] = $res->fields['e_no'];
				$arrReturn[$i]['e_amount'] = $res->fields['e_amount'];
                $arrReturn[$i]['e_min'] = $res->fields['e_min'];
                $arrReturn[$i]['e_type'] = $res->fields['e_type'];
                $arrReturn[$i]['e_limit'] = $res->fields['e_limit'];
                $arrReturn[$i]['agency_id'] = $res->fields['agency_id'];

                $arrReturn[$i]['start_date_time'] = strtotime($res->fields['start_date']);
                $arrReturn[$i]['end_date_time'] = strtotime($res->fields['end_date']);

                $arrReturn[$i]['start_date'] = date("d F Y",strtotime($res->fields['start_date']));
                if($res->fields['start_date']=='0000-00-00'){$arrReturn[$i]['start_date']='No selected.';$arrReturn[$i]['start_date_time']='';}
                $arrReturn[$i]['end_date'] = date("d F Y",strtotime($res->fields['end_date']));
                if($res->fields['end_date']=='0000-00-00'){$arrReturn[$i]['end_date']='No selected.';$arrReturn[$i]['end_date_time']='';}

                $arrReturn[$i]['e_desc'] = $res->fields['e_desc'];
				$arrReturn[$i]['status'] = $res->fields['status'];
				$arrReturn[$i]['create_dtm'] = date("d F Y",strtotime($res->fields['create_dtm']));
				$arrReturn[$i]['create_by'] = $res->fields['create_by'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			//GF::print_r($arrReturn);
			return 	$arrReturn;
		}
		private function _getEcoupon(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$e_id = $base->get('_ids');
	 		$sql = "SELECT * FROM project_ecoupon WHERE status='O' AND e_id=".GF::quote($e_id);
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();
			$arrReturn['e_id'] = $res->fields['e_id'];
			$arrReturn['e_no'] = $res->fields['e_no'];
			$arrReturn['e_amount'] = $res->fields['e_amount'];
            $arrReturn['e_min'] = $res->fields['e_min'];
			$arrReturn['status'] = $res->fields['status'];
			$arrReturn['e_type'] = $res->fields['e_type'];
            $arrReturn['e_desc'] = $res->fields['e_desc'];
            $arrReturn['e_limit'] = $res->fields['e_limit'];
			$arrReturn['start_date'] = '';
			if($res->fields['start_date']!='0000-00-00'){
				$arrReturn['start_date'] = date("d-m-Y",strtotime($res->fields['start_date']));
			}
			$arrReturn['end_date'] = '';
			if($res->fields['end_date']!='0000-00-00'){
				$arrReturn['end_date'] = date("d-m-Y",strtotime($res->fields['end_date']));
			}

			$arrReturn['status'] = $res->fields['status'];
			$arrReturn['create_dtm'] = date("d F Y",strtotime($res->fields['create_dtm']));
			$arrReturn['create_by'] = $res->fields['create_by'];

			//GF::print_r($arrReturn);
			return 	$arrReturn;

		}
		public function getEcouponByCode(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$e_id = $base->get('e_no');
	 		$sql = "SELECT * FROM project_ecoupon WHERE status='O' AND e_no=".GF::quote($e_id);
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();
			$arrReturn['e_id'] = $res->fields['e_id'];
			$arrReturn['e_no'] = $res->fields['e_no'];
			$arrReturn['e_amount'] = $res->fields['e_amount'];
			$arrReturn['status'] = $res->fields['status'];
            $arrReturn['e_type'] = $res->fields['e_type'];
            $arrReturn['e_min'] = $res->fields['e_min'];
            $arrReturn['e_limit'] = $res->fields['e_limit'];
            $arrReturn['start_date'] = $res->fields['start_date'];
            $arrReturn['end_date'] = $res->fields['end_date'];
			$arrReturn['status'] = $res->fields['status'];
			$arrReturn['create_dtm'] = date("d F Y",strtotime($res->fields['create_dtm']));
			$arrReturn['create_by'] = $res->fields['create_by'];
            $arrReturn['used_user'] = $this->_chackMemberCoupon($res->fields['e_id']);
            $arrReturn['used_total'] = $this->_chackUsedCoupon($res->fields['e_id']);
            $arrReturn['used'] = 'T';
            if($res->fields['e_limit']>0){
                if($arrReturn['used_user']>0){
                    $arrReturn['used'] = 'F';
                }else{
                    if($arrReturn['used_total']>=$res->fields['e_limit']){
                        $arrReturn['used'] = 'F';
                    }
                }
            }

			//GF::print_r($arrReturn);
			return 	$arrReturn;

		}
        private function _chackMemberCoupon($e_id){
            $base = Base::getInstance();
	 		$db = DB2::getInstance();

            $member = new Member();
			$memberInfomation = $member->memberInfomation();
			$user_id = $memberInfomation['id'];

            $result = $db->count("project_ecoupon_log",array("AND"=>array(
                                                                        "e_id"=>$e_id,
                                                                        "user_id"=>$user_id
                                                                        )));
            return $result;

        }
        private function _chackUsedCoupon($e_id){
            $base = Base::getInstance();
	 		$db = DB2::getInstance();

            $result = $db->count("project_ecoupon_log",array("AND"=>array(
                                                                        "e_id"=>$e_id
                                                                        )));
            return $result;

        }
		private function _delEcoupon(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$e_id = $base->get('POST.e_id');
	 		$sql = "UPDATE project_ecoupon SET status='C' WHERE e_id=".GF::quote($e_id);
			$res = $db->Execute($sql);
			if($res){
				return TRUE;
			}else{
				return FALSE;
			}
		}
		private function _checkEcoupon(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$e_no = $base->get('POST.e_no');
	 		$sql = "SELECT COUNT(*) AS num FROM project_ecoupon WHERE status='O' AND e_no=".GF::quote($e_no);
	 		//echo $sql;
			$res = $db->Execute($sql);

			if($res->fields['num']>0){
				return TRUE;
			}else{
				return FALSE;
			}
		}
		private function _uploadTemp(){
			$base = Base::getInstance();
			//GF::print_r($_FILES);
			if(!empty($_FILES['filetemp']['name'])){
				$picname = $_FILES['filetemp']['name'];

				$random = GF::randomNum(25);

				$type = substr(strrchr($picname, '.'), 1);
				$dest_picname_o = $base->get('BASEDIR')."/uploads/images/".$random.".".$type;

				$tmp_file = $_FILES['filetemp']['tmp_name'];
				@copy($tmp_file, $dest_picname_o);
				$temp_name = $random.".".$type;

				return $base->get('BASEURL')."/uploads/images/".$random.".".$type;
			}else{
				return 'F';
			}
		}
		private function _createFtpTemplate(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
			$user_id = $memberInfomation['id'];

	 		$sql = "INSERT INTO project_export_template (
												  template_name,
												  ftp_host,
												  ftp_port,
												  ftp_username,
												  ftp_password,
												  ftp_path,
												  status,
												  update_dtm,
												  update_by
												)VALUES(
													".GF::quote($base->get('POST.template_name')).",
													".GF::quote($base->get('POST.ftp_host')).",
													".GF::quote($base->get('POST.ftp_port')).",
													".GF::quote($base->get('POST.ftp_username')).",
													".GF::quote($base->get('POST.ftp_password')).",
													".GF::quote($base->get('POST.ftp_path')).",
													'O',
													NOW(),
													".GF::quote($user_id)."
												)";

			$res = $db->Execute($sql);
			if($res){
				$last_id = $db->Insert_ID();
				$tempFields = $base->get('POST.master_id');

				if(count($tempFields>0)){
					foreach($tempFields as $vals){
						$sql = "INSERT INTO project_export_template_item (
															  template_id,
															  master_id
															)VALUES(
																".GF::quote($last_id).",
																".GF::quote($vals)."
															)";

						$res = $db->Execute($sql);
					}
				}

			}
			return true;

		}
		private function _updateFtpTemplate(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
			$user_id = $memberInfomation['id'];


			$sql = "UPDATE project_export_template SET
														template_name=".GF::quote($base->get('POST.template_name')).",
														ftp_host=".GF::quote($base->get('POST.ftp_host')).",
														ftp_port=".GF::quote($base->get('POST.ftp_port')).",
														ftp_username=".GF::quote($base->get('POST.ftp_username')).",
														ftp_password=".GF::quote($base->get('POST.ftp_password')).",
														ftp_path=".GF::quote($base->get('POST.ftp_path'))."

														WHERE template_id=".GF::quote($base->get('POST.template_id'));
			$res = $db->Execute($sql);

			if($res){

				$sql = "DELETE FROM project_export_template_item WHERE template_id=".GF::quote($base->get('POST.template_id'));
				$res = $db->Execute($sql);

				$last_id = $base->get('POST.template_id');
				$tempFields = $base->get('POST.master_id');

				if(count($tempFields>0)){
					foreach($tempFields as $vals){
						$sql = "INSERT INTO project_export_template_item (
															  template_id,
															  master_id
															)VALUES(
																".GF::quote($last_id).",
																".GF::quote($vals)."
															)";

						$res = $db->Execute($sql);
					}
				}

			}
			return true;

		}

		private function _templateList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$sql = "SELECT * FROM project_export_template WHERE status='O'";
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();
	 		$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['template_id'] = $res->fields['template_id'];
				$arrReturn[$i]['template_name'] = $res->fields['template_name'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			return 	$arrReturn;
		}
		private function _templateInfo(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$e_id = $base->get('_ids');

	 		$sql = "SELECT * FROM project_export_template WHERE template_id=".GF::quote($e_id);
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();

			$arrReturn['template_id'] = $res->fields['template_id'];
			$arrReturn['template_name'] = $res->fields['template_name'];
			$arrReturn['ftp_host'] = $res->fields['ftp_host'];
			$arrReturn['ftp_username'] = $res->fields['ftp_username'];
			$arrReturn['ftp_password'] = $res->fields['ftp_password'];
			$arrReturn['ftp_port'] = $res->fields['ftp_port'];
			$arrReturn['ftp_path'] = $res->fields['ftp_path'];
			$arrReturn['data'] = $this->_templateDataList();

			//GF::print_r($arrReturn);
			return 	$arrReturn;

		}
		private function _templateDataList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$sql = "SELECT * FROM project_export_template_item WHERE template_id=".GF::quote($base->get('_ids'));
	 		$res = $db->Execute($sql);
	 		$arrReturn = array();
	 		$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['master_id'] = $res->fields['master_id'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			return 	$arrReturn;
		}
		private function _deleteFtpTemplate(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
			$user_id = $memberInfomation['id'];


			$sql = "UPDATE project_export_template SET status='C' WHERE template_id=".GF::quote($base->get('_ids'));
			$res = $db->Execute($sql);


			return true;

		}
		private function _addrate(){
			$base 	= Base::getInstance();
            $db 	= DB::getInstance();
            $db2 	= DB2::getInstance();

            $checkDumplicate = $this->_checkRateList();
            if($checkDumplicate!=''){return false;exit();}

            $member = new Member();


            $memberInfomation = $member->memberInfomation();

            $user_id = $memberInfomation['id'];
            
            $getship_by = $db2->get("project_master","*",array("id"=>$base->get('POST.order_type')));
            $shipCode = $getship_by['master_value'];
            if($shipCode=='G'){
                $shipCode = '';
            }

			//Add Weerasak 13/02/2565
			$getShip_type = $db2->get("project_master","*",array("id"=>$base->get('POST.ship_by')));
            $shipType = $getShip_type['master_value'];
			//END
            $getunit = $db2->get("project_master","*",array("id"=>$base->get('POST.unit')));
            $unitCode = $getunit['master_value'];

			//Add Weerasak 13/02/2565
			if($shipType == 'Sea'){
				$shipCode = $shipCode.$shipType;
			}
			//END

            $unitCodeValue = $unitCode.$shipCode;

            $sql ="INSERT INTO project_user_rate(
                                                    user_id,
                                                    order_type,
                                                    ship_by,
                                                    unit,
                                                    unit_code,
                                                    rate,
                                                    create_dtm,
                                                    create_by
                                                    )
                                                    VALUES(
                                                    ".GF::quote($base->get('POST.user_id')).",
                                                    ".GF::quote($base->get('POST.order_type')).",
                                                    ".GF::quote($base->get('POST.ship_by')).",
                                                    ".GF::quote($base->get('POST.unit')).",
                                                    ".GF::quote($unitCodeValue).",
                                                    ".GF::quote($base->get('POST.rate')).",
                                                    ".GF::quote($user_id).",
                                                    NOW()
                                                    )";
            $res = $db->Execute($sql);
            return true;
	    }
      private function  _checkRateList(){
			$base 	= Base::getInstance();
	 		$db 	= DB::getInstance();



			$sql = "SELECT id FROM project_user_rate WHERE user_id=".$base->get('POST.user_id')."  AND order_type=".$base->get('POST.order_type')." AND ship_by=".$base->get('POST.ship_by')." AND unit=".$base->get('POST.unit')." LIMIT 1";
			$res = $db->Execute($sql);


			return 	$res->fields['id'];
		}
		private function  _rateUserList(){
			$base 	= Base::getInstance();
	 		$db 	= DB::getInstance();



			$sql = "SELECT * FROM project_user_rate WHERE user_id=".$base->get('_ids')."  ORDER BY id ASC";
			$res = $db->Execute($sql);

			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['id'] 			= $res->fields['id'];
				$arrReturn[$i]['order_type'] 	= $res->fields['order_type'];
				$arrReturn[$i]['ship_by'] 		= $res->fields['ship_by'];
				$arrReturn[$i]['unit'] 			= $res->fields['unit'];
                $arrReturn[$i]['rate'] 			= $res->fields['rate'];
                $arrReturn[$i]['unit_code'] 	= $res->fields['unit_code'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			//GF::print_r($arrReturn);
			return 	$arrReturn;
		}
		
		
		private function  _deleteRate(){
			$base 	= Base::getInstance();
	 		$db 	= DB::getInstance();

	 		$sql = "DELETE FROM project_user_rate WHERE id=".GF::quote($base->get('GET.id'));
	 		$res = $db->Execute($sql);


		}
        private function  _getGeneralSetting(){
			$base 	= Base::getInstance();
            $db 	= DB::getInstance();
             
            $member = new Member();
            $memberInfomation = $member->memberInfomation();

            $cond = " AND agency_id='1'";
            if($memberInfomation['user_role_id']!='1'){
                $cond = " AND agency_id=".intval($memberInfomation['id']);
            }

            $gen_id = $base->get('genid');

			$sql = "SELECT * FROM project_general_setting WHERE gen_id=".GF::quote($gen_id).$cond;
			$res = $db->Execute($sql);

			return 	$res->fields;
        }
        private function  _getGeneralSettingByuser(){
			$base 	= Base::getInstance();
            $db 	= DB::getInstance();
             
            $member = new Member();
            $memberInfomation = $member->memberInfomation();

            $cond = " AND agency_id='1'";

            if($memberInfomation['user_role_id']!='1' && $memberInfomation['user_role_id']!=''){
                $cond = " AND agency_id=".intval($memberInfomation['ref_id']);
                if($memberInfomation['ref_id']=='' || $memberInfomation['ref_id']=='0' ){
                    $cond = " AND agency_id='1'";
                }
            }

            if($base->get('GET.ref_id')!=''){
                $cond = " AND agency_id=".intval($base->get('GET.ref_id'));
            }



            $gen_id = $base->get('genid');

			$sql = "SELECT * FROM project_general_setting WHERE gen_id=".GF::quote($gen_id).$cond;
			$res = $db->Execute($sql);

			
			return 	$res->fields;
		}
        private function _updateGeneral(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
			$user_id = $memberInfomation['id'];

            $field_name = $base->get('field_name');

			$sql = "UPDATE project_general_setting SET ".$field_name."=".GF::quote($base->get('field_value'))." WHERE gen_id=".GF::quote($base->get('gen_id'));
			$res = $db->Execute($sql);


			return true;

        }
        private function _updateGeneralContent(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
			$memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];

            $cond = " AND agency_id='1'";
            $agency_id = "1";
            if($memberInfomation['user_role_id']!='1'){
                $cond = " AND agency_id=".intval($memberInfomation['id']);
                $agency_id = intval($memberInfomation['id']);
            }

            $field_name = $base->get('field_name');

            $sqlCheck = "SELECT at_id FROM project_general_setting WHERE gen_id=".GF::quote($base->get('gen_id')).$cond;
            $resCheck = $db->Execute($sqlCheck);
            if($resCheck->fields['at_id']==''){
                $sql = "INSERT INTO project_general_setting(
                                                            agency_id,
                                                            gen_id,
                                                            ".$field_name."
                                                            ) VALUES(
                                                               ".GF::quote($agency_id).",
                                                               ".GF::quote($base->get('gen_id')).",
                                                               ".GF::quote($base->get('field_value'))."
                                                            )";
                $db->Execute($sql);
            }else{
                $sql = "UPDATE project_general_setting SET ".$field_name."=".GF::quote($base->get('field_value'))." WHERE gen_id=".GF::quote($base->get('gen_id')).$cond;
                $res = $db->Execute($sql);
            }

			


			return true;

		}
		
		private function _syncList(){
			$base 	= Base::getInstance();
	 		$db 	= DB::getInstance();
	 		
			$sql = "SELECT * FROM project_webservice WHERE  1=1 ORDER BY id DESC";
			$res = $db->Execute($sql);
			$count = 0;
				while(!$res->EOF){
					$count++;
					$res->MoveNext();
				}
				$res->Close();

				$base->set('allPage',ceil($count/20));

			$page = $base->get('GET.page');

			$numr = 20;
			$start = "";
			$end = "";
			if($page==1||empty($page)){
				$start = 0;
				$page = 1;
				//$end = $numr;
			}else{
				//$end = ($page*$numr);
				$start = ($page*$numr)-$numr;
			}
			 $condNum = " LIMIT ".$start.",".$numr;

			 $base->set('currpage',$page);

			$res = $db->Execute($sql.$condNum);
			$i=0;
			$ArrList = array();
			while(!$res->EOF){
				$ArrList[$i] = $res->fields;
				$i++;
				$res->MoveNext();
			}

			//print_r();
			//$dtatReturn['data'] = $ArrList;

			return $ArrList;
        }
        
        private function _agencyList(){
			$base = Base::getInstance();
	 		$db = DB2::getInstance();
	 		
            $role = $db->select("project_role",array("id"),array("email_account"=>'A'));
            $dataRole = array();
            foreach($role as $vals){
                $dataRole[] = $vals['id'];
            }

            $result = $db->select("project_user",array("id","user_code","user_name"),
                                array("AND"=>
                                    array(
                                        "user_role_id"=>$dataRole,
                                        "active_status"=>'O'
                                    )
                                ));

			return $result;

        }
        private function _insertBank(){
			$base = Base::getInstance();
	 		$db = DB2::getInstance();
	 		
            $member = new Member();
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];

            //GF::print_r($memberInfomation);

            
            $agency_id = "1";
            if($memberInfomation['user_role_id']!='1'){
                $agency_id = intval($memberInfomation['id']);
            }elseif ($memberInfomation['user_role_id']=='1' || $memberInfomation['position_name']=="Owner" ) {
            	$agency_id = intval($memberInfomation['id']);
            }
            //exit();

            $result = $db->insert("project_bank",array(
                                                       "master_id"=>$base->get('POST.master_id'),
                                                       "bank_desc"=>$base->get('POST.bank_desc'),
                                                       "bank_type"=>$base->get('POST.bank_type'),
                                                       "agency_id"=>$agency_id,
                                                       "status"=>'O',
                                                       "create_dtm"=>date('Y-m-d H:i:s'),
                                                       "create_by"=>$user_id
                                                    ));

			return $result;

        }
        private function _updateBank(){
			$base = Base::getInstance();
	 		$db = DB2::getInstance();
	 		

            $result = $db->update("project_bank",array(
                                                       "master_id"=>$base->get('POST.master_id'),
                                                       "bank_desc"=>$base->get('POST.bank_desc'),
                                                       "bank_type"=>$base->get('POST.bank_type')
                                                    ),array("bank_id"=>$base->get('POST.bank_id')));

			return $result;

        }
        private function _bankList(){
			$base = Base::getInstance();
            //$db = DB2::getInstance();
			$db = DB::getInstance();

            $member = new Member();
            $memberInfomation = $member->memberInfomation();

            //$cond = array();
           // $cond = array("status[!]"=>'D');
            if($memberInfomation['user_role_id']!='1'){
               // $cond = array("status[!]"=>'D',"agency_id"=>$memberInfomation['id']);
				
				$cond = " AND b.agency_id=".GF::quote($memberInfomation['id']);
            }
	 		
			
          /*$result = $db->select("project_bank","*",array(
                                                            "AND"=>$cond    
                                                            ));*/
			$sql = "SELECT b.*, m.master_name, m.master_value, m.master_char FROM project_bank b 
			LEFT JOIN project_master m ON m.id=b.master_id
			WHERE b.status!='D' AND m.master_char='Y' ".$cond." ORDER BY b.bank_id DESC";				
											
			$res = $db->Execute($sql);	
			
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['bank_id'] 			= $res->fields['bank_id'];
                $arrReturn[$i]['master_id'] 	= $res->fields['master_id'];
                $arrReturn[$i]['bank_type'] 	= $res->fields['bank_type'];
				$arrReturn[$i]['bank_desc'] 		= $res->fields['bank_desc'];
				$arrReturn[$i]['agency_id'] 			= $res->fields['agency_id'];
				$arrReturn[$i]['master_value'] 			= $res->fields['master_value'];
				$arrReturn[$i]['master_char'] 			= $res->fields['master_char'];
				
				
                
				$i++;
				$res->MoveNext();
			}
			$res->Close();										
											
			//return $result;
			return $arrReturn;

        }
        private function _bankInfo(){
			$base = Base::getInstance();
            $db = DB2::getInstance();


            $result = $db->get("project_bank","*",array("bank_id"=>$base->get('_ids')));

			return $result;

        }
        private function _deleteBank(){
			$base = Base::getInstance();
            $db = DB2::getInstance();

            $result = $db->update("project_bank",array("status"=>'D'),array("bank_id"=>$base->get('_ids')));

			return $result;

        }
        private function _manualList(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();


	 		$member = new Member();


	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];



	 		$cond_select = '';
	 		
            
            

			$sql = "SELECT * FROM project_content_data WHERE status!='D' ORDER BY create_dtm DESC";

			//echo $sql;
			$res = $db->Execute($sql);
			$count = $res->NumRows();

				$base->set('allPage',ceil($count/10));

			$page = $base->get('GET.page');

			$numr = 10;
			$start = "";
			$end = "";
			if($page==1||empty($page)){
				$start = 0;
				$page = 1;
				//$end = $numr;
			}else{
				//$end = ($page*$numr);
				$start = ($page*$numr)-$numr;
			}
			 $cond = " LIMIT ".$start.",".$numr;

			 $base->set('currpage',$page);

			$res = $db->Execute($sql.$cond);
			$arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i]['content_id'] = $res->fields['content_id'];
				$arrReturn[$i]['content_name'] = $res->fields['content_name_'.GF::langdefault()];
				$arrReturn[$i]['content_desc'] = $res->fields['content_desc_'.GF::langdefault()];
                $arrReturn[$i]['sort'] = $res->fields['sort'];
                $arrReturn[$i]['status'] = $res->fields['status'];
                $arrReturn[$i]['category_id'] = $res->fields['category_id'];
				$arrReturn[$i]['update_dtm'] = $res->fields['update_dtm'];
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			//GF::print_r($arrReturn);
			return 	$arrReturn;


        }
        private function _createContent(){
			$base = Base::getInstance();
 			$db = DB::getInstance();
 			$member = new Member();

 			$memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];
            

 			$sql = "INSERT INTO project_content_data (
													  status,
													  create_dtm,
													  create_by,
													  update_dtm,
													  update_by
													)VALUES(
                                                        'O',
														NOW(),
														".GF::quote($user_id).",
														NOW(),
														".GF::quote($user_id)."
													)";

			$res = $db->Execute($sql);
 			$content_id = $db->Insert_ID();

 			//$langlist = GF::langlist();

 			//$random_number = GF::randomNumb(20);
 			$vals['lang_prefix'] = 'th';

            $lang_prefix = $vals['lang_prefix'];

            $sql = "UPDATE project_content_data SET

                                                    content_name_".$lang_prefix."=".GF::quote($base->get('POST.content_name_'.$lang_prefix)).",
                                                    content_desc_".$lang_prefix."=".GF::quote($base->get('POST.content_desc_'.$lang_prefix)).",
                                                    category_id=".GF::quote($base->get('POST.category_id'))."
                                                    WHERE content_id=".GF::quote($content_id);
            ////echo $sql;
            $res = $db->Execute($sql);

 			return true;


		}

		private function _updateContent(){


			$base = Base::getInstance();
 			$db = DB::getInstance();
 			$member = new Member();
 			//GF::print_r($base->get('POST'));

 			$memberInfomation = $member->memberInfomation();
 			$user_id = $memberInfomation['id'];

 			$content_id = $base->get('POST.content_id');


 			$vals['lang_prefix'] = 'th';

            $lang_prefix = $vals['lang_prefix'];

            $sql = "UPDATE project_content_data SET

                                                    content_name_".$lang_prefix."=".GF::quote($base->get('POST.content_name_'.$lang_prefix)).",
                                                    content_desc_".$lang_prefix."=".GF::quote($base->get('POST.content_desc_'.$lang_prefix)).",
                                                    category_id=".GF::quote($base->get('POST.category_id')).",
                                                    update_dtm=NOW(),
                                                    update_by=".GF::quote($user_id)."
                                                    WHERE content_id=".GF::quote($content_id);
            ////echo $sql;
            $res = $db->Execute($sql);

 			return true;


        }
        private function _updateStatusContent(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

	 		$member = new Member();


	 		$memberInfomation = $member->memberInfomation();

	 		$user_id = $memberInfomation['id'];

	 		$sql = "UPDATE project_content_data SET
	 												status=".GF::quote($base->get('GET.status')).",
	 												update_dtm=NOW(),
	 												update_by=".GF::quote($user_id)."
	 												WHERE content_id=".GF::quote($base->get('GET.id'));
	 		////echo $sql;
	 		$res = $db->Execute($sql);
	 		if($res){
				echo('T');
			}
	 	}

	 	private function _contentInfomation(){
	 		$base = Base::getInstance();
	 		$db = DB::getInstance();
	 		$member = new Member();
	 		$content_id = $base->get('_ids');
	 		//echo $content_id;
	 		//echo $token;
	 		if($content_id!=''){
				$sql = "SELECT * FROM project_content_data WHERE content_id=".GF::quote($content_id);


				$db->SetFetchMode(ADODB_FETCH_ASSOC);
				$res = $db->Execute($sql);
				$base->set('user_id',$res->fields['create_by']);
				$res->fields['create'] = $member->memberInfomationByID();

				return $res->fields;
			}else{
				return NULL;
			}

        }
        private function  _getEmailTemplate(){
			$base 	= Base::getInstance();
            $db 	= DB::getInstance();
             
            $member = new Member();
            $memberInfomation = $member->memberInfomation();

            $cond = " AND agency_id='1'";
            if($memberInfomation['user_role_id']!='1'){
                $cond = " AND agency_id=".intval($memberInfomation['id']);
            }

            $gen_id = $base->get('genid');

            $sql = "SELECT COUNT(email_id) AS ttl FROM project_email_template WHERE 1=1 ".$cond;
            $res = $db->Execute($sql);

            if($res->fields['ttl']=='0'){
                $sql = "SELECT * FROM project_email_template WHERE agency_id='0' ";
                $res = $db->Execute($sql);
                while(!$res->EOF){
                    $sqlInsert = "INSERT INTO project_email_template(
                                                                    agency_id,
                                                                    gen_id,
                                                                    gen_name,
                                                                    gen_field_1,
                                                                    gen_field_2,
                                                                    status
                                                                    )VALUES(
                                                                        ".GF::quote($memberInfomation['id']).",
                                                                        ".GF::quote($res->fields['gen_id']).",
                                                                        ".GF::quote($res->fields['gen_name']).",
                                                                        ".GF::quote($res->fields['gen_field_1']).",
                                                                        ".GF::quote($res->fields['gen_field_2']).",
                                                                        ".GF::quote($res->fields['status'])."
                                                                    )";
                    $db->Execute($sqlInsert);
                    $res->MoveNext();
                }
                $res->Close();
            }

			$sql = "SELECT * FROM project_email_template WHERE status='Y' ".$cond;
			$res = $db->Execute($sql);
            $arrReturn = array();
			$i = 0;
			while(!$res->EOF){
				$arrReturn[$i] = $res->fields;
				$i++;
				$res->MoveNext();
			}
			$res->Close();
			return 	$arrReturn;
        }
        private function _updateEmailTemplate(){
			$base = Base::getInstance();
	 		$db = DB::getInstance();

            $data = $base->get('POST');
            if(count($data['gen_field_2'])>0){
                foreach($data['gen_field_2'] as $key=>$vals){
                    $sql = "UPDATE project_email_template SET
                                                            gen_field_2=".GF::quote($vals).",
                                                            gen_field_1=".GF::quote($data['gen_field_1'][$key])."
                                                            WHERE email_id=".GF::quote($key);
                    ////echo $sql;
                    $res = $db->Execute($sql);
                }
            }

	 	}
		
		private function _updateUserRate(){
			
			$base = Base::getInstance();
	 		$db = DB::getInstance();
			
			$groupID = $base->get('POST.gId');
			
			
			
			$sql = "SELECT * FROM project_user WHERE user_role_id=".$groupID;
			
			
			
			
			$res = $db->Execute($sql);
			
	 		$arrReturn = array();
			$dataInArr = array();
			$i=0;
			while(!$res->EOF){
				
				
				$dataInArr['user_code'][$i] = $res->fields['user_code'];				
				
				$i++;
				$res->MoveNext();
			}
			
			$res->Close();
			
			$sql2 = "SELECT u.user_code,r.* FROM project_user u
			INNER JOIN project_user_rate r ON r.user_id = u.user_role_id
			WHERE u.user_role_id = " .GF::quote($base->get('POST.gId'));
			$res2 = $db->Execute($sql2);

			$j=0;
				while(!$res2->EOF){
					
						$dataInArr['user_data_rate'][$j]['rate_user_code'] 		= $res2->fields['user_code'];
						$dataInArr['user_data_rate'][$j]['rate_goods_code'] 	= $res2->fields['unit_code'];
						$dataInArr['user_data_rate'][$j]['rate_price'] 			= $res2->fields['rate'];
						$dataInArr['user_data_rate'][$j]['rate_status'] 			= 1;
					
					$j++;	
					
					$res2->MoveNext();		
				}
				
				$res2->Close();
			
			
			
			$data = json_encode($dataInArr);
			
			  $randomString = substr(str_shuffle("AabBcCDd123456789012345678901234567890123456789012345678901234567890"), 0, 20);
			
			  $sqlInsert = "INSERT INTO project_webservice(id, 
								service_token, 
								service_method, 
								service_data,
								`status`, 
								create_dtm, 
								update_dtm)
								VALUES(NULL, 
								".GF::quote($randomString).", 
								".GF::quote('SYNC_MEMBER_RATE').", 
								".GF::quote($data).", 
								".GF::quote('N').", 
								".GF::quote(date('Y-m-d H:i:s')).", 
								".GF::quote(date('Y-m-d H:i:s')).")";
			
			
            $db->Execute($sqlInsert);
			
			//$SQL = "UPDATE project_webservice SET service_data=".GF::quote($data)." WHERE service_token=".GF::quote($randomString);
			
			//$db->Execute($SQL);
			//GF::print_r($SQL);
			
			
		}

	}
?>
