<?php
class SRegister{
	public function saveRegister(){
		$resultreturn = $this->_saveRegister();
		return $resultreturn;
	}
	public function confermclasslogin(){
		$resultreturn = $this->_confermclasslogin();
		return $resultreturn;
	}
   public function provinceList(){
		$resultreturn = $this->_provinceList();
		return $resultreturn;
	}
   public function amphurList(){
		$resultreturn = $this->_amphurList();
		return $resultreturn;
	}
   public function districtList(){
		$resultreturn = $this->_districtList();
		return $resultreturn;
	}
    public function resetPassword(){
		$resultreturn = $this->_resetPassword();
		return $resultreturn;
	}



	private function _saveRegister(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$mail = Mailer::getInstance();
 		$email		=	$base->get('POST.email-register');
 		$pass		=	md5($base->get('POST.pass-register'));
        $name		=	$base->get('POST.name-register');
        $nickname	=	$base->get('POST.nickname-register');
 		$tel		=	$base->get('POST.tel-register');
 		$adress		=	$base->get('POST.adress-register');
 		$province	=	$base->get('POST.province-register');
      	$amphur		=	$base->get('POST.amphur-register');
      	$district	=	$base->get('POST.district-register');
 		$post		=	$base->get('POST.post-register');
 		$line		=	$base->get('POST.line-register');
        $bookbank	=	$base->get('POST.bookbank-register');
        $birthdate	=	$base->get('POST.birth-date');

        if($base->get('POST.ref_id')==0 || $base->get('POST.ref_id')==""){
        	$group		=	$this->_getAgencyRole(0);
        	$ref_id	=	1;
        }else {
        	$ref_id	=	$base->get('POST.ref_id');
        	$group		=	$this->_getAgencyRole($ref_id);
        }
 		


 		$token = GF::randomStr(25);
 		$lineMail	=	$line;
 		$bookbankMail	=	$bookbank;
 		if(empty($line)){
			$lineMail = "-";
		}
		if(empty($bookbank)){
			$bookbankMail = "-";
		}


 		
 		$sql = "INSERT INTO project_user(
 										user_username,
 										ref_id,
 										user_email,
 										user_password,
 										user_name,
 										user_nickname,
 										user_address,
 										user_province,
                              			province,
                              			amphur,
                              			district,
 										user_post,
 										user_line_id,
 										user_bookbank,
 										user_thumb,
 										position_id,
 										user_birthdate,
 										user_start_date,
 										user_phone_number,
 										user_role_id,
 										user_token_mail,
 										personal_leave,
 										sick_leave,
 										vacation_leave,
 										early_leave,
 										status,
 										other_status,
 										active_status,
 										user_token,
 										create_dtm,
 										create_by,
 										update_dtm,
 										update_by
 										)VALUES(
 										".GF::quote($email).",
 										".GF::quote($ref_id).",
 										".GF::quote($email).",
 										".GF::quote($pass).",
 										".GF::quote($name).",
 										".GF::quote($nickname).",
 										".GF::quote($adress).",
 										".GF::quote($province).",
                              			".GF::quote($province).",
                              			".GF::quote($amphur).",
                              			".GF::quote($district).",
 										".GF::quote($post).",
 										".GF::quote($line).",
 										".GF::quote($bookbank).",
 										'',
 										'1',
 										".GF::quote($birthdate).",
 										NOW(),
 										".GF::quote($tel).",
 										".GF::quote($group).",
 										".GF::quote($token).",
 										'',
 										'',
 										'',
 										'',
 										'E',
 										'',
 										'O',
 										'',
 										NOW(),
 										'1',
 										NOW(),
 										'1'
 										)";
 										//echo $sql;

 										//exit();
		$res = $db->Execute($sql);
		$role_id = $db->Insert_ID();
		if($res){

            $ppv = GF::cvprovince($province);
			$dateNow = date('d F Y');
			$BASEURL=$base->get('BASEURL');
			$mailHTML = "";
			$mailHTML .= "<div style=\"border: 1px solid #ccc; width: 80%;\">
							<div style=\"width: 100%; padding-top: 15px; padding-bottom: 10px;  background-color: #FFF;;\">
								<img src=\"$BASEURL/assets/images/logo-home.png\" style=\"margin-left: 46%;\" />
							</div>
							<br/>
							<span style=\"font-size: 18px; margin-left: 10px;\"><strong>สวัสดี คุณ :</strong> $name  </span><br/><br/>
							<span style=\"font-size: 18px; margin-left: 10px;\">ชื่อผู้ใช้งาน : <strong>$email</strong></span><br/>
							<span style=\"font-size: 18px; margin-left: 10px;\">เบอร์โทรศัพท์ : <strong> $tel</strong></span><br/>
							<span style=\"font-size: 18px; margin-left: 10px;\">ที่อยู่ :<strong> $adress </strong></span><br/>
							<span style=\"font-size: 18px; margin-left: 10px;\">จังหวัด :<strong> $ppv </strong></span><br/>
							<span style=\"font-size: 18px; margin-left: 10px;\">รหัสไปรษณีย์ :<strong> $post </strong></span><br/>
							<span style=\"font-size: 18px; margin-left: 10px;\">LINE ID :<strong> $lineMail </strong></span><br/>
                     <br/>
							<br/>
							<span style=\"font-size: 18px; margin-left: 10px;\"><strong>ยืนยันการสมัครเพื่อเข้าสู่ระบบ</strong></span><br/>
							<div style=\"width: 100%; text-align:center;\">
									คลิก <a href=\"$BASEURL/register/confermlogin/?token=$token&member=$role_id\" target=\"_blank\" >$BASEURL/register/confermlogin/?token=$token&member=$role_id</a>
							</div><br/><br/>
						<div style=\"width: 100%;  padding-top: 10px; color: #000;   padding-bottom: 10px;  background-color: #FFF;\">
								<span style=\"font-size: 14px; margin-left: 10px;\">ขอบคุณค่ะ</span><br/>
								<span style=\"font-size: 14px; margin-left: 10px;\">TAOBAO CHINA CARGO</span><br/>
								<span style=\"font-size: 14px; margin-left: 10px;\">$dateNow.</span>
						</div>
						</div>";

            $dataReplace = array(
                "{CUST_NAME}"=>$name,
                "{CUST_MAIL}"=>$email,
                "{LINK}"=>$BASEURL.'/register/confermlogin/?token='.$token.'&member='.$role_id
            );
	 		$mail->Subject = GF::mailsubject('1',$ref_id);
			$mail->msgHTML = GF::mailtemplate('1',$dataReplace,$ref_id);
			$mail->addAddressEmail = $email;
			$mail->addAddressName = $name;
			$mail->sendMail();
			return TRUE;
		}else{
			return FALSE;
		}
 	}
 	private function _confermclasslogin(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$db2 = DB2::getInstance();
		$member_id = $base->get('GET.member');
		$token = $base->get('GET.token');
		$sql = "SELECT COUNT(*) AS total FROM project_user WHERE id=".GF::quote($member_id)." AND user_token_mail=".GF::quote($token);
		$db->SetFetchMode(ADODB_FETCH_ASSOC);
		$res = $db->Execute($sql);
		if($res->fields['total']>0){
			$datauser = $db2->get("project_user","*",array("id"=>$member_id));

			//GF::print_r($datauser);
			$membercode = GF::genmembercode("MEMBER_ORDER",$datauser['ref_id']);


			//exit();
			$sql = "UPDATE project_user SET user_token_mail='',user_code='".$membercode."'  WHERE id=".GF::quote($member_id);
			$res = $db->Execute($sql);
			
			
			$dataSet = $db2->get("project_user",array(
			  										    "user_code(user_code)",
			  										    "user_email(user_email)",
			  										    "user_name(user_name)",
			  										    "user_nickname(user_surname)",
			  										    "user_address(user_address)",
			  										    "user_phone_number(user_tel)",
			  										    "create_dtm(user_datecreate)"
			  											),array(
			  											"user_code"=>$membercode
			  											));
			$addressdata = $db2->get("project_user","*",array("id"=>$member_id));
			
			$dst = $db2->get("project_thai_district","*",array("DISTRICT_ID"=>$addressdata['district']));
			$amp = $db2->get("project_thai_amphur","*",array("AMPHUR_ID"=>$addressdata['amphur']));
			$prv = $db2->get("project_thai_province","*",array("PROVINCE_ID"=>$addressdata['provicne']));
			
			$dataSet['user_address'] = $dataSet['user_address']." ".$dst['DISTRICT_NAME']." ".$amp['AMPHUR_NAME']." ".$prv['PROVINCE_NAME']." ".$addressdata['user_post'];
			
			$dataSet['user_group'] = 'TCC';
			$dataSet['user_role'] = 'member';
			$dataSet['user_type'] = 'order';
			$dataSet['user_status'] = '1';
            
            $role_id = $this->_getAgencyRole($addressdata['ref_id']);

            $resultRate = $db2->select("project_user_rate","*",array("user_id"=>$role_id)); 
            $dataArrSet = array();
            $x=0;
            foreach($resultRate as $dataArr){
                //$dataMaster = $db2->get("project_master","*",array("id"=>$dataArr['unit']));
                $dataArrSet[$x]['rate_user_code'] = $addressdata['user_code'];
                $dataArrSet[$x]['rate_goods_code'] = $dataArr['unit_code'];
                $dataArrSet[$x]['rate_price'] = $dataArr['rate'];
                $dataArrSet[$x]['rate_status'] = '1';
                $x++;
            }
			$dataSet['user_data_rate'] = $dataArrSet;								
			  											
			$db2->insert("project_webservice",array(
														"service_token"=>GF::randomStr(20),
														"service_method"=>"ADD_MEMBER",
														"service_data"=>json_encode($dataSet),
														"status"=>'N',
														"create_dtm"=>date('Y-m-d H:i:s'),
														"update_dtm"=>date('Y-m-d H:i:s')
														));
            $members = GF::memberinfo($member_id);
            $dataReplace = array(
                "{CUST_NAME}"=>$members['user_name'],
                "{CUST_MAIL}"=>$members['user_email'],
                "{LINK}"=>$base->get('BASEURL')
            );

            $mail = Mailer::getInstance();
            $mail->userid = $member_id;
            $mail->Subject = GF::mailsubject('13',$members['ref_id']);
            $mail->msg = GF::mailtemplate('13',$dataReplace,$members['ref_id']);

            
            $mail->SendingToNoti();

            $mail->msg = '';
            $mail->Subject = GF::mailsubject('13',$members['ref_id']);
            $mail->msgHTML = GF::mailtemplate('13',$dataReplace,$members['ref_id']);
			$mail->addAddressEmail = $members['user_name'];
            $mail->addAddressName = $members['user_email'];
            $mail->sendMail();

			return 'T';
		}else{
				return 'F';
		}
    }
    private function _getAgencyRole($ref_id){
		$base = Base::getInstance();
        $db = DB2::getInstance();

        $dataReturn = '2';
        $member_data = $db->get("project_user",array("user_role_id"),array("id"=>$ref_id));

        //GF::print_r($ref_id);

        if(intval($member_data['user_role_id'])>0){


            $data = $db->get("project_role",array("id"),array(
                "AND"=>array(
                    "agency_type"=>'O',
                    "agency_id"=>$ref_id,
                    "agency_default"=>'Y'
                )
            ));

          // GF::print_r($dataReturn);
            $dataReturn = $data['id'];
        }

        return $dataReturn;

    }
   private function _provinceList(){
		$base = Base::getInstance();
 		$db = DB2::getInstance();
        $resultReturn = $db->query("SELECT * FROM project_thai_province ORDER BY PROVINCE_NAME ASC")->fetchAll();
    //   $resultReturn = $db->select("project_thai_province","*",array(
                                                                    
    //                                                                 "ORDER"=>array("PROVINCE_ID"=>"ASC")
    //                                                             ));
    //                                                             echo $db->last_query();

      return $resultReturn;
	}
   private function _amphurList(){
		$base = Base::getInstance();
 		$db = DB2::getInstance();

         $set_id = $base->get('_set_id');
         $ids = $base->get('GET.province');
         if($set_id!=''){
            $ids = $set_id;
         }

      $resultReturn = $db->select("project_thai_amphur","*",array("PROVINCE_ID"=>$ids));

      return $resultReturn;
	}
   private function _districtList(){
		$base = Base::getInstance();
 		$db = DB2::getInstance();

         $set_id = $base->get('_set_id');
         $ids = $base->get('GET.amphur');
         if($set_id!=''){
            $ids = $set_id;
         }

      $resultReturn = $db->select("project_thai_district","*",array("AMPHUR_ID"=>$ids));

      return $resultReturn;
	}
    private function _resetPassword(){
		$base = Base::getInstance();
 		$db = DB2::getInstance();
 		$mail = Mailer::getInstance();

        $email = trim($base->get('GET.email'));

        $resultCheck = $db->get("project_user","*",array("user_email"=>$email));
 		
		if($resultCheck['id']!=''){

            $newPassword = GF::randomStr(10);
            $newPasswordHash = md5($newPassword);

            $name = $resultCheck['user_name'];

            $db->update("project_user",array("user_password"=>$newPasswordHash),array("id"=>$resultCheck['id']));

			$dateNow = date('d F Y');
			$BASEURL=$base->get('BASEURL');
			$mailHTML = "";
			$mailHTML .= "<div style=\"border: 1px solid #ccc; width: 100%;\">
							<div style=\"width: 100%; padding-top: 15px; padding-bottom: 10px;  background-color: #FFF;;\">
								<img src=\"$BASEURL/assets/images/logo-home.png\" style=\"margin-left: 46%;\" />
							</div>
							<br/>
							<span style=\"font-size: 18px; margin-left: 10px;\"><strong>สวัสดี คุณ :</strong> $name  </span><br/><br/>
							<span style=\"font-size: 18px; margin-left: 10px;\">ชื่อผู้ใช้งาน : $email</span><br/>
                            <span style=\"font-size: 18px; margin-left: 10px;\">รหัสผ่านใหม่ : <strong>$newPassword</strong></span><br/>
                            <span style=\"font-size: 18px; margin-left: 10px;\">เข้าสู่ระบบ : <a href='".$BASEURL."'>".$BASEURL."</a></span><br/>
                        <br/><br/>
						<div style=\"width: 100%;  padding-top: 10px; color: #000;   padding-bottom: 10px;  background-color: #FFF;\">
								<span style=\"font-size: 14px; margin-left: 10px;\">ขอบคุณค่ะ</span><br/>
								<span style=\"font-size: 14px; margin-left: 10px;\">TAOBAO CHINA CARGO</span><br/>
								<span style=\"font-size: 14px; margin-left: 10px;\">$dateNow.</span>
						</div>
						</div>";


	 		$mail->Subject = '[TAOBAO CHINA CARGO Mail]:: ขอรหัสผ่านใหม่ ';
			$mail->msgHTML = $mailHTML;
			$mail->addAddressEmail = $email;
			$mail->addAddressName = $name;
			$mail->sendMail();
			return TRUE;
		}else{
			return FALSE;
		}
 	}
}
?>
