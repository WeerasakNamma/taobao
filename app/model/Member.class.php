<?php
 class Member {





 	public function memberInfomation(){
		$resultreturn = $this->_memberIfomation();
		return $resultreturn;
	}
	public function memberInfomationByID(){
		$resultreturn = $this->_memberIfomationByID();
		return $resultreturn;
	}
	public function createMember(){
		$resultreturn = $this->_createMember();
		return $resultreturn;
	}
	public function updateMember(){
		$resultreturn = $this->_updateMember();
		return $resultreturn;
	}
	public function updateUserActiveStatus(){
		$resultreturn = $this->_updateUserActiveStatus();
		return $resultreturn;
	}
	public function checkUsername(){
		$resultreturn = $this->_checkUsername();
		return $resultreturn;
	}
	public function addRate(){
		$resultreturn = $this->_addrate();
		return $resultreturn;
	}
	public function rateUserList(){
		$resultreturn = $this->_rateUserList();
		return $resultreturn;
	}
	public function deleteRate(){
		$resultreturn = $this->_deleteRate();
		return $resultreturn;
	}
	public function saveWallet(){
		$resultreturn = $this->_saveWallet();
		return $resultreturn;
	}
	public function walletLog($status=NULL){
		$resultreturn = $this->_walletLog($status);
		return $resultreturn;
	}
	public function updateWithdraw(){
		$resultreturn = $this->_updateWithdraw();
		return $resultreturn;
	}
	public function withdrawList(){
		$resultreturn = $this->_withdrawList();
		return $resultreturn;
	}
	public function withdrawStatus(){
		$resultreturn = $this->_withdrawStatus();
		return $resultreturn;
	}
    public function withdrawComment(){
		$resultreturn = $this->_withdrawComment();
		return $resultreturn;
	}
	public function withdrawListAll(){
		$resultreturn = $this->_withdrawListAll();
		return $resultreturn;
	}
   public function withdrawImage(){
		$resultreturn = $this->_withdrawImage();
		return $resultreturn;
	}
	// Edit By Weerasak 06-01-2566
	public function refundDetail(){
		$resultreturn = $this->_refunDetail();
		return $resultreturn;
	}
	//END



 	private function _memberIfomation(){
 		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$cookie = $base->get('COOKIE');
 		$token = $cookie['token'];
 		//echo $token;
 		if($token!=''){

			$sql = "SELECT pu.*,pr.role_name,pr.email_account,pr.email_notification,pr.admin_group
						FROM project_user AS pu
						LEFT JOIN project_role AS pr ON(pr.id=pu.user_role_id)
						WHERE pu.user_token=".GF::quote($token);

			//$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);
			$arrReturn = array();
			$arrReturn['id'] = $res->fields['id'];
			$arrReturn['user_username'] = $res->fields['user_username'];
			$arrReturn['user_name'] = $res->fields['user_name'];
			$arrReturn['user_code'] = $res->fields['user_code'];
            $arrReturn['extra_code'] = $res->fields['extra_code'];
            $arrReturn['agency_code'] = $res->fields['agency_code'];
			$arrReturn['user_nickname'] = $res->fields['user_nickname'];
			$arrReturn['user_email'] = $res->fields['user_email'];
			$arrReturn['user_thumb'] = $res->fields['user_thumb'];
			$arrReturn['user_role_id'] = $res->fields['user_role_id'];
            $arrReturn['agency'] = $res->fields['email_account'];
            $arrReturn['ref_id'] = $res->fields['ref_id'];
            $arrReturn['email_notification'] = $res->fields['email_notification'];
            $arrReturn['admin_group'] = $res->fields['admin_group'];
			//$materdata = GF::masterdata($res->fields['position_id']);
			$arrReturn['position_name'] = $res->fields['role_name'];
			$arrReturn['user_start_date'] = $res->fields['user_start_date'];
			$arrReturn['sick_leave'] = $res->fields['sick_leave'];
			$arrReturn['personal_leave'] = $res->fields['personal_leave'];
			$arrReturn['vacation_leave'] = $res->fields['vacation_leave'];
			$arrReturn['early_leave'] = $res->fields['early_leave'];
			$arrReturn['position_id'] = $res->fields['position_id'];
			$arrReturn['user_birthdate'] = $res->fields['user_birthdate'];
			$arrReturn['user_phone_number'] = $res->fields['user_phone_number'];
            $arrReturn['status'] = $res->fields['status'];
            $arrReturn['update_status'] = $res->fields['update_status'];
			$arrReturn['other_status'] = $res->fields['other_status'];
			$arrReturn['wallet'] = $res->fields['wallet'];
			$arrReturn['user_address'] = $res->fields['user_address'];
			$arrReturn['user_province'] = $res->fields['user_province'];
			$arrReturn['user_post'] = $res->fields['user_post'];
			$arrReturn['user_line_id'] = $res->fields['user_line_id'];
			$arrReturn['user_bookbank'] = $res->fields['user_bookbank'];
            $arrReturn['province'] = $res->fields['province'];
            $arrReturn['amphur'] = $res->fields['amphur'];
            $arrReturn['district'] = $res->fields['district'];
			$arrReturn['bank_id'] = $res->fields['bank_id'];
			//GF::print_r($arrReturn);
			return $arrReturn;
		}else{
			return NULL;
		}

	}
	private function _memberIfomationByID(){
 		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$user_id = $base->get('user_id');
 		preg_match("/T/", $user_id, $output_array);
 		//echo $token;
 		if($user_id!=''){
			$sql = "SELECT pu.*,pr.role_name,pr.email_account,pr.admin_group
						FROM project_user AS pu
						LEFT JOIN project_role AS pr ON(pr.id=pu.user_role_id)
						WHERE pu.id=".GF::quote($user_id);

			if(count($output_array)>0){
				$sql = "SELECT pu.*,pr.role_name,pr.email_account,pr.admin_group
						FROM project_user AS pu
						LEFT JOIN project_role AS pr ON(pr.id=pu.user_role_id)
						WHERE pu.user_code=".GF::quote($user_id);
			}

            //$db->SetFetchMode(ADODB_FETCH_ASSOC);
            
			$res = $db->Execute($sql);
			$arrReturn = array();
			$arrReturn['id'] = $res->fields['id'];
			$arrReturn['user_username'] = $res->fields['user_username'];
			$arrReturn['user_name'] = $res->fields['user_name'];
			$arrReturn['user_code'] = $res->fields['user_code'];
            $arrReturn['extra_code'] = $res->fields['extra_code'];
            $arrReturn['agency_code'] = $res->fields['agency_code'];
			$arrReturn['role_name'] = $res->fields['role_name'];
			$arrReturn['user_nickname'] = $res->fields['user_nickname'];
			$arrReturn['user_email'] = $res->fields['user_email'];
			$arrReturn['user_thumb'] = $res->fields['user_thumb'];
			$arrReturn['user_role_id'] = $res->fields['user_role_id'];
            $arrReturn['agency'] = $res->fields['email_account'];
            $arrReturn['admin_group'] = $res->fields['admin_group'];
            $arrReturn['ref_id'] = $res->fields['ref_id'];
			//$materdata = GF::masterdata($res->fields['position_id']);
			$arrReturn['position_name'] = $res->fields['role_name'];
			$arrReturn['user_start_date'] = $res->fields['user_start_date'];
			$arrReturn['sick_leave'] = $res->fields['sick_leave'];
			$arrReturn['personal_leave'] = $res->fields['personal_leave'];
			$arrReturn['vacation_leave'] = $res->fields['vacation_leave'];
			$arrReturn['early_leave'] = $res->fields['early_leave'];
			/*$arrReturn['position_id'] = $res->fields['position_id'];*/
			$arrReturn['user_birthdate'] = $res->fields['user_birthdate'];
			$arrReturn['user_phone_number'] = $res->fields['user_phone_number'];
			$arrReturn['status'] = $res->fields['status'];
			$arrReturn['wallet'] = $res->fields['wallet'];
			$arrReturn['other_status'] = $res->fields['other_status'];
			$base->set('_userid',$res->fields['id']);
			if($res->fields['id']!=''){
				$arrReturn['rate'] = $this->_rateUserList();
			}else{
				$arrReturn['rate'] = array();
			}
			$arrReturn['user_address'] = $res->fields['user_address'];
			$arrReturn['user_province'] = $res->fields['user_province'];
			$arrReturn['user_post'] = $res->fields['user_post'];
			$arrReturn['user_line_id'] = $res->fields['user_line_id'];
			$arrReturn['user_bookbank'] = $res->fields['user_bookbank'];
			$arrReturn['bank_id'] = $res->fields['bank_id'];

            $arrReturn['province'] = $res->fields['province'];
            $arrReturn['amphur'] = $res->fields['amphur'];
            $arrReturn['district'] = $res->fields['district'];

			// Edit By Weerasak 09-01-2566
			$sql = "SELECT master_name,master_value FROM project_master WHERE id = ".GF::quote($res->fields['bank_id']);
			$res = $db->Execute($sql);
			$arrReturn['bank_name'] = $res->fields['master_name'];
			$arrReturn['bank_image'] = $res->fields['master_value'];
			// END
			//GF::print_r($res);
			return $arrReturn;
		}else{
			return NULL;
		}

	}
	private function _checkUsername(){
 		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$user_name = $base->get('GET.name');
 		//echo $token;
 		if($user_name!=''){
			$sql = "SELECT * FROM project_user WHERE user_username=".GF::quote($user_name)." AND active_status='O' ";

			//$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);


			$arr =  $res->fields['id'];
			if($arr==''){
				return 'A';
			}else{
				return 'F';
			}


		}else{
			return 'F';
		}

	}
	public function memberList(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$working = new Working();

      $memberInfomation = $this->memberInfomation();

      $user_id = $memberInfomation['id'];
      $cond_search= '';
      $agency =  $memberInfomation['agency'];
      if($agency=='A'){

         $cond_search .= " AND pu.ref_id =".GF::quote($user_id);
      }

		$sql = "SELECT pu.*,pr.role_name
						FROM project_user AS pu
						LEFT JOIN project_role AS pr ON(pr.id=pu.user_role_id)
						WHERE pu.active_status='O' ".$cond_search." ORDER BY pu.id ASC";
						//echo $sql;
		$res = $db->Execute($sql);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
			$arrReturn[$i]['user_code'] = $res->fields['user_code'];
			$arrReturn[$i]['role_name'] = $res->fields['role_name'];
			$arrReturn[$i]['user_nickname'] = $res->fields['user_nickname'];
			$arrReturn[$i]['user_thumb'] = $res->fields['user_thumb'];
			$arrReturn[$i]['user_email'] = $res->fields['user_email'];
			$arrReturn[$i]['user_start_date'] = $res->fields['user_start_date'];
			$arrReturn[$i]['user_birthdate'] = $res->fields['user_birthdate'];
			$arrReturn[$i]['sick_leave'] = $res->fields['sick_leave'];
			$arrReturn[$i]['personal_leave'] = $res->fields['personal_leave'];
			$arrReturn[$i]['vacation_leave'] = $res->fields['vacation_leave'];
			$arrReturn[$i]['early_leave'] = $res->fields['early_leave'];
			$materdata = GF::masterdata($res->fields['position_id']);
			$arrReturn[$i]['position_name'] = $materdata['master_name'];
			$arrReturn[$i]['status'] = $res->fields['status'];
			$arrReturn[$i]['wallet'] = $res->fields['wallet'];
			$arrReturn[$i]['active_status'] = $res->fields['active_status'];
			$arrReturn[$i]['other_status'] = $res->fields['other_status'];
			$base->set('_user_id',$res->fields['id']);
			//$arrReturn[$i]['online'] = $working->checkWokingByID();
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	public function memberListByNew(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$working = new Working();

      $memberInfomation = $this->memberInfomation();

      $cond = '';
      if($_SESSION['member_search']['user_code']!=''){
		 $cond .= " AND pu.user_code =".GF::quote($_SESSION['member_search']['user_code'])." ";
	  }
      if($_SESSION['member_search']['user_name']!=''){
		 $cond .= " AND pu.user_name LIKE '%".$_SESSION['member_search']['user_name']."%' ";
	  }
      if($_SESSION['member_search']['user_email']!=''){
		 $cond .= " AND pu.user_email =".GF::quote($_SESSION['member_search']['user_email'])." ";
	  }
      if($_SESSION['member_search']['user_role_id']!='ALL' && $_SESSION['member_search']['user_role_id']!=''){
		 $cond .= " AND pu.user_role_id =".GF::quote($_SESSION['member_search']['user_role_id'])." ";
      }
      
      if($_SESSION['member_search']['user_birthdate_start']!=''){
        $cond .= " AND pu.user_birthdate >=".GF::quote($_SESSION['member_search']['user_birthdate_start'])." ";
      }

      if($_SESSION['member_search']['user_birthdate_end']!=''){
        $cond .= " AND pu.user_birthdate <=".GF::quote($_SESSION['member_search']['user_birthdate_end'])." ";
      }

      $user_id = $memberInfomation['id'];
      $cond_search= '';
      $agency =  $memberInfomation['agency'];
      if($agency=='A'){

         $cond_search .= " AND pu.ref_id =".GF::quote($user_id);
      }

		$sql = "SELECT pu.*,pr.role_name
						FROM project_user AS pu
						LEFT JOIN project_role AS pr ON(pr.id=pu.user_role_id)
						WHERE pu.active_status='O' ".$cond_search.$cond." ORDER BY pu.id DESC";
        $sqlCount = "SELECT count(pu.id) AS ttl
						FROM project_user AS pu
						WHERE pu.active_status='O' ".$cond_search.$cond." ORDER BY pu.id DESC";
						//echo $sql;
		$res = $db->Execute($sqlCount);
		
		//$res = $db->Execute($sql);
		$count = $res->fields['ttl'];
        //echo $count;
			// while(!$res->EOF){
			// 	$count++;
			// 	$res->MoveNext();
			// }
			// $res->Close();

			$base->set('allPage',ceil($count/20));

		$page = $base->get('GET.page');

		$numr = 20;
		$start = "";
		$end = "";
		if($page==1||empty($page)){
			$start = 0;
			$page = 1;
			//$end = $numr;
		}else{
			//$end = ($page*$numr);
			$start = ($page*$numr)-$numr;
		}
		 $condNum = " LIMIT ".$start.",".$numr;

		 $base->set('currpage',$page);

		$res = $db->Execute($sql.$condNum);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
            $arrReturn[$i]['user_code'] = $res->fields['user_code'];
            $arrReturn[$i]['agency_code'] = $res->fields['agency_code'];
			$arrReturn[$i]['role_name'] = $res->fields['role_name'];
			$arrReturn[$i]['user_nickname'] = $res->fields['user_nickname'];
			$arrReturn[$i]['user_thumb'] = $res->fields['user_thumb'];
			$arrReturn[$i]['user_email'] = $res->fields['user_email'];
			$arrReturn[$i]['user_start_date'] = $res->fields['user_start_date'];
			$arrReturn[$i]['user_birthdate'] = $res->fields['user_birthdate'];
			$materdata = GF::masterdata($res->fields['position_id']);
			$arrReturn[$i]['position_name'] = $materdata['master_name'];
			$arrReturn[$i]['status'] = $res->fields['status'];
			$arrReturn[$i]['wallet'] = $res->fields['wallet'];
			$arrReturn[$i]['active_status'] = $res->fields['active_status'];
			$arrReturn[$i]['other_status'] = $res->fields['other_status'];
			$base->set('_user_id',$res->fields['id']);
			$arrReturn[$i]['online'] = $working->checkWokingByID();
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	public function memberOnlineDashboard(){
		$alllist = $this->memberList();

		$i=0;
		$j=0;
		$arr_a = array();
		$arr_b = array();
		foreach($alllist as $vals){
			if($vals['online']>0){
				$arr_a[$i] = $vals;
				$i++;
			}else{
				$arr_b[$j] = $vals;
				$j++;
			}
		}
		$arrReturn = array();
		$num = 0;
		if(count($arr_a)>0){
			foreach($arr_a as $value_a){
				$arrReturn[$num] = $value_a;
				$num++;
			}
		}
		if(count($arr_b)>0){
			foreach($arr_b as $value_b){
				$arrReturn[$num] = $value_b;
				$num++;
			}
		}
		return 	$arrReturn;
	}
	public function memberListByGroup(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$working = new Working();

 		$group_id = $base->get('_role_id');

		$sql = "SELECT * FROM project_user WHERE user_role_id=".GF::quote($group_id)." AND active_status='O' ORDER BY id ASC";
		$res = $db->Execute($sql);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['id'];
			$arrReturn[$i]['user_name'] = $res->fields['user_name'];
			$arrReturn[$i]['user_code'] = $res->fields['user_code'];
			$arrReturn[$i]['user_nickname'] = $res->fields['user_nickname'];
			$arrReturn[$i]['user_thumb'] = $res->fields['user_thumb'];
			$arrReturn[$i]['user_email'] = $res->fields['user_email'];
			$arrReturn[$i]['user_start_date'] = $res->fields['user_start_date'];
			$arrReturn[$i]['sick_leave'] = $res->fields['sick_leave'];
			$arrReturn[$i]['personal_leave'] = $res->fields['personal_leave'];
			$arrReturn[$i]['vacation_leave'] = $res->fields['vacation_leave'];
			$arrReturn[$i]['early_leave'] = $res->fields['early_leave'];
			$materdata = GF::masterdata($res->fields['position_id']);
			$arrReturn[$i]['position_name'] = $materdata['master_name'];
			$arrReturn[$i]['status'] = $res->fields['status'];
			$arrReturn[$i]['active_status'] = $res->fields['active_status'];
			$arrReturn[$i]['other_status'] = $res->fields['other_status'];
			$base->set('_user_id',$res->fields['id']);
			//$arrReturn[$i]['online'] = $working->checkWokingByID();
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	public function positionList(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

		$sql = "SELECT * FROM project_position WHERE status='O' AND active_status='O' ORDER BY id ASC";
		$res = $db->Execute($sql);

		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['id'];
			$arrReturn[$i]['position_name'] = $res->fields['position_name'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}

 	private function _createMember(){
		$base = Base::getInstance();
 		$db = DB::getInstance();

 		$picsave = '';

 		if($_FILES['user_image']['name']){
			$picname = $_FILES['user_image']['name'];

			$filetype = explode('.',$picname);
			$picname = GF::randomStr(25);

			$dest_picname_o = $base->get('BASEDIR').'/uploads/userthumbnail/'.$picname."_original.".end($filetype);
			$dest_picname_m = $base->get('BASEDIR').'/uploads/userthumbnail/'.$picname."_160x160.".end($filetype);

			$tmp_file = $_FILES['user_image']['tmp_name'];
			if(copy($tmp_file, $dest_picname_o)){
				$thumb = new ThumbnailRC($dest_picname_o);
				$thumb->crop($dest_picname_m, 160, 160);
				$picsave = $picname."_160x160.".end($filetype);
			}
		}

 		$sql = "INSERT INTO project_user (
											  user_username,
											  user_code,
											  agency_code,
											  user_email,
											  user_password,
											  user_name,
											  user_nickname,
											  user_thumb,
											  position_id,
											  user_birthdate,
											  user_start_date,
											  user_phone_number,
											  sick_leave,
											  personal_leave,
											  vacation_leave,
											  early_leave,
											  user_role_id,
											  status,
											  other_status,
											  active_status,
											  user_address,
											  user_province,
											  user_post,
											  user_line_id,
											  user_bookbank,
											  create_dtm
											)VALUES(
												".GF::quote($base->get('POST.user_username')).",
												".GF::quote($base->get('POST.user_code')).",
												".GF::quote($base->get('POST.agency_code')).",
												".GF::quote($base->get('POST.user_email')).",
												".GF::quote(md5($base->get('POST.user_password'))).",
												".GF::quote($base->get('POST.user_name')).",
												".GF::quote($base->get('POST.user_nickname')).",
												".GF::quote($picsave).",
												".GF::quote($base->get('POST.user_position')).",
												".GF::quote(date("Y-m-d", strtotime($base->get('POST.user_birthdate')))).",
												NOW(),
												".GF::quote($base->get('POST.user_phone_number')).",
												".GF::quote($base->get('POST.sick_leave')).",
												".GF::quote($base->get('POST.personal_leave')).",
												".GF::quote($base->get('POST.vacation_leave')).",
												".GF::quote($base->get('POST.early_leave')).",
												'2',
												".GF::quote($base->get('POST.user_status')).",
												".GF::quote($base->get('POST.user_other')).",
												'O',
												".GF::quote($base->get('POST.user_address')).",
												".GF::quote($base->get('POST.user_province')).",
												".GF::quote($base->get('POST.user_post')).",
												".GF::quote($base->get('POST.user_line_id')).",
												".GF::quote($base->get('POST.user_bookbank')).",
												NOW()
											)";

		$res = $db->Execute($sql);

		if($res){
			return true;
		}else{
			return false;
		}

	}

	private function _updateMember(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
		$db2 = DB2::getInstance();
 		$password = $base->get('POST.user_password');

 		if($_FILES['user_image']['name']){
			$picname = $_FILES['user_image']['name'];

			$filetype = explode('.',$picname);
			$picname = GF::randomStr(25);

			$dest_picname_o = $base->get('BASEDIR').'/uploads/userthumbnail/'.$picname."_original.".end($filetype);
			$dest_picname_m = $base->get('BASEDIR').'/uploads/userthumbnail/'.$picname."_160x160.".end($filetype);

			$tmp_file = $_FILES['user_image']['tmp_name'];
			if(copy($tmp_file, $dest_picname_o)){
				$thumb = new ThumbnailRC($dest_picname_o);
				$thumb->crop($dest_picname_m, 160, 160);
				$picsave = $picname."_160x160.".end($filetype);
				$sql = "UPDATE project_user SET user_thumb=".GF::quote($picsave)." WHERE id=".GF::quote($base->get('POST.user_id'));
				$res = $db->Execute($sql);
				if(is_file($base->get('BASEDIR')."/uploads/userthumbnail/".$base->get('POST.old_thumb'))){
					@unlink($base->get('BASEDIR')."/uploads/userthumbnail/".$base->get('POST.old_thumb'));
					@unlink($base->get('BASEDIR')."/uploads/userthumbnail/".str_replace("160x160",'original',$base->get('POST.old_thumb')));
				}


			}
		}
		if(!empty($password)){
			$sql = "UPDATE project_user SET user_password=".GF::quote(md5($base->get('POST.user_password')))." WHERE id=".GF::quote($base->get('POST.user_id'));
			$res = $db->Execute($sql);
		}
// Edit By Weerasak 08-01-2566
		// $sql = "UPDATE project_user SET
		// 							user_username=".GF::quote($base->get('POST.user_username')).",
		// 							user_code=".GF::quote($base->get('POST.user_code')).",
		// 							agency_code=".GF::quote($base->get('POST.agency_code')).",
		// 							user_email=".GF::quote($base->get('POST.user_email')).",
        //                             user_role_id=".GF::quote($base->get('POST.user_role_id')).",
		// 							user_name=".GF::quote($base->get('POST.user_name')).",
		// 							user_nickname=".GF::quote($base->get('POST.user_nickname')).",
		// 							position_id=".GF::quote($base->get('POST.user_position')).",
		// 							user_birthdate=".GF::quote(date("Y-m-d", strtotime($base->get('POST.user_birthdate')))).",
		// 							user_phone_number=".GF::quote($base->get('POST.user_phone_number')).",
		// 							sick_leave=".GF::quote($base->get('POST.sick_leave')).",
		// 							personal_leave=".GF::quote($base->get('POST.personal_leave')).",
		// 							vacation_leave=".GF::quote($base->get('POST.vacation_leave')).",
		// 							early_leave=".GF::quote($base->get('POST.early_leave')).",
		// 							status=".GF::quote($base->get('POST.user_status')).",
		// 							other_status=".GF::quote($base->get('POST.user_other')).",
		// 							user_address=".GF::quote($base->get('POST.user_address')).",
		// 							user_province=".GF::quote($base->get('POST.user_province')).",
		// 							user_post=".GF::quote($base->get('POST.user_post')).",
		// 							user_line_id=".GF::quote($base->get('POST.user_line_id')).",
		// 							user_bookbank=".GF::quote($base->get('POST.user_bookbank')).",
        //                             province=".GF::quote($base->get('POST.province')).",
        //                             amphur=".GF::quote($base->get('POST.amphur')).",
        //                             district=".GF::quote($base->get('POST.district')).",									  
		// 							update_dtm=NOW() 
		// 							WHERE id=".GF::quote($base->get('POST.user_id'));

		$sql = "UPDATE project_user 
				SET 
					user_username=".GF::quote($base->get('POST.user_username')).",
		 			user_code=".GF::quote($base->get('POST.user_code')).",
		 			agency_code=".GF::quote($base->get('POST.agency_code')).",
		 			user_email=".GF::quote($base->get('POST.user_email')).",
					user_role_id=".GF::quote($base->get('POST.user_role_id')).",
					user_name=".GF::quote($base->get('POST.user_name')).",
					user_nickname=".GF::quote($base->get('POST.user_nickname')).",
					position_id=".GF::quote($base->get('POST.user_position')).",
					user_birthdate=".GF::quote(date("Y-m-d", strtotime($base->get('POST.user_birthdate')))).",
					user_phone_number=".GF::quote($base->get('POST.user_phone_number')).",
					status=".GF::quote($base->get('POST.user_status')).",
					other_status=".GF::quote($base->get('POST.user_other')).",
					user_address=".GF::quote($base->get('POST.user_address')).",
					user_province=".GF::quote($base->get('POST.user_province')).",
					user_post=".GF::quote($base->get('POST.user_post')).",
					user_line_id=".GF::quote($base->get('POST.user_line_id')).",
					user_bookbank = ".GF::quote($base->get('POST.user_bookbank')).",
					province=".GF::quote($base->get('POST.province')).",
					amphur=".GF::quote($base->get('POST.amphur')).",
					district=".GF::quote($base->get('POST.district')).",
					bank_id=".GF::quote($base->get('POST.bank_id')).",
					update_dtm=NOW()
				WHERE id=".GF::quote($base->get('POST.user_id'));
		$res = $db->Execute($sql);

		//$db2->insert("test",array("order_id"=>$base->get('POST.bank_id'),"price"=>0));

	//END
 		if($res){
			return true;
		}else{
			return false;
		}
	}

	private function _updateUserActiveStatus(){

		$base = Base::getInstance();
 		$db = DB::getInstance();

		$sql = "UPDATE project_user SET
									  active_status='C',
									  update_dtm=NOW()
										WHERE id=".GF::quote($base->get('_userid'));
		$res = $db->Execute($sql);
 		if($res){
			return true;
		}else{
			return false;
		}
	}


	public function  getProjectTeambyLeader($owner_id = NULL)
	{
		$base 	= Base::getInstance();
 		$db 	= DB::getInstance();

 		$working = new Working();

 		if(isset($owner_id)){
 			$owner_id_arr = explode(',', $owner_id);
 		}else{
 			$owner_id_arr = array();
 		}

 		$ownerID = "";
 		foreach ($owner_id_arr as $id) {
 			$ownerID .= " AND id!=".$id;
 		}

		$sql = "SELECT  id,user_name,user_nickname
				FROM project_user
				WHERE active_status='O'  ".$ownerID."  ORDER BY id ASC";
		$res = $db->Execute($sql);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] 			= $res->fields['id'];
			$arrReturn[$i]['user_name'] 	= $res->fields['user_name'];
			$arrReturn[$i]['user_nickname'] = $res->fields['user_nickname'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	private function _addrate(){
		$base 	= Base::getInstance();
 		$db 	= DB::getInstance();

 		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

 		$sql ="INSERT INTO project_user_rate(
 												user_id,
 												order_type,
 												ship_by,
 												unit,
 												rate,
 												create_dtm,
 												create_by
 												)
 												VALUES(
 												".GF::quote($base->get('POST.user_id')).",
 												".GF::quote($base->get('POST.order_type')).",
 												".GF::quote($base->get('POST.ship_by')).",
 												".GF::quote($base->get('POST.unit')).",
 												".GF::quote($base->get('POST.rate')).",
 												".GF::quote($user_id).",
 												NOW()
 												)";
 		$res = $db->Execute($sql);
 		return true;
	}
	private function  _rateUserList(){
		$base 	= Base::getInstance();
 		$db 	= DB::getInstance();

 		$sqlx = "SELECT user_role_id FROM project_user WHERE  id='".$base->get('_userid')."'  ORDER BY id ASC";
 		$resx= $db->Execute($sqlx);

		$sql = "SELECT * FROM project_user_rate WHERE user_id='".$resx->fields['user_role_id']."'  ORDER BY id ASC";
		$res = $db->Execute($sql);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] 			= $res->fields['id'];
			$arrReturn[$i]['order_type'] 	= $res->fields['order_type'];
			$arrReturn[$i]['ship_by'] 		= $res->fields['ship_by'];
			$arrReturn[$i]['unit'] 			= $res->fields['unit'];
			$arrReturn[$i]['rate'] 			= $res->fields['rate'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	private function  _deleteRate(){
		$base 	= Base::getInstance();
 		$db 	= DB::getInstance();

 		$sql = "DELETE FROM project_user_rate WHERE id=".GF::quote($base->get('GET.id'));
 		$res = $db->Execute($sql);


 	}
 	private function _saveWallet(){
		$base 	= Base::getInstance();
        $db 	= DB::getInstance();
        $db2 	= DB2::getInstance();

 		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

        $member_old_info = GF::memberinfo($base->get('POST.user_id'));
        $oldWallet = $member_old_info['wallet'];

        $old_balance = $oldWallet;
        $new_balance = 0;
       /* if($base->get('POST.wallet_type')=='IN' || $base->get('POST.wallet_type')=='INOU'){
            $new_balance = $oldWallet+$base->get('POST.wallet');
        }
        if($base->get('POST.wallet_type')=='OU'){
            $new_balance = $oldWallet-$base->get('POST.wallet');
        }*/
         if($base->get('POST.wallet_type')=='IN'){
            $new_balance = $oldWallet+$base->get('POST.wallet');
        }
        if($base->get('POST.wallet_type')=='OU' || $base->get('POST.wallet_type')=='INOU'){
			
			if($oldWallet<0){
				$new_balance = $oldWallet+$base->get('POST.wallet');
			}else{
               $new_balance = $oldWallet-$base->get('POST.wallet');
			}
        }

 		if($base->get('POST.wallet_type')=='IN'){
           $wallet_type = 'IN';
        }else if($base->get('POST.wallet_type')=='OU'){
           $wallet_type = 'OU';
        }else if($base->get('POST.wallet_type')=='INOU'){
           $wallet_type = 'INOU';
           $shipid = $base->get('POST.shipid');
           $dtm = date("Y-m-d H:i:s");
           $wallet = $base->get('POST.wallet');
           $sqlC = "SELECT ship_price,otr_price_baht,tracking FROM web_shipping_item WHERE shipping_id=".GF::quote($shipid);
           $resC = $db->Execute($sqlC);
           $sumShip = 0;
           $ship_price = $resC->fields['ship_price'];
           $otr_price_baht=$resC->fields['otr_price_baht'];
           $sumShip = $ship_price+$otr_price_baht;
           $sqlShip = "UPDATE web_shipping SET status='6',ship_pay=".GF::quote($wallet).",billing_total=".GF::quote($sumShip).",update_dtm=".GF::quote($dtm).",ship_pay_dtm=".GF::quote($dtm)." WHERE id=".GF::quote($shipid);
           $db->Execute($sqlShip);
           $base->set('_ids',$shipid);
           $shipping = new STracking();
		   $shippininfo = $shipping->shippingInfo();
           foreach($shippininfo['tracking'] as $tracking){
				$sqlTrack = "UPDATE web_tracking SET status='8' WHERE tracking=".GF::quote($tracking['tracking']);
				$db->Execute($sqlTrack);
			}
           $dataSet = $db2->select("web_tracking",array(
			  										    "tracking(track_no)",
														"member_code(track_user_code)",
														"order_type(track_goods_code)",
														"status(track_status)",
														"order_comment(track_remark)",
														"order_id(track_order)",
														"saller_name(track_shop)",
														"weight(track_weight)",
														"dime_w(track_width)",
														"dime_x(track_long)",
														"dime_y(track_hight)",
														"cbm(track_cbm)",
														"update_dtm(track_upddate)",
														"create_dtm(track_createdate)"
			  											),array(
			  											"tracking"=>$resC->fields['tracking']
			  											));
			  if(count($dataSet)>0){
			  	foreach($dataSet as $key=>$valdata){
					$dataSet[$key]['track_goods_code'] = GF::convgoodscode($valdata['track_goods_code']);
					$dataSet[$key]['track_timp_code'] = 'Car';
				}
				$db2->insert("project_webservice",array(
														"service_token"=>GF::randomStr(20),
														"service_method"=>"UPDATE_TRACK",
														"service_data"=>json_encode($dataSet),
														"status"=>'N',
														"create_dtm"=>date('Y-m-d H:i:s'),
														"update_dtm"=>date('Y-m-d H:i:s')
														));
			  }
           
           
          
          
        }


 		$sql ="INSERT INTO project_wallet_log(
 												user_id,
 												wallet,
                                    before_balance,
                                    after_balance,
 												wallet_type,
 												wallet_desc,
 												create_dtm,
 												create_by
 												)
 												VALUES(
 												".GF::quote($base->get('POST.user_id')).",
 												".GF::quote($base->get('POST.wallet')).",
                                    ".GF::quote($old_balance).",
                                    ".GF::quote($new_balance).",
 												".GF::quote($wallet_type).",
 												".GF::quote($base->get('POST.wallet_desc')).",
 												".GF::quote(date("Y-m-d H:i:s",strtotime($base->get('POST.create_dtm')." ".$base->get('POST.time_dtm')))).",
 												".GF::quote($user_id)."
 												)";
 		$res = $db->Execute($sql);

        $member_new_info = GF::memberinfo($base->get('POST.user_id'));
        $currentWallet = $member_new_info['wallet'];
        $tatal = $currentWallet;
       /* if($base->get('POST.wallet_type')=='IN' || $base->get('POST.wallet_type')=='INOU'){
            $tatal = $currentWallet+doubleval($base->get('POST.wallet'));
        }
        if($base->get('POST.wallet_type')=='OU'){
            $tatal = $currentWallet-doubleval($base->get('POST.wallet'));
        }*/
        if($base->get('POST.wallet_type')=='IN'){
            $tatal = $currentWallet+doubleval($base->get('POST.wallet'));
        }
        if($base->get('POST.wallet_type')=='OU' || $base->get('POST.wallet_type')=='INOU'){
			if($currentWallet<0){
				$tatal = $currentWallet+doubleval($base->get('POST.wallet'));
			}else{
                $tatal = $currentWallet-doubleval($base->get('POST.wallet'));
			}
        }
  	if($base->get('POST.wallet_type')=='IN' || $base->get('POST.wallet_type')=='OU' || $base->get('POST.wallet_type')=='INOU' ){
		// $sql = "UPDATE project_user SET
		// 							  wallet='".$tatal."',
		// 							  update_dtm=NOW()
		// 								WHERE id=".GF::quote($base->get('POST.user_id'));
        // $res = $db->Execute($sql);
        }
        
       /* if($base->get('POST.wallet_type')=='INOU'){
            $billingdata = $db2->get("web_shipping","*",array("shipping_code"=>$base->get('POST.shipping_code')));
            //GF::print_r($billingdata);
            if($billingdata['shipping_code']!=''){
                $totalvalue = $billingdata['billing_total']+$billingdata['shipping_th_price']+$billingdata['shipping_other_price']+$billingdata['shipping_th_service_price'];

                $totalvalue = $totalvalue-$billingdata['ship_pay'];

                if($totalvalue>0){
                    $divtt = $tatal-$totalvalue;
                    if($divtt>0){
                        $shippay = $billingdata['ship_pay']+$totalvalue;
                        $db2->update("web_shipping",array(
                                                        "ship_pay"=>$shippay,
                                                        "update_dtm"=>date('Y-m-d H:i:s')
                                                        ),array(
                                                            "shipping_code"=>$base->get('POST.shipping_code')
                                                        ));
                        $sql ="INSERT INTO project_wallet_log(
                            user_id,
                            wallet,
                            before_balance,
                            after_balance,
                            wallet_type,
                            wallet_desc,
                            create_dtm,
                            create_by
                            )
                            VALUES(
                            ".GF::quote($base->get('POST.user_id')).",
                            ".GF::quote($totalvalue).",
                            ".GF::quote($tatal).",
                            ".GF::quote($divtt).",
                            'OU',
                            'ชำระค่านำเข้า ORDER ID : ".$base->get('POST.shipping_code')."',
                            ".GF::quote(date("Y-m-d H:i:s",strtotime($base->get('POST.create_dtm')." ".$base->get('POST.time_dtm')))).",
                            ".GF::quote($user_id)."
                            )";

                        $res = $db->Execute($sql);

                        $sql = "UPDATE project_user SET
                                                wallet='".$divtt."',
                                                update_dtm=NOW()
                                                WHERE id=".GF::quote($base->get('POST.user_id'));
                        $res = $db->Execute($sql);
                    }else{
                        $divtt = $totalvalue-$tatal;
                        $shippay = $billingdata['ship_pay']+$tatal;
                        $db2->update("web_shipping",array(
                                                        "ship_pay"=>$shippay,
                                                        "update_dtm"=>date('Y-m-d H:i:s')
                                                        ),array(
                                                            "shipping_code"=>$base->get('POST.shipping_code')
                                                        ));
                        $sql ="INSERT INTO project_wallet_log(
                            user_id,
                            wallet,
                            before_balance,
                            after_balance,
                            wallet_type,
                            wallet_desc,
                            create_dtm,
                            create_by
                            )
                            VALUES(
                            ".GF::quote($base->get('POST.user_id')).",
                            ".GF::quote($tatal).",
                            ".GF::quote($tatal).",
                            '0',
                            'OU',
                            'ชำระค่านำเข้า ORDER ID : ".$base->get('POST.shipping_code')."',
                            ".GF::quote(date("Y-m-d H:i:s",strtotime($base->get('POST.create_dtm')." ".$base->get('POST.time_dtm')))).",
                            ".GF::quote($user_id)."
                            )";
                        $res = $db->Execute($sql); 

                        $sql = "UPDATE project_user SET
                                                wallet='0',
                                                update_dtm=NOW()
                                                WHERE id=".GF::quote($base->get('POST.user_id'));
                        $res = $db->Execute($sql);                               
                    }
                    
                }
            }
        }
*/

 		return true;
	}
	

	private function  _walletLog($status=NULL){
		$base 	= Base::getInstance();
 		$db 	= DB::getInstance();

		$cond = '';
		if($status!=NULL){
			$cond .= " AND wallet_type=".GF::quote($status)." ";
		}

		$sql = "SELECT * FROM project_wallet_log WHERE wallet_desc != 'เติมเงิน' and user_id=".$base->get('_userid').$cond."  ORDER BY id DESC";
		$res = $db->Execute($sql);
		$count = $res->NumRows();

			$base->set('allPage',ceil($count/20));

		$page = $base->get('GET.page');

		$numr = 20;
		$start = "";
		$end = "";
		if($page==1||empty($page)){
			$start = 0;
			$page = 1;
			//$end = $numr;
		}else{
			//$end = ($page*$numr);
			$start = ($page*$numr)-$numr;
		}
		 $condNum = " LIMIT ".$start.",".$numr;

		 $base->set('currpage',$page);

		$res = $db->Execute($sql.$condNum);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] 			= $res->fields['id'];
			$arrReturn[$i]['wallet'] 		= $res->fields['wallet'];
            $arrReturn[$i]['before_balance'] 		= $res->fields['before_balance'];
            $arrReturn[$i]['after_balance'] 		= $res->fields['after_balance'];
			$arrReturn[$i]['wallet_type'] 	= $res->fields['wallet_type'];
			$arrReturn[$i]['wallet_desc'] 	= $res->fields['wallet_desc'];
			$arrReturn[$i]['create_dtm'] 	= $res->fields['create_dtm'];
			$base->set('user_id',$res->fields['create_by']);
			$userinfo = $this->_memberIfomationByID();
			$arrReturn[$i]['username'] 	= $userinfo['user_name'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}

	private function  _updateWithdraw(){
		$base 	= Base::getInstance();
 		$db 	= DB2::getInstance();

 		$member = new Member();
		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];
		 $data = $base->get('POST');
		 $last_id = $db->insert("project_withdraw",array(
			 "status"=>'R',
			 "create_dtm"=>date('Y-m-d H:i:s'),
			 "user_id"=> $user_id,
			 "wd_total"=>$data['refund_amount'],
			 "wd_wallet"=>$data['refund_amount'],
			 "wd_bank"=> $memberInfomation['bank_id'],
			 "wd_number"=> $memberInfomation['user_bookbank'],
			 "wd_name"=> $memberInfomation['user_name'],
			 "create_by"=>$user_id,
			 "update_dtm"=>date('Y-m-d H:i:s'),
			 "update_by"=>$user_id,			 
			 "order_id"=>$data['order_id']
			));

 		// $memberInfomation = $member->memberInfomation();

 		// $user_id = $memberInfomation['id'];

		// $data = $base->get('POST');
		// unset($data['mode']);
		// $data['status'] = 'R';
		// $data['create_dtm'] = date('Y-m-d H:i:s');
		// $data['user_id'] = $user_id;
		// $data['create_by'] = $user_id;
		// $data['update_dtm'] = date('Y-m-d H:i:s');
		// $data['update_by'] = $user_id;
      	// $data['wd_wallet'] = $memberInfomation['wallet'];
		// $data['order_id'] = 1;

		// $last_id = $db->insert("project_withdraw",$data);

		return $last_id;
	}
	/* รายการ refund */
	private function  _withdrawList(){
		$base 	= Base::getInstance();
 		$db 	= DB::getInstance();

 		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$sql = "SELECT 
		o.id
		,o.order_number
		,MAX(ref.create_dtm) as create_dtm
		,SUM(oi.product_qty) as qty
		,SUM(ref_thb) as amount
		,CASE WHEN (SELECT count(*) FROM project_withdraw WHERE user_id = ref.create_by) = 1 AND ref.status = 'Refunded'
		THEN (SELECT wd_wallet FROM project_withdraw WHERE user_id = ref.create_by) 
		ELSE 0 END refund_amount
		,ref_admin as remark
		,CASE 
		WHEN ref.status = 'Refunded' AND COALESCE(wd.status,'') != '' THEN
							CASE wd.status WHEN 'A' THEN 'SUCCESS'
							WHEN 'N' THEN 'CANCEL'
							WHEN 'R' THEN 'WAIT'
							END
		WHEN ref.status = 'Refunded' THEN 'SUCCESS'
		ELSE 'WAIT'
		END status
		FROM project_item_refund ref
		LEFT JOIN web_order_item oi ON oi.id = ref.item_id
		LEFT JOIN web_order o ON o.id = oi.order_id 
		LEFT JOIN project_withdraw wd ON wd.user_id = ref.create_by AND COALESCE(wd.order_id,0) = o.id
		WHERE ref.create_by = ".$user_id."
		GROUP BY
		o.id
		,o.order_number
		,ref_admin
		,wd.wd_wallet
		ORDER BY ref.ref_id desc";

		//echo $sql;
		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['order_id'] = $res->fields['id'];
			$arrReturn[$i]['order_number'] 	= $res->fields['order_number'];
			$arrReturn[$i]['qty'] 	= $res->fields['qty'];
			$arrReturn[$i]['amount'] = $res->fields['amount'];
			$arrReturn[$i]['refund_amount'] 	= $res->fields['refund_amount'];
			$arrReturn[$i]['remark'] 	= $res->fields['remark'];
			$arrReturn[$i]['create_dtm'] 	= $res->fields['create_dtm'];
			$arrReturn[$i]['status'] 	= $res->fields['status'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return $arrReturn;
	}
	/*
	private function  _withdrawList(){
		$base 	= Base::getInstance();
 		$db 	= DB2::getInstance();

 		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$datas = $db->select("project_withdraw","*", array(
			"user_id" => $user_id,
			"ORDER" => "wd_id DESC"
		));

		return $datas;
	}
	*/
	private function  _withdrawListAll(){
   		$base 	= Base::getInstance();
    		$db 	= DB2::getInstance();

    		$member = new Member();


    		$memberInfomation = $member->memberInfomation();

    		$user_id = $memberInfomation['id'];

         $arrSearch = array();
         $arrSearch['status[!]']='D';

         if($_SESSION['wallet_wd']['status']!=''){
   			$arrSearch['status'] = $_SESSION['wallet_wd']['status'];
   		}

         if($_SESSION['wallet_wd']['user_code']!=''){
            $user_id_pure = GF::user_id($_SESSION['wallet_wd']['user_code']);
   			$arrSearch['user_id'] = $user_id_pure;
            if($user_id_pure==''){
               unset($arrSearch['user_id']);
            }
   		}

         $memberInfomation = $member->memberInfomation();

         $user_id = $memberInfomation['id'];
         $cond_search= '';
         $agency =  $memberInfomation['agency'];
         if($agency=='A'){
            $list_ref = GF::refgroup();
            if(count($list_ref)>0){
               $arrSearch['user_id'] = $list_ref;
              //$cond_search .= " AND create_by IN(".$ref_str.") ";
            }else{
               $arrSearch['user_id'] = 'X';
            }
         }


         $count = $db->count("project_withdraw","*",array(
   	 			"AND"=>$arrSearch,
   	 			"ORDER"=>"wd_id DESC"
	 		));
			//print_r($db->last_query());

			$base->set('allPage',ceil($count/10));

			$page = $base->get('GET.page');

			$numr = 10;
			$start = "";
			$end = "";
			if($page==1||empty($page)){
				$start = 0;
				$page = 1;
				//$end = $numr;
			}else{
				//$end = ($page*$numr);
				$start = ($page*$numr)-$numr;
			}
			 //$cond = " LIMIT ".$start.",".$numr;

			 $base->set('currpage',$page);



	 		$result = $db->select("project_withdraw","*",array(
	 			"AND"=>$arrSearch,
	 			"ORDER"=>"wd_id DESC",
	 			"LIMIT"=>[$start,$numr]

	 		));


		return $result;
	}
	public function  _withdrawStatus(){
		$base 	= Base::getInstance();
 		$db 	= DB2::getInstance();

 		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

		$data = $db->get("project_withdraw", "*", array(
			"wd_id" => $base->get('GET.id')
		));

        $status = $base->get('GET.status');
        $textMail = 'อนุมัติ';
		if($status=='N'){
			$last_id = $db->update("project_withdraw",array(
				"status"=>'N',
				'update_dtm' => date('Y-m-d H:i:s'),
				'update_by' => $user_id
			),array(
				"wd_id"=>$data['wd_id']
            ));
            
            $textMail = 'ไม่อนุมัติ';
		}
		if($status=='A'){
			$last_id = $db->update("project_withdraw",array(
				"status"=>'A',
				'update_dtm' => date('Y-m-d H:i:s'),
				'update_by' => $user_id
			),array(
				"wd_id"=>$data['wd_id']
			));

         $member_old_info = GF::memberinfo($data['user_id']);
         $oldWallet = $member_old_info['wallet'];

         $old_balance = $oldWallet;
         $new_balance = $oldWallet-$data['wd_total'];


			$datasLog = array();
			$datasLog['wallet'] = $data['wd_total'];
            $datasLog['before_balance'] = $old_balance;
            $datasLog['after_balance'] = $new_balance;
			$datasLog['wallet_type'] = 'WD';
			$datasLog['wallet_desc'] = "ถอนเงิน";
			$datasLog['create_by'] = $user_id;
			$datasLog['create_dtm'] = date('Y-m-d H:i:s');

			$last_id = $db->insert("project_wallet_log",$datasLog);
			//Edit When 2566-06-07
			// $last_id = $db->update("project_user",array(
			// 	"wallet[-]"=>$data['wd_total']
			// ),array(
			// 	"id"=>$data['user_id']
			// ));

        }
        
        // $mail = Mailer::getInstance();
        
        // $mail->userid = $data['member_id'];
        // $mail->Subject = "แจ้งสถานะคำขอถอนเงิน";
        // $members = GF::memberinfo($data['user_id']);
        // $dataHtml = GF::mailtemplate('8');
        // $dataHtml = str_replace("{CUST_NAME}",$members['user_name'],$dataHtml);
        // $dataHtml = str_replace("{PAYMENT_TOTAL}",$data['wd_total'],$dataHtml);
        // $dataHtml = str_replace("{PAYMENT_STATUS}",$textMail,$dataHtml);
        // $mail->msg = $dataHtml;
        // $mail->SendingToNoti();

        $members = GF::memberinfo($data['user_id']);

        //GF::print_r($members);

       $link = $base->get('BASEURL').'/#/ewallet/withdraw/';
        $dataReplace = array(
            "{CUST_NAME}"=>$members['user_name'],
            "{LINK}"=>GF::shorturl($link,'1'),
            "{PAYMENT_TOTAL}"=>$data['wd_total'],
            "{PAYMENT_STATUS}"=>$textMail
        );
        $mail = Mailer::getInstance();
        $mail->Subject = GF::mailsubject('8',$members['ref_id']);
        $mail->msgHTML = GF::mailtemplate('8',$dataReplace,$members['ref_id']);
        $mail->addAddressEmail = $members['user_email'];
        $mail->addAddressName = $members['user_name'];
        $mail->sendMail();


        

		return true;
	}
    private function  _withdrawComment(){
		$base 	= Base::getInstance();
 		$db 	= DB2::getInstance();

 		$member = new Member();


 		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];

        $last_id = $db->update("project_withdraw",array(
				"wd_comment"=>$base->get('GET.comment'),
				'update_dtm' => date('Y-m-d H:i:s'),
				'update_by' => $user_id
			),array(
				"wd_id"=>$base->get('GET.id')
			));

		

		return true; 
	}
   private function  _withdrawImage(){
		$base 	= Base::getInstance();
 		$db 	= DB2::getInstance();


      $last_id = $db->update("project_withdraw",array(
         "wd_img"=>$base->get('wd_img')
      ),array(
         "wd_id"=>$base->get('POST.key')
      ));
      $db->last_query();
		return true;
	}
// Edit By Weerasak 06-01-2566
	private function _refunDetail(){
		$base = Base::getInstance();
		$db = DB::getInstance();

		$member = new Member();
		$memberInfomation = $member->memberInfomation();
		$member_id = $memberInfomation['id'];

		$sql = "SELECT o.order_id,o.order_number,o.refund_wallet
		FROM web_order o 
		LEFT JOIN project_withdraw wd ON wd.order_id = o.order_id
		WHERE  o.member_id = ".GF::quote($member_id)." AND o.refund_wallet > 0
		AND COALESCE(wd.status,'') != 'R'";
		$res = $db->Execute($sql);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['order_id'] = $res->fields['order_id'];
			$arrReturn[$i]['order_number'] 	= $res->fields['order_number'];
			$arrReturn[$i]['refund_amount'] = $res->fields['refund_wallet'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return $arrReturn;
	}

 }
?>
