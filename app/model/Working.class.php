<?php
 class Working {
 	
 	function validateWorking(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		
 		$cookie = $base->get('COOKIE');
 		$token = $cookie['token'];
 		
 		$currentdate = date('Y-m-d');
 		
 		
 		$sql = "SELECT * FROM project_user_working WHERE token=".GF::quote($token)." AND status='W' AND working_date='".$currentdate."' ORDER BY id DESC LIMIT 1";

 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
		$res = $db->Execute($sql);

		
		if($res->fields['id']!=''){
			return true;
		}else{
			return false;
		}
 		
	}
 	public function setWorking(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		
 		$validateWorking = $this->validateWorking();
 		
 		$cookie = $base->get('COOKIE');
 		$token = $cookie['token'];
 		
 		$currentdate = date('Y-m-d');
 		$sql = "SELECT * FROM project_user_working WHERE token=".GF::quote($token)." AND working_date='".$currentdate."' ORDER BY id DESC LIMIT 1";
 		$res = $db->Execute($sql);
 		if($res->fields['id']!=''){
			$sql = "UPDATE project_user_working SET status='W',late_time='0' WHERE id=".GF::quote($res->fields['id']);
			$res = $db->Execute($sql);
			
			if($res){
				return true;
			}else{
				return false;
			}
			exit();
		}

 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
		$res = $db->Execute($sql);
 		
 		if(!$validateWorking){
			$ipaddress = GF::viewipaddress();
			$sql = "INSERT INTO project_user_working (
												  user_id,
												  token,
												  working_date,
												  working_time,
												  working_ip,
												  status
												)VALUES(
													".GF::quote($memberInfomation['id']).",
													".GF::quote($token).",
													NOW(),
													NOW(),
													".GF::quote($ipaddress).",
													'W'
												)";

			$res = $db->Execute($sql);
			if($res){
				return true;
			}else{
				return false;
			}
		}
 		
	}
	public function stopWorking(){
		
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$member = new Member();
 		$memberInfomation = $member->memberInfomation();
 		
 		$cookie = $base->get('COOKIE');
 		$token = $cookie['token'];
 		
 		$currentdate = date('Y-m-d');
 		
 		
 		$sql = "SELECT * FROM project_user_working WHERE token=".GF::quote($token)." AND status='W' AND working_date='".$currentdate."' ORDER BY id DESC LIMIT 1";

 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
		$res = $db->Execute($sql);
		
		$w_id = $res->fields['id'];
		
		if($res->fields['id']!=''){
			
			$ipaddress = GF::viewipaddress();
				
			$sql = "UPDATE project_user_working SET end_date=NOW(), end_time=NOW() , status='E',end_ip='".$ipaddress."' WHERE id=".GF::quote($res->fields['id']);
			//echo $sql;
			$res = $db->Execute($sql);
			
			$this->_calculateLateTime($w_id);
			
			if($res){
				return true;
			}else{
				return false;
			}
			
		}else{
			return false;
		}
		
	}
	private function _calculateLateTime($id){
		
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$member  = new Member();
 		
 		$cf_start = strtotime('09:00:00');
 		$cf_end = strtotime('18:00:00');
 		$cf_maxend = strtotime('18:30:00');
 		
		$sql = "SELECT * FROM project_user_working WHERE id=".GF::quote($id);

 		$db->SetFetchMode(ADODB_FETCH_ASSOC);
		$res = $db->Execute($sql);
		
		$curr_start = strtotime($res->fields['working_time']);
		$curr_end = strtotime($res->fields['end_time']);
		
		$late_time = 0;
		
		if($curr_start>$cf_start){
			$late_time += round(abs($curr_start - $cf_start) / 60,2);
		}
		
		if($curr_end<$cf_end){
			$late_time += round(abs($cf_end - $curr_end) / 60,2);
		}
		
		if($curr_end>$cf_end){
			if($curr_end>$cf_maxend){
				$late_time -= 30;//round(abs($curr_end - $cf_end) / 60,2);
			}else{
				$late_time -= round(abs($curr_end - $cf_end) / 60,2);
			}
			
			if($late_time<0){
				$late_time = 0;
			}
		}
		
		$sql = "UPDATE project_user_working SET late_time='".$late_time."' WHERE id=".GF::quote($res->fields['id']);
		$res = $db->Execute($sql);
		
	}
	public function _findWorkingUser(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$member  = new Member();
 		
 		$sql = "SELECT * FROM project_user_working  WHERE status='W' ORDER BY working_date,working_time DESC";
		$res = $db->Execute($sql);

		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['id'];
			$arrReturn[$i]['working_date'] = $res->fields['working_date'];
			$arrReturn[$i]['working_time'] = $res->fields['working_time'];
			$arrReturn[$i]['working_ip'] = $res->fields['working_ip'];
			$base->set('user_id',$res->fields['user_id']);
			$memberinfo = $member->memberInfomationByID();
			$arrReturn[$i]['member'] = $memberinfo;
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	public function checkWokingByID(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$member  = new Member();
 		
 		$user_id = $base->get('_user_id');
 		
 		$sql = "SELECT * FROM project_user_working  WHERE status='W' AND user_id=".GF::quote($user_id);
		$res = $db->Execute($sql);
		$i = 0;
		while(!$res->EOF){
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$i;
	}
	public function findLoser(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$month = date('m');
 		
 		$sql = "SELECT user_id FROM project_user_working  WHERE status='L' AND MONTH(working_date)=".GF::quote($month)." GROUP BY user_id ";
		$res = $db->Execute($sql);
		$i = 0;
		$result = array();
		while(!$res->EOF){
			$result[$i] = $res->fields['user_id'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		
		$arrReturn = array();
		$i = 0;
		if(count($result)>0){
			$member = new Member();
			foreach($result as $user_id){
				$sql = "SELECT count(user_id) as total FROM project_user_working  WHERE status='L' AND user_id=".GF::quote($user_id)." AND MONTH(working_date)=".GF::quote($month);
				$res = $db->Execute($sql);
				$arrReturn[$i]['total'] = $res->fields['total'];
				$res->Close();
				
				$base->set('user_id',$user_id);
				$arrReturn[$i]['member'] = $member->memberInfomationByID();
				$i++;
			}
		}
		return $arrReturn;
	}
 	
 }
?>