<?php
//  class DB {
//  	private static $instance = false;
//  	public static function getInstance() {
// 	    if (!self::$instance) {
// 	      self::$instance = new DB();
// 	    }

// 	    return self::$instance;
// 	}
	
// 	private function _connectMySQL(){
		
// 		$base = Base::getInstance();
		
// 		include_once($base->get('BASEDIR').'/assets/libs/adodb/adodb.inc.php');
// 		$db = ADONewConnection('mysqli'); 

// 		$server = $base->get('SERVER');
// 		$user = $base->get('DB_USER');
// 		$password = $base->get('DB_PASSWORD');
// 		$database = $base->get('DB_NAME');
		

		 
// 		$db->Connect($server,$user,$password,$database) or die('No Select Database');
//         $db->Execute('SET NAMES utf8');
//         $db->SetFetchMode(ADODB_FETCH_ASSOC);
// 		return $db;
// 	}
// 	public function condb(){
// 		$resultreturn = $this->_connectMySQL();
// 		return $resultreturn;
// 	}
	
//  }

  class DB {
      private static $instance = NULL;

      private function __construct() {}
      private function __clone(){}

      public function __destruct(){
          //echo "DB Connection End";
      }

      public static function getInstance() {

          if (!self::$instance){

            try{

                 $base = Base::getInstance();
                 $server = $base->get('SERVER');
                 $user = $base->get('DB_USER');
                 $password = $base->get('DB_PASSWORD');
                 $database = $base->get('DB_NAME');

                 include_once($base->get('BASEDIR').'/assets/libs/adodb/adodb.inc.php');
                 self::$instance = ADONewConnection('mysqli');
                 self::$instance->Connect($server,$user,$password,$database) or die('Please contact administrator. [Error : 001]');
                 self::$instance->Execute('SET NAMES utf8');
                 self::$instance->SetFetchMode(ADODB_FETCH_ASSOC);

                 //print_r(self::$instance);

                 //self::$instance = $database;

            }catch(PDOException $e){
                throw new SQLException($e->getMessage());
                //echo $e->getMessage();
                exit();
            }
          }

          return self::$instance;
      }
      public static function error($e) {
   			print "Adodb Debug : ";
               adodb_backtrace($e->gettrace());
   			print "<BR><BR>Connection : ".$e->getMessage();
   	}

 }
?>