<?php
//error_reporting(E_ALL);
	class SReport {

		public function paidReport(){
			$resultReturn = $this->_paidReport();
			return $resultReturn;
        }
        public function orderListBuy(){
			$resultReturn = $this->_orderListBuy();
			return $resultReturn;
        }
        public function waitShipping(){
			$resultReturn = $this->_waitShipping();
			return $resultReturn;
        }
        public function orderList(){
			$resultReturn = $this->_orderList();
			return $resultReturn;
        }
        public function shippingList(){
			$resultReturn = $this->_shippingList();
			return $resultReturn;
        }
        public function refundList(){
			$resultReturn = $this->_refundList();
			return $resultReturn;
        }
        public function withdrawList(){
			$resultReturn = $this->_withdrawList();
			return $resultReturn;
        }
        public function walletList(){
			$resultReturn = $this->_walletList();
			return $resultReturn;
        }
        public function rateList(){
			$resultReturn = $this->_rateList();
			return $resultReturn;
        }
        public function reportAllOrder(){
			$resultReturn = $this->_reportAllOrder();
			return $resultReturn;
		}
        public function reportListKiloQ(){
            $resultReturn = $this->_reportListKiloQ();
			return $resultReturn;
        }

        private function _paidReport(){
			$base = Base::getInstance();
            $db = DB::getInstance();
            
            $member = new Member();   
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];

            if($_SESSION['report_payorder_search']['start_date']=='' && $_SESSION['report_payorder_search']['end_date']==''){
                $_SESSION['report_payorder_search']['start_date'] = date('d/m/Y');
                //$_SESSION['report_payorder_search']['end_date'] = date('d/m/Y');
            }
            //$_SESSION['report_payorder_search']['start_date'] = '04/10/2017';
            if($_SESSION['report_payorder_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_payorder_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND project_wallet_log.create_dtm >=".GF::quote($date_check);
            }
            if($_SESSION['report_payorder_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_payorder_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND project_wallet_log.create_dtm <=".GF::quote($date_check);
            }

            $sql = "SELECT project_wallet_log.*,web_order.order_number,
                                project_user.user_name,
                                project_user.user_code
                                FROM project_wallet_log
                                LEFT JOIN project_user ON(project_user.id=project_wallet_log.user_id)
                                LEFT JOIN web_order ON(web_order.id=project_wallet_log.wallet_ref)
                                WHERE  project_wallet_log.wallet_ref_type='1' AND project_user.ref_id=".$db->Quote($user_id)." ".$cond." ORDER BY project_wallet_log.create_dtm DESC";
            
            $res = $db->Execute($sql);
            // $count = $res->NumRows();
            
            // $base->set('allPage',ceil($count/20));
    
            // $page = $base->get('GET.page');
    
            // $numr = 20;
            // $start = "";
            // $end = "";
            // if($page==1||empty($page)){
            //     $start = 0;
            //     $page = 1;
            // }else{
            //     $start = ($page*$numr)-$numr;
            // }
            //     $condNum = " LIMIT ".$start.",".$numr;
    
            //     $base->set('currpage',$page);
    
            // $res = $db->Execute($sql.$condNum);
            $i=0;
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i] = $res->fields;
                $i++;
                $res->MoveNext();
            }
    
    
            return $ArrList;

        }

        private function _waitShipping(){
			$base = Base::getInstance();
            $db = DB::getInstance();
            
            $member = new Member();   
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];

            if($_SESSION['report_waitship_search']['start_date']=='' && $_SESSION['report_waitship_search']['end_date']==''){
                $_SESSION['report_waitship_search']['start_date'] = date('d/m/Y');
                //$_SESSION['report_waitship_search']['end_date'] = date('d/m/Y');
            }
            //$_SESSION['report_waitship_search']['start_date'] = '04/10/2017';
            //$_SESSION['report_waitship_search']['start_date'] = '04/05/2017';
            if($_SESSION['report_waitship_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_waitship_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND web_shipping.create_dtm >=".GF::quote($date_check);
            }
            if($_SESSION['report_waitship_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_waitship_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND web_shipping.create_dtm <=".GF::quote($date_check);
            }

           $sql = "SELECT web_shipping.*,SUM(billing_total+shipping_th_price+shipping_th_service_price+shipping_other_price) AS billing_sum,
                                project_user.user_name,
                                project_user.user_code
                                FROM web_shipping
                                LEFT JOIN project_user ON(project_user.id=web_shipping.create_by)
                                WHERE  web_shipping.status='9' AND project_user.ref_id=".$db->Quote($user_id)." ".$cond." ORDER BY web_shipping.create_dtm DESC";
            
            $res = $db->Execute($sql);

            $i=0;
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i] = $res->fields;
                $i++;
                $res->MoveNext();
            }
    
    
            return $ArrList;

        }
        private function _shippingList(){
			$base = Base::getInstance();
            $db = DB::getInstance();
            
            $member = new Member();   
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];

            if($_SESSION['report_shipping_search']['start_date']=='' && $_SESSION['report_shipping_search']['end_date']==''){
                $_SESSION['report_shipping_search']['start_date'] = date('d/m/Y');
                //$_SESSION['report_shipping_search']['end_date'] = date('d/m/Y');
            }
            //$_SESSION['report_shipping_search']['start_date'] = '04/10/2017';
            //$_SESSION['report_shipping_search']['start_date'] = '04/05/2017';
            if($_SESSION['report_shipping_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_shipping_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND web_shipping.create_dtm >=".GF::quote($date_check);
            }
            if($_SESSION['report_shipping_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_shipping_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND web_shipping.create_dtm <=".GF::quote($date_check);
            }

            $sql = "SELECT web_shipping.*,(web_shipping.billing_total+web_shipping.shipping_th_price+web_shipping.shipping_th_service_price+web_shipping.shipping_other_price) AS billing_sum,
                                project_user.user_name,
                                project_user.user_code
                                FROM web_shipping
                                LEFT JOIN project_user ON(project_user.id=web_shipping.create_by)
                                WHERE  (web_shipping.status='5' OR web_shipping.status='6') AND project_user.ref_id=".$db->Quote($user_id)." ".$cond." ORDER BY web_shipping.create_dtm DESC";
            
            $res = $db->Execute($sql);

            $i=0;
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i] = $res->fields;
                $ArrList[$i]['count_data'] = $this->_shippingItems($res->fields['id']);
                $i++;
                $res->MoveNext();
            }
    
    
            return $ArrList;

        }
        private function _shippingItems($shipping_id){
            
            $base = Base::getInstance();
            $db = DB::getInstance();

            $sql = "SELECT SUM(web_tracking.weight) AS total_weight, 
                            SUM(web_tracking.cbm) AS total_cbm 
                            FROM web_shipping_item
                            LEFT JOIN web_tracking ON(web_tracking.tracking_id=web_shipping_item.tracking_id)
                            WHERE web_shipping_item.shipping_id=".intval($shipping_id);
            $res = $db->Execute($sql);

            return $res->fields;

        }
        private function _orderListBuy(){
            
            $base = Base::getInstance();
            $db = DB::getInstance();
    
            $member = new Member();   
            $order = new SOrdermrg();
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];    
            $dtatReturn=array();

            $cond = '';


            if($_SESSION['report_sendcn_search']['start_date']=='' && $_SESSION['report_sendcn_search']['end_date']==''){
                $_SESSION['report_sendcn_search']['start_date'] = date('d/m/Y');
                //$_SESSION['report_sendcn_search']['end_date'] = date('d/m/Y');
            }
    
            if($_SESSION['report_sendcn_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_sendcn_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND export_date >=".GF::quote($date_check);
            }
            if($_SESSION['report_sendcn_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_sendcn_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND export_date <=".GF::quote($date_check);
            }


    
            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $list_ref = GF::refgroup();
                if(count($list_ref)>0){
                $ref_str = implode(',', $list_ref);
                $cond .= " AND member_id IN(".$ref_str.") ";
                }else{
                $cond .= " AND member_id ='X' ";
                }
            }
            //echo $type;
            $sql = "SELECT * FROM web_order WHERE send_order_status='Y' AND active_status='O' ".$cond." AND export!='N' ORDER BY update_dtm DESC,create_dtm DESC";
    
            $res = $db->Execute($sql);
            $count = $res->NumRows();
    
            $base->set('allPage',ceil($count/20));
    
            $page = $base->get('GET.page');
    
            $numr = 20;
            $start = "";
            $end = "";
            if($page==1||empty($page)){
                $start = 0;
                $page = 1;
            }else{
                $start = ($page*$numr)-$numr;
            }
                $condNum = " LIMIT ".$start.",".$numr;
    
                $base->set('currpage',$page);
    
            $res = $db->Execute($sql.$condNum);
            $i=0;
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i]['id'] = $res->fields['id'];
                $ArrList[$i]['order_id'] = $res->fields['order_id'];
                $ArrList[$i]['order_number'] = $res->fields['order_number'];
                $ArrList[$i]['status'] = $res->fields['status'];
                $ArrList[$i]['order_price'] = $res->fields['order_price'];
                $ArrList[$i]['order_comment'] = $res->fields['order_comment'];
                $ArrList[$i]['ship_by'] = $res->fields['ship_by'];
                $ArrList[$i]['rate'] = $res->fields['rate'];
                $ArrList[$i]['order_pay_ship'] = $res->fields['order_pay_ship'];
                $ArrList[$i]['order_pay_cn'] = $res->fields['order_pay_cn'];
                $ArrList[$i]['order_discount'] = $res->fields['order_discount'];
                $ArrList[$i]['status_text'] = $order->_convStatus('order',$res->fields['status']);
                $base->set('user_id',$res->fields['member_id']);
                $ArrList[$i]['member'] = $member->memberInfomationByID();
                $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
                $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
                $ArrList[$i]['export'] = $res->fields['export'];
                $ArrList[$i]['export_date'] = $res->fields['export_date'];
                $ArrList[$i]['send_order_status'] = $res->fields['send_order_status'];
                $ArrList[$i]['send_order_dtm'] = $res->fields['send_order_dtm'];
                $ArrList[$i]['send_order_round'] = $res->fields['send_order_round'];
                $ArrList[$i]['items'] = $order->_countItem($res->fields['id']);
                $ArrList[$i]['countprice'] = $order->_countItemPrice($res->fields['id']);
                $i++;
                $res->MoveNext();
            }
    
            //print_r();
            $dtatReturn['data'] = $ArrList;
    
            return $dtatReturn;
        }


        private function _orderList(){
            
            $base = Base::getInstance();
            $db = DB::getInstance();
    
            $member = new Member();   
            $order = new SOrdermrg();
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];    
            $dtatReturn=array();

            $cond = '';


            if($_SESSION['report_buyrp_search']['start_date']=='' && $_SESSION['report_buyrp_search']['end_date']==''){
                $_SESSION['report_buyrp_search']['start_date'] = date('d/m/Y');
                //$_SESSION['report_buyrp_search']['end_date'] = date('d/m/Y');
            }
    
            if($_SESSION['report_buyrp_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_buyrp_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND export_date >=".GF::quote($date_check);
            }
            if($_SESSION['report_buyrp_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_buyrp_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND export_date <=".GF::quote($date_check);
            }


    
            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $list_ref = GF::refgroup();
                if(count($list_ref)>0){
                $ref_str = implode(',', $list_ref);
                $cond .= " AND member_id IN(".$ref_str.") ";
                }else{
                $cond .= " AND member_id ='X' ";
                }
            }
            // Edit By Weerasak 25-01-2566
            // กำหนด status = 5 คือเสร็จสิ้น
            $sql = "SELECT member_id,create_dtm,order_number,old_rate,rate
            ,order_price_rmb
            ,ROUND((order_price_rmb * old_rate),2) as order_price_thb
            ,actual_purchase_rmb
            ,ROUND((actual_purchase_rmb * rate),2) actual_purchase_thb
            FROM (
            SELECT o.create_dtm,o.order_number,o.member_id
            ,min(o.old_rate) as old_rate
            ,min(o.rate) as rate
            ,SUM(IFNULL(oi.product_qty,oi.product_buy_qty)) AS qty
            ,SUM(oi.product_price_rmb * oi.product_qty) + o.order_pay_ship_yuan AS order_price_rmb
            ,(SELECT ROUND(SUM((saller_amount_rmb + saller_shipping_rmb)-saller_discount_rmb),2) 
                FROM web_order_item_ship WHERE order_id = oi.order_id
                ) AS actual_purchase_rmb
            FROM web_order o
            INNER JOIN web_order_item oi ON oi.order_id = o.id
            WHERE oi.status != 'C' AND oi.item_typs_s = 'NR' 
            ".$cond."
            AND o.active_status = 'O' 
            AND o.export='S' AND o.status='5'
            GROUP BY o.create_dtm,o.order_number,o.member_id
            ) K 
            ORDER BY create_dtm DESC";
            // $sql = "SELECT * FROM web_order WHERE  active_status='O' ".$cond." AND export='S' AND status='5' ORDER BY update_dtm DESC,create_dtm DESC";

            //echo $sql;
            $res = $db->Execute($sql);
            $count = $res->NumRows();
    
            $base->set('allPage',ceil($count/20));
    
            $page = $base->get('GET.page');
    
            $numr = 20;
            $start = "";
            $end = "";
            if($page==1||empty($page)){
                $start = 0;
                $page = 1;
            }else{
                $start = ($page*$numr)-$numr;
            }
                $condNum = " LIMIT ".$start.",".$numr;
    
                $base->set('currpage',$page);
    
            $res = $db->Execute($sql.$condNum);
            $i=0;
            $ArrList = array();
            // Edit By Weerasak 25-01-2566
            while(!$res->EOF){
                $ArrList[$i]['create_dtm']=$res->fields['create_dtm'];
                $ArrList[$i]['order_number']=$res->fields['order_number'];
                $ArrList[$i]['old_rate']=$res->fields['old_rate'];
                $ArrList[$i]['rate']=$res->fields['rate'];
                $ArrList[$i]['order_price_rmb']=$res->fields['order_price_rmb'];
                $ArrList[$i]['order_price_thb']=$res->fields['order_price_thb'];
                $ArrList[$i]['actual_purchase_rmb']=$res->fields['actual_purchase_rmb'];
                $ArrList[$i]['actual_purchase_thb']=$res->fields['actual_purchase_thb'];
                
                // $ArrList[$i]['id'] = $res->fields['id'];
                // $ArrList[$i]['order_id'] = $res->fields['order_id'];
                // $ArrList[$i]['order_number'] = $res->fields['order_number'];
                // $ArrList[$i]['status'] = $res->fields['status'];
                // $ArrList[$i]['order_price'] = $res->fields['order_price'];
                // $ArrList[$i]['order_comment'] = $res->fields['order_comment'];
                // $ArrList[$i]['ship_by'] = $res->fields['ship_by'];
                // $ArrList[$i]['old_rate'] = $res->fields['old_rate'];
                // $ArrList[$i]['rate'] = $res->fields['rate'];
                // $ArrList[$i]['order_pay_ship'] = $res->fields['order_pay_ship'];
                // $ArrList[$i]['order_pay_cn'] = $res->fields['order_pay_cn'];
                // $ArrList[$i]['order_discount'] = $res->fields['order_discount'];
                // $ArrList[$i]['status_text'] = $order->_convStatus('order',$res->fields['status']);
                 $base->set('user_id',$res->fields['member_id']);
                 $ArrList[$i]['member'] = $member->memberInfomationByID();
                // $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
                // $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
                // $ArrList[$i]['export'] = $res->fields['export'];
                // $ArrList[$i]['export_date'] = $res->fields['export_date'];
                // $ArrList[$i]['send_order_status'] = $res->fields['send_order_status'];
                // $ArrList[$i]['send_order_dtm'] = $res->fields['send_order_dtm'];
                // $ArrList[$i]['send_order_round'] = $res->fields['send_order_round'];
                // $ArrList[$i]['items'] = $order->_countItem($res->fields['id']);
                // $ArrList[$i]['countprice'] = $order->_countItemPrice($res->fields['id']);
                $i++;
                $res->MoveNext();
            }
            // END
            //print_r();
            $dtatReturn['data'] = $ArrList;
    
            return $dtatReturn;
        }

        private function _refundList(){
            $base = Base::getInstance();
            $db = DB::getInstance();
            $db2 = DB2::getInstance();
       
            $member = new Member();
            $order = new SOrdermrg();

       
            $memberInfomation = $member->memberInfomation();
    
            $user_id = $memberInfomation['id'];
            $cond_search= '';
            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $list_ref = GF::refgroup();
                if(count($list_ref)>0){
                //GF::print_r($list_ref);
                $ref_str = implode(',', $list_ref);
                $cond_search .= " AND create_by IN(".$ref_str.") ";
                }else{
                $cond_search .= " AND create_by ='X' ";
                }
            }
    
            
            $cond = '';
            
            
            if($_SESSION['report_refundrp_search']['start_date']=='' && $_SESSION['report_refundrp_search']['end_date']==''){
                $_SESSION['report_refundrp_search']['start_date'] = date('d/m/Y');
            }
    
            if($_SESSION['report_refundrp_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_refundrp_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND create_dtm >=".GF::quote($date_check);
            }
            if($_SESSION['report_refundrp_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_refundrp_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $cond .= " AND create_dtm <=".GF::quote($date_check);
            }
       
       
           $sql = "SELECT * FROM project_item_refund WHERE status='Refunded' ".$cond_search.$cond."  ORDER BY ref_id DESC";

            $res = $db->Execute($sql);
 
            $i=0;
            $ArrList = array();
            while(!$res->EOF){
               $ArrList[$i]['ref_id'] = $res->fields['ref_id'];
               $ArrList[$i]['item_id'] = $res->fields['item_id'];
               $ArrList[$i]['ref_refund'] = $res->fields['ref_refund'];
               $ArrList[$i]['ref_item'] = $res->fields['ref_item'];
               $ArrList[$i]['ref_file'] = $res->fields['ref_file'];
               $ArrList[$i]['ref_file2'] = $res->fields['ref_file2'];
               $ArrList[$i]['ref_rmb'] = $res->fields['ref_rmb'];
               $ArrList[$i]['ref_thb'] = $res->fields['ref_thb'];
               $ArrList[$i]['ref_rate'] = $res->fields['ref_rate'];
               $ArrList[$i]['ref_comment'] = $res->fields['ref_comment'];
               $ArrList[$i]['ref_admin'] = $res->fields['ref_admin'];
               $ArrList[$i]['status'] = $res->fields['status'];
               $ArrList[$i]['order_number'] = $order->_getOrderNumber($res->fields['item_id']);
               $ArrList[$i]['create_by'] = $res->fields['create_by'];
               $ArrList[$i]['create_dtm'] = $res->fields['create_dtm'];
               $ArrList[$i]['update_dtm'] = $res->fields['update_dtm'];
               $ArrList[$i]['export'] = $res->fields['export'];
               $ArrList[$i]['export_date'] = $res->fields['export_date'];
               $base->set('_ids',$res->fields['item_id']);
               $ArrList[$i]['item_info'] = $order->_itemInfo();
               $base->set('_ids',$ArrList[$i]['item_info']['order_id']);
               $ArrList[$i]['order_info'] = $order->_orderInfo();
               $i++;
               $res->MoveNext();
            }
            //print_r($ArrList);
            return $ArrList;
        }

        private function  _withdrawList(){
            $base 	= Base::getInstance();
            $db 	= DB2::getInstance();

            $member = new Member();


            $memberInfomation = $member->memberInfomation();

            $user_id = $memberInfomation['id'];
 
            $arrSearch = array();
            $arrSearch['status']='A';
    
            $memberInfomation = $member->memberInfomation();
    
            $user_id = $memberInfomation['id'];
            $cond_search= '';
            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $list_ref = GF::refgroup();
                if(count($list_ref)>0){
                    $arrSearch['user_id'] = $list_ref;
                //$cond_search .= " AND create_by IN(".$ref_str.") ";
                }else{
                    $arrSearch['user_id'] = 'X';
                }
            }
 
            if($_SESSION['report_withdrawrp_search']['start_date']=='' && $_SESSION['report_withdrawrp_search']['end_date']==''){
                $_SESSION['report_withdrawrp_search']['start_date'] = date('d/m/Y');
            }
    
            if($_SESSION['report_withdrawrp_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_withdrawrp_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $arrSearch['create_dtm[>=]'] = $date_check;
            }
            if($_SESSION['report_withdrawrp_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_withdrawrp_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $arrSearch['create_dtm[<=]'] = $date_check;
            }
 
            $result = $db->select("project_withdraw","*",array(
                "AND"=>$arrSearch,
                "ORDER"=>"wd_id DESC"

            ));
            //echo $db->last_query();
 
 
            return $result;
        }

        private function  _walletList(){
            $base 	= Base::getInstance();
            $db 	= DB2::getInstance();

            $member = new Member();


            $memberInfomation = $member->memberInfomation();

            $user_id = $memberInfomation['id'];
 
            $arrSearch = array();
           
    
            $memberInfomation = $member->memberInfomation();
    
            $user_id = $memberInfomation['id'];
            $cond_search= '';
            $agency =  $memberInfomation['agency'];
            // if($agency=='A'){
            //     $list_ref = GF::refgroup();
            //     if(count($list_ref)>0){
            //         $arrSearch['user_id'] = $list_ref;
            //     //$cond_search .= " AND create_by IN(".$ref_str.") ";
            //     }else{
            //         $arrSearch['user_id'] = 'X';
            //     }
            // }
 
            if($_SESSION['report_walletrp_search']['start_date']=='' && $_SESSION['report_walletrp_search']['end_date']==''){
                $_SESSION['report_walletrp_search']['start_date'] = date('d/m/Y');
            }
    
            if($_SESSION['report_walletrp_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_walletrp_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $arrSearch['create_dtm[>=]'] = $date_check;
            }
            if($_SESSION['report_walletrp_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_walletrp_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $arrSearch['create_dtm[<=]'] = $date_check;
            }

            $arrSearch['wallet_ref_type'] = '5';
 
            $result = $db->select("project_wallet_log","*",array(
                "AND"=>$arrSearch,
                "ORDER"=>"id ASC"

            ));
            //echo $db->last_query();
            // $result = $db->select("project_withdraw","*",array(
            //     "AND"=>$arrSearch,
            //     "ORDER"=>"id DESC"

            // ));
 
 
            return $result;
        }


        private function  _rateList(){
            $base 	= Base::getInstance();
            $db 	= DB2::getInstance();
 
            $arrSearch = array();
           
    
            
 
            if($_SESSION['report_raterp_search']['start_date']=='' && $_SESSION['report_raterp_search']['end_date']==''){
                $_SESSION['report_raterp_search']['start_date'] = date('d/m/Y');
            }
    
            if($_SESSION['report_raterp_search']['start_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_raterp_search']['start_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $arrSearch['create_dtm[>=]'] = $date_check;
            }
            if($_SESSION['report_raterp_search']['end_date']!=''){
                $vdate = str_replace("/",'-',$_SESSION['report_raterp_search']['end_date']);
                $date_check = date('Y-m-d',strtotime($vdate));
                $arrSearch['create_dtm[<=]'] = $date_check;
            }


 
            $result_1 = $db->select("web_rate","*",array(
                "AND"=>$arrSearch,
                "ORDER"=>"id ASC"

            ));
            $result_2 = $db->select("web_rate_2","*",array(
                "AND"=>$arrSearch,
                "ORDER"=>"id ASC"

            ));
            $result_3 = $db->select("web_rate_3","*",array(
                "AND"=>$arrSearch,
                "ORDER"=>"id ASC"

            ));
            //echo $db->last_query();
            // $result = $db->select("project_withdraw","*",array(
            //     "AND"=>$arrSearch,
            //     "ORDER"=>"id DESC"

            // ));
            $arrReturn = array();
            $arrReturn['rate_1'] = $result_1;
            $arrReturn['rate_2'] = $result_2;
            $arrReturn['rate_3'] = $result_3;

            return $arrReturn;
        }

        private function _reportAllOrder(){
            
            $base = Base::getInstance();
            $db = DB::getInstance();
    
            $member = new Member();   
            //$order = new SOrdermrg();
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];    
            $dtatReturn=array();

            $agency =  $memberInfomation['agency'];
            if($agency=='A'){
                $cond .= " AND project_user.ref_id=".intval($user_id);
            }
            //echo $type;

           $sql = "SELECT SUM(web_order.order_price-web_order.refund_wallet) AS total_price,
                            DATE_FORMAT(web_order.create_dtm,'%Y-%m') AS date_data
                                    FROM web_order
                                    LEFT JOIN project_user ON(project_user.id=web_order.member_id) 
                                    WHERE  web_order.active_status='O' ".$cond." AND web_order.export='S' AND web_order.status='3'
                                    GROUP BY DATE_FORMAT(web_order.create_dtm,'%Y-%m')
                                    ORDER BY web_order.update_dtm DESC,web_order.create_dtm DESC";
    
            $res = $db->Execute($sql);
           // exit();
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i] = $res->fields;
                $i++;
                $res->MoveNext();
            }
    
            return $ArrList;
        }

        private function _reportAllAgency(){
            
            $base = Base::getInstance();
            $db = DB::getInstance();
    
            $member = new Member();   
            $memberInfomation = $member->memberInfomation();
            $user_id = $memberInfomation['id'];    
            $dtatReturn=array();

            


            //echo $type;
            $sql = "SELECT project_user.user_code,project_user.user_name,project_user.id
                                        FROM project_user
                                        LEFT JOIN project_role ON(project_role.id=project_user.user_role_id)
                                        WHERE project_role.email_account='A' AND project_user.active_status='O'
                                        ORDER BY project_user.id ASC";
    
            $res = $db->Execute($sql);
            
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i] = $res->fields;
                $i++;
                $res->MoveNext();
            }
    
            return $ArrList;
        }
        private function _reportAllOrderByuser($user_id){
            
            $base = Base::getInstance();
            $db = DB::getInstance();
    
            $member = new Member();   

            $dtatReturn=array();

            $cond .= " AND project_user.ref_id=".intval($user_id);

            //echo $type;
            $sql = "SELECT SUM(web_order.order_price-web_order.refund_wallet) AS total_price,
                            
                                    FROM web_order
                                    LEFT JOIN project_user ON(project_user.id=web_order.member_id) 
                                    WHERE  web_order.active_status='O' ".$cond." AND web_order.export='S' AND web_order.status='3'
                                    GROUP BY DATE_FORMAT(web_order.create_dtm,'%Y-%m')
                                    ORDER BY web_order.update_dtm DESC,web_order.create_dtm DESC";
    
            $res = $db->Execute($sql);
            
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i] = $res->fields;
                $i++;
                $res->MoveNext();
            }
    
            return $ArrList;
        }
        // Edit By Weerasak 06-02-2566
        private function _reportListKiloQ(){
            $base = Base::getInstance();
            $db = DB::getInstance();

            $vdate = str_replace("/",'-',$_SESSION['report_kiloq_search']['start_date']);
            $date1 = date('Y-m-d',strtotime($vdate));
            $vdate = str_replace("/",'-',$_SESSION['report_kiloq_search']['end_date']);
            $date2 = date('Y-m-d',strtotime($vdate));
            $sql = "SELECT user_code
            ,K.shipping_code
            ,SUM(CASE WHEN unit_code IN ('K','Q') THEN cbm ELSE 0 END) AS Q
            ,SUM(CASE WHEN unit_code = 'QB' THEN cbm ELSE 0 END) AS QB
            ,SUM(CASE WHEN unit_code = 'QM' THEN cbm ELSE 0 END) AS QM
            ,SUM(CASE WHEN unit_code = 'KSea' THEN cbm ELSE 0 END) AS KSea-- เรือ ธรรมดา
            ,SUM(CASE WHEN unit_code = 'QSea' THEN cbm ELSE 0 END) AS QSea -- คิวเรือ ธรรมดา
            ,SUM(CASE WHEN unit_code IN ('KBSea','QBSea') THEN cbm ELSE 0 END) AS QBSea -- คิวเรือ แบรนด์
            ,SUM(CASE WHEN unit_code = 'QMSea' THEN cbm ELSE 0 END) AS QMSea -- คิวเรือ มอก
            ,s.shipping_th_price
            ,s.shipping_th_service_price
            FROM (
            SELECT DISTINCT u.user_code
            ,t.shipping_code
            ,round(t.cbm,4) as cbm
            ,r.unit_code
            FROM web_tracking t
            INNER JOIN project_user u ON u.user_code = t.member_code
            INNER JOIN project_user_rate r ON r.user_id = u.user_role_id
            WHERE t.ship_by = r.ship_by 
            AND t.order_type = r.order_type
            AND substr(t.price_rate,1,1) = substr(r.unit_code,1,1)
            AND date(t.update_dtm) BETWEEN ".GF::quote($date1)." AND ".GF::quote($date2)."
            ) K
            LEFT JOIN web_shipping s ON s.shipping_code = K.shipping_code
            WHERE K.shipping_code IS NOT NULL
            GROUP BY K.user_code,K.shipping_code,shipping_th_price,shipping_th_service_price
            ORDER BY K.user_code,K.shipping_code";
            
            $res = $db->Execute($sql);

            $count = $res->NumRows();
    
            $base->set('allPage',ceil($count/20));
    
            $page = $base->get('GET.page');
    
            $numr = 20;
            $start = "";
            $end = "";
            if($page==1||empty($page)){
                $start = 0;
                $page = 1;
            }else{
                $start = ($page*$numr)-$numr;
            }
                $condNum = " LIMIT ".$start.",".$numr;
    
                $base->set('currpage',$page);
    
            $res = $db->Execute($sql.$condNum);

            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i]['user_code'] = $res->fields['user_code'];
                $ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
                $ArrList[$i]['Q'] = $res->fields['Q'];
                $ArrList[$i]['QB'] = $res->fields['QB'];
                $ArrList[$i]['QM'] = $res->fields['QM'];
                $ArrList[$i]['KSea'] = $res->fields['KSea'];
                $ArrList[$i]['QSea'] = $res->fields['QSea'];
                $ArrList[$i]['QBSea'] = $res->fields['QBSea'];
                $ArrList[$i]['QMSea'] = $res->fields['QMSea'];
                $ArrList[$i]['shipping_th_price'] = $res->fields['shipping_th_price'];
                $ArrList[$i]['shipping_th_service_price'] = $res->fields['shipping_th_service_price'];
                $i++;
                $res->MoveNext();
            }
            return $ArrList;
        }

        public function exportListKiloQ(){
            $base = Base::getInstance();
            $db = DB::getInstance();

            $vdate = str_replace("/",'-',$_SESSION['report_kiloq_search']['start_date']);
            $date1 = date('Y-m-d',strtotime($vdate));
            $vdate = str_replace("/",'-',$_SESSION['report_kiloq_search']['end_date']);
            $date2 = date('Y-m-d',strtotime($vdate));
            $sql = "SELECT user_code
            ,K.shipping_code
            ,SUM(CASE WHEN unit_code IN ('K','Q') THEN cbm ELSE 0 END) AS Q
            ,SUM(CASE WHEN unit_code = 'QB' THEN cbm ELSE 0 END) AS QB
            ,SUM(CASE WHEN unit_code = 'QM' THEN cbm ELSE 0 END) AS QM
            ,SUM(CASE WHEN unit_code = 'KSea' THEN cbm ELSE 0 END) AS KSea-- เรือ ธรรมดา
            ,SUM(CASE WHEN unit_code = 'QSea' THEN cbm ELSE 0 END) AS QSea -- คิวเรือ ธรรมดา
            ,SUM(CASE WHEN unit_code IN ('KBSea','QBSea') THEN cbm ELSE 0 END) AS QBSea -- คิวเรือ แบรนด์
            ,SUM(CASE WHEN unit_code = 'QMSea' THEN cbm ELSE 0 END) AS QMSea -- คิวเรือ มอก
            ,s.shipping_th_price
            ,s.shipping_th_service_price
            FROM (
            SELECT DISTINCT u.user_code
            ,t.shipping_code
            ,round(t.cbm,4) as cbm
            ,r.unit_code
            FROM web_tracking t
            INNER JOIN project_user u ON u.user_code = t.member_code
            INNER JOIN project_user_rate r ON r.user_id = u.user_role_id
            WHERE t.ship_by = r.ship_by 
            AND t.order_type = r.order_type
            AND substr(t.price_rate,1,1) = substr(r.unit_code,1,1)
            AND date(t.update_dtm) BETWEEN ".GF::quote($date1)." AND ".GF::quote($date2)."
            ) K
            LEFT JOIN web_shipping s ON s.shipping_code = K.shipping_code
            WHERE K.shipping_code IS NOT NULL
            GROUP BY K.user_code,K.shipping_code,shipping_th_price,shipping_th_service_price
            ORDER BY K.user_code,K.shipping_code";
            
            $res = $db->Execute($sql);
            $ArrList = array();
            while(!$res->EOF){
                $ArrList[$i]['user_code'] = $res->fields['user_code'];
                $ArrList[$i]['shipping_code'] = $res->fields['shipping_code'];
                $ArrList[$i]['Q'] = $res->fields['Q'];
                $ArrList[$i]['QB'] = $res->fields['QB'];
                $ArrList[$i]['QM'] = $res->fields['QM'];
                $ArrList[$i]['KSea'] = $res->fields['KSea'];
                $ArrList[$i]['QSea'] = $res->fields['QSea'];
                $ArrList[$i]['QBSea'] = $res->fields['QBSea'];
                $ArrList[$i]['QMSea'] = $res->fields['QMSea'];
                $ArrList[$i]['shipping_th_price'] = $res->fields['shipping_th_price'];
                $ArrList[$i]['shipping_th_service_price'] = $res->fields['shipping_th_service_price'];
                $i++;
                $res->MoveNext();
            }
            return $ArrList;
        }
        //END

}

?>
