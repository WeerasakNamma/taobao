<?php
 class Leaves {
 	
 	
 	
 	
 	
 	public function createLeave(){
		$resultreturn = $this->_createLeave();
		return $resultreturn;
	}
	public function leaveList(){
		$resultreturn = $this->_leaveList();
		return $resultreturn;
	}
	public function leaveInfomation(){
		$resultreturn = $this->_leaveInfomation();
		return $resultreturn;
	}
	public function updateLeave(){
		$resultreturn = $this->_updateLeave();
		return $resultreturn;
	}
	public function deleteLeave(){
		$resultreturn = $this->_deleteLeave();
		return $resultreturn;
	}
	public function deleteLeaveItem(){
		$resultreturn = $this->_deleteLeaveItem();
		return $resultreturn;
	}
	public function cancelLeave(){
		$resultreturn = $this->_cancelLeave();
		return $resultreturn;
	}
	
	
 	
 	private function _createLeave(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$member = new Member();
 		
 		$memberInfomation = $member->memberInfomation();
 		
 		$user_id = $memberInfomation['id'];
 		
 		$picsave = '';
 		
 		$new_user_id = $base->get('POST.user_id');
 		if($new_user_id!=''){
			$user_id = $new_user_id;
		}
 		
 		if($_FILES['leave_image']['name']){
			$picname = $_FILES['leave_image']['name'];
			
			$filetype = explode('.',$picname);
			$picname = GF::randomStr(25);
			
			$dest_picname_o = $base->get('BASEDIR').'/uploads/leave/'.$picname."_original.".end($filetype);
			
			$tmp_file = $_FILES['leave_image']['tmp_name'];
			if(copy($tmp_file, $dest_picname_o)){
				$picsave = $picname."_original.".end($filetype);
			}
		}
 		
 		
 		$sql = "INSERT INTO project_leave (
											  user_id,
											  leave_title,
											  leave_type,
											  leave_desc,
											  leave_image,
											  status,
											  create_dtm
											)VALUES(
												".GF::quote($user_id).",
												".GF::quote($base->get('POST.leave_title')).",
												".GF::quote($base->get('POST.leave_type')).",
												".GF::quote($base->get('POST.leave_desc')).",
												".GF::quote($picsave).",
												'O',
												NOW()
											)";

		$res = $db->Execute($sql);
		
		if($res){
			$leave_id = $db->Insert_ID();
			$leave_type = $base->get('POST.leave_type');
			if($leave_type=='5'){
				
				$item_date = $base->get('POST.item_date_e');
				$item_type = $base->get('POST.item_type_e');
				
				$item_date = str_replace('/','-',$item_date);
				$item_date = date("Y-m-d", strtotime($item_date));
				$sql = "INSERT INTO project_leave_item (
													  leave_id,
													  item_date,
													  item_type,
													  status
													)VALUES(
														".GF::quote($leave_id).",
														".GF::quote($item_date).",
														".GF::quote($item_type).",
														'O'
													)";
				$res = $db->Execute($sql);
				
				if($res){
					//$item_id = $db->Insert_ID();
					
					
					$item_date_1 = $base->get('POST.item_date_early_1');
					$item_type_1 = $base->get('POST.item_type_early_1');
					
					$item_date_1 = str_replace('/','-',$item_date_1);
					$item_date_1 = date("Y-m-d", strtotime($item_date_1));
					$sql = "INSERT INTO project_leave_early (
														  leave_id,
														  item_date,
														  item_type,
														  status
														)VALUES(
															".GF::quote($leave_id).",
															".GF::quote($item_date_1).",
															".GF::quote($item_type_1).",
															'O'
														)";
					$res = $db->Execute($sql);
					if($item_type=='2'){
						$item_date_2 = $base->get('POST.item_date_early_2');
						$item_type_2 = $base->get('POST.item_type_early_2');
						
						$item_date_2 = str_replace('/','-',$item_date_2);
						$item_date_2 = date("Y-m-d", strtotime($item_date_2));
						$sql = "INSERT INTO project_leave_early (
															  leave_id,
															  item_date,
															  item_type,
															  status
															)VALUES(
																".GF::quote($leave_id).",
																".GF::quote($item_date_2).",
																".GF::quote($item_type_2).",
																'O'
															)";
						$res = $db->Execute($sql);
					}
				}
				
				
			}else{
				$item_date = $base->get('POST.item_date');
				$item_type = $base->get('POST.item_type');
				foreach($item_date as $key=>$itemValue){
					$itemValue = str_replace('/','-',$itemValue);
					$itemValue = date("Y-m-d", strtotime($itemValue));
					$sql = "INSERT INTO project_leave_item (
														  leave_id,
														  item_date,
														  item_type,
														  status
														)VALUES(
															".GF::quote($leave_id).",
															".GF::quote($itemValue).",
															".GF::quote($item_type[$key]).",
															'O'
														)";
					$res = $db->Execute($sql);
				}
			}

			$base->set('_leave_id_',$leave_id);
			$this->_sendNotification('created');
			return true;
		}else{
			return false;
		}
 		
	}
	
	private function _sendNotification($type){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$mail = Mailer::getInstance();
 		
 		$member = new Member();
 		$memberList = $member->memberList();
 		$base->set('_userid',$base->get('_leave_id_'));
 		$leaveInfomation = $this->_leaveInfomation();
 		$memberInfomation = $member->memberInfomation();
 		
 		
        $leave_type = 'ลากิจหักเงิน';
        if($leaveInfomation['leave_type']=='2'){
			$leave_type = 'ลากิจรับเงิน';
		}
		else if($leaveInfomation['leave_type']=='3'){
			$leave_type = 'ลาป่วย';
		}
		else if($leaveInfomation['leave_type']=='4'){
			$leave_type = 'ลาพักร้อน';
		}   
		else if($leaveInfomation['leave_type']=='5'){
			$leave_type = 'ขอออกก่อนเวลา';
		}              	
 		
 		$mailHTML = "<b>".$memberInfomation['user_name']."</b> just ".$type." leave : <b>".$leaveInfomation['leave_title']."</b><br />";
 		foreach($leaveInfomation['item'] as $items){
			$mailHTML .= "<br /> <b>Date : <b>".date("d-m-Y",strtotime($items['item_date']))."</b>";
			$dateType = '<b> Full Day</b>';
			if($items['item_type']=='A'){
				$dateType = '<b> Half Day - Afternoon</b>';
			}
			else if($items['item_type']=='M'){
				$dateType = '<b> Half Day - Morning</b>';
			}
			else if($items['item_type']=='1'){
				$dateType = '<b> 1 Hour</b>';
			}else if($items['item_type']=='2'){
				$dateType = '<b> 2 Hour</b>';
			}
			$mailHTML .= $dateType;
		}
		if($leaveInfomation['leave_type']=='5'){
				foreach($leaveInfomation['early'] as $items){
					$mailHTML .= "<br /> <b>Make up Date : <b>".date("d-m-Y",strtotime($items['item_date']))."</b>";
					//$dateType = '<b> Full Day</b>';
					if($items['item_type']=='A'){
						$dateType = '<b> Afternoon (18.00-19.00)</b>';
					}
					else if($items['item_type']=='M'){
						$dateType = '<b> Morning (8.00-9.00)</b>';
					}
					$mailHTML .= $dateType;
				}
		}
 		$mailHTML .= "<br />Leave Type : <b>".$leave_type."</b>";
 		$mailHTML .= "<br />Description : <b>".$leaveInfomation['leave_desc']."</b>";
 		$mailHTML .= "<br />Create : <b>".date("d-m-Y H:i:s",strtotime($leaveInfomation['create_dtm']))."</b>";
 		
 		/*foreach($memberList as $mailMember){
			$mail->Subject = '[Leave Mail]::'.$memberInfomation['user_name']." Create Leave :".$leave_type;
			$mail->msgBody = $mailHTML;
			
			$mail->addAddressEmail = $mailMember['user_email'];
			$mail->addAddressName = $mailMember['user_name'];
			$mail->sendMail();
		}*/
		$mail->Subject = '[Leave Mail]::'.$memberInfomation['user_name']." created leave :".$leave_type;
		$mail->msgBody = $mailHTML;
			
		$mail->addAddressEmail = $memberInfomation['user_email'];
		$mail->addAddressName = $memberInfomation['user_name'];
		$mail->sendMail();
		
		$mail->Subject = '[Leave Mail]::'.$memberInfomation['user_name']." created leave :".$leave_type;
		$mail->msgBody = $mailHTML;
			
		$mail->addAddressEmail = 'leave@intervisionbiz.com';
		$mail->addAddressName = 'Leave Intervisionbiz';
		$mail->sendMail();
 		
	}
	
	public function sendNotificationApprove($type,$leave_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		$mail = Mailer::getInstance();
 		
 		$member = new Member();
 		//$memberList = $member->memberList();
 		$base->set('_userid',$leave_id);
 		$leaveInfomation = $this->_leaveInfomation();
 		$base->set('user_id',$leaveInfomation['user_id']);
 		$memberInfomation = $member->memberInfomationByID();
 		$memberInfomation2 = $member->memberInfomation();
 		
 		
        $leave_type = 'ลากิจหักเงิน';
        if($leaveInfomation['leave_type']=='2'){
			$leave_type = 'ลากิจรับเงิน';
		}
		else if($leaveInfomation['leave_type']=='3'){
			$leave_type = 'ลาป่วย';
		}
		else if($leaveInfomation['leave_type']=='4'){
			$leave_type = 'ลากพักร้อน';
		}                	
 		
 		$mailHTML = "<b>".$memberInfomation2['user_name']."</b> has changed status leave : <b>".$leaveInfomation['leave_title']."</b><br />";
 		if($type=='R'){
			$mailHTML .= "<br /> Status : <b><span style='color:red'>Cancel</span></b>";
		}else{
			$mailHTML .= "<br /> Status : <b><span style='color:green'>Approval</span></b>";
		}
 		
 		foreach($leaveInfomation['item'] as $items){
			$mailHTML .= "<br /> <b>Date : <b>".date("d-m-Y",strtotime($items['item_date']))."</b>";
			$dateType = '<b> Full Day</b>';
			if($items['item_type']=='A'){
				$dateType = '<b> Half Day - Afternoon</b>';
			}
			else if($items['item_type']=='M'){
				$dateType = '<b> Half Day - Morning</b>';
			}
			$mailHTML .= $dateType;
		}
 		$mailHTML .= "<br />Leave Type : <b>".$leave_type."</b>";
 		$mailHTML .= "<br />Description : <b>".$leaveInfomation['leave_desc']."</b>";
 		$mailHTML .= "<br />Create : <b>".date("d-m-Y H:i:s",strtotime($leaveInfomation['create_dtm']))."</b>";
 		
 		
			$mail->Subject = '[Leave Mail]::'.$memberInfomation2['user_name']." has changed status leave :".$leaveInfomation['leave_title'];
			$mail->msgBody = $mailHTML;
			
			$mail->addAddressEmail = $memberInfomation['user_email'];
			$mail->addAddressName = $memberInfomation['user_name'];
			$mail->sendMail();
			
			$mail->Subject = '[Leave Mail]::'.$memberInfomation2['user_name']." has changed status leave :".$leaveInfomation['leave_title'];
			$mail->msgBody = $mailHTML;
			
			$mail->addAddressEmail = 'leave@intervisionbiz.com';
			$mail->addAddressName = 'Leave Intervisionbiz';
			$mail->sendMail();
		
 		
	}
	

	private function _leaveList(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$member = new Member();
 		
 		$memberInfomation = $member->memberInfomation();
 		
 		$user_id = $memberInfomation['id'];
 		
		$sql = "SELECT * FROM project_leave WHERE user_id=".GF::quote($user_id)." AND status='O' ORDER BY leave_id DESC";
		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['leave_id'];
			$arrReturn[$i]['leave_title'] = $res->fields['leave_title'];
			$arrReturn[$i]['leave_type'] = $res->fields['leave_type'];
			$arrReturn[$i]['leave_desc'] = $res->fields['leave_desc'];
			$arrReturn[$i]['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn[$i]['approve'] = $res->fields['approve'];
			
			$arrReturn[$i]['item'] = $this->_leaveItem($res->fields['leave_id']);
			
			
			
			$total_day = 0;
			foreach($arrReturn[$i]['item'] as $vals){
				if($vals['item_type']=='F'){
					$total_day = $total_day+1;
				}else{
					$total_day = $total_day+0.5;
				}
			}
			if($res->fields['leave_type']=='5'){
				$arrReturn[$i]['early'] = $this->_leaveEarltItem($res->fields['leave_id']);
				$total_day = intval($arrReturn[$i]['item'][0]['item_type']);
			}
			$arrReturn[$i]['total_day'] =  $total_day;
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	
	private function _leaveItem($leave_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		
		$sql = "SELECT * FROM project_leave_item WHERE leave_id=".GF::quote($leave_id)." ORDER BY item_id ASC";
		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['item_id'];
			$arrReturn[$i]['item_date'] = $res->fields['item_date'];
			$arrReturn[$i]['item_type'] = $res->fields['item_type'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	private function _leaveEarltItem($leave_id){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		
		$sql = "SELECT * FROM project_leave_early WHERE leave_id=".GF::quote($leave_id)." ORDER BY item_id ASC";
		$res = $db->Execute($sql);
		$arrReturn = array();
		$i = 0;
		while(!$res->EOF){
			$arrReturn[$i]['id'] = $res->fields['item_id'];
			$arrReturn[$i]['item_date'] = $res->fields['item_date'];
			$arrReturn[$i]['item_type'] = $res->fields['item_type'];
			$i++;
			$res->MoveNext();
		}
		$res->Close();
		return 	$arrReturn;
	}
	private function _leaveInfomation(){
 		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$leave_id = $base->get('_userid');
 		//echo $token;
 		if($leave_id!=''){
			$sql = "SELECT * FROM project_leave WHERE leave_id=".GF::quote($leave_id); 

			$db->SetFetchMode(ADODB_FETCH_ASSOC);
			$res = $db->Execute($sql);
			$arrReturn = array();
			
			$arrReturn['id'] = $res->fields['leave_id'];
			$arrReturn['leave_title'] = $res->fields['leave_title'];
			$arrReturn['leave_type'] = $res->fields['leave_type'];
			$arrReturn['leave_desc'] = $res->fields['leave_desc'];
			$arrReturn['leave_image'] = $res->fields['leave_image'];
			$arrReturn['create_dtm'] = $res->fields['create_dtm'];
			$arrReturn['user_id'] = $res->fields['user_id'];
			$arrReturn['item'] = $this->_leaveItem($res->fields['leave_id']);
			if($res->fields['leave_type']=='5'){
				$arrReturn['early'] = $this->_leaveEarltItem($res->fields['leave_id']);
			}
			//GF::print_r($arrReturn);
			return $arrReturn;
		}else{
			return NULL;
		}
		
	}
	
	private function _updateLeave(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$leave_id = $base->get('POST.leave_id');
 		
 		if(!empty($leave_id)){
 			
 			$picsave = '';
 		
	 		if($_FILES['leave_image']['name']){
				$picname = $_FILES['leave_image']['name'];
				
				$filetype = explode('.',$picname);
				$picname = GF::randomStr(25);
				
				$dest_picname_o = $base->get('BASEDIR').'/uploads/leave/'.$picname."_original.".end($filetype);
				
				$tmp_file = $_FILES['leave_image']['tmp_name'];
				if(copy($tmp_file, $dest_picname_o)){
					$picsave = $picname."_original.".end($filetype);
					$sql = "UPDATE project_leave SET 
													leave_image=".GF::quote($picsave)." 
													WHERE leave_id=".GF::quote($leave_id);
					$res = $db->Execute($sql);
					if(is_file($base->get('BASEDIR')."/uploads/leave/".$base->get('POST.old_thumb'))){
						@unlink($base->get('BASEDIR')."/uploads/leave/".$base->get('POST.old_thumb'));
					}
				}
			}
 			
			$sql = "UPDATE project_leave SET 
											leave_title=".GF::quote($base->get('POST.leave_title')).",
											leave_type=".GF::quote($base->get('POST.leave_type')).",
											leave_desc=".GF::quote($base->get('POST.leave_desc')).",
											update_dtm=NOW() 
											WHERE leave_id=".GF::quote($leave_id);
			$res = $db->Execute($sql);
			
			$item_del = $base->get('POST.item_del');
			
			$sql = "DELETE FROM project_leave_item WHERE leave_id=".GF::quote($leave_id);
			$res = $db->Execute($sql);
				
			$item_date = $base->get('POST.item_date');
			$item_date_e = $base->get('POST.item_date_e');
			$leave_type = $base->get('POST.leave_type');
			if($item_date[0]!='' || $item_date_e != ''){
				$item_type = $base->get('POST.item_type');
				if($leave_type=='5'){
					
					$item_date = $base->get('POST.item_date_e');
					$item_type = $base->get('POST.item_type_e');
					
					$item_date = str_replace('/','-',$item_date);
					$item_date = date("Y-m-d", strtotime($item_date));
					$sql = "INSERT INTO project_leave_item (
														  leave_id,
														  item_date,
														  item_type,
														  status
														)VALUES(
															".GF::quote($leave_id).",
															".GF::quote($item_date).",
															".GF::quote($item_type).",
															'O'
														)";
					$res = $db->Execute($sql);
					$sql = "DELETE FROM project_leave_early WHERE leave_id=".GF::quote($leave_id);
					//echo $sql;
					$res = $db->Execute($sql);
					if($res){
						//$item_id = $db->Insert_ID();
						
						
						$item_date_1 = $base->get('POST.item_date_early_1');
						$item_type_1 = $base->get('POST.item_type_early_1');
						
						$item_date_1 = str_replace('/','-',$item_date_1);
						$item_date_1 = date("Y-m-d", strtotime($item_date_1));
						$sql = "INSERT INTO project_leave_early (
															  leave_id,
															  item_date,
															  item_type,
															  status
															)VALUES(
																".GF::quote($leave_id).",
																".GF::quote($item_date_1).",
																".GF::quote($item_type_1).",
																'O'
															)";
						$res = $db->Execute($sql);
						if($item_type=='2'){
							$item_date_2 = $base->get('POST.item_date_early_2');
							$item_type_2 = $base->get('POST.item_type_early_2');
							
							$item_date_2 = str_replace('/','-',$item_date_2);
							$item_date_2 = date("Y-m-d", strtotime($item_date_2));
							$sql = "INSERT INTO project_leave_early (
																  leave_id,
																  item_date,
																  item_type,
																  status
																)VALUES(
																	".GF::quote($leave_id).",
																	".GF::quote($item_date_2).",
																	".GF::quote($item_type_2).",
																	'O'
																)";
							$res = $db->Execute($sql);
						}
					}
					
					
				}else{
					foreach($item_date as $key=>$itemValue){
						if($itemValue!=''){
						$itemValue = str_replace('/','-',$itemValue);
						$itemValue = date("Y-m-d", strtotime($itemValue));
						$sql = "INSERT INTO project_leave_item (
															  leave_id,
															  item_date,
															  item_type,
															  status
															)VALUES(
																".GF::quote($leave_id).",
																".GF::quote($itemValue).",
																".GF::quote($item_type[$key]).",
																'O'
															)";
						$res = $db->Execute($sql);
						}
					}
				}
				
				
			}
			$base->set('_leave_id_',$leave_id);
			$this->_sendNotification('Edit');
			return true;
		}else{
			return false;
		}
		
		
	}
	
	private function _deleteLeave(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
		$sql = "UPDATE project_leave SET 
									  status='C',
									  update_dtm=NOW()
										WHERE leave_id=".GF::quote($base->get('_userid'));
		$res = $db->Execute($sql);
 		if($res){
 			$sql = "UPDATE project_leave_item SET 
										  status='C'
											WHERE leave_id=".GF::quote($base->get('_userid'));
			$res = $db->Execute($sql);
			return true;
		}else{
			return false;
		}
	}
	private function _cancelLeave(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
		$sql = "UPDATE project_leave SET 
									  approve='C',
									  update_dtm=NOW()
										WHERE leave_id=".GF::quote($base->get('_userid'));
		$res = $db->Execute($sql);
		return true;
 		
	}
	
	private function _deleteLeaveItem(){
		$base = Base::getInstance();
 		$db = DB::getInstance();
 		
 		$sql = "DELETE FROM project_leave_item WHERE item_id=".GF::quote($base->get('_userid'));
		$res = $db->Execute($sql);
 		
		
	}
	
	
	
 }
?>