<?php
 class Myewallet extends Permission{

 	public function beforeroute($base){
 		$this->module_ids = '9';
		$this->set_permission('mywallet',NULL);
		$this->set_view('mywallet',NULL);


		$this->PermissionAuth();

	}



	public function mywallet($base){
		$member = new Member();
		$order = new SOrder();

		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];
 		$base->set('_userid',$user_id);
		$base->set('user_id',$base->get('_userid'));
		$banklist = $order->bankList(); 

		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();
		$walletLog = $member->walletLog();

		//$walletLogWD = $member->walletLog('WD');
        $walletLogWD = $member->withdrawList();

		$walletLogIN = $member->walletLog('IN');

		$walletLogOU = $member->walletLog('OU');

		//$refundDetail = $member->refundDetail(); //Edit By Weerasak 06-01-2566
		$orderDetail = $order->get_order_detail(); //Edit By Weerasak 14-01-2566

		$base->set('memberinfomation',$memberInfomation);
		$base->set('walletLog',$walletLog);
		$base->set('walletLogWD',$walletLogWD);
		$base->set('walletLogIN',$walletLogIN);
		$base->set('walletLogOU',$walletLogOU);
		//$base->set('refundDetail',$refundDetail);
		$base->set('orderDetail',$orderDetail);
		$base->set('banklist',$banklist);
		//GF::print_r($refundDetail);
		$base->set('tab','view');

		Template::getInstance()->render('ewallet/mywallet.htm');
	}

	public function paymentlist($base){



 		$order = new SOrder();

 		$paymentList = $order->paymentList();


 		$member = new Member();
		$memberInfomation = $member->memberInfomation();

 		$user_id = $memberInfomation['id'];
 		$base->set('_userid',$user_id);
		$base->set('user_id',$base->get('_userid'));


		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();


		$base->set('memberinfomation',$memberInfomation);


		$base->set('tab','view');

 		$base->set('paymentList',$paymentList);
		Template::getInstance()->render('ewallet/paymentlist.htm');
	}
	public function withdraw($base){
		$member = new Member();
		$memberInfomation = $member->memberInfomation();
		$wdList = $member->withdrawList();

 		$user_id = $memberInfomation['id'];
 		$base->set('_userid',$user_id);
		$base->set('user_id',$base->get('_userid'));


		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();


		$base->set('memberinfomation',$memberInfomation);
		$base->set('wdList',$wdList);

		$base->set('tab','view');

		//Template::getInstance()->render('ewallet/withdraw.htm'); ปิดใช้งาน Weerasak 17-01-2566
	}
   public function confirmpayment($base){

		$order = new SOrder();

		$orderInfo = $order->orderInfo();
        $bank = $order->getBank();
        $banklist = $order->bankList();

        $base->set('bank',$bank);
        $base->set('banklist',$banklist);
		$base->set('order_type','1');

      if($orderInfo['id']==''){
         $base->set('order_type','3');
      }

      $member = new Member();
		$memberInfomation = $member->memberInfomation();
		$wdList = $member->withdrawList();

 		$user_id = $memberInfomation['id'];
 		$base->set('_userid',$user_id);
		$base->set('user_id',$base->get('_userid'));



		$memberInfomation = $member->memberInfomationByID();


		$base->set('memberinfomation',$memberInfomation);

 		$base->set('orderInfo',$orderInfo);
		//Template::getInstance()->render('ewallet/confirmpay.htm'); ปิดใช้งาน Weerasak 17-01-2566
	}
	public function processFrm($base){
		$member = new Member();
		$mode = $base->get('POST.mode');
		
		if($mode=='updatewd'){
			$result = $member->updateWithdraw();
			if($result){
				echo '<script>window.top.saveSccCallback();</script>';
			}else{
				echo '<script>window.top.saveSccCallback();</script>';
			}
		}

	}
 }
?>
