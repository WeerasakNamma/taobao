<?php
 class Ordervendor extends Permission{

 	public function beforeroute($base){
 		$this->module_ids = '14';
		$this->set_permission('orderlist','36');

		$this->set_view('view',NULL);
		$this->set_edit('create',NULL);
		$this->set_edit('edit',NULL);
		$this->set_delete('deleteexp',NULL);

		$this->PermissionAuth();

	}

	public function orderlist($base){

		$order = new SOrdermrg();
		$setting = new SSetting();

        $base->set('type','buy');
        $base->set('type_b','buy');
		$orderList = $order->orderListBuy();
		$tplList = $setting->templateList();
		
		$base->set('tplList',$tplList);
 		$base->set('orderList',$orderList);
		Template::getInstance()->render('ordervendor/orderlist.htm');
	}


	public function view($base){

		$order = new SOrdermrg();

		$itemList = $order->itemList();
		$orderInfo = $order->orderInfo();
        $shipItem = $order->getshipItem();
		$orderBySaller = $order->getSallerOrder();
		$rate = $order->getRate();

 		$base->set('rate',$rate);

 		$member = new Member();
		$memberonfomation = $member->memberInfomation();

 		//GF::print_r($memberonfomation);
 		/*if($memberonfomation['user_role_id']!=1){
 			echo '<script>window.top.location.hash = "/dashboard/";</script>';
 		}*/

		//GF::print_r($shipItem);
 		$base->set('itemList',$itemList);
 		$base->set('orderInfo',$orderInfo);
      $base->set('shipItem',$shipItem);
	  $base->set('orderBySaller',$orderBySaller);
		Template::getInstance()->render('ordervendor/view.htm');
	}
    public function viewdetail($base){

		$order = new SOrdermrg();

		$itemList = $order->itemList();
		$orderInfo = $order->orderInfo();
      $shipItem = $order->getshipItem();

		$rate = $order->getRate();

 		$base->set('rate',$rate);

 		$base->set('itemList',$itemList);
 		$base->set('orderInfo',$orderInfo);
      $base->set('shipItem',$shipItem);
		Template::getInstance()->render('ordervendor/viewdetail.htm');
	}
	public function view2($base){

		$order = new SOrdermrg();

		$itemList = $order->itemList();
		$orderInfo = $order->orderInfo();
      $shipItem = $order->getshipItem();

		$rate = $order->getRate();

 		$base->set('rate',$rate);

 		$base->set('itemList',$itemList);
 		$base->set('orderInfo',$orderInfo);
      $base->set('shipItem',$shipItem);
		Template::getInstance()->render('order/view2.htm');
	}


	public function resetsearch($base){
 		unset($_SESSION['odrbuy_search']);
	}

   public function delitem($base){
 		$order = new SOrdermrg();
 		$order->delItem();
	}
	public function updateqty($base){
 		$order = new SOrdermrg();
 		$order->itemQty2();
	}
	public function updatermb($base){
 		$order = new SOrdermrg();
 		$order->priceRmb2();
	}
   public function updatecommentitem($base){
 		$order = new SOrdermrg();
 		$order->itemComment();
	}
	    /*Weerasak 16-10-2564*/
		public function updateDiscountSaller($base){
			$order = new SOrdermrg();
			$order->updateDiscountSaller();
		}
	
		public function updateOrderAmountSaller($base)
		{
			$order = new SOrdermrg();
			$order->updateAmountSaller();
		}
	
		public function updateCNShippingSaller($base){
			$order = new SOrdermrg();
			$order->updateShippingSaller();
		}
		/*End Edit*/
	public function removeitem($base){
 		$order = new SOrdermrg();
 		$order->delItem();
	}
   public function pricecnship($base){
 		$order = new SOrdermrg();
 		$order->priceCnShip();
	}
   public function itemtype($base){
 		$order = new SOrdermrg();
 		$order->itemType();
	}
   public function orderstatus($base){
 		$order = new SOrdermrg();
 		$order->orderStatus3();
	}

   public function addcnship($base){
 		$order = new SOrdermrg();
 		$order->addshipItem();
	}
   public function updatecnship($base){
 		$order = new SOrdermrg();
 		$order->updateshipItem();
	}
	public function updatesaller($base){
		$order = new SOrdermrg();
		$order->sallerNumber();
   }


	public function processFrm($base){
		$order = new SOrdermrg();
		$tracking = new STracking();
		$mode = $base->get('POST.mode');
		//GF::print_r($base->get('POST'));
      if($mode=='createitem'){
			$result = $order->addOtherItem();
			if($result){
				echo '<script>window.top.rederOrder("'.$base->get('POST.order_id').'");</script>';
			}else{
				echo "F";
			}
		}
	   if($mode=='searchodr'){
			$_SESSION['odrbuy_search']['order_number'] = trim($base->get('POST.order_number'));
         $_SESSION['odrbuy_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['odrbuy_search']['status'] = trim($base->get('POST.status'));
			$_SESSION['odrbuy_search']['create_dtm'] = trim($base->get('POST.create_dtm'));
			echo '<script>window.top.reorderList();</script>';
		}


	}



 }
?>
