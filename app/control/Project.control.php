<?php
 class Project extends Permission{

 	public function beforeroute($base){
 		$this->module_ids = '2';
		$this->set_permission('projectlist','1');
		$this->set_permission('view','1');
		$this->set_permission('sprintlist','16');
		$this->set_permission('mysprintlist','17');
		$this->set_permission('mysprinttest','18');
		
		$this->set_view('view','1');
		$this->set_edit('create','1');
		$this->set_edit('edit','1');
		$this->set_delete('deletepro','1');
		
		$this->set_view('viewmysprintlist','17');
		$this->set_view('viewsprintlist','16');
		$this->set_edit('createsprintlist','16');
		$this->set_edit('editsprintlist','16');

		$this->PermissionAuth();

	}

 	public function projectlist($base){

 		$project = new SProject();
 		
 		

 		$case = $base->get('GET.case');

 		if($case==''){
			$case = 'OW';
		}
		$base->set('_case',$case);
 		$projectList = $project->projectList();
		$member = new Member();
		$memberList = $member->memberList();
		$base->set('memberList',$memberList);
		$base->set('projectList',$projectList);
Template::getInstance()->render('project/projectlist.htm');
	}
	public function create($base){
 		$member = new Member();

		$memberList = $member->memberList();

		$base->set('memberList',$memberList);

		Template::getInstance()->render('project/create.htm');
	}
	public function addon($base){
 		$member = new Member();

		$memberList = $member->memberList();

		$base->set('memberList',$memberList);

		$project = new SProject();

		$infomation = $project->projectInfomation();

		if(!is_array($infomation)){
			Template::getInstance()->render('error/404.htm');
		}else{
			$addonList = $project->addonList();
			$base->set('infomation',$infomation);
			$base->set('addonList',$addonList);

			$mode = $base->get('GET.mode');
			//print_r($base->get('GET'));
			if($mode=='edit'){
				$idinfomation = $project->addonInfomation();
				$base->set('idinfomation',$idinfomation);

			}

			Template::getInstance()->render('project/addon.htm');
		}


	}
	public function deleteaddon($base){
		$project = new SProject();
		$project->deleteAddon();
	}
	public function deletepro($base){
		$project = new SProject();
		$project->deleteProject();
	}
	public function deletecomment($base){
		$project = new SProject();
		$project->deleteComment();
	}
	public function edit($base){

		$member = new Member();

		$memberList = $member->memberList();

		$base->set('memberList',$memberList);

		$project = new SProject();

		$infomation = $project->projectInfomation();

		if(!is_array($infomation)){
			Template::getInstance()->render('error/404.htm');
		}else{
			$base->set('infomation',$infomation);
			Template::getInstance()->render('project/edit.htm');
		}


	}
	public function view($base){
		$member = new Member();

		$memberList = $member->memberList();

 		$memberInfomation = $member->memberInfomation();

		$base->set('memberList',$memberList);

		$project = new SProject();

		$infomation = $project->projectInfomation();
		
		$base->set('memberinfomation',$memberInfomation);

		if(!is_array($infomation)){
			Template::getInstance()->render('error/404.htm');
		}else{
			$base->set('infomation',$infomation);
			$addonList = $project->addonList();
			$base->set('addonList',$addonList);
			$paymentList = $project->paymentList();
			$base->set('payList',$paymentList);

			Template::getInstance()->render('project/view.htm');
		}


	}

	public function payment($base){
		$member = new Member();

		$memberList = $member->memberList();

		$base->set('memberList',$memberList);

		$project = new SProject();

		$infomation = $project->projectInfomation();

		$paymentList = $project->paymentList();

		if(!is_array($infomation)){
			Template::getInstance()->render('error/404.htm');
		}else{
			$project->checkRoundForupdate($paymentList);
			$base->set('payList',$paymentList);
			$base->set('infomation',$infomation);
			//GF::print_r($infomation);

			Template::getInstance()->render('project/payment.htm');
		}
	}
	///////////////////// Sprint List /////////////////////
	public function sprintlist($base){
		$sprint = new SSprint();
		$member = new Member();
		$memberList = $member->memberList();
		$projectList = $sprint->projectList();
		$sprintList = $sprint->sprintList();
		$base->set('memberList',$memberList);
		$base->set('projectlist',$projectList);
		$base->set('sprintlist',$sprintList);
		Template::getInstance()->render('project/sprintlist.htm');
		
	}
	
	public function createsprintlist($base){
		$sprint = new SSprint();
		$member = new Member();
		$setting = new SSetting();
		$sprinttemplate=$setting->getSprintTemplateList();
		$projectList = $sprint->projectList();
		$memberList = $member->memberList();
		$base->set('memberList',$memberList);
 		$base->set('projectlist',$projectList);
 		$base->set('sprinttemplate',$sprinttemplate);
		Template::getInstance()->render('project/createsprintlist.htm');
		
	}
	public function sprintlistbacklog($base){
		$sprint = new SSprint();
		$member = new Member();
		$SprintTemplateItemList = $sprint->getSprintTemplateItem();
		$memberList = $member->memberList();
		$base->set('memberList',$memberList);
		$base->set('SprintTemplateItemList',$SprintTemplateItemList);
		Template::getInstance()->render('project/sprintlist-backlog.htm');
	}
	public function checkDateSprintList($base){
		$star_date=$base->get('POST.star_date');
		$end_date=$base->get('POST.end_dat');
		if(strtotime($star_date)>strtotime($end_date)){
			$res = array("result" => "error");
			echo  json_encode($res);
		}else{
			$sprint = new SSprint();
			$result = $sprint->calculatProcessing();
			
			/*$res = array("result" => "success");*/
		}
		
		
	}
	public function editsprintlist($base){
		$sprint = new SSprint();
		$member = new Member();
		$setting = new SSetting();
		$sprinttemplate=$setting->getSprintTemplateList();
		$getEditSprintList = $sprint->getEditSprintList();
		$projectList = $sprint->projectList();
		//GF::print_r($getEditSprintList);
		$memberList = $member->memberList();
		//GF::print_r($memberList);
		$base->set('total',count($getEditSprintList));
		$base->set('view','0');
		$base->set('memberList',$memberList);
 		$base->set('getEditSprintList',$getEditSprintList);
 		$base->set('projectlist',$projectList);
 		$base->set('sprinttemplate',$sprinttemplate);
 		Template::getInstance()->render('project/createsprintlist.htm');
	}
	public function viewsprintlist($base){
		$sprint = new SSprint();
		$member = new Member();
		$setting = new SSetting();
		$sprinttemplate=$setting->getSprintTemplateList();
		$getEditSprintList = $sprint->getEditSprintList();
		$calculateBurnDownAll = $sprint->calculateBurnDownAll();
		$projectList = $sprint->projectList();
		$getburnDown = $sprint->burnDown();
		//GF::print_r($getburnDown);
		//GF::print_r($getEditSprintList);
		$calculateDayBurnDown = $sprint->calculateDayBurnDown();
		$memberList = $member->memberList();
		//GF::print_r($calculateDayBurnDown);
		$base->set('total',count($getEditSprintList));
		$base->set('view','1');
		$base->set('memberList',$memberList);
 		$base->set('getEditSprintList',$getEditSprintList);
 		$base->set('projectlist',$projectList);
 		$base->set('calculateDay',$calculateDayBurnDown);
 		$base->set('calculateBurnDownAll',$calculateBurnDownAll);
 		$base->set('getEstimateHour',$getburnDown);
 		$base->set('sprinttemplate',$sprinttemplate);
 		Template::getInstance()->render('project/createsprintlist.htm');
	}
	public function delsprintlist($base){
		$sprint = new SSprint();
		$result = $sprint->delSprintList();
	}	
	public function delbacklog($base){
		$sprint = new SSprint();
		$result = $sprint->delBacklog();
	}
	public function savebackloglist($base){
		$sprint = new SSprint();
		$result = $sprint->savebackloglist();
	}
	///////////////////////////////////////////////////////
	
	
	///////////////////// My Sprint List /////////////////////
	public function mysprintlist($base){
		$sprint = new SSprint();
		$member = new Member();
		$memberList = $member->memberList();
		$case = $base->get('GET.case');
 		if($case==''){
			$case = 'MS';
		}
		$MySprintList = $sprint->getMySprintList();
		//GF::print_r($MySprintList);
		//echo count($MySprintList);
		$base->set('_case',$case);
		$base->set('total',count($MySprintList));
		$base->set('MySprintList',$MySprintList);
		$base->set('memberList',$memberList);
		Template::getInstance()->render('project/mysprintlist.htm');
		
	}
	public function savemysprintlist($base){
		$sprint = new SSprint();
		$result = $sprint->updateMySprintList();
	}	
	public function viewmysprintlist($base){
		$sprint = new SSprint();
		$member = new Member();
		$setting = new SSetting();
		$sprinttemplate=$setting->getSprintTemplateList();
		$getEditSprintList = $sprint->getEditSprintList();
		$calculateDayBurnDown = $sprint->calculateDayBurnDown();
		$calculateBurnDown = $sprint->calculateBurnDown();
		$getburnDown = $sprint->burnDown();
		$projectList = $sprint->projectList();
		//GF::print_r($calculateBurnDown);
		$memberList = $member->memberList();
		//GF::print_r($memberList);
		$base->set('total',count($getEditSprintList));
		$base->set('view','1');
		$base->set('memberList',$memberList);
 		$base->set('getEditSprintList',$getEditSprintList);
 		$base->set('projectlist',$projectList);
 		$base->set('calculateDay',$calculateDayBurnDown);
 		$base->set('calculateBurnDown',$calculateBurnDown);
 		$base->set('getEstimateHour',$getburnDown);
 		$base->set('sprinttemplate',$sprinttemplate);
 		Template::getInstance()->render('project/viewmysprintlist.htm');
	}
	
	///////////////////////////////////////////////////////
	
	///////////////////// My Sprint Test //////////////////
	public function mysprinttest($base){
		$sprint = new SSprint();
		$member = new Member();
		$memberList = $member->memberList();
		$projectList = $sprint->projectList();
		$MySprintTest = $sprint->getMySprintTest();
		//GF::print_r($MySprintTest);
		$base->set('view','0');
		$base->set('memberList',$memberList);
		$base->set('projectlist',$projectList);
		$base->set('total',count($MySprintTest));
		$base->set('MySprintTest',$MySprintTest);
		
		Template::getInstance()->render('project/mysprinttest.htm');
	}
	public function viewmysprinttest($base){
		$sprint = new SSprint();
		$member = new Member();
		$memberList = $member->memberList();
		$viewMySprintTest = $sprint->viewMySprintTest();
		$MySprintTestList = $sprint->getMySprintTestList();
		$base->set('memberList',$memberList);
		$base->set('total',count($MySprintTestList));
		$base->set('MySprintTest',$MySprintTestList);
		$base->set('viewMySprintTest',$viewMySprintTest);

		Template::getInstance()->render('project/viewmysprinttest.htm');
	}
	public function savemysprinttest($base){
		$sprint = new SSprint();
		$result = $sprint->updateMySprintTest();
		/*if($result){echo "1";}else{
			echo "2";
		} */
	}
	///////////////////////////////////////////////////////
	
	public function deletepayment($base){
		$project = new SProject();
		//$project->deletePaymentRoundC();
		$project->deletePayment();
	}
	public function updatepayment($base){
		$project = new SProject();
		//$project->deletePaymentRoundC();
		$project->updatePaymentValue();
	}
	public function deleteround($base){
		$project = new SProject();
		//$project->deletePaymentRoundC();
		$project->deleteRound();
	}

	public function deletefile($base){
		$project = new SProject();
		$project->deleteFile();
	}
	public function updatefile($base){
		$dataname = $base->get('_ids');
		$project = new SProject();
		$project->updateFileName();
	}
	
	
	
	
	public function processFrm($base){

		//print_r($_FILES['project_file']);

		$project = new SProject();
		$mode = $base->get('POST.mode');
		//GF::print_r($base->get('POST'));exit();
		if($mode=='create'){
			$result = $project->createProject();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='edit'){
			$result = $project->updateProject();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='createaddon'){
			$result = $project->createAddon();
			if($result){
				echo '<script>window.top.saveAddonCallback();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='editaddon'){
			$result = $project->updateAddon();
			if($result){
				echo '<script>window.top.saveAddonEditCallback("'.$base->get('POST.project_id').'");</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='createpayment'){
			$result = $project->createPayment();
			if($result){
				echo '<script>window.top.savePaymentCallback();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='createround'){
			//GF::print_r($base->get('POST'));
			$result = $project->createPaymentRound();
			if($result){
				echo '<script>window.top.savePaymentRoundCallback();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='updateround'){
			//GF::print_r($base->get('POST'));
			$result = $project->updatePaymentRound();
			if($result){
				echo '<script>window.top.savePaymentRoundCallback();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='createcomment'){
			//GF::print_r($base->get('POST'));
			$result = $project->createComment();
			if($result){
				echo '<script>window.top.saveCommentCallback();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='uploadfile'){
			//GF::print_r($base->get('POST'));
			$result = $project->uploadFile();
			if($result){
				echo '<script>window.top.saveFileCallback();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='searchprojectlist'){

				$start1 = $base->get('POST.start_date1');
				$start2 = $base->get('POST.start_date2');
	 			$end1 = $base->get('POST.end_date1');
	 			$end2 = $base->get('POST.end_date2');

				$_SESSION['project_project']['start_date1'] = $start1;
				$_SESSION['project_project']['start_date2'] = $start2;
				$_SESSION['project_project']['end_date1'] = $end1;
				$_SESSION['project_project']['end_date2'] = $end2;

				$project_name = $base->get('POST.project_name');
	 			$project_status = $base->get('POST.project_status');
	 			$project_type = $base->get('POST.project_type');

	 			$project_name = ltrim($project_name);
	 			$project_name = rtrim($project_name);
	 			$project_name = str_replace("'",'',$project_name);
	 			$_SESSION['project_project']['project_name'] = $project_name;
	 			$_SESSION['project_project']['project_status'] = $project_status;
	 			$_SESSION['project_project']['project_type'] = $project_type;

	 			$user_type = $base->get('POST.user_type');
	 			$user_id = $base->get('POST.user_id');

	 			$_SESSION['project_project']['user_type'] = $user_type;
	 			$_SESSION['project_project']['user_id'] = $user_id;
	 			if($user_type==''){
					$_SESSION['project_project']['user_id'] = '';
				}
				$_SESSION['project_project']['project_case'] = $base->get('POST.mode_case');

			echo '<script>window.top.successProjectlistCallBack();</script>';
		}else if($mode=='clearprojectlist'){
			unset($_SESSION['project_project']);
			//session_destroy($_SESSION['project_report']);
			echo '<script>window.top.successProjectlistCallBack();</script>';
		}else if($mode=='createsprintlist'){
			$sprint = new SSprint();
			$result = $sprint->saveSprintTemplateList();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='searchsprintlist'){

				$start1 = $base->get('POST.start_date1');
				$start2 = $base->get('POST.start_date2');
	 			$end1 = $base->get('POST.end_date1');
	 			$end2 = $base->get('POST.end_date2');

				$_SESSION['project_sprint']['start_date1'] = $start1;
				$_SESSION['project_sprint']['start_date2'] = $start2;
				$_SESSION['project_sprint']['end_date1'] = $end1;
				$_SESSION['project_sprint']['end_date2'] = $end2;
				
				$sprint_project_id = $base->get('POST.sprint_project_id');
				$_SESSION['project_sprint']['sprint_project_id'] = $sprint_project_id;
				
	 			$sprint_status = $base->get('POST.sprint_status');
	 			$_SESSION['project_sprint']['sprint_status'] = $sprint_status;
	 			
	 			$sprint_owner = $base->get('POST.sprint_owner');
	 			$_SESSION['project_sprint']['sprint_owner'] = $sprint_owner;
	 			
	 			$sprint_list_team = $base->get('POST.sprint_list_team');
	 			$_SESSION['project_sprint']['sprint_list_team'] = $sprint_list_team;
	 			
	 			$sprint_name_list = $base->get('POST.sprint_name_list');
	 			$_SESSION['project_sprint']['sprint_name_list'] = $sprint_name_list;
	 			//$project_type = $base->get('POST.project_type');
				/*
	 			$project_name = ltrim($project_name);
	 			$project_name = rtrim($project_name);
	 			$project_name = str_replace("'",'',$project_name);
	 			$_SESSION['project_sprint']['project_name'] = $project_name;
	 			$_SESSION['project_sprint']['project_status'] = $project_status;
	 			$_SESSION['project_sprint']['project_type'] = $project_type;

	 			$user_type = $base->get('POST.user_type');
	 			$user_id = $base->get('POST.user_id');

	 			$_SESSION['project_sprint']['user_type'] = $user_type;
	 			$_SESSION['project_sprint']['user_id'] = $user_id;
	 			if($user_type==''){
					$_SESSION['project_sprint']['user_id'] = '';
				}
				$_SESSION['project_sprint']['project_case'] = $base->get('POST.mode_case');*/

			echo '<script>window.top.successSprintlistCallBack();</script>';
		}else if($mode=='clearsprintlist'){
			unset($_SESSION['project_sprint']);
			//session_destroy($_SESSION['project_report']);
			echo '<script>window.top.successSprintlistCallBack();</script>';
		}else if($mode=="editsprintlist"){
			$sprint = new SSprint();
			$result = $sprint->saveSprintTemplateList();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}else if($mode=="searchMysprintTest"){
			$getArray = array();
			$getArray['sprint_id'] = $base->get('POST.sprint_id');
			$getArray['task_name'] = $base->get('POST.task_name');
			$getArray['assign'] = $base->get('POST.assign');
			$getArray['status'] = $base->get('POST.status');
			$base->set('_result_search',$getArray);
			$sprint = new SSprint();
			$MySprintTestList = $sprint->getMySprintTestList();
		}else if($mode=='searchmysprintlisttest'){
			$_SESSION['project_mysprinttest']['start_date'] 		= 	$base->get('POST.start_date');
			$_SESSION['project_mysprinttest']['end_date'] 			= 	$base->get('POST.end_date');
			$_SESSION['project_mysprinttest']['sprint_project_id']	= 	$base->get('POST.sprint_project_id');
			$_SESSION['project_mysprinttest']['sprint_status'] 		= 	$base->get('POST.sprint_status');
			$_SESSION['project_mysprinttest']['sprint_owner'] 		= 	$base->get('POST.sprint_owner');
			$_SESSION['project_mysprinttest']['sprint_name_list'] 	= 	$base->get('POST.sprint_name_list');
			
			$sprint = new SSprint();
			$result = $sprint->getMySprintTest();
			echo '<script>window.top.successMySprintTestCallBackTwo();</script>';
		}else if($mode=='clearmysprintlisttest'){
			unset($_SESSION['project_mysprinttest']);
			
			$sprint = new SSprint();
			$result = $sprint->getMySprintTest();
			echo '<script>window.top.successMySprintTestCallBackTwo();</script>';
		}
	}

	/**
	 * Get Project Team By  Owner leader
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function  getProjectTeam($data){
		$member = new Member();

		//GF::print_r($data->get('POST.owner-id'));
		//$input_data  = $data->table['POST']['owner-id'];

		$input_data  =  $data->get('POST.owner-id');
		$member_list = $member->getProjectTeambyLeader($input_data);

		echo json_encode($member_list);
	}


 }
?>