<?php
	class Report extends Permission{

		public function beforeroute($base){
	 		$this->module_ids = '8';
			$this->set_permission('home',NULL);


			//$this->set_view('view','4');

			$this->PermissionAuth();
		}

		public function home($base){



			Template::getInstance()->render('report/home.htm');
		}

        public function paidrp($base){
            $report = new SReport(); 
            $paidReport = $report->paidReport();
            
            $base->set('paidReport',$paidReport);
            $base->set('pagecontent','paidrp.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function waitshipping($base){
            $report = new SReport(); 
            $waitShipping = $report->waitShipping();
            
            $base->set('waitShipping',$waitShipping);
            //GF::print_r($waitShipping);
            $base->set('pagecontent','waitshipping.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function buyrp($base){
            $report = new SReport(); 
            $orderList = $report->orderList();
            
            $base->set('orderList',$orderList);
            //GF::print_r($waitShipping);
            $base->set('pagecontent','buyrp.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function shippingrp($base){
            $report = new SReport(); 
            $shippingList = $report->shippingList();
            
            $base->set('shippingList',$shippingList);
            $base->set('pagecontent','shippingrp.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function refundrp($base){
            $report = new SReport(); 
            $refundList = $report->refundList();
            
            $base->set('refundList',$refundList);
            $base->set('pagecontent','refundrp.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function withdrawrp($base){
            $report = new SReport(); 
            $withdrawList = $report->withdrawList();//GF::print_r($withdrawList);
            
            $base->set('withdrawList',$withdrawList);
            $base->set('pagecontent','withdrawrp.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function walletrp($base){
            $report = new SReport(); 
            $walletList = $report->walletList();
            
            $base->set('walletList',$walletList);
            $base->set('pagecontent','walletrp.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function raterp($base){
            $report = new SReport(); 
            $rateList = $report->rateList();
            
            $base->set('rateList',$rateList);
            $base->set('pagecontent','raterp.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function sendcn($base){
            
            $report = new SReport();
            $base->set('type','buy');
            $orderList = $report->orderListBuy();

            //GF::print_r($orderList);
            $base->set('orderList',$orderList);
            $base->set('pagecontent','sendcn.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function listkiloq($base){
            $report = new SReport(); 
            $listKiloQ = $report->reportListKiloQ();
            
            $base->set('listKiloQ',$listKiloQ);
            //GF::print_r($waitShipping);
            $base->set('pagecontent','listkiloq.htm');
            Template::getInstance()->render('report/home.htm');
        }
        public function listkiloqexportexcel($base){
            $report = new SReport();
            $listKiloQ = $report->exportListKiloQ();
            $base->set('listKiloQ',$listKiloQ);    
            Template::getInstance()->render('report/listkiloqexport.htm');
        }

        public function resetsearchsendcn($base){
            unset($_SESSION['report_sendcn_search']);
        }
        public function resetsearchpaidrp($base){
            unset($_SESSION['report_payorder_search']);
        }
        public function resetsearchbuyrp($base){
            unset($_SESSION['report_buyrp_search']);
        }
        public function resetsearchshippingrp($base){
            unset($_SESSION['report_shipping_search']);
        }
        public function resetsearchrefundrp($base){
            unset($_SESSION['report_refundrp_search']);
        }
        public function resetsearchwithdrawrp($base){
            unset($_SESSION['report_withdrawrp_search']);
        }
        public function resetsearchwalletrp($base){
            unset($_SESSION['report_walletrp_search']);
        }
        public function resetsearchraterp($base){
            unset($_SESSION['report_raterp_search']);
        }

        public function resetwaitship($base){
            unset($_SESSION['report_waitship_search']);
        }
        public function resetKiloq($base){
            unset($_SESSION['report_kiloq_search']);
        }

		public function processFrm($base){
			$report = new SReport();
			$mode = $base->get('POST.mode');

			if($mode=='searchodrchina'){
                $_SESSION['report_sendcn_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_sendcn_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searchpaid'){
                $_SESSION['report_payorder_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_payorder_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searchwaitship'){
                $_SESSION['report_waitship_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_waitship_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searchbuyrp'){
                $_SESSION['report_buyrp_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_buyrp_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searchshipping'){
                $_SESSION['report_shipping_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_shipping_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searchrefundrp'){
                $_SESSION['report_refundrp_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_refundrp_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searchwithdraw'){
                $_SESSION['report_withdrawrp_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_withdrawrp_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searchwallet'){
                $_SESSION['report_walletrp_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_walletrp_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searchrate'){
                $_SESSION['report_raterp_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_raterp_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.reorderList();</script>';
            }
            else if($mode=='searckiloq'){
                $_SESSION['report_kiloq_search']['start_date'] = trim($base->get('POST.start_date'));
                $_SESSION['report_kiloq_search']['end_date'] = trim($base->get('POST.end_date'));
                echo '<script>window.top.listkiloq();</script>';
            }
		}
	}

?>
