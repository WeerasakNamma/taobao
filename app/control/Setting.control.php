<?php
	class Setting extends Permission{

		public function beforeroute($base){
	 		$this->module_ids = '9';
			$this->set_permission('permission','6');
			$this->set_permission('position','10');
			$this->set_permission('masterdata','13');
			$this->set_permission('masterlist','13');
			$this->set_permission('sprinttemplate','14');
			$this->set_permission('holiday','15');
			$this->set_permission('rateexchange','19');
			$this->set_permission('ecoupon','20');
			$this->set_permission('ordertype','21');
            $this->set_permission('ftpmrg','23');
            $this->set_permission('emailtemplate','43');
            $this->set_permission('banklist','44');
            $this->set_permission('manuallist','46');
			//$this->set_permission('userlist',11);

			//$this->set_view('view','2');
			$this->set_edit('creategroup','6');
			$this->set_edit('editgroup','6');
			$this->set_edit('edituser','6');
			$this->set_delete('deletegroup','6');

			$this->set_edit('createposition','10');
			$this->set_edit('editposition','10');
			$this->set_edit('editposition','10');
			$this->set_delete('deletegposition','10');

			$this->set_edit('createmastergroup','13');
			$this->set_edit('editmastergroup','13');
			$this->set_delete('deletemastergroup','13');

			$this->set_edit('createmasterdata','13');
			$this->set_edit('editmasterdata','13');
			$this->set_delete('deletemasterdata','13');

			$this->set_edit('editsprinttemplate','14');
			$this->set_delete('delsprinttemplate','14');


			$this->set_edit('editholiday','15');
			$this->set_delete('delholiday','15');

			$this->set_edit('editrateexchange','19');

			$this->set_edit('createecoupon','20');
			$this->set_edit('editecoupon','20');
			$this->set_delete('delecoupon','20');

			$this->set_edit('createtype','21');
			$this->set_edit('editetype','21');
            $this->set_delete('deletype','21');
            
            $this->set_edit('banklist','44');
            $this->set_delete('deletebank','44');
            $this->set_edit('manuallist','46');

			$this->PermissionAuth();

		}

		/////////////// Permission Controller //////////////////
		public function permission($base){
			$setting = new SSetting();
			$groupList = $setting->groupList();
            
			$base->set('groupList',$groupList);
			Template::getInstance()->render('setting/permissionlist.htm');
		}

		public function creategroup($base){
			$setting = new SSetting();
            $moduleList = $setting->moduleList();
            $agencyList = $setting->agencyList();
			//GF::print_r($agencyList);

            $base->set('moduleList',$moduleList);
            $base->set('agencyList',$agencyList);

			Template::getInstance()->render('setting/creategroup.htm');
		}
		public function editgroup($base){
			$setting = new SSetting();
			$moduleList = $setting->moduleList();
			$base->set('_ids_','');
			$groupInfomation = $setting->groupInfomation();
			$rateList = $setting->rateUserList();
            //GF::print_r($rateList);
            //GF::print_r($groupInfomation);
            $agencyList = $setting->agencyList();
			//GF::print_r($agencyList);

			$base->set('rateList',$rateList);
			$base->set('moduleList',$moduleList);
			$base->set('groupInfo',$groupInfomation);
            $base->set('agencyList',$agencyList);

			Template::getInstance()->render('setting/creategroup.htm');
		}
		public function deletegroup($base){
			$setting = new SSetting();
			$setting->deleteGroup();
		}
		public function edituser($base){
			$setting = new SSetting();
			$member = new Member();
			$base->set('_ids_','');
			$groupInfomation = $setting->groupInfomation();

			$base->set('groupInfo',$groupInfomation);

			$base->set('_role_id',$base->get('_ids'));

			$memberListGroup = $member->memberListByGroup();//GF::print_r($memberListGroup);
			$memberList = $member->memberList();

			$base->set('memberList',$memberList);
			$base->set('memberListGroup',$memberListGroup);

			Template::getInstance()->render('setting/groupuserlist.htm');
		}
		////////////////////////  Group Rate ///////////////////////
		public function delrate($base){
			$setting = new SSetting();
			$setting->deleteRate();
		}
		////////////////////////  general ///////////////////////
		public function general($base){
            $setting = new SSetting();
            $base->set('genid','1');
            $ship_content_1 = $setting->getGeneralSetting();
            $base->set('genid','2');
            $ship_content_2 = $setting->getGeneralSetting();
            $base->set('genid','3');
            $ship_content_3 = $setting->getGeneralSetting();
            $base->set('genid','4');
            $ship_content_4 = $setting->getGeneralSetting();
            $base->set('genid','5');
            $ship_content_5 = $setting->getGeneralSetting();
            $base->set('genid','6');
            $ship_content_6 = $setting->getGeneralSetting();

            $base->set('ship_content_1',$ship_content_1);
            $base->set('ship_content_2',$ship_content_2);
            $base->set('ship_content_3',$ship_content_3);
            $base->set('ship_content_4',$ship_content_4);
            $base->set('ship_content_5',$ship_content_5);
            $base->set('ship_content_6',$ship_content_6);
            Template::getInstance()->render('setting/general.htm');
        }
        
        /////////////// E-Mail Template Controller //////////////////
        public function emailtemplate_temp($base){
            
            $setting = new SSetting();
            $base->set('genid','7');
            $gen_content_7 = $setting->getGeneralSetting();
            $base->set('genid','8');
            $gen_content_8 = $setting->getGeneralSetting();
            $base->set('genid','9');
            $gen_content_9 = $setting->getGeneralSetting();
            $base->set('genid','10');
            $gen_content_10 = $setting->getGeneralSetting();
            $base->set('genid','11');
            $gen_content_11 = $setting->getGeneralSetting();
            $base->set('genid','12');
            $gen_content_12 = $setting->getGeneralSetting();
            $base->set('genid','13');
            $gen_content_13 = $setting->getGeneralSetting();
            $base->set('genid','14');
            $gen_content_14 = $setting->getGeneralSetting();
            $base->set('genid','15');
            $gen_content_15 = $setting->getGeneralSetting();

            $base->set('gen_content_7',$gen_content_7);
            $base->set('gen_content_8',$gen_content_8);
            $base->set('gen_content_9',$gen_content_9);
            $base->set('gen_content_10',$gen_content_10);
            $base->set('gen_content_11',$gen_content_11);
            $base->set('gen_content_12',$gen_content_12);
            $base->set('gen_content_13',$gen_content_13);
            $base->set('gen_content_14',$gen_content_14);
            $base->set('gen_content_15',$gen_content_15);
            Template::getInstance()->render('setting/emailtemplate.htm');
        }
        public function emailtemplate($base){
            
            $setting = new SSetting();
            $template = $setting->getEmailTemplate();
            
            $base->set('template',$template);
            Template::getInstance()->render('setting/email_template.htm');
        }

		/////////////// Masterdata Controller //////////////////

		public function masterdata($base){

			$setting = new SSetting();
			$groupList = $setting->masterGroupList();

			$base->set('groupList',$groupList);
			Template::getInstance()->render('setting/masterdatagrouplist.htm');
		}
		public function createmastergroup($base){


			Template::getInstance()->render('setting/createmastergroup.htm');
		}
		public function editmastergroup($base){
			$setting = new SSetting();

			$groupInfomation = $setting->masterGroupInfomation();

			$base->set('groupInfomation',$groupInfomation);

			Template::getInstance()->render('setting/createmastergroup.htm');
		}
		public function deletemastergroup($base){
			$setting = new SSetting();
			$setting->deleteMasterGroup();
		}
		public function masterlist($base){

			$setting = new SSetting();
			$masterdataList = $setting->masterdataList();

			$groupInfomation = $setting->masterGroupInfomation();

			$base->set('groupInfomation',$groupInfomation);

			$base->set('masterList',$masterdataList);
			Template::getInstance()->render('setting/masterdatalist.htm');
		}
		public function createmasterdata($base){
			$setting = new SSetting();

			$groupInfomation = $setting->masterGroupInfomation();

			$base->set('groupInfomation',$groupInfomation);

			Template::getInstance()->render('setting/createmasterdata.htm');
		}
		public function editmasterdata($base){
			$setting = new SSetting();

			$masterInfomation = $setting->masterdataInfomation();
			$groupInfomation = $setting->masterGroupInfomation();

			$base->set('groupInfomation',$groupInfomation);
			$base->set('masterInfomation',$masterInfomation);

			Template::getInstance()->render('setting/createmasterdata.htm');
		}

		public function deletemasterdata($base){
			$setting = new SSetting();
			$setting->deleteMasterdata();
        }


        //////////////////////// BANK ///////////////////
        public function banklist($base){
			$setting = new SSetting();
			$bankList = $setting->bankList();

			$base->set('bankList',$bankList);
			Template::getInstance()->render('setting/banklist.htm');
        }
        public function createbank($base){
			Template::getInstance()->render('setting/createbank.htm');
        }
        public function editbank($base){
            $setting = new SSetting();
            
            $bankInfo = $setting->bankInfo();
            
            $base->set('bankInfo',$bankInfo);
			Template::getInstance()->render('setting/createbank.htm');
        }
        public function deletebank($base){
            $setting = new SSetting();
            $bankInfo = $setting->deleteBank();
		}



		//////////////////////// Rate Exchange ///////////////////
		public function rateexchange($base){
			$setting = new SSetting();
			$rateExchange = $setting->getRateExchange();
            $rateExchange2 = $setting->getRateExchange2();
            $rateExchange3 = $setting->getRateExchange3();
			//GF::print_r($rateExchange);
			$base->set('total',count($rateExchange));
			$base->set('rateExchange',$rateExchange);
            $base->set('rateExchange2',$rateExchange2);
            $base->set('rateExchange3',$rateExchange3);
			Template::getInstance()->render('setting/rateexchange.htm');
		}

		////////////////////////  E-Coupon ///////////////////////
		public function ecoupon($base){
			$setting = new SSetting();
			$ecouponlist = $setting->getEcouponList();
			//GF::print_r($ecouponlist);
			$base->set("ecouponlist",$ecouponlist);
			Template::getInstance()->render('setting/ecouponlist.htm');
		}
		public function createecoupon($base){
			$setting = new SSetting();
			$base->set("total","0");
			Template::getInstance()->render('setting/createecoupon.htm');
		}

		public function editecoupon($base){
			$setting = new SSetting();
			$ecouponlist = $setting->getEcoupon();
			//GF::print_r($ecouponlist);
			$base->set("ecouponlist",$ecouponlist);
			$base->set("total","1");
			Template::getInstance()->render('setting/createecoupon.htm');
		}
		////////////////////////  Sync Data ///////////////////////
		public function syncdata($base){
			$setting = new SSetting();
			$syncList = $setting->syncList();
			//GF::print_r($syncList);
			$base->set("syncList",$syncList);
			Template::getInstance()->render('setting/synclist.htm');
		}
		////////////////////////  FTP Management ///////////////////////
		public function ftpmrg($base){
			$setting = new SSetting();

			$base->set('templateList',$setting->templateList());
			Template::getInstance()->render('setting/ftplist.htm');
		}
		public function createtemplate($base){
			$setting = new SSetting();

			Template::getInstance()->render('setting/createtemplate.htm');
		}
		public function edittemplate($base){
			$setting = new SSetting();

			$templateInfo = $setting->templateInfo();

			$base->set('templateInfo',$templateInfo);
			Template::getInstance()->render('setting/createtemplate.htm');
		}
		public function deletetemplate($base){
			$setting = new SSetting();
			$setting->deleteTemplate();
        }
        /////////////// Manual Controller //////////////////

		public function manuallist($base){

			$setting = new SSetting();
			$contentList = $setting->manualList();

			$base->set('contentList',$contentList);
			Template::getInstance()->render('setting/manuallist.htm');
        }
        public function createmanual($base){
            
            Template::getInstance()->render('setting/createmanual.htm');
        }
        public function editmanual($base){
            $setting = new SSetting();
    
            
            $contentInfo = $setting->contentInfomation();
            
             $base->set('contentInfo',$contentInfo);
    
            Template::getInstance()->render('setting/createmanual.htm');
        }
        public function updatemanual($base){
            $setting = new SSetting();
            $setting->updateStatusContent();
        }
		/////////////// Process Form Controller //////////////////
		public function processFrm($base){
			$setting = new SSetting();
			$mode = $base->get('POST.mode');
			//GF::print_r($base->get('POST'));exit();
			if($mode=='creategroup'){
				$result = $setting->createGroup();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='updaterate'){
				//GF::print_r($base->get('POST'));
				$result = $setting->updateUserRate();
			}
			else if($mode=='editgroup'){
				$result = $setting->updateGroup();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='edituser'){
				$result = $setting->updateUserPermission();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='createposition'){
				$result = $setting->createPosition();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='editposition'){
				$result = $setting->updatePosition();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='createmastergroup'){
				$result = $setting->createMasterdataGroup();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='editmastergroup'){
				$result = $setting->updateMasterGroup();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='createmasterdata'){
				$result = $setting->createMasterData();
				if($result){
					echo '<script>window.top.successMasterCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='editmasterdata'){
				$result = $setting->updateMasterData();
				if($result){
					echo '<script>window.top.successMasterCallBack();</script>';
				}else{
					echo "F";
				}
			}else if($mode=='createsprint'){
				$result = $setting->saveSprintTemplate();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}else if($mode=='editsprint'){
				$result = $setting->saveSprintTemplate();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}else if($mode=='createholiday'){
				$result = $setting->saveHolidaySetting();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}else if($mode=='editholiday'){
				$result = $setting->saveHolidaySetting();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}
			}else if($mode=='searchholiday'){

				$start = $base->get('POST.start_date');
		 		$end = $base->get('POST.end_date');
		 		$holiday_name = $base->get('POST.holiday_name');
		 		$_SESSION['holiday']['start_date'] = $start;
				$_SESSION['holiday']['end_date'] = $end;
				$_SESSION['holiday']['holiday_name'] = $holiday_name;
				echo '<script>window.top.successCallBack();</script>';

			}else if($mode=='clearholiday'){
				unset($_SESSION['holiday']);
				session_destroy($_SESSION['holiday']);
				echo '<script>window.top.location.hash = "/setting/holiday/?'.time().'";</script>';

			}else if($mode=='createrateexchange'){
				$result = $setting->saveRateExchange();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

			}else if($mode=='createrateexchange2'){
				$result = $setting->saveRateExchange2();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

			}else if($mode=='createrateexchange3'){
				$result = $setting->saveRateExchange3();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

			}else if($mode=='updatetemplate'){
				$result = $setting->createFtpTemplate();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

			}else if($mode=='edittemplate'){
				$result = $setting->updateFtmTemplate();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

			}else if($mode=='createecoupon'){
				$result = $setting->saveEcoupon();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

			}else if($mode=='editecoupon'){
				$result = $setting->saveEcoupon();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

			}else if($mode=='delecoupon'){
				$result = $setting->delEcoupon();
				if($result){
					echo "T";
				}else{
					echo "F";
				}

			}else if($mode=='checkecoupon'){
				$result = $setting->checkEcoupon();
				if($result){
					echo "T";
				}else{
					echo "F";
				}

			}
			else if($mode=='addrate'){
				$result = $setting->addRate();
				if($result){
					// echo '<script>window.top.location.reload();</script>';
					echo '<script>window.top.saveRateSccCallback();</script>';
				}else{
					echo '<script>window.top.preload("hidden"); window.top.noticeMessage("รายการซ้ำ กรุณาเลือกรายการใหม่");</script>';
				}
			}
         else if($mode=='updatecontent'){
            $base->set('field_name','gen_field_1');
            $base->set('field_value',$base->get('POST.cont_1'));
            $base->set('gen_id','1');
            $setting->updateGeneralContent();

            $base->set('field_name','gen_field_1');
            $base->set('field_value',$base->get('POST.cont_2'));
            $base->set('gen_id','2');
            $setting->updateGeneralContent();

            $base->set('field_name','gen_field_1');
            $base->set('field_value',$base->get('POST.cont_3'));
            $base->set('gen_id','3');
            $setting->updateGeneralContent();

            $base->set('field_name','gen_field_1');
            $base->set('field_value',$base->get('POST.cont_4'));
            $base->set('gen_id','4');
            $setting->updateGeneralContent();

            $base->set('field_name','gen_field_1');
            $base->set('field_value',$base->get('POST.cont_5'));
            $base->set('gen_id','5');
            $setting->updateGeneralContent();

            $base->set('field_name','gen_field_1');
            $base->set('field_value',$base->get('POST.cont_6'));
            $base->set('gen_id','6');
            $setting->updateGeneralContent();

            $base->set('field_name','gen_field_2');
            $base->set('field_value',$base->get('POST.cont_6_2'));
            $base->set('gen_id','6');
            $setting->updateGeneralContent();


            echo '<script>window.top.location.hash = "/setting/general/?'.time().'";</script>';

            }
            else if($mode=='updateemail'){

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_7'));
                // $base->set('gen_id','7');
                // $setting->updateGeneral();

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_8'));
                // $base->set('gen_id','8');
                // $setting->updateGeneral();

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_9'));
                // $base->set('gen_id','9');
                // $setting->updateGeneral();

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_10'));
                // $base->set('gen_id','10');
                // $setting->updateGeneral();

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_11'));
                // $base->set('gen_id','11');
                // $setting->updateGeneral();

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_12'));
                // $base->set('gen_id','12');
                // $setting->updateGeneral();

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_13'));
                // $base->set('gen_id','13');
                // $setting->updateGeneral();

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_14'));
                // $base->set('gen_id','14');
                // $setting->updateGeneral();

                // $base->set('field_name','gen_field_1');
                // $base->set('field_value',$base->get('POST.cont_15'));
                // $base->set('gen_id','15');
                // $setting->updateGeneral();
                $setting->updateEmailTemplate();
                echo '<script>window.top.location.hash = "/setting/emailtemplate/?'.time().'";</script>';
            }
            else if($mode=='createbank'){
				$result = $setting->insertBank();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

            }
            else if($mode=='editbank'){
				$result = $setting->updateBank();
				if($result){
					echo '<script>window.top.successCallBack();</script>';
				}else{
					echo "F";
				}

            }
            else if($mode=='createcontent'){
                $result = $setting->createContent();
                if($result){
                    echo '<script>window.top.successContetnCallBack();</script>';
                }else{
                    echo "F";
                }
            }
            else if($mode=='editcontent'){
    
                $result = $setting->updateContent();
                if($result){
                    echo '<script>window.top.successContetnCallBack();</script>';
                }else{
                    echo "F";
                }
            }
		}

	}
?>
