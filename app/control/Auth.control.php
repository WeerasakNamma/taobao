<?php
 class Auth {
 	
 	public function noroute(){
		
	}

    public function resetpassword($base){
        $setting = new SSetting();
        $base->set('genid','6');
        $ship_content = $setting->getGeneralSettingByuser();
 		
        $base->set('ship_content',$ship_content);
        Template::getInstance()->render('login/resetpassword.htm');
    }
 	
 	public function authen($base){

        $auth = new Authen();
        $validate = $auth->validateLogin();
        if($validate){
            header('Location:'.$base->get('BASEURL'));
            exit();
        }

        $setting = new SSetting();
        $base->set('genid','6');
        $ship_content = $setting->getGeneralSettingByuser();
 		
        $base->set('ship_content',$ship_content);
		Template::getInstance()->render('login/login.htm');
    }
    public function authenagency($base){
        
                $setting = new SSetting();
                $base->set('genid','6');
                $ship_content = $setting->getGeneralSettingByuser();
                 
                $base->set('ship_content',$ship_content);
                Template::getInstance()->render('login/loginagency.htm');
            }
	
	public function login($base){
		$username = $base->get('POST.vc_username');
		$password = $base->get('POST.vc_password');
		
		$authen = new Authen();
		echo $authen->accessLogin();
		//GF::print_r($authen->accessLogin());
	}
	public function logout($base){
		$authen = new Authen();
		$authen->accessLogout();
		echo '<script>window.location = "'.$base->get('BASEURL').'";</script>';
	}
	
	public function clearlogin(){
		$authen = new Authen();
		$authen->clearLogin();
	}
    public function setpwd(){
		$register = new SRegister();
		$result = $register->resetPassword();
        if($result){
            echo 'A';
        }else{
            echo 'F';
        }
	}
 	
 }
?>