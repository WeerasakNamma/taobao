<?php
   //ini_set('display_errors', 1);
   //ini_set('display_startup_errors', 1);
   //error_reporting(E_ALL);

 class Buyship extends Permission{

     public function beforeroute($base){
 		$this->module_ids = '2';
		$this->set_permission('order','1');

		$this->set_view('view',NULL);
		$this->set_edit('create',NULL);
		$this->set_edit('edit',NULL);
		$this->set_delete('deleteexp',NULL);

		$this->PermissionAuth();
    $_SESSION["THRate"] = GF::getrate();
		$odr = new SOrder();
		$odr->countSessions();

	}

 	public function order($base){
      	unset($_SESSION['odr_search']);
 		$orser = new SOrder();



		$base->set('cnrate',$orser->getRate());
		Template::getInstance()->render('buyship/order.htm');
	}
  public function tempToCart($base){
    $order = new SOrder();
    $order->addTmpToCart();
    Template::getInstance()->render('buyship/cart.htm');
  }

  public function cart($base){
    Template::getInstance()->render('buyship/cart.htm');
  }

	public function checkout($base){
      unset($_SESSION['odr_search']);
 		$order = new SOrder();
 		$rate = $order->getRate();

 		$base->set('rate',$rate);
		Template::getInstance()->render('buyship/checkout.htm');
	}
	public function orderlist($base){

		$order = new SOrder();

		$orderList = $order->orderList();
		//GF::print_r($orderList);
 		$base->set('orderList',$orderList);
		Template::getInstance()->render('buyship/orderlist.htm');
	}
	public function moneywallet($base){
		$order = new SOrdermrg();
		$order->moneyWallet();
	}
	public function view($base){

		$order = new SOrder();
        $ordermrg = new SOrdermrg();

        $setting = new SSetting();
        $base->set('genid','4');
        $ship_content = $setting->getGeneralSettingByuser();

		$itemList = $order->itemList();
		$orderInfo = $order->orderInfo();
        $shipItem = $order->getshipItem();
        $orderBySaller = $ordermrg->getSallerOrder(); //Weerasak 29-10-2561
		//GF::print_r($orderInfo);

		//GF::print_r($itemList);

        //GF::print_r($orderBySaller);

 		$base->set('itemList',$itemList);
 		$base->set('orderInfo',$orderInfo);
        $base->set('shipItem',$shipItem);
        $base->set('ship_content',$ship_content);
        $base->set('orderBySaller',$orderBySaller); //Weerasak 29-10-2564
		Template::getInstance()->render('buyship/view.htm');
	}
	public function confirmpayment($base){
      unset($_SESSION['odr_search']);
		$order = new SOrder();

		$orderInfo = $order->orderInfo(); 
        $bank = $order->getBank();
        $banklist = $order->bankList();

        $base->set('bank',$bank);
        $base->set('banklist',$banklist);
		$base->set('order_type','1');

      if($orderInfo['id']==''){
         $base->set('order_type','3');
      }

 		$base->set('orderInfo',$orderInfo);
		Template::getInstance()->render('buyship/confirmpay.htm');
	}
	public function refundlist($base){
      unset($_SESSION['odr_search']);
		$order = new SOrder();

		$refList = $order->refundlist();
      //GF::print_r($refList);

 		$base->set('refList',$refList);
		Template::getInstance()->render('buyship/refundlist.htm');
	}
	public function reorder($base){
 		$order = new SOrder();
 		$order->reOrder();
	}
	public function cancelorder($base){
 		$order = new SOrder();
 		$order->cancelOrder();
	}
	public function delitem($base){
 		$order = new SOrder();
 		$order->delItem();
	}
	public function updateqty($base){
 		$order = new SOrder();
 		$order->updateQTY();
	}
	public function updatecomment($base){
 		$order = new SOrder();
 		$order->updateComment();
	}
	public function updateoption($base){
 		$order = new SOrder();
 		$order->updateOption();
	}
	public function removeitem($base){
 		$order = new SOrder();
 		//$order->removeItem();
 		//GF::print_r($_SESSION['item'][$base->get('GET.key')]);
 		if(isset($_SESSION['item'][$base->get('GET.key')])){
			unset($_SESSION['item'][$base->get('GET.key')]);
			setcookie('carts', $_SESSION['item'], time() + (86400 * 2), "/");
		}
	}
   public function cancelrefund($base){
 		$order = new SOrder();
 		$order->cancelrefund();
	}
	public function resetsearch($base){
 		unset($_SESSION['odr_search']);
	}
   public function changeoption($base){
      $order = new SOrder();
 		$result = $order->changeoption();
	}


	public function getecoupon($base){
		$setting = new SSetting();
		$base->set('e_no',$base->get('GET.e_no'));
		$ecoupon = $setting->getEcouponByCode();
		//GF::print_r($ecoupon);
		if($ecoupon['e_id']!=''){
			$now = date('Y-m-d');
			$now = strtotime($now);
            if($ecoupon['used']=='T'){
                if($ecoupon['start_date']!='0000-00-00'){
                    $strat_date = strtotime($ecoupon['start_date']);
                    $end_date = strtotime($ecoupon['end_date']);
                    //echo($strat_date."-".$now);
                    if($now>=$strat_date && $now<=$end_date){
                        $_SESSION['coupon_code'] = $ecoupon['e_no'];
                        $_SESSION['e_amount'] = $ecoupon['e_amount'];
                        $_SESSION['e_type'] = $ecoupon['e_type'];
                        if($ecoupon['e_min']>0){
                            $ttbaht = $_SESSION['allyuan']*GF::getrate();
                            if($ttbaht<$ecoupon['e_min']){
                                $_SESSION['coupon_code'] = '';
                                $_SESSION['e_amount'] = '';
                                $_SESSION['e_type'] = '';
                                $_SESSION['e_min'] = $ecoupon['e_min'];
                                $ecoupon['used'] = 'N';
                            }
                        }
                    }else{
                        $_SESSION['coupon_code'] = '';
                        $_SESSION['e_amount'] = '';
                        $_SESSION['e_type'] = '';
                        $_SESSION['e_min'] = '';
                    }
                }else{
                    $_SESSION['coupon_code'] = $ecoupon['e_no'];
                    $_SESSION['e_amount'] = $ecoupon['e_amount'];
                    $_SESSION['e_type'] = $ecoupon['e_type'];
                    if($ecoupon['e_min']>0){
                        $ttbaht = $_SESSION['allyuan']*GF::getrate();
                        if($ttbaht<$ecoupon['e_min']){
                            $_SESSION['coupon_code'] = '';
                            $_SESSION['e_amount'] = '';
                            $_SESSION['e_type'] = '';
                            $_SESSION['e_min'] = $ecoupon['e_min'];
                            $ecoupon['used'] = 'N';
                        }
                    }
                }
            }else{
                $_SESSION['coupon_code'] = '';
                $_SESSION['e_amount'] = '';
                $_SESSION['e_type'] = '';
                $_SESSION['e_min'] = '';
            }
			echo $ecoupon['used'];


		}else{
            $_SESSION['coupon_code'] = '';
            $_SESSION['e_amount'] = '';
            $_SESSION['e_type'] = '';
            $_SESSION['e_min'] = '';
            echo 'NULL';
        }
	}

	public function setkeyorder($base){
		$order = new SOrder();
		$result = $order->setKeyForOrder();
		if($result){
			echo '<script> window.top.location.hash = "/buyship/checkout/?'.time().'";</script>';
		}else{
			echo "F";
		}
	}

  public function iteminfo($base){
		$order = new SOrder();
		$result = $order->itemInfo();

    $resultreturn = json_encode($result);
    echo $resultreturn;

	}

  public function get_order_overdue_payment($base){
    $order = new SOrder();
    $result = $order->get_order_overdue_payment();
    $resultreturn = json_encode($result);
    echo $resultreturn;
  }
	public function processFrm($base){
		$order = new SOrder();
		$mode = $base->get('POST.mode');
		if($mode=='additem'){
			$result = $order->additem();
			
			 echo json_encode($result);
			
		}
		else if($mode=='takeorder'){
			$result = $order->takeOrder();
			if($result){
				echo '<script>window.top.location.hash ="/buyship/view/'.$result.'/";</script>';
			}else{
				echo $result;
			}
		}
		else if($mode=='updateslip'){
			$result = $order->updateSlip();
			if($result){
				echo '<script>window.top.resultSlip();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='uploadtemp'){
			$result = $order->uploadTemp();
			if($result){
				echo '<script>window.top.resultTemp("'.$result.'");</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='uploadtemp2'){
			//GF::print_r($base->get('POST'));
			$result = $order->uploadTemp();
			if($result){
				$_SESSION['item'][$base->get('POST.key')]['product_image'] = $result;
				echo '<script>window.top.resultTemp2();</script>';
			}else{
				echo "F";
			}
		}
      else if($mode=='uploadtemp3'){
			//GF::print_r($base->get('POST'));
			$result = $order->uploadTemp();
			if($result){
				//$_SESSION['item'][$base->get('POST.key')]['product_image'] = $result;
            //$xd = explode('/', $result);

            $base->set('pathimg',$result);
            $order->updateItemImg();
				echo '<script>window.top.uploadfiletempCallBack();</script>';
			}else{
				echo "F";
			}
		}
    else if($mode=='addrefund'){
			//GF::print_r($base->get('POST'));
			$result = $order->addrefund();
			if($result){
				echo '<script>window.top.saveRefundSuccess();</script>';
			}else{
				echo '<script>window.top.saveRefundSuccess();</script>';
			}
		}
	else if($mode=='searchodr'){
			$_SESSION['odr_search']['order_number'] = $base->get('POST.order_number');
			$_SESSION['odr_search']['status'] = $base->get('POST.status');
			$_SESSION['odr_search']['create_dtm'] = $base->get('POST.create_dtm');
			echo '<script>window.top.reorderList();</script>';
		}

	}



	public function actionDetail()
    {
      try{
          $session = Yii::$app->session;
          $cart_db = $session->get('cart_pro');


          $get = Yii::$app->request->get();

          $curl = curl_init();
           $provider = empty($_GET['provider']) ? '' : $_GET['provider'];
          if($provider == "taobao"){
                $endpoint = "https://api.openchinaapi.com/v1/taobao/products/";
              }else if($provider == "1688"){
                $endpoint = "https://api.openchinaapi.com/v1/1688/products/";
              }else{
                $endpoint = "https://api.openchinaapi.com/v1/taobao/products/";
              }

              $apiToken = "ad94ef3201d175a764b47e153e764e22680b9f4c";
              $authorization  = "Authorization: Token ".$apiToken;


          curl_setopt_array($curl, array(
            CURLOPT_URL => $endpoint.$get["num_iid"],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              $authorization
            ),
          ));

          $response = curl_exec($curl);

          $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return $this->redirect('nullpage');
            } else

          if ($response != "invalid ip") {
             $json = json_decode($response,true);
             $isPromo = 0;
             $promotion_price = 0;
             if(empty($json['data']['is_promotion'])){
               $isPromo = 0;
             }else{
               $isPromo = $json['data']['is_promotion'];
               $promotion_price = $json['data']['orginal_price'];
             }

             if(isset($json['data']['price'])){
               $price = $json['data']['price'];

             }else{
                $itemid = $get["num_iid"];
                //$result    = json_decode($response, true);
               //return $this->redirect('nullpage');
               //return $this->redirect(Url::to(['/product/nullpage','itemid'=>$itemid]));
               return $this->render('null_page',['itemid' => $itemid]);
             }
             if(isset($json['data']['priceRange'])){
               $priceRange = $json['data']['priceRange'];
             }else{
               $priceRange = [];

             }

             $stock = $json['data']['num'];
             if(isset($json['data']['min_num'])){
               $minNum = $json['data']['min_num'];
             }else{
               $minNum = 1;
             }
             //$promoPrice = ($json["promotion_price"][0]["price"])?$json["promotion_price"][0]["price"]:$json["promotion_price"][0]["price"];
             //if($json["price"][0]["price"]!=$json["promotion_price"][0]["price"]){
             // $isPromo = 1;
             //}
             $item = array();
             $item["item"] = $json['data'];
            // print_r( $item);
             $data = [
                 "item" => json_encode($item, JSON_FORCE_OBJECT),
                 "itemId" => $get["num_iid"],
                 "provider" => $get["provider"],
                 "isPromo" => $isPromo,
                 "price" => $price,
                 "priceRange" => json_encode($priceRange),
                 "promotion_price" => $promotion_price,
                 "stock" => $stock,
                 "minNum" => $minNum,
                 "cart" => $cart_db,
             ];
             return $this->render('detail',$data);
          } else {
             return $this->redirect('/site/index');
          }

      }catch(Exception $e) {
        echo "string";
      }

    }

 }
?>
