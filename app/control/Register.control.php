<?php
 class Register {

 	public function noroute(){

	}

 	public function authenregister($base){
        $register = new SRegister();
        $province = $register->provinceList();

        $setting = new SSetting();
        $base->set('genid','5');
        $ship_content = $setting->getGeneralSettingByuser();


        $base->set('province',$province);
        $base->set('ship_content',$ship_content);
		Template::getInstance()->render('register/register.htm');
	}
   public function amphur($base){
      $register = new SRegister();
      $amphur = $register->amphurList();

      $resultHTML = "<label>อำเภอ</label>";
      $resultHTML .= '<select class="form-control" onchange="changeAmphur(this);"  id="amphur-register" name="amphur-register">';
      $resultHTML .= '<option value="">เลือกอำเภอ</option>';
      if(count($amphur)>0){
         foreach($amphur as $alist){
            $resultHTML .= '<option value="'.$alist['AMPHUR_ID'].'">'.$alist['AMPHUR_NAME'].'</option>';
         }
      }
      $resultHTML .= '</select>';
      $resultHTML .= '<span class="glyphicon glyphicon-star form-control-feedback" style="color: red; font-size:8px; "></span>';

      echo $resultHTML;

	}
   public function district($base){
      $register = new SRegister();
      $district = $register->districtList();

      $resultHTML = "<label>ตำบล</label>";
      $resultHTML .= '<select class="form-control"  id="district-register" name="district-register">';
      $resultHTML .= '<option value="">เลือกตำบล</option>';
      if(count($district)>0){
         foreach($district as $dlist){
            $resultHTML .= '<option value="'.$dlist['DISTRICT_ID'].'">'.$dlist['DISTRICT_NAME'].'</option>';
         }
      }
      $resultHTML .= '</select>';
      $resultHTML .= '<span class="glyphicon glyphicon-star form-control-feedback" style="color: red; font-size:8px; "></span>';

      echo $resultHTML;

	}
   // public function district($base){
   //    $register = new SRegister();
   //    $district = $register->districtList();
   //
   //    $resultHTML = "<label>ตำบล</label>";
   //    $resultHTML .= '<select class="form-control">';
   //    $resultHTML .= '<option value="">เลือกตำบล</option>';
   //    if(count($district)>0){
   //       foreach($district as $dlist){
   //          $resultHTML .= '<option value="'.$dlist['ADISTRICT_ID'].'">'.$dlist['DISTRICT_NAME'].'</option>';
   //       }
   //    }
   //    $resultHTML .= '</select>';
   //    $resultHTML .= '<span class="glyphicon glyphicon-star form-control-feedback" style="color: red; font-size:8px; "></span>';
   //
   //    echo $resultHTML;
   //
	// }
	public function registerMember($base){
		$register = new SRegister();
		$result	=	$register->saveRegister();
		if($result){
			echo '<script>window.top.registerMemberCallback();</script>';
		}
		//GF::print_r($base->get('POST'));
	}
	public function confermlogin($base){
		$register = new SRegister();
		$result	=	$register->confermclasslogin();
		$BASEURL=$base->get('BASEURL');
		if($result=='T'){
				echo "<script>
				alert('Register Complete ,. Please Login !');
				window.location = '$BASEURL/auth/';</script>";

			}else{
				echo "<script>
				alert('Register Fail ,.Try again ... !');
				window.location = '$BASEURL/register/';
				</script>";

			}
	}
	public function checkRegister($base){
		$member = new Member();
  		echo $member->checkUsername();
	}

 }
?>
