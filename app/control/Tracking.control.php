<?php
 class Tracking extends Permission{
 	
 	public function beforeroute($base){
 		$this->module_ids = '6';
		$this->set_permission('trackinglist','24');

		
		$this->set_view('view',NULL);
		$this->set_edit('create',NULL);
		$this->set_edit('edit',NULL);
		$this->set_delete('deleteexp',NULL);
		
		$this->PermissionAuth();
		
	}
 

	
	/**
	* 
	* TRACKING
	* 
	* @return
	*/
	public function trackinglist($base){
 		
 		$tracking = new STracking();
 		
 		$trackingList = $tracking->trackingListByUser();
		
		//GF::print_r($trackingList);
		$base->set('trackingList',$trackingList);
		Template::getInstance()->render('tracking/tracking.htm');
	}
	public function trackingforship($base){
		$order = new SOrder();
 		$tracking = new STracking();
 		
 		$base->set('status','4');
 		$trackingList = $tracking->trackingListByUserStatus();
		$orderDetail = $order->get_order_detail();

		$base->set('trackingList',$trackingList);
		$base->set('orderDetail',$orderDetail);
		
		Template::getInstance()->render('tracking/trackingok.htm');
	}

	public function view($base){
 		
 		$order = new SOrdermrg();
 		$tracking = new STracking();

 		$trackingInfo = $tracking->trackingInfo();
		//GF::print_r($trackingInfo);
		$base->set('trackingInfo',$trackingInfo);
		Template::getInstance()->render('tracking/viewtracking.htm');
	}
	public function getship($base){
		
		$tracking = new STracking();
		
		$member = new Member();
 		$memberInfomation = $member->memberInfomation();

 		$trackingList = $tracking->listTrackingByLink();
 		//$user_id = $memberInfomation['id'];
 		
 		//GF::print_r($trackingList);
 		$base->set('trackingList',$trackingList);
 		$base->set('memberinfo',$memberInfomation);
		Template::getInstance()->render('tracking/getship.htm');
	}
	public function shippinglist($base){
 		
 		$tracking = new STracking();
 		
 		$shippingList = $tracking->shippingListByuser();
		
		// GF::print_r($shippingList);
 		
 		$base->set('shippingList',$shippingList);
		Template::getInstance()->render('tracking/shippinglist.htm');
	}
	public function daleteshipping($base){
 		$tracking = new STracking();
 		$shippingList = $tracking->deleteShipping();	
	}
	public function viewshipping($base){
		$tracking = new STracking();
		$shippingInfo= $tracking->shippingInfo();
 		
 		$base->set('shippingInfo',$shippingInfo);
		Template::getInstance()->render('tracking/viewshipping.htm');
	}
	public function moneywallet($base){
		$tracking = new STracking();
		$tracking->getMoneyFromWallet();
	}
	
	public function confirmpayment($base){
		
		$order = new SOrder();
		$tracking = new STracking();
		$shippingInfo= $tracking->shippingInfo();
		
		$orderInfo = $shippingInfo;
        $bank = $order->getBank();
        $banklist = $order->bankList();
		
		$base->set('bank',$bank);
		$base->set('order_type','2');
         $base->set('orderInfo',$orderInfo);
         $base->set('banklist',$banklist);
		Template::getInstance()->render('buyship/confirmpay.htm');
	}
	public function daletetrackinguser($base){
		$tracking = new STracking();
		$tracking->deleteTrackingUser();
	}
	
	
	public function processFrm($base){
		$order = new SOrdermrg();
		$tracking = new STracking();
		$mode = $base->get('POST.mode');
		//GF::print_r($base->get('POST'));
		
		if($mode=='grouptrackid'){
			
			$tracking_id = $base->get('POST.trackid');
			$track_IN = implode(',',$tracking_id);
			
			$trackencode = base64_encode($track_IN);
			$trackencode = urlencode($trackencode);
			
			
			echo '<script>window.top.location.hash = "/tracking/getship/?id='.$trackencode.'";</script>';
			
		}
		else if($mode=='getshipping'){
			$result = $tracking->saveGetShipping();
			if($result){
				echo '<script>window.top.successShipCallBack();</script>';
			}else{
				echo "F";
			}
		}
		
	}
	
 }
?>