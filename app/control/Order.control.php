

<?php
// เปิดดู Error 
   //ini_set('display_errors', 1);
   //ini_set('display_startup_errors', 1);
   //error_reporting(E_ALL);

 class Order extends Permission{

 	public function beforeroute($base){
 		$this->module_ids = '7';
		$this->set_permission('orderlist','36');
		$this->set_permission('chinaorderview','36');
		$this->set_permission('tracking','22');
		$this->set_permission('orderbuy','27');
        $this->set_permission('refundlist','32');

		$this->set_view('view',NULL);
		$this->set_edit('create',NULL);
		$this->set_edit('edit',NULL);
		$this->set_delete('deleteexp',NULL);

        $this->set_view('view','32');
        $this->set_edit('edit','32');
        $this->set_delete('delete','32');



		$this->PermissionAuth();

	}

 	/*public function orderlist($base){

		Template::getInstance()->render('buyship/order.htm');
	}*/
	/*public function checkout($base){
 		$order = new SOrdermrg();
 		$rate = $order->getRate();

 		$base->set('rate',$rate);
		Template::getInstance()->render('buyship/checkout.htm');
	}*/
	public function checkmembercode($base){
		$memberinfo = GF::memberinfo($base->get('GET.code'));
		if($memberinfo['id']!=''){
			$arr['code'] = "T";
			
			echo json_encode($arr);
		}else{
			$arr['code'] = "F";
			echo json_encode($arr);
		}
	}
    public function ordernotrackinglist($base){
        unset($_SESSION['pay_search']);
        unset($_SESSION['odrbuy_search']);
        unset($_SESSION['odr_ref']);

		$order = new SOrdermrg();
		$setting = new SSetting();

		$base->set('type','buy');
		$orderList = $order->ordernoTrackList();
		//GF::print_r($orderList);
		$tplList = $setting->templateList();
 		$outsourceList = $order->outsourceListNoTrack();
		$base->set('outsourceList',$outsourceList);
		$base->set('tplList',$tplList);
 		$base->set('orderList',$orderList);
		Template::getInstance()->render('order/ordernotrackinglist.htm');
    }
    public function ordernotracking_send($base){
        unset($_SESSION['pay_search']);
        unset($_SESSION['odrbuy_search']);
        unset($_SESSION['odr_ref']);

		$order = new SOrdermrg();
		$setting = new SSetting();

		$base->set('type','buy');
		$orderList = $order->ordernoTrackListSend();
		$tplList = $setting->templateList();
		$outsourceList = $order->outsourceListNoTrack();
$base->set('outsourceList',$outsourceList);
		$base->set('tplList',$tplList);
 		$base->set('orderList',$orderList);
		Template::getInstance()->render('order/ordernotracking_send.htm');
    }
    public function setnotrack($base){
        $order = new SOrdermrg();
        $order->setNotrackToDash();
    }
	public function orderlist($base){

        

        unset($_SESSION['pay_search']);
        unset($_SESSION['odrbuy_search']);
        unset($_SESSION['odr_ref']);

		$order = new SOrdermrg();
		$setting = new SSetting();

		$base->set('type','buy');
		$orderList = $order->orderList();
        $tplList = $setting->templateList();
        $outsourceList = $order->outsourceList();
//GF::print_r($orderList);
		$base->set('tplList',$tplList);
         $base->set('orderList',$orderList);
         $base->set('outsourceList',$outsourceList);
		Template::getInstance()->render('order/orderlist.htm');
	}
    public function setforimport($base){
        $value_cc = $base->get('GET.value');
        if($value_cc!=''){
            //$_SESSION['order_import'] = array();
            if(count($_SESSION['order_import'])==0){
                $_SESSION['order_import'][] = $value_cc;
            }else{
                $t = 0;
                foreach($_SESSION['order_import'] as $key=>$vals){
                    if($vals==$value_cc){
                        unset($_SESSION['order_import'][$key]);
                        $t = 1;
                        //echo 'rrr';
                    }
                }
                if($t==0){
                    $_SESSION['order_import'][] = $value_cc;
                }
            }
        }
        
        print_r($_SESSION['order_import']);
    }
    
    public function setfororder($base){
        $values = $base->get('POST.order_id_id');
        if(count($values)>0){
            foreach($values as $key=>$vals){
				$_SESSION['order_import'][] = $vals;
			}
        }
    }
	public function unsetfororder($base){
        $values = $base->get('POST.order_id_id');
        if(count($values)>0){
            foreach($values as $key=>$vals){
				foreach($_SESSION['order_import'] as $kayd=>$valsd){
					if($valsd==$vals){
						unset($_SESSION['order_import'][$kayd]);
					}
				}
			}
        }
    }

	public function setforchinaorder($base){
        $values = $base->get('POST.order_id_id');
        if(count($values)>0){
            foreach($values as $key=>$vals){
				$_SESSION['order_chinarorder'][] = $vals;
			}
        }
    }
	public function unsetforchinaorder($base){
        $values = $base->get('POST.order_id_id');
        if(count($values)>0){
            foreach($values as $key=>$vals){
				foreach($_SESSION['order_chinarorder'] as $kayd=>$valsd){
					if($valsd==$vals){
						unset($_SESSION['order_chinarorder'][$kayd]);
					}
				}
			}
        }
    }
	public function setforchinaorder_id($base){
        $values = $base->get('GET.value');
		if($values!=''){
			$_SESSION['order_chinarorder'][] = $values;
		}	
    }
	public function unsetforchinaorder_id($base){
        $values = $base->get('GET.value');
		if($values!=''){
            if(count($_SESSION['order_chinarorder'])>0){
				foreach($_SESSION['order_chinarorder'] as $kayd=>$valsd){
					if($valsd==$values){
						unset($_SESSION['order_chinarorder'][$kayd]);
					}
				}
			}
		}
    }
	public function importlist($base){

      unset($_SESSION['pay_search']);
      unset($_SESSION['odrbuy_search']);
      unset($_SESSION['odr_ref']);
      unset($_SESSION['odr_search']);

		$order = new SOrdermrg();
		$setting = new SSetting();

		$base->set('type','import');
		$orderList = $order->orderList();
		$tplList = $setting->templateList();

		$base->set('tplList',$tplList);
 		$base->set('orderList',$orderList);
		Template::getInstance()->render('order/importlist.htm');
	}

	public function view($base){

		$order = new SOrdermrg();

		$itemList = $order->itemList();
		$orderInfo = $order->orderInfo();
        $shipItem = $order->getshipItem();
        $orderBySaller = $order->getSallerOrder(); //Weerasak 29-10-2561
		$rate = $order->getRate();

 		$base->set('rate',$rate);



 		$base->set('itemList',$itemList);
 		$base->set('orderInfo',$orderInfo);
        $base->set('shipItem',$shipItem);
        $base->set('orderBySaller',$orderBySaller); //Weerasak 29-10-2564
		Template::getInstance()->render('order/view.htm');
	}
	public function view2($base){

		$order = new SOrdermrg();

		$itemList = $order->itemList();
		$orderInfo = $order->orderInfo();
      $shipItem = $order->getshipItem();

		$rate = $order->getRate();

 		$base->set('rate',$rate);

 		$base->set('itemList',$itemList);
 		$base->set('orderInfo',$orderInfo);
      $base->set('shipItem',$shipItem);
		Template::getInstance()->render('order/view2.htm');
	}
	public function orderbuy($base){

      unset($_SESSION['pay_search']);
      //unset($_SESSION['odrbuy_search']);
      unset($_SESSION['odr_ref']);
      unset($_SESSION['odr_search']);

		$order = new SOrdermrg();
		$setting = new SSetting();

		$base->set('type','export');
		$orderList = $order->orderList();
		$tplList = $setting->templateList();

		$base->set('tplList',$tplList);
 		$base->set('orderList',$orderList);
		Template::getInstance()->render('order/ordermrg.htm');
	}

	public function paymentlist($base){

      //unset($_SESSION['pay_search']);
      unset($_SESSION['odrbuy_search']);
      unset($_SESSION['odr_ref']);
      unset($_SESSION['odr_search']);

 		$order = new SOrdermrg();

 		$paymentList = $order->paymentList();

 		//GF::print_r($paymentList);

 		$base->set('paymentList',$paymentList);
		Template::getInstance()->render('order/paymentlist.htm');
	}
   public function refundlist($base){

      unset($_SESSION['pay_search']);
      unset($_SESSION['odrbuy_search']);
      //unset($_SESSION['odr_ref']);
      unset($_SESSION['odr_search']);

		$order = new SOrdermrg();

		$refList = $order->refundlist();
      //GF::print_r($refList);

 		$base->set('refList',$refList);
		Template::getInstance()->render('order/refundlist.htm');
	}
    public function refundlisthold($base){

      unset($_SESSION['pay_search']);
      unset($_SESSION['odrbuy_search']);
      //unset($_SESSION['odr_ref']);
      unset($_SESSION['odr_search']);

		$order = new SOrdermrg();
        $base->set('_hold_','A');
		$refList = $order->refundlist();
      //GF::print_r($refList);

 		$base->set('refList',$refList);
		Template::getInstance()->render('order/refundlist.htm');
	}
   public function refundexport($base){
 		$order = new SOrdermrg();
 		$result = $order->exportRefund();
		//GF::print_r($_SESSION);
      echo $result;
	}
   public function refundexportexcel($base){
		$order = new SOrdermrg();
      $dataid = $base->get('GET.id');
      $base->set('_exp_ref',$dataid);

      $refList = $order->refundListExcel();

 		$base->set('refList',$refList);
		Template::getInstance()->render('order/refundexportexcel.htm');
	}

   public function chinaorder($base){

      unset($_SESSION['pay_search']);
      //unset($_SESSION['odrbuy_search']);
      unset($_SESSION['odr_ref']);
      unset($_SESSION['odr_search']);

		$order = new SOrdermrg();
		$setting = new SSetting();

        $base->set('type','buy');
        $base->set('type_b','view');
		$orderList = $order->orderListBuy();
		$tplList = $setting->templateList();
//GF::print_r($orderList);
		$base->set('tplList',$tplList);
 		$base->set('orderList',$orderList);
		Template::getInstance()->render('order/ordervendorlist.htm');
    }
    public function chinaorderuser($base){
        
              unset($_SESSION['pay_search']);
              //unset($_SESSION['odrbuy_search']);
              unset($_SESSION['odr_ref']);
              unset($_SESSION['odr_search']);
        
                $order = new SOrdermrg();
                $setting = new SSetting();
        
                $base->set('type','buy');
                
                $orderList = $order->orderListBuyUser();
                $tplList = $setting->templateList();
                $outsourceList = $order->outsourceList();
        
                $base->set('tplList',$tplList);
                $base->set('orderList',$orderList);
                $base->set('outsourceList',$outsourceList);
                Template::getInstance()->render('order/ordervendorlistuser.htm');
            }
   public function chinaorderview($base){



		$order = new SOrdermrg();
		$setting = new SSetting();

		$base->set('type','buy');
		$orderList = $order->orderListBuyView();
		//$tplList = $setting->templateList();

		//$base->set('tplList',$tplList);
 		$base->set('orderList',$orderList);
		Template::getInstance()->render('order/chinaorderlistview.htm');
	}
    public function chinaordercal($base){
        $rate = $base->get('GET.rate');
        $valuse = $base->get('GET.value');
        echo number_format(($valuse*$rate),2);
	}

	public function orderstatus($base){
 		$order = new SOrdermrg();
 		$order->orderStatus();
	}
	public function reorder($base){
 		$order = new SOrdermrg();
 		$order->reOrder();
	}
	public function cancelorder($base){
 		$order = new SOrdermrg();
 		$order->cancelOrder();
	}
	public function delitem($base){
 		$order = new SOrdermrg();
 		$order->delItem();
	}
	public function updateqty($base){
 		$order = new SOrdermrg();
 		$order->itemQty();
	}
	public function updatermb($base){
 		$order = new SOrdermrg();
 		$order->priceRmb();
	}
	public function updatesaller($base){
 		$order = new SOrdermrg();
 		$order->sallerNumber();
	}
	public function updatethb($base){
 		$order = new SOrdermrg();
 		$order->priceThb();
	}
	public function itemstatus($base){
 		$order = new SOrdermrg();
 		$order->itemStatus();
	}
   public function itemtype($base){
 		$order = new SOrdermrg();
 		$order->itemType();
	}
	public function updatecommentitem($base){
 		$order = new SOrdermrg();
 		$order->itemComment();
	}
	public function removeitem($base){
 		$order = new SOrdermrg();
 		$order->delItem();
	}
	public function paymentstatus($base){
 		$order = new SOrdermrg();
 		$order->paymentStatus();
	}
	public function orderpay($base){
 		$order = new SOrdermrg();
 		$order->orderPay();
	}
	public function notibuystatus($base){
 		$order = new SOrdermrg();
 		$order->notiBuyStatus();
	}
	public function refundwallet($base){
 		$order = new SOrdermrg();
 		$order->refundToWallet();
	}

   public function addcnship($base){
 		$order = new SOrdermrg();
 		$order->addshipItem();
	}
   public function updatecnship($base){
 		$order = new SOrdermrg();
 		$order->updateshipItem();
	}
   public function changetruerate($base){
 		$order = new SOrdermrg();
 		$order->updateTrueRate();
    }
    public function sendmailordernotrack($base){
        $order = new SOrdermrg();
        $order->mailOrderNoTracking();
    }
    public function notrackcomment($base){
        $order = new SOrdermrg();
        $order->setNotrackComment();
    }
   
	/**
	*
	* TRACKING
	*
	* @return
	*/
	/* Add By Weerasak 28/02/2565 */
	public function searchTracking($base){
		$tracking = new STracking();
		$track_number = $base->get('GET.tracking');
		//$base->set('tracking',$track_number);
		$result = $tracking->SearchTracking();
		echo json_encode($result);
	}
	/* End */
	public function tracking($base){

 		$order = new SOrdermrg();
 		$tracking = new STracking();

 		$trackingList = $tracking->trackingList();

 		//GF::print_r($trackingList);
		$base->set('trackingList',$trackingList);
		Template::getInstance()->render('order/tracking.htm');
	}
	public function trackingok($base){

 		$order = new SOrdermrg();
 		$tracking = new STracking();

 		$base->set('status','4');
 		$trackingList = $tracking->trackingListByStatus();
		$base->set('trackingList',$trackingList);
		Template::getInstance()->render('order/trackingok.htm');
	}
	public function createexcel($base){
		$order = new SOrdermrg();
		$order->exportExcel();
	}

	public function createtracking($base){


		Template::getInstance()->render('order/viewtracking.htm');
	}
	public function viewtracking($base){

 		$order = new SOrdermrg();
 		$tracking = new STracking();

 		$trackingInfo = $tracking->trackingInfo();

 		//GF::print_r($trackingInfo);

		$base->set('trackingInfo',$trackingInfo);
		Template::getInstance()->render('order/viewtracking.htm');
	}

	
	public function deletetracking($base){
 		$tracking = new STracking();
 		$tracking->deleteTracking();
	}
	public function ratebyorder($base){
 		$tracking = new STracking();
 		//$tracking->deleteTracking();
 		$user_id = $base->get('GET.member');

 		$member = new Member();
 		$base->set('user_id',$user_id);
		$memberInfo = $member->memberInfomationByID();


 		$params['member_id'] = $memberInfo['id'];

 		$ship_by = $base->get('GET.ship_by');
 		$odr_typ = $base->get('GET.type');

 		$base->set('odr_type',$odr_typ);
 		$base->set('odr_by',$ship_by);

 		$result = $tracking->getRateByOrder($params);

 		echo json_encode($result);

	}
   public function trackingforship($base){

 		$tracking = new STracking();

 		$base->set('status','4');
 		$trackingList = $tracking->trackingListByUserStatusAdmin();
		$base->set('trackingList',$trackingList);
		Template::getInstance()->render('order/trackingforship.htm');
	}
   public function getship($base){

		$tracking = new STracking();

		$member = new Member();
      $base->set('user_id',$_SESSION['track_search']['user_code']);
 		$memberInfomation = $member->memberInfomationByID();

 		$trackingList = $tracking->listTrackingByLink();
 		//$user_id = $memberInfomation['id'];

 		//GF::print_r($trackingList);
 		$base->set('trackingList',$trackingList);
 		$base->set('memberinfo',$memberInfomation);
		Template::getInstance()->render('order/getship.htm');
	}
   /**
	*
	* TRACKING HOLD
	*
	* @return
	*/
   public function trackinghold($base){

 		$order = new SOrdermrg();
 		$tracking = new STracking();

 		$trackingList = $tracking->trackingListHold(); 

 		//GF::print_r($trackingList);
		$base->set('trackingList',$trackingList);
		Template::getInstance()->render('order/trackinghold.htm');
	}

    /**
	*
	* TH SHIPPING
	*
	* @return
	*/

    public function thshippinglist($base){

 		$tracking = new STracking();

 		$shippingList = $tracking->shippingListForTh();



 		$base->set('shippingList',$shippingList);
		Template::getInstance()->render('order/thshippinglist.htm');
	}

	/**
	*
	* SHIPPING
	*
	* @return
	*/

	public function shippinglist($base){

 		$tracking = new STracking();

 		$shippingList = $tracking->shippingListAll();

 		//GF::print_r($shippingList);

 		$base->set('shippingList',$shippingList);
		Template::getInstance()->render('order/shippinglist.htm');
	}
	public function daleteshipping($base){
 		$tracking = new STracking();
 		$shippingList = $tracking->deleteShipping();
	}
	public function viewshipping($base){
		$tracking = new STracking();
		$shippingInfo= $tracking->shippingInfo();
		//GF::print_r($shippingInfo);
 		$base->set('shippingInfo',$shippingInfo);
		Template::getInstance()->render('order/viewshipping.htm');
	}
	public function editshipping($base){
		$tracking = new STracking();
		$shippingInfo= $tracking->shippingInfo();
		//GF::print_r($shippingInfo);
 		$base->set('shippingInfo',$shippingInfo);
		Template::getInstance()->render('order/editshipping.htm');
	}

   public function refundinfo($base){
 		$order = new SOrdermrg();
 		$result = $order->refundinfo();

     $resultreturn = json_encode($result);
     echo $resultreturn;

 	}
   public function refundbeyref($base){
 		$order = new SOrdermrg();
 		$result = $order->refundToWalletByREF();
	}
   public function refundbeyorder($base){
 		$order = new SOrdermrg();
 		$result = $order->refundToWalletByOrder();
	}




   public function updateshippings($base){
 		$tracking = new STracking();
 		$result = $tracking->updateGetShipping2();
 	}
	public function resetsearch($base){
 		unset($_SESSION['odr_search']);
	}
	public function resetsearchno($base){
 		unset($_SESSION['odrno_search']);
	}
	public function resetsearchnose($base){ 
 		unset($_SESSION['odrnose_search']);
	}
   public function resetsearchpay($base){
 		unset($_SESSION['pay_search']);
	}
   public function resetsearch2($base){
 		unset($_SESSION['odrbuy_search']);
	}
   public function resetsearchref($base){
 		unset($_SESSION['odr_ref']);
	}
   public function resetsearchtrack($base){
 		unset($_SESSION['track_search']);
	}
   public function resetSearchtrackinghold($base){
 		unset($_SESSION['tracking_hold_search']);
	}
    public function resetSearchtrackingList($base){
 		unset($_SESSION['tra_search']);
	}
    public function resetSearchshippinglist($base){
 		unset($_SESSION['ship_search']);
	}
    public function resetSearchshippingthlist($base){
 		unset($_SESSION['ship_th_search']);
    }
    public function resetsearchchinauser($base){
        unset($_SESSION['odrbuy_user_search']);
   }



	public function processFrm($base){
		$order = new SOrdermrg();
		$tracking = new STracking();
		$mode = $base->get('POST.mode');
		//GF::print_r($base->get('POST'));
		if($mode=='createitem'){
			$result = $order->addOtherItem();
			if($result){
				echo '<script>window.top.rederOrder("'.$base->get('POST.order_id').'");</script>';
			}else{
				echo "F";
			}
        }
        if($mode=='uploadseller'){
            //GF::print_r($base->get('POST'));
            $result = $order->uploadSallerImage();
            if($result){
                echo '<script>window.top.uploadfiletempCallBack();</script>';
            }else{
                //echo '<script>window.top.successShip22CallBack();</script>';
            }
			
		}
		if($mode=='exportExcel'){
			$result = $order->exportExcel();
			if($result){
				echo '<script>window.top.reorderList();</script>';
			}else{
				echo "F";
			}
		}
		if($mode=='importexcel'){

			$result = $tracking->importTracking();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
		if($mode=='updatetracking'){

			$result = $tracking->updateTracking();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
		if($mode=='updatetrackinggroup'){

			$result = $tracking->updateTrackingGroup();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
      if($mode=='holdtracking'){

			$result = $tracking->updateTrackingHold();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
      if($mode=='holdstatus'){

			$result = $tracking->updateTrackingHoldStatus();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
		if($mode=='editshipping'){
			$result = $tracking->updateGetShipping();
			if($result){
				echo '<script>window.top.successShipCallBack();</script>';
			}else{
				echo "F";
			}
		}
		if($mode=='importorderexcel'){
			$result = $order->importOrderExcel();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo '<script>window.top.noticeMessage("มีรายการที่ถูกคืนเงินแล้วอยู่ในรายการ Order ระบบไม่สามารถอัพโหลดไฟล์ได้",null);</script>';
			}
		}
      if($mode=='updaterefund'){
			$result = $order->updaterefund();
			if($result){
				echo '<script>window.top.location.hash = "/ordermanagement/refundlist/?'.time().'";</script>';
			}else{
				echo "F";
			}
		}
       if($mode=='uploadtemp3'){
			//GF::print_r($base->get('POST'));
			$result = $order->uploadTempOrder();
			if($result){
				//$_SESSION['item'][$base->get('POST.key')]['product_image'] = $result;
            //$xd = explode('/', $result);

            $base->set('pathimg',$result);
            $order->updateItemImg();
				echo '<script>window.top.uploadfiletempCallBack();</script>';
			}else{
				echo "F";
			}
		}
	   if($mode=='searchodr'){
			$_SESSION['odr_search']['order_number'] = trim($base->get('POST.order_number'));
            $_SESSION['odr_search']['user_code'] = trim($base->get('POST.user_code'));
            $_SESSION['odr_search']['buy_id'] = trim($base->get('POST.buy_id'));
			$_SESSION['odr_search']['status'] = trim($base->get('POST.status'));
			$_SESSION['odr_search']['create_dtm'] = trim($base->get('POST.create_dtm'));
			echo '<script>window.top.reorderList();</script>';
		}
		if($mode=='searchodrno'){
			$_SESSION['odrno_search']['order_number'] = trim($base->get('POST.order_number'));
            $_SESSION['odrno_search']['user_code'] = trim($base->get('POST.user_code'));
            $_SESSION['odrno_search']['buy_id'] = trim($base->get('POST.buy_id'));
			$_SESSION['odrno_search']['create_dtm'] = trim($base->get('POST.create_dtm'));
			echo '<script>window.top.reorderList();</script>';
		}
		if($mode=='searchodrnoSe'){
			$_SESSION['odrnose_search']['order_number'] = trim($base->get('POST.order_number'));
            $_SESSION['odrnose_search']['user_code'] = trim($base->get('POST.user_code'));
            $_SESSION['odrnose_search']['buy_id'] = trim($base->get('POST.buy_id'));
			$_SESSION['odrnose_search']['create_dtm'] = trim($base->get('POST.create_dtm'));
			echo '<script>window.top.reorderListSe();</script>';
		}
      if($mode=='searchpay'){
            $_SESSION['pay_search']['user_code'] = trim($base->get('POST.user_code'));
  			$_SESSION['pay_search']['order_number'] = trim($base->get('POST.order_number'));
  			$_SESSION['pay_search']['status'] = trim($base->get('POST.status'));
  			$_SESSION['pay_search']['create_dtm'] = trim($base->get('POST.create_dtm'));
			$_SESSION['pay_search']['start_dtm'] = trim($base->get('POST.start_dtm'));
			$_SESSION['pay_search']['end_dtm'] = trim($base->get('POST.end_dtm'));
  			echo '<script>window.top.rePaymentList();</script>';
  		}
      if($mode=='searchodrchina'){
			$_SESSION['odrbuy_search']['order_number'] = trim($base->get('POST.order_number'));
            $_SESSION['odrbuy_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['odrbuy_search']['status'] = trim($base->get('POST.status'));
			$_SESSION['odrbuy_search']['create_dtm'] = trim($base->get('POST.create_dtm'));
            $_SESSION['odrbuy_search']['status_send'] = trim($base->get('POST.status_send'));
			echo '<script>window.top.reorderList();</script>';
        }
        if($mode=='searchodrchinauser'){
			$_SESSION['odrbuy_user_search']['order_number'] = trim($base->get('POST.order_number'));
            $_SESSION['odrbuy_user_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['odrbuy_user_search']['status'] = trim($base->get('POST.status'));
			echo '<script>window.top.reorderUserList();</script>';
		}
      if($mode=='searchodrref'){
			$_SESSION['odr_ref']['order_number'] = trim($base->get('POST.order_number'));
            $_SESSION['odr_ref']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['odr_ref']['status'] = trim($base->get('POST.status'));
			echo '<script>window.top.reorderListRef();</script>';
		}
      if($mode=='searchtrackinghold'){
			$_SESSION['tracking_hold_search']['order_number'] = trim($base->get('POST.order_number'));
            $_SESSION['tracking_hold_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['tracking_hold_search']['status'] = trim($base->get('POST.status'));
			$_SESSION['tracking_hold_search']['create_dtm'] = trim($base->get('POST.create_dtm'));
			echo '<script>window.top.retrackingholdList();</script>';
		}
      if($mode=='searchtrackok'){
         $_SESSION['track_search']['user_code'] = trim($base->get('POST.user_code'));
			echo '<script>window.top.reorderListTrack();</script>';
		}
      if($mode=='send_order_cn'){
			$result = $order->sendOrderToCN();
			if($result){
				echo '<script>window.top.location.hash = "/ordermanagement/chinaorder/?'.time().'";</script>';
			}else{
				echo "F";
			}
        }
        if($mode=='send_order_cnuser'){
			$result = $order->sendOrderToCNUser();
			if($result){
				echo '<script>window.top.location.hash = "/ordermanagement/chinaorderuser/?'.time().'";</script>';
			}else{
				echo "F";
			}
        }
        
      if($mode=='grouptrackid'){

			$tracking_id = $base->get('POST.trackid');
			$track_IN = implode(',',$tracking_id);

			$trackencode = base64_encode($track_IN);
			$trackencode = urlencode($trackencode);


			echo '<script>window.top.location.hash = "/ordermanagement/getship/?id='.$trackencode.'";</script>';

		}
      else if($mode=='getshipping'){
			$result = $tracking->saveGetShippingByAdmin();
			if($result){
				echo '<script>window.top.successShipCallBack();</script>';
			}else{
				echo "F";
			}
		}
        if($mode=='searchtracking'){
            $_SESSION['tra_search']['tracking'] = trim($base->get('POST.tracking'));
			$_SESSION['tra_search']['order_number'] = trim($base->get('POST.order_number'));
            $_SESSION['tra_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['tra_search']['status'] = trim($base->get('POST.status'));
			echo '<script>window.top.successCallBack();</script>';
		}
        if($mode=='searchship'){
            $_SESSION['ship_search']['shipping_code'] = trim($base->get('POST.shipping_code'));
			$_SESSION['ship_search']['track_code'] = trim($base->get('POST.track_code'));
            $_SESSION['ship_search']['ship_by'] = trim($base->get('POST.ship_by'));
            $_SESSION['ship_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['ship_search']['status'] = trim($base->get('POST.status'));
			echo '<script>window.top.successShip2CallBack();</script>';
		}
        if($mode=='searchthship'){
            $_SESSION['ship_th_search']['shipping_code'] = trim($base->get('POST.shipping_code'));
			$_SESSION['ship_th_search']['track_code'] = trim($base->get('POST.track_code'));
            $_SESSION['ship_th_search']['ship_by'] = trim($base->get('POST.ship_by'));
            $_SESSION['ship_th_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['ship_th_search']['status'] = trim($base->get('POST.status'));
			echo '<script>window.top.successShip22CallBack();</script>';
		}
        if($mode=='savethshipping'){
            //GF::print_r($base->get('POST'));
            $result = $tracking->updateTHShipping();
            if($result){
                echo '<script>window.top.successShip22CallBack();</script>';
            }else{
                //echo '<script>window.top.successShip22CallBack();</script>';
            }
			
        }
        

	}

	public function get_tracking_number($tracking){
		$tracking = new STracking();
		$result = $tracking->getTrackingNumber($tracking);
		$resultreturn = json_encode($result);
		echo $resultreturn;
	  }
	// Edit By Weerasak 25-01-2566
	  public function updateServiceShippingPrice($base){
		$ship = new SOrdermrg();
		$ship->updateServiceShipping();
	  }
	//   END
 }
?>
