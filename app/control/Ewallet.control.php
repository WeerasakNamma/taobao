<?php
 class Ewallet extends Permission{
	
	
 	public function beforeroute($base){
 		$this->module_ids = '10';
		$this->set_permission('userlist',11);
		$this->set_view('view',NULL);
		 $this->set_edit('edit',NULL);
        $this->set_delete('delete',NULL);


		$this->PermissionAuth();

	}

 	public function userlist($base){

		$member = new Member();

		$memberList = $member->memberListByNew();
		$memberinfo = $member->memberInfomation();
		
		/*if($memberinfo['user_role_id']!=1){
            header('Location:'.$base->get('BASEURL'));
            exit();
        }*/
		
		//GF::print_r($memberinfo);

		$base->set('memberList',$memberList);

		Template::getInstance()->render('ewallet/userlist.htm');
	}

	public function withdrawal($base){

		$member = new Member();
		$memberInfomation = $member->memberInfomation();
		$wdList = $member->withdrawListAll();
//GF::print_r($memberinfo);

		$base->set('wdList',$wdList);

		Template::getInstance()->render('ewallet/wdlist.htm');
	}

	public function view($base){
		$member = new Member();
		$memberinfo = $member->memberInfomation();
		
		//if($memberinfo['user_role_id']!=1){
          //  header('Location:'.$base->get('BASEURL'));
           // exit();
        //}
		//GF::print_r($base->get('_userid'));
		$base->set('userid',$base->get('_userid'));
		
		$base->set('user_id',$base->get('_userid'));
		
		


		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();
		$walletLog = $member->walletLog();

		$base->set('memberinfomation',$memberInfomation);
		$base->set('walletLog',$walletLog);

		$base->set('tab','view');

		Template::getInstance()->render('ewallet/viewprofile.htm');
	}

	public function mywallet($base){
		$member = new Member();

		$base->set('user_id',$base->get('_userid'));


		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();
		$walletLog = $member->walletLog();

		$base->set('memberinfomation',$memberInfomation);
		$base->set('walletLog',$walletLog);

		$base->set('tab','view');

		Template::getInstance()->render('ewallet/mywallet.htm');
	}
	public function wdstatus($base){
		$member = new Member();
		$result = $member->withdrawStatus();
	}
    public function wdcomment($base){
		$member = new Member();
		$result = $member->withdrawComment();
    }
    public function checkbill($base){
		$tracking = new STracking();
        $result = $tracking->checkBillingData();
		
		//GF::print_r($result);
        $res = array("result"=>$result['result'],"values"=>$result['shipvalue'],"shipid"=>$result['shipid']);
        echo json_encode($res);
	}
   public function resetsearch($base){
 		unset($_SESSION['wallet_wd']);
	}
    public function resetSearchMember($base){
 		unset($_SESSION['member_search']);
	}


	public function processFrm($base){
		$member = new Member();
		$mode = $base->get('POST.mode');
		if($mode=='savewallet'){
			$result = $member->saveWallet();
			if($result){
				echo '<script>window.top.saveSccCallback();</script>';
			}else{
				echo '<script>window.top.saveSccCallback();</script>';
			}
		}
      else if($mode=='uploadtemp'){
         $order = new SOrder();

			$result = $order->uploadTemp();
			if($result){
            $wd_img = explode('/', $result);
            $base->set('wd_img',end($wd_img));
            $member->withdrawImage();
            //echo $result;
				echo '<script>window.top.sccRs();</script>';
			}else{
				echo "F";
			}
		}
      else if($mode=='searchwd'){
   			$_SESSION['wallet_wd']['user_code'] = $base->get('POST.user_code');
   			$_SESSION['wallet_wd']['status'] = $base->get('POST.status');
   			echo '<script>window.top.sccRs();</script>';
   	    }
        else if($mode=='searchmember'){
            $_SESSION['member_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['member_search']['user_name'] = trim($base->get('POST.user_name'));
            $_SESSION['member_search']['user_email'] = trim($base->get('POST.user_email'));
            $_SESSION['member_search']['user_role_id'] = trim($base->get('POST.user_role_id'));
			echo '<script>window.top.memberListCallback();</script>';
		}
	}

 }
?>
