<?php
 class Payment extends Permission{
 	
 	public function beforeroute($base){
 		$this->module_ids = '4';
		$this->set_permission('paymentlist','7');
		
		$this->set_view('view',NULL);
		$this->set_edit('create',NULL);
		$this->set_edit('edit',NULL);
		$this->set_delete('deleteexp',NULL);
		
		$this->PermissionAuth();
		
	}
 	
 	public function paymentlist($base){
 		
 		$order = new SOrder();
 		
 		$paymentList = $order->paymentList();
 		
 		$base->set('paymentList',$paymentList);
		Template::getInstance()->render('payment/list.htm');
	}


	
	
	public function processFrm($base){
		$order = new SOrder();
		$mode = $base->get('POST.mode');
		if($mode=='additem'){
			$result = $order->additem();
			if($result){
				echo '<script>window.top.setnewItemOrderMember();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='takeorder'){
			$result = $order->takeOrder();
			if($result){
				echo '<script>window.top.setnewItemOrderMember();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='updateslip'){
			$result = $order->updateSlip();
			if($result){
				echo '<script>window.top.resultSlip();</script>';
			}else{
				echo "F";
			}
		}
		
	}
	
 }
?>