<?php
//   ini_set('display_errors', 1);
//   ini_set('display_startup_errors', 1);
//   error_reporting(E_ALL);
class Dashboard {
	
	
	
	public function home($base){
        $dashboard = new SDashboard();
		$content = new SContent();

        $member = new Member();
        $report = new SReport(); 
 		$memberInfomation = $member->memberInfomation();
 		$role_owner = $memberInfomation['email_notification'];
        $role_agency= $memberInfomation['agency'];
        $role_admin= $memberInfomation['admin_group'];

        // GF::print_r($memberInfomation);
        
        //GF::print_r($dataAll);

        //exit();

        if($role_owner=='A'){
            $adminReport = $dashboard->adminCountReport();
            $agencyReport = $dashboard->agencyCountReport(); 
            $reportAllOrder = $report->reportAllOrder();

            $base->set('adminReport',$adminReport);
            $base->set('agencyReport',$agencyReport);
            $base->set('reportAllOrder',$reportAllOrder);
            Template::getInstance()->render('dashboard/owner.htm');
            exit();
        }

        if($role_admin=='A'){
            $adminReport = $dashboard->adminCountReport();
            $agencyReport = $dashboard->agencyCountReport();

            $base->set('adminReport',$adminReport);
            $base->set('agencyReport',$agencyReport);
            Template::getInstance()->render('dashboard/admin.htm');
            exit();
        }

        if($role_agency=='A'){
            //$adminReport = $dashboard->adminCountReport();
            $agencyReport = $dashboard->agencyCountReport();
            //GF::print_r($adminReport);
           // GF::print_r($agencyReport);

           // $base->set('adminReport',$adminReport);
            $base->set('agencyReport',$agencyReport);
            Template::getInstance()->render('dashboard/agency.htm');
            exit();
        }
		
		
		$base->set('rules','group');
		$contentGroup = $content->contentListByRules();
		
		$countInbox = $dashboard->countInbox();
		$countInboxFlag = $dashboard->countInboxFlag();
		
		$order = new SOrder();
		$orderList = $order->orderList();
		$base->set('orderList',$orderList);
		
		$tracking = new STracking();
		$trackingList = $tracking->trackingListByUser();
		$base->set('trackingList',$trackingList);
		
		$base->set('status','4');
		$trackingListOk = $tracking->trackingListByUserStatus();
		$base->set('trackingListOk',$trackingListOk);
        //GF::print_r($trackingListOk);
		
		//GF::print_r($orderList);
		
		$base->set('contentGroup',$contentGroup);
		$base->set('countInbox',$countInbox);
		$base->set('countInboxFlag',$countInboxFlag);
		
		///		GF::print_r($contentUser);
		Template::getInstance()->render('dashboard/home.htm');
	}
	public function profile($base){
		$member = new Member();
		$memberonfomation = $member->memberInfomation();
		$base->set('user_id',$base->get('_userid'));
		
		
		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();
		
		$base->set('memberinfomation',$memberInfomation);
		$base->set('positionList',$positionList);
		$base->set('tab','profile');
		
		Template::getInstance()->render('dashboard/viewprofile.htm');
	}
	public function inbox($base){
		
		$dashboard = new SDashboard();
		$content = new SContent();
		$base->set('rules','user');
		$contentUser = $content->contentListByRules();
		
		$base->set('contentUser',$contentUser);
		
		Template::getInstance()->render('dashboard/inbox.htm');
	}
	public function viewrate($base){
		
		$dashboard = new SDashboard();
		
		$rateList = $dashboard->getRate();
		
		//G		F::print_r($rateList);
		
		$base->set('rateList',$rateList);
		
		Template::getInstance()->render('dashboard/viewrate.htm');
	}
	public function detail($base){
		
		$dashboard = new SDashboard();
		$content = new SContent();
		
		$contetn_id = $base->get('GET.inbox');
		$contetn_id = urldecode($contetn_id);
		$contetn_id = base64_decode($contetn_id);
		$dashboard->setFlag();
		$base->set('_ids',$contetn_id);
		$contentInfo = $content->contentInfomation();
		
		
		
		$base->set('contentInfo',$contentInfo);
		
		Template::getInstance()->render('dashboard/detail.htm');
	}
	
}
?>
