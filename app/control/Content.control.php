<?php
 class Content extends Permission{

 	public function beforeroute($base){
 		$this->module_ids = '12';

		$this->set_permission('contentlist',NULL);



		$this->set_edit('createcontent',NULL);

		$this->set_delete('contentlist',NULL);

		$this->PermissionAuth();

	}


	/**
	*
	* @param Content Section
	*
	* @return
	*/
	public function contentlist($base){
		$content = new SContent();

		$contentList = $content->contentList();//GF::print_r($contentList);
 		//$categoryList = $content->categoryListAll();

 		//$base->set('categoryList',$categoryList);
 		$base->set('contentList',$contentList);
		Template::getInstance()->render('content/contentlist.htm');
	}
	public function createcontent($base){
		$member = new Member();
		$setting = new SSetting();
		$groupList = $setting->groupList();
		$memberList = $member->memberList();

		$base->set('contentUser',array());
 		$base->set('memberList',$memberList);
 		$base->set('groupList',$groupList);
		Template::getInstance()->render('content/createcontent.htm');
	}
	public function editcont($base){
		$content = new SContent();
		$setting = new SSetting();
		$member = new Member();

		$groupList = $setting->groupList();
		$contentInfo = $content->contentInfomation();
		$mapping = $content->contentMapping();
		$memberList = $member->memberList();
		$contentUser = $content->contentUser();
		//GF::print_r($contentUser);
 		$base->set('mapping',$mapping);
 		$base->set('contentInfo',$contentInfo);
 		$base->set('groupList',$groupList);
 		$base->set('memberList',$memberList);
 		$base->set('contentUser',$contentUser);

		Template::getInstance()->render('content/createcontent.htm');
	}
	public function updatecont($base){
		$content = new SContent();
		$content->updateStatusContent();
	}
	public function updatesortct($base){
		$content = new SContent();
		$content->updateSortContent();
	}

	/**
	*
	* @param Form Section
	*
	* @return
	*/

	public function processFrm($base){
		$content = new SContent();
		GF::print_r($base->get('POST'));
		$mode = $base->get('POST.mode');

		/**
		*
		* @var Content
		*
		*/

		if($mode=='createcontent'){
			$result = $content->createContent();
			if($result){
				echo '<script>window.top.successContetnCallBack();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='editcontent'){

			$result = $content->updateContent();
			if($result){
				echo '<script>window.top.successContetnCallBack();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='clearcontent'){
			unset($_SESSION['content_content']);
			echo '<script>window.top.successContetnCallBack();</script>';
		}
		else if($mode=='searchcontent'){

				$content_name = $base->get('POST.content_name');
				$status = $base->get('POST.status');
				$category_id = $base->get('POST.category_id');

				$_SESSION['content_content']['content_name'] = $content_name;
				$_SESSION['content_content']['status'] = $status;
				$_SESSION['content_content']['category_id'] = $category_id;


			echo '<script>window.top.successContetnCallBack();</script>';
		}

	}

 }
?>
