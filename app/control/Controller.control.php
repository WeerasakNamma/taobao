<?php
 class Controller {
 	
 	public function beforeroute($base){
         
		$auth = new Authen();
        $validate = $auth->validateLogin();
        // if($_SESSION['dev']==''){
        //     $validate = false;
        // }
		if(!$validate){
            if($base->get('GET.d')=='dev'){
                $_SESSION['dev'] = 'dev';
            }
            $setting = new SSetting();
            $base->set('genid','6');
            $ship_content = $setting->getGeneralSettingByuser();

            $base->set('ship_content',$ship_content);
			Template::getInstance()->render('login/login.htm');
			exit();
		}else{
            $member = new Member();
            $m_inf = $member->memberInfomation();
            $base->set('memberinfo',$m_inf);
           
		}
		
	}
 }
?>