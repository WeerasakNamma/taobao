<?php
 class Shipreq extends Permission{
 	
 	public function beforeroute($base){
 		$this->module_ids = '13';
		$this->set_permission('reqlist',NULL);

		
		$this->set_view('view',NULL);
		$this->set_edit('create',NULL);
		$this->set_edit('edit',NULL);
		$this->set_delete('deleteexp',NULL);
		
		$this->PermissionAuth();
		
	}
 


	public function reqlist($base){
 		
 		$shipping = new SShipping();
 		
 		$reqList = $shipping->registerList();
 		
 		$base->set('reqList',$reqList);
		Template::getInstance()->render('shipreq/reqlist.htm');
	}
	public function updateregister($base){
 		$shipping = new SShipping();
 		
 		$shipping->updateRegister();	
	}

	
 }
?>