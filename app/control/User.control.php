<?php
 class User extends Permission{
 	
 	public function beforeroute($base){
 		$this->module_ids = '5';
		$this->set_permission('userlist','2');
		$this->set_view('view','2');
		$this->set_edit('create','2');
		$this->set_edit('edit','2');
		$this->set_delete('delete','2');
		
		$this->PermissionAuth();
		
	}
 	
 	public function userlist($base){
 		
		$member = new Member();
        $setting = new SSetting();
		
		$memberList = $member->memberListByNew();
        $grouplist = $setting->groupList();

        //GF::print_r($memberList);
       // GF::print_r($grouplist);
		
		$base->set('memberList',$memberList);
        $base->set('grouplist',$grouplist);
		
		Template::getInstance()->render('user/userlist.htm');
	}
	public function create($base){
 		$member = new Member();
		
		$grouplist = $setting->groupList();
		
		
		$base->set('grouplist',$grouplist);
		Template::getInstance()->render('user/create.htm');
	}
	public function edit($base){
		$member = new Member();
        $register = new SRegister();
        $setting = new SSetting();
		
		$base->set('user_id',$base->get('_userid'));
		
		
		//$positionList = $member->positionList();
        $grouplist = $setting->groupList();
		$memberInfomation = $member->memberInfomationByID();
		$rateList = $member->rateUserList();

        $province = $register->provinceList();
        $base->set('_set_id',$memberInfomation['province']);
        $amphur = $register->amphurList();
        $base->set('_set_id',$memberInfomation['amphur']);
		$district = $register->districtList();
		
		
		$base->set('rateList',$rateList);
		$base->set('memberinfomation',$memberInfomation);
		//$base->set('positionList',$positionList);
        $base->set('province',$province);
        $base->set('amphur',$amphur);
        $base->set('district',$district);
		$base->set('grouplist',$grouplist);
		Template::getInstance()->render('user/create.htm');
	}
    public function amphur($base){
        //echo 'ddd';exit();
        $register = new SRegister();
        $amphur = $register->amphurList();

        $resultHTML = '<label class="col-sm-2 control-label">อำเภอ</label>';
        $resultHTML .= '<div class="col-sm-5">';
        $resultHTML .= '<select class="form-control" onchange="changeAmphur(this);"  id="amphur-register" name="amphur-register">';
        $resultHTML .= '<option value="">เลือกอำเภอ</option>';
        if(count($amphur)>0){
            foreach($amphur as $alist){
                $resultHTML .= '<option value="'.$alist['AMPHUR_ID'].'">'.$alist['AMPHUR_NAME'].'</option>';
            }
        }
        $resultHTML .= '</select>';
        $resultHTML .= '</div>';

        echo $resultHTML;

	}
   public function district($base){
        $register = new SRegister();
        $district = $register->districtList();

        $resultHTML = '<label class="col-sm-2 control-label">ตำบล</label>';
        $resultHTML .= '<div class="col-sm-5">';
        $resultHTML .= '<select class="form-control"  id="district-register" name="district-register">';
        $resultHTML .= '<option value="">เลือกตำบล</option>';
        if(count($district)>0){
            foreach($district as $dlist){
                $resultHTML .= '<option value="'.$dlist['DISTRICT_ID'].'">'.$dlist['DISTRICT_NAME'].'</option>';
            }
        }
        $resultHTML .= '</select>';
        $resultHTML .= '</div>';

        echo $resultHTML;

	}
	public function profile($base){
		$member = new Member();
		$memberonfomation = $member->memberInfomation();
		$base->set('user_id',$memberonfomation['id']);
		
		
		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();
		
		$base->set('memberinfomation',$memberInfomation);
		$base->set('positionList',$positionList);
		$base->set('tab','profile');
		
		Template::getInstance()->render('user/viewprofile.htm');
	}
	public function editprofile($base){
		$member = new Member();
        $register = new SRegister();
		$memberonfomation = $member->memberInfomation();
		$base->set('user_id',$memberonfomation['id']);
		
		
		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();

        $province = $register->provinceList();
        $base->set('_set_id',$memberInfomation['province']);
        $amphur = $register->amphurList();
        $base->set('_set_id',$memberInfomation['amphur']);
		$district = $register->districtList();
		
		$base->set('memberinfomation',$memberInfomation);
		$base->set('positionList',$positionList);
        $base->set('province',$province);
        $base->set('amphur',$amphur);
        $base->set('district',$district);
		
		Template::getInstance()->render('user/profile.htm');
	}
	public function view($base){
		$member = new Member();
		
		$base->set('user_id',$base->get('_userid'));
		
		
		$positionList = $member->positionList();
		$memberInfomation = $member->memberInfomationByID();
		
		$base->set('memberinfomation',$memberInfomation);
		$base->set('positionList',$positionList);
		
		$base->set('tab','view');
		
		Template::getInstance()->render('user/viewprofile.htm');
	}
	public function delete($base){
		$member = new Member();
		$member->updateUserActiveStatus();
	}
	public function checkusername($base){
		$member = new Member();
		echo $member->checkUsername();
		//echo $base->get('GET.name');
	}
	public function delrate($base){
		$member = new Member();
		$member->deleteRate();
	}
    public function resetSearchMember($base){
 		unset($_SESSION['member_search']);
	}
	
	
	public function processFrm($base){
		$member = new Member();
		$mode = $base->get('POST.mode');
		if($mode=='create'){
			$result = $member->createMember();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='edit'){
			$result = $member->updateMember();
			if($result){
				echo '<script>window.top.successCallBack();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='profile'){
			$result = $member->updateMember();
			if($result){
				// echo '<script>window.top.location.reload();</script>';
				echo '<script>window.top.location.hash="/user/editprofile/?'.time().'";</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='addrate'){
			$result = $member->addRate();
			if($result){
				// echo '<script>window.top.location.reload();</script>';
				echo '<script>window.top.saveRateSccCallback();</script>';
			}else{
				echo "F";
			}
		}
        else if($mode=='searchmember'){
            $_SESSION['member_search']['user_code'] = trim($base->get('POST.user_code'));
			$_SESSION['member_search']['user_name'] = trim($base->get('POST.user_name'));
            $_SESSION['member_search']['user_email'] = trim($base->get('POST.user_email'));
            $_SESSION['member_search']['user_birthdate_start'] = trim($base->get('POST.user_birthdate_start'));
            $_SESSION['member_search']['user_birthdate_end'] = trim($base->get('POST.user_birthdate_end'));
            $_SESSION['member_search']['user_role_id'] = trim($base->get('POST.user_role_id'));
			echo '<script>window.top.memberListCallback();</script>';
		}
	}
	
 }
?>