<?php
 class Shipping extends Permission{

 	public function beforeroute($base){
 		$this->module_ids = '2';
		$this->set_permission('order','1');

		$this->set_view('view',NULL);
		$this->set_edit('create',NULL);
		$this->set_edit('edit',NULL);
		$this->set_delete('deleteexp',NULL);

		$this->PermissionAuth();

	}
 	public function register($base){
 		$setting = new SSetting();
 		$shipping = new SShipping();

 		//GF::print_r($setting->getGeneralSettingByuser());

 		$base->set('regis',$shipping->checkRegister());
      	$base->set('genid','1');
      	$base->set('content',$setting->getGeneralSettingByuser());
         $base->set('genid','3');
      	$base->set('content3',$setting->getGeneralSettingByuser());
		Template::getInstance()->render('shipping/register.htm');
	}
	public function address($base){
      $setting = new SSetting();
      $base->set('genid','2');
      $base->set('content',$setting->getGeneralSettingByuser());
		Template::getInstance()->render('shipping/address.htm');
	}
 	public function order($base){
 		
 		if(isset($_SESSION['tra_search'])){ unset($_SESSION['tra_search']); }
 		


		Template::getInstance()->render('shipping/order.htm');
	}

	public function orderlist($base){

		$order = new SShipping();

		$orderList = $order->orderListImp();

		//GF::print_r($orderList);

 		$base->set('orderList',$orderList);
		Template::getInstance()->render('shipping/orderlist.htm');
	}

	public function view($base){

		$order = new SOrder();

		$itemList = $order->itemList();
		$orderInfo = $order->orderInfo();

 		$base->set('itemList',$itemList);
 		$base->set('orderInfo',$orderInfo);
		Template::getInstance()->render('shipping/view.htm');
	}
	public function confirmpayment($base){

		$order = new SOrder();

		$orderInfo = $order->orderInfo();
		$bank = $order->getBank();

		$base->set('bank',$bank);
 		$base->set('orderInfo',$orderInfo);
		Template::getInstance()->render('shipping/confirmpay.htm');
	}

	public function takeorder($base){
 		$order = new SShipping();
      $tracking = new STracking();
 		$result = $tracking->updateOrderTracking();
 		if($result){
			echo 'A';
		}
	}
   public function removeitemimp($base){
 		$order = new SShipping();
 		$order->removeItemImp();
	}
   public function checktrackingno($base){
 		$tracking = new STracking();
 		$trackNo = $tracking->checkTrackingNo();
        echo $trackNo;
	}




	public function processFrm($base){
		$order = new SShipping();
		$order2 = new SOrder();

		$mode = $base->get('POST.mode');
		if($mode=='additem'){
			$result = $order->additem_import();
			if($result){
				echo '<script>window.top.setnewItemImport();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='updateslip'){
			$result = $order2->updateSlip();
			if($result){
				echo '<script>window.top.resultSlip();</script>';
			}else{
				echo "F";
			}
		}
		else if($mode=='addregister'){
			$result = $order->addRegister();
			if($result){
				echo '<script>window.top.regisResult();</script>';
			}else{
				echo '<script>window.top.alert("ไฟล์ไม่ถูกต้องกรุณาลองใหม่");window.top.preload("hide");</script>';
			}
		}

	}

 }
?>
