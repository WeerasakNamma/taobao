<?php
	class Account {
		
		public function payment($base){
			$account = new SAccount();
			$project = new SProject();
			$paymentList = $account->paymentList();
		
			$base->set('payList',$paymentList);
			$project->checkRoundForupdate($paymentList);
			
			Template::getInstance()->render('account/paymentlist.htm');
		}
		public function processFrm($base){
			$report = new SReport();
			$mode = $base->get('POST.mode');
			if($mode=='searchworking'){
				$result = $report->searchWorkingList();
				if($result){
					echo '<script>window.top.successWorkingCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='searchleave'){
				$result = $report->seacrhLeave();
				if($result){
					echo '<script>window.top.successLeaveCallBack();</script>';
				}else{
					echo "F";
				}
			}
			else if($mode=='searchpayment'){
				
				$start = $base->get('POST.start_date');
		 		$end = $base->get('POST.end_date');
		 		
		 		$payment_type = $base->get('POST.payment_type');
		 		$round_status = $base->get('POST.round_status');
		 		
		 		$project_name = $base->get('POST.project_name');
		 		
				$_SESSION['payment']['start_date'] = $start;
				$_SESSION['payment']['end_date'] = $end;
				$_SESSION['payment']['payment_type'] = $payment_type;
				$_SESSION['payment']['round_status'] = $round_status;
				$_SESSION['payment']['project_name'] = $project_name;
				
				echo '<script>window.top.successPaymentCallBack();</script>';
			}
			else if($mode=='clearpayment'){
				unset($_SESSION['payment']);
				session_destroy($_SESSION['payment']);
				echo '<script>window.top.location.hash = "/account/payment/?'.time().'";</script>';
			}
			
			
		}
		
	}
?>