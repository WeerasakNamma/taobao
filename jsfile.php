<?php
    $JSFILE = array();
 
    $JS['dashboard']['default'] = array(
    							"/assets/js/web/dashboard/dashboard.js"
    							);

    $JS['user']['userlist'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/user/userlist.js"
    							);
    $JS['user']['create'] = array(

    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.extensions.js",
    							"/assets/template/adminlte/plugins/daterangepicker/daterangepicker.js",
    							"/assets/js/web/user/create.js"
    							);
    $JS['user']['profile'] = array(

    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.extensions.js",
    							"/assets/template/adminlte/plugins/daterangepicker/daterangepicker.js",
    							"/assets/js/web/user/create.js"
    							);
    $JS['user']['editprofile'] = array(

    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.extensions.js",
    							"/assets/template/adminlte/plugins/daterangepicker/daterangepicker.js",
    							"/assets/js/web/user/create.js"
    							);
    $JS['user']['edit'] = array(

    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.extensions.js",
    							"/assets/js/web/user/create.js"
    							);
    $JS['user']['view'] = array(

    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js",
    							"/assets/template/adminlte/plugins/input-mask/jquery.inputmask.extensions.js",
    							"/assets/js/web/user/create.js",
    							"/assets/js/web/user/view.js"
    							);

    $JS['buyship']['order'] = array(

    							"/assets/js/web/api/script-tmall.js",
    							"/assets/js/web/api/script-taobao.js",
    							"/assets/js/web/api/script-1688.js",
    							"/assets/js/web/api/script.js",
    							"/assets/js/web/api/member.js",
    							"/assets/js/web/buyship/odr.js"
    							);
    $JS['buyship']['checkout'] = array(

    							"/assets/js/web/api/script-tmall.js",
    							"/assets/js/web/api/script-taobao.js",
    							"/assets/js/web/api/script-1688.js",
    							"/assets/js/web/api/script.js",
    							"/assets/js/web/api/member.js"
    							);
    $JS['buyship']['view'] = array(

    							"/assets/js/web/api/script-tmall.js",
    							"/assets/js/web/api/script-taobao.js",
    							"/assets/js/web/api/script-1688.js",
    							"/assets/js/web/api/member.js",
                        "/assets/js/web/buyship/refund.js"
    							);
   $JS['buyship']['refundlist'] = array(

   							"/assets/js/web/buyship/refund.js"
   							);
    $JS['shipping']['order'] = array(


    							"/assets/js/web/api/script.js",
    							"/assets/js/web/api/member.js"
    							);
    $JS['buyship']['confirmpayment'] = array(

    							"/assets/js/web/buyship/payment.js"
    							);

    $JS['buyship']['orderlist'] = array(

    							"/assets/js/web/api/script-tmall.js",
    							"/assets/js/web/api/script-taobao.js",
    							"/assets/js/web/api/script-1688.js",
    							"/assets/js/web/api/script.js",
    							"/assets/js/web/api/member.js",
    							"/assets/js/web/buyship/odrlist.js"
    							);
    $JS['shipping']['view'] = array(

    							"/assets/js/web/api/script-tmall.js",
    							"/assets/js/web/api/script-taobao.js",
    							"/assets/js/web/api/script-1688.js",
    							"/assets/js/web/api/script.js",
    							"/assets/js/web/api/member.js"
    							);
    $JS['shipping']['confirmpayment'] = array(

    							"/assets/js/web/buyship/payment.js"
    							);
    $JS['shipping']['register'] = array(

    							"/assets/js/web/shipping/shipping.js"
    							);
    $JS['ordermanagement']['orderlist'] = array(

    							"/assets/js/web/order/view.js",
    							"/assets/js/web/order/odrlist.js"
    							);
    $JS['ordermanagement']['view'] = array(

    							"/assets/js/web/order/view.js"
    							);

    $JS['ordermanagement']['paymentlist'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/payment/payment.js"
    							);
    $JS['ordermanagement']['tracking'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/order/tracking.js"
    							);
    $JS['ordermanagement']['trackingok'] = array(
    							"/assets/js/web/order/trackingok.js"
    							);
    $JS['ordermanagement']['viewtracking'] = array(
    							"/assets/js/web/order/viewtracking.js"
    							);
   $JS['ordermanagement']['createtracking'] = array(
   							"/assets/js/web/order/viewtracking.js"
   							);
   $JS['ordermanagement']['trackinghold'] = array(
   							"/assets/js/web/order/trackinghold.js"
   							);
   $JS['ordermanagement']['trackingforship'] = array(
   							"/assets/js/web/order/trackingforship.js"
   							);
   $JS['ordermanagement']['getship'] = array(
   							"/assets/js/web/order/trackingforship.js"
   							);
    $JS['ordermanagement']['shippinglist'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/order/tracking.js"
    							);
    $JS['ordermanagement']['thshippinglist'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/order/thtracking.js"
    							);
    $JS['ordermanagement']['editshipping'] = array(
    							"/assets/js/web/order/editshipping.js"
    							);
    $JS['ordermanagement']['orderbuy'] = array(
    							"/assets/js/web/order/orderbuy.js"
    							);
   $JS['ordermanagement']['refundlist'] = array(
                       "/assets/js/web/order/refund.js"
                       );
  $JS['ordermanagement']['chinaorder'] = array(

                     "/assets/js/web/order/chinaorder.js"
                     );
    $JS['ordermanagement']['chinaorderuser'] = array(
    
                            "/assets/js/web/order/chinaorder.js"
                            );
    $JS['ordermanagement']['ordernotrackinglist'] = array(
    
                            "/assets/js/web/order/notracking.js"
                            );
     $JS['ordermanagement']['ordernotracking_send'] = array(
    
                            "/assets/js/web/order/notracking.js"
                            );
   $JS['ordervendor']['orderlist'] = array(

                      "/assets/js/web/ordervendor/orderlist.js"
                      );
   $JS['ordervendor']['view'] = array(

                       "/assets/js/web/ordervendor/view.js"
                       );
    $JS['tracking']['trackinglist'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/tracking/tracking.js"
    							);
    $JS['tracking']['trackingforship'] = array(
    							"/assets/js/web/tracking/trackingok.js"
    							);
	$JS['tracking']['getship'] = array(
    							"/assets/js/web/tracking/trackingok.js"
    							);
   $JS['tracking']['view'] = array(
   							"/assets/js/web/tracking/viewtracking.js"
   							);
    $JS['tracking']['shippinglist'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/tracking/tracking.js"
    							);
    $JS['payment']['paymentlist'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/payment/payment.js"
    							);
    $JS['tracking']['confirmpayment'] = array(

    							"/assets/js/web/buyship/payment.js"
    							);
    $JS['ewalletaccount']['default'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/ewalletaccount/userlist.js"
    							);
    $JS['ewalletaccount']['userlist'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/ewalletaccount/userlist.js"
    							);
    $JS['ewalletaccount']['view'] = array(
    							"/assets/js/web/ewalletaccount/editwallet.js"
    							);
    $JS['ewalletaccount']['withdrawal'] = array(
    							"/assets/js/web/ewalletaccount/withdraw.js"
    							); 
    $JS['ewallet']['default'] = array(
      "/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
      "/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
   							"/assets/js/web/ewallet/ewallet.js",
							"/assets/js/web/ewallet/withdraw.js"
   							);
    $JS['ewallet']['withdraw'] = array(
      "/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
      "/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/ewallet/withdraw.js"
    							);
    $JS['ewallet']['paymentlist'] = array(
      "/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
      "/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
   							"/assets/js/web/ewallet/paymentlist.js"
   							);
    $JS['ewallet']['confirmpayment'] = array(
      "/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
      "/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
   							"/assets/js/web/buyship/payment.js"
   							);
    $JS['shipreq']['default'] = array(
    							"/assets/js/web/shipreq/shipreq.js"
    							);

    $JS['content']['default'] = array(
    							"/assets/template/adminlte/plugins/select2/js/select2.full.min.js",
    							"/assets/js/web/content/contentlist.js"
    							);
    $JS['content']['createcontent'] = array(
    							"/assets/template/adminlte/plugins/select2/js/select2.full.min.js",
    							"/assets/template/adminlte/plugins/iCheck/icheck.min.js",
    							"/assets/js/web/content/createcontent.js"
    							);
    $JS['content']['editcont'] = array(
    							"/assets/template/adminlte/plugins/select2/js/select2.full.min.js",
    							"/assets/template/adminlte/plugins/iCheck/icheck.min.js",
    							"/assets/js/web/content/createcontent.js"
    							);
	$JS['content']['contentlist'] = array(
    							"/assets/template/adminlte/plugins/select2/js/select2.full.min.js",
    							"/assets/js/web/content/contentlist.js"
    							);


   	$JS['setting']['permission'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/permission.js"
                                );
    $JS['setting']['banklist'] = array(
        
                                        "/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
                                        "/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
                                        "/assets/js/web/setting/banklist.js"
                                        );
    $JS['setting']['createbank'] = array(
                                        "/assets/js/web/setting/createbank.js"
                                        );
    $JS['setting']['editbank'] = array(
                                        "/assets/js/web/setting/createbank.js"
                                        );
    $JS['setting']['edituser'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/template/adminlte/plugins/iCheck/icheck.min.js",
    							"/assets/js/web/setting/groupuserlist.js"
    							);
    $JS['setting']['creategroup'] = array(

    							"/assets/template/adminlte/plugins/iCheck/icheck.min.js",
    							"/assets/js/web/setting/creategroup.js"
    							);
    $JS['setting']['editgroup'] = array(

    							"/assets/template/adminlte/plugins/iCheck/icheck.min.js",
    							"/assets/js/web/setting/creategroup.js"
    							);
    $JS['setting']['position'] = array(

    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/position.js"
    							);
   	$JS['setting']['createposition'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/position.js"
    							);
    $JS['setting']['editposition'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/position.js"
    							);
    $JS['setting']['masterdata'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/masterdata.js"
    							);
    $JS['setting']['createmastergroup'] = array(
    							"/assets/js/web/setting/masterdata.js"
    							);
    $JS['setting']['sprinttemplate'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/sprinttemplate.js"
    							);

    $JS['setting']['createsprinttemplate'] = array(
    							"/assets/js/web/setting/sprinttemplate.js"
    							);
	$JS['setting']['holiday'] = array(
								"/assets/js/web/setting/holidaysetting.js"
    							);
	$JS['setting']['createholiday'] = array(
    							"/assets/js/web/setting/holidaysetting.js"
    							);
	$JS['setting']['editholiday'] = array(
    							"/assets/js/web/setting/holidaysetting.js"
    							);
     $JS['setting']['editsprinttemplate'] = array(
    							"/assets/js/web/setting/sprinttemplate.js"
    							);
    $JS['setting']['delsprinttemplate'] = array(
    							"/assets/js/web/setting/sprinttemplate.js"
    							);
    $JS['setting']['editmastergroup'] = array(
    							"/assets/js/web/setting/masterdata.js"
    							);
   	$JS['setting']['createmasterdata'] = array(
    							"/assets/js/web/setting/masterdata.js"
    							);
    $JS['setting']['editmasterdata'] = array(
    							"/assets/js/web/setting/masterdata.js"
    							);
    $JS['setting']['rateexchange'] = array(
    							"/assets/js/web/setting/rateexchange.js"
    							);
   $JS['setting']['general'] = array(
                       "/assets/js/web/setting/general.js"
                       );
    $JS['setting']['emailtemplate'] = array(
                        "/assets/js/web/setting/emailtemplate.js"
                        );
    $JS['setting']['masterlist'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/masterdata.js"
    							);


    $JS['account']['payment'] = array(
    							"/assets/js/web/account/payment.js"
    							);
    $JS['setting']['createecoupon'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/ecoupon.js"
    							);
	$JS['setting']['editecoupon'] = array(
								"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",

    							"/assets/js/web/setting/ecoupon.js"
    							);
    $JS['setting']['ecoupon'] = array(
    							"/assets/template/adminlte/plugins/datatables/jquery.dataTables.min.js",
    							"/assets/template/adminlte/plugins/datatables/dataTables.bootstrap.min.js",
    							"/assets/js/web/setting/ecoupon.js"
    							);
    $JS['setting']['createtemplate'] = array(
    							"/assets/js/web/setting/template.js"
    							);
    $JS['setting']['edittemplate'] = array(
    							"/assets/js/web/setting/template.js"
    							);
	$JS['setting']['ftpmrg'] = array(
    							"/assets/js/web/setting/template.js"
    							);
    $JS['setting']['syncdata'] = array(
    							"/assets/js/web/setting/sync.js"
                                );
    $JS['setting']['manuallist'] = array(
                                "/assets/js/web/setting/manual.js"
                                );
    $JS['setting']['editmanual'] = array(
                                "/assets/js/web/setting/manual.js"
                                );
    $JS['setting']['createmanual'] = array(
                                "/assets/js/web/setting/manual.js"
                                );
    $JS['report']['default'] = array(
                        "https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js",
                        "/assets/template/adminlte/plugins/morris/morris.min.js",
    							"/assets/js/web/report/report.js"
    							);
   $JS['report']['paidrp'] = array(

                        "/assets/template/adminlte/plugins/morris/morris.min.js",
    							"/assets/js/web/report/reportpaid.js"
                                );
    $JS['report']['sendcn'] = array(
        
                                 "/assets/js/web/report/sendcn.js"
                                        );
    $JS['report']['buyrp'] = array(
        
                                    "/assets/js/web/report/buyrp.js"
                                        );
    $JS['report']['shippingrp'] = array(
        
                                    "/assets/js/web/report/shippingrp.js"
                                        );     
    $JS['report']['refundrp'] = array(
        
                                    "/assets/js/web/report/refundrp.js"
                                        );      
    $JS['report']['withdrawrp'] = array(
        
                                    "/assets/js/web/report/withdrawrp.js"
                                        );   
    $JS['report']['walletrp'] = array(
        
                                    "/assets/js/web/report/walletrp.js"
                                        );
    $JS['report']['raterp'] = array(
        
                                    "/assets/js/web/report/raterp.js"
                                        ); 
     $JS['report']['waitshipping'] = array(
        
                                    "/assets/js/web/report/waitshipping.js"
                                        );
	$JS['report']['listkiloq'] = array(
										"/assets/js/web/report/kiloq.js"
										);

?>
