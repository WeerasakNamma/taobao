function saveWithdraw() {

    if ($('#wd_number').val() == '') {
        noticeMessage('กรุณาระบุ เลขบัญชีธนาคาร', 'scriptFocus("#wd_number");');
        return;
    }
    if ($('#wd_name').val() == '') {
        noticeMessage('กรุณาระบุ ชื่อบัญชี', 'scriptFocus("#wd_name");');
        return;
    }
    if ($('#wd_total').val() == '') {
        noticeMessage('กรุณาระบุ จำนวนเงิน', 'scriptFocus("#wd_total");');
        return;
    }

    
   // alert(parseInt($('#wd_total').val()));

    //var wd_total = $('#wd_total').val();

    /*if ($('#total_wddss').val()< $('#wd_total').val()) {
        alert(parseFloat($('#wd_total').val()).toFixed(2)+' =====1 '+parseFloat($('#total_wddss').val()).toFixed(2));
        noticeMessage('กรุณาระบุ ยอดถอนเงินไม่ถูกต้อง', 'scriptFocus("#wd_total");');
        return;
    }*/

    if($('#wd_total').val().indexOf('.') !== -1){
    	noticeMessage('กรุณาระบุ ยอดถอนเงินเป็นจำนวนเต็ม', 'scriptFocus("#wd_total");');
        return;
    }

    if(parseFloat($('#wd_total').val()).toFixed(2) === parseFloat($('#total_wddss').val()).toFixed(2)){

        alertMessage('กรุณายืนยันการถอนเงิน.', 'saveWithdrawCallback');
    }else if(parseInt($('#wd_total').val()) <= parseInt($('#total_wddss').val())){
        alertMessage('กรุณายืนยันการถอนเงิน.', 'saveWithdrawCallback');
    }else{

        noticeMessage('กรุณาระบุ ยอดถอนเงินไม่ถูกต้อง', 'scriptFocus("#wd_total");');
        return;
    }


    alertMessage('กรุณายืนยันการถอนเงิน.', 'saveWithdrawCallback');
}

function saveWithdrawCallback() {
    preload('show');
    var f = document.getElementById('wdFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ewallet/processFrm",
        f.submit();
}

function saveSccCallback() {
    //window.location.hash = '/ewallet/withdraw/?' + new Date().getTime();
    window.location.reload();
}


function viewWd(elem) {
    var imgss = $(elem).attr('data-img');
    $('#wdimg').attr('src', imgss);
    $('#wdModal').modal('show');
}

// Edit By Weerasak 06-01-2566
function withdrawRequest(elems){
    if($(elems).attr('data-bank') == '')
    {
        noticeMessage('ไม่พบบัญชีธนาคารโปรดตรวจสอบที่หน้า profile.', 'scriptFocus("");');
        return;
    }
   
    $('#withdrawModal').modal('show');
    var orderId = $(elems).attr('data-id');
    var price = $(elems).attr('data-price');

    var elemOrderId = document.getElementById("order_id");
    var elemAmount = document.getElementById("refund_amount");
    elemOrderId.value = orderId;
    elemAmount.value = price;

    $('#order_id').value = orderId;
    $('#ref_amount').text(price);

    //alertMessage('กรุณายืนยันการถอนเงิน.', 'saveWithdrawRequest');
}
function saveWithdrawRequest(){
    var f = document.getElementById('wdFrm');
// console.log(f);    
    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ewallet/processFrm",
    f.submit(); 
}

//แจ้งชำระเงิน
function ConfirmPayment(elems){    
    var orderId = $(elems).attr('data-id');
    var price = $(elems).attr('data-price');

     var elemOrderId = document.getElementById("order_id_confirmpayment");
     var elemAmount = document.getElementById("pay_amount");
     elemAmount.value = price;
     elemOrderId.value = orderId;
     console.log(elemAmount);
     $('#confirmPaymentModal').modal('show');
}
// END
$(function() {

    /*$("#example1").DataTable();*/
    $('#example-withdraw').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false
    });
});
//Edit By Weerasak 15-01-2566
//แจ้งชำระเงิน
function paymentUpdate(){

	var upload=document.getElementById("payment_slip");
//alert(upload.value);
	if($('#payment_date').val()==''){
		alert('กรุณากรอกวันที่');
		return;
	}
	if($('#payment_time').val()==''){
		alert('กรุณากรอกเวลา');
		return;
	}
	if($('#pay_amount').val()==''){
		alert('กรุณากรอกจำนวนเงิน');
		return;
	}
	if(upload.value==''){
		alert('กรุณาแนบไฟล์รูปภาพ');
		return;
	}

	// if(!confirm('คุณต้องการบันทึกรายการนี้หรือไม่?')){
	// 	return;
	// }

	//$('#preLoadSearch').show();

    var f = document.getElementById('slipFrm');
    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL+"/buyship/processFrm/",
    f.submit();
    window.location.reload();
    
}

$(document).ready(function(){
	//alert(dashdata);
	$('.datepickerX').datepicker({
      autoclose: true,
      dateFormat: 'dd-mm-yyyy'
    });


    $(".timepicker").timepicker({
      showInputs: false,
      showMeridian : false,
      minuteStep : 1
    });

});
// END