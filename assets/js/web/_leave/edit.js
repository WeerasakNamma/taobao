$(function () {
      $(".leave_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd/mm/yyyy' 
      });
      
      $('body').on('click', '#saveLeave', function(){ 
			alertMessage('Please confirm to Create Leave.','saveLeaveCallback');
	  });
	  $('body').on('click', '#clonedate', function(){ 
			$('#clone_c').clone().appendTo('#leave_o');
			$('.leave_date').each(function(){
			    $(this).datepicker({
			    	todayBtn: "linked",
			       language: "th",
			       autoclose: true,
			       todayHighlight: true,
			       format: 'dd/mm/yyyy' 
			    });
			});
			idx = 1;
			$('#leave_o .form-group').each(function(){
			    $('.control-label',this).text("Leave Date "+idx);
			    idx++;
			});
	  });
      
});

function saveLeaveCallback(){
	preload('show');
	var f = document.getElementById('createFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/leave/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/leave/'
}

