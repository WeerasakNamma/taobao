
var dataid = '0';

$(function () {
		
		  $(".select2").select2();
	      $(".my_date").datepicker({
		      	todayBtn: "linked",
		       language: "th",
		       autoclose: true,
		       todayHighlight: true,
		       format: 'dd-mm-yyyy'
	      }).on('changeDate', function(ev) {
	      	/*console.log(ev)*/
	      	var date_c = ev.target.attributes[3].nodeValue;
	      	var star_date= $('#start_date').val();
	      	if(date_c=="end_date"&&star_date!=''){
				
			 	var end_dat= $('#'+date_c).val();
			 		 $.ajax({
	                type: "POST",
	                data:	{
	                		star_date:star_date,
	                		end_dat:end_dat
	                		},
	                dataType: "json",
	                url: BASEURL + "/project/checkDateSprintList/",
	                success: function(resp) {
	                	
		                       if (resp.result == "success") {
		                     
		                            $('#sprint_elapsed').val(resp.elapeseday);
		                            $('#sprint_holiday').val(resp.holiday);
		                            $('#sprint_working').val(resp.workingday);
		                            $('#sprint_number_dev').attr("readonly", false);
		                            $('#sprint_utilization').attr("readonly", false);
		                            
		                        }else{
									$('#start_date').val(null);
									$('#end_date').val(null);
									$('#start_date').focus();
								}
	                        }
	                      });  
			}// end if to chaeck end date
			else{
				
			}
	      	
		 	
		});
	$(document).ready(function() {
	    $("#sprint_utilization").keydown(function (e) {
			setTimeout(function(){
				if  ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
					(e.keyCode >= 35 && e.keyCode <= 40)) {
						
							
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		        	e.preventDefault();
		        }else{
					var workingday =  $('#sprint_working').val();
		        	var sprint_number_dev =  $('#sprint_number_dev').val();
		        	var sprint_utilization =  $('#sprint_utilization').val();
		        	var sprint_avalible_how = parseFloat(workingday)*parseFloat(sprint_number_dev)*parseFloat(sprint_utilization)*8;
		        	//alert(sprint_utilization);
		        	var sprint_daily_how = parseFloat(sprint_utilization)/parseFloat(workingday);
		        	$('#sprint_avalible_how').val(sprint_avalible_how);
		        	$('#sprint_daily_how').val(sprint_daily_how);
		        	
		        	
				}
			},600);
	        
	       
	    });
	});
});

function changSprintTemplate(elem){
	var thisvalue = $(elem).val();

	ajaxRequestProcess('/project/sprintlistbacklog/?thisvalue='+thisvalue,changSprintTemplateCallback)
}

function changSprintTemplateCallback(resp){
	$('#html_backlog').html(resp);
    $(".select2").select2();
    preload('hide');
}
function removet(selects){
	var mybuttn = $(selects).parents();
	$(mybuttn[1]).remove();
}
function clonefn (){
	//$('.select2').select2('disable');
    $("#text_clone","#stm_clone_b").clone().appendTo('#stm_clone_a');
    $(".select3").select2();
    //var mybuttns = $('#stm_clone_a','#html_backlog').parent();
    //alert(mybuttns);s
   /* console.log($('.select2check').length);*/
	//$(".select2").select2();		
}
function saveSprintList(){
	
	preload('show');
	var f = document.getElementById('frm_createsprint');
	//alert(f); 
	f.method = 'post';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/project/sprintlist/';
}
function utilizationProcess(evn){
	alert(evn.code);
}

function searchSprintlist(){
	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}
function successSprintlistCallBack(){
	//location.reload();
	location.hash = '/project/sprintlist/?'+new Date().getTime();
}


function clearSprintlist(){

	preload('show');
	var f = document.getElementById('clearFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}

