
var dataid = '0';
var valid1 = '0';
var valid2 = '0';
$(function () {

		  $(".select2").select2();
	      $(".my_date").datepicker({
		      	todayBtn: "linked",
		       language: "th",
		       autoclose: true,
		       todayHighlight: true,
		       format: 'dd-mm-yyyy'
	      }).on('changeDate', function(ev) {
	      	/*console.log(ev)*/
	      	var date_c = ev.target.attributes[3].nodeValue;
	      	var star_date= $('#start_date').val();
	      	if(date_c=="end_date"&&star_date!=''){
				
			 	var end_dat= $('#'+date_c).val();
			 		 $.ajax({
	                type: "POST",
	                data:	{
	                		star_date:star_date,
	                		end_dat:end_dat
	                		},
	                dataType: "json",
	                url: BASEURL + "/project/checkDateSprintList/",
	                success: function(resp) {
	                	
		                       if (resp.result == "success") {
		                     
		                            $('#sprint_elapsed').val(resp.elapeseday);
		                            $('#sprint_holiday').val(resp.holiday);
		                            $('#sprint_working').val(resp.workingday);
		                            $('#sprint_number_dev').attr("readonly", false);
		                            $('#sprint_utilization').attr("readonly", false);
		                            
		                        }else{
									$('#start_date').val(null);
									$('#end_date').val(null);
									$('#start_date').focus();
								}
	                        }
	                      });  
			}// end if to chaeck end date
			else{
				
			}
	      	
		 	
		});
		
	$(document).ready(function() {
		
	    $("#sprint_utilization").keydown(function (e) {
			setTimeout(function(){
				if  ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
					(e.keyCode >= 35 && e.keyCode <= 40)) {
						
							
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		        	e.preventDefault();
		        }else{
					var workingday =  $('#sprint_working').val();
		        	var sprint_number_dev =  $('#sprint_number_dev').val();
		        	var sprint_number_dev = parseFloat(sprint_number_dev/100);
		        	var sprint_utilization =  $('#sprint_utilization').val();
		        	var sprint_avalible_how = parseFloat(workingday)*parseFloat(sprint_number_dev)*parseFloat(sprint_utilization)*8;
		        	//alert(sprint_utilization);
		        	var sprint_daily_how = parseFloat(sprint_avalible_how)/parseFloat(workingday);
		        	$('#sprint_avalible_how').val(sprint_avalible_how.toFixed(2));
		        	$('#sprint_daily_how').val(sprint_daily_how.toFixed(2));
		        	
		        	
				}
			},600);
	        
	       
	    });
	   
	     $("#sprint_number_dev").keydown(function (e) {
	     	 if($("#sprint_utilization").val()==''){}else{
			setTimeout(function(){
				if  ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
					(e.keyCode >= 35 && e.keyCode <= 40)) {
						
							
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		        	e.preventDefault();
		        }else{
					var workingday =  $('#sprint_working').val();
		        	var sprint_number_dev =  $('#sprint_number_dev').val();
		        	var sprint_number_dev = parseFloat(sprint_number_dev/100);
		        	var sprint_utilization =  $('#sprint_utilization').val();
		        	var sprint_avalible_how = parseFloat(workingday)*parseFloat(sprint_number_dev)*parseFloat(sprint_utilization)*8;
		        	//alert(sprint_utilization);
		        	var sprint_daily_how = parseFloat(sprint_avalible_how)/parseFloat(workingday);
		        	$('#sprint_avalible_how').val(sprint_avalible_how.toFixed(2));
		        	$('#sprint_daily_how').val(sprint_daily_how.toFixed(2));
		        	
		        	
				}
			},600);
	        
	       }
	    });
	    
		if($("#mode").val()=="editsprintlist"){
			var workingday =  $('#sprint_working').val();
        	var sprint_number_dev =  $('#sprint_number_dev').val();
        	var sprint_number_dev = parseFloat(sprint_number_dev/100);
        	var sprint_utilization =  $('#sprint_utilization').val();
        	var sprint_avalible_how = parseFloat(workingday)*parseFloat(sprint_number_dev)*parseFloat(sprint_utilization)*8;
        	//alert(sprint_utilization);
        	var sprint_daily_how = parseFloat(sprint_avalible_how)/parseFloat(workingday);
        	$('#sprint_avalible_how').val(sprint_avalible_how.toFixed(2));
        	$('#sprint_daily_how').val(sprint_daily_how.toFixed(2));
		}
		if($("#mode").val()=="viewsprintlist"){
			
			var workingday =  $('#sprint_working').val();
        	var sprint_number_dev =  $('#sprint_number_dev').val();
        	var sprint_number_dev = parseFloat(sprint_number_dev/100);
        	var sprint_utilization =  $('#sprint_utilization').val();
        	var sprint_avalible_how = parseFloat(workingday)*parseFloat(sprint_number_dev)*parseFloat(sprint_utilization)*8;
        	//alert(sprint_utilization);
        	var sprint_daily_how = parseFloat(sprint_avalible_how)/parseFloat(workingday);
        	$('#sprint_avalible_how').val(sprint_avalible_how.toFixed(2));
        	$('#sprint_daily_how').val(sprint_daily_how.toFixed(2));
        	$('#text_view1').text(sprint_avalible_how.toFixed(2));
        	$('#text_view2').text(sprint_daily_how.toFixed(2));
/*        	$('input').css('disabled','disabled');
        	$('select').css('disabled','disabled');*/
		}
		
	});
	

		
});

function changSprintTemplate(elem){
	var thisvalue = $(elem).val();

	ajaxRequestProcess('/project/sprintlistbacklog/?thisvalue='+thisvalue,changSprintTemplateCallback)
}

function changSprintTemplateCallback(resp){
	$('#html_backlog').html(resp);
    $(".select2").select2();
    preload('hide');
}
function removet(selects){
	var mybuttn = $(selects).parents();
	//console.log(mybuttn);
	hd_input = $('#hd_backlog_id',mybuttn[1]).val();
	sprint_id = $('#sprint_id_hd',mybuttn[1]).val();
	if(hd_input==0){
		$(mybuttn[1]).remove();
	}else{
		alertMessage('Please confirm to delete Sprint Backlog.','deleteCloneFunc');
	}
	//alert(hd_input);
	/*$(mybuttn[1]).remove();*/
}
function deleteCloneFunc(){
	if(hd_input!='0'){
		ajaxRequestProcess('project/delbacklog/'+hd_input+'/',deleteCLCallback);
		
	}else{
		return;
	}
}
function deleteCLCallback(){
	
	window.location.hash = '/project/editsprintlist/'+sprint_id+'/?'+new Date().getTime();
}








function clonefn (){
	//$('.select2').select2('disable');
    $("#text_clone","#stm_clone_b").clone().appendTo('#stm_clone_a');
    $(".select3").select2();
    //var mybuttns = $('#stm_clone_a','#html_backlog').parent();
    //alert(mybuttns);s
   /* console.log($('.select2check').length);*/
	//$(".select2").select2();		
}
function saveSprintList(){
	if($('#sprint_name').val()==''){
		noticeMessage('Please Enter Sprint Name','scriptFocus("#sprint_name");');
		return false;
	}
	if($('#s_project_name').val()==''||$('#s_project_name').val()==null){
		noticeMessage('Please Enter Project Name','scriptFocus("#s_project_name");');
		return false;
	}
	if($('#user_owner').val()==''||$('#user_owner').val()==null){
		noticeMessage('Please Enter Sprint Owner','scriptFocus("#user_owner");');
		return false;
	}
	if($('#start_date').val()==''||$().val('#start_date')==null){
		noticeMessage('Please Enter Start Date','scriptFocus("#start_date");');
		return false;
	}
	if($('#end_date').val()==''||$('#end_date').val()==null){
		noticeMessage('Please Enter End Date','scriptFocus("#end_date");');
		return false;
	}
	if($('#sprint_number_dev').val()==''||$().val('#sprint_number_dev')==null){
		noticeMessage('Please Enter Number of Dev.','scriptFocus("#sprint_number_dev");');
		return false;
	}
	if($('#sprint_utilization').val()==''||$('#sprint_utilization').val()==null){
		noticeMessage('Please Enter Utilization','scriptFocus("#sprint_utilization");');
		return false;
	}
	if($('#mode').val()=="editsprintlist"){
		
	}else{
		if($('#sprint_template').val()==''||$('#sprint_template').val()==null){
		noticeMessage('Please Enter Sprint Template','scriptFocus("#sprint_template");');
		return false;
	}
	}
	
	if($('#sprint_es_name').val()==''){
		noticeMessage('Please Enter Estimate Hour','scriptFocus("#html_backlog");');
		return false;
	}
	if($('#sprint_task_name').val()==''||$('#sprint_task_name').val()==null){
		noticeMessage('Please Enter Task Name','scriptFocus("#html_backlog");');
		return false;
	}/**/
	alertMessage('Please confirm to save Sprint List.','saveSprintListCallback');
}
function saveSprintListCallback(){

	preload('show');
	var f = document.getElementById('frm_createsprint');
	//alert(f); 
	f.method = 'post';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/project/sprintlist/';
}
function utilizationProcess(evn){
	alert(evn.code);
}

function searchSprintlist(){
	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}
function successSprintlistCallBack(){
	//location.reload();
	location.hash = '/project/sprintlist/?'+new Date().getTime();
}


function clearSprintlist(){

	preload('show');
	var f = document.getElementById('clearFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}
var dataids=0;
var datastatus=0;
var mytextnote=0;
var mynote=0;
var status_backlog=0;
function deleteSprintList(myojb){
	dataids = $(myojb).attr('data-id');
	alertMessage('Please confirm to delete Sprint List data.','deleteSprintListFunc');
}
function deleteSprintListFunc(){
	if(dataids!='0'){
		ajaxRequestProcess('project/delsprintlist/'+dataids+'/',deleteSLCallback);
		
	}else{
		return;
	}
}
function deleteSLCallback(){
	window.location.hash = '/project/sprintlist/?'+new Date().getTime();
}
function saveMySprintList(myojb){
	
	dataojb =  $(myojb).parents();
	dataids = $(myojb).attr('data-id');//sprint_status_backlog
	datastatus = $('.sprint_status_backlog',dataojb[1]).val();
	status_backlog =  $('.sprint_status_backlog',dataojb[1]);
	mytextnote = $('#sprint_note_backlog',dataojb[1]).val();
	mynote = $('#sprint_note_backlog',dataojb[1]);
	/*alert(datastatus);
	return false;*/
	var status_sprint = $('#sprint_status_backlog',dataojb[1]).attr('data-status');
	var status_change = $('#sprint_status_backlog',dataojb[1]).val();
	//console.log(datastatus);
	if( 
			
			(status_sprint=="Co" && status_change=="In") || 
			(status_sprint=="Co" && status_change=="Wa") ||
			(status_sprint=="In" && status_change=="Wa")
		
		){
			if($(mynote).val()==''){
				noticeMessage('Please Enter Note ','scriptFocus(mynote);');
				return false;
			}else{
				alertMessage('Please confirm to save My Sprint List.','saveMySprintListCallback');
			}
		}else{
			alertMessage('Please confirm to save My Sprint List.','saveMySprintListCallback');
		}
	
}
function saveMySprintListCallback(){
	if(dataids!='0'){
		ajaxRequestProcess('project/savemysprintlist/'+dataids+'/?status='+datastatus+'&text_note='+mytextnote,successMySprintListCallBack);
		
	}else{
		return;
	}
	
}
function successMySprintListCallBack(){
	preload('hide');
	mynote.fadeOut();
	mynote.val('');
	$(status_backlog).attr('data-status',datastatus);
/*	window.location.hash = '/project/mysprintlist/?'+new Date().getTime();*/
}
var s_id 		= '0';
var es_h  		= '0';
var task_n  	= '0';
var asign  		= '0';
var status_b	= '0';
var effrot		= '0';
var note		= '0';
function saveBacklogList(selects){
	var mybuttn = $(selects).parents();
	//console.log(mybuttn);
	hd_input 	= $('#hd_backlog_id',mybuttn[1]).val();
	s_id 		= $('#sprint_id').val();
	es_h		= $('#sprint_es_name',mybuttn[1]).val();
	task_n		= $('#sprint_task_name',mybuttn[1]).val();
	asign		= $('#backlog_owner',mybuttn[1]).val();
	status_b	= $('#sprint_status_backlog',mybuttn[1]).val();
	effrot		= $('#backlog_effrot:checked',mybuttn[1]).val();
	note		= $('#sprint_note_backlog',mybuttn[1]);
	text_note	= $('#sprint_note_backlog',mybuttn[1]).val();
	
	if(effrot==null||effrot==''){
		effrot='N';
	}
	
	/*alert(effrot);
	return;
	alert(es_h);
	alert(task_n);
	alert(asign);
	alert(status_b);
	return;*/
	if(hd_input==0){
		
		if($('#sprint_es_name',mybuttn[1]).val()==''){
			valid1 = $('#sprint_es_name',mybuttn[1]);
			noticeMessage('Please Enter Estimate Hour','scriptFocusBacklog1();');
			
		}else if($('#sprint_task_name',mybuttn[1]).val()==''){
			valid2 = $('#sprint_task_name',mybuttn[1]);
			noticeMessage('Please Enter Task Name','scriptFocusBacklog2();');
			
		}else{
			var status_sprint = $('#sprint_status_backlog',mybuttn[1]).attr('data-status');
			var status_change = $('#sprint_status_backlog',mybuttn[1]).val();
			if( 
				(status_sprint=="Ca" && status_change=="Co") || 
				(status_sprint=="Ca" && status_change=="In") || 
				(status_sprint=="Ca" && status_change=="Wa") || 
				(status_sprint=="Co" && status_change=="In") || 
				(status_sprint=="Co" && status_change=="Wa") ||
				(status_sprint=="In" && status_change=="Wa")
				){
				if($('#sprint_note_backlog',mybuttn[1]).val()=='' || $('#sprint_note_backlog',mybuttn[1]).val()==null){
					noticeMessage('Please Enter Note Backlog','scriptFocusNoteBacklog();');
				}else{
				alertMessage('Please confirm to save  Backlog List.','saveBacklogListFunc');
				}
			}else{
				if(text_note==null){
					text_note='';
				}
				alertMessage('Please confirm to save  Backlog List.','saveBacklogListFunc');
			}
		}
		/*$(mybuttn[1]).remove();*/
	}else{
		if($('#sprint_es_name',mybuttn[1]).val()==''){
			valid1 = $('#sprint_es_name',mybuttn[1]);
			noticeMessage('Please Enter Estimate Hour','scriptFocusBacklog1();');
			
		}else if($('#sprint_task_name',mybuttn[1]).val()==''){
			valid2 = $('#sprint_task_name',mybuttn[1]);
			noticeMessage('Please Enter Task Name','scriptFocusBacklog2();');
			
		}else{
			var status_sprint = $('#sprint_status_backlog',mybuttn[1]).attr('data-status');
			var status_change = $('#sprint_status_backlog',mybuttn[1]).val();
			if( 
					(status_sprint=="Ca" && status_change=="Co") || 
					(status_sprint=="Ca" && status_change=="In") || 
					(status_sprint=="Ca" && status_change=="Wa") || 
					(status_sprint=="Co" && status_change=="In") || 
					(status_sprint=="Co" && status_change=="Wa") ||
					(status_sprint=="In" && status_change=="Wa"))
			{
				if($('#sprint_note_backlog',mybuttn[1]).val()=='' || $('#sprint_note_backlog',mybuttn[1]).val()==null){
					noticeMessage('Please Enter Note Backlog','scriptFocusNoteBacklog();');
				}else{
					alertMessage('Please confirm to save  Backlog List.','saveBacklogListFunc');
				}
			}else{
				alertMessage('Please confirm to save  Backlog List.','saveBacklogListFunc');
			}
			
		}
		
	}
	//alert(hd_input);
	/*$(mybuttn[1]).remove();*/
}
function scriptFocusNoteBacklog(){
	note.focus();
	return false;
}
function scriptFocusBacklog2(){
	valid2.focus();
	return false;
}
function scriptFocusBacklog1(){
	valid1.focus();
	return false;
}
function saveBacklogListFunc(){
	ajaxRequestProcess('project/savebackloglist/'+s_id+'/?estimate='+es_h+'&input_hd='+hd_input+'&task_name='+task_n+'&assign='+asign+'&status='+status_b+'&effrot='+effrot+'&text_note='+text_note,saveBacklogListCLCallback);
}
function saveBacklogListCLCallback(){
	preload('hide');
	window.location.hash = '/project/editsprintlist/'+s_id+'/?'+new Date().getTime();
}
function checkedFn(elm){
	var mybuttn = $(elm).parents();
	if($('#backlog_effrot:checked',mybuttn[1]).val()=="Y"){
		$('#backlog_effrot_hd',mybuttn[1]).val('Y');
		
	}else{
		$('#backlog_effrot_hd',mybuttn[1]).val('N');
	}
	
}
function statusNote(elm){
	var mybuttn = $(elm).parents();
	var status_sprint = $(elm).attr('data-status');
	var status_change = $('#sprint_status_backlog',mybuttn[1]).val();
	if(status_sprint==status_change){
		$('#sprint_note_backlog',mybuttn[1]).fadeOut();
		$('#save_sprint').css('display','inline-block');
			$('#btn-clone-save',mybuttn[1]).css('display','inline-block');
	}else{
		if( 
			
			(status_sprint=="Ca" && status_change=="Co") || 
			(status_sprint=="Ca" && status_change=="In") || 
			(status_sprint=="Ca" && status_change=="Wa") || 
			(status_sprint=="Co" && status_change=="In") || 
			(status_sprint=="Co" && status_change=="Wa") ||
			(status_sprint=="In" && status_change=="Wa")
		
		){
			$('#save_sprint').css('display','none');
			$('#btn-clone-save',mybuttn[1]).css('display','none');
			$('#sprint_note_backlog',mybuttn[1]).fadeIn();
			
		}else{
			$('#sprint_note_backlog',mybuttn[1]).fadeIn();
			$('#save_sprint').css('display','inline-block');
			$('#btn-clone-save',mybuttn[1]).css('display','inline-block');
		}
	}
	/*$('#sprint_status_backlog',mybuttn[1]).val();*/
	//alert(status_change);
}
function checkSaveBtn(inp){
	var mybuttn = $(inp).parents();
	setTimeout(function(){
		if($('#sprint_note_backlog',mybuttn[1]).val()=='' || $('#sprint_note_backlog',mybuttn[1]).val()==null){
			$('#btn-clone-save',mybuttn[1]).css('display','none');
		}else{
			$('#save_sprint').css('display','inline-block');
			$('#btn-clone-save',mybuttn[1]).css('display','inline-block');
		}
			
		},600);
}
function statusMyBacklog(inp){
	var mybuttn = $(inp).parents();
	var status_sprint = $(inp).attr('data-status');
	var status_change = $('#sprint_status_backlog',mybuttn[1]).val();
	if(status_sprint==status_change){
		$('#btn-save-mybacklog',mybuttn[1]).css('display','inline-block');
		$('#sprint_note_backlog',mybuttn[1]).fadeOut();
	}else{
		if( 
			
			(status_sprint=="Co" && status_change=="In") || 
			(status_sprint=="Co" && status_change=="Wa") ||
			(status_sprint=="In" && status_change=="Wa")
		
		){
			$('#sprint_note_backlog',mybuttn[1]).fadeIn();
			
		}else{
			$('#sprint_note_backlog',mybuttn[1]).fadeIn();
			$('#btn-save-mybacklog',mybuttn[1]).css('display','inline-block');
		}
	}
	
	
	
}
var z=0;
function checkSaveMyBtn(inp){
	var mybuttn = $(inp).parents();
	
	$('#sprint_note_backlog').keyup(function(e){
		
		setTimeout(function(){

		if(e.keyCode == 8){
			var status_sprint = $('#sprint_status_backlog').attr('data-status');
			var status_change = $('#sprint_status_backlog',mybuttn[1]).val();
			if( (status_sprint=="Co" && status_change=="In") || (status_sprint=="Co" && status_change=="In") ){
				if($('#sprint_note_backlog').val()==null || $('#sprint_note_backlog').val()==''){
							$('#btn-save-mybacklog',mybuttn[1]).css('display','none');
				}
			}
			
				}
		},600);
		});
	setTimeout(function(){

			
		if($('#sprint_note_backlog',mybuttn[1]).val()=='' || $('#sprint_note_backlog',mybuttn[1]).val()==null){
			$('#btn-save-mybacklog',mybuttn[1]).css('display','none');
		}else{
			$('#btn-save-mybacklog',mybuttn[1]).css('display','inline-block');
		}
			
		},600);
}
function viewMyBacklog(inp){
	var mybuttn = $(inp).parents();
	//console.log(mybuttn);
	$('#hd_view_backlog',mybuttn[1]).slideToggle();

}

function statusMyBacklogTest(inp){
	var mybuttn = $(inp).parents();
	var status_sprint = $(inp).attr('data-status');
	var status_change = $('#sprint_status_backlog_test',mybuttn[1]).val();
	if((status_sprint==status_change)){
		$('#btn-save-mybacklog',mybuttn[1]).css('display','inline-block');
		$('#sprint_note_backlog',mybuttn[1]).fadeOut();
	}else{
		if( 
			
			(status_sprint=="Co" && status_change=="In") || 
			(status_sprint=="Co" && status_change=="Wa") ||
			(status_sprint=="In" && status_change=="Wa")
		
		){
			$('#btn-save-mybacklog',mybuttn[1]).css('display','none');
			$('#sprint_note_backlog',mybuttn[1]).fadeIn();
			
		}else{
			$('#sprint_note_backlog',mybuttn[1]).fadeIn();
			$('#btn-save-mybacklog',mybuttn[1]).css('display','inline-block');
		}
	}

}
var text_test_hd = '0';
var btn_save_my_test = '0';
var text_hd_ts	= '0';
function saveMySprintTest(myojb){
	
	dataojb				=  $(myojb).parents();
	dataids 			= $(myojb).attr('data-id');//sprint_status_backlog
	datastatus 			= $('#sprint_status_backlog_test',dataojb[1]).val();
	status_backlog 		=  $('#sprint_status_backlog_test',dataojb[1]);
	mytextnote 			= $('#sprint_note_backlog',dataojb[1]).val();
	mynote 				= $('#sprint_note_backlog',dataojb[1]);
	text_test_hd 		= $('#tester_text',dataojb[1]);
	btn_save_my_test 	= $('#btn-save-mybacklog-test',dataojb[1])
	text_hd_ts 			= $('#text_hd_ts',dataojb[1])
	/*alert(datastatus);
	return false;*/
	
	//console.log(datastatus);
	
	alertMessage('Please confirm to save My Sprint Tester.','saveMySprintTestCallback');
}
function saveMySprintTestCallback(){
	if(dataids!='0'){
		ajaxRequestProcess('project/savemysprinttest/'+dataids+'/?status='+datastatus+'&text_note='+mytextnote,successMySprintTestCallBack);
		
	}else{
		return;
	}
	
}
function successMySprintTestCallBack(){
	preload('hide');
	mynote.fadeOut();
	mynote.val('');
	$(status_backlog).attr('data-status',datastatus);
	if(datastatus=='In'){
		$(text_test_hd).text('Inprogress');
		$(text_test_hd).css('color','orange');
		$(status_backlog).css('display','none');
		$(btn_save_my_test).css('display','none');
		$(text_hd_ts).text('Inprogress');
		$(text_hd_ts).css('display','inline-block')
		$(text_hd_ts).css('color','orange');
	}
	if(datastatus=='Co'){
		$(text_test_hd).text('Completed');
		$(text_test_hd).css('color','green');
	}
	
/*	window.location.hash = '/project/mysprintlist/?'+new Date().getTime();*/
}
