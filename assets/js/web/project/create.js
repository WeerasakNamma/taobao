
//alert(project_id);
$(function () {
      $(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy'
      });
       $(".select2").select2();

       //--Set Team filter selection on load
       $(window).load(teamSelectFilter());

});

//--Team filter selection
function teamSelectFilter(){
	var owner_id 	= [];

		$('#user_owner :selected').each(function(i, selected){
		  owner_id[i] 	= $(selected).val();
		});

		var params = 'owner-id='+owner_id;
		$.ajax({
            type: 'post',
            dataType: 'json',
            url:  BASEURL+"/project/getProjectTeam/",
            data: params,
            success: function(resp){
            	//---- Remove old list widthout selected option
				$('#user_team option[data_check!="old"]').remove();

            	//---- Add New List from Query
            	var _select = $('#user_team');
            	for (var i = 0; i < resp.length; i++) {
            		$(_select).append(
            			$("<option></option>").val(resp[i].id).html(resp[i].user_name+" ("+resp[i].user_nickname+")")
            		);
            	};
            }
        });//Ajax Post Action
}


function saveProject(){
	alertMessage('Please confirm to Save Project.','saveProjectCallback');
}

function saveProjectCallback(){

	if($('#project_name').val()==''){
		noticeMessage('Please Enter Project Name');
		$('#project_name').focus();
		return;
	}

	if($('#project_desc').val()==''){
		noticeMessage('Please Enter Project Description');
		$('#project_desc').focus();
		return;
	}
	
	if($('#project_start').val()==''){
		noticeMessage('Please Enter Project Start');
		$('#project_start').focus();
		return;
	}

	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/project/projectlist'
}

var file_id = '';
function deleteFile(file_id_get){
	file_id = file_id_get;
	alertMessage('Please confirm to Delete File.','deleteFileFunc');
}

function deleteFileFunc(){
	if(file_id!=''){
		ajaxRequestProcess('project/deletefile/'+file_id,deleteFileCallback);
	}else{
		return;
	}
}
function deleteFileCallback(){
	//location.reload();
	location.hash = '/project/edit/'+project_id+'/?'+new Date().getTime();
}


var file_id2 = '';
function deleteFile2(file_id_get2){
	file_id2 = file_id_get2;
	alertMessage('Please confirm to Delete File.','deleteFileFunc2');
}

function deleteFileFunc2(){
	if(file_id2!=''){
		ajaxRequestProcess('project/deletefile/'+file_id2,deleteFile2Callback);
	}else{
		return;
	}
}
function deleteFile2Callback(){
	//location.reload();
	location.hash = '/project/view/'+project_id+'/?'+new Date().getTime();
}


function openCreate(){
	$('#addon').slideToggle();
}
function openComment(){
	$('#comment').slideToggle();
}
function saveComment(){
	if($('#comment_text').val()==''){
		noticeMessage('Please Enter Comment','scriptFocus("#comment_text");');
		return;
	}


	alertMessage('Please confirm to post comment.','saveCommentFunc');
}

function saveCommentFunc(){


	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}
function saveCommentCallback(){
	//location.reload();
	location.hash = '/project/view/'+project_id+'/?'+new Date().getTime();
}
function savFile(){
	// if($('#file_name').val()==''){
// 		noticeMessage('Please Enter File name','scriptFocus("#file_name");');
// 		return;
// 	}
	alertMessage('Please confirm to Upload File.','saveFileFunc');
}

function saveFileFunc(){

	preload('show');
	var f = document.getElementById('uploadFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}
function saveFileCallback(){
	//location.reload();
	location.hash = '/project/view/'+project_id+'/?'+new Date().getTime();
}
function openCreate_C(){
	$('#addon').slideToggle();
	$('#addon_c').slideToggle();
}

function saveAddon(){
	alertMessage('Please confirm to Save Add-On.','saveAddonFunc');
}

function saveAddonFunc(){

	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}
function saveAddonCallback(){
	//location.reload();
	location.hash = '/project/edit/'+project_id+'/?'+new Date().getTime();
}
function saveAddonEditCallback(hashid){
	preload('hide');
	window.location.hash = '/project/addon/'+hashid+"/";
}
var add_id = '';
function deleteAddon(add_id_get){
	add_id = add_id_get;
	alertMessage('Please confirm to Delete Add-On.','deleteAddonFunc');
}

function deleteAddonFunc(){
	if(add_id!=''){
		ajaxRequestProcess('project/deleteaddon/'+add_id,deleteAddonCallback);
	}else{
		return;
	}
}
function deleteAddonCallback(){
	//location.reload();
	location.hash = '/project/addon/'+project_id+'/?'+new Date().getTime();
}



function checkPayment(lthis){
	var tptype = lthis.value;
	if(tptype!='1'){
		$('.noproject').show('slow');
	}else{
		$('.noproject').hide('slow');
	}
}



function savePayment(){

	alertMessage('Please confirm to Save Payment.','savePaymentFunc');
}
function savePaymentFunc(){

	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();
}
function savePaymentCallback(){
	location.hash = '/project/payment/'+paymentid+'/?'+new Date().getTime();
	//location.reload();
}
function saveRound(payid){

	if($('#round_name_e_'+payid).val()==''){
		noticeMessage('Please Enter งวดที่ ?','scriptFocus("#round_name_e_+payid");');
		return;
	}

	if($('#round_desc_e_'+payid).val()==''){
		noticeMessage('Please Enter รายละเอียด','scriptFocus("#round_desc_e_+payid");');
		return;
	}

	if($('#round_percent_e_'+payid).val()==''){
		noticeMessage('Please Enter เปอร์เซ็น ?','scriptFocus("#round_percent_e_+payid");');
		return;
	}

	if($('#round_bath_e_'+payid).val()==''){
		noticeMessage('Please Enter Bath','scriptFocus("#round_bath_e_+payid");');
		return;
	}

	var roundname = $('#round_name_e_'+payid).val();
	var rounddesc = $('#round_desc_e_'+payid).val();
	var roundpercent = $('#round_percent_e_'+payid).val();
	var roundbath = $('#round_bath_e_'+payid).val();
	var roundstatus = $('#round_status_e_'+payid).val();
	var roundnote = $('#note_e_'+payid).val();
	var roundstart = $('#round_start_e_'+payid).val();
	var roundend = $('#round_end_e_'+payid).val();

	//alert(roundstart);

	$('#round_name_s').val(roundname);
	$('#round_desc_s').val(rounddesc);
	$('#round_percent_s').val(roundpercent);
	$('#round_bath_s').val(roundbath);
	$('#round_status_s').val(roundstatus);
	$('#note_s').val(roundnote);
	$('#round_start_s').val(roundstart);
	$('#round_end_s').val(roundend);

	//alert($('#round_end_s').val());

	$('#mode_s').val('createround');
	$('#payment_id_s').val(payid);
	alertMessage('Please confirm to Save Payment Round.','savePaymentRoundFunc');
}

function editRound(payid){
	var roundname = $('#round_name_d_'+payid).val();
	var rounddesc = $('#round_desc_d_'+payid).val();
	var roundpercent = $('#round_percent_d_'+payid).val();
	var roundbath = $('#round_bath_d_'+payid).val();
	var roundstatus = $('#round_status_d_'+payid).val();
	var roundnote = $('#note_d_'+payid).val();

	$('#round_name_s').val(roundname);
	$('#round_desc_s').val(rounddesc);
	$('#round_percent_s').val(roundpercent);
	$('#round_bath_s').val(roundbath);
	$('#round_status_s').val(roundstatus);
	$('#note_s').val(roundnote);

	$('#mode_s').val('updateround');
	$('#round_id_s').val(payid);
	alertMessage('Please confirm to Save Payment Round.','savePaymentRoundFunc');
}

function savePaymentRoundFunc(){


	preload('show');
	var f = document.getElementById('createFrmPay');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/project/processFrm",
	f.submit();


}
function savePaymentRoundCallback(){
	//location.reload();
	location.hash = '/project/payment/'+paymentid+'/?'+new Date().getTime();
}

var round_id = '';
function deleteRound(round_id_get){
	round_id = round_id_get;
	alertMessage('Please confirm to Delete Round.','deleteARoundFunc');
}

function deleteARoundFunc(){
	if(round_id!=''){
		ajaxRequestProcess('project/deleteround/'+round_id+"/",deleteRoundCallback);
	}else{
		return;
	}
}
function deleteRoundCallback(){
	//location.reload();
	location.hash = '/project/payment/'+paymentid+'/?'+new Date().getTime();
}

var paym_id = '';
function deletePayment(paym_id_get){
	paym_id = paym_id_get;
	alertMessage('Please confirm to Delete Payment.','deletePaymentFunc');
}

function deletePaymentFunc(){
	if(paym_id!=''){
		ajaxRequestProcess('project/deletepayment/'+paym_id+"/",deletePaymentCallback);
	}else{
		return;
	}
}
function deletePaymentCallback(){
	//location.reload();
	location.hash = '/project/payment/'+paymentid+'/?'+new Date().getTime();
}

var paym_val = '';

function updatePaymentValue(paymidget){
	paym_val = $('#payment_value_'+paymidget).val();
	paym_id = paymidget;
	alertMessage('Please confirm to Update Payment Value.','updatePaymentValFunc');
}

function updatePaymentValFunc(){
	if(paym_id!=''){
		ajaxRequestProcess('project/updatepayment/'+paym_id+"/?value="+paym_val,updatePaymentValCallback);
	}else{
		return;
	}
}
function updatePaymentValCallback(){
	//location.reload();
	location.hash = '/project/payment/'+project_id+'/?'+new Date().getTime();
}

function openHistory(roundids){
	$('#history_'+roundids).slideToggle();
}


function cloneFileProject(thisojb){
	var thisojbc =  $('#clone_c').clone().appendTo('#project_o');
	var divparr = $(thisojbc).parents();
	//console.log(divparr[0]['children'].length);
	var lastidx = (divparr[0]['children'].length)-1;
	$('.item-name',divparr[0]['children'][lastidx]).val('');
	$('.btn-danger',divparr[0]['children'][lastidx]).show();
	idx = 1;
	$('#expenses_o .form-group').each(function(){
	    $('.control-label',this).text("File "+idx);
	    idx++;
	});
}



var mydatadelete;
var didrive = [];

//Remove file in view.htm
function removeFileV(thisojb){
	//console.log($(thisojb).parent());return;
	
	mydatadelete = $(thisojb).attr('data-id');
	didrive[1] = $(thisojb).parent();
	alertMessage('Please confirm to Delete File.','daleteFileFunc');

}



function removeFileT(thisojb){
	mydatadelete = $(thisojb).attr('data-id');
	didrive = $(thisojb).parents();
	alertMessage('Please confirm to Delete File.','daleteFileFunc');

}
function daleteFileFunc(){
	ajaxRequestProcess('/project/deletefile/'+mydatadelete+'/',callbackDeleteFileCallback)
}
function callbackDeleteFileCallback(){
	//console.log(didrive);
	$(didrive[1]).remove();
	preload('hide');
}
function removeFileProject(thisojb){ 
	var divparr = $(thisojb).parents();
	console.log(divparr);
	$(divparr[1]).remove();
}
var title_value;
function updateFileTitleProject(thisojb){
	title_value = $(thisojb).val();
	title_value = Base64.encode(title_value);
	var dataid = $(thisojb).attr('data-id');
	ajaxRequestProcess('/project/updatefile/'+title_value+'/?myid='+dataid,callbackUpdateFileCallback);
}
function callbackUpdateFileCallback(){
	preload('hide');
}

var comment_id = '0';
function deleateComment(comment_idx){
	comment_id = comment_idx;
	alertMessage('Please confirm to Delete Comment.','daleteCommentFunc');
}
function daleteCommentFunc(){
	ajaxRequestProcess('/project/deletecomment/'+comment_id+'/',callbackDeleteCommentCallback);
}
function callbackDeleteCommentCallback(){
	location.hash = '/project/view/'+project_id+'/?'+new Date().getTime();
}