$(function () {
      
      
      
      $(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy' 
      });
     
      
});

/*function searchPayment(){
	alertMessage('Please confirm to Search...','searchPaymentCallback');
}*/

function searchPayment(){

	preload('show');
	var f = document.getElementById('createFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/account/processFrm", 
	f.submit();
}
function successPaymentCallBack(){
	//location.reload();
	window.location.hash = '/account/payment/?'+new Date().getTime();
}


function clearPayment(){

	preload('show');
	var f = document.getElementById('clearFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/account/processFrm", 
	f.submit();
}

function opentable(elems){
	var selector_m = $(elems).parents();
	var dataopen = $(elems).attr('data-open');
	if(dataopen=='C'){
		$('.m-hidden',selector_m[1]).show('slow');
		$(elems).attr('data-open','O');
		$(elems).text('Hide');
	}else{
		$('.m-hidden',selector_m[1]).hide('slow');
		$(elems).attr('data-open','C');
		$(elems).text('Show More');
	}
	
}