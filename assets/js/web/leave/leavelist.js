file_name[1] = '/assets/js/visioninter/leave/leavelist.js';

var dataid = '0';

$(function () {

       /*$("#example1").DataTable();*/
        $('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });


      $('.box-body').on('click', '.leave-delete', function(){
			dataid = $(this).attr('data-id');
			alertMessage('Please confirm to Delete Leave.','daleteLeaveCallback');

		});


});
function daleteLeaveCallback(){
	if(dataid!='0'){
		ajaxRequestProcess('leave/delete/'+dataid,deleteLeavesCallback);
	}else{
		return;
	}

}
function deleteLeavesCallback(){

	location.hash = '/leave/?'+new Date().getTime();
}

function changeYearMy(elems){
	//location.reload();
	var yaerss = $(elems).val();
	
	location.hash = '/leave/?year='+yaerss+'&'+new Date().getTime();

}

function cancelLeave(elems){
	dataid = $(elems).attr('data-id');
	alertMessage('Please confirm to Cancel Leave.','cancelLeaveCallback');
}
function cancelLeaveCallback(){
	if(dataid!='0'){
		ajaxRequestProcess('leave/cancel/'+dataid,deleteLeavesCallback);
	}else{
		return;
	}

}
