file_name[0] = '/assets/js/visioninter/leave/create.js';

$(function () {
      $(".leave_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy',
      })
      .on('changeDate', function(ev){
            console.log($(this).parents());
            var ppr = $(this).parents();
            var dataidsd_1 = ppr[2].id;
        
            if(dataidsd_1=='item_date_early_1_div' || dataidsd_1=='item_date_early_2_dive'){
            	
				if($('#item_date_early_1').val()==$('#item_date_early_2').val()){
					//alert($('#item_date_early_1').val());
					$(this).val('');
					alert('Please select again.');
				}
			}
        });

      /*$('body').on('click', '#saveLeave', function(){
			alertMessage('Please confirm to Create Leave.','saveLeaveCallback');
	  });*/
	  /*$('body').on('click', '#clonedate', function(){
			$('#clone_c').clone().appendTo('#leave_o');
			$('.leave_date').each(function(){
			    $(this).datepicker({
			    	todayBtn: "linked",
			       language: "th",
			       autoclose: true,
			       todayHighlight: true,
			       format: 'dd/mm/yyyy'
			    });
			});
			idx = 1;
			$('#leave_o .form-group').each(function(){
			    $('.control-label',this).text("Leave Date "+idx);
			    idx++;
			});
	  });*/

});


function saveLeave(){
	alertMessage('Please confirm to Save Leave.','saveLeaveCallback');

}


function cloneDate(){
	$('#clone_c').clone().appendTo('#leave_o');
	$('.leave_date').each(function(){
	    $(this).datepicker({
	    	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy'
	    });
	});
	idx = 1;
	$('#leave_o .form-group').each(function(){
	    $('.control-label',this).text("Leave Date "+idx);
	    idx++;
	});
	$('#leave_o .leave_date').last().val('');

}

function saveLeaveCallback(){

	if($('#leave_title').val()==''){
		noticeMessage('Please Enter Leave Title',"scriptFocus('#leave_title')");
		//$('#leave_title').focus();
		return;
	}

	if($('#leave_type').val()==0){
		noticeMessage('Please Enter Leave Type',"scriptFocus('#leave_type')");
		//$('#leave_type').focus();
		return;
	}

	if($('#leave_desc').val()==''){
		noticeMessage('Please Enter Description',"scriptFocus('#leave_desc')");
		//$('#leave_desc').focus();
		return;
	}

	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/leave/processFrm",
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/leave/'
}
function deleteClone(thisis){
	var lengths = $('div.div-ldate').length;
	if(lengths>1){
		var pprent = $(thisis).parents();
		$(pprent[1]).remove();
		idx = 1;
		$('#leave_o .form-group').each(function(){
		    $('.control-label',this).text("Leave Date "+idx);
		    idx++;
		});
	}else{
		noticeMessage("Can't delete it.");
	}


}
var dataid = '0';
var elemetdel = '';

function deleteDateLeave(thisis){
	dataid = $(thisis).attr('data-id');
	elemetdel = thisis;
	alertMessage('Please confirm to Delete Date.','deleteDateLeaveFunc');
}

function deleteDateLeaveFunc(){
	if(dataid!='0'){
		ajaxRequestProcess('leave/deletedate/'+dataid,deleteDateLeaveCallback);
	}else{
		return;
	}
}
function deleteDateLeaveCallback(){
	var pparent = $(elemetdel).parents();
	//console.log(pparent);
	$(pparent[1]).remove();
	preload('hide');
}

function checkLeaveType(elems){
	var dataelem = $(elems).val();
	if(dataelem=='5'){
		$('#normal_leave').hide('slow');
		$('#earlyleave').show('slow');
	}else{
		$('#normal_leave').show('slow');
		$('#earlyleave').hide('slow');
	}
	
}

function checkTimeForLeave(elems){
	var dataelem = $(elems).val();
	if(dataelem=='1'){
		$('#item_date_early_2_dive').hide('slow');
		$('#item_date_early_2').val('');
	}else{
		$('#item_date_early_2_dive').show('slow');
		
	}
}