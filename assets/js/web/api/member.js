



function setnewItemImport() {
    window.location.hash = '/shipping/order/?' + new Date().getTime();
}
/*function itemSearch(){

	$.ajax({
		   type: "GET",
		   url:  BASEURL+"/assets/API/demo/api.php?item_id=111",
		   success: function(resp){
		   		$('#searchBody').html(resp);
			}
		 });
}*/
function removeItem(key) {

    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/removeitem/?key=" + key,
        success: function(resp) {
            window.location.hash = '/buyship/order/?' + new Date().getTime();
        }
    });
}

function changeImg(imgs) {
    $('#topImg').attr('src', imgs);
    $('#pd_img').val(imgs);
}

function orderNow() {
    if ($('#product_qty').val() <= 0) {
        alert('กรุณาระบุจำนวนสินค้า');
        $('#product_qty').focus();
        return;
    }
    var f = document.getElementById('productHome');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'orderFrm';
    f.action = BASEURL + "/order/additemMember/",
        f.submit();
}
var dataidexs = 0;
var datapriceesx = 0;

function setWallet(elems) {
    dataidexs = $(elems).attr('data-id');
    datapriceesx = $(elems).attr('data-price');
    alertMessage('Please confirm to use E Wallet.', 'setWalletCallback');
}

function setWalletCallback() {
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/moneywallet/?orderid=" + dataidexs + "&order_price=" + datapriceesx,
        success: function(resp) {
            window.location.hash = '/buyship/orderlist/?' + new Date().getTime();
        }
    });
}

function takeorder() {

    var numberOfChecked = $('input[name="select_product[]"]:checked').length;
    if (numberOfChecked == 0) {
        noticeMessage('กรุณาติ๊กเลือกสินค้าที่ต้องการสั่งซื้อ');
        return;
    }

    alertMessage('โปรดยืนยันการสั่งซื้อสินค้า', 'takeorderCallback');
}

function takeorderCallback() {
    //$('#preLoadSearch').show();
    preload('show');
    var f = document.getElementById('takeOrder');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/buyship/processFrm/",
        f.submit();
}

function sendTracking() {
    if (!confirm('โปรดยืนยันการยืนยันการส่งสินค้าหรือไม่?')) {
        return;
    }
    $('#orderMsg').modal({
        backdrop: 'static'
    });
    var f = document.getElementById('takeOrder');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'frameUpdate';
    f.action = BASEURL + "/order/sendtrack/",
        f.submit();
}

function reOrder(orderid) {

    if (!confirm('คุณต้องการสั่งซื้อรายการนี้อีกครั้งหรือไม่?')) {
        return;
    }
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/reorder/?order_id=" + orderid,
        success: function(resp) {
            window.location.hash = '/buyship/orderlist/?' + new Date().getTime();
        }
    });
}

function addItemImp() {

    if ($('#tracking_no').val() == '') {
        noticeMessage('กรุณากรอกเลขแทรค', 'scriptFocus("#tracking_no");');

        return;
    }

    $('#preLoadSearch').show();
    var trackno = $('#tracking_no').val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/shipping/checktrackingno/?tracking=" + trackno,
        success: function(resp) {

        	//alert(resp);
        	

            if (resp == '') {
               var f = document.getElementById('takeOrder_Import');

                f.method = 'post';
                f.enctype = 'multipart/form-data';
                f.target = 'updateFrm';
                f.action = BASEURL + "/shipping/processFrm/",
                    f.submit();
            }else if(resp == 2){
            	noticeMessage('ไม่สามารถบันทึกข้อมูลได้ เนื่องจากแทรคซ้ำ กรุณาติดต่อ Admin', 'scriptFocus("#tracking_no");');
                $('#preLoadSearch').hide();
                return;

            } else {
                noticeMessage('รบกวนลูกค้าติดต่อพนักงานเพื่อหาสินค้าในกลุ่มรอหาเจ้าของค่ะ taobaochinacargo@gmail.com', 'scriptFocus("#tracking_no");');
                $('#preLoadSearch').hide();
                return;
            }
        }
    });


}

function removeItemImp(key) {
    if (!confirm('คุณต้องการลบรายการนี้หรือไม่')) {
        return;
    }
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/shipping/removeitemimp/?key=" + key,
        success: function(resp) {
            window.location.hash = '/shipping/order/' + new Date().getTime();
        }
    });
}

function takeorderImp() {
    alertMessage('คุณต้องการเพิ่มข้อมูลแทรคทั้งหมดหรือไม่?', 'takeorderImpCallback');
}

function takeorderImpCallback() {
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/shipping/takeorder/",
        success: function(resp) {

            window.location.hash = '/tracking/trackinglist/';

        }
    });
}

function orderimpUpdate() {

    if (!confirm('คุณต้องการแก้ไขรายการนี้หรือไม่?')) {
        return;
    }
    $('#orderMsg').modal({
        backdrop: 'static'
    });
    var f = document.getElementById('editOrder_Import');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'frameUpdate';
    f.action = BASEURL + "/order/editOrderImp",
        f.submit();
}

function paymentUpdate() {

    if (!confirm('คุณต้องการแจ้งโอนเงินรายการนี้หรือไม่?')) {
        return;
    }
    $('#orderMsg').modal({
        backdrop: 'static'
    });
    var f = document.getElementById('updateFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'frameUpdate';
    f.action = BASEURL + "/member/payment_track/insert",
        f.submit();
}

function updateProfile() {
    if ($('#member_email').val() == '') {
        alert('กรุณาระบุอีเมล์');
        $('#member_email').focus();
        return;
    }

    if ($('#old_pwd').val() != '' || $('#new_pwd').val() != '' || $('#renew_pwd').val() != '') {

        if ($('#new_pwd').val() != $('#renew_pwd').val() || $('#new_pwd').val() == '') {
            alert('กรุณากรอกรหัสผ่านให้ตรงกัน');
            $('#renew_pwd').focus();
            return;
        }

    }

    if (!confirm('คุณต้องการเปลี่ยนแปลงข้อมูลหรือไม่?')) {
        return;
    }
    $('#orderMsg').modal({
        backdrop: 'static'
    });
    var f = document.getElementById('profileFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'frameUpdate';
    f.action = BASEURL + "/member/profile/update",
        f.submit();

}

function wrongPwd() {
    $('#orderMsg').modal('hide');
    alert('กรุณากรอกรหัสผ่านเดิมให้ตรงกัน');
    $('#old_pwd').focus();
}

function validateKey(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function cancelOrder(itemid) {
    if (!confirm('คุณต้องการ ยกเลิก รายการนี้หรือไม่')) {
        return;
    }
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/cancelorder/?order_id=" + itemid,
        success: function(resp) {

            window.location.hash = '/buyship/orderlist/?' + new Date().getTime();
        }
    });
}

function checkAll(elem) {
    var propdata = $(elem).attr('data-prop');
    if (propdata == 'C') {
        $('.checkbox').prop('checked', true);
        $(elem).attr('data-prop', 'O');
    } else {
        $('.checkbox').prop('checked', false);
        $(elem).attr('data-prop', 'C');
    }
    //alert();

}

function getecoupon(elem) {
    var e_no = $(elem).val();
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/getecoupon/?e_no=" + e_no,
        success: function(resp) {
            //alert(resp);
            if (resp == 'T') {
                window.location.hash = '/buyship/checkout/' + new Date().getTime();
            } else if (resp == 'NULL') {
                $('#preLoadSearch').hide();
                window.location.hash = '/buyship/checkout/' + new Date().getTime();
            } else {
                $('#preLoadSearch').hide();
                alert('E-Coupon นี้ไม่สามารถใช้งานได้');
                window.location.hash = '/buyship/checkout/' + new Date().getTime();
                return;
            }

        }
    });
}

function setkeyorder() {
    $('#preLoadSearch').show();

    var f = document.getElementById('takeOrder');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/buyship/setkeyorder/",
        f.submit();
}