function getDATAHTML_1(dataJson) {
    var data = '';
    $.ajax({
        type: "GET",
        url: BASEURL + "/API/html_2.php?a=json",
        success: function(resp) {
            
            getHTMLTITLE();
            getHTMLSHOP_1();
            var ojb = JSON.parse(resp);
            skuMap_c = ojb;
            preload('hide');
            $('#orderFrm').show();
            $('#product_link').val(dataJson.itemLink);
            $('#topImg').html(dataJson.dataImg);
            $('#J_ImgBooth').attr('width', '100%');
            if (typeof ojb.sku != "undefined") {
                console.log(ojb.sku.skuProps);

                var x_html = '<div id="sku-1688">';

                $.each(ojb.sku.skuProps, function(key, value) {
                    /*var cts = 'สี';
			   		var cts_c = 'color';
				    if(key==1){
				   	  cts = 'ขนาด';
				   	  cts_c = 'size';
				    }*/

                    var str_Input = '<input type="hidden" name="op_name[]" id="name_' + value.keylock + '" value=""/>';
                    str_Input += '<input type="hidden" name="op_name_th[]" id="name_th_' + value.keylock + '" value=""/>';
                    $('#pd_option').append(str_Input);
                    $('#name_' + value.keylock).val(value.prop);
                    translateAPIval(value.prop, '#name_th_' + value.keylock);

                    x_html += '<dl><dt class="tb-property-type">' + value.prop + '</dt><dd><ul data-property="' + value.prop + '" class="J_TSaleProp tb-clearfix cx_' + value.keylock + '">';
                    var cclass = '';
                    //if(key_c==0){


                    //}
                    $j = 0;
                    $.each(value.value, function(key_c, value_c) {
                        var cclass = '';
                        if ($j == 0) {
                            cclass = 'class="active"';
                            $j = 1;
                            var str_Input_c = '<input type="hidden" name="op_value[]" id="value_' + value.keylock + '" value=""/>';
                            str_Input_c += '<input type="hidden" name="op_value_th[]" id="value_th_' + value.keylock + '" value=""/>';
                            $('#pd_option').append(str_Input_c);
                            var sp_selector = '#value_' + value.keylock;
                            var sp_selector2 = '#value_th_' + value.keylock;
                            $(sp_selector).val(value_c.name);
                            translateAPIval(value_c.name, sp_selector2);
                        }
                        var cxml = '';
                        var kstyle = '';
                        //alert(value_c.imageUrl);
                        if (value_c.imageUrl == null || value_c.imageUrl=="") {
                            cxml = '';
                            
                        }else{ 
                            cxml = '<img src="' + value_c.imageUrl + '" width="40" alt="' + value_c.name + '" title="' + value_c.name + '" /><br />';
                            kstyle = 'min-height:65px;';
                        }
                        /*x_html += '<li style="height:auto" data-value="'+value_c.mapping+'" style="'+kstyle+'" '+cclass+'>'+cxml+'<a href="javascript:;" data-toggle="tooltip" title="'+value_c.name+'"><span data-cn="'+value_c.name+'">'+value_c.name+'</span></a></li>';*/
                        if (kstyle == '') {
                            x_html += '<li style="height:auto" data-value="' + value_c.mapping + '" style="' + kstyle + '" ' + cclass + '>' + cxml + '<a href="javascript:;" data-toggle="tooltip" title="' + value_c.name + '"><span data-cn="' + value_c.name + '">' + value_c.name + '</span></a></li>';
                        } else {
                            x_html += '<li style="height:auto" data-value="' + value_c.mapping + '" style="' + kstyle + '" ' + cclass + '>' + cxml + '<a href="javascript:;" data-toggle="tooltip" title="' + value_c.name + '"><span data-cn="' + value_c.name + '" style="display:none;">' + value_c.name + '</span></a></li>';
                        }
                    });
                    x_html += '</ul></dd></dl>';
                });

                x_html += '</div>';

                $('#preLoadSearch').hide();
                $('#productHome').show();
                $('#prop_data').html(x_html);

                $("dt.tb-property-type").each(function(index) {
                    translateAPI($(this).text(), this);
                });

                $("ul.J_TSaleProp").each(function(index) {
                    $("li a span", this).each(function(index) {
                        translateAPI($(this).text(), this);
                    });
                });


                //console.log(skuMap_c.sku.priceRange);
                var price_rmb = skuMap_c.sku.price;
                //alert(price_rmb);
                //price_rmb = price_rmb.split("-", 1);
                /*if (typeof skuMap_c.sku.priceRange === "undefined") {
                    price_rmb = skuMap_c.sku.price;
                    var price_rmb_x = price_rmb;
                    price_rmb = price_rmb.split("-",1);
                    //alert(price_rmb);
                    if(price_rmb=='0.00'){
                		price_rmb = price_rmb_x.replace("0.00-", "");
                	}
                    
                }else{
                	price_rmb = skuMap_c.sku.priceRange[0][1];
                }*/

                /*$.each( skuMap_c.sku.priceRange, function( key, value ) {
                	if(value[0]==1){
                		price_rmb = value[1];
                	}
                	
                });*/



                var pice_th = price_rmb * cnrate;
                if (isNaN(pice_th)) {
                    $('#productHome').hide();
                    $('#noresult').show();
                    return;
                }
                $('#price_th').text(pice_th.toFixed(2));
                $('#price_rmb').text(price_rmb);
                $('#product_price_thb').val(pice_th.toFixed(2));
                $('#product_price_rmb').val(price_rmb);

                $('#product_qty').attr('onchange', 'calMap();');

                var datImg = $('#topImg img').attr('src');
                //alert(datImg);
                $('#pd_img').val(datImg);

                $('#product_qty').val(skuMap_c.sku.priceRange[0][0]);

            } else {
                $('#preLoadSearch').hide();
                $('#productHome').show();
                var price_rmb = 0.00;
                price_rmb = dataJson.dataPrice;
                var pice_th = price_rmb * cnrate;
                $('#price_th').text(pice_th);
                $('#price_rmb').text(price_rmb);
                $('#product_price_thb').val(pice_th);
                $('#product_price_rmb').val(price_rmb);


                var datImg = $('#topImg img').attr('src');
                //alert(datImg);
                $('#pd_img').val(datImg);
            }
            /*console.log(ojb.sku.skuProps);
            alert(ojb.sku.skuProps);
            console.log(ojb.sku.skuMap);
            alert(ojb.sku.skuMap);*/
        }
    });
    //return data;
}

function calMap() {
    var dataVal = $('#product_qty').val();
    var price_rmb = 0;
    $.each(skuMap_c.sku.priceRange, function(key, value) {
        if (value[0] <= dataVal) {
            price_rmb = value[1];
        }
    });
    if (price_rmb == 0) {
        $('#product_qty').val(skuMap_c.sku.priceRange[0][0]);
        price_rmb = skuMap_c.sku.priceRange[0][1];
        alert('ขั้นต่ำในการสั่งซื้อ ' + skuMap_c.sku.priceRange[0][0] + "ชิ้น");
    }

    var pice_th = price_rmb * cnrate;
    $('#price_th').text(pice_th);
    $('#price_rmb').text(price_rmb);
    $('#product_price_thb').val(pice_th);
    $('#product_price_rmb').val(price_rmb);
}



function getHTMLSHOP_1() {
    var data = '';
    $.ajax({
        type: "GET",
        url: BASEURL + "/API/html_2.php?a=shopname",
        success: function(resp) {

            if (resp == '') {
                $('#shopname').html('ไม่พบชื่อร้าน');
                $('#shop_name').val($('#shopname').text());
                $('#shop_link').val($('#shopname').text());
                return;
            }
            $('#shopname').html(resp);
            $('#shop_name').val($('#shopname').text());
            var shoplink = $('a', '#shopname').attr('href');
            $('#shop_link').val('http:' + shoplink);
        }
    });
}

$('[data-toggle="tooltip"]').tooltip();