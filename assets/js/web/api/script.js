/*$(document).ready(function(){$(".megamenu").megamenu();});*/
var checkdata = 'F';



function uploadfiletemp(elem) {
    $('.up-text').hide();
    $('.fa-spin').show();

    //alert();
    $('#uploadFile').val($(elem).val());
    setTimeout(function() {

        //$(elem).val('');
        mmUpload();
    }, 800);

}

function mmUpload() {
    setTimeout(function() {

        var f = document.getElementById('tempFrm');

        f.method = 'post';
        f.enctype = 'multipart/form-data';
        f.target = 'updateFrm';
        f.action = BASEURL + "/utility/uploadfiletemp/",
            f.submit();
    }, 1500);
}

function uploadfiletempCallBack(pathname) {
    $('.up-text').show();
    $('.fa-spin').hide();
    $('#uploadFile').val('');
    $('#pd_img').val(pathname);
    $('#topImg img').attr('data-src', pathname);
    $('#topImg img').attr('src', pathname);
}




function getrate() {
    /*$.ajax({
       type: "GET",
       url:  BASEURL+"/order/getrate",
       success: function(resp){
       		cnrate = Number(resp);
       		//alert(cnrate);
    	}
     });*/
}

function bigSearch(action) {
    if (action == 'open') {
        $('.big-search').show('slow');
        $('#searchurl').focus();
    } else {
        $('.big-search').hide('slow');
    }
}

function OrderItem(){
    $('#noresult').show('slow');
}

function itemSearch(webtype) {
    /*$.ajax({
       type: "GET",
       url:  BASEURL+"/order/cleartemp",
       success: function(resp){

       		$('#preLoadItem').hide();
    		$('#basket_pd').show();
       		$('#basket_pd').html(resp);

    	}
     });*/
    //alert('k');

    var webLink = $('#searchurl').val();
    if (webLink == '') {
        return;
    }
    if (webtype == 'front2') {
        webLink = $('#searchurl2').val();
    }
    if (webLink == '') {
        $('#searchurl').focus();
        return;
    }
    if (webtype == 'back') {
        //$('#preLoadSearch').show();
        /*$('#orderFrm').hide();
        $('#preLoadSearch').show();*/
        preload('show');
    }
    if (webtype == 'front' || webtype == 'front2') {
        $('#itemSearch').modal({
            backdrop: 'static'
        });

        $('#orderFrm').hide();
        $('#preLoadSearch').show();
    }

    $('#noresult').hide();
    /*$.ajax({
    	   type: "GET",
    	   url:  BASEURL+"/assets/API/demo/api.php?item_id=111",
    	   success: function(resp){
    	   		$('#searchBody').html(resp);
    		}
    	 });*/
    var codelink = Base64.encode(webLink);
    $('#pd_option').html('');
    $('#product_price_thb').val('');
    $('#product_price_rmb').val('');
    $('#pd_img').val('');
    $('#product_link').val('');
    $('#img-tag-c').html('');
    $('#shopname').html('');
    $('#shop_name').val('');
    $('#shop_link').val('');
    //alert(codelink);
    $.ajax({
        type: "GET",
        url: BASEURL + "/API/open_API.php?link=" + codelink,
        timeout:5000,
        error:function(){
            preload('hide');
            $('#orderFrm').hide();
            $('#noresult').show('slow');
        },
        datatype: "json",
        success: function(resp) {
            console.log(resp);
          // alert(resp.itemLink);
            if (resp.webApi == 'tmallx' && resp.itemLink!=null) {
                supperJson = resp;
                var dhtml = getDATAHTML(resp);
                /*$.ajax({
					   type: "GET",
					   url:  BASEURL+"/order/loadorder",
					   success: function(resp){
					   		$('#basket_pd').html(resp);
						}
					 });*/
                //alert();
            } else if ((resp.webApi == 'taobao' && resp.itemLink!=null) || (resp.webApi == 'tmall' &&  resp.itemLink!=null)) {
                var dhtml = getDATAHTML_1(resp);
                // supperJson = resp;
                // $.ajax({
                //    type: "GET",
                //    url:  BASEURL+"/API/html.php?a=json",
                //    datatype:"json",
                //    success: function(resp_c){
                //    		//var ojb = JSON.parse(resp_c);
                //     if(resp_c=='' || typeof resp_c == undefined){
                //        preload('hide');
                //        $('#orderFrm').hide();
                // 			$('#noresult').show('slow');
                //     }
                // 		var dhtml = getDATAHTML_1(resp,resp_c);
                // 		/*$.ajax({
                // 		   type: "GET",
                // 		   url:  BASEURL+"/order/loadorder",
                // 		   success: function(resp){
                // 		   		$('#basket_pd').html(resp);
                // 			}
                // 		 });*/

                // 	}
                // });

            } else if (resp.webApi == '1688' && resp.itemLink!=null) {
                supperJson = resp;
                var dhtml = getDATAHTML_2(resp);
                /*$.ajax({
                   type: "GET",
                   url:  BASEURL+"/order/loadorder",
                   success: function(resp){
                   		$('#basket_pd').html(resp);
                	}
                 });*/
                // alert();
            } else {
                //$('#preLoadSearch').hide();
                preload('hide');
                $('#orderFrm').hide();
                $('#noresult').show('slow');
            }
            //$('#searchBody').html(dhtml);
        }
    });

    //  setTimeout(function(){
    //     if(checkdata=='F'){
    //       $('#preLoadSearch').hide();
    //       $('#noresult').show('slow');
    //       checkdata = 'F';
    //       return;
    //
    //     }else{
    //        checkdata = 'F';
    //        return;
    //     }
    //
    //  },10000);
}

function changeImg(imgs) {
    $('#topImg').attr('src', imgs);
    $('#pd_img').val(imgs);
}

function orderNow() {
    if ($('#product_qty').val() <= 0) {
        alert('กรุณาระบุจำนวนสินค้า');
        $('#product_qty').focus();
        return;
    }
    $('#preLoadItem').show();
    $('#basket_pd').hide();
    var f = document.getElementById('orderFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'orderFrm';
    f.action = BASEURL + "/order/additem/",
        f.submit();

}

function setnewItemOrder() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/order/loadorder",
        success: function(resp) {

            $('#preLoadItem').hide();
            $('#basket_pd').show();
            $('#basket_pd').html(resp);

        }
    });
}

function setnewItemOrderMember() {

    window.location.hash = '/buyship/order/?' + new Date().getTime();
}

function orderNow_N() {
   
    //$('#preLoadSearch').show();

    if ($('#product_qty').val() <= 0) {
        alert('กรุณาระบุจำนวนสินค้า');
        $('#product_qty').focus();
        return;
    }
     preload('show');
     var datastring = $("#orderFrm").serialize();
    $.ajax({
        type: "POST",
        url: BASEURL + "/buyship/processFrm/",
        data: datastring,
        success: function(resp) {
             updateCartTemp();
             preload('hide');

        }
    });
    /*
    preload('show');
    //$('#preLoadItem').show();
    //$('#basket_pd').hide();
    var f = document.getElementById('orderFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/buyship/processFrm/",
        f.submit();*/

}

function orderNow_N2() {
    //$('#preLoadSearch').show();

    if ($('#product_qty', '#orderFrm2').val() <= 0) {
        alert('กรุณาระบุจำนวนสินค้า');
        $('#product_qty', '#orderFrm2').focus();
        return;
    }
    if ($('#product_name_null', '#orderFrm2').val() <= 0) {
        alert('กรุณาระบุชื่อสินค้า');
        $('#product_name_null', '#orderFrm2').focus();
        return;
    }
    if ($('#product_link_null', '#orderFrm2').val() <= 0) {
        alert('กรุณาระบุลิ้งสินค้า');
        $('#product_link_null', '#orderFrm2').focus();
        return;
    }
    preload('show');
    //$('#preLoadItem').show();
    //$('#basket_pd').hide();
    var f = document.getElementById('orderFrm2');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/buyship/processFrm/",
        f.submit();

}

function updateTempQTY(key){
    $('#preLoadSearch').show();
    setTimeout(function(){
        var vals = $('#qty_'+key).val();
        var key_c = parseInt(vals);//alert(key_c);
        if(isNaN(key_c)){
            key_c = '1';
        }
        if(key_c==0){
            removeItem(key);
            return;
        }
        
        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({ 
           type: "GET", 
           url:  BASEURL+"/search/updateTempQTY/?key="+key+"&value="+key_c,
           success: function(resp){
                   updateCartTemp();
                   $('#preLoadSearch').hide();
            }
         });
    },100);
    
}
function updateTempComment(elem){
    
        $('#preLoadSearch').show();
        var vals = $(elem).val();
        var key = $(elem).attr('data-key');

        
        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({ 
           type: "GET", 
           url:  BASEURL+"/search/updateTempComment/?key="+key+"&value="+vals,
           success: function(resp){
                   updateCartTemp();
                   $('#preLoadSearch').hide();
            }
         });

    
}


function removeCartTempItem(key){
    $.ajax({ 
           type: "GET", 
           url:  BASEURL+"/API/cart_temp_controller.php?action=del&key="+key,
           success: function(resp){
                   updateCartTemp();
            }
         });
}
function updateCartTemp(){
    
        $.ajax({ 
           type: "GET", 
           url:  BASEURL+"/API/cart_temp.php",
           success: function(resp){
                   $("#cart_temp").html(resp);
            }
         });
    }

function translateAPI(keyname, selector) {
    /*if(keyname=='S' || keyname=='M' || keyname=='L'){
    	$(selector).text(keyname);
    	return;
    }*/
    $.ajax({
        type: "GET",
        url: "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + TRANSKEY + "&lang=zh-th&text=" + keyname,
        datatype: "json",
        success: function(resp) {
            rester = resp.text[0];
            if (rester == "สีของการแบ่งประเภท") {
                rester = 'สี';
            }
            $(selector).text(rester);
        }
    });

}

function translateAPIval(keyname, selector) {
    /*if(keyname=='S' || keyname=='M' || keyname=='L'){
    	$(selector).val(keyname);
    	return;
    }*/
    $.ajax({
        type: "GET",
        url: "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + TRANSKEY + "&lang=zh-th&text=" + keyname,
        datatype: "json",
        success: function(resp) {
            rester = resp.text[0];
            if (rester == "สีของการแบ่งประเภท") {
                rester = 'สี';
            }
            $(selector).val(rester);
        }
    });

}

$(document).ready(function() {
    $('#prop_data').on('click', 'li', function() {
        var datval = $(this).attr('data-value');
        //alert(datval);
        if (typeof datval == "undefined") {
            datval = $(this).attr('data-pv');
        }
        var datamap = datval.split(":");
        //alert(datval);
        if (datamap[0] == '1688') {
            $("ul.cx_" + datamap[1] + " li").each(function(e) {
                $(this).removeClass('active');
            });
            $(this).addClass('active');
            var selector = '#value_' + datamap[1];
            var selector2 = '#value_th_' + datamap[1];
            //alert($(selector).val());
            $(selector).val($('span', this).attr('data-cn'));
            //translateAPIval($(this).text(), selector2);
            $(selector2).val($('span', this).text());


        } else {
            var dtcheck = 'X';
            $("ul.J_TSaleProp li").each(function(e) {
                var datval_n = $(this).attr('data-value');
                //dtcheck = datval_n;
                //alert(datval_n);
                var datamap_n = datval_n.split(":");
                if (datamap_n[0] == datamap[0]) {
                    $(this).removeClass('active');
                }

            });

            $("div.item-sku li").each(function(e) {
                var datval_n = $(this).attr('data-pv');
                var datamap_n = datval_n.split(":");
                //alert(datval_n);
                if (datamap_n[0] == datamap[0]) {
                    $(this).removeClass('active');
                }
                //console.log(datval_n);
            });

            var selector = '#value_' + datamap[0];
            var selector2 = '#value_th_' + datamap[0];
            //translateAPIval($(this).text(),selector);
            $(selector).val($('span', this).attr('data-cn'));

            $(selector2).val($('span', this).text());
            $(this).addClass('active');
            var dataimgs = $('a', this).attr('class');
            if (dataimgs == 'tb-img') {
                var attrstyle = $('a', this).attr('style');
                attrstyle = attrstyle.replace('background:url(', '');
                attrstyle = attrstyle.replace('_30x30.jpg) center no-repeat;', '');
                if (attrstyle != '' && typeof attrstyle != 'undefined') {
                    $('#J_ThumbView').css('opacity', '0.5');
                    $('#J_ThumbView').attr('src', attrstyle);
                    $.ajax({
                        type: "GET",
                        url: attrstyle,
                        success: function(resp) {
                            $('#J_ThumbView').css('opacity', '1');
                            $('#pd_img').val(attrstyle);
                        }
                    });
                }

            }
            var tmimgs = $('img', this).attr('src');
            //alert(tmimgs);
            if (tmimgs != '' && typeof tmimgs != 'undefined') {
                var attrstyle = $('img', this).attr('src');
                attrstyle = attrstyle.replace('_40x40q90.jpg', '');
                //alert(attrstyle);
                if (attrstyle != '' && typeof attrstyle != 'undefined') {
                    $('#J_ImgBooth').css('opacity', '0.5');
                    $('#J_ImgBooth').attr('src', attrstyle);
                    $.ajax({
                        type: "GET",
                        url: attrstyle,
                        success: function(resp) {
                            $('#J_ImgBooth').css('opacity', '1');
                            $('#pd_img').val(attrstyle);
                        }
                    });
                }

            }



        }




    });

});

function delItem(elems) {
    var itemid = $(elems).attr('data-item');
    var oder_id = $(elems).attr('data-ord');
    if (!confirm('คุณต้องการ ลบ รายการนี้หรือไม่')) {
        return;
    }
    $('#orderMsg').modal({
        backdrop: 'static'
    });
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/delitem/?item=" + itemid,
        success: function(resp) {
            window.location.hash = '/buyship/view/' + oder_id + '/?' + new Date().getTime();
        }
    });
}



$(function() {
    $('#payment_slip').bind('change', function() {

        var file = this.files[0];
        var sizeinbytes = this.files[0].size;
        var file_type = file.name.split('.')[file.name.split('.').length - 1].toLowerCase();
        var Size_mb = 1024 * 1024;
        var _filesize = (sizeinbytes / Size_mb).toFixed(2);
        if (_filesize > 2) {
            var fSExt = new Array('Bytes', 'KB', 'MB', 'GB');
            fSize = sizeinbytes;
            i = 0;
            while (fSize > 900) {
                fSize /= 1024;
                i++;
            }
            alert("File Size is " + (Math.round(fSize * 100) / 100) + ' ' + fSExt[i] + "\n\n" + "File size must be less than 2MB.");
            $(this).attr({ value: '' });
            this.value = null;
            return false;

        }
        switch (file_type.toLowerCase()) {
            case 'jpg':
            case 'png':
            case 'gif':
                return true;
        }
        alert("File Type is : ." + file_type + "\nSupport type : .jpg, .png, .gif");
        $(this).attr({ value: '' });
        this.value = null;
        return false;

    });

    $('#img_file').bind('change', function() {

        var file = this.files[0];
        var sizeinbytes = this.files[0].size;
        var file_type = file.name.split('.')[file.name.split('.').length - 1].toLowerCase();
        var Size_mb = 1024 * 1024;
        var _filesize = (sizeinbytes / Size_mb).toFixed(2);
        if (_filesize > 2) {
            var fSExt = new Array('Bytes', 'KB', 'MB', 'GB');
            fSize = sizeinbytes;
            i = 0;
            while (fSize > 900) {
                fSize /= 1024;
                i++;
            }
            alert("File Size is " + (Math.round(fSize * 100) / 100) + ' ' + fSExt[i] + "\n\n" + "File size must be less than 2MB.");
            $(this).attr({ value: '' });
            this.value = null;
            return false;

        }
        switch (file_type.toLowerCase()) {
            case 'jpg':
            case 'png':
            case 'gif':
                return true;
        }
        alert("File Type is : ." + file_type + "\nSupport type : .jpg, .png, .gif");
        $(this).attr({ value: '' });
        this.value = null;
        return false;

    });

});



function validateKeyC(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}


function chengeValues(selele) {
    var datanum = $(selele).val();
    var datakey = $(selele).attr('data-id');
    $('#preLoadItem').show();
    $('#basket_pd').hide();
    $.ajax({
        type: "GET",
        url: BASEURL + "/order/chengeorder/?key=" + datakey + "&val=" + datanum,
        success: function(resp) {

            $('#preLoadItem').hide();
            $('#basket_pd').show();
            $('#basket_pd').html(resp);

        }
    });
}

function chengeDel(selele) {
    var datanum = $(selele).val();
    var datakey = $(selele).attr('data-id');
    $('#preLoadItem').show();
    $('#basket_pd').hide();
    $.ajax({
        type: "GET",
        url: BASEURL + "/order/chengeorderdel/?key=" + datakey,
        success: function(resp) {

            $('#preLoadItem').hide();
            $('#basket_pd').show();
            $('#basket_pd').html(resp);

        }
    });
}

function mergeData() {

    $('#preLoadItem').show();
    $('#basket_pd').hide();
    $.ajax({
        type: "GET",
        url: BASEURL + "/order/mergedata/",
        success: function(resp) {

            window.location = BASEURL + '/order/checkout';

        }
    });
}

function uploadfiletemp(elems) {
    //alert($(elems).val());
    var input = document.getElementById("uploadBtn");
    var fReader = new FileReader();
    fReader.readAsDataURL(input.files[0]);
    fReader.onloadend = function(event) {
        //var img = document.getElementById("yourImgTag");

        //img.src = event.target.result;
        //alert(event.target.result);
        $('#uploadFile').val(event.target.result);
    }

    //$('#uploadFile').val($(elems).val());
    $('.up-text').hide();
    $('#preTemp').show();
    setTimeout(function() {
        var f = document.getElementById('tempFrm');

        f.method = 'post';
        f.enctype = 'multipart/form-data';
        f.target = 'updateFrm';
        f.action = BASEURL + "/buyship/processFrm/",
            f.submit();
        $('#uploadBtn').val('');
    }, 1000);

}

function uploadfiletemp2(elems) {
    //alert($(elems).val());
    var input = document.getElementById("uploadBtn2");
    var fReader = new FileReader();
    fReader.readAsDataURL(input.files[0]);
    fReader.onloadend = function(event) {
        //var img = document.getElementById("yourImgTag");

        //img.src = event.target.result;
        //alert(event.target.result);
        $('#uploadFile').val(event.target.result);
    }

    //$('#uploadFile').val($(elems).val());
    $('.up-text').hide();
    $('#preTemp2').show();
    setTimeout(function() {
        var f = document.getElementById('tempFrm');

        f.method = 'post';
        f.enctype = 'multipart/form-data';
        f.target = 'updateFrm';
        f.action = BASEURL + "/buyship/processFrm/",
            f.submit();
        $('#uploadBtn2').val('');
    }, 1000);

}

function uploadfiletemplist(elems) {
    //alert($(elems).val());
    var input = document.getElementById("uploadBtn3");
    var fReader = new FileReader();
    fReader.readAsDataURL(input.files[0]);
    fReader.onloadend = function(event) {
        //var img = document.getElementById("yourImgTag");

        //img.src = event.target.result;
        //alert(event.target.result);
        $('#uploadFileCC').val(event.target.result);
        //alert($('#uploadFileCC').val());
        var datakey = $(elems).attr('data-key');
        $('#mkey').val(datakey);

        //$('#uploadFile').val($(elems).val());
        $('.up-text23').hide();
        $('#preTemp23').show();
        setTimeout(function() {
            var f = document.getElementById('temp2Frm');

            f.method = 'post';
            f.enctype = 'multipart/form-data';
            f.target = 'updateFrm';
            f.action = BASEURL + "/buyship/processFrm/",
                f.submit();
        }, 1000);
    }

    //$('#uploadBtn2').val('');
}

function resultTemp(rstemp) {
    $('#J_ThumbView').attr('src', rstemp);
    $('#J_ThumbView2').attr('src', rstemp);
    $('#pd_img').val(rstemp);
    $('#pd_img2').val(rstemp);
    $('.up-text').show();
    $('#preTemp').hide();
    $('#uploadBtn').val('');

    $('#preTemp2').hide();
    $('#uploadBtn2').val('');
}

function resultTemp2() {
    window.location.hash = '/buyship/order/?' + new Date().getTime();
}

function changeOrdertype(elems) {
    $('#order_type').val($(elems).attr('data-val'));
    $('li', '.item-sku2').removeClass('active');
    $(elems).addClass('active');
}

function plusClick() {
    var oldnam = $('#product_qty').val();
    oldnam = parseInt(oldnam);
    oldnam = oldnam + 1;
    $('#product_qty').val(oldnam);
}

function minClick() {
    var oldnam = $('#product_qty').val();
    oldnam = parseInt(oldnam);
    oldnam = oldnam - 1;
    if (oldnam <= 1) {
        oldnam = 1;
    }
    $('#product_qty').val(oldnam);
}

function editRmb() {
    var prmb = $('#price_rmb').text();
    $('#newRmb').val(prmb);
    $('#edit_rmb').show();
}


function changeYuan(elem) {
    var price_rmb = $(elem).val();
    var pice_th = price_rmb * cnrate;
    $('#price_th').text(pice_th);
    $('.price_rmb').text(price_rmb);
    $('#product_price_thb').val(pice_th);
    $('#product_price_rmb').val(price_rmb);

    $('#product_price_thb', '#orderFrm2').val(pice_th);
    $('#product_price_rmb', '#orderFrm2').val(price_rmb);
}

function updateOption(elem) {

    $('#preLoadSearch').show();
    var vals = $(elem).val();
    var key = $(elem).attr('data-key');
    var keyn = $(elem).attr('data-keyn');


    /*$('#preLoadSearch2').show();
    $('#myTable').hide();*/
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/updateoption/?key=" + key + "&value=" + vals + "&keyn=" + keyn,
        success: function(resp) {
            window.location.hash = '/buyship/order/?' + new Date().getTime();
        }
    });


}