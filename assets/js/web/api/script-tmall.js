
function getDATAHTML(dataJson){
	var data = '';
	$.ajax({
		   type: "GET",
		   url:  BASEURL+"/API/html.php?a=prop",
		   success: function(resp){
		   
		   		getHTMLTITLE();
		   		getHTMLSHOP();
		   		getHTMLSHOPINFO();
		   		//$('#preLoadSearch').hide();
               preload('hide');
		   		$('#orderFrm').show();
		   		$('#prop_data').html(resp);
		   		$('.tb-btn-buy').remove();
		   		$('.tb-amount').remove();
		   		$().remove();
		   		$('#product_link').val(dataJson.itemLink);
		   		$('#topImg').html(dataJson.dataImg);
		   		$('#J_ImgBooth').attr('width','100%');
		   		//alert(dataJson.dataImg);
		   		var factive = '';
		   		var price_rmb = 0.00;
		   		if(dataJson.skuMap!=null){
					$.each( dataJson.skuMap, function( key, value ) {
					  console.log( key + ": " + value );
					  if(value.stock>=1 && factive==''){
					  	factive = key;
					  	price_rmb = value.price;
					  }
					});
				}


				if(price_rmb==0.00){
					price_rmb = dataJson.noprice;
				}

				var pice_th = price_rmb*cnrate;
				$('#price_th').text(pice_th.toFixed(2));
				$('.price_rmb').text(price_rmb);
				$('#product_price_thb').val(pice_th);
				$('#product_price_rmb').val(price_rmb);

				var datamap = factive.split(";");

				$.each( datamap, function( key, value ) {
					$( "ul.J_TSaleProp li" ).each(function(e) {
					  	var datval = $(this).attr('data-value');
					  	console.log(datval);
					  	if(value==datval){
							$(this).addClass('active');
							var c_data = datval.split(":");
							var str_Input = '<input type="hidden" name="op_name[]" id="name_'+c_data[0]+'" value=""/>';
								str_Input += '<input type="hidden" name="op_name_th[]" id="name_th_'+c_data[0]+'" value=""/>';
                				str_Input +='<input type="hidden" name="op_value[]" id="value_'+c_data[0]+'" value=""/>';
                				str_Input +='<input type="hidden" name="op_value_th[]" id="value_th_'+c_data[0]+'" value=""/>';


                			$('#pd_option').append(str_Input);
                			/*var sp_selector = '#value_'+c_data[0];
                			translateAPIval($(this).text(),sp_selector);*/

                			var sp_selector = '#value_th_'+c_data[0];
                			var sp_selector2 = '#value_'+c_data[0];
                			//alert($(this).text());
                			//$(sp_selector2).attr('data-cn',$('span',this).text());
                			$(sp_selector2).val($('span',this).text());
                			translateAPIval($('span',this).text(),sp_selector);

						}
					});
		   		});

		   		$( "dt.tb-metatit" ).each(function(index) {
				  translateAPI($(this).text(),this);
				});



				$( "ul.J_TSaleProp" ).each(function(index) {
				  	$( "li a span",this).each(function(index) {
				  	  $(this).attr('data-cn',$(this).text());
					  translateAPI($(this).text(),this);
					});
				});

				$( "ul.J_TSaleProp" ).each(function(index) {
				  	$( "li a",this).each(function(index) {
				  	  $(this).attr('href','javascript:;');
					});
				});

				$( "ul.J_TSaleProp" ).each(function(index) {
					var dataprop = $(this).attr('data-property');
					var ks = 'b';
				  	$( "li",this).each(function(index) {
				  		var dt_name = $(this).attr('data-value');
				  		var c_data = dt_name.split(":");

				  		var sp_selector = '#name_th_'+c_data[0];
				  		var sp_selector2 = '#name_'+c_data[0];
				  		if(ks=='b'){
				  			$(sp_selector2).attr('data-cn',dataprop);
							translateAPIval(dataprop,sp_selector);
							$(sp_selector2).val(dataprop);
							ks='c';
						}

					});
				});


		   		$( "ul.tb-img li" ).each(function(index) {
				   var bg = $('a',this).css('background');
			        bg = bg.replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');
			        var kname = $('a span',this).text();
			        var textimg = '<img src="'+bg+'" /><br /><span>'+kname+'</span>';
			        $(this).html(textimg);
			        console.log(bg);
				});
				$( "ul.tb-img li span" ).each(function(index) {
				   translateAPI($(this).text(),this);
				});

				var datImg = $('#J_ImgBooth').attr('src');
				//alert(datImg);
				$('#pd_img').val('http:'+datImg);

            checkdata = 'T';
			}

		 });
	//return data;
}
function getHTMLTITLE(){
	var data = '';
	$.ajax({
		   type: "GET",
		   url:  BASEURL+"/API/html.php?a=title",
		   success: function(resp){
		   		$('#img-tag-c').html(resp);
		   		$('#product_name').val($('#img-tag-c').text());
			}
		 });
}
function getHTMLSHOP(){
	var data = '';
	$.ajax({
		   type: "GET",
		   url:  BASEURL+"/API/html.php?a=shopname",
		   success: function(resp){
		   		if(resp==''){
					$('#shopname').html('ไม่พบชื่อร้าน');
					$('#shop_name').val($('#shopname').text());
			   		$('#shop_link').val($('#shopname').text());
					return;
				}
		   		$('#shopname').html(resp);
		   		$('span.flagship-icon').remove();
		   		$('#shop_name').val($('#shopname').text());
		   		var shoplink = $('a','div.hd-shop-name').attr('href');
		   		$('#shop_link').val('http:'+shoplink);
			}
		 });
}
