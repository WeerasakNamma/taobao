function viewReq(elem) {
    var imgss = $(elem).attr('data-img');
    $('#reqimg').attr('src', imgss);
    $('#reqModal').modal('show');
}

var ddis;
var sstu;

function updatereq(ids, status) {
    ddis = ids;
    sstu = status;
    alertMessage('กรุณายืนยันการบันทึก', 'updatereqCallback');
}

function updatereqCallback() {

    ajaxRequestProcess('shipreq/updateregister/?id=' + ddis + "&status=" + sstu, updatereqSccCallback);
}

function updatereqSccCallback() {
    preload('hide');
    window.location.hash = '/shipreq/?' + new Date().getTime();
}