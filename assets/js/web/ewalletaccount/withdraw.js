var dataidexs = 0;
var datapriceesx = 0;
var comments = '';

function statusWithdraw(elems) {
    dataidexs = $(elems).attr('data-id');
    datapriceesx = $(elems).attr('data-status');
    comments = $('#comment-'+dataidexs).val();
    alertMessage('คุณต้องการบันทึกรายการถอนเงินหรือไม่', 'statusWithdrawCallback');
}

function statusWithdrawCallback() {
    ajaxRequestProcess("ewalletaccount/wdstatus/?id=" + dataidexs + "&status=" + datapriceesx +"&comment="+comments, sccRs);
}

function commentsWithdraw(elems) {
    dataidexs = $(elems).attr('data-id');
    var datacomment = $(elems).val();
    ajaxRequestProcess("ewalletaccount/wdcomment/?id=" + dataidexs + "&comment=" + datacomment, sccRsD);
}

function sccRsD() {
    // window.location.hash = '/ewalletaccount/withdrawal/?' + new Date().getTime();
    preload('hidden');
}

function sccRs() {
    window.location.hash = '/ewalletaccount/withdrawal/?' + new Date().getTime();
}


function uploadfiletemplist(elems) {
    //alert($(elems).val());
    var datakey = $(elems).attr('data-key');
    //alert(datakey);
    var input = document.getElementById("uploadBtn3" + datakey);
    var elemss = $(elems).parents();
    var fReader = new FileReader();
    fReader.readAsDataURL(input.files[0]);
    fReader.onloadend = function(event) {
        //var img = document.getElementById("yourImgTag");

        //img.src = event.target.result;
        //alert(event.target.result);
        $('#uploadFileCC').val(event.target.result);
        //alert($('#uploadFileCC').val());

        $('#mkey').val(datakey);

        //$('#uploadFile').val($(elems).val());
        $('.up-text23', elemss[1]).hide();
        $('#preTemp23', elemss[1]).show();
        setTimeout(function() {
            var f = document.getElementById('temp2Frm');

            f.method = 'post';
            f.enctype = 'multipart/form-data';
            f.target = 'updateFrm';
            f.action = BASEURL + "/ewalletaccount/processFrm/",
                f.submit();
        }, 1000);
    }

    //$('#uploadBtn2').val('');
}


function searchwd() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ewalletaccount/processFrm/",
        f.submit();
}



function resetSearch() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ewalletaccount/resetsearch/",
        success: function(resp) {
            window.location.hash = '/ewalletaccount/withdrawal/?' + new Date().getTime();
        }
    });
}


function viewWd(elem) {
    var imgss = $(elem).attr('data-img');
    $('#wdimg').attr('src', imgss);
    $('#wdModal').modal('show');
}