$(function() {
    $(".my_date").datepicker({
        todayBtn: "linked",
        language: "th",
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy'
    });
    $(".timepicker").timepicker({
        showInputs: false,
        showMeridian: false,
        showSeconds: true
    });
});

function openForm() {
    $('#formopen').slideToggle();
}

function saveWallet() {

    if ($('#n_wallet_desc').val() == '') {
        noticeMessage('กรุณาระบุรายการ', '');
        return;
    }
	if ($('input[name=wallet_type]:checked').val() == 'INOU') {
        if($('#n_wallet').val()<$('#hdn_wallet').val()){
			  noticeMessage('กรุณาตรวจสอบจำนวนเงิน', '');
			  $('#n_wallet').focus();
        		return;
		}
      
    }
    if ($('input[name=wallet_type]:checked').length == 0) {
        // do something here
        noticeMessage('กรุณาเลือกประเภท', '');
        return;
    }
    if ($('input[name=wallet_type]:checked').val() == 'INOU') {
        if ($('#shipping_code').val() == '') {
            noticeMessage('กรุณาระบุเลขที่บิล', '');
            return;
        }
    }

    if ($('#n_wallet').val() == '') {
        noticeMessage('กรุณาระบุจำนวนเงิน', '');
        return;
    }

    if ($('#n_create_dtm').val() == '') {
        noticeMessage('กรุณาระบุวันที่', '');
        return;
    }

    if ($('#n_time_dtm').val() == '') {
        noticeMessage('กรุณาระบุเวลา', '');
        return;
    }

    alertMessage('กรุณายืนยันเพื่อ บันทึก Wallet.', 'saveWalletCallback');
}

function saveWalletCallback() {
    preload('show');
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ewalletaccount/processFrm",
        f.submit();
}

function saveSccCallback() {
    window.location.hash = '/ewalletaccount/view/' + $('#user_id').val() + '/?' + new Date().getTime();
}

function checkbillingdata(useridd) {
    var codename = $('#shipping_code').val();
	
	/*if($('#oldBalance').val()<=0){
		alert("Account Balance มียอดติดลบ กรุณานำเงินเข้าก่อนทำรายการ หักเงินสด");
	}else{*/
	
    if (codename != '') {
    	
	$.ajax({ 
	   type: "GET", 
	   url:  BASEURL+"/ewalletaccount/checkbill/", 
	   dataType:"JSON",
	   data: {
	   	id: useridd,
	   	code: codename,
	   }, 
	   success: function(resp){ //alert(resp); return;
	   
	 	 preload('hidden');
	    if (resp.result == 'T') {
	    	$('#n_wallet').val(resp.values);
	    	$('#hdn_wallet').val(resp.values);
	    	$('#shipid').val(resp.shipid); 
	    	return;
	    } else {
	        alert('ไม่พบรายการบิลที่ต้องการ');
	        $('#shipping_code').val('');
	        $('#shipping_code').focus();
	    $('#hdn_wallet').val(0);
	    	
	    }
	   }
	 });
       /* ajaxRequestProcess("ewalletaccount/checkbill/?id=" + useridd + "&code=" + codename, rsCheckBilling);*/
    }
	
	//}

}

function rsCheckBilling(results) {
    preload('hidden');
    alert();
    if (results.result == 'T') {
    	$('#n_wallet').val(results.values);
    	$('#n_wallet').attr('min',results.values);
        return;
    } else {
        alert('ไม่พบรายการบิลที่ต้องการ');
        $('#shipping_code').val('');
        $('#shipping_code').focus();
        $('#n_wallet').attr('min',0);
    }
}