var orderidxx = '';
var datavaluexx = '';
var datadiff = '';

function checngOrderStatus(elem) {
    orderidxx = $(elem).attr('data-id');
    datavaluexx = $(elem).val();
    alertMessage('กรุณายืนยันการบันทึกรายการสั่งซื้อ', 'checngOrderStatusCallback');
}

function checngOrderStatusActive(elem, tatoldiff) {
    datavaluexx = 'S';
    datadiff = tatoldiff;
    orderidxx = elem;
    //datavaluexx = $(elem).attr('data-value');
    
    alertMessage('กรุณายืนยันการบันทึกรายการสั่งซื้อ', 'checngOrderStatusCallback');
}

function checngOrderStatusButton(elem) {
    orderidxx = $(elem).attr('data-id');
    datavaluexx = $(elem).attr('data-val');
    //alertMessage('Please confirm to Change  Status.','checngOrderStatusCallback');
}

function checngOrderStatusCallback() {
    $('#preLoadSearch').show();

    $.ajax({
        type: "GET",
        url: BASEURL + "/ordervendor/orderstatus/?order_id=" + orderidxx + "&value=" + datavaluexx + "&diff=" + datadiff,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            //updateOrderPrice(orderidxx,datadiff)
            rederOrder(orderidxx);
        }
    });
}
//Weerasak 21-10-2564
// function updateOrderPrice(orderid,price) {
//     $('#preLoadSearch').show();
//     $.ajax({
//         type: "GET",
//         //url: BASEURL + "/ordervendor/orderpay/?order_id=" + orderid + "&value=" + datavalue,
//         url: BASEURL + "/ordervendor/updateOrderPrice/?order_id=" + orderid + "&value="+price,
//         success: function(resp) {
//         }
//     });
// }
//End edit
function itemDel(itemid) {
    if (!confirm('คุณต้องการลบรายการนี้หรือไม่?')) {
        return;
    }
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();

    $.ajax({
        type: "GET",
        url: BASEURL + "/ordervendor/removeitem/?item=" + itemid,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function itemStatus(elem) {

    $('#preLoadSearch').show();
    var itemid = $(elem).attr('data-id');
    var orderid = $('#ordid').val();
    var datavalue = $(elem).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordervendor/itemstatus/?item_id=" + itemid + "&value=" + datavalue,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function itemType(elem) {

    $('#preLoadSearch').show();
    var itemid = $(elem).attr('data-id');
    var orderid = $('#ordid').val();
    var datavalue = $(elem).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordervendor/itemtype/?item_id=" + itemid + "&value=" + datavalue,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function updateCommentOrder(elem) {

    $('#preLoadSearch').show();
    var itemid = $(elem).attr('data-id');
    var orderid = $('#ordid').val();
    var datavalue = $(elem).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordervendor/updatecommentitem/?item_id=" + itemid + "&value=" + datavalue,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function rederOrder(orderid) {
    // $.ajax({
    // 	   type: "GET",
    // 	   url:  BASEURL+"/ordervendor/view2/"+orderid+"/",
    // 	   success: function(resp){
    // 	   		//window.location.hash = '/buyship/view/?'+new Date().getTime();
    // 	   		$('.box-body').html(resp);
    // 	   		$('#preLoadSearch').hide();
    // 		}
    // 	 });
    window.location.hash = '/ordervendor/view/' + orderid + '/?' + new Date().getTime();
}

function updateQTY(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    var cnrate = $('#cnrate').val();
    setTimeout(function() {
        var vals = $(elem).val();

        // if ($(elem).val() == 0) {
        //     $('#preLoadSearch').hide();
        //     return;
        // }

        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/updateqty/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val() + "&price_rmb=" + $(elem).attr('data-rmb') + "&rate=" + cnrate,
            success: function(resp) {
                rederOrder(orderid);
            }
        });
    }, 100);

}

function updateYuan(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    var cnrate = $('#cnrate').val();
    setTimeout(function() {
        var vals = $(elem).val();



        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/updatermb/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val(),
            success: function(resp) {
                rederOrder(orderid);
            }
        });
    }, 100);
}

function updateCnShip(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    var cnrate = $('#cnrate').val();
    setTimeout(function() {
        var vals = $(elem).val();



        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/pricecnship/?id=" + orderid + "&value=" + $(elem).val(),
            success: function(resp) {
                rederOrder(orderid);
            }
        });
    }, 100);
}

function updateBath(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    var cnrate = $('#cnrate').val();
    setTimeout(function() {
        var vals = $(elem).val();



        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/updatethb/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val(),
            success: function(resp) {
                rederOrder(orderid);
            }
        });
    }, 100);
}

function updateSaller(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    setTimeout(function() {
        var vals = $(elem).val();



        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/updatesaller/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val(),
            success: function(resp) {
                rederOrder(orderid);
            }
        });
    }, 100);
}

//Edit by Weerasak 29-10-2564
// function updateDiscountSaller(elem){
//     $('#preLoadSearch').show();
//     var dataID = $(elem).attr('data-id');
//     var order_id = $('#ordid').val();    
//     var cnrate = $('#cnrate').val(); 
//     if(dataID == 0)
//     {
//         var saller_id = $(elem).attr('data-saller');      
//         $.ajax({
//             type: "GET",
//             url: BASEURL + "/ordervendor/addcnship/?order_id=" + order_id + "&saller_id=" + saller_id + "&rate=" + cnrate + "&type=discount&value=" + $(elem).val(),
//             success: function(resp) {
//                 rederOrder(order_id);
//                 $('#preLoadSearch').hide();
//             }
//         });
//     } else {
//         setTimeout(function(){
//             $.ajax({
//                 type: "GET",
//                 url: BASEURL + "/ordervendor/updateDiscountSaller/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val(),
//                 success: function(resp) {
//                     rederOrder(order_id);
//                     $('#preLoadSearch').hide();
//                 }
//             });
//         },100);
//     }
// }

// function updateOrderAmountSaller(elem){
//     $('#preLoadSearch').show();
//     var dataID = $(elem).attr('data-id');
//     var order_id = $('#ordid').val();    
//     var cnrate = $('#cnrate').val();
//     if(dataID == 0)
//     {
//         var saller_id = $(elem).attr('data-saller');      
//         $.ajax({
//             type: "GET",
//             url: BASEURL + "/ordervendor/addcnship/?order_id=" + order_id + "&saller_id=" + saller_id + "&rate=" + cnrate + "&type=amount&value=" + $(elem).val(),
//             success: function(resp) {
//                 rederOrder(order_id);
//                 $('#preLoadSearch').hide();
//             }
//         });
//     } else {
//         setTimeout(function(){
//             $.ajax({
//                 type: "GET",
//                 url: BASEURL + "/ordervendor/updateOrderAmountSaller/?item_id=" + $(elem).attr('data-id') +"&rate=" + cnrate +"&value=" + $(elem).val(),
//                 success: function(resp) {
//                     rederOrder(order_id);
//                     $('#preLoadSearch').hide();
//                 }
//             });
//         },100);
//     }
// }

function updateCNShippingSaller(elem){
    $('#preLoadSearch').show();
    var dataID = $(elem).attr('data-id')
    var order_id = $('#ordid').val(); 
    var cnrate = $('#cnrate').val();
    var price = $('#price'+dataID).val();   
    var ship = $('#shipping'+dataID).val();
    var discount = $('#discount'+dataID).val();
    
    if(dataID == 0)
    {
        var saller_id = $(elem).attr('data-saller');
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/addcnship/?order_id=" + order_id + "&saller_id=" + saller_id + "&rate=" + cnrate + "&value=" + price+"&ship=" + ship +"&discount="+discount,
            success: function(resp) {
                rederOrder(order_id);
                $('#preLoadSearch').hide();
            }
        });
    }else{
        setTimeout(function(){
            $.ajax({
                type: "GET",
                url: BASEURL + "/ordervendor/updateCNShippingSaller/?item_id=" + dataID + "&rate=" + cnrate + "&order_price=" + price+"&ship=" + ship +"&discount="+discount,
                success: function(resp) {
                    rederOrder(order_id);
                    $('#preLoadSearch').hide();
                }
            });
        },100);
    }
}

function jumpto(h){
    var top = document.getElementById(h).offsetTop;
    window.scrollTo(0, top);
}
//End Edit
function addItem() {
    $('#preLoadSearch').show();
    var f = document.getElementById('itemFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordervendor/processFrm/",
        f.submit();
}

function exportExcel() {

    alertMessage('กรุณายืนยันการส่งใบสั่งซื้อ', 'exportExcelCalback');
}

function confirmOrder(orderId, tatoldiff,isstatus){
    $('#preLoadSearch').show();
    var order_status = 'S';
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/orderstatus/?order_id=" + orderId + "&value=" + order_status + "&diff=" + tatoldiff,
            success: function(resp) {                
                rederOrder(orderId);
                $('#preLoadSearch').hide();
            }
        });
}

function exportExcelCalback() {
    // if(!confirm('Please connfirm to export.')){
    // 	return;
    // }
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordervendor/processFrm/",
        f.submit();
}

function setFilepath(ddd) {
    $('#filepath').show(ddd);
    $('#filepath').attr('href', ddd);
    $('#preLoadSearch').hide();
}


function updateCnShip(elsem) {
    var dataID = $(elsem).attr('data-id');
    var shipval = $(elsem).val();
    var cnrate = $('#cnrate').val();
    var order_id = $(elsem).attr('data-order');
    if (dataID == 0) {

        var saller_id = $(elsem).attr('data-saller');


        $('#preLoadSearch').show();
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/addcnship/?order_id=" + order_id + "&saller_id=" + saller_id + "&rate=" + cnrate + "&value=" + shipval,
            success: function(resp) {
                rederOrder(order_id);
                $('#preLoadSearch').hide();
            }
        });
    } else {
        $('#preLoadSearch').show();
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordervendor/updatecnship/?id=" + dataID + "&value=" + shipval + "&rate=" + cnrate,
            success: function(resp) {
                rederOrder(order_id);
                $('#preLoadSearch').hide();
            }
        });
    }
}

var supperid = '';

function uploadfiletempCallBack() {
    // $('.up-text').show();
    // $('.fa-spin').hide();
    // $('#uploadFile').val('');
    // $('#pd_img').val(pathname);
    // $('#topImg img').attr('data-src',pathname);
    // $('#topImg img').attr('src',pathname);
    window.location.hash = '/ordervendor/view/' + supperid + '/?' + new Date().getTime();
}

function saveImagSaller(oderid, salldes) {
    // if(!confirm('Please connfirm to export.')){
    // 	return;
    // }
    supperid = oderid;
    $('#preLoadSearch').show();
    var f = document.getElementById('saller_' + salldes);

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}