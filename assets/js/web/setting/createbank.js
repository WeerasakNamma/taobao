function saveBank() {

    alertMessage('Please confirm to save Bank.', 'saveBankCallback');
}

function saveBankCallback() {
    preload('show');
    var f = document.getElementById('bankFrm');
    //alert(f); 
    f.method = 'post';
    f.target = 'updateFrm';
    f.action = BASEURL + "/setting/processFrm",
        f.submit();
}

function successCallBack() {
    preload('hide');
    window.location.hash = '/setting/banklist/?' + new Date().getTime();
}