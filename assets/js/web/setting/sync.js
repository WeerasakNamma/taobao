function synctcctoserver() {
    preload('show');
    $.ajax({
        type: "GET",
        url: BASEURL + "/webservice/tcc_sync.php?service=syncall",
        success: function(resp) {
            preload('hidden');
            window.location.hash = '/setting/syncdata/?' + new Date().getTime();
        }
    });
}

function syncservertotcc() {
    preload('show');
    $.ajax({
        type: "GET",
        url: BASEURL + "/webservice/tcc_sync.php?service=syncserver",
        success: function(resp) {
            preload('hidden');
            window.location.hash = '/setting/syncdata/?' + new Date().getTime();
        }
    });
}

function syncservertotccbill() {
    preload('show');
    $.ajax({
        type: "GET",
        url: BASEURL + "/webservice/tcc_sync.php?service=syncserver_bill",
        success: function(resp) {
            preload('hidden');
            window.location.hash = '/setting/syncdata/?' + new Date().getTime();
        }
    });
}

function syncbytoken(tokenid) {
    preload('show');
    $.ajax({
        type: "GET",
        url: BASEURL + "/webservice/tcc_sync.php?service=synctoken&service_token=" + tokenid,
        success: function(resp) {
            preload('hidden');
            window.location.hash = '/setting/syncdata/?' + new Date().getTime();
        }
    });
}