$(function () {
		
    	
        
        $('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });
      

		
});

function saveMasterGroup(){
	alertMessage('Please confirm to save master data group.','saveMasterGroupCallback');
}

function saveMasterGroupCallback(){
	
	

	preload('show');
	var f = document.getElementById('createFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/setting/masterdata/';
}

var dataids='0';
function deleteMasterGroup(myojb){
	dataids = $(myojb).attr('data-id');
	alertMessage('Please confirm to delete master group.','deletePMasterGroupFunc');
}
function deletePMasterGroupFunc(){
	if(dataids!='0'){
		ajaxRequestProcess('setting/deletemastergroup/'+dataids,deleteMSGCallback);
	}else{
		return;
	}
}
function deleteMSGCallback(){
	window.location.hash = '/setting/masterdata/?'+new Date().getTime();
}


var g_ids = 0;
function saveMasterData(n_g_id){
	g_ids = n_g_id;
	alertMessage('Please confirm to save master data.','saveMasterdataCallback');
}

function saveMasterdataCallback(){
	
	

	preload('show');
	var f = document.getElementById('createFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}
function successMasterCallBack(){
	preload('hide');
	window.location.hash = '/setting/masterlist/'+g_ids;
}


function deleteMasterdata(myojb){
	dataids = $(myojb).attr('data-id');
	g_ids = $(myojb).attr('data-gid');
	alertMessage('Please confirm to delete master data.','deletePMasterdataFunc');
}
function deletePMasterdataFunc(){
	if(dataids!='0'){
		ajaxRequestProcess('setting/deletemasterdata/'+dataids,deleteMSCallback);
	}else{
		return;
	}
}
function deleteMSCallback(){
	window.location.hash = '/setting/masterlist/'+g_ids+'/?'+new Date().getTime();
}