$(function () {
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });
});

function saveGroup(){
	alertMessage('Please confirm to create Group.','saveGroupCallback');
}

function saveGroupCallback(){
	
	/*if($('#meeting_title').val()==''){
		noticeMessage('Please Enter Meeting Title');
		$('#meeting_title').focus();
		return;
	}*/
	

	preload('show');
	var f = document.getElementById('createFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/setting/permission/';
}

function saveRate(){
	alertMessage('Please confirm to save rate.','saveRateCallback');
}

function usersysRate(){
	alertMessage('Please confirm to Sync rate.','syncratetoserver');
}

function syncratetoserver() {
    preload('show');
	var gId = $("#vuid").val();
$.ajax({
        type: "POST",
        url: BASEURL+"/setting/processFrm",
		data: {gId:gId,mode:"updaterate"},
        success: function(resp) {
			/*Edit by Weerasak*/
			$.ajax({
				type: "GET",
				url: BASEURL + "/webservice/sync_userrate.php?token=SYNC_MEMBER_RATE&gId="+gId,
				success: function(resp) {
            		preload('hidden');
				}
			});	
		}
    });
	

}


function saveRateCallback(){
	preload('show');
	var f = document.getElementById('createFrm2'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}


function saveRateSccCallback(){
	window.location.hash = '/setting/editgroup/'+$('#vuid').val()+'/?'+new Date().getTime();
}

var rrtid=0;

function dalrate(vrate){
	rrtid = vrate;
	alertMessage('Please confirm to dalete rate.','dalrateCallback');
}
function dalrateCallback(){
	
	ajaxRequestProcess('setting/delrate/?id='+rrtid,saveRateSccCallback);
}