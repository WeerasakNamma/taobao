$(function () {
		
    	
        
        $('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });
      
	
});


function clonefn (){

	$("#text_clone","#stm_clone_b").clone().appendTo('#stm_clone_a');
					
}
function removet(selects){
	var mybuttn = $(selects).parents();
	$(mybuttn[1]).remove();
}
function saveSprint(){
	if($('#template_name').val()==''){
		noticeMessage('Please Enter Sprint template Name','scriptFocus("#template_name");');
		return false;
	}
	if($('#estime_name').val()==''){
		noticeMessage('Please Enter Sprint Estime Name','scriptFocus("#estime_name");');
		return false;
	}
	if($('#task_name').val()==''){
		noticeMessage('Please Enter Sprint Task Name','scriptFocus("#task_name");');
		return false;
	}
	alertMessage('Please confirm to save Sprint Template.','saveSprintCallback');
}
function saveSprintCallback(){
	
	preload('show');
	var f = document.getElementById('Frm_spinttemplete');
	//alert(f); 
	f.method = 'post';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/setting/sprinttemplate/';
}
var dataids=0;
function deleteSprintTemplateData(myojb){
	dataids = $(myojb).attr('data-id');
	alertMessage('Please confirm to delete Sprint Template data.','deleteSprintTemplateDataFunc');
}
function deleteSprintTemplateDataFunc(){
	if(dataids!='0'){
		ajaxRequestProcess('setting/delsprinttemplate/'+dataids+'/',deleteSTCallback);
		
	}else{
		return;
	}
}
function deleteSTCallback(){
	window.location.hash = '/setting/sprinttemplate/?'+new Date().getTime();
}