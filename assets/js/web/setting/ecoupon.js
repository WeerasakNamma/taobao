$(function(){
	$('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });
      $(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy'
      });
	
});
function saveEcoupon(){
	if($('#ecoupon_number').val()==''){
		noticeMessage('Please Enter E-Coupon No.','scriptFocus("#ecoupon_number");');
		return false;
	}
	var eno = $('#ecoupon_number').val();
	/*$.ajax({
            type: "POST",
            data:{
        		mode:"checkecoupon",
        		e_no:eno
            	 },
           	url: BASEURL + "/setting/processFrm/",
            success: function(rsp) {
            	if(rsp=="T"){
					noticeMessage('Invalid E-Coupon No.','scriptFocus("#ecoupon_number");');
					return false;
				}
            	
            }
           }); */
	if($('#ecoupon_amount').val()==''){
		noticeMessage('Please Enter E-Coupon Amount','scriptFocus("#ecoupon_amount");');
		return false;
	}
	alertMessage('Please confirm to save E-Coupon.','saveEcouponCallback');
}
function saveEcouponCallback(){
	preload('show');
	var f = document.getElementById('ecouponFrm');
	//alert(f); 
	f.method = 'post';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/setting/ecoupon/?'+new Date().getTime();
}
var delId = 0;
function delEcoupon(oj){
	var btnoj = $(oj).parents();
	var id =  $(oj).attr("data-id");
	delId = id;
	alertMessage('Please confirm to Delete E-Coupon.','delEcouponCallback');
}
function delEcouponCallback(){
	preload('show');
	$.ajax({
            type: "POST",
            data:{
        		mode:"delecoupon",
        		e_id:delId
            	 },
           	url: BASEURL + "/setting/processFrm/",
            success: function(rsp) {
            	if(rsp=="T"){
					preload('hide');
					window.location.hash = '/setting/ecoupon/?'+new Date().getTime();
				}else{
					preload('hide');
				}
            }
	  });
}




