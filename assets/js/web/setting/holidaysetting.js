$(function () {

	$(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy' 
      });
      
        
       
	
});

function saveHoliday(){
	if($('#holiday_name').val()==''){
		noticeMessage('Please Enter Holiday Name','scriptFocus("#holiday_name");');
		return false;
	}
	if($('#holiday_date').val()==''){
		noticeMessage('Please Enter Date','scriptFocus("#holiday_date");');
		return false;
	}
	alertMessage('Please confirm to save Holiday Setting.','saveHolidayCallback');
}
function saveHolidayCallback(){
	
	preload('show');
	var f = document.getElementById('Frm_holidaysetting');
	//alert(f); 
	f.method = 'post';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/setting/holiday/?'+new Date().getTime();
}
var dataids=0;
function deleteHolidaySetting(myojb){
	dataids = $(myojb).attr('data-id');
	alertMessage('Please confirm to delete Holiday Setting.','deleteHolidaySettingFunc');
}
function deleteHolidaySettingFunc(){
	if(dataids!='0'){
		ajaxRequestProcess('setting/delholiday/'+dataids+'/',deleteHSCallback);
		
	}else{
		return;
	}
}
function deleteHSCallback(){
	window.location.hash = '/setting/holiday/?'+new Date().getTime();
}

function searchHolidaySetting(){

	preload('show');
	var f = document.getElementById('createFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}

function clearHolidaySetting(){

	preload('show');
	var f = document.getElementById('clearFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/setting/processFrm", 
	f.submit();
}