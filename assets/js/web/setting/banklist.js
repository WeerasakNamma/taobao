$(function() {



    $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false
    });



});

var dataids = '0';

function deleteBank(myojb) {
    dataids = $(myojb).attr('data-id');
    alertMessage('Please confirm to delete bank.', 'deleteBankFunc');
}

function deleteBankFunc() {
    if (dataids != '0') {
        ajaxRequestProcess('setting/deletebank/' + dataids, deleteBankCallback);
    } else {
        return;
    }
}

function deleteBankCallback() {
    //location.reload();

    window.location.hash = '/setting/banklist/?' + new Date().getTime();
}