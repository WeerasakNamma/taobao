$(function() {
    preload('hide');

});
var mailcalback = '0';

function registerfrm() {
    if ($('#email-register').val() == '') {
        noticeMessage('กรุณาระบุ E-Mail [User Login].', 'scriptFocus("#email-register")');
        return;
    }
    if (!IsEmail($('#email-register').val())) {
        noticeMessage('รูปแบบ E-Mail ไม่ถูกต้อง', 'scriptFocus("#email-register")');
        return;
    }
    var email = $('#email-register').val();
    mailcalback = email;
    $.ajax({
        type: "GET",
        url: BASEURL + "/register/checkRegister/",
        data: {
            name: email
        },
        success: function(resp) {
            if (resp == "F") {
                noticeMessage('E-mail นี้มีในระบบแล้ว', 'scriptFocus("#email-register")');
                return false;
            } else {
                if ($('#pass-register').val() == '') {
                    noticeMessage('กรุณาระบุรหัสผ่าน', 'scriptFocus("#pass-register")');
                    return;
                }
                if ($('#repass-register').val() == '') {
                    noticeMessage('กรุณายืนยันรหัสผ่านอีกครั้ง', 'scriptFocus("#repass-register")');
                    return;
                }
                if ($('#pass-register').val() != $('#repass-register').val()) {
                    $('#pass-register').val('');
                    $('#repass-register').val('');
                    noticeMessage('รหัสผ่านไม่ตรงกัน', 'scriptFocus("#pass-register")');
                    return;
                }
                if ($('#name-register').val() == '') {
                    noticeMessage('กรุณาระบุ \' ชื่อ - นามสกุล \'', 'scriptFocus("#name-register")');
                    return;
                }
                if ($('#nickname-register').val() == '') {
                    noticeMessage('กรุณาระบุ \' ชื่อเล่น \'', 'scriptFocus("#nickname-register")');
                    return;
                }
                if ($('#tel-register').val() == '') {
                    noticeMessage('กรุณาระบุ \' เบอร์โทร \'', 'scriptFocus("#tel-register")');
                    return;
                }
                if ($('#tel-register').val() == '') {
                    noticeMessage('กรุณาระบุ \' เบอร์โทร \'', 'scriptFocus("#tel-register")');
                    return;
                }
                if ($('#birth-date').val() == '') {
                    noticeMessage('กรุณาระบุ \' วันเกิด \'', 'scriptFocus("#birth-date")');
                    return;
                }
                if ($('#adress-register').val() == '') {
                    noticeMessage('กรุณาระบุ \' ที่อยู่ \'', 'scriptFocus("#adress-register")');
                    return;
                }
                if ($('#province-register').val() == '') {
                    noticeMessage('กรุณาระบุ \' จังหวัด \'', 'scriptFocus("#province-register")');
                    return;
                }
                if ($('#post-register').val() == '') {
                    noticeMessage('กรุณาระบุ \' รหัสไปรษณีย์ \'', 'scriptFocus("#post-register")');
                    return;
                }

                if (!document.getElementById('term-register').checked) {
                    noticeMessage('กรุณายอมรับข้อมตกลง', 'scriptFocus("#term-register")');
                    return;
                }

                alertMessage('กรุณายืนยันข้อมมูลการสมัคร', 'registerFunc');
            }
        }
    });
}

function registerFunc() {
    preload('show');
    var f = document.getElementById('registerFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/register/registerMember/",
        f.submit();
}

function registerMemberCallback() {
    preload('hide');
    $('#registerFrm').slideToggle();
    $('#succes_frm').slideToggle();
    $('#user_email_tx').text(mailcalback);
}

function changeProvince(elems) {
    var province_code = $(elems).val();
    if (province_code == '') {
        return;
    }
    $.ajax({
        type: "GET",
        url: BASEURL + "/register/amphur/?province=" + province_code,
        success: function(resp) {
            $('#amphur').html(resp);
            $('#amphur').show();
            $('#district').hide();
        }
    });
}

function changeAmphur(elems) {
    var amphur_code = $(elems).val();
    if (amphur_code == '') {
        return;
    }
    $.ajax({
        type: "GET",
        url: BASEURL + "/register/district/?amphur=" + amphur_code,
        success: function(resp) {
            $('#district').html(resp);
            $('#district').show();
        }
    });
}