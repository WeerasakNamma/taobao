function saveTracking() {
    if (!confirm('กรุณายืนยันการบันทึก tracking.')) {
        return;
    }
    if ($('#codecheck').val() == '0') {
        alert('กรุณากรอก Member Code ให้ถูกต้อง');
        $('#codecheck').focus();
        return;
    }
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function successCallBack() {
    window.location.hash = '/ordermanagement/tracking/?' + new Date().getTime();
}

function cehcekmembercode() {
    var dtsc = $('#codecheck').val();
    if (dtsc == '0') {
        alert('กรุณากรอก Member Code ให้ถูกต้อง');
        $('#member_code').focus();
        return;
    }
}

function checkmemberinfo(elems) {
    var dataVal = $(elems).val();

    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: BASEURL + "/ordermanagement/checkmembercode/?code=" + dataVal,
        success: function(resp) {

        	//alert(resp.code);
            
            if (resp.code == "T") {

                $('#codecheck').val('1');
            } else {
                $('#codecheck').val('0');
                alert('กรุณากรอก Member Code ให้ถูกต้อง');
                $('#member_code').focus();
                return;
            }
        }
    });
}

function calDimention() {
    var dimenx = $('#dime_x').val();
    var dimeny = $('#dime_y').val();
    var dimenw = $('#dime_w').val();

    var membercode = $('#member_code').val();
    var order_type = $('#order_type').val();

    if (dimenx != '' && dimeny != '' && dimenw != '' && order_type != '' && membercode != '') {
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/ratebyorder/?ship_by=4&type=" + order_type + "&member=" + membercode,
            success: function(resp) {
                console.log(resp);
                obj = JSON.parse(resp);
                var ttdimen = (dimenx * dimeny * dimenw) / 1000000;
                var tprice = ttdimen * obj.que;
                $('#price_dimen').val(tprice);
                $('#cbm').val(ttdimen);
            }
        });
    }
}

function calWeight() {
    var membercode = $('#member_code').val();
    var order_type = $('#order_type').val();

    var tweight = $('#weight').val();
    if (tweight != '' && order_type != '' && membercode != '') {
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/ratebyorder/?ship_by=4&type=" + order_type + "&member=" + membercode,
            success: function(resp) {
                console.log(resp);
                obj = JSON.parse(resp);
                var tprice = tweight * obj.kilo;
                $('#price_weight').val(tprice);
            }
        });
    }
}
// Add By Weerasak 28/02/2565
function getTracking(){
    var track = $('#inputTrack').val().trim();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/searchTracking/?tracking=" + track,
        success: function(resp) {            
            console.log(resp);
            var result = JSON.parse(resp);
             if(result.length !== 0)
            {
                window.location.hash = '/ordermanagement/viewtracking/' + result.tracking_id;
            }
            else
            {
                $('#trackingno').val(track);
            }
        }
    });
}