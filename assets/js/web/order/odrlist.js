$(document).ready(function() {
    //alert(dashdata);
    $('.datepickerX').datepicker({
        autoclose: true,
        dateFormat: 'dd-mm-yyyy'
    });
});


function searchodr() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function reorderList() {
    window.location.hash = '/ordermanagement/orderlist/?' + new Date().getTime();
}

function resetSearch() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetsearch/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/orderlist/?' + new Date().getTime();
        }
    });
}


var ddis = 0;
var valuess = 0;

function refundWalletOrder(elems) {
    ddis = $(elems).attr('data-id');
    valuess = $(elems).attr('data-price');
    alertMessage('กรุณายืนยันการคืนเงินเข้า E Wallet รายการนี้', 'refundWalletOrderCallback');
}

function refundWalletOrderCallback() {
    ajaxRequestProcess('ordermanagement/refundbeyorder/' + ddis + '/?value=' + valuess, reorderList);
}

var ddis2 = 0;
var valuess2 = 0;

function changerTrueRate(elems) {
    ddis2 = $(elems).attr('data-id');
    valuess2 = $(elems).val();
    alertMessage('คุณต้องการเปลี่ยนแปลงเรทไช่หรือไม่?', 'changerTrueRateCallback');
}

function changerTrueRateCallback() {
    ajaxRequestProcess('ordermanagement/changetruerate/?rate=' + valuess2 + "&order=" + ddis2, reorderList);
}