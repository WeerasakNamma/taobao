$(document).ready(function() {
    //alert(dashdata);
    $('.datepickerX').datepicker({
        autoclose: true,
        dateFormat: 'dd-mm-yyyy'
    });
});


function searchodr() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function searchodrChinaUser() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function reorderList() {
    window.location.hash = '/ordermanagement/chinaorder/?' + new Date().getTime();
}

function reorderUserList() {
    window.location.hash = '/ordermanagement/chinaorderuser/?' + new Date().getTime();
}

function resetSearch() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetsearch2/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/chinaorder/?' + new Date().getTime();
        }
    });
}

function resetSearchChinaUser() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetsearchchinauser/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/chinaorderuser/?' + new Date().getTime();
        }
    });
}

function sendOrderToCN() {
    var params = $('#createFrm').serialize();
    $.ajax({
        type: "POST",
        data: params,
        url: BASEURL + "/ordermanagement/chinaorderview/",
        success: function(rsp) {
            alertMessageHtml(rsp, 'sendOrderToCNCallback');
        }
    });

}

function sendOrderToCNCallback() {
    preload('show');
    var ratedata = $('#rate_data').val();
    if (ratedata == '') {
        $('#rate_data').focus();
        return;
    }

    $('#ratesend').val(ratedata);
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function calrates() {
    var rate_data = $('#rate_data').val();
    var id_yuan = $('#id_yuan').val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/chinaordercal/?rate=" + rate_data + "&value=" + id_yuan,
        success: function(resp) {
            $('#ttbs').text(resp);
        }
    });
}

$('#select-all').click(function(event) {
    if (this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
        var params = $('#checkFrm').serialize();
        $.ajax({
            type: "POST",
            data: params,
            url: BASEURL + "/ordermanagement/setforchinaorder/",
            success: function(rsp) {

            }
        });

    } else {
        var params = $('#checkFrm').serialize();
        $.ajax({
            type: "POST",
            data: params,
            url: BASEURL + "/ordermanagement/unsetforchinaorder/",
            success: function(rsp) {
                $(':checkbox').each(function() {
                    this.checked = false;
                });
            }
        });

    }
});

$('.check_chinaorder').click(function(event) {
    if (this.checked) {
        var dataid = $(this).val();
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/setforchinaorder_id/?value=" + dataid,
            success: function(rsp) {

            }
        });

    } else {
        var dataid = $(this).val();
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/unsetforchinaorder_id/?value=" + dataid,
            success: function(rsp) {
                $(':checkbox').each(function() {
                    this.checked = false;
                });
            }
        });

    }
});


function sendOrderToCNUser() {
    if (!confirm('กรุณายืนยันบันทึกรายการ')) { return; }
    preload('show');

    var f = document.getElementById('checkFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}