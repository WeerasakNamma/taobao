$('#select-all').click(function(event) {
    if (this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
    } else {
        $(':checkbox').each(function() {
            this.checked = false;
        });
    }
});

function saveTrackingToship() {
    alertMessage('กรุณายืนยันการบันทึก Tracking.', 'saveTrackingCallback');
}

function saveTrackingCallback() {
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function successCallBack() {
    window.location.hash = '/ordermanagement/trackingok/?' + new Date().getTime();
}