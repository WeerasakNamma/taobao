$(document).ready(function() {
    //alert(dashdata);
    $('.datepickerX').datepicker({
        autoclose: true,
        dateFormat: 'dd-mm-yyyy'
    });
});

function sendOrderNotrack() {

    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/sendmailordernotrack/",
        success: function(rsp) {
            alertMessage('ส่งรายการเรียบร้อยแล้ว', '');
        }
    });

}

function searchodr() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}
function searchodrSe() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}
function setnotrack(orderid, saller) {
    if (!confirm('กรุณายืนยันการยกเลิกรายการ')) {
        return;
    }
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/setnotrack/?order=" + orderid + "&saller=" + saller,
        success: function(rsp) {
            window.location.hash = '/ordermanagement/ordernotrackinglist/?' + new Date().getTime();
        }
    });

}


function saveNotrackComment(elems) {
    var dataid = $(elems).attr('data-id');
    var datasaller = $(elems).attr('data-saller');
    var datacommetn = $(elems).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/notrackcomment/?order=" + dataid + "&saller=" + datasaller + "&comment=" + datacommetn,
        success: function(rsp) {
            window.location.hash = '/ordermanagement/ordernotrackinglist/?' + new Date().getTime();
        }
    });

}

function saveNotrackComment2(elems) {
    var dataid = $(elems).attr('data-id');
    var datasaller = $(elems).attr('data-saller');
    var datacommetn = $(elems).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/notrackcomment/?order=" + dataid + "&saller=" + datasaller + "&comment=" + datacommetn,
        success: function(rsp) {
            window.location.hash = '/ordermanagement/ordernotracking_send/?' + new Date().getTime();
        }
    });

}

function reorderList() {
    window.location.hash = '/ordermanagement/ordernotrackinglist/?' + new Date().getTime();
}

function reorderListSe() {
    window.location.hash = '/ordermanagement/ordernotracking_send/?' + new Date().getTime();
}
function resetSearch() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetsearchno/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/ordernotrackinglist/?' + new Date().getTime();
        }
    });
}
function resetSearchSe() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetsearchnose/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/ordernotracking_send/?' + new Date().getTime();
        }
    });
}