function saveGetshipping() {
    alertMessage('คุณต้องการบันทึกบิลนี้หรือไม่?', 'saveGetshippingCallback');
}

function saveGetshippingCallback() {
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function successShipCallBack() {
    window.location.hash = '/ordermanagement/shippinglist/?' + new Date().getTime();
}

// Edit By Weerasak 25-01-2566
function updateServiceShipping(elem) {
    $('#preLoadSearch').show();
    var ship_id = $(elem).attr('data-id');
    var ship_price = $('#ship_price').val();
    var service_price = $('#service_price').val();
    var other_price = $('#other_price').val();

    if(ship_price.length == 0)
    {
        document.getElementById("ship_price").value = "0.00";
    }else{
        document.getElementById("ship_price").value = parseFloat(ship_price).toFixed(2);
    }
    if(service_price.length == 0)
    {
        document.getElementById("service_price").value = "0.00";
    }else{
        document.getElementById("service_price").value = parseFloat(service_price).toFixed(2);
    }
    if(other_price.length == 0)
    {
        document.getElementById("other_price").value = "0.00";
    }else{
        document.getElementById("other_price").value = parseFloat(other_price).toFixed(2);
    }

    setTimeout(function() {
        var vals = $(elem).val();
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/updateServiceShippingPrice/?shipping_id=" + ship_id +"&ship_price="+ship_price+"&service_price="+service_price+"&other_price="+other_price,
            success: function(resp) {
                $('#preLoadSearch').hide();
            }
        });
    }, 100);
}
// END
