function changeDDDTS() {
    var itmqty = $('#item_qty').text();
    var itmddt = $('#ref_refund').val();
    var ttds = itmqty - itmddt;

    $('#tt_dss').text(ttds);
}

function showItemRefundInfo(elems) {
    itemid = $(elems).attr('data-id');
    preload('show');
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/refundinfo/" + itemid,
        success: function(resp) {
            console.log(resp);
            var newdata = JSON.parse(resp);
            $('#item_id').val(newdata['item_info'].id);
            $('#itm_name').text(newdata['item_info'].product_title);
            $('#item_qty').text(newdata['item_info'].product_buy_qty); 


            $('#ref_id').val(newdata.ref_id);
            $('#ref_rmb').val(newdata.ref_rmb);
            $('#ref_rate').val(newdata['order_info'].rate);
            // if (newdata.ref_rate == '0' || newdata.ref_rate == '') {
            //     $('#ref_rate').val(newdata['order_info'].rate);
            // } else {
            //     $('#ref_rate').val(newdata.ref_rate);
            // }

            $('#status').val(newdata.status);
            $('#ref_admin').val(newdata.ref_admin);

            var ref_img = BASEURL + "/uploads/refund/" + newdata.ref_file2;
            $('#ref_img').attr('src', ref_img);

            var ttds = newdata['item_info'].product_buy_qty - newdata.ref_refund;

            $('#ref_refund').val(ttds);

            $('#tt_all').val(newdata['item_info'].product_buy_qty);

            $('#img_pd').attr("src", newdata['item_info'].img_path);
            preload('hide');
            $('#refundModal').modal('show');
        }
    });
    //  $('#refundModal').modal('show');
}

function showItemRefundInfo2(elems) {
    itemid = $(elems).attr('data-id');
    preload('show');
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/refundinfo/" + itemid,
        success: function(resp) {
            console.log(resp);
            var newdata = JSON.parse(resp);
            $('#item_id2').text(newdata['item_info'].id);
            $('#itm_name2').text(newdata['item_info'].product_title);
            $('#item_qty2').text(newdata['item_info'].product_buy_qty);


            $('#ref_id2').val(newdata.ref_id);
            $('#ref_rmb2').text(newdata.ref_rmb);
            $('#ref_rate2').text(newdata['order_info'].rate);
            // if (newdata.ref_rate == '0' || newdata.ref_rate == '') {
            //     $('#ref_rate').val(newdata['order_info'].rate);
            // } else {
            //     $('#ref_rate').val(newdata.ref_rate);
            // }

            $('#status2').val(newdata.status);
            // alert(newdata.status);
            $('#ref_admin2').text(newdata.ref_admin);

            var ref_img = BASEURL + "/uploads/refund/" + newdata.ref_file2;
            $('#ref_img2').attr('src', ref_img);

            var ttds = newdata['item_info'].product_buy_qty - newdata.ref_refund;

            $('#ref_refund2').text(ttds);

            $('#tt_all2').text(newdata['item_info'].product_buy_qty);

            $('#img_pd2').attr("src", newdata['item_info'].img_path);
            preload('hide');
            $('#refund2Modal').modal('show');
        }
    });
    //  $('#refundModal').modal('show');
}
var ddis = 0;

function refundWallet(dataid) {
    ddis = dataid;
    alertMessage('กรุณายืนยันการแจ้งถอนเงิน', 'refundWalletCallback');
}

function refundWalletCallback() {    
    ajaxRequestProcess('ordermanagement/refundbeyref/' + ddis + '/', successCallBack);
}

function successCallBack() {
    window.location.hash = '/ordermanagement/refundlist/?' + new Date().getTime();
}

function saveRefund() {
    alertMessage('กรุณายืนยันการคืนเงิน', 'saveRefundCallback');
}

function saveRefundCallback() {

    preload('show');
    var f = document.getElementById('refundFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm",
        f.submit();
    $('#refundModal').modal('hide');
    preload('hide');
}

var dataidexs = 0;
var datapriceesx = 0;

function cancelRefund(elems) {
    dataidexs = $(elems).attr('data-id');
    datapriceesx = $(elems).attr('data-item');
    alertMessage('คุณต้องการยกเลิกรายการหรือไม่?', 'cancelRefundCallback');
}

function cancelRefundCallback() {
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/cancelrefund/?ref_id=" + dataidexs + "&item_id=" + datapriceesx,
        success: function(resp) {
            window.location.hash = '/ordermanagement/refundlist/?' + new Date().getTime();
        }
    });
}

function exportRefund() {
    alertMessage('กรุณายืนยันการ ส่งออก รายการขอคืนสินค้า', 'exportRefundCallback');
}

function exportRefundCallback() {
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/refundexport/",
        success: function(resp) {
            $('#preLoadSearch').hide();
            if (resp != '0') {
                //window.location.hash = '/ordermanagement/refundlist/?' + new Date().getTime();
                window.open(BASEURL + "/ordermanagement/refundexportexcel/?id=" + resp);
            } else {
                return
            }


        }
    });
}


function searchodrRef() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function reorderListRef() {
    window.location.hash = '/ordermanagement/refundlist/?' + new Date().getTime();
}

function resetSearchRef() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetsearchref/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/refundlist/?' + new Date().getTime();
        }
    });
}

function ddUmg(elems) {
    $('#ddImg').attr('src', elems);
    $('#imgModal').modal('show');
}