$('#select-all').click(function(event) {
    if (this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
    } else {
        $(':checkbox').each(function() {
            this.checked = false;
        });
    }
});

$(document).ready(function() {
    //alert(dashdata);
    $('.datepickerX').datepicker({
        autoclose: true,
        dateFormat: 'dd-mm-yyyy'
    });
});

function saveTrackingToHold() {
    alertMessage('กรุณายืนยันการบันทึก Tracking.', 'saveTrackingCallback');
}

function saveTrackingCallback() {
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function successCallBack() {
    window.location.hash = '/ordermanagement/trackinghold/?' + new Date().getTime();
}

//var tracking_idd;
function openTrackHold(elems) {

    var tracking_idd = $(elems).attr('data-id');
    var datacomment = $(elems).attr('data-comment');
    var datastatus = $(elems).attr('data-status');

    $('#datacomment').val(datacomment);
    $('#datastatus').val(datastatus);
    $('#tracking_id').val(tracking_idd);

    $('#holdModal').modal('show');
}


function saveTrackingToHoldStatus() {
    alertMessage('กรุณายืนยันการบันทึก Tracking.', 'saveTrackingToHoldStatusCallback');
}

function saveTrackingToHoldStatusCallback() {
    $('#holdModal').modal('hide');
    $('#preLoadSearch').show();
    var f = document.getElementById('dataFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function searchtrackinghold() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function retrackingholdList() {
    window.location.hash = '/ordermanagement/trackinghold/?' + new Date().getTime();
}

function resetSearchtrackinghold() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetSearchtrackinghold/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/trackinghold/?' + new Date().getTime();
        }
    });
}