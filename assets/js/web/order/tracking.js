$(function() {




    //     $('#example1').DataTable({
    //       "paging": true,
    //       "lengthChange": true,
    //       "searching": true,
    //       "ordering": false,
    //       "info": true,
    //       "autoWidth": false
    //   });



});

function importExcel() {
    if (!confirm('Please connfirm to import.')) {
        return;
    }
    $('#preLoadSearch').show();
    var f = document.getElementById('importFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function successCallBack() {
    window.location.hash = '/ordermanagement/tracking/?' + new Date().getTime();
}
var track_id = 0;

function deleteTracking(trackid) {
    track_id = trackid;
    alertMessage('กรุณายืนยันการลบรายการ Tracking.', 'deleteTrackingCallback');
}

function deleteTrackingCallback() {
    ajaxRequestProcess('ordermanagement/deletetracking/' + track_id + '/', successCallBack);
}


var dataids = 0;

function deleteShipping(idds) {
    dataids = idds;
    alertMessage('กรุณายืนยันการลบรายการ Shipping.', 'deleteShippingCallback');
}

function deleteShippingCallback() {
    if (dataids != '0') {
        ajaxRequestProcess('ordermanagement/daleteshipping/' + dataids, successCallBack);
    } else {
        return;
    }
}
var datavalues = '';
var dataids = 0;

function saveGetshipping2(elems) {
    datavalues = $(elems).val();
    dataids = $(elems).attr('data-id');
    alertMessage('กรุณายืนยันการบันทึกข้อมูล', 'saveGetshipping2Callback');
}

function saveGetshipping2Callback() {
    ajaxRequestProcess('ordermanagement/updateshippings/?shipping_id=' + dataids + '&track_code=' + datavalues, successShip2CallBack);
}

function successShip2CallBack() {
    window.location.hash = '/ordermanagement/shippinglist/?' + new Date().getTime();
}

function searchtracking() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function resetSearchTracking() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetSearchtrackingList/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/tracking/?' + new Date().getTime();
        }
    });
}

function searchship() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}


function resetSearchShip() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetSearchshippinglist/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/shippinglist/?' + new Date().getTime();
        }
    });
}