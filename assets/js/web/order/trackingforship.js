$('#select-all').click(function(event) {
    if (this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
    } else {
        $(':checkbox').each(function() {
            this.checked = false;
        });
    }
});

function searchtrack() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function reorderListTrack() {
    window.location.hash = '/ordermanagement/trackingforship/?' + new Date().getTime();
}

function resetSearchTrack() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/resetsearchtrack/",
        success: function(resp) {
            window.location.hash = '/ordermanagement/trackingforship/?' + new Date().getTime();
        }
    });
}

function saveTrackingToship() {
    alertMessage('กรุณายืนยันการบันทึก Tracking.', 'saveTrackingCallback');
}

function saveTrackingCallback() {
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function successShipCallBack() {
    window.location.hash = '/ordermanagement/trackingforship/?' + new Date().getTime();
}