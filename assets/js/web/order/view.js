var orderidxx = '';
var datavaluexx = '';

function checngOrderStatus(elem) {
    orderidxx = $(elem).attr('data-id');
    datavaluexx = $(elem).val();
    alertMessage('กรุณายืนยันการเปลี่ยนแปลงสถานะการสั่งซื้อ', 'checngOrderStatusCallback');
}

function checngOrderStatusButton(elem) {
    orderidxx = $(elem).attr('data-id');
    datavaluexx = $(elem).attr('data-val');
    alertMessage('กรุณายืนยันการเปลี่ยนแปลงสถานะการสั่งซื้อ', 'checngOrderStatusCallback');
}

function checngOrderStatusCallback() {

    $('#preLoadSearch').show();

    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/orderstatus/?order_id=" + orderidxx + "&value=" + datavaluexx,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderidxx);
        }
    });
}

function updateOrderPrice(elem) {

    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    var datavalue = $(elem).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/orderpay/?order_id=" + orderid + "&value=" + datavalue,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function itemDel(itemid) {
    if (!confirm('คุณต้องการลบรายการนี้หรือไม่?')) {
        return;
    }
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();

    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/removeitem/?item=" + itemid,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function itemStatus(elem) {

    $('#preLoadSearch').show();
    var itemid = $(elem).attr('data-id');
    var orderid = $('#ordid').val();
    var datavalue = $(elem).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/itemstatus/?item_id=" + itemid + "&value=" + datavalue,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function itemType(elem) {

    $('#preLoadSearch').show();
    var itemid = $(elem).attr('data-id');
    var orderid = $('#ordid').val();
    var datavalue = $(elem).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/itemtype/?item_id=" + itemid + "&value=" + datavalue,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function updateCommentOrder(elem) {

    $('#preLoadSearch').show();
    var itemid = $(elem).attr('data-id');
    var orderid = $('#ordid').val();
    var datavalue = $(elem).val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/updatecommentitem/?item_id=" + itemid + "&value=" + datavalue,
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            rederOrder(orderid);
        }
    });
}

function rederOrder(orderid) {
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/view2/" + orderid + "/",
        success: function(resp) {
            //window.location.hash = '/buyship/view/?'+new Date().getTime();
            $('.box-body').html(resp);
            $('#preLoadSearch').hide();
        }
    });
}

function updateQTY(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    var cnrate = $('#cnrate').val();
    setTimeout(function() {
        var vals = $(elem).val();

        xddsmv = parseInt(vals);

        // if (xddsmv == 0) {
        //     $('#preLoadSearch').hide();
        //     alert("ไม่สมารถระบุค่า 0 หรือ ช่องว่างได้");
        //     $(elem).val('1');
        //     $(elem).focus();
        //     return;
        // }

        // if($(elem).val()==0){
        //
        // 	return;
        // }

        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/updateqty/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val() + "&price_rmb=" + $(elem).attr('data-rmb') + "&rate=" + cnrate,
            success: function(resp) {
                rederOrder(orderid);
            }
        });
    }, 100);

}

function updateYuan(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    var cnrate = $('#cnrate').val();
    setTimeout(function() {
        var vals = $(elem).val();



        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/updatermb/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val() + "&qty=" + $(elem).attr('data-qty') + "&rate=" + cnrate,
            success: function(resp) {
                rederOrder(orderid);
            }
        });
    }, 100);
}

function updateBath(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    var cnrate = $('#cnrate').val();
    setTimeout(function() {
        var vals = $(elem).val();



        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/updatethb/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val(),
            success: function(resp) {
                rederOrder(orderid);
            }
        });
    }, 100);
}

function updateSaller(elem) {
    $('#preLoadSearch').show();
    var orderid = $('#ordid').val();
    setTimeout(function() {
        var vals = $(elem).val();

        /*$('#preLoadSearch2').show();
        $('#myTable').hide();*/
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/updatesaller/?item_id=" + $(elem).attr('data-id') + "&value=" + $(elem).val(),
            success: function(resp) {
                rederOrder(orderid);
                location.reload();
                //window.location.hash = '/ordermanagement/view/'+orderid;
            }
        });
    }, 100);
}

//Edit by hatairat 17-10-2564
function jumpto(h){
    var top = document.getElementById(h).offsetTop;
    window.scrollTo(0, top);
}
//End Edit


function addItem() {
    $('#preLoadSearch').show();
    var f = document.getElementById('itemFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function checkForExport(elems) {
    //alert($(elems).is(":checked"));
    dataid = $(elems).val();
    //alert(dataid);
    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/setforimport/?value=" + dataid,
        success: function(resp) {
            // rederOrder(order_id);
            // $('#preLoadSearch').hide();
        }
    });
}
$('#select-all-order').click(function(event) {
    if (this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
        var params = $('#checkFrm').serialize();
        $.ajax({
            type: "POST",
            data: params,
            url: BASEURL + "/ordermanagement/setfororder/",
            success: function(rsp) {

            }
        });

    } else {
        var params = $('#checkFrm').serialize();
        $.ajax({
            type: "POST",
            data: params,
            url: BASEURL + "/ordermanagement/unsetfororder/",
            success: function(rsp) {
                $(':checkbox').each(function() {
                    this.checked = false;
                });
            }
        });

    }
});

function exportExcel() {

    alertMessage('กรุณายืนยันการส่งใบสั่งซื้อ', 'exportExcelCalback');
}

function exportExcelCalback() {
    // if(!confirm('Please connfirm to export.')){
    // 	return;
    // }
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function setFilepath(ddd) {
    $('#filepath').show(ddd);
    $('#filepath').attr('href', ddd);
    $('#preLoadSearch').hide();
}

function updateCnShip(elsem) {
    var dataID = $(elsem).attr('data-id');
    var shipval = $(elsem).val();
    var cnrate = $('#cnrate').val();
    var order_id = $(elsem).attr('data-order');
    if (dataID == 0) {

        var saller_id = $(elsem).attr('data-saller');


        $('#preLoadSearch').show();
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/addcnship/?order_id=" + order_id + "&saller_id=" + saller_id + "&rate=" + cnrate + "&value=" + shipval,
            success: function(resp) {
                rederOrder(order_id);
                $('#preLoadSearch').hide();
            }
        });
    } else {
        $('#preLoadSearch').show();
        $.ajax({
            type: "GET",
            url: BASEURL + "/ordermanagement/updatecnship/?id=" + dataID + "&value=" + shipval + "&rate=" + cnrate,
            success: function(resp) {
                rederOrder(order_id);
                $('#preLoadSearch').hide();
            }
        });
    }
}


function uploadfiletempAdmin(elems) {
    //alert($(elems).val());
    //var input = document.getElementById("uploadBtn");
    var fReader = new FileReader();
    fReader.readAsDataURL(elems.files[0]);
    fReader.onloadend = function(event) {
        //var img = document.getElementById("yourImgTag");

        //img.src = event.target.result;
        //alert(event.target.result);
        $('#uploadFile').val(event.target.result);
        var item_id = $(elems).attr('data-item');
        $('#item_id').val(item_id);

    }

    //$('#uploadFile').val($(elems).val());
    var pparent = $(elems).parents();
    $('.up-text', pparent[0]).hide();
    $('.preTemp', pparent[0]).show();

    setTimeout(function() {
        var f = document.getElementById('tempFrm');

        f.method = 'post';
        f.enctype = 'multipart/form-data';
        f.target = 'updateFrm';
        f.action = BASEURL + "/buyship/processFrm/",
            f.submit();
        $(elems).val('');
    }, 1000);

}

function uploadfiletempCallBack() {
    // $('.up-text').show();
    // $('.fa-spin').hide();
    // $('#uploadFile').val('');
    // $('#pd_img').val(pathname);
    // $('#topImg img').attr('data-src',pathname);
    // $('#topImg img').attr('src',pathname);
    window.location.hash = '/ordermanagement/view/' + supperid + '/?' + new Date().getTime();
}

function saveImagSaller(oderid, salldes) {
    // if(!confirm('Please connfirm to export.')){
    // 	return;
    // }
    supperid = oderid;
    $('#preLoadSearch').show();
    var f = document.getElementById('saller_' + salldes);

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}