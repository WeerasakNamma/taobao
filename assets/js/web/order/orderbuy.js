function importOrderExcel(trackid) {
    track_id = trackid;
    alertMessage('Please confirm to Import Excel.', 'importOrderExcelCallback');
}

function importOrderExcelCallback() {
    //$('#preLoadSearch').show();
    var f = document.getElementById('importFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function successCallBack() {
    window.location.hash = '/ordermanagement/orderbuy/?' + new Date().getTime();
}

var orderid_new = 0;

function restatusOrder(orderid) {
    orderid_new = orderid;
    alertMessage('กรุณายืนยันการเปลี่ยนแปลงสถานะ', 'restatusOrderCallback');
}

function restatusOrderCallback() {
    ajaxRequestProcess('ordermanagement/notibuystatus/' + orderid_new + '/', successCallBack);
}

function refundWallet(orderid) {
    orderid_new = orderid;
    alertMessage('กรุณายืนยันการคืนเงินเข้า E Wallet', 'refundWalletCallback');
}

function refundWalletCallback() {
    ajaxRequestProcess('ordermanagement/refundwallet/' + orderid_new + '/', successCallBack);
}