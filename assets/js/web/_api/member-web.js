function updateQTY(key){
	$('#preLoadSearch').show();
	setTimeout(function(){
		var vals = $('#qty_'+key).val();
		var key_c = parseInt(vals);//alert(key_c);
		if(isNaN(key_c)){
			key_c = '1';
		}
		if(key_c==0){
			removeItem(key);
			return;
		}
		
		/*$('#preLoadSearch2').show();
		$('#myTable').hide();*/
		$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/search/updateqty/?key="+key+"&value="+key_c,
		   success: function(resp){
		   		window.location = BASEURL+'/search/';
			}
		 });
	},100);
	
}

function updateComment(elem){
	
		$('#preLoadSearch').show();
		var vals = $(elem).val();
		var key = $(elem).attr('data-key');

		
		/*$('#preLoadSearch2').show();
		$('#myTable').hide();*/
		$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/search/updatecomment/?key="+key+"&value="+vals,
		   success: function(resp){
		   		window.location = BASEURL+'/search/';
			}
		 });

	
}
function updateOption(elem){
	
		$('#preLoadSearch').show();
		var vals = $(elem).val();
		var key = $(elem).attr('data-key');
		var keyn = $(elem).attr('data-keyn');

		
		/*$('#preLoadSearch2').show();
		$('#myTable').hide();*/
		$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/search/updateoption/?key="+key+"&value="+vals+"&keyn="+keyn,
		   success: function(resp){
		   		window.location = BASEURL+'/search/';
			}
		 });

	
}
/*function itemSearch(){
		
	$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/assets/API/demo/api.php?item_id=111",
		   success: function(resp){
		   		$('#searchBody').html(resp);
			}
		 });
}*/
function removeItem(key){
	
	$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/search/removeitem/?key="+key,
		   success: function(resp){
		   		window.location = BASEURL+'/search/';
			}
		 });
}

function changeImg(imgs){
	$('#topImg').attr('src',imgs);
	$('#pd_img').val(imgs);
}
function orderNow(){
		if($('#product_qty').val()<=0){
			alert('กรุณาระบุจำนวนสินค้า');
			$('#product_qty').focus();
			return;
		}
		var f = document.getElementById('productHome'); 
		
		f.method = 'post';
		f.enctype = 'multipart/form-data';
		f.target = 'orderFrm';
		f.action = BASEURL+"/order/additemMember/", 
		f.submit();
}
function takeorder(){
	alertMessage('Please confirm to Take Order.','takeorderCallback');
}
function takeorderCallback(){
		$('#preLoadSearch').show();
		var f = document.getElementById('takeOrder'); 
		
		f.method = 'post';
		f.enctype = 'multipart/form-data';
		f.target = 'updateFrm';
		f.action = BASEURL+"/buyship/processFrm/", 
		f.submit();
}
function sendTracking(){
		if(!confirm('โปรดยืนยันการยืนยันการส่งสินค้าหรือไม่?')){
			return;
		}
		$('#orderMsg').modal({
			backdrop: 'static'
		});
		var f = document.getElementById('takeOrder'); 
		
		f.method = 'post';
		f.enctype = 'multipart/form-data';
		f.target = 'frameUpdate';
		f.action = BASEURL+"/order/sendtrack/", 
		f.submit();
}
function reOrder(orderid){
		
		if(!confirm('คุณต้องการสั่งซื้อรายการนี้อีกครั้งหรือไม่?')){
			return;
		}
		$('#preLoadSearch').show();
		$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/buyship/reorder/?order_id="+orderid,
		   success: function(resp){
		   		window.location.hash = '/buyship/orderlist/?'+new Date().getTime();
			}
		 });
}
function addItemImp(){
		$('#orderMsg').modal({
			backdrop: 'static'
		});
		var f = document.getElementById('takeOrder_Import'); 
		
		f.method = 'post';
		f.enctype = 'multipart/form-data';
		f.target = 'frameUpdate';
		f.action = BASEURL+"/order/additem_import/", 
		f.submit();
}
function removeItemImp(key){
	if(!confirm('คุณต้องการลบรายการนี้หรือไม่')){
		return;
	}
	$('#orderMsg').modal({
			backdrop: 'static'
		});
	/*$.ajax({ 
		   type: "P", 
		   url:  BASEURL+"/order/removeItemImp/?key="+key,
		   success: function(resp){
		   		location.reload();
			}
		 });*/
	var f = document.getElementById('fakeFrm'); 
		
		f.method = 'post';
		f.enctype = 'multipart/form-data';
		f.target = 'frameUpdate';
		f.action = BASEURL+"/order/removeItemImp/?key="+key, 
		f.submit();
}
function takeorderImp(){
		
		if(!confirm('คุณต้องการสั่งซื้อสินค้าทั้งหมดหรือไม่?')){
			return;
		}
	
		$('#orderMsg').modal({
			backdrop: 'static'
		});
		$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/order/takeOrderImp/",
		   success: function(resp){
		   		if(resp=='T'){
					//location.reload();
					window.location = BASEURL+"/member/order_2/track/";
				}
			}
		 });
}
function orderimpUpdate(){
		
		if(!confirm('คุณต้องการแก้ไขรายการนี้หรือไม่?')){
			return;
		}
		$('#orderMsg').modal({
			backdrop: 'static'
		});
		var f = document.getElementById('editOrder_Import'); 
		
		f.method = 'post';
		f.enctype = 'multipart/form-data';
		f.target = 'frameUpdate';
		f.action = BASEURL+"/order/editOrderImp", 
		f.submit();
}
function paymentUpdate(){
		
		if(!confirm('คุณต้องการแจ้งโอนเงินรายการนี้หรือไม่?')){
			return;
		}
		$('#orderMsg').modal({
			backdrop: 'static'
		});
		var f = document.getElementById('updateFrm'); 
		
		f.method = 'post';
		f.enctype = 'multipart/form-data';
		f.target = 'frameUpdate';
		f.action = BASEURL+"/member/payment_track/insert", 
		f.submit();
}

function updateProfile(){
	if($('#member_email').val()==''){
		alert('กรุณาระบุอีเมล์');
		$('#member_email').focus();
		return;
	}
	
	if($('#old_pwd').val()!='' || $('#new_pwd').val()!='' || $('#renew_pwd').val()!=''){
		
		if($('#new_pwd').val()!=$('#renew_pwd').val() || $('#new_pwd').val()==''){
			alert('กรุณากรอกรหัสผ่านให้ตรงกัน');
			$('#renew_pwd').focus();
			return;
		}
		
	}
	
	if(!confirm('คุณต้องการเปลี่ยนแปลงข้อมูลหรือไม่?')){
			return;
		}
		$('#orderMsg').modal({
			backdrop: 'static'
		});
		var f = document.getElementById('profileFrm'); 
		
		f.method = 'post';
		f.enctype = 'multipart/form-data';
		f.target = 'frameUpdate';
		f.action = BASEURL+"/member/profile/update", 
		f.submit();
	
}
function wrongPwd(){
	$('#orderMsg').modal('hide');
	alert('กรุณากรอกรหัสผ่านเดิมให้ตรงกัน');
	$('#old_pwd').focus();
}
function validateKey(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

function cancelOrder(itemid){
	if(!confirm('คุณต้องการ ยกเลิก รายการนี้หรือไม่')){
		return;
	}
	$('#preLoadSearch').show();
	$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/buyship/cancelorder/?order_id="+itemid,
		   success: function(resp){
			   	
		   		window.location.hash = '/buyship/orderlist/?'+new Date().getTime();
			}
		 });
}