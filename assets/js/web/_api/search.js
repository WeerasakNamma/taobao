/*$(document).ready(function(){$(".megamenu").megamenu();});*/

var Base64 = {
// private property
_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

// public method for encoding
encode : function (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    input = Base64._utf8_encode(input);

    while (i < input.length) {

        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }

        output = output +
        Base64._keyStr.charAt(enc1) + Base64._keyStr.charAt(enc2) +
        Base64._keyStr.charAt(enc3) + Base64._keyStr.charAt(enc4);

    }

    return output;
},

// public method for decoding
decode : function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    while (i < input.length) {

        enc1 = Base64._keyStr.indexOf(input.charAt(i++));
        enc2 = Base64._keyStr.indexOf(input.charAt(i++));
        enc3 = Base64._keyStr.indexOf(input.charAt(i++));
        enc4 = Base64._keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

    }

    output = Base64._utf8_decode(output);

    return output;

},

// private method for UTF-8 encoding
_utf8_encode : function (string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {

        var c = string.charCodeAt(n);

        if (c < 128) {
            utftext += String.fromCharCode(c);
        }
        else if((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        }
        else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }

    }

    return utftext;
},

// private method for UTF-8 decoding
_utf8_decode : function (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while ( i < utftext.length ) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i+1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i+1);
            c3 = utftext.charCodeAt(i+2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }
    return string;
}
}




var supperJson = '';
function itemSearch(){
	var webLink = $('#searchurl').val();
	if(webLink==''){
		$('#searchurl').focus();
		return;
	}
	
	$('#preLoadSearch').show();
	$('#productHome').hide();
	$('#preLoadSearch').show();
		
	/*$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/assets/API/demo/api.php?item_id=111",
		   success: function(resp){
		   		$('#searchBody').html(resp);
			}
		 });*/
	var codelink = Base64.encode(webLink);
	$('#pd_option').html('');
	$('#product_price_thb').val('');
	$('#product_price_rmb').val('');
	$('#pd_img').val('');
	$('#product_link').val('');
	$('#img-tag-c').html('');
	$('#shopname').html('');
	$('#shop_name').val('');
	$('#shop_link').val('');
	$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/API/open_API.php?link="+codelink,
		   datatype:"json",
		   success: function(resp){
		   		console.log(resp);
		   		
		   		var dhtml = getDATAHTML(resp);
		   		supperJson = resp;
		   		//$('#searchBody').html(dhtml);
			}
		 });
}
function getDATAHTML(dataJson){
	var data = '';
	$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/API/html.php?a=prop",
		   success: function(resp){
		   		getHTMLTITLE();
		   		getHTMLSHOP();
		   		$('#preLoadSearch').hide();
		   		$('#productHome').show();
		   		$('#prop_data').html(resp);
		   		$('.tb-btn-buy').remove();
		   		$('.tb-amount').remove();
		   		$('#product_link').val(dataJson.itemLink);
		   		$('#topImg').html(dataJson.dataImg);
		   		$('#J_ImgBooth').attr('width','100%');
		   		
		   		var factive = '';
		   		var price_rmb = 0.00;
		   		$.each( dataJson.skuMap, function( key, value ) {
				  console.log( key + ": " + value );
				  if(value.stock>=1 && factive==''){
				  	factive = key;
				  	price_rmb = value.price;
				  }
				});
				var pice_th = price_rmb*5.5;
				$('#price_th').text(pice_th);
				$('#price_rmb').text(price_rmb);
				$('#product_price_thb').val(pice_th);
				$('#product_price_rmb').val(price_rmb);
				
				var datamap = factive.split(";");
				
				$.each( datamap, function( key, value ) {
					$( "ul.J_TSaleProp li" ).each(function(e) {
					  	var datval = $(this).attr('data-value');
					  	console.log(datval);
					  	if(value==datval){
							$(this).addClass('active');
							var c_data = datval.split(":");
							var str_Input = '<input type="hidden" name="op_name[]" id="name_'+c_data[0]+'" value=""/>';
                				str_Input +='<input type="hidden" name="op_value[]" id="value_'+c_data[0]+'" value=""/>';
                			$('#pd_option').append(str_Input);				
                			var sp_selector = '#value_'+c_data[0];
                			translateAPIval($(this).text(),sp_selector);
						}
					});
		   		});
		   		
		   		$( "dt.tb-metatit" ).each(function(index) {
				  translateAPI($(this).text(),this);
				});
				
				$( "ul.J_TSaleProp" ).each(function(index) {
				  	$( "li a span",this).each(function(index) {
					  translateAPI($(this).text(),this);
					});
				});
				
				$( "ul.J_TSaleProp" ).each(function(index) {
					var dataprop = $(this).attr('data-property');
					var ks = 'b';
				  	$( "li",this).each(function(index) {
				  		var dt_name = $(this).attr('data-value');
				  		var c_data = dt_name.split(":");
				  		
				  		var sp_selector = '#name_'+c_data[0];
				  		if(ks=='b'){
							translateAPIval(dataprop,sp_selector);
							ks='c';
						}
                		
					});
				});
				
				
		   		$( "ul.tb-img li" ).each(function(index) {
				   var bg = $('a',this).css('background');
			        bg = bg.replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');			        
			        var kname = $('a span',this).text();
			        var textimg = '<img src="'+bg+'" /><br /><span>'+kname+'</span>';
			        $(this).html(textimg);
			        console.log(bg);
				});
				$( "ul.tb-img li span" ).each(function(index) {
				   translateAPI($(this).text(),this);
				});
				
				$( "ul.J_TSaleProp" ).each(function(index) {
				  	$( "li a",this).each(function(index) {
					  $(this).attr('href','javascript:;');
					});
				});
				
				var datImg = $('#J_ImgBooth').attr('src');
				//alert(datImg);
				$('#pd_img').val('http:'+datImg);
				
		   		
			}
		 });
	//return data;
}
function getHTMLTITLE(){
	var data = '';
	$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/API/html.php?a=title",
		   success: function(resp){
		   		$('#img-tag-c').html(resp);
		   		$('#product_name').val($('#img-tag-c').text());
			}
		 });
	//return data;
}
function getHTMLSHOP(){
	var data = '';
	$.ajax({ 
		   type: "GET", 
		   url:  BASEURL+"/API/html.php?a=shopname",
		   success: function(resp){
		   		$('#shopname').html(resp);
		   		$('#shop_name').val($('#shopname').text());
		   		var shoplink = $('a.slogo-shopname').attr('href');
		   		$('#shop_link').val('http:'+shoplink);
			}
		 });
		 
		
}

function translateAPI(keyname,selector){
	
	$.ajax({ 
		   type: "GET", 
		   url: "https://translate.yandex.net/api/v1.5/tr.json/translate?key="+TRANSKEY+"&lang=zh-th&text="+keyname,
		   datatype:"json",
		   success: function(resp){
		   		rester = resp.text[0];
		   		if(rester=="สีของการแบ่งประเภท"){
					rester = 'สี';
				}
		   		$(selector).text(rester);
			}
		 });
	
}

function translateAPIval(keyname,selector){
	
	$.ajax({ 
		   type: "GET", 
		   url: "https://translate.yandex.net/api/v1.5/tr.json/translate?key="+TRANSKEY+"&lang=zh-th&text="+keyname,
		   datatype:"json",
		   success: function(resp){
		   		rester = resp.text[0];
		   		if(rester=="สีของการแบ่งประเภท"){
					rester = 'สี';
				}
		   		$(selector).val(rester);
			}
		 });
	
}

$(document).ready(function() {
	$('#prop_data').on('click', 'li', function(){
			var datval = $(this).attr('data-value');
			var datamap = datval.split(":");
			
			
				$( "ul.J_TSaleProp li" ).each(function(e) {
				  	var datval_n = $(this).attr('data-value');
				  	var datamap_n = datval_n.split(":");
				  	if(datamap_n[0]==datamap[0]){
						$(this).removeClass('active');
					}
				  	//console.log(datval_n);
				});
			var selector = '#value_'+datamap[0];
			translateAPIval($(this).text(),selector);
			$(this).addClass('active');
   		
	});
 
});




