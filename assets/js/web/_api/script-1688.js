
function getDATAHTML_2(dataJson){
	var data = '';
	$.ajax({
		   type: "GET",
		   url:  BASEURL+"/API/html.php?a=json",
		   success: function(resp){
		   	getHTMLTITLE();
		   	getHTMLSHOP_2();
		   	getHTMLSHOPINFO();
		   	var ojb = JSON.parse(resp);
		   	skuMap_c = ojb;
		   	$('#product_link').val(dataJson.itemLink);
		   	$('#topImg').html(dataJson.dataImg);
		   	$('#J_ImgBooth').attr('width','100%');
		   	if (typeof ojb.sku != "undefined") {
		   		console.log(ojb.sku.skuProps);

			   var x_html = '<div id="sku-1688">';
			   $.each( ojb.sku.skuProps, function( key, value ) {
			   		/*var cts = 'สี';
			   		var cts_c = 'color';
				    if(key==1){
				   	  cts = 'ขนาด';
				   	  cts_c = 'size';
				    }*/

					var str_Input = '<input type="hidden" name="op_name[]" id="name_'+key+'" value=""/>';
						str_Input += '<input type="hidden" name="op_name_th[]" id="name_th_'+key+'" value=""/>';
					$('#pd_option').append(str_Input);
					$('#name_'+key).val(value.prop);
					translateAPIval(value.prop,'#name_th_'+key);

				   x_html += '<dl><dt class="tb-property-type">'+value.prop+'</dt><dd><ul data-property="'+value.prop+'" class="J_TSaleProp tb-clearfix cx_'+key+'">';
				   $.each( value.value, function( key_c, value_c ) {
				   		var cclass = '';
				   		if(key_c==0){
							var cclass = 'class="active"';
                			var	str_Input_c ='<input type="hidden" name="op_value[]" id="value_'+key+'" value=""/>';
                				str_Input_c +='<input type="hidden" name="op_value_th[]" id="value_th_'+key+'" value=""/>';
                			$('#pd_option').append(str_Input_c);
                			var sp_selector = '#value_'+key;
                			var sp_selector2 = '#value_th_'+key;
                			$(sp_selector).val(value.prop);
                			translateAPIval(value_c.name,sp_selector2);
						}
						var cxml = '';
						var kstyle = '';
						if (typeof value_c.imageUrl != "undefined") {

							cxml = '<img src="'+value_c.imageUrl+'" width="40" /><br />';
							kstyle = 'min-height:65px;';
						}
				   		x_html += '<li data-value="1688:'+key+'" style="'+kstyle+'" '+cclass+'>'+cxml+'<a href="javascript:;"><span data-cn="'+value_c.name+'">'+value_c.name+'</span></a></li>';
				   	});
				   x_html += '</ul></dd></dl>';
				});

			   x_html += '</div>';

			   //$('#preLoadSearch').hide();
            preload('hide');
		   		$('#orderFrm').show();
		   		$('#prop_data').html(x_html);

		   		$( "dt.tb-property-type" ).each(function(index) {
				  translateAPI($(this).text(),this);
				});

				$( "ul.J_TSaleProp" ).each(function(index) {
				  	$( "li a span",this).each(function(index) {
					  translateAPI($(this).text(),this);
					});
				});
				//console.log(skuMap_c.sku.skuMap.price);
				// var price_rmb = 0.00;
				//
				// price_rmb = skuMap_c.sku.priceRange[0][1];
            var price_rmb = 0.00;

				if (typeof skuMap_c.sku.priceRange === "undefined") {
				    price_rmb = skuMap_c.sku.price;
				    price_rmb = price_rmb.split("-",1);
				}else{
					price_rmb = skuMap_c.sku.priceRange[0][1];
				}

				/*$.each( skuMap_c.sku.priceRange, function( key, value ) {
					if(value[0]==1){
						price_rmb = value[1];
					}

				});*/



				var pice_th = price_rmb*cnrate;
				$('#price_th').text(pice_th.toFixed(2));
				$('.price_rmb').text(price_rmb);
				$('#product_price_thb').val(pice_th);
				$('#product_price_rmb').val(price_rmb);

				$('#product_qty').attr('onchange','calMap();');

				var datImg = $('#topImg img').attr('src');
				//alert(datImg);
				$('#pd_img').val(datImg);

				$('#product_qty').val(skuMap_c.sku.priceRange[0][0]);

			}else{
				//$('#preLoadSearch').hide();
            preload('hide');
		   		$('#productHome').show();
		   		var price_rmb = 0.00;
		   		price_rmb = dataJson.dataPrice;
		   		var pice_th = price_rmb*cnrate;
				$('#price_th').text(pice_th);
				$('#price_rmb').text(price_rmb);
				$('#product_price_thb').val(pice_th);
				$('#product_price_rmb').val(price_rmb);


				var datImg = $('#topImg img').attr('src');
				//alert(datImg);
				$('#pd_img').val(datImg);
			}
		   		/*console.log(ojb.sku.skuProps);
		   		alert(ojb.sku.skuProps);
		   		console.log(ojb.sku.skuMap);
		   		alert(ojb.sku.skuMap);*/
               checkdata = 'T';
			}

		 });
	//return data;
}
function calMap(){
	var dataVal = $('#product_qty').val();
	var price_rmb = 0;
	$.each( skuMap_c.sku.priceRange, function( key, value ) {
		if(value[0]<=dataVal){
			price_rmb = value[1];
		}
	});
	if(price_rmb==0){
		$('#product_qty').val(skuMap_c.sku.priceRange[0][0]);
		price_rmb = skuMap_c.sku.priceRange[0][1];
		alert('ขั้นต่ำในการสั่งซื้อ '+skuMap_c.sku.priceRange[0][0]+"ชิ้น");
	}

	var pice_th = price_rmb*cnrate;
	$('#price_th').text(pice_th);
	$('#price_rmb').text(price_rmb);
	$('#product_price_thb').val(pice_th);
	$('#product_price_rmb').val(price_rmb);
}



function getHTMLSHOP_2(){
	var data = '';
	$.ajax({
		   type: "GET",
		   url:  BASEURL+"/API/html.php?a=shopname",
		   success: function(resp){

		   		if(resp==''){
					$('#shopname').html('ไม่พบชื่อร้าน');
					$('#shop_name').val($('#shopname').text());
			   		$('#shop_link').val($('#shopname').text());
					return;
				}
		   		$('#shopname').html(resp);
		   		$('#shopname span').removeClass('info');
		   		$('#shop_name').val($('#shopname').text());
		   		var shoplink = $('a.name').attr('href');
		   		$('#shop_link').val(shoplink);


			}
		 });
}
