function showItemRefundInfo(itemid, ref_item) {
    preload('show');
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/iteminfo/" + itemid,
        success: function(resp) {
            console.log(resp);
            var newdata = JSON.parse(resp);
            $('#item_id2').val(newdata.id);
            $('#itm_name').text(newdata.product_title);
            $('#item_qty').val(newdata.product_buy_qty);
            $('#img_pd').attr("src", newdata.img_path);
            $('#ref_item').val(ref_item);
            preload('hide');
            $('#refundModal').modal('show');
        }
    });
    //  $('#refundModal').modal('show');
}


function saveRefund() {
    if ($('#ref_refund').val() <= 0) {
        alert('กรุณากรอก จำนวนสินค้าที่ขาด');
        $('#ref_refund').focus();
        return;
    }
    alertMessage('ยืนยันการ Refund.', 'saveRefundCallback');
}

function saveRefundCallback() {

    preload('show');
    var f = document.getElementById('refundFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/buyship/processFrm",
        f.submit();
}

function saveRefundSuccess() {
    $('#refundModal').modal('hide');
    setTimeout(function() {
        location.hash = "/buyship/refundlist/";
    }, 500);


}
var dataidexs = 0;
var datapriceesx = 0;

function cancelRefund(elems) {
    dataidexs = $(elems).attr('data-id');
    datapriceesx = $(elems).attr('data-item');
    alertMessage('กรุณายืนยัน ยกเลิกรายการ Refund.', 'cancelRefundCallback');
}

function cancelRefundCallback() {
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/cancelrefund/?ref_id=" + dataidexs + "&item_id=" + datapriceesx,
        success: function(resp) {
            window.location.hash = '/buyship/refundlist/?' + new Date().getTime();
        }
    });
}

function changeOption(elems) {
    var datavals = $(elems).val();
    var dataopt = $(elems).attr('data-opid');
    var dataodr = $(elems).attr('data-odr');
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/changeoption/?opid=" + dataopt + "&order_id=" + dataodr + "&value=" + datavals,
        success: function(resp) {
            window.location.hash = '/buyship/view/' + dataodr + '/?' + new Date().getTime();
        }
    });

}

// function uploadfiletemp(elem){
//    var pparent = $(elem).parents();
// 	$('.up-text',pparent[0]).hide();
// 	$('.preTemp',pparent[0]).show();
//
//
// 	$('#uploadFile').val($(elem).val());
// 	setTimeout(function(){
//
// 		//$(elem).val('');
// 		mmUpload();
// 	},800);
//
// }

function uploadfiletemp(elems) {
    //alert($(elems).val());
    //var input = document.getElementById("uploadBtn");
    var fReader = new FileReader();
    fReader.readAsDataURL(elems.files[0]);
    fReader.onloadend = function(event) {
        //var img = document.getElementById("yourImgTag");

        //img.src = event.target.result;
        //alert(event.target.result);
        $('#uploadFile').val(event.target.result);
        var item_id = $(elems).attr('data-item');
        $('#item_id').val(item_id);

    }

    //$('#uploadFile').val($(elems).val());
    var pparent = $(elems).parents();
    $('.up-text', pparent[0]).hide();
    $('.preTemp', pparent[0]).show();

    setTimeout(function() {
        var f = document.getElementById('tempFrm');

        f.method = 'post';
        f.enctype = 'multipart/form-data';
        f.target = 'updateFrm';
        f.action = BASEURL + "/buyship/processFrm/",
            f.submit();
        $(elems).val('');
    }, 1000);

}

function uploadfiletempCallBack() {
    // $('.up-text').show();
    // $('.fa-spin').hide();
    // $('#uploadFile').val('');
    // $('#pd_img').val(pathname);
    // $('#topImg img').attr('data-src',pathname);
    // $('#topImg img').attr('src',pathname);
    window.location.hash = '/buyship/view/' + supperid + '/?' + new Date().getTime();
}

function ddUmg(elems) {
    $('#ddImg').attr('src', elems);
    $('#imgModal').modal('show');
}

//Edit by hatairat 17-10-2564
function jumpto(h){
    var top = document.getElementById(h).offsetTop;
    window.scrollTo(0, top);
}
//End Edit