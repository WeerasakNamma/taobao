$(document).ready(function(){  
    $.ajax({
        type: "GET",
        url: BASEURL + "/buyship/get_order_overdue_payment/",
        success: function(resp) {            
            var result = JSON.parse(resp);
            console.log(result.length);
            if(result.length !== 0){
               if(result.wallet < 0 || result.total > 0){ 
                    let dollarUSLocale = Intl.NumberFormat('en-US');                

                    var order = '<li class="list-group-item d-flex justify-content-between align-items-center">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6">'+
                                            '<strong>จำนวนใบสั่งซื้อ</strong>'+
                                        '</div>'+
                                        '<div class="col-md-6" align="right">'+
                                            '<span class="badge badge-primary badge-pill btn-danger">'+result.count+'</span>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="text-red">ยอดค้างชำระรวม : <strong>'+dollarUSLocale.format(result.total)+'</strong> บาท</div>'+
                                '</li>';
                    var shipping = '<li class="list-group-item d-flex justify-content-between align-items-center">'+
                                        '<div class="row">'+
                                            '<div class="col-md-6">'+
                                                '<strong>จำนวนค้างชำระบิลขนส่ง</strong>'+
                                            '</div>'+
                                            '<div class="col-md-6" align="right">'+
                                                '<span class="badge badge-primary badge-pill btn-danger">14</span>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="text-red">ยอดค้างชำระรวม : <strong>217.58</strong> บาท</div>'+
                                    '</li>';
                    var wallet ='<li class="list-group-item d-flex justify-content-between align-items-center">'+
                                    '<strong>ค้างชำระใน E-wallet</strong>'+
                                    '<div class="text-red">ยอดค้างชำระรวม : <strong>'+dollarUSLocale.format(Math.abs(result.wallet))+'</strong> บาท</div>'+
                                '</li>';


                    if(result.count > 0)
                        $('#overdue').append(order);

                    // Edit By Weerasak 19-01-2566
                    // if(result.wallet < 0)
                    //     $('#overdue').append(wallet);
                   
                    $('#payment_model').modal('show')
                }
            }
        }
    });    
});

function gotoOrderlist(){
	window.location.hash = '/buyship/orderlist';
}