$(function () {
      $(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy' 
      });
       $(".select2").select2();
       
       
       	
       
     
      
});

function saveMeeting(){
	alertMessage('Please confirm to Save Meeting.','saveMeetingCallback');
}

function saveMeetingCallback(){
	
	if($('#meeting_title').val()==''){
		noticeMessage('Please Enter Meeting Title',"scriptFocus('#meeting_title')");

		return;
	}
	if(createFrm.elements["meeting_team[]"].selectedIndex == -1) {
	  	noticeMessage('Please Enter Meeting Title');
		return;
	}
	if($('#meeting_date').val()==''){
		noticeMessage('Please Enter Meeting Date',"scriptFocus('#meeting_date')");

		return;
	}

	preload('show');
	var f = document.getElementById('createFrm'); 

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/meeting/processFrm", 
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/meeting/meetinglistall/';
}