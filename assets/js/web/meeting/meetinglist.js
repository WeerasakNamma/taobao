      $(function () {

        /* initialize the external events
         -----------------------------------------------------------------*/
       

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        //alert(m);
        cdate = y+"-"+m+"-01";
        $('#calendar').fullCalendar({
          defaultDate: moment(cdate),
          header: {
            left: 'prev,next ',
            center: 'title',
            right: ''
          },
          //Random default events
          events: dataresult,
          editable: false,
          droppable: false, // this allows things to be dropped onto the calendar !!!
          drop: false,
          eventRender: function(event, eventElement) {
			if (event.imageurl)
			{
			   eventElement.find("div.fc-content").append(event.imageurl);
			   console.log(eventElement);
			}
		  },
		  eventClick: function(calEvent, jsEvent, view) {

			        //alert('Event: ' + calEvent.tbx);
			        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
			        //alert('View: ' + view.name);

			        // change the border color just for fun
			        //$(this).css('border-color', 'red');
			        if(calEvent.meetingID!='0'){
						ajaxRequestProcess('/meeting/renderHtml/?mode=meetingdetail&id='+calEvent.meetingID,setMeeting);
					}else{
						if(calEvent.usernameID!='0'){
							viewprofilemeet(calEvent.usernameID);
						}else{
							return;
						}
						
					}
			        
			        //$('#myModalLabel').text(calEvent.title);
			        

			    }
        });

		$('.fc-prev-button').attr('onclick','prevMonth();');
      	$('.fc-next-button').attr('onclick','nextMonth();');
       
      });
      
      function prevMonth(){
      	var setm = m;
      	var sety = y;
      	if(m==1){
			setm = 11;
			sety = y-1;
		}else{
			setm = m-1;
		}
	  	window.location.hash = '/meeting/?month='+setm+"&year="+sety;
	  }
	  function nextMonth(){
      	var setm = m;
      	var sety = y;
      	if(m==12){
			setm = 1;
			sety = y+1;
		}else{
			setm = m+1;
		}
	  	window.location.hash = '/meeting/?month='+setm+"&year="+sety;
	  }
      
      function setMeeting(datameeting){
	  	 $('#contentMeeting').html(datameeting);
	  	 preload('hide');
	  	 setTimeout(function(){
	  	 	$('#myModal').modal('show');
	  	 },1000);
	  	 
	  }
	  function viewprofilemeet(userid){
	
			ajaxRequestProcess('dashboard/profile/'+userid+'/',profileMeetCallback);
		}
		function profileMeetCallback(resp){
			 $('#contentMeeting').html(resp);
		  	 preload('hide');
		  	 setTimeout(function(){
		  	 	$('#myModal').modal('show');
		  	 },1000);
		}
      