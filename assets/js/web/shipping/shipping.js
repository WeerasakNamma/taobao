function saveRegister() {
    if ($('#file_img').val() == '') {
        noticeMessage('กรุณาเลือกไฟล์', 'scriptFocus("#file_img");');
        return false;
    }

    alertMessage('กรุณายืนยันการสมัคร', 'saveRegisterCallback');
}

function saveRegisterCallback() {
    preload('show');
    var f = document.getElementById('reqFrm');
    //alert(f);
    f.method = 'post';
    f.target = 'updateFrm';
    f.action = BASEURL + "/shipping/processFrm",
        f.submit();
}

function regisResult() {

    preload('hide');
    window.location.hash = '/shipping/register/?' + new Date().getTime();

}

$('#myModal').modal({
    backdrop: 'static',
    keyboard: false
});

function acceptRegister() {
    if (document.getElementById('isAgeSelected').checked) {
        $('#myModal').modal('hide');
    } else {
        alert("กรุณายอมรับเงื่อนไขข้อตกลง");
        return;
    }
}