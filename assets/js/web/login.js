function preload(action) {
    if (action == 'hide') {
        $('#preloadModal').modal('hide');
    } else {
        $('#preloadModal').modal({
            keyboard: false,
            backdrop: 'static'

        });
    }

}
document.onkeydown = function() {
    if (window.event.keyCode == '13') {
        submitLogin();
    }
}

function submitLogin() {
    if ($('#vc_username').val() == "") {
        alert('USERNAME ไม่ถูกต้อง');
        $('#vc_username').focus();
        return;
    }
    if ($('#vc_password').val() == "") {
        alert('PASSWORD ไม่ถูกต้อง');
        $('#vc_password').focus();
        return;
    }
    preload('show');
    var params = $('#loginFrm').serialize();


    $.ajax({
        type: "POST",
        url: BASEURL + "/auth/login/",
        data: params,
        success: function(resp) {
            if (resp == 'A') {
                setTimeout(function() {
                    preload('hide');
                    window.location = BASEURL;
                }, 1500);

            } else if (resp == 'AP') {
                setTimeout(function() {
                    preload('hide');
                    window.location = BASEURL+'#/user/editprofile/';
                    window.location.reload();
                }, 1500);

            } else if (resp == 'P') {

                setTimeout(function() {
                    preload('hide');
                    alert('PASSWORD ไม่ถูกต้อง');
                    $('#vc_password').focus();
                }, 500);
            } else if (resp == 'U') {

                setTimeout(function() {
                    preload('hide');
                    alert('USERNAME ไม่ถูกต้อง');
                    $('#vc_username').focus();
                }, 500);
            }
            console.log(BASEURL);
        }
    });
}

function submitReset() {
    if ($('#vc_username').val() == "") {
        alert('กรุณากรอก "Username"');
        $('#vc_username').focus();
        return;
    }
    if (!confirm('กรุณายืนยันการเปลี่ยนรหัสผ่าน')) {
        return;
    }
    preload('show');
    var username = $('#vc_username').val();
    $.ajax({
        type: "GET",
        url: BASEURL + "/auth/setpwd/?email=" + username,
        success: function(resp) {

            if (resp == 'A') {
                setTimeout(function() {
                    preload('hide');
                    $('.register-box').hide();
                    $('.scc-box').show();
                }, 1500);

            } else {

                setTimeout(function() {
                    preload('hide');
                    alert('ไม่พบ E-Mail ที่ท่านระบุ');
                    $('#vc_username').focus();
                }, 500);
            }
        }
    });
}