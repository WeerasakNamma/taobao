$(function() {

    $(".datepickerx").datepicker({
        todayBtn: "linked",
        language: "th",
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });

});

function searchPaid() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/report/processFrm/",
        f.submit();
}

function reorderList() {
    window.location.hash = '/report/paidrp/?' + new Date().getTime();
}

function resetSearchPaid() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/report/resetsearchpaidrp/",
        success: function(resp) {
            window.location.hash = '/report/paidrp/?' + new Date().getTime();
        }
    });
}