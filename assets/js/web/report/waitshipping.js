$(function() {

    $(".datepickerx").datepicker({
        todayBtn: "linked",
        language: "th",
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });

});

function searchWaitship() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/report/processFrm/",
        f.submit();
}

function reorderList() {
    window.location.hash = '/report/waitshipping/?' + new Date().getTime();
}

function resetWaitship() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/report/resetwaitship/",
        success: function(resp) {
            window.location.hash = '/report/waitshipping/?' + new Date().getTime();
        }
    });
}