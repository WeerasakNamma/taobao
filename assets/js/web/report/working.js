$(function () {
      //alert('d');
      $('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });

      /*$('#example2').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });*/

      $(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy'
      });

       $(".select2").select2();


});
var memcheck = '';
function searchWorking(memcheckC){
	memcheck = memcheckC;
	//alert(memcheck);
	//alertMessage('Please confirm to Search.','searchWorkingCallback');
	searchWorkingCallback();
}

function searchWorkingCallback(){

	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/report/processFrm",
	f.submit();
}
function successWorkingCallBack(){
	//location.reload();
	if(memcheck=='99999'){
		location.hash = '/report/working/?'+new Date().getTime();
	}else{
		location.hash = '/report/userworking/?'+new Date().getTime();
	}

}

function openWorkingHistory(memid){
	//$('#history_'+memid).slideToggle();
	//$('#historyModal').modal('show');
	if(memid!='0'){
		ajaxRequestProcess('report/workingHistory/'+memid,historyCallback);
	}else{
		return;
	}
}
function openWorkingHistory2(memid){
	//$('#history_'+memid).slideToggle();
	//$('#historyModal').modal('show');
	if(memid!='0'){
		ajaxRequestProcess('report/workingHistorynew/'+memid,historyCallback);
	}else{
		return;
	}
}
function historyCallback(mydatacontent){
	$('#historyvalue').html(mydatacontent);
	$('#historyModal').modal('show');
	preload('hide');
}

/*function searchLeave(){
	alertMessage('Please confirm to Search.','searchLeaveCallback');
}
*/
function searchLeave(){

	preload('show');
	var f = document.getElementById('createFrm');

	f.method  =  'post';
	f.enctype =  'multipart/form-data';
	f.target  =  'updateFrm';
	f.action  =  BASEURL+"/report/processFrm",
	f.submit();
}
function successLeaveCallBack(){
	//location.reload();
	location.hash = '/report/leave/?'+new Date().getTime();
	//location.hash = ''
}

function clearLeave(){

	preload('show');
	var f = document.getElementById('clearFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/report/processFrm",
	f.submit();
}
function clearWorking(memcheckC){
	memcheck = memcheckC;
	preload('show');
	var f = document.getElementById('clearFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/report/processFrm",
	f.submit();
}
dataid = '0';
function approveLeave(get_dataid){
	dataid = get_dataid;
	//alert(get_dataid);
	alertMessage('Please confirm to Approve.','approveLeaveCallback');
}
function approveLeaveCallback(){
	if(dataid!='0'){
		ajaxRequestProcess('report/approveleave/'+dataid,approveLeavesCallback);
	}else{
		return;
	}

}
function approveLeavesCallback(){
	//location.reload();
	location.hash = '/report/leave/?'+new Date().getTime();
}

function cancelLeave(get_dataid){
	dataid = get_dataid;

	alertMessage('Please confirm to Cancel.','cancelLeaveCallback');
}
function cancelLeaveCallback(){
	if(dataid!='0'){
		ajaxRequestProcess('report/cancelleave/'+dataid,cancelLeavesCallback);
	}else{
		return;
	}

}

function rejectLeave(elems){
	dataid = $(elems).attr('data-id');
	alertMessage('Please confirm to Cancel Leave.','rejectLeaveCallback');
}
function rejectLeaveCallback(){
	if(dataid!='0'){
		ajaxRequestProcess('report/rejectleave/'+dataid,cancelLeavesCallback);
	}else{
		return;
	}

}


function cancelLeavesCallback(){
	//location.reload();
	location.hash = '/report/leave/?'+new Date().getTime();
}

function linktoLeave(mylink){
	$('#historyModal').modal('hide');
	setTimeout(function(){
		location.hash = mylink+"?module=working";
	},600);
	
}

function editRecord(my_date,my_type,my_id){
	if(my_id!='0'){
		ajaxRequestProcess('report/editWorkingHistory/'+my_id+"/?date="+my_date+"&type="+my_type,editHistoryCallback);
	}else{
		return;
	}
}
function editHistoryCallback(mydatacontent){
	$('#historyvalue').html(mydatacontent);
	$('#historyModal').modal('show');
	preload('hide');
	setTimeout(function(){
		$(".my_date2").datepicker({
		      	todayBtn: "linked",
		       language: "th",
		       autoclose: true,
		       todayHighlight: true,
		       format: 'dd-mm-yyyy'
	      });
	},1000);
	
	$(".timepicker").timepicker({
      showInputs: false,
      maxHours: 24,
      showMeridian : false,
      showSeconds: true
    });
    
	
}

function savesWorking(){
	alertMessage('Please confirm to save Working.','saveWorkingCallback');
}

function saveWorkingCallback(){

	preload('show');
	var f = document.getElementById('createFrm2');

	f.method  =  'post';
	f.enctype =  'multipart/form-data';
	f.target  =  'updateFrm';
	f.action  =  BASEURL+"/report/processFrm",
	f.submit();
}
function successSavesWorkingCallBack(mid){
	//location.reload();
	$('#historyModal').modal('hide');
	openWorkingHistory2(mid);
	//location.hash = ''
}

