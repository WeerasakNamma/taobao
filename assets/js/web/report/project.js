file_name[1] = '/assets/js/visioninter/leave/leavelist.js';

var dataid = '0';

$(function () {

       /*$("#example1").DataTable();*/
        /*$('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });*/
      $(".select2").select2();
      $(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy'
      });
      $('.box-body').on('click', '.leave-delete', function(){
			dataid = $(this).attr('data-id');
			alertMessage('Please confirm to Delete Leave.','daleteLeaveCallback');

		});


});
function daleteLeaveCallback(){
	if(dataid!='0'){
		ajaxRequestProcess('leave/delete/'+dataid,deleteLeavesCallback);
	}else{
		return;
	}

}
function deleteLeavesCallback(){
	/*preload('hide');
	window.location.hash = '#/user/userlist/';
	$('html, body').animate({ scrollTop: 0 }, 'fast');*/
	location.reload();
}

var dataids='0';
function deleteProject(myojb){
	dataids = $(myojb).attr('data-id');
	//alert(dataids);
	alertMessage('Please confirm to Delete Project.','deleteProjectFunc');
}
function deleteProjectFunc(){
	if(dataids!='0'){
		ajaxRequestProcess('/project/deletepro/'+dataids+"/",deleteProjectCallback);
	}else{
		return;
	}
}
function deleteProjectCallback(){
	//location.reload();
	location.hash = '/report/project_list/?'+new Date().getTime();
}


/*function searchProject(){
	alertMessage('Please confirm to Search.','searchProjectCallback');
}*/

function searchProject(){
	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/report/processFrm",
	f.submit();
}
function successProjectCallBack(){
	//location.reload();
	location.hash = '/report/project_list/?'+new Date().getTime();
}


function clearProject(){

	preload('show');
	var f = document.getElementById('clearFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/report/processFrm",
	f.submit();
}

function onclickUser(typed){
	if(typed=='O'){
		$('#my_user_id').show();
	}else{
		$('#my_user_id').hide();
	}
}