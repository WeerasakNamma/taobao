$(document).ready(function() {
    //alert(dashdata);
    $('.datepickerx').datepicker({
        autoclose: true,
        dateFormat: 'dd-mm-yyyy'
    });
});


function searchwithdraw() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/report/processFrm/",
        f.submit();
}

function reorderList() {
    window.location.hash = '/report/withdrawrp/?' + new Date().getTime();
}

function resetSearchwithdraw() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/report/resetsearchwithdrawrp/",
        success: function(resp) {
            window.location.hash = '/report/withdrawrp/?' + new Date().getTime();
        }
    });
}