$(function () {

      $('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });

      $('#example2').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });

      $(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy'
      });
       $(".select2").select2();


});

function searchExpenses(){
	alertMessage('Please confirm to Search.','searchExpensesCallback');
}

function searchExpensesCallback(){

	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/report/processFrm",
	f.submit();
}

function successExpensesCallBack(){
	//location.reload();
	location.hash = '/report/expenses/?'+new Date().getTime();
}

function clearExpenses(){

	preload('show');
	var f = document.getElementById('clearFrm');

    console.log(f);

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/report/processFrm",
	f.submit();
}


dataid = '0';
function approveExpenses(get_dataid){
	dataid = get_dataid;
	//alert(get_dataid);
	alertMessage('Please confirm to Approve.','approveExpensesFunc');
}
function approveExpensesFunc(){
	if(dataid!='0'){
		ajaxRequestProcess('report/approveexpenses/'+dataid,approveExpensesCallback);
	}else{
		return;
	}

}
function approveExpensesCallback(){
	//location.reload();
	location.hash = '/report/expenses/?'+new Date().getTime();
}

dataid2 = '0';
function cancelExpenses(get_dataid){
	dataid2 = get_dataid;
	//alert(get_dataid);
	alertMessage('Please confirm to Cancel.','cancelExpensesFunc');
}
function cancelExpensesFunc(){
	if(dataid2!='0'){
		ajaxRequestProcess('report/cancelexpenses/'+dataid2,calcelExpensesCallback);
	}else{
		return;
	}

}
function calcelExpensesCallback(){
	//location.reload();
	location.hash = '/report/expenses/?'+new Date().getTime();
}

var dataids='0';
function deleteExp(myojb){
	dataids = $(myojb).attr('data-id');
	alertMessage('Please confirm to Delete Expenses.','deleteExpFunc');
}
function deleteExpFunc(){
	if(dataids!='0'){
		ajaxRequestProcess('expenses/deleteexp/'+dataids,deleteExpCallback);
	}else{
		return;
	}
}
function deleteExpCallback(){
	//location.reload();
	location.hash = '/report/expenses/?'+new Date().getTime();
}