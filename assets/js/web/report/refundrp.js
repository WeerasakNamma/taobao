$(document).ready(function() {
    //alert(dashdata);
    $('.datepickerx').datepicker({
        autoclose: true,
        dateFormat: 'dd-mm-yyyy'
    });
});


function searchodrRefundrp() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/report/processFrm/",
        f.submit();
}

function reorderList() {
    window.location.hash = '/report/refundrp/?' + new Date().getTime();
}

function resetSearchRefundrp() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/report/resetsearchrefundrp/",
        success: function(resp) {
            window.location.hash = '/report/refundrp/?' + new Date().getTime();
        }
    });
}