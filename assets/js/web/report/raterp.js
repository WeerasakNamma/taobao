$(document).ready(function() {
    //alert(dashdata);
    $('.datepickerx').datepicker({
        autoclose: true,
        dateFormat: 'dd-mm-yyyy'
    });
});


function searchRate() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/report/processFrm/",
        f.submit();
}

function reorderList() {
    window.location.hash = '/report/raterp/?' + new Date().getTime();
}

function resetSearchRate() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/report/resetsearchraterp/",
        success: function(resp) {
            window.location.hash = '/report/raterp/?' + new Date().getTime();
        }
    });
}