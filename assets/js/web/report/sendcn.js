$(document).ready(function() {
    //alert(dashdata);
    $('.datepickerx').datepicker({
        autoclose: true,
        dateFormat: 'dd-mm-yyyy'
    });
});


function searchodr() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/report/processFrm/",
        f.submit();
}

function reorderList() {
    window.location.hash = '/report/sendcn/?' + new Date().getTime();
}

function resetSearch() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/report/resetsearchsendcn/",
        success: function(resp) {
            window.location.hash = '/report/sendcn/?' + new Date().getTime();
        }
    });
}