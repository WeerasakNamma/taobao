$(function () {
      $(".my_date").datepicker({
	      	todayBtn: "linked",
	       language: "th",
	       autoclose: true,
	       todayHighlight: true,
	       format: 'dd-mm-yyyy'
      });
       $(".select2").select2();



});

function saveExp(){
	alertMessage('Please confirm to Save Expenses.','saveExpCallback');
}

function saveExpCallback(){

	if($('#exp_title').val()==''){
		noticeMessage('Please Enter Expenses Title','scriptFocus("#exp_title");');
		return;
	}

	if(createFrm.elements["exp_team[]"].selectedIndex == -1) {
	  	noticeMessage('Please Select Expenses Team');
		return;
	}

	if($('#exp_date').val()==''){
		noticeMessage('Please Enter Expenses Date','scriptFocus("#exp_date");');
		return;
	}


	if($('#exp_payment').val()==''){
		noticeMessage('Please Enter Expenses Payment','scriptFocus("#exp_payment");');
		return;
	}




	preload('show');
	var f = document.getElementById('createFrm');

	f.method = 'post';
	f.enctype = 'multipart/form-data';
	f.target = 'updateFrm';
	f.action = BASEURL+"/expenses/processFrm",
	f.submit();
}
function successCallBack(){
	preload('hide');
	window.location.hash = '/expenses/';
}

function cloneFile(thisojb){
	var thisojbc =  $('#clone_c').clone().appendTo('#expenses_o');
	var divparr = $(thisojbc).parents();
	//console.log(divparr[0]['children'].length);
	var lastidx = (divparr[0]['children'].length)-1;
	$('.item-name',divparr[0]['children'][lastidx]).val('');
	$('.btn-danger',divparr[0]['children'][lastidx]).show();
	idx = 1;
	$('#expenses_o .form-group').each(function(){
	    $('.control-label',this).text("File "+idx);
	    idx++;
	});
}
function removeFile(thisojb){
	var divparr = $(thisojb).parents();
	console.log(divparr);
	$(divparr[1]).remove();
}

function selectBY(thisojb){
	var vc_val = $(thisojb).val();
	if(vc_val=='2'){
		$('#by_other').show('slow');
	}else{
		$('#by_other').hide('slow');
	}
}
var mydatadelete;
var didrive;
function removeFileT(thisojb){
	mydatadelete = $(thisojb).attr('data-id');
	didrive = $(thisojb).parents();
	alertMessage('Please confirm to Delete File.','daleteFileFunc');

}
function daleteFileFunc(){
	ajaxRequestProcess('/expenses/deletefile/'+mydatadelete+'/',callbackDeleteFileCallback)
}
function callbackDeleteFileCallback(){
	$(didrive[1]).remove();
	preload('hide');
}
var title_value;
function updateFileTitle(thisojb){
	title_value = $(thisojb).val();
	title_value = Base64.encode(title_value);
	var dataid = $(thisojb).attr('data-id');
	ajaxRequestProcess('/expenses/updatefile/'+title_value+'/?myid='+dataid,callbackUpdateFileCallback);
}
function callbackUpdateFileCallback(){
	preload('hide');
}

