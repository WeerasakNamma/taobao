$(function() {




    $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false
    });



});
var dataids = 0;

function deleteShipping(idds) {
    dataids = idds;
    alertMessage('กรุณายืนยันการลบรายการ', 'deleteShippingCallback');
}

function deleteShippingCallback() {
    if (dataids != '0') {
        ajaxRequestProcess('tracking/daleteshipping/' + dataids, successCallBack);
    } else {
        return;
    }
}

function successCallBack() {
    window.location.hash = '/tracking/shippinglist/?' + new Date().getTime();
}


var dataidexs = 0;
var datapriceesx = 0;

function setWallet(elems) {
    dataidexs = $(elems).attr('data-id');
    datapriceesx = $(elems).attr('data-price');
    alertMessage('กรุณายืนยันการชำระเงินจาก E Wallet.', 'setWalletCallback');
}

function setWalletCallback() {
    $('#preLoadSearch').show();
    $.ajax({
        type: "GET",
        url: BASEURL + "/tracking/moneywallet/?orderid=" + dataidexs + "&order_price=" + datapriceesx,
        success: function(resp) {
            window.location.hash = '/tracking/shippinglist/?' + new Date().getTime();
        }
    });
}

var dataTrackingIds = 0;

function deleteTrackingUser(idds) {
    dataTrackingIds = idds;
    alertMessage('กรุณายืนยันการลบรายการ', 'deleteTrackingCallback');
}

function deleteTrackingCallback() {
    if (dataTrackingIds != '0') {
        ajaxRequestProcess('tracking/daletetrackinguser/?tracking=' + dataTrackingIds, successTrackingCallBack);
    } else {
        return;
    }
}

function successTrackingCallBack() {
    dataTrackingIds = 0;
    window.location.hash = '/tracking/trackinglist/?' + new Date().getTime();
}