$('#select-all').click(function(event) {
    if (this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
    } else {
        $(':checkbox').each(function() {
            this.checked = false;
        });
    }
});

function saveTrackingToship() {
    var shipCh = $("#createFrm input[type=checkbox]:checked").length;
    var hasPay = $('#extrapay').val();
    
    // By Weerasak 20/09/2566
    if(hasPay > 0){
        alertMessage('มียอดค้างชำระเงินไม่สามารถทำรายการได้', '');
    }else{
        if(shipCh<=0){
            alertMessage('กรุณาเลือกรายการแจ้งรับสินค้า', '');
        }else{
            alertMessage('กรุณายืนยันการแจ้งรับสินค้า', 'saveTrackingCallback');
        }
    }
   // alert(shipCh);
    //
}

function saveTrackingCallback() {
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/tracking/processFrm/",
        f.submit();
}

function successCallBack() {
    window.location.hash = '/tracking/trackingforship/?' + new Date().getTime();
}

function deleteTrack(elems) {
    if (!confirm('กรุณายืนยันการลบรายการ Tracking.')) {
        return;
    }
    var pprent = $(elems).parents();
    $(pprent[1]).remove();
}

function saveGetshipping() {
    alertMessage('กรุณายืนยันการบันทึก Tracking.', 'saveGetshippingCallback');
}

function saveGetshippingCallback() {
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/tracking/processFrm/",
        f.submit();
}

function successShipCallBack() {
    window.location.hash = '/tracking/shippinglist/?' + new Date().getTime();
}