function saveTracking() {
    if (!confirm('กรุณายืนยันการบันทึก Tracking.')) {
        return;
    }
    if ($('#codecheck').val() == '0') {
        alert('กรุณากรอก Member Code ให้ถูกต้อง');
        $('#codecheck').focus();
        return;
    }
    $('#preLoadSearch').show();
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/ordermanagement/processFrm/",
        f.submit();
}

function successCallBack() {
    window.location.hash = '/ordermanagement/tracking/?' + new Date().getTime();
}

function cehcekmembercode() {
    var dtsc = $('#codecheck').val();
    if (dtsc == '0') {
        alert('กรุณากรอก Member Code ให้ถูกต้อง');
        $('#member_code').focus();
        return;
    }
}

function checkmemberinfo(elems) {
    var dataVal = $(elems).val();

    $.ajax({
        type: "GET",
        url: BASEURL + "/ordermanagement/checkmembercode/?code=" + dataVal,
        success: function(resp) {
            if (resp == 'T') {
                $('#codecheck').val('1');
            } else {
                $('#codecheck').val('0');
                alert('กรุณากรอก Member Code ให้ถูกต้อง');
                $('#member_code').focus();
                return;
            }
        }
    });
}

function viewTK(elem) {
    var imgss = $(elem).attr('data-img');
    $('#tkimg').attr('src', imgss);
    $('#tkModal').modal('show');
}