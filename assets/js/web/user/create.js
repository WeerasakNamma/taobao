$(function() {
    //$("#birthdate").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
    // $("#startdate").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});

    /*$('body').on('click', '#saveUser', function(){ 
			alertMessage('Please confirm to Create Employee.','saveUserCallback');
	  });*/
    $(".my_date").datepicker({
        todayBtn: "linked",
        language: "th",
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });

});


function saveUser() {
	 if ($('#user_name').val() == '') {
        noticeMessage('Please Enter First Name - Last Name', 'scriptFocus("#user_name");');
        return false;
    }

    /*if ($('#user_nickname').val() == '') {
        noticeMessage('Please Enter Nick Name', 'scriptFocus("#user_nickname");');
        return;
    }*/

    if ($('#user_email').val() == '') {
        noticeMessage('Please Enter Email', 'scriptFocus("#user_email");');
        return false;
    }

    if (!IsEmail($('#user_email').val())) {
        noticeMessage('Please Enter Email Fotmat', 'scriptFocus("#user_email");');
        return false;
    }

    if ($('#user_phone_number').val() == '') {
        noticeMessage('Please Enter User Phone Number', 'scriptFocus("#user_phone_number");');
        return false;
    }
    if ($('#province').val() == '0') {
        noticeMessage('Please Select Province', 'scriptFocus("#province");');
        return false;
    }
	if ($('#amphur-register').val() == '') {
        noticeMessage('Please Select District', 'scriptFocus("#amphur-register");');
        return;
    }
	if ($('#district-register').val() == '') {
        noticeMessage('Please Select Sub District', 'scriptFocus("#district-register");');
        return false;
    }
	 
    /*if($('#user_other').val()==''){
    	noticeMessage('Please Enter Other Type');
    	$('#user_other').focus();
    	return;
    }*/

    if ($('#user_username').val() == '') {
        noticeMessage('Please Enter Username', 'scriptFocus("#user_username");');
        //$('#user_username').focus();
        return;
    }
    if($('#bank_id').val() == '0'){
        noticeMessage('กรุณาเลือกธนาคาร', 'scriptFocus("#bank_id");');
        return;   
    }
    if($('#user_bookbank').val() == ''){
        noticeMessage('กรุณาระบุเลขบัญชีธนาคาร', 'scriptFocus("#user_username");');
        return;   
    }
    /*if($('#user_password').val()==''){
    	noticeMessage('Please Enter Password');
    	$('#user_password').focus();
    	return;
    }*/
    alertMessage('Please confirm to save employee.', 'saveUserCallback');
}

function saveUserCallback() {

   

    preload('show');
    var f = document.getElementById('createFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/user/processFrm",
        f.submit();
}

function successCallBack() {
    preload('hide');
    location.hash = '/user/userlist/'
}

function checkUsername(myselector) {
    var lengthtext = $(myselector).val().length;
    if (lengthtext < 6) {
        noticeMessage('Please Enter Username more than 6 charracters', 'scriptFocus("#user_username");');
        return;
    }
    ajaxRequestProcess('user/checkusername/?name=' + $(myselector).val(), checkUsernameCallback);
}

function checkUsernameCallback(resp) {
    preload('hide');
    if (resp == 'F') {
        $('#save_emp_btn').hide();
        noticeMessage("Username Invalid, please try again.", 'scriptFocus("#user_username");');
        return;
    } else {
        $('#save_emp_btn').show();
    }
}

function saveRate() {
    alertMessage('Please confirm to save rate.', 'saveRateCallback');
}

function saveRateCallback() {
    preload('show');
    var f = document.getElementById('createFrm2');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/user/processFrm",
        f.submit();
}

function saveRateSccCallback() {
    window.location.hash = '/user/edit/' + $('#vuid').val() + '/?' + new Date().getTime();
}

var rrtid = 0;

function dalrate(vrate) {
    rrtid = vrate;
    alertMessage('Please confirm to dalete rate.', 'dalrateCallback');
}

function dalrateCallback() {

    ajaxRequestProcess('user/delrate/?id=' + rrtid, saveRateSccCallback);
}

function changeProvince(elems) {
    var province_code = $(elems).val();
    if (province_code == '') {
        return;
    }
    $.ajax({
        type: "GET",
        url: BASEURL + "/user/amphur/?province=" + province_code,
        success: function(resp) {
            $('#amphur').html(resp);
            $('#amphur').show();
            $('#district').hide();
        }
    });
}

function changeAmphur(elems) {
    var amphur_code = $(elems).val();
    if (amphur_code == '') {
        return;
    }
    $.ajax({
        type: "GET",
        url: BASEURL + "/user/district/?amphur=" + amphur_code,
        success: function(resp) {
            $('#district').html(resp);
            $('#district').show();
        }
    });
}