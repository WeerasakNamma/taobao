var dataid = '0';

$(function() {

    /*$("#example1").DataTable();*/
    /*$('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false
      });*/


    $('.box-body').on('click', '.user-delete', function() {
        dataid = $(this).attr('data-id');
        alertMessage('กรุณายืนยันการลบผู้ใช้', 'daleteUserCallback');

    });

    $(".my_date").datepicker({
        todayBtn: "linked",
        language: "th",
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });


});

function daleteUserCallback() {
    if (dataid != '0') {
        ajaxRequestProcess('user/delete/' + dataid, deleteCallback);
    } else {
        return;
    }

}

function deleteCallback() {
    /*preload('hide');
    window.location.hash = '#/user/userlist/';
    $('html, body').animate({ scrollTop: 0 }, 'fast');*/
    //location.reload();
    window.location.hash = '/user/userlist/?' + new Date().getTime();
}

function searchMember() {

    var f = document.getElementById('searchFrm');

    f.method = 'post';
    f.enctype = 'multipart/form-data';
    f.target = 'updateFrm';
    f.action = BASEURL + "/user/processFrm/",
        f.submit();
}

function memberListCallback() {
    window.location.hash = '/user/userlist/?' + new Date().getTime();
}

function resetSearchMember() {
    $.ajax({
        type: "GET",
        url: BASEURL + "/user/resetSearchMember/",
        success: function(resp) {
            memberListCallback()
        }
    });
}