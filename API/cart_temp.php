<?php
session_start();
?>
<div class="panel panel-warning">
                                <div class="panel-heading">
                                    <i class="fa fa-shopping-cart"></i> ตะกร้าสินค้า
                                </div>
                                <div class="panel-body">
                                    <form>
                                        <div id="preLoadSearch2" style="text-align: center;display:none;">
                                            <img src="<?=BASEURL?>/assets/template/back-end/images/712.GIF" style="max-width: 50px;" /><br /> Please Wait...
                                        </div>
                                        <?php if(count($_SESSION['item_tmp'])>0){ ?>
                                        <table class="table table-hover" id="myTable">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;" colspan="2">รายการ</th>
                                                    <th style="text-align: center;">
                                                        <nobr>ราคาหยวน</nobr>
                                                    </th>
                                                    <th style="text-align: center;">จำนวน</th>
                                                    <th style="text-align: center;">
                                                        <nobr>รวม (หยวน)</nobr>
                                                    </th>
                                                    <th style="text-align: center;">
                                                        <nobr>รวม (บาท)</nobr>
                                                    </th>
                                                    <th style="text-align: center;">
                                                        <nobr>หมายเหตุ</nobr>
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="basket_member">
                                                <?php $preserved = array_reverse($_SESSION['item_tmp'], true); ?>
                                                <?php $total_th = 0; $total_cn = 0; foreach($preserved as $key=>$itm){ ?>
                                                <?php
                                                    $total_th +=  $itm['product_price_thb']*$itm['product_qty'];
                                                    $total_cn +=  $itm['product_price_rmb']*$itm['product_qty'];
                                                  ?>
                                                    <tr>
                                                        <th width="100" style="text-align: center;">
                                                            <img src="<?=$itm['product_image']?>" width="60" alt="" /><br />
                                                            <!-- <div class="fileUpload btn btn-primary btn-sm">
                                                                <span class="up-text23">Upload</span><span id="preTemp23" style="display: none;"><i class="fa fa-spinner fa-spin"></i> Uploading...</span>

                                                            </div> -->
                                                        </th>
                                                        <td width="60%">
                                                            <a href="<?=$itm['product_link']?>" target="_blank">
                                                                <?=$itm['product_name']?>
                                                            </a><br /> ร้านค้า :
                                                            <a href="<?=$itm['saller_link']?>" target="_blank">
                                                                <?=$itm['saller_name']?>
                                                            </a><br />
                                                            <?php if(count($itm['product_option'] )>0){ ?>
                                                            <?php foreach($itm['product_option'] as $keyn=>$val_op){ ?>
                                                            <?=$val_op['op_name_th']?> : <input style="width:180px;margin-bottom: 5px;" type="text" value="<?=$val_op['op_value_th']?>" data-keyn="<?=$keyn?>" data-key="<?=$key?>" onchange="updateOption(this);" />
                                                                <!--<b><?=$val_op['op_value_th']?></b>--><br />
                                                                <?php } ?>
                                                                <?php } ?>
                                                                <br />
                                                                
                                                        </td>
                                                        <td style="text-align: center;">&yen;
                                                            <?=@number_format($itm['product_price_rmb'],2)?>
                                                        </td>
                                                        <td align="right"> <input maxlength="4" id="qty_<?=$key?>" style="width:60px; text-align:center;" type="text" value="<?=$itm['product_qty']?>" onchange="updateTempQTY('<?=$key?>');" /> </td>
                                                        <td style="text-align: center;">
                                                            <?=@number_format(($itm['product_price_rmb']*$itm['product_qty']),2)?>
                                                        </td>
                                                        <td style="text-align: center;">
                                                            <?=@number_format(($itm['product_price_rmb']*$itm['product_qty'])*$_SESSION['thrate'],2)?>
                                                        </td>
                                                        <td>
                                                            <textarea data-key="<?=$key?>" onchange="updateTempComment(this);" style="min-width: 200px;" class="form-control"><?=$itm['comment']?></textarea>
                                                        </td>
                                                        <td valign="middle">
                                                            <button type="button" class="btn btn-danger btn-xs" onclick="removeCartTempItem('<?=$key?>');"><i class="fa fa-times"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                            </tbody>
                                        </table>
                                        <?php }else{ ?>
                                        <div class="panel text-center" role="alert">Empty Cart</div>
                                        <?php } ?>
                                    </form>
                                </div>
                                <div class="panel-footer" style="text-align: right;">
                                    <?php if(count($_SESSION['item_tmp'])>0){ ?>
                                    <a href="#/buyship/tempToCart" class="btn btn-warning"><i class="fa fa-credit-card" aria-hidden="true"></i> ตะกร้าสินค้า</a>
                                    <?php } ?>
                                </div>
                            </div>