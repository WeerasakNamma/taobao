<?php
   date_default_timezone_set('Asia/Bangkok');
   include('libs/Webservice.class.php');
   include('libs/medoo.php');
   include('config.php');
//error_reporting(E_ALL);
   $db = new medoo([
   	// required
   	'database_type' => 'mysql',
   	'database_name' => $db_database_name,
   	'server' => 'localhost',
   	'username' => $db_username,
   	'password' => $db_password,
   	'charset' => 'utf8',

   	// [optional]
   	'port' => 3306,

   	// [optional] Table prefix
   	'prefix' => '',

   	// [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
   	'option' => [
   		PDO::ATTR_CASE => PDO::CASE_NATURAL
   	]
   ]);

   $param_config = array();
   $param_config['service_url'] = 'http://www.accgroup.co.th/act-system/webservice/userrate.php';
   $param_config['service_appId'] = 'xxxxxxxxxxx';

   $webservice = new Webservice($param_config);
   
  
   if(empty($_GET['token'])){ echo 'Service Not Available.'; exit(); }
   
   $service = $_GET['token'];
   $groupID = $_GET['gId'];
   $service_token = trim($_GET['service_token']);
   
   if($service=="UPDATE_MEMBER_RATE_OLD"){ //Edit Weerasak
	   
			$datauser_rate = $webservice->sendDataTowebService($service);
			$dataUser = $db->select('project_user',"*",array("user_role_id"=>$groupID));
	    	$dataRate = $db->select('project_user_rate',"*",array("user_id"=>$groupID));
			$dataInArr = array();
			if(count($dataUser)>=1){
				$i=0;
				foreach($dataUser as $vald){
					
					$dataInArr[$i]['user_code'] = $vald['user_code'];
					
					$j=0;
					foreach($dataRate as $vals){
						
						$dataInArr[$i]['user_data_rate'][$j]['rate_user_code'] 		= $vald['user_code'];
						$dataInArr[$i]['user_data_rate'][$j]['rate_goods_code'] 	= $vals['unit_code'];
						$dataInArr[$i]['user_data_rate'][$j]['rate_price'] 				= $vals['rate'];
						$dataInArr[$i]['user_data_rate'][$j]['rate_status'] 			= 1;
							
						$j++;
					}
					$i++;
				}
			}
			$data = json_encode($dataInArr);
			
			$randomString = substr(str_shuffle("123456789012345678901234567890123456789012345678901234567890"), 0, 20);
        
	    	$dd = $db->insert("project_webservice",array(
	    										"service_token"=>"".$randomString,
	    										"service_method"=>'UPDATE_RATE',
	    										"service_data"=>"".$dataInArr,
	    										"status"=>'N',
	    										"create_dtm"=>date('Y-m-d H:i:s'),
	    										"update_dtm"=>date('Y-m-d H:i:s')
                                                ));
			
			
	   print_r($dd);
	//Edit Weerasak 20/01/2565
	}else if($service =="SYNC_MEMBER_RATE"){
		$webservice->sendDataTowebService('UPDATE_MEMBER_RATE');		
	}else if($service == "MEMBER_RATE"){
		$result = $db->select("project_webservice",array("id","service_data"),["AND"=>[
			"service_method"=>"SYNC_MEMBER_RATE",
			"status"=>"N"
		]]);
		//echo $db->last_query();
		$std = new stdClass();
		$std->service = 'UPDATE_MEMBER_RATE';
		$std->data = $result;
		echo json_encode($std);		

		foreach($result as $vals){
		$db->update("project_webservice",array(
			"status"=>'Y',
			"update_dtm"=>date('Y-m-d H:i:s'))
			,array("id"=>$vals['id'])
		);
		}
   	}
	//END
   

   
 ?>