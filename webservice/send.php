<?php
   date_default_timezone_set('Asia/Bangkok');
   include('libs/Webservice.class.php');
   include('libs/medoo.php');
   include('config.php');

   $db = new medoo([
   	// required
   	'database_type' => 'mysql',
   	'database_name' => $db_database_name,
   	'server' => 'localhost',
   	'username' => $db_username,
   	'password' => $db_password,
   	'charset' => 'utf8',

   	// [optional]
   	'port' => 3306,

   	// [optional] Table prefix
   	'prefix' => '',

   	// [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
   	'option' => [
   		PDO::ATTR_CASE => PDO::CASE_NATURAL
   	]
   ]);

   $param_config = array();
  // $param_config['service_url'] = 'http://api.siamgolive.com/webservice/receive.php';
   $param_config['service_url'] = 'http://trackingsystem.taobaochinacargo.com/webservice/receive.php';
   $param_config['service_appId'] = 'xxxxxxxxxxx';

   $webservice = new Webservice($param_config);


   if(empty($_GET['service'])){ echo 'Service Not Available.'; exit(); }

   $service = $_GET['service'];
   $trackng = trim($_GET['tracking']);

   if($service=='insert'){

      $dataTracking = $db->get('web_tracking',array(
         "tracking","member_code","saller_name","order_id","order_type","status","weight","weight_cn","dime_x","dime_y","dime_w","order_comment"
      ),array("tracking"=>$trackng));

      if($dataTracking!=''){
         $result = $webservice->insertService($dataTracking);
         echo json_encode($result);
      }else{
         $std = new stdClass();
         $std->status = '100';
         $std->message = 'not found tracking';
         echo json_encode($std);
      }


   }

   else if($service=='update'){

      $dataTracking = $db->get('web_tracking',array(
         "tracking","member_code","saller_name","order_id","order_type","status","weight","weight_cn","dime_x","dime_y","dime_w","order_comment"
      ),array("tracking"=>$trackng));

      if($dataTracking!=''){
         $result = $webservice->updateService($dataTracking);
         echo json_encode($result);
      }else{
         $std = new stdClass();
         $std->status = '100';
         $std->message = 'not found tracking';
         echo json_encode($std);
      }


   }
   else if($service=='sync'){

      $dataTracking = $db->select('web_tracking',array(
         "tracking","member_code","saller_name","order_id","order_type","status","weight","weight_cn","dime_x","dime_y","dime_w","order_comment"
      ),array("sync_status"=>'N'));

      $total = count($dataTracking);
      $success = 0;
      $failer = 0;

      if(count($dataTracking)>0){
         foreach($dataTracking as $vals){
            $result = $webservice->updateService($vals);
            if($result->status=='200'){
               $db->update("web_tracking",array("sync_status"=>'Y'),array("tracking"=>$vals['tracking']));
               $success++;
            }else{
               $failer++;
            }
         }
      }
      $std = new stdClass();
      $std->total = $total;
      $std->success = $success;
      $std->failer = $failer;
      echo json_encode($std);
   }
   else if($service=='syncserver'){
      $result = $webservice->syncService();
      //$dataunit = $result->dataunit;
      //print_r($result->dataunit[0]->tracking);
      $dataUnit = $result->dataunit;
      if(count($dataUnit)>0){
         foreach ($dataUnit as $key => $value) {
            $checkTrack = $db->get('web_tracking',array("tracking","tracking_id"),array("tracking"=>$value->tracking));
            if($checkTrack['tracking']!=''){

               $dataInsert = (array)$value;
               $dataInsert['update_dtm'] = date('Y-m-d H:i:s');
               $dataInsert['update_by'] = '0';
               $dataInsert['sync_status'] = 'Y';

               $member_info = $db->get("project_user","*",array("user_code"=>$value->member_code));

               $price_true = 0;
               $price_dimen = 0;
               $cbm = 0;
               $price_weight = 0;
               if($member_info['id']!=''){


                  $dimen_rate = $db->get("project_user_rate","*",array(
                     "AND"=>array(
                        "user_id"=>$member_info['id'],
                        "unit"=>'16',
                        "order_type"=>$value->order_type,
                        "ship_by"=>'4'
                     )
                  ));

                  $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
                  $price_dimen = $cbm*$dimen_rate['rate'];


                  $weight_rate = $db->get("project_user_rate","*",array(
                     "AND"=>array(
                        "user_id"=>$member_info['id'],
                        "unit"=>'17',
                        "order_type"=>$value->order_type,
                        "ship_by"=>'4'
                     )
                  ));

                  $price_weight = $weight_rate['rate']*$value->weight;

                  $price_true = $price_dimen;

                  if($price_weight>$price_dimen){
                     $price_true = $price_weight;
                  }

               }
               $dataInsert['price_weight'] = $price_weight;
               $dataInsert['price_dimen'] = $price_dimen;
               $dataInsert['price_true'] = $price_true;
               $dataInsert['cbm'] = $cbm;

               $last_id = $db->update("web_tracking",$dataInsert,array("tracking"=>$value->tracking));


               $data_history = array();
               $data_history['tracking_id'] = $checkTrack['tracking_id'];
               $data_history['status'] = $value->status;
               $data_history['create_dtm'] = date('Y-m-d H:i:s');
               $data_history['create_by'] = '0';

               $log_history = $db->insert("web_tracking_history",$data_history);

               $dataUpdateItem = array();
               $dataUpdateItem['status'] = $value->status;
               $dataUpdateItem['product_sku_1'] = $value->tracking;
               $dataUpdateItem['create_dtm'] = date('Y-m-d H:i:s');

               $updateOrderItem = $db->update("web_order_item",$dataUpdateItem,array(
                  "AND"=>array(
                     "order_number"=>$value->order_id,
                     "saller_number"=>$value->saller_name,
                     "status[!]"=>'D',
                     "status[!]"=>'14'
                  )
               ));

            }else{
               $dataInsert = (array)$value;
               $dataInsert['create_dtm'] = date('Y-m-d H:i:s');
               $dataInsert['create_by'] = '0';
               $dataInsert['sync_status'] = 'Y';

               $member_info = $db->get("project_user","*",array("user_code"=>$value->member_code));

               $price_true = 0;
               $price_dimen = 0;
               $cbm = 0;
               $price_weight = 0;
               if($member_info['id']!=''){


                  $dimen_rate = $db->get("project_user_rate","*",array(
                     "AND"=>array(
                        "user_id"=>$member_info['id'],
                        "unit"=>'16',
                        "order_type"=>$value->order_type,
                        "ship_by"=>'4'
                     )
                  ));

                  $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
                  $price_dimen = $cbm*$dimen_rate['rate'];


                  $weight_rate = $db->get("project_user_rate","*",array(
                     "AND"=>array(
                        "user_id"=>$member_info['id'],
                        "unit"=>'17',
                        "order_type"=>$value->order_type,
                        "ship_by"=>'4'
                     )
                  ));

                  $price_weight = $weight_rate['rate']*$value->weight;

                  $price_true = $price_dimen;

                  if($price_weight>$price_dimen){
                     $price_true = $price_weight;
                  }

               }
               $dataInsert['price_weight'] = $price_weight;
               $dataInsert['price_dimen'] = $price_dimen;
               $dataInsert['price_true'] = $price_true;
               $dataInsert['cbm'] = $cbm;

               $last_id = $db->insert("web_tracking",$dataInsert);


               $data_history = array();
               $data_history['tracking_id'] = $last_id;
               $data_history['status'] = $value->status;
               $data_history['create_dtm'] = date('Y-m-d H:i:s');
               $data_history['create_by'] = '0';

               $log_history = $db->insert("web_tracking_history",$data_history);

               $dataUpdateItem = array();
               $dataUpdateItem['status'] = $value->status;
               $dataUpdateItem['product_sku_1'] = $value->tracking;
               $dataUpdateItem['create_dtm'] = date('Y-m-d H:i:s');

               $updateOrderItem = $db->update("web_order_item",$dataUpdateItem,array(
                  "AND"=>array(
                     "order_number"=>$value->order_id,
                     "saller_number"=>$value->saller_name,
                     "status[!]"=>'D',
                     "status[!]"=>'14'
                  )
               ));
            }
         }
      }

   }

 ?>
