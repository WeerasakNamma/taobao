<?php

   // Service:
   //    'insert': add new tracking;
   //    'update': update tracking;
   //    'sync': sync tracking;
   date_default_timezone_set('Asia/Bangkok');
   include('libs/Webservice.class.php');
   include('libs/medoo.php');
   include('config.php');

   $webservice = new Webservice($param_config);

   $json_return = new stdClass();
   $json_return->status = '100';

   //print_r($_POST['data']);

   if(empty($_POST['data'])){
      $json_return->status = '100';
      $json_return->message = 'Service Not Available.';
      echo json_encode($json_return);
      exit();
   }

   $data = json_decode($_POST['data']);

   $service = $data->service;
   $dataUnitRecieve = $data->dataunit;


   if($service=='token'){

      $token = $webservice->genTokenKey(20);
      $json_return->status = '200';
      $json_return->token = $token;

   }

   else if($service=='insert'){
      trackinginst($dataUnitRecieve);
      $json_return->status = '200';
   }
   else if($service=='update'){
      trackinginst($dataUnitRecieve);
      $json_return->status = '200';
   }else{
      $json_return->status = '100';
      $json_return->message = 'Service Not Available.';
   }


   echo json_encode($json_return);



   function trackinginst($value){

      $db = new medoo([
      	// required
      	'database_type' => 'mysql',
      	'database_name' => $db_database_name,
      	'server' => 'localhost',
      	'username' => $db_username,
      	'password' => $db_password,
      	'charset' => 'utf8',

      	// [optional]
      	'port' => 3306,

      	// [optional] Table prefix
      	'prefix' => '',

      	// [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
      	'option' => [
      		PDO::ATTR_CASE => PDO::CASE_NATURAL
      	]
      ]);

         $checkTrack = $db->get('web_tracking',array("tracking","tracking_id"),array("tracking"=>$value->tracking));
         if($checkTrack['tracking']!=''){

            $dataInsert = (array)$value;
            $dataInsert['update_dtm'] = date('Y-m-d H:i:s');
            $dataInsert['update_by'] = '0';
            $dataInsert['sync_status'] = 'Y';

            $member_info = $db->get("project_user","*",array("user_code"=>$value->member_code));

            $price_true = 0;
            $price_dimen = 0;
            $cbm = 0;
            $price_weight = 0;
            $price_rate = '';
            if($member_info['id']!=''){


                // $dimen_rate = $db->get("project_user_rate","*",array(
                //     "AND"=>array(
                //         "user_id"=>$member_info['id'],
                //         "unit"=>'16',
                //         "order_type"=>$value->order_type,
                //         "ship_by"=>'4'
                //     )
                // ));

                // $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
                // $price_dimen = $cbm*$dimen_rate['rate'];


                // $weight_rate = $db->get("project_user_rate","*",array(
                //     "AND"=>array(
                //         "user_id"=>$member_info['id'],
                //         "unit"=>'17',
                //         "order_type"=>$value->order_type,
                //         "ship_by"=>'4'
                //     )
                // ));

                // $price_weight = $weight_rate['rate']*$value->weight;

                // $price_true = $price_dimen;

                // if($price_weight>$price_dimen){
                //     $price_true = $price_weight;
                // }
                $dataRate = $db->select('project_user_rate',"*",array(
                    "AND"=>array(
                        "order_type"=>$value->order_type,
                        "user_id"=>$member_info['user_role_id']
                        )
                ));

                foreach($dataRate as $valsRate){
                    $masterCK = $db->get("project_master","*",array("id"=>$valsRate['unit']));

                    if($masterCK['master_char']=='CALK'){
                        $price_weight = $valsRate['rate']*$value->weight;
                        if($price_true<$price_weight){
                            $price_true = $price_weight;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }
                    if($masterCK['master_char']=='CALQ'){
                        $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
                        $price_dimen = $cbm*$dimen_rate['rate'];
                        if($price_true<$price_dimen){
                            $price_true = $price_dimen;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }
                }

            }
            $dataInsert['price_weight'] = $price_weight;
            $dataInsert['price_dimen'] = $price_dimen;
            $dataInsert['price_true'] = $price_true;
            $dataInsert['price_rate'] = $price_rate;
            $dataInsert['cbm'] = $cbm;

            $last_id = $db->update("web_tracking",$dataInsert,array("tracking"=>$value->tracking));


            $data_history = array();
            $data_history['tracking_id'] = $checkTrack['tracking_id'];
            $data_history['status'] = $value->status;
            $data_history['create_dtm'] = date('Y-m-d H:i:s');
            $data_history['create_by'] = '0';

            $log_history = $db->insert("web_tracking_history",$data_history);

            $dataUpdateItem = array();
            $dataUpdateItem['status'] = $value->status;
            $dataUpdateItem['product_sku_1'] = $value->tracking;
            $dataUpdateItem['create_dtm'] = date('Y-m-d H:i:s');

            $weborderArr = $db->get("web_order",array("id"),array("order_number"=>$value->order_id));
            $updateOrderItem = $db->update("web_order_item",$dataUpdateItem,array(
               "AND"=>array(
                  "order_id"=>$weborderArr['id'],
                  "saller_number"=>$value->saller_name,
                  "status[!]"=>'D',
                  "status[!]"=>'14'
               )
            ));

         }else{
            $dataInsert = (array)$value;
            $dataInsert['create_dtm'] = date('Y-m-d H:i:s');
            $dataInsert['create_by'] = '0';
            $dataInsert['sync_status'] = 'Y';

            $member_info = $db->get("project_user","*",array("user_code"=>$value->member_code));

            $price_true = 0;
            $price_dimen = 0;
            $cbm = 0;
            $price_weight = 0;
            $price_rate = '';
            if($member_info['id']!=''){


                // $dimen_rate = $db->get("project_user_rate","*",array(
                //     "AND"=>array(
                //         "user_id"=>$member_info['id'],
                //         "unit"=>'16',
                //         "order_type"=>$value->order_type,
                //         "ship_by"=>'4'
                //     )
                // ));

                // $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
                // $price_dimen = $cbm*$dimen_rate['rate'];


                // $weight_rate = $db->get("project_user_rate","*",array(
                //     "AND"=>array(
                //         "user_id"=>$member_info['id'],
                //         "unit"=>'17',
                //         "order_type"=>$value->order_type,
                //         "ship_by"=>'4'
                //     )
                // ));

                // $price_weight = $weight_rate['rate']*$value->weight;

                // $price_true = $price_dimen;

                // if($price_weight>$price_dimen){
                //     $price_true = $price_weight;
                // }
                $dataRate = $db->select('project_user_rate',"*",array(
                    "AND"=>array(
                        "order_type"=>$value->order_type,
                        "user_id"=>$member_info['user_role_id']
                        )
                ));

                foreach($dataRate as $valsRate){
                    $masterCK = $db->get("project_master","*",array("id"=>$valsRate['unit']));

                    if($masterCK['master_char']=='CALK'){
                        $price_weight = $valsRate['rate']*$value->weight;
                        if($price_true<$price_weight){
                            $price_true = $price_weight;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }
                    if($masterCK['master_char']=='CALQ'){
                        $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
                        $price_dimen = $cbm*$dimen_rate['rate'];
                        if($price_true<$price_dimen){
                            $price_true = $price_dimen;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }
                }

            }
            $dataInsert['price_weight'] = $price_weight;
            $dataInsert['price_dimen'] = $price_dimen;
            $dataInsert['price_true'] = $price_true;
            $dataInsert['price_rate'] = $price_rate;
            $dataInsert['cbm'] = $cbm;

            $last_id = $db->insert("web_tracking",$dataInsert);


            $data_history = array();
            $data_history['tracking_id'] = $last_id;
            $data_history['status'] = $value->status;
            $data_history['create_dtm'] = date('Y-m-d H:i:s');
            $data_history['create_by'] = '0';

            $log_history = $db->insert("web_tracking_history",$data_history);

            $dataUpdateItem = array();
            $dataUpdateItem['status'] = $value->status;
            $dataUpdateItem['product_sku_1'] = $value->tracking;
            $dataUpdateItem['create_dtm'] = date('Y-m-d H:i:s');

            $weborderArr = $db->get("web_order",array("id"),array("order_number"=>$value->order_id));
            $updateOrderItem = $db->update("web_order_item",$dataUpdateItem,array(
               "AND"=>array(
                  "order_id"=>$weborderArr['id'],
                  "saller_number"=>$value->saller_name,
                  "status[!]"=>'D',
                  "status[!]"=>'14'
               )
            ));
         }

   }

 ?>
