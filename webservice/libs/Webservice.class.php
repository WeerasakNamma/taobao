<?php

   /**
    *
    */
   class Webservice {

      private $service_url;
      private $service_appId;

      function __construct($params_config){
         $this->service_url = $params_config['service_url'];
         $this->service_appId = $params_config['service_appId'];
      }

      public function genTokenKey($length){
         $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
   		return $randomString;
      }

      public function insertService($dataTracking){
         $dataArray = array();
         $dataArray['service'] = 'insert';
         $dataArray['dataunit'] = $dataTracking;
         $resulereturn = $this->sendDataService($dataArray);
         return $resulereturn;
      }
      public function updateService($dataTracking){
         $dataArray = array();
         $dataArray['service'] = 'update';
         $dataArray['dataunit'] = $dataTracking;
         $resulereturn = $this->sendDataService($dataArray);
         return $resulereturn;
      }

      private function sendDataService($data){
         $requestToken = $this->requestToken();
		
         if($requestToken->status=='200'){
            $result = $this->sendDataTowebService($data);
            return $result;
         }else{
            $std = new stdClass();
            $std->status = '100';
            $std->message = 'request error';
            return $std;
         }

      }

      private function requestToken(){

         $ch = curl_init( $this->service_url);

         $dataPost = array("service_appId"=> $this->service_appId,"service"=>"token");
         $payload = json_encode($dataPost);

         curl_setopt( $ch, CURLOPT_POSTFIELDS, array("data"=>$payload) );
         //curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
         curl_setopt($ch, CURLOPT_TIMEOUT,10);
         curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);

         $result = curl_exec($ch);
         $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         // echo $httpcode."<br />";
         // echo $result;
         if($httpcode=='200'){
            return json_decode($result);
         }else{
            $std = new stdClass();
            $std->status = '100';
            $std->message = 'request error';
            return $std;
         }

      }
      public function sendDataTowebService($token){

	        $ch = curl_init( $this->service_url."?token=".$token);

	        $header[0] = 'Accept: text/xml,application/xml,application/xhtml+xml,'
	                . 'text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
			$header[] = 'Cache-Control: max-age=0';
			$header[] = 'Connection: keep-alive';
			$header[] = 'Keep-Alive: 300';


			$cookieFile = "cookie_china";

			$options = array(
			            CURLOPT_RETURNTRANSFER => true,
			            CURLOPT_HEADER => false,
			            CURLOPT_FOLLOWLOCATION => true,
			            CURLOPT_ENCODING => 'gzip,deflate',
			            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0 FirePHP/0.6',
			            CURLOPT_AUTOREFERER => true,
			            CURLOPT_CONNECTTIMEOUT => 120,
			            CURLOPT_TIMEOUT => 120,
			            CURLOPT_MAXREDIRS => 10,
			            CURLOPT_SSL_VERIFYHOST => 0,
			            CURLOPT_SSL_VERIFYPEER => false,
			            CURLOPT_VERBOSE => 1,
			            CURLOPT_HTTPHEADER => $header,
			            CURLOPT_COOKIEFILE => $cookieFile,
			            CURLOPT_COOKIEJAR => $cookieFile,
			);

			curl_setopt_array($ch, $options);

			$dataResult = curl_exec($ch);
			
			return $dataResult;

      }
      

   }


?>
