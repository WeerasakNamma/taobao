<?php
//exit;
   date_default_timezone_set('Asia/Bangkok');
   include('libs/Webservice.class.php');
   include('libs/medoo.php');
   include('config.php');
   require '../assets/libs/PHPMailer/PHPMailerAutoload.php';


//error_reporting(E_ALL);
   $db = new medoo([
   	// required
   	'database_type' => 'mysql',
   	'database_name' => $db_database_name,
   	'server' => 'localhost',
   	'username' => $db_username,
   	'password' => $db_password,
   	'charset' => 'utf8',

   	// [optional]
   	'port' => 3306,

   	// [optional] Table prefix
   	'prefix' => '',

   	// [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
   	'option' => [
   		PDO::ATTR_CASE => PDO::CASE_NATURAL
   	]
   ]);

   $param_config = array();
   $param_config['service_url'] = 'http://accgroup.co.th/act-test/webservice/api.php';//'http://accgroup.co.th/act-system/webservice/api.php';
   $param_config['service_appId'] = 'xxxxxxxxxxx';


   $webservice = new Webservice($param_config);


   if(empty($_GET['service'])){ echo 'Service Not Available.'; exit(); }

   $service = $_GET['service'];
   $service_token = trim($_GET['service_token']);
 //Edit Weerasak 12/02/2565
 $arrProductType = array(); //เก็บข้อมูล ประเภทสินค้า
 $arrShippingType = array(); //เก็บข้อมูล ประเภทขนส่ง
 $track_ship_code = $db->select("project_master",array("id","master_value","group_id"),
 ["AND"=>[
      "group_id"=>["1","2"],
      "status"=>"O"
      ]
  ]);
 foreach($track_ship_code as $arr){
     if($arr["group_id"] == 1)
          $arrProductType[$arr['master_value']] = $arr['id'];
      if($arr["group_id"] == 2)
          $arrShippingType[$arr['master_value']] = $arr['id'];
 }
 //End edit  

   if($service=='sync'){

      $dataTracking = $db->select('web_tracking',array(
         "tracking","member_code","saller_name","order_id","order_type","status","weight","dime_x","dime_y","dime_w","order_comment"
      ),array("sync_status"=>'N'));

      $total = count($dataTracking);
      $success = 0;
      $failer = 0;

      if(count($dataTracking)>0){
         foreach($dataTracking as $vals){
            $result = $webservice->updateService($vals);
            if($result->status=='200'){
              // $db->update("web_tracking",array("sync_status"=>'Y'),array("tracking"=>$vals['tracking']));
               $success++;
            }else{
               $failer++;
            }
         }
      }
      $std = new stdClass();
      $std->total = $total;
      $std->success = $success;
      $std->failer = $failer;
      echo json_encode($std);
   }
   
   else if($service=='syncall'){
	   	$dataTracking = $db->select('project_webservice',"*",array("status"=>'N'));
	      
	    if(count($dataTracking)>0){
			foreach($dataTracking as $vals){
				$re = $webservice->sendDataTowebService($vals['service_token']);
				$db->update("project_webservice",array("status"=>'Y',"update_dtm"=>date('Y-m-d H:i:s')),array("service_token"=>$vals['service_token']));
				//echo $re."<br />";
			}
		}
   }
   else if($service=='syncdel'){


    $dataDel = $db->query("SELECT *  FROM project_webservice  WHERE status='Y' AND create_dtm < DATE_ADD(NOW(), INTERVAL -1 MONTH) ORDER BY id ")->fetchAll();

     if(count($dataDel)>0){

      foreach($dataDel as $vals){

        //echo $vals['id'];

        $ck = $db->delete("project_webservice",array("id"=>$vals['id']));
      }


     }
    //$dataTracking = $db->select('project_webservice',"*",array("status"=>'Y'),"ORDER" => ["id" => "ASC"]);

    //print_r($dataDel);

   }
   else if($service=='synctoken'){
	   	$dataTracking = $db->select('project_webservice',"*",array("service_token"=>$service_token));

      
	      
	    if(count($dataTracking)>0){
			foreach($dataTracking as $vals){
				$re = $webservice->sendDataTowebService($vals['service_token']);
				$db->update("project_webservice",array("status"=>'Y',"update_dtm"=>date('Y-m-d H:i:s')),array("service_token"=>$vals['service_token']));
				echo $re."<br />";
			}
		}
   }
   else if($service=='syncserver'){

		$result = $webservice->sendDataTowebService('SYNC_TRACK');
		//echo $result;
  // echo "<br/>";

    //print($data->data);
   
		//$dataClean = explode("133",$result);
		
		$data = json_decode($result);

	//  $dataTo = json_decode($data->data); //Edit By Weerasak
		$dataArr = (array)$dataTo;
        $sync_data = json_encode($data->data); //Add By Weerasak

		
        $randomString = substr(str_shuffle("123456789012345678901234567890123456789012345678901234567890"), 0, 20);
        
	    $db->insert("project_webservice",array(
	    										"service_token"=>"TCC_".$randomString,
	    										"service_method"=>'SYNC_TRACK',
	    										"service_data"=>$sync_data, //Edit Weerasak
	    										"status"=>'Y',
	    										"create_dtm"=>date('Y-m-d H:i:s'),
	    										"update_dtm"=>date('Y-m-d H:i:s')
                                                ));
        //$db->select("project_webservice","*",array("service_token[!]"=>'xxx'));
                                                
                                                //echo '777';exit();
	    $dataTo = $data->data;

      
		
		
		if(count($dataTo)>0){
			
		   		foreach($dataTo as $vals){
                       //Edit Weerasak 12-02-2565
                       //เพิ่ม parameter $arrProductType และ $arrShippingType
                       caltrack($arrProductType,$arrShippingType,$vals,$db);				  				  
				}
				
		   }
   	}
    else if($service=='syncserver_bill'){ 

		$result = $webservice->sendDataTowebService('SYNC_BILL');
		//echo $result;
		//$dataClean = explode("133",$result);
		//print_r($result);
		$data = json_decode($result);
		$dataTo = json_decode($data->data);
		$dataArr = (array)$dataTo;
		/*echo "<pre>";
	    print_r($data->data);
	    echo "</pre>";*/
	    $randomString = substr(str_shuffle("123456789012345678901234567890123456789012345678901234567890"), 0, 20);
	    $db->insert("project_webservice",array(
	    										"service_token"=>"TCC_".$randomString,
	    										"service_method"=>'SYNC_BILL',
	    										"service_data"=>'',
	    										"status"=>'Y',
	    										"create_dtm"=>date('Y-m-d H:i:s'),
	    										"update_dtm"=>date('Y-m-d H:i:s')
	    										));
	    
	    $dataTo = $data->data;
        $dataDet = $data->data2;
        
		if(count($dataTo)>0){
		   		foreach($dataTo as $vals){
                    //$value->order_type = $productTypeArr[$value->order_type];//Edit Weerasak 30/03/2565 convgoodscode($value->order_type);
                    //$shipType = $shipTypeArr[$value->ship_by]; //Add Weerasak 30/03/2565 ประเภทขนส่ง
                   // $value->ship_by = $shipType; //Add Weerasak 30/03/2565
                    $db->insert("test",array("value"=>'123'));
                        $oldTrack = $db->get("web_shipping","*",array("shipping_code"=>$vals->shipping_codeg));

		   			    $db->update("web_shipping",array(
                             "track_code"=>$vals->track_code,
                             "status"=>$vals->status,
                             "shipping_th_price"=>$vals->shipping_th_price,
                             "shipping_th_service_price"=>$vals->shipping_th_service_price,
                             "shipping_other_price"=>$vals->shipping_other_price,
                             "update_dtm"=>date('Y-m-d H:i:s'),
                             "update_by"=>'1',
                             "remark"=>$vals->bill_note
                         ),array(
                             "shipping_code"=>$vals->shipping_code
                         ));




				




				}


                foreach($dataDet as $valsDet){


                    $trackingData = $db->get("web_tracking","*",array("tracking"=>$valsDet->tracking));



                    //if($trackingData['tracking']!=''){


                    
					//print_r($trackingData);	

                    $price_true = 0;
		            $price_dimen = 0;
		            $cbm = 0;

		            if($trackingData['status']>=6){

		              $cbm =  ($trackingData['dime_x']*$trackingData['dime_y']*$trackingData['dime_w'])/1000000;
		              $price_dimen = $cbm*$dimen_rate['rate'];

		            }


		            $price_weight = 0;
		            $price_rate = '';

		            $ch_CAL = 150*$cbm;

		            $member_info = $db->get("project_user","*",array("user_code"=>$trackingData['member_code']));

		            
		            

              

                $dataRate = $db->select('project_user_rate',"*",array(
                    "AND"=>array(
                        "order_type"=>$trackingData['order_type'],
                        "ship_by"=>$trackingData['ship_by'], //Add Weerasak 30/03/2565 เพิ่มเงื่อนไขประเภทการขนส่ง
                        "user_id"=>$member_info['user_role_id']
                        )
                ));

                //print_r($dataRate);

                $ii =0;
                foreach($dataRate as $valsRate){


                    $masterCK = $db->get("project_master","*",array("id"=>$valsRate['unit']));

                    //print_r($masterCK);

                    if($trackingData['weight']>$ch_CAL){

                    	if($masterCK['master_char']=='CALK'){
                        	$price_weight = $valsRate['rate']*$trackingData['weight'];
	                        if($price_true<$price_weight){
	                            $price_true = $price_weight;
	                            $price_rate = $valsRate['unit_code'];
	                        }
                    	}

	                }else if($trackingData['weight']<$ch_CAL){

	                	if($masterCK['master_char']=='CALQ'){
	                        $cbm =  ($trackingData['dime_x']*$trackingData['dime_y']*$trackingData['dime_w'])/1000000;
	                        $price_dimen = $cbm*$valsRate['rate'];
	                        if($price_true<$price_dimen){
	                            $price_true = $price_dimen;
	                            $price_rate = $valsRate['unit_code'];
	                        }
	                    }

	                }

                    

                    


                }





                

                $dataInsert['price_weight'] = $price_weight;
	            $dataInsert['price_dimen'] = $price_dimen;
	            $dataInsert['price_true'] = $price_true;
	            $dataInsert['price_rate'] = $price_rate;
	            $dataInsert['cbm'] = $cbm;


	            $shipping_it['ship_price'] = $price_true;
	          	$shipping_it['track_rate'] = $price_rate;

	          	//print_r($dataInsert);	

	          	$last_shipping = $db->update("web_shipping_item",$shipping_it,array("tracking"=>$trackingData['tracking']));

	//print_r($dataInsert);	
	            $last_id = $db->update("web_tracking",$dataInsert,array("tracking"=>$trackingData['tracking']));




                    /*$db->update("web_shipping_item",array(
                            "ship_price"=>$trackingData['price_true'],
                            "track_rate"=>$trackingData['price_rate']
                        ),array(
                            "tracking"=>$valsDet->tracking
                        ));*/
                         //echo $db->last_query();

                    $ii++;
				}
		   }
   	}

function convgoodscode($code){
		$myArr = array("1","2","3");
		$convArr = array("G","M","B");
		
		$key = array_search($code,$convArr);
		
		return $myArr[$key];
}
//Edit Weerasak 12/02/2565
//เพิ่ม parameter $productType และ $shipType
function caltrack($productTypeArr,$shipTypeArr,$value,$db){
	
	
	
	$checkTrack = $db->get("web_tracking","*",array("tracking"=>$value->tracking));
	//print_r($value);
	
  $member_info = $db->get("project_user","*",array("user_code"=>$value->member_code));
      
     
  if($member_info['id']==''){
      
      return false; exit();
  }else{
  	//print_r($value);
  }

		
	if($checkTrack['tracking']!='' ){
		
           // echo $member_id;
			if(intval($checkTrack['status'])>=intval($value->status)){
				return false; exit();
            }
            $unit_type = $value->order_type;

            $value->order_type = $productTypeArr[$value->order_type];//Edit Weerasak 14/01/2565 convgoodscode($value->order_type);
            $shipType = $shipTypeArr[$value->ship_by]; //Add Weerasak 12/02/2565 ประเภทขนส่ง
            $value->ship_by = $shipType;

            $dataInsert = (array)$value;
            
            $dataInsert['update_dtm'] = date('Y-m-d H:i:s');
            $dataInsert['update_by'] = '0';
            $dataInsert['sync_status'] = 'Y';
			
			//print_r($value);
			
            //print_r($checkTrack);
          
            $price_true = 0;
            $price_dimen = 0;
            $cbm = 0;

            if($value->status>=6){

              $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
              $price_dimen = $cbm*$dimen_rate['rate'];

            }


            $price_weight = 0;
            $price_rate = '';


			$member_info = $db->get("project_user","*",array("user_code"=>$value->member_code));
			
		 
            if($member_info['id']==''){
				      return false; exit();
            }else {

                $dataRate = $db->select('project_user_rate',"*",array(
                    "AND"=>array(
                        "order_type"=>$value->order_type,
                        "ship_by"=>$shipType, //Add Weerasak 12/02/2565 เพิ่มเงื่อนไขประเภทการขนส่ง
                        "user_id"=>$member_info['user_role_id']
                        )
                ));


                $ch_CAL = 150*$cbm;

                

                foreach($dataRate as $valsRate){
                    $masterCK = $db->get("project_master","*",array("id"=>$valsRate['unit']));

                    //print_r($masterCK);

                    if($checkTrack['weight']>$ch_CAL){

                    	if($masterCK['master_char']=='CALK'){
                        	$price_weight = $valsRate['rate']*$value->weight;
	                        if($price_true<$price_weight){
	                            $price_true = $price_weight;
	                            $price_rate = $valsRate['unit_code'];
	                        }
                    	}

	                }else if($checkTrack['weight']<$ch_CAL){

	                	if($masterCK['master_char']=='CALQ'){
	                        $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
	                        $price_dimen = $cbm*$valsRate['rate'];
	                        if($price_true<$price_dimen){
	                            $price_true = $price_dimen;
	                            $price_rate = $valsRate['unit_code'];
	                        }
	                    }

	                }

                    

                    


                }


                // $dimen_rate = $db->get("project_user_rate","*",array(
                //     "AND"=>array(
                //         "user_id"=>$member_info['user_role_id'],
                //         "unit"=>'16',
                //         "order_type"=>$value->order_type,
                //         "ship_by"=>'4'
                //     )
                // ));

                // if($checkTrack['tracking']=='1000834700647'){
                //     echo '<pre>';
                //     print_r($member_info['id']);
                //     echo '</pre>';
                // }

                //$cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/10000000;
                //$price_dimen = $cbm*$dimen_rate['rate'];


                // $weight_rate = $db->get("project_user_rate","*",array(
                //     "AND"=>array(
                //         "user_id"=>$member_info['id'],
                //         "unit"=>'17',
                //         "order_type"=>$value->order_type,
                //         "ship_by"=>'4'
                //     )
                // ));

                // $price_weight = $weight_rate['rate']*$value->weight;

               //$price_true = $price_dimen;

            //    if($price_weight>$price_dimen){
            //       $price_true = $price_weight;
            //    }else{
            //     $price_true = $price_dimen;
            //    }  

            $maildata = renderMailHTML($value,$member_info,$db);
				if(intval($checkTrack['status'])<intval($value->status)){
           		sendMailNoti($member_info,$maildata);
				}

            }
			
		
            $dataInsert['price_weight'] = $price_weight;
            $dataInsert['price_dimen'] = $price_dimen;
            $dataInsert['price_true'] = $price_true;
            $dataInsert['price_rate'] = $price_rate;
            $dataInsert['cbm'] = $cbm;


            $shipping_it['ship_price'] = $price_true;
          	$shipping_it['track_rate'] = $price_rate;
          	$last_shipping = $db->update("web_shipping_item",$shipping_it,array("tracking"=>$value->tracking));

//print_r($dataInsert);	
            $last_id = $db->update("web_tracking",$dataInsert,array("tracking"=>$value->tracking));
 
			///

            $data_history = array();
            $data_history['tracking_id'] = $checkTrack['tracking_id'];
            $data_history['status'] = $value->status;
            $data_history['create_dtm'] = date('Y-m-d H:i:s');
            $data_history['create_by'] = '0';

            $log_history = $db->insert("web_tracking_history",$data_history);

            $dataUpdateItem = array();
            $dataUpdateItem['status'] = $value->status;
            $dataUpdateItem['product_sku_1'] = $value->tracking;
            $dataUpdateItem['create_dtm'] = date('Y-m-d H:i:s');
//print_r($dataUpdateItem);
 
			if($value->order_id!=''){  
           		$weborderArr = $db->get("web_order","*",array("order_number"=>$value->order_id));
          		$updateOrderItem = $db->update("web_order_item",$dataUpdateItem,array(
               "AND"=>array(
                  "order_id"=>$weborderArr['id'],
                  "saller_number"=>$value->saller_name,
                  "status[!]"=>'D',
                  "status[!]"=>'14'
               )
            ));

          		


          		
          		//SELECT * FROM `web_shipping_item`
			
			}
          // echo $db->last_query();

         }else{

            $unit_type = $value->order_type;
         	
             $value->order_type = $productTypeArr[$value->order_type];//Edit Weerasak 14/01/2565 convgoodscode($value->order_type);
             $shipType = $shipTypeArr[$value->ship_by]; //Add Weerasak 12/02/2565 ประเภทขนส่ง
             $value->ship_by = $shipType;
             
            $dataInsert = (array)$value;
            $dataInsert['create_dtm'] = date('Y-m-d H:i:s');
            $dataInsert['create_by'] = '0';
            $dataInsert['sync_status'] = 'Y';

            $member_info = $db->get("project_user","*",array("user_code"=>$value->member_code));
           // echo $db->last_query();

            

            $price_true = 0;
            $price_dimen = 0;
            $cbm = 0;
            $price_weight = 0;
            $price_rate = '';


            if($member_info['id']==''){
              return false; exit();
            }else{

                $dataRate = $db->select('project_user_rate',"*",array(
                    "AND"=>array(
                        "order_type"=>$value->order_type,
                        "ship_by"=>$shipType, //Add Weerasak 12/02/2565 เพิ่มเงื่อนไขประเภทการขนส่ง
                        "user_id"=>$member_info['user_role_id']
                        )
                ));

                


                // $dimen_rate = $db->get("project_user_rate","*",array(
                //     "AND"=>array(
                //         "user_id"=>$member_info['id'],
                //         "unit"=>'16',
                //         "order_type"=>$value->order_type,
                //         "ship_by"=>'4'
                //     )
                // ));

                 $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
                 $price_dimen = $cbm*$dimen_rate['rate'];


                 $ch_CAL = 150*$cbm;



                 foreach($dataRate as $valsRate){
                    $masterCK = $db->get("project_master","*",array("id"=>$valsRate['unit']));


                    if($value->weight>$ch_CAL){

                    	if($masterCK['master_char']=='CALK'){
                        	$price_weight = $valsRate['rate']*$value->weight;
	                        if($price_true<$price_weight){
	                            $price_true = $price_weight;
	                            $price_rate = $valsRate['unit_code'];
	                        }
                    	}

	                }else if($value->weight<$ch_CAL){

	                	if($masterCK['master_char']=='CALQ'){
	                        $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
	                        $price_dimen = $cbm*$valsRate['rate'];
	                        if($price_true<$price_dimen){
	                            $price_true = $price_dimen;
	                            $price_rate = $valsRate['unit_code'];
	                        }
	                    }

	                }

                   /* if($masterCK['master_char']=='CALK'){
						
                        $price_weight = $valsRate['rate']*$value->weight;
						
                        if($price_true<$price_weight){
                            $price_true = $price_weight;
                            $price_rate = $valsRate['unit_code'];
                        }
                    } 
                    if($masterCK['master_char']=='CALQ'){
                        $cbm =  ($value->dime_x*$value->dime_y*$value->dime_w)/1000000;
                        $price_dimen = $cbm*$valsRate['rate'];
                        if($price_true<$price_dimen){
                            $price_true = $price_dimen;
                            $price_rate = $valsRate['unit_code'];
                        }
                    }*/
                }


                // $weight_rate = $db->get("project_user_rate","*",array(
                //     "AND"=>array(
                //         "user_id"=>$member_info['id'],
                //         "unit"=>'17',
                //         "order_type"=>$value->order_type,
                //         "ship_by"=>'4'
                //     )
                // ));

                // $price_weight = $weight_rate['rate']*$value->weight;

                // $price_true = $price_dimen;

                // if($price_weight>$price_dimen){
                //     $price_true = $price_weight;


                // }
                

                if($member_info['user_code']!='b999'){

                $maildata = renderMailHTML($value,$member_info,$db);

            	}


               // sendMailNoti($member_info,$maildata);
            }


            if($member_info['user_code']!='b999'){

            	
            	$dataInsert['tracking'] = $value->tracking;
            	$dataInsert['member_code'] = $value->member_code;
            	$dataInsert['saller_name'] = '';
            	$dataInsert['order_id'] = '';
            	$dataInsert['order_type'] = $valsRate['order_type'];
            	//$dataInsert['ship_by'] = $shipType; //Edit Weerasak 12/02/2565 ประเภทขนส่ง
            	$dataInsert['order_id'] = '';
            	$dataInsert['order_id'] = '';
            	
            	$dataInsert['weight'] = $value->weight;
            	$dataInsert['weight_cn'] = $value->weight_cn;
            	$dataInsert['dime_x'] = $value->dime_x;
            	$dataInsert['dime_y'] = $value->dime_y;
            	$dataInsert['dime_w'] = $value->dime_w;


            	$dataInsert['price_weight'] = $price_weight;
	            $dataInsert['price_dimen'] = $price_dimen;
	            $dataInsert['price_true'] = $price_true;
	            $dataInsert['price_rate'] = $price_rate;
	            $dataInsert['cbm'] = $cbm;


	            $dataInsert['other_desc'] = '';
	            $dataInsert['other_price'] = '0';
	            $dataInsert['tracking_img'] = '';
	            $dataInsert['tracking_slip'] = '';
	            $dataInsert['num_box'] = '0';
	            $dataInsert['status'] = $value->status;
	            $dataInsert['flag'] = '1';
	            $dataInsert['hold_status'] = 'N';
	            $dataInsert['create_dtm'] = date("Y-m-d H:i:s");
	            $dataInsert['update_dtm'] = '0000-00-00 00:00:00';

	            


	            $dataInsert['create_by'] = $member_info['id'];
	            $dataInsert['update_by'] = '0';
	            $dataInsert['mail_flag'] = 'Y';

	            $last_id = $db->insert("web_tracking",$dataInsert);

            }else{

	            $dataInsert['price_weight'] = $price_weight;
	            $dataInsert['price_dimen'] = $price_dimen;
	            $dataInsert['price_true'] = $price_true;
	            $dataInsert['price_rate'] = $price_rate;
	            $dataInsert['cbm'] = $cbm;
	            $dataInsert['create_by'] = $member_info['id'];
	            $last_id = $db->insert("web_tracking",$dataInsert);

            
        	}
			
            //print_r($dataInsert);


            $data_history = array();
            $data_history['tracking_id'] = $last_id;
            $data_history['status'] = $value->status;
            $data_history['create_dtm'] = date('Y-m-d H:i:s');
            $data_history['create_by'] = '0';


            $shipping_it['ship_price'] = $price_true;
          	$shipping_it['track_rate'] = $price_rate;


          	$last_shipping = $db->update("web_shipping_item",$shipping_it,array("tracking"=>$value->tracking));
          	

            $log_history = $db->insert("web_tracking_history",$data_history);

            $dataUpdateItem = array();
            $dataUpdateItem['status'] = $value->status;
            $dataUpdateItem['product_sku_1'] = $value->tracking;
            $dataUpdateItem['create_dtm'] = date('Y-m-d H:i:s');
			
				if($value->order_id!=''){ 
					$weborderArr = $db->get("web_order","*",array("order_number"=>$value->order_id));
					$updateOrderItem = $db->update("web_order_item",$dataUpdateItem,array(
					   "AND"=>array(
						  "order_id"=>$weborderArr['id'],
						  "saller_number"=>$value->saller_name,
						  "status[!]"=>'D',
						  "status[!]"=>'14'
					   )
					));
					
				}
         }
}

//$dataArr = (array)$vals;
					/*$dataArr['order_type'] = convgoodscode($dataArr['order_type']);
					$ck = $db->get("web_tracking","*",array("tracking"=>$dataArr['tracking']));
					if($ck['tracking']==''){
						$db->insert("web_tracking",$dataArr);
						$track_id = $database->id();
						$db->insert("web_tracking_history",array(
														"tracking_id"=>$track_id,
														"status"=>$dataArr['status'],
														"create_dtm"=>$dataArr['update_dtm']
														));
					}else{
						if($ck['status']!=$dataArr['status']){
							
							$db->update("web_tracking",$dataArr,array("tracking"=>$dataArr['tracking']));
							$db->insert("web_tracking_history",array(
														"tracking_id"=>$ck['tracking_id'],
														"status"=>$dataArr['status'],
														"create_dtm"=>$dataArr['update_dtm']
														));
						}
						
                    }*/

function renderMailHTML($tracking,$member,$db){
    
    $testStatus = '';
    if($tracking->status=='1'){
        $testStatus = 'ร้านจีนให้แทรค';
    }
    else if($tracking->status=='2'){
        $testStatus = 'เข้าโกดังจีน';
    }
    else if($tracking->status=='3'){
        $testStatus = 'ออกโกดังจีน';      
    }
    else if($tracking->status=='4'){
        $testStatus = 'เข้าโกดังไทย';             
    }
    else if($tracking->status=='5'){
        $testStatus = 'รอตรวจสอบพัสดุ';                   
    }
    else if($tracking->status=='6'){
        $testStatus = 'รอการชำระเงิน';                              
    }
    else if($tracking->status=='7'){
        $testStatus = 'เตรียมจัดส่ง';                                      
    }
    else if($tracking->status=='8'){
        $testStatus = 'จัดส่งพัสดุ';     
    }

    // $textTPL = '<div id="outbody" style="width: 70%;
    // border: 1px solid #aaaaaa;
    // border-radius: 5px;
    // font-size: 15px;
    // font-family: "Tahoma";
    // color: #6b6b6b;
    // padding-top: 20px;
    // padding-bottom: 20px;
    // padding-left: 20px;
    // margin:0 auto;
    // text-align:center;
    // -webkit-box-shadow: 0px 7px 22px -8px rgba(0,0,0,0.75);
    // -moz-box-shadow: 0px 7px 22px -8px rgba(0,0,0,0.75);
    // box-shadow: 0px 7px 22px -8px rgba(0,0,0,0.75);">
    
    //     <div class="body">
    //         เรียนท่านลูกค้าคุณ '.$member["user_name"].'<br/><br/>
    //         แจ้งสถานะสินค้าเลนที่ออเดอร์ <b>'.$tracking->order_id.'</b> ร้านค้าที่ <b>'.$tracking->saller_name.'</b> เลขแทรคสินค้า <b>'.$tracking->tracking.'</b><br />
    //         สินค้าของท่านอยู่ในสถานะ : <b>'.$testStatus.'</b><br/><br/><br/>
    //         ขอแสดงความนับถือ
    //     </div>
    
    // </div>';
    $ref_id = $member["ref_id"];
    
    if(intval($ref_id)==0){
        $ref_id = 1; 
    }
    $gen_id = '10';
    if($tracking->status=='4'){
        $gen_id = '11';
    }
    $result = $db->get("project_email_template","*",array(
        "AND"=>array(
            "gen_id"=>$gen_id,
            "agency_id"=>$ref_id
        )
    ));
    //print_r($result);
    $checkTrack = $db->get("web_tracking","*",array("tracking"=>$tracking->tracking));
    
    $link = 'http://trackingsystem.taobaochinacargo.com/#/tracking/view/'.$checkTrack['tracking_id'];

    $dataHtml = file_get_contents("http://trackingsystem.taobaochinacargo.com/app/view/adminlte/utility_ui/mail.htm");
    $dataHtml = str_replace("{MAIL_BODY}",$result['gen_field_1'],$dataHtml);
    $dataHtml = str_replace("{TRACKING}",$tracking->tracking,$dataHtml);
    $dataHtml = str_replace("{ORDER}",$tracking->order_id,$dataHtml);
    $dataHtml = str_replace("{SELLER}",$tracking->saller_name,$dataHtml);
    $dataHtml = str_replace("{STATUS}",$testStatus,$dataHtml);
    $dataHtml = str_replace("{CUST_NAME}",$member["user_name"],$dataHtml);
    $dataHtml = str_replace("{LINK}",$link,$dataHtml);

    $subject = $result['gen_field_2'];

    $arrReturn = array();
    $arrReturn['body']=$dataHtml;
    $arrReturn['subject']=$subject;
    return $arrReturn;
}   
function renderMailHTML2($billing,$member){
    $testStatus = '';
    if($tracking->status=='1'){
        $testStatus = 'ร้านจีนให้แทรค';
    }
    else if($tracking->status=='2'){
        $testStatus = 'เข้าโกดังจีน';
    }
    else if($tracking->status=='3'){
        $testStatus = 'ออกโกดังจีน';      
    }
    else if($tracking->status=='4'){
        $testStatus = 'เข้าโกดังไทย';             
    }
    else if($tracking->status=='5'){
        $testStatus = 'รอตรวจสอบพัสดุ';                   
    }
    else if($tracking->status=='6'){
        $testStatus = 'รอการชำระเงิน';                              
    }
    else if($tracking->status=='7'){
        $testStatus = 'เตรียมจัดส่ง';                                      
    }
    else if($tracking->status=='8'){
        $testStatus = 'จัดส่งพัสดุ';     
    }

    $ref_id = $member["ref_id"];
    if(intval($ref_id)==0){
        $ref_id = 1; 
    }
    $gen_id = '12';

    $result = $db->get("project_email_template","*",array(
        "AND"=>array(
            "gen_id"=>$gen_id,
            "agency_id"=>$ref_id
        )
    ));
    $checkTrack = $db->get("web_tracking","*",array("tracking"=>$tracking->tracking));
    $link = 'http://trackingsystem.taobaochinacargo.com/#/tracking/view/'.$checkTrack['tracking_id'];

    $dataHtml = file_get_contents("http://trackingsystem.taobaochinacargo.com/app/view/adminlte/utility_ui/mail.htm");
    $dataHtml = str_replace("{MAIL_BODY}",$result['gen_field_1'],$dataHtml);
    $dataHtml = str_replace("{TRACKING}",$tracking->tracking,$dataHtml);
    $dataHtml = str_replace("{ORDER}",$tracking->order_id,$dataHtml);
    $dataHtml = str_replace("{SELLER}",$tracking->saller_name,$dataHtml);
    $dataHtml = str_replace("{STATUS}",$testStatus,$dataHtml);
    $dataHtml = str_replace("{CUST_NAME}",$member["user_name"],$dataHtml);
    $dataHtml = str_replace("{LINK}",$link,$dataHtml);

    $subject = $result['gen_field_2'];

    $arrReturn = array();
    $arrReturn['body']=$dataHtml;
    $arrReturn['subject']=$subject;
    return $arrReturn;
}                 


function sendMailNoti($mamber,$ddhtml){

  
    $mail = new PHPMailer;
    $mail->IsSMTP();
	$mail->IsHTML(true);
    $mail->SMTPDebug = 0;  
    $mail->Debugoutput = 'html';   
    $mail->CharSet = 'utf-8';    
    $mail->Host = 'mail.taobaochinacargo.com';  
    $mail->Port = 587;   
    $mail->SMTPAuth = true;
     
	  
    $mail->Username = 'service@taobaochinacargo.com';    
    $mail->Password = 'service2018!';    
    $mail->setFrom('service@taobaochinacargo.com', 'TAOBAO CHINA CARGO');   
    $mail->addAddress($mamber['user_email'], $mamber['user_name']);   
    $mail->Subject = $ddhtml['subject'];   
    $mail->msgHTML($ddhtml['body']);
    $mail->send();
}
 ?>
