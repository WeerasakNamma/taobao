<?php
   date_default_timezone_set('Asia/Bangkok');
   include('libs/Webservice.class.php');
   include('libs/medoo.php');
   include('config.php');

   $db = new medoo([
   	// required
   	'database_type' => 'mysql',
   	'database_name' => $db_database_name,
   	'server' => 'localhost',
   	'username' => $db_username,
   	'password' => $db_password,
   	'charset' => 'utf8',

   	// [optional]
   	'port' => 3306,

   	// [optional] Table prefix
   	'prefix' => '',

   	// [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
   	'option' => [
   		PDO::ATTR_CASE => PDO::CASE_NATURAL
   	]
   ]);



   if(empty($_GET['token'])){ echo 'Service Not Available.'; exit(); }

   $service = $_GET['token'];
   
   if($service=='SYNC_TRACK'){
	   	$std = new stdClass();
	    $std->service = 'SYNC_TRACK';
	    if($_GET['start_date']!=''){
			$std->start_date = $_GET['start_date'];
			$std->end_date = $_GET['end_date'];
		}else{
            //$std->start_date = "2017-07-24 00:00:01";
            $std->start_date = date('Y-m-d 00:00:00');
			   $std->end_date = date('Y-m-d H:i:s');
		}
	    echo json_encode($std);
   }
   else if($service=='SYNC_BILL'){
	   	$std = new stdClass();
	    $std->service = 'SYNC_BILL';
	    if($_GET['start_date']!=''){
			$std->start_date = $_GET['start_date'];
			$std->end_date = $_GET['end_date'];
		}else{
			//$std->start_date = "2017-07-24 00:00:01";
			$std->start_date = date('Y-m-d 00:00:00');
			$std->end_date = date('Y-m-d H:i:s');
		}
	    echo json_encode($std);
   }
   else{
   		$dataReturn = $db->get('project_webservice',"*",array("service_token"=>$service));
	   
	   	if($dataReturn['service_token']!=''){
	   		$std = new stdClass();
	        $std->service = $dataReturn['service_method'];
	        $std->data = $dataReturn['service_data'];
	        echo json_encode($std);
	   	}
   }
   
   
   


 ?>
