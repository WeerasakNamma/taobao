<?php
   date_default_timezone_set('Asia/Bangkok');
   include('libs/Webservice.class.php');
   include('libs/medoo.php');
   include('config.php');

   $db = new medoo([
   	// required
   	'database_type' => 'mysql',
   	'database_name' => $db_database_name,
   	'server' => 'localhost',
   	'username' => $db_username,
   	'password' => $db_password,
   	'charset' => 'utf8',

   	// [optional]
   	'port' => 3306,

   	// [optional] Table prefix
   	'prefix' => '',

   	// [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
   	'option' => [
   		PDO::ATTR_CASE => PDO::CASE_NATURAL
   	]
   ]);



   if(empty($_GET['token'])){ echo 'Service Not Available.'; exit(); }

   $param_config = array();
   $param_config['service_url'] = 'http://www.accgroup.co.th/act-system/webservice/user.php';
   $param_config['service_appId'] = 'xxxxxxxxxxx';

   $webservice = new Webservice($param_config);

   $service = $_GET['token'];
   
   if($service=='SYNC_USER'){
	   	$datauser_code = $webservice->sendDataTowebService($service);
        echo '<pre>';
        //print_r(json_decode($datauser_code));
        echo '</pre>';
        $dataSet = json_decode($datauser_code);
       // print_r($dataSet);
       echo count($dataSet)."<br />";
       $i=1;
        foreach($dataSet as $data){
              $member_code =   genmembercode($data->user_type,'',$db);
              $user_role_id = '2';
              if($data->user_type=='ship'){
                $user_role_id='3';
              }
              if($data->user_datecreate=='0000-00-00 00:00:00'){
                  $data->user_datecreate = date("Y-m-d H:i:s");
              }
              $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
              $db->insert("project_user",array(
                  "user_code"=>$member_code,
                  "extra_code"=>$data->user_code,
                  "ref_id"=>'1',
                  "user_username"=>$data->user_email,
                  "user_password"=>md5($randomString),
                  "user_email"=>$data->user_email,
                  "user_name"=>$data->user_name." ".$data->user_surname,
                  "user_address"=>$data->user_address,
                  "user_phone_number"=>$data->user_tel,
                  "user_role_id"=>$user_role_id,
                  "active_status"=>'O',
                  "create_dtm"=>$data->user_datecreate,
                  "create_by"=>'1',
                  "update_dtm"=>date("Y-m-d H:i:s")

              ));
              echo $i."<br />";
              $i++;
        }
   }
   else if($service=='SYNC_USER_CODE'){
       $result = $db->select("project_user",array("user_code","extra_code"),array("extra_code[!]"=>''));
       //print_r($result);
       echo json_encode($result);
   }else{
   		
   }
   
   function genmembercode($type,$ref_id,$db){
		
		if($type=='order'){
			$type = "MEMBER_ORDER";
			$extra_code = 'A';
			if($ref_id!=''){
				$ref_user = $db->get("project_user","*",array("id"=>$ref_id));
				$extra_code = $ref_user['extra_code'];
				if($extra_code==''){
					$extra_code = 'A';
				}
			}
			
			$dataCode = $db->get("project_run_number","*",array("AND"=>array("run_module"=>$type,"run_extra"=>$extra_code)));
			$db->update("project_run_number",array("run_number"=>($dataCode['run_number']+1)),array("run_id"=>$dataCode['run_id']));
			
			$lastRun = "T".$extra_code."O".str_pad($dataCode['run_number'],4,"0",STR_PAD_LEFT);
			return $lastRun;
			
		}
		
		if($type=='ship'){
			$type = "MEMBER_SHIP";
			$extra_code = 'A';
			if($ref_id!=''){
				$ref_user = $db->get("project_user","*",array("id"=>$ref_id));
				$extra_code = $ref_user['extra_code'];
				if($extra_code==''){
					$extra_code = 'A';
				}
			}
			
			$dataCode = $db->get("project_run_number","*",array("AND"=>array("run_module"=>$type,"run_extra"=>$extra_code)));
			$db->update("project_run_number",array("run_number"=>($dataCode['run_number']+1)),array("run_id"=>$dataCode['run_id']));
			
			$lastRun = "T".$extra_code."S".str_pad($dataCode['run_number'],4,"0",STR_PAD_LEFT);
			return $lastRun;
		}
		
		
		
	}
   


 ?>
